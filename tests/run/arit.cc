/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/* 
 * This test demonstrates
 *
 *	- recursion
 *	- arithmetic operators
 *	- if
 *	- return statement
 *
 * It should return 0, as the correct precomputed result is subtracted at the end.
 */

#include <lio.hh>

int factorial_recur(int n) {
	if ( n==0 ) {
		return 1;
	}
	return factorial_recur(n-1)*n;
}

int fibonaci_recur(int n) {
	if (n<3) {
		return 1;
	}
	return fibonaci_recur(n-1) + fibonaci_recur(n-2);
}

int a,b,c,d,e,f,g,h;

int arit_test() {
	a = 3333 + 7777;//11110
	b = 6666 - 3333;//3333
	c = 1000 * 1000;//1000000
	d = 1000 / 10;	//100
	e = 1001 % 10;	//1
	f = 15 & 4; 	//4
	g = 10 | 5; 	//15
	h = 9 ^ 15; 	//6 
	
	return a + b + c + d + e + f + g + h;//1014569;	
}

int main() {
	print("Test of arit. operations. Result(should be 1014569)=");
	print(arit_test());
	
	print("Factorial of 10. Result(should be 3628800)=");
	print(factorial_recur(10));

	print("9th number of Fibonnaci. Result(should be 34)=");
	print(fibonaci_recur(9));
	
	return arit_test() + fibonaci_recur(9) - 34 + factorial_recur(10) - 3628800;
}
