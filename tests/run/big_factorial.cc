/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*
    This example computes big factorial. Maximum input number is 1000.  
    
    It demonstrates:
	- pointer aritmetic
	- pointer dereference
	- assigment to dereferenced pointers
	- function calls
	
*/
#include <lio.hh>

#define BUFLEN 8192
#define ALLOC_BUFFER string_alloc(BUFLEN) 

void zero_buffer (char *buff)
{
    int cnt = 0;
    
    char* tmp1;
    
    while ( cnt < BUFLEN) {
	tmp1 = buff + cnt;
	*tmp1 = '0';
	cnt++;
    }
    
    tmp1 = buff + ( BUFLEN - 1 );
	    
    *tmp1 = 0;
    
}

void minus_one (char *g)
{

    int p;
    char digit;

    char* tmp;
    
    p = string_length(g) - 1;
    
    tmp = g + p;
    digit = *tmp;
    
    while (digit == '0')
    {
	tmp = g + p;
	*tmp = '9';
	--p;
        
	tmp  = g + p;
        digit = *tmp;
    }
    
    tmp = g + p;
    *tmp -= 1;
}

int isnull (char *g)
{
    char* tmp;
    int p, ok = 1;

    p = 0;
    while ( p < string_length(g)) {
	tmp = g + p;
        
	if (*tmp != '0')
          ok = 0;
	
	p++;
    }
    
    return ok;
}


void multiply (char *g1, char *g2, char *g3)
{
    char* tmp;
    
    int gp1, gp2, cumpos, respos, mod, div;
    int cmod, cdiv, resoff, wdig1, wdig2, base;

    zero_buffer (g3);
    
    gp2 = string_length(g2) - 1;
    
    while (gp2 >= 0)
    {
	  tmp = g2 + gp2;
          wdig2 = *tmp - 48;
	  
          resoff = string_length(g2) - gp2 - 1;
          respos = BUFLEN - resoff - 2;
          
	  gp1 = string_length(g1)-1;
	  while (gp1 >= 0)
          {
		tmp = g1 + gp1;
                wdig1 = *tmp - 48;
		
                mod = (wdig1 * wdig2) % 10;
		div = (wdig1 * wdig2) / 10;
		
		tmp = g3 + respos;
		base = *tmp - 48;
		cmod = (base + mod) % 10;
		cdiv = (base + mod) / 10 + div;
		
		tmp = g3 + respos;
		*tmp = cmod + 48;
		cumpos = --respos;
		
		while (cdiv > 0)
		{
		    tmp = g3 + cumpos;
		    base = *tmp - 48;
		    
		    tmp = g3 + respos;
		    *tmp = ((base + cdiv) % 10 + 48);
		    cumpos--;
		    cdiv = (base + cdiv) / 10;
		}
		
		gp1--;
	    }
	    
	    gp2--;
    }
    
    respos = 0;
    tmp = g3 + respos;
    while (*tmp == '0') {
	 respos++ ;
	 tmp = g3 + respos;
    }
    
    tmp = g3 + respos;
    string_duplicate (g3, tmp);

    if (*g3 == 0)
	string_duplicate (g3, "0");
	
}





void factorial (char *g)
{
    char *h1 = ALLOC_BUFFER, *h2 = ALLOC_BUFFER;

    string_duplicate (h1, "1");

    while (!isnull(g))
    {
        multiply (h1, g, h2);
        string_duplicate (h1, h2);
        minus_one (g);
    }
    
    string_duplicate (g, h1);
    
    string_dealloc(h1);
    string_dealloc(h2);
}


int main ()
{
	char *g = ALLOC_BUFFER;
        print ("Enter a number (max is 999): ");
	
	get_string_from_stdin(g,10);
	
	int len = string_length(g);
	char* tmp  = g + len;
	tmp--;
	*tmp = 0;
	
	
        print("Factorial is: ");
	factorial (g);
	print (g);
	
	string_dealloc(g);
}

