/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/* 
 * This is simple compilable 'game'. It demonstrates that the following language constructs
 * work
 *
 *	- dynamic initialization (i.e. initialization by expression) of a pointer
 *	- overload resolution (choses the correct print() for const char *)
 *	- while cycle works
 *	- if statement works
 *	- the conversion for pointer inside the if() condition is correctly converted to bool
 */

#include <lio.hh>

int main() {
	char* s = string_alloc(100);
	
	print("I have chosen number 1-10. Guess which one!");
	
	while(true) {
		s = get_string_from_stdin(s,99);
		if (!s) {
			print("End");
			return 1;
		}
		
		if ( string_compare(s,"11\n") ) {
			print("Wrong guess!");
		} else {
			print("Geez .. ");
			return 0;
		}
	}
	
	return 0;
}

