/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/* 
 * This example demonstrates the functionality of overload resolution.
 */

#include <lio.hh>

int a = 13;


/* If the following parameters are changed to float and double
 * for the following two functions, lestes will correctly detect
 * the ambiguity. This also works for classes, but backend can't
 * habdle them yet
 */
long f(unsigned long z) 
{
	print("Vybrala se funkce unsigned long\n");
	return z+a;
}


long f(long z) 
{
	print("Vybrala se funkce signed long\n");
	return z+a;
}

int f(int z)
{
	print("Vybrala se funkce int\n");
	return z+a;
}

int main(int argc, char *argv[])
{ 
	int d;
	int e = 3;
	long g = 3;
	
	d = e + f(g);
	print(d+1);
	return ++d;
}

