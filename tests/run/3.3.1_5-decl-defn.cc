/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
//?GCC 3.4.1: vypise: 7, 7 protoze ignoruje deklaraci default arg pri volani
// ma vypsat 7, 3
#include <iostream>
namespace A{
typedef int T;
int f(T i) { std::cout << "tady " << i <<'\n';}
}
int T;
// gcc 3.4.1: decl
int A::f(T=7);

int main(){
A::f();
// gcc 3.4.1: decl
// gcc 2.95.4: cast s volanim
int (::A::f(T=3));
//int ::A::f(T=3);
A::f();
return 0;
}

// dusledek pro nas:

// definice se neodlisuji od deklaraci, protoze menit lookup v zavislosti na
// tom, zda nekde vpravo (libovolne daleko) bude drive { nebo ; je sebevrazda.
