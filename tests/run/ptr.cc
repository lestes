/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*
 * Example demonstrating as->ss expressions transformation and overload resolution, specifically
 * for the postfix and prefix ++ and -- operators for integers and pointers.
 *
 * From the resulting dump, there can be seen what is the difference between postfix and prefix 
 * increment (can be interchanged for decrement). The main things to notice are
 * 
 * 	- the result of prefix operation is lvalue, and therefore can be used 
 *	  further in expression expecting lvalue (e.g. another increment/decrement).
 *	- the result of the prefix operation (as seen by 'upper' expressions) is the
 * 	  assignment operation itself. 
 *	- the result of the postfix operation (as seen by 'upper' expressions) is the
 *	  + (or - respectively) operation.
 *
 * \author jikos
 */

int *ptr, *ptr2;
char *p;
int i;
long l;
bool t;

int main()
{
	/* This shows that it is possible to assign pointers of the same type */
	ptr = ptr2;
	ptr++;
	ptr+=1234;
	ptr-=3;

	/* This shows dereferencing of pointer */
	i = *ptr;
	
	/* If the following line is uncommented, the compilation will (correctly) fail, because
	 * [13.6] does not allow assigning pointers of non-compatible types
	 */
	//ptr = p;

	/* If the following line is uncommented, the compilation will (correctly) fail, because
	 * there is no operator+(p*, p*)
	 */
	//p1 += p2++;
	
	/* the following three expressions demonstrate that pointer arithmetic works */
	ptr+1;
	ptr-1;
	1+ptr;

	/* this demonstrates that it is possible to subtract pointer from pointer. 
	 * it also shows that it doesn't matter whether the result is int or long, as
	 * assign is defined for all combinations of arithmetic types and promoted arithmetic types
	 */
	l = ptr - ptr2;

	/* check if p1 > p2 in terms of pointer comparsion */
	t = ptr > ptr2;
	
	/* if you uncomment the following, the compilation will (correctly) fail. the result
	 * of pointer subtraction is not pointer
	 */
	//ptr = ptr - ptr2;


	/* if you uncomment the following, the compilation will (correctly) fail.
	 * the norm doesn't define ptrdiff_t - ptr */
	// 1-ptr 

	++(++ptr);
	(++ptr)++;

	++(++i);
	(++i)++;
}

