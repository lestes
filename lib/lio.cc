/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <stdio.h>
#include <string.h>
#include <malloc.h>

int print(int i){
return printf("%d\n",i);
}

int print1(int i) {
return printf("%d",i);
}

int print(const char *s){
return printf("%s\n",s);
}

int print1(const char *s){
return printf("%s",s);
}

int print(int i,const char *s){
return printf("%d %s\n",i,s);
}

int print(const char *s,int i){
return printf("%s %d\n",s,i);
}

int print(int i,int j){
return printf("%d %d\n",i,j);
}

int print(const char *s,const char *t){
return printf("%s %s\n",s,t);
}

char* get_string_from_stdin(char *buf, int max) {
	return fgets(buf,max,stdin);
}

int string_compare(char* s1, const char* s2) {
	return strcmp(s1,s2);
}

char* string_alloc(int length) {
	return (char*)malloc(length);
}

char * string_duplicate( const char * str )
{
	return strdup( str );
}

int string_duplicate(char* dest, const char * src )
{
	strcpy(dest,src);
	
	return strlen(dest);
}

int string_length(char * s) {
    return strlen(s);
}

void string_dealloc(char* s) {
    free(s);
}


int* int_array_alloc(int len) {
    return (int*)malloc(len*sizeof(int));
}

void int_array_dealloc(int* arr) {
    free(arr);
}
