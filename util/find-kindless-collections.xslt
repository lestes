<?xml version="1.0"?>
<!DOCTYPE xsl:transform [
    <!ENTITY nl "&#x0A;">
    <!ENTITY tab "&#x09;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:lsd="http://lestes.jikos.cz/schemas/lsd">
<!-- Generate C++ headers for data structures from XML description. -->
    <xsl:output method="text" encoding="ISO-8859-1"/>

    <xsl:template match="/">
	    <xsl:for-each select="lsd:lsd/lsd:class/lsd:collection[count(@kind) = 0]">
		    <xsl:value-of select="concat('&tab;',../@name,'/',@name,'&nl;')" />
	    </xsl:for-each>
	    <!--xsl:for-each select="lsd:lsd/lsd:class[count(@base) = 0]">
		    <xsl:value-of select="concat(@name,'&nl;')" />
	    </xsl:for-each-->
    </xsl:template>
</xsl:stylesheet>
