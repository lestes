<?xml version="1.0"?>
<!DOCTYPE xsl:transform [
    <!ENTITY nl "&#x0A;">
    <!ENTITY tab "&#x09;">
]>

<!--
	This file contains xslt templates that process machine description XML file and generate
	/target/machine/${CPU}/lestes/md/tasm/tm_asm.mdg.hh file. The generated file
	contains declaration of tm_asm class.
-->


<xsl:transform	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:md="http://lestes.jikos.cz/machinedescription">
<xsl:output method="text" encoding="ISO-8859-1"/>

<xsl:template match="/md:TargetArchitecture">
    <xsl:text>#ifndef lestes__backend__target__tm_asm_mdg_hh__included
#define lestes__backend__target__tm_asm_mdg_hh__included
/* This file is automaticaly generated from machine-description */

/*!
	\file
	\brief Target-machine asm description.
	\author jaz
*/

#include &lt;lestes/md/tasm/tm_asm_base.g.hh&gt;
#include &lt;lestes/md/common.hh&gt;

package(lestes);
package(md);
package(tasm);

/*!
	\brief Wrapper class for asm directives and asm language element templates.
*/
class tm_asm : public tm_asm_base {
&tab;public:
</xsl:text>

<xsl:apply-templates mode="asm_def_function" select="md:Assembler/md:Entities/md:Entity"/> 

<xsl:text>
};

end_package(tasm);
end_package(md);
end_package(lestes);

#endif

</xsl:text>
</xsl:template>

<xsl:template mode="asm_def_function" match="*">
	<xsl:text>&tab;&tab;static lstring </xsl:text><xsl:value-of select="@id"/><xsl:text>_get();&nl;</xsl:text> 
</xsl:template>


</xsl:transform>



