<?xml version="1.0"?>
<!DOCTYPE xsl:transform [
    <!ENTITY nl "&#x0A;">
    <!ENTITY tab "&#x09;">
]>

<!--
	This file contains xslt templates that process machine description XML file and generate
	/target/machine/${CPU}/lestes/md/instructions/tm_register.mdg.hh file. The generated file
	contains enumeration of register ids.
-->


<xsl:transform	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:md="http://lestes.jikos.cz/machinedescription">
<xsl:output method="text" encoding="ISO-8859-1"/>

<xsl:template match="/md:TargetArchitecture">
    <xsl:text>#ifndef lestes__md__registers__tm_register_mdg_hh__included
#define lestes__md__registers__tm_register_mdg_hh__included
/* This file is automaticaly generated from machine-description */

/*!
	\file
	\brief IDs used by ::lestes::md::registers::tm_register class.
	\author jaz
*/

#include &lt;lestes/std/objectize_macros.hh&gt;

package(lestes);
package(md);
package(registers);

/*!
	\brief Ids of registers defined by machine description.
*/
enum reg_id_type {&nl;&tab;R_UNDEFINED,&tab;//0</xsl:text>
    <xsl:apply-templates mode="tm_reg_id" select="md:Registers" />
	<xsl:text>
	RIT_TERMINATOR
};

end_package(registers);
end_package(md);

package(std);

specialize_objectize_for_enum( ::lestes::md::registers::reg_id_type );

end_package(std);
end_package(lestes);

#endif&nl;</xsl:text>
</xsl:template>



<xsl:template mode="tm_reg_id" match="*">
	<xsl:for-each select="md:Register" >
		<xsl:text>&nl;&tab;</xsl:text>
		<xsl:value-of select="@id"/>
		<xsl:text>,</xsl:text>
		<xsl:text>&tab;//</xsl:text>
		<xsl:value-of select="position()"/>
	</xsl:for-each>
</xsl:template>

</xsl:transform>


