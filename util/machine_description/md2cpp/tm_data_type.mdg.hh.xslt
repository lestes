<?xml version="1.0"?>
<!DOCTYPE xsl:transform [
    <!ENTITY nl "&#x0A;">
    <!ENTITY tab "&#x09;">
]>

<!--
	This file contains xslt templates that process machine description XML file and generate
	/target/machine/${CPU}/lestes/md/types/tm_data_type.mdg.hh file. The generated file
	contains enumeration of data type ids.
-->


<xsl:transform	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:md="http://lestes.jikos.cz/machinedescription">
<xsl:output method="text" encoding="ISO-8859-1"/>

<xsl:template match="/md:TargetArchitecture">
    <xsl:text>#ifndef lestes__md__types__tm_data_type_mdg_hh__included
#define lestes__md__types__tm_data_type_mdg_hh__included
/* This file is automaticaly generated from machine-description */

/*!
	\file
	\brief IDs used by some ::lestes::md::types types.
	\author jaz
*/

#include &lt;lestes/md/common.hh&gt;
#include &lt;lestes/std/objectize_macros.hh&gt;

package(lestes);
package(md);
package(types);

/*!
	\brief Ids of datatypes defined by machine description.
*/
enum dt_id_type {&nl;&tab;DT_ARRAY,&tab;//0&nl;&tab;DT_STRUCT,&tab;//1&nl;&tab;DT_UNION,&tab;//2</xsl:text>
    <xsl:apply-templates mode="tm_dt_id" select="md:DataTypes" />
	<xsl:text>
};


end_package(types);
end_package(md);

package(std);

specialize_objectize_for_enum( ::lestes::md::types::dt_id_type );

end_package(std);
end_package(lestes);

#endif&nl;</xsl:text>
</xsl:template>

<xsl:key name="formats" match="md:DataType" use="@format" />

<xsl:template mode="tm_dt_format" match="*">
	<xsl:for-each select="md:DataType[ generate-id(.)=generate-id(key('formats', @format))]">
	    <xsl:text>&nl;&tab;</xsl:text>
	    <xsl:value-of select="@format"/>
	    <xsl:if test="position() != last()">
			<xsl:text>,</xsl:text>
		</xsl:if>
	</xsl:for-each>
</xsl:template>


<xsl:template mode="tm_dt_id" match="*">
	<xsl:for-each select="md:DataType" >
		<xsl:text>&nl;&tab;</xsl:text>
		<xsl:value-of select="@id"/>
		<xsl:if test="position() != last()">
			<xsl:text>,</xsl:text>
		</xsl:if>
		<xsl:text>&tab;//</xsl:text>
		<xsl:value-of select="position()+2"/>
	</xsl:for-each>
</xsl:template>

</xsl:transform>


