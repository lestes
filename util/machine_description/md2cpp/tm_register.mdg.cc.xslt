<?xml version="1.0"?>
<!DOCTYPE xsl:transform [
    <!ENTITY nl "&#x0A;">
    <!ENTITY tab "&#x09;">
]>

<!--
	This file contains xslt templates that process machine description XML file and generate
	/target/machine/${CPU}/lestes/md/register/tm_register.mdg.cc file. The generated file
	contains an instance method that constructs instances of registers defined by target architecture.
-->


<xsl:transform	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:md="http://lestes.jikos.cz/machinedescription">
<xsl:output method="text" encoding="ISO-8859-1"/>

<xsl:template match="/md:TargetArchitecture">
    <xsl:text>/* This file is generated from machine description. */
/*!
	\file
	\brief Target-machine register
	\author jaz
*/

#include &lt;lestes/std/map.hh&gt;
#include &lt;lestes/std/set.hh&gt;
#include &lt;lestes/md/registers/tm_register.g.hh&gt;
#include &lt;lestes/md/types/tm_data_type.g.hh&gt;
#include &lt;lestes/md/instructions/pi_pi2ge_pi_template.g.hh&gt;

package(lestes);
package(md);
package(registers);

using namespace ::lestes::md::types;
using namespace ::lestes::md::instructions;

ptr&lt;tm_register&gt; tm_register::instance(tm_register_base::id_type id) {
&tab;map&lt;tm_register_base::id_type, srp&lt;tm_register&gt; &gt;::iterator existing_reg = id_to_instance_get()-&gt;find(id);&nl;
&tab;if ( existing_reg != id_to_instance_get()-&gt;end() ) {
&tab;&tab;return existing_reg-&gt;second;
&tab;}&nl;
&tab;ptr&lt;tm_register&gt; new_reg = NULL;
&tab;switch(id) {
&tab;&tab;case R_UNDEFINED: lassert(false); break;
</xsl:text>

    <xsl:apply-templates mode="tm_register_generate" select="md:Registers"/> 
	
    <xsl:text>
&tab;&tab;default: lassert(false); break;&nl;
&tab;}&nl;
&tab;lassert(new_reg);
&tab;id_to_instance_get()-&gt;insert(::std::pair&lt;tm_register_base::id_type, srp&lt;tm_register&gt; &gt;(id,new_reg));
&tab;return new_reg;
}

end_package(registers);
end_package(md);
end_package(lestes);
</xsl:text>
</xsl:template>

<xsl:template mode="tm_register_generate" match="*">
	<xsl:for-each select="md:Register">
	    <xsl:text>&tab;&tab;case </xsl:text>
	    <xsl:value-of select="@id"/>
	    <xsl:text>: {&nl;&tab;&tab;&tab;ptr&lt;set&lt;tm_data_type_base::id_type&gt; &gt; dt_list = set&lt;tm_data_type_base::id_type&gt;::create();&nl;</xsl:text>
	
	    <xsl:call-template name="tokenize_types">
			<xsl:with-param name="function">&tab;&tab;&tab;dt_list-&gt;insert</xsl:with-param>
			<xsl:with-param name="input"><xsl:value-of select="@types"/></xsl:with-param>
	    </xsl:call-template>
	    
		<xsl:text>&tab;&tab;&tab;ptr&lt;set&lt;tm_register_base::id_type&gt; &gt; reg_list = set&lt;tm_register_base::id_type&gt;::create();&nl;</xsl:text>
	    
		<xsl:text>&tab;&tab;&tab;reg_list-&gt;insert(</xsl:text><xsl:value-of select="@id"/><xsl:text>);&nl;</xsl:text>
		<xsl:if test="@aliases">
			<xsl:call-template name="tokenize_aliases">
				<xsl:with-param name="function">&tab;&tab;&tab;reg_list-&gt;insert</xsl:with-param>
				<xsl:with-param name="input"><xsl:value-of select="@aliases"/></xsl:with-param>
		    </xsl:call-template>
		</xsl:if>
		
		
		<xsl:text>&tab;&tab;&tab;ptr&lt;set&lt;ulint&gt; &gt; move_templates = set&lt;ulint&gt;::create();&nl;</xsl:text>
	    
		<xsl:if test="@move_templates">
			<xsl:call-template name="tokenize_instructions">
				<xsl:with-param name="input"><xsl:value-of select="@move_templates"/></xsl:with-param>
	    	</xsl:call-template>
		</xsl:if>
	    
		<xsl:text>&tab;&tab;&tab;new_reg = tm_register::create(</xsl:text>	
	    <xsl:value-of select="@id"/>
	    <xsl:text>, </xsl:text>
	    <xsl:value-of select="@bitwidth"/>
	    <xsl:text>, dt_list, reg_list, move_templates, lstring("</xsl:text><xsl:value-of select="@asm"/><xsl:text>"),</xsl:text>
		
		<xsl:choose>
			<xsl:when test="@flags">
				<xsl:value-of select="@flags"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>0</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	
		<xsl:text>);&nl;&tab;&tab;} break;&nl;</xsl:text>
	</xsl:for-each>
</xsl:template>


<xsl:template name="tokenize_types">
	<xsl:param name="input"/>
	<xsl:param name="function"/>
	
	<xsl:variable name="string" select="normalize-space($input)"/>
	<xsl:choose>
		<xsl:when test="contains($string,' ')">
			<xsl:variable name="id" select="substring-before($string,' ')"/>
    
			<xsl:apply-templates mode="print_type" select="//md:DataType[@id=$id]">
			    <xsl:with-param name="function"><xsl:value-of select="$function"/></xsl:with-param>
			</xsl:apply-templates>
			
			<xsl:apply-templates mode="print_type_group" select="//md:DataTypeGroup[@id=$id]">
			    <xsl:with-param name="function"><xsl:value-of select="$function"/></xsl:with-param>
			</xsl:apply-templates>
    
			<xsl:call-template name="tokenize_types">
				<xsl:with-param name="input"><xsl:value-of select="substring-after($string,' ')"/></xsl:with-param>
				<xsl:with-param name="function"><xsl:value-of select="$function"/></xsl:with-param>
			</xsl:call-template>
		</xsl:when>
		<xsl:otherwise>
			<xsl:apply-templates mode="print_type" select="//md:DataType[@id=$string]">
			    <xsl:with-param name="function"><xsl:value-of select="$function"/></xsl:with-param>
			</xsl:apply-templates>
			
			<xsl:apply-templates mode="print_type_group" select="//md:DataTypeGroup[@id=$string]">
			    <xsl:with-param name="function"><xsl:value-of select="$function"/></xsl:with-param>
			</xsl:apply-templates>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template mode="print_type" match="*">
	<xsl:param name="function"/>
	<xsl:value-of select="$function"/>
	<xsl:text>(</xsl:text>
	<xsl:value-of select="@id"/>
	<xsl:text>);&nl; </xsl:text>
</xsl:template>

<xsl:template mode="print_type_group" match="*">
	<xsl:param name="function"/>
	<xsl:call-template name="tokenize_types">
        	<xsl:with-param name="input"><xsl:value-of select="@types"/></xsl:with-param>
		<xsl:with-param name="function"><xsl:value-of select="$function"/></xsl:with-param>
      	</xsl:call-template>
</xsl:template>

<xsl:template name="tokenize_aliases">
	<xsl:param name="input"/>
	<xsl:param name="function"/>
	
	<xsl:variable name="string" select="normalize-space($input)"/>
	<xsl:choose>
		<xsl:when test="contains($string,' ')">
			<xsl:variable name="id" select="substring-before($string,' ')"/>
    
			<xsl:apply-templates mode="print_alias" select="//md:Register[@id=$id]">
			    <xsl:with-param name="function"><xsl:value-of select="$function"/></xsl:with-param>
			</xsl:apply-templates>
			
			<xsl:apply-templates mode="print_alias_group" select="//md:RegisterGroup[@id=$id]">
			    <xsl:with-param name="function"><xsl:value-of select="$function"/></xsl:with-param>
			</xsl:apply-templates>
    
			<xsl:call-template name="tokenize_aliases">
				<xsl:with-param name="input"><xsl:value-of select="substring-after($string,' ')"/></xsl:with-param>
				<xsl:with-param name="function"><xsl:value-of select="$function"/></xsl:with-param>
			</xsl:call-template>
		</xsl:when>
		<xsl:otherwise>
			<xsl:apply-templates mode="print_alias" select="//md:Register[@id=$string]">
			    <xsl:with-param name="function"><xsl:value-of select="$function"/></xsl:with-param>
			</xsl:apply-templates>
			
			<xsl:apply-templates mode="print_alias_group" select="//md:RegisterGroup[@id=$string]">
			    <xsl:with-param name="function"><xsl:value-of select="$function"/></xsl:with-param>
			</xsl:apply-templates>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template mode="print_alias" match="*">
	<xsl:param name="function"/>
	<xsl:value-of select="$function"/>
	<xsl:text>(</xsl:text>
	<xsl:value-of select="@id"/>
	<xsl:text>);&nl; </xsl:text>
</xsl:template>

<xsl:template mode="print_alias_group" match="*">
	<xsl:param name="function"/>
	<xsl:call-template name="tokenize_aliases">
        	<xsl:with-param name="input"><xsl:value-of select="@types"/></xsl:with-param>
		<xsl:with-param name="function"><xsl:value-of select="$function"/></xsl:with-param>
      	</xsl:call-template>
</xsl:template>

<xsl:template name="tokenize_instructions">
	<xsl:param name="input"/>
	
	<xsl:variable name="string" select="normalize-space($input)"/>
	<xsl:choose>
		<xsl:when test="contains($string,' ')">
			<xsl:variable name="id" select="substring-before($string,' ')"/>
    
			<xsl:call-template name="print_instruction">
				<xsl:with-param name="input"><xsl:value-of select="$id"/></xsl:with-param>
			</xsl:call-template>
			
			<xsl:call-template name="tokenize_instructions">
				<xsl:with-param name="input"><xsl:value-of select="substring-after($string,' ')"/></xsl:with-param>
			</xsl:call-template>
		</xsl:when>
		<xsl:otherwise>
			<xsl:call-template name="print_instruction">
				<xsl:with-param name="input"><xsl:value-of select="$input"/></xsl:with-param>
			</xsl:call-template>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="print_instruction">
	<xsl:param name="input"/>
	
	<xsl:choose>
		<xsl:when test="contains($input,'*')">
			<xsl:variable name="idpart" select="substring-before($input,'*')"/>
			
			<xsl:for-each select="/md:TargetArchitecture/md:Instructions/md:Instruction[starts-with(@id,$idpart)]" >
				<xsl:text>&tab;&tab;&tab;move_templates-&gt;insert(</xsl:text>
				<xsl:value-of select="@id"/>
				<xsl:text>);&nl;</xsl:text>
			</xsl:for-each>
			
		</xsl:when>
		<xsl:otherwise>
			<xsl:text>&tab;&tab;&tab;move_templates-&gt;insert(</xsl:text>
			<xsl:value-of select="$input"/>
			<xsl:text>);&nl;</xsl:text>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>


</xsl:transform>

