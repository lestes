<?xml version="1.0"?>
<!DOCTYPE xsl:transform [
    <!ENTITY nl "&#x0A;">
    <!ENTITY tab "&#x09;">
]>

<!--
	This file contains xslt templates that process machine description XML file and generate
	/target/machine/${CPU}/lestes/md/instructions/pi_pi2ge_pi_template.mdg.cc file. The generated file
	contains methods that constructs pi_pi2ge_pi_template instances used during pi_pi2ge_pi conversion.
-->

<xsl:transform	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:md="http://lestes.jikos.cz/machinedescription">
<xsl:output method="text" encoding="ISO-8859-1"/>

<xsl:template match="/md:TargetArchitecture">
    <xsl:text>/* This file is generated from machine description */
#include &lt;lestes/md/instructions/instruction_group.g.hh&gt;
#include &lt;lestes/md/instructions/pi_pi2ge_pi_template.g.hh&gt;
#include &lt;lestes/md/instructions/tm_instr.g.hh&gt;

/*! \file
	\author jaz
*/

package(lestes);
package(md);
package(instructions);

&nl;</xsl:text>

<xsl:apply-templates mode="create_function" select="md:Pi2Ge/md:Template"/> 

<xsl:text>
/*!
	\brief Returns instance of a template for the given id.
	\param id The id of a instance.
	\return The instance.
*/
ptr&lt;pi_pi2ge_pi_template&gt; pi_pi2ge_pi_template::instance(ulint id) {
&tab;map&lt;ulint,srp&lt;pi_pi2ge_pi_template&gt; &gt;::iterator existing_patt = id_to_instance_get()-&gt;find(id);&nl;
&tab;if ( existing_patt != id_to_instance_get()-&gt;end() ) {
&tab;&tab;return existing_patt-&gt;second;&nl;&tab;}&nl;
&tab;ptr&lt;pi_pi2ge_pi_template&gt; new_patt = NULL;
&tab;switch(id) {
</xsl:text>

    <xsl:apply-templates mode="call_function" select="md:Pi2Ge/md:Template"/> 
    
<xsl:text>
&tab;&tab;default: lassert(false);
&tab;}&nl;
&tab;id_to_instance_get()-&gt;insert(::std::pair&lt;ulint,srp&lt;pi_pi2ge_pi_template&gt; &gt;(id, new_patt));
&tab;return new_patt;
}

end_package(instructions);
end_package(md);
end_package(lestes);
</xsl:text>
</xsl:template>

<xsl:template mode="call_function" match="*">
	<xsl:text>
&tab;&tab;case </xsl:text><xsl:value-of select="@id"/><xsl:text>: {
&tab;&tab;&tab;new_patt = pi_pi2ge_pi_template_create_</xsl:text><xsl:value-of select="@id"/><xsl:text>();
&tab;&tab;} break;&nl;</xsl:text> 
</xsl:template>



<xsl:template mode="create_function" match="*">
	<xsl:text>
ptr&lt;pi_pi2ge_pi_template&gt; pi_pi2ge_pi_template_create_</xsl:text><xsl:value-of select="@id"/><xsl:text>() {
&tab;ptr&lt;vector&lt;srp&lt;instruction_group_base&gt; &gt; &gt; instr_patts = vector&lt;srp&lt;instruction_group_base&gt; &gt;::create();

&tab;ptr&lt;vector&lt;ulint&gt; &gt; instrs;
&tab;ptr&lt;vector&lt;ulint&gt; &gt; in_pids;
&tab;ptr&lt;vector&lt;ulint&gt; &gt; out_pids;
&tab;ptr&lt;vector&lt;ulint&gt; &gt; in_refs;
&tab;ptr&lt;vector&lt;ulint&gt; &gt; out_refs;
	</xsl:text>
	
	<xsl:apply-templates mode="process_instruction" select="md:Instr" />
	
	<xsl:text>
&tab;return pi_pi2ge_pi_template::create(</xsl:text>
	<xsl:value-of select="@id"/>
	<xsl:text>,instr_patts);&nl;}&nl;</xsl:text>
	
</xsl:template>


<xsl:template mode="process_instruction" match="*">

<xsl:text>
&tab;instrs = vector&lt;ulint&gt;::create();
&tab;in_pids = vector&lt;ulint&gt;::create();
&tab;out_pids = vector&lt;ulint&gt;::create();
&tab;in_refs = vector&lt;ulint&gt;::create();
&tab;out_refs = vector&lt;ulint&gt;::create();

</xsl:text>
	
	<xsl:call-template name="tokenize_instructions">
		  <xsl:with-param name="input"><xsl:value-of select="@ref_instr"/></xsl:with-param>
	</xsl:call-template>

<xsl:text>
&tab;//input operands
</xsl:text>
	
	<xsl:apply-templates mode="process_operands" select="md:In" >
	    <xsl:with-param name="function">in</xsl:with-param>
	</xsl:apply-templates>

<xsl:text>
&tab;//output operands
</xsl:text>
	
	<xsl:apply-templates mode="process_operands" select="md:Out" >
	    <xsl:with-param name="function">out</xsl:with-param>
	</xsl:apply-templates>

<xsl:text>
&tab;instr_patts->push_back(instruction_group::create(instrs,in_pids,out_pids,in_refs,out_refs));&nl;
</xsl:text>

	
</xsl:template>


<xsl:template mode="process_operands" match="*">
	<xsl:param name="function"/>
	
	<xsl:for-each select="*" >
		<xsl:text>&tab;</xsl:text>	
		<xsl:value-of select="$function"/>
		<xsl:text>_pids-&gt;push_back(</xsl:text>
		<xsl:value-of select="@pid"/>
		<xsl:text>);&nl;</xsl:text>	
		
		<xsl:text>&tab;</xsl:text>	
		<xsl:value-of select="$function"/>
		<xsl:text>_refs-&gt;push_back(</xsl:text>
		<xsl:value-of select="@ref_op"/>
		<xsl:text>);&nl;</xsl:text>	
	</xsl:for-each>
</xsl:template>

<xsl:template name="tokenize_instructions">
	<xsl:param name="input"/>
	
	<xsl:variable name="string" select="normalize-space($input)"/>
	<xsl:choose>
		<xsl:when test="contains($string,' ')">
			<xsl:variable name="id" select="substring-before($string,' ')"/>
    
			<xsl:call-template name="print_instruction">
				<xsl:with-param name="input"><xsl:value-of select="$id"/></xsl:with-param>
			</xsl:call-template>
			
			<xsl:call-template name="tokenize_instructions">
				<xsl:with-param name="input"><xsl:value-of select="substring-after($string,' ')"/></xsl:with-param>
			</xsl:call-template>
		</xsl:when>
		<xsl:otherwise>
			<xsl:call-template name="print_instruction">
				<xsl:with-param name="input"><xsl:value-of select="$input"/></xsl:with-param>
			</xsl:call-template>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="print_instruction">
	<xsl:param name="input"/>
	
	<xsl:choose>
		<xsl:when test="contains($input,'*')">
			<xsl:variable name="idpart" select="substring-before($input,'*')"/>
			
			<xsl:for-each select="/md:TargetArchitecture/md:Instructions/md:Instruction[starts-with(@id,$idpart)]" >
				<xsl:text>&tab;instrs-&gt;push_back(</xsl:text>
				<xsl:value-of select="@id"/>
				<xsl:text>);&nl;</xsl:text>
			</xsl:for-each>
			
		</xsl:when>
		<xsl:otherwise>
			<xsl:text>&tab;instrs-&gt;push_back(</xsl:text>
			<xsl:value-of select="$input"/>
			<xsl:text>);&nl;</xsl:text>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>



</xsl:transform>



