<?xml version="1.0"?>
<!DOCTYPE xsl:transform [
    <!ENTITY nl "&#x0A;">
    <!ENTITY tab "&#x09;">
]>

<!--
	This file contains xslt templates that process machine description XML file and generate
	/target/machine/${CPU}/lestes/md/types/ss_type2tm_type.mdg.cc file. The generated file
	contains visitor methods that converts ss-level intercode datatype to its tm-level
	equivalent.
-->


<xsl:transform	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:md="http://lestes.jikos.cz/machinedescription">
<xsl:output method="text" encoding="ISO-8859-1"/>

<xsl:template match="/md:TargetArchitecture">
    <xsl:text>/* This file is automaticaly generated from machine-description */

/*!
	\file 
	\brief ss_type to tm_data type convertor.
	\author jaz
*/
    
#include &lt;lestes/md/types/ss_type2tm_type.g.hh&gt;
#include &lt;lestes/md/types/tm_data_type.g.hh&gt;
#include &lt;lestes/lang/cplus/sem/ss_type_builtin.g.hh&gt;


package(lestes);
package(md);
package(types);

using namespace ::lestes::md::types;
using namespace ::lestes::lang::cplus::sem;

    </xsl:text>
	
	<xsl:for-each select="md:DataTypes/md:DataType">
		<xsl:if test="@mapped_types">
		    <xsl:call-template name="tokenize_types">
				<xsl:with-param name="types"><xsl:value-of select="@mapped_types"/></xsl:with-param>
				<xsl:with-param name="tm_type"><xsl:value-of select="@id"/></xsl:with-param>
			</xsl:call-template> 
		</xsl:if>
	</xsl:for-each>
	
<xsl:text>

end_package(types);
end_package(md);
end_package(lestes);

</xsl:text>
</xsl:template>

<xsl:template name="ss_type_to_tm_type_mapper_generate" >
	<xsl:param name="type"/>
	<xsl:param name="tm_type"/>
	
    <xsl:text>
ptr&lt;tm_data_type_base&gt; ss_type2tm_type::visit_</xsl:text>
    <xsl:value-of select="$type"/>
    <xsl:text>( ptr&lt;</xsl:text>
    <xsl:value-of select="$type"/>
    <xsl:text>&gt;) {&nl;&tab;return tm_dt_simple::instance(</xsl:text>
    <xsl:value-of select="$tm_type"/>
    <xsl:text>);
}
    </xsl:text>
</xsl:template>


<xsl:template name="tokenize_types">
	<xsl:param name="types"/>
	<xsl:param name="tm_type"/>
	
	<xsl:variable name="string" select="normalize-space($types)"/>
	<xsl:choose>
		<xsl:when test="contains($string,' ')">
			<xsl:variable name="type" select="substring-before($string,' ')"/>
    		
			<xsl:call-template name="ss_type_to_tm_type_mapper_generate"> 
    			<xsl:with-param name="type"><xsl:value-of select="$type"/></xsl:with-param>
				<xsl:with-param name="tm_type"><xsl:value-of select="$tm_type"/></xsl:with-param>
			</xsl:call-template>
			
			<xsl:call-template name="tokenize_types">
				<xsl:with-param name="types"><xsl:value-of select="substring-after($string,' ')"/></xsl:with-param>
				<xsl:with-param name="tm_type"><xsl:value-of select="$tm_type"/></xsl:with-param>
			</xsl:call-template>
		</xsl:when>
		<xsl:otherwise>
			<xsl:call-template name="ss_type_to_tm_type_mapper_generate"> 
    			<xsl:with-param name="type"><xsl:value-of select="$types"/></xsl:with-param>
				<xsl:with-param name="tm_type"><xsl:value-of select="$tm_type"/></xsl:with-param>
			</xsl:call-template>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>



</xsl:transform>






