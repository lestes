<?xml version="1.0"?>
<!DOCTYPE xsl:transform [
    <!ENTITY nl "&#x0A;">
    <!ENTITY tab "&#x09;">
]>

<!--
	This file contains xslt templates that process machine description XML file and generate
	/target/machine/${CPU}/lestes/md/instructions/pi_pi2ge_pi_templace.mdg.hh file. The generated file
	contains enumeration of template ids.
-->


<xsl:transform	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:md="http://lestes.jikos.cz/machinedescription">
<xsl:output method="text" encoding="ISO-8859-1"/>

<xsl:template match="/md:TargetArchitecture">
    <xsl:text>#ifndef lestes__md__instructions__pi_pi2ge_pi_template_mdg_hh__included
#define lestes__md__instructions__pi_pi2ge_pi_template_mdg_hh__included
/* This file is automaticaly generated from machine-description */

/*!
	\file
	\author jaz
*/

#include &lt;lestes/md/common.hh&gt;
#include &lt;lestes/std/objectize_macros.hh&gt;

package(lestes);
package(md);
package(instructions);

/*!
	\brief Ids of pi_pi2ge_pi_templates
*/
enum pi_pi2ge_template_id_type {</xsl:text>
    <xsl:apply-templates mode="pi_pi2ge_pi_template_id" select="md:Pi2Ge" />
    <xsl:text>
};



end_package(instructions);
end_package(md);

package(std);

specialize_objectize_for_enum( ::lestes::md::instructions::pi_pi2ge_template_id_type );

end_package(std);
end_package(lestes);

#endif&nl;</xsl:text>
</xsl:template>




<xsl:template mode="pi_pi2ge_pi_template_id" match="*">
	<xsl:for-each select="md:Template" >
		<xsl:text>&nl;&tab;</xsl:text>
		<xsl:value-of select="@id"/>
		<xsl:if test="position() != last()">
			<xsl:text>,</xsl:text>
		</xsl:if>
		<xsl:text>&tab;//</xsl:text>
		<xsl:value-of select="position()-1"/>
	</xsl:for-each>
</xsl:template>

</xsl:transform>


