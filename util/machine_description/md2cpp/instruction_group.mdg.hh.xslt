<?xml version="1.0"?>
<!DOCTYPE xsl:transform [
    <!ENTITY nl "&#x0A;">
    <!ENTITY tab "&#x09;">
]>

<!--
	This file contains xslt templates that process machine description XML file and generate
	/target/machine/${CPU}/lestes/md/instructions/instruction_group.mdg.hh file.
-->

<xsl:transform	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:md="http://lestes.jikos.cz/machinedescription">
<xsl:output method="text" encoding="ISO-8859-1"/>

<xsl:template match="/md:TargetArchitecture">
    <xsl:text>#ifndef lestes__md__instructions__instruction_group_mdg_hh__included
#define lestes__md__instructions__instruction_group_mdg_hh__included
/* This file is automaticaly generated from machine-description */

/*!
	\file
	\author jaz
*/

#include &lt;lestes/md/common.hh&gt;
#include &lt;lestes/std/objectize_macros.hh&gt;

package(lestes);
package(md);
package(instructions);

/*!
	\brief Operand ids within instruction templates.
	
	PIN_X id represents input operands that template receives from source pi-level pseudoinstruction.
	POUT_X id represents output operand that template receives from source pi-level pseudoinstruction.
	The rest of ids are operands defined and passed among ge-level pseudoinstructions within the template itself.
*/
enum instruction_template_operand_pid_type {
	PIN_1,
	PIN_2,
	PIN_3,
	PIN_4,
	POUT_1,
	POUT_2,
	POUT_3,
	POUT_4,</xsl:text>
    <xsl:apply-templates mode="it_op_id" select="md:Pi2Ge/md:Template/md:Instr/*" />
	<xsl:text>
	IROPT_TERMINATOR
};


end_package(instructions);
end_package(md);

package(std);

specialize_objectize_for_enum( ::lestes::md::instructions::instruction_template_operand_pid_type );

end_package(std);
end_package(lestes);

#endif&nl;</xsl:text>
</xsl:template>


<xsl:key name="operands" match="md:Pi2Ge/md:Template/md:Instr/*/md:Op" use="@pid" />

<xsl:template mode="it_op_id" match="*">
	<xsl:for-each select="md:Op[not(starts-with(@pid,'PIN_') or starts-with(@pid,'POUT_')) and generate-id(.)=generate-id(key('operands', @pid))]">
	    <xsl:text>&nl;&tab;</xsl:text>
	    <xsl:value-of select="@pid"/>
		<xsl:text>,</xsl:text>
	</xsl:for-each>
</xsl:template>


</xsl:transform>


