<?xml version="1.0"?>
<!DOCTYPE xsl:transform [
    <!ENTITY nl "&#x0A;">
    <!ENTITY tab "&#x09;">
]>

<!--
	This file contains xslt templates that process machine description XML file and generate
	/target/machine/${CPU}/lestes/md/instructions/tm_instr.mdg.cc file. The generated file
	contains instance method that constructs instances of instructions defined by target architecture.
-->


<xsl:transform	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:md="http://lestes.jikos.cz/machinedescription">
<xsl:output method="text" encoding="ISO-8859-1"/>

<xsl:template match="/md:TargetArchitecture">
    <xsl:text>/* This file is generated from machine description */
#include &lt;lestes/std/set.hh&gt;
#include &lt;lestes/md/types/tm_data_type.g.hh&gt;
#include &lt;lestes/md/registers/tm_register.g.hh&gt;
#include &lt;lestes/md/instructions/tm_instr.g.hh&gt;
#include &lt;lestes/md/instructions/execution_info.g.hh&gt;

/*! \file
	\brief Target-machine instruction.
	\author jaz
*/

package(lestes);
package(md);
package(instructions);

using namespace ::lestes::md::types;
using namespace ::lestes::md::registers;

&nl;</xsl:text>

<xsl:apply-templates mode="create_function" select="md:Instructions/md:Instruction"/> 

<xsl:text>
ptr&lt;tm_instr&gt; tm_instr::instance(ulint id) {
&tab;map&lt;ulint,srp&lt;tm_instr&gt; &gt;::iterator existing_instr = id_to_instance_get()-&gt;find(id);&nl;
&tab;if ( existing_instr != id_to_instance_get()-&gt;end() ) {
&tab;&tab;return existing_instr-&gt;second;&nl;&tab;}&nl;
&tab;ptr&lt;tm_instr&gt; new_instr = NULL;
&tab;switch(id) {
</xsl:text>

    <xsl:apply-templates mode="call_function" select="md:Instructions/md:Instruction"/> 
    
<xsl:text>
&tab;&tab;default: lassert(false);
&tab;}&nl;
&tab;lassert(new_instr);
&tab;id_to_instance_get()-&gt;insert(::std::pair&lt;ulint,srp&lt;tm_instr&gt; &gt;(id, new_instr));
&tab;return new_instr;
}

end_package(instructions);
end_package(md);
end_package(lestes);
</xsl:text>
</xsl:template>

<xsl:template mode="call_function" match="*">
	<xsl:text>
&tab;&tab;case </xsl:text><xsl:value-of select="@id"/><xsl:text>: {
&tab;&tab;&tab;new_instr = tm_instr_create_</xsl:text><xsl:value-of select="@id"/><xsl:text>();
&tab;&tab;} break;&nl;</xsl:text> 
</xsl:template>



<xsl:template mode="create_function" match="*">
	<xsl:text>
ptr&lt;tm_instr&gt; tm_instr_create_</xsl:text><xsl:value-of select="@id"/><xsl:text>() {
&tab;ptr&lt;vector&lt;srp&lt;tm_instr_op_base&gt; &gt; &gt; in_operands = vector&lt;srp&lt;tm_instr_op_base&gt; &gt;::create();
&tab;ptr&lt;vector&lt;srp&lt;tm_instr_op_base&gt; &gt; &gt; out_operands = vector&lt;srp&lt;tm_instr_op_base&gt; &gt;::create();
&tab;ptr&lt;set&lt;tm_register_base::id_type&gt; &gt; req_regs = set&lt;tm_register_base::id_type&gt;::create();
&tab;ptr&lt;map&lt;ulint, lstring&gt; &gt; props = map&lt;ulint, lstring&gt;::create();
&tab;ptr&lt;set&lt;tm_data_type_base::id_type&gt; &gt; allowed_types; 
&tab;ptr&lt;execution_info&gt; einfo;
&tab;ptr&lt;set&lt;ulint&gt; &gt; versions;
	</xsl:text>
	
	<xsl:apply-templates mode="process_instruction" select="." />
	
	<xsl:text>
&nl;&tab;//return target pseudoinstruction
&tab;return tm_instr::create(</xsl:text>
	<xsl:value-of select="@id"/>
	<xsl:text>,in_operands,out_operands,lstring(</xsl:text>
	<xsl:apply-templates mode="asm_output" select="." />
	<xsl:text>), props,versions,</xsl:text>
	
	<xsl:choose>
		<xsl:when test="@flags">
			<xsl:value-of select="@flags"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text>0</xsl:text>
		</xsl:otherwise>
	</xsl:choose>
	
	<xsl:text>,einfo);
}
</xsl:text>
	
</xsl:template>

<xsl:template mode="process_instruction" match="*">


<xsl:text>
&tab;//input operands
</xsl:text>
	
	<xsl:apply-templates mode="process_operands" select="md:In" >
		<xsl:with-param name="instr_id"><xsl:value-of select="@id"/></xsl:with-param>
	    <xsl:with-param name="function">in</xsl:with-param>
	</xsl:apply-templates>

<xsl:text>
&tab;//output operands
</xsl:text>
	
	<xsl:apply-templates mode="process_operands" select="md:Out" >
		<xsl:with-param name="instr_id"><xsl:value-of select="@id"/></xsl:with-param>
	    <xsl:with-param name="function">out</xsl:with-param>
	</xsl:apply-templates>


<xsl:text>
&tab;//execution info
</xsl:text>
	
	<xsl:apply-templates mode="execution_info" select="md:Exec" >
	</xsl:apply-templates>

<xsl:text>
&tab;//properties
</xsl:text>
	
	<xsl:for-each select="md:Props/md:P" >
		<xsl:text>&tab;props->insert(::std::pair&lt;ulint, lstring&gt;(</xsl:text><xsl:value-of select="@key"/><xsl:text>,"</xsl:text>
		<xsl:value-of select="@value"/> 
		<xsl:text>"));&nl;</xsl:text>
	</xsl:for-each>

<xsl:text>
&tab;//versions
</xsl:text>

	<xsl:variable name="idpart" select="substring-before(@id,'__')"/>
	<xsl:variable name="id" select="@id"/>
			
	<xsl:for-each select="/md:TargetArchitecture/md:Instructions/md:Instruction[@id != $id and starts-with(@id,$idpart)]" >
		<xsl:if test="position() = 1">
			<xsl:text>&tab;versions = set&lt;ulint&gt;::create();&nl;</xsl:text>
    	</xsl:if>	
		<xsl:text>&tab;versions-&gt;insert(</xsl:text>
		<xsl:value-of select="@id"/>
		<xsl:text>);&nl;</xsl:text>
	</xsl:for-each>
			
</xsl:template>

			


<xsl:template mode="process_operands" match="*">
	<xsl:param name="instr_id"/>
	<xsl:param name="function"/>
	<xsl:for-each select="*" >
		 	<xsl:text>&tab;ptr&lt;set&lt;tm_data_type_base::id_type&gt; &gt; dt_list_</xsl:text>
			<xsl:value-of select="@id"/>
			<xsl:text> = set&lt;tm_data_type_base::id_type&gt;::create();&nl;</xsl:text>
		
			<xsl:variable name="id" select="@id"/>
			
			
			
			<xsl:call-template name="tokenize_types">
					<xsl:with-param name="input"><xsl:value-of select="@types"/></xsl:with-param>
					<xsl:with-param name="function"><xsl:text>&tab;dt_list_</xsl:text><xsl:value-of select="@id"/><xsl:text>-&gt;insert</xsl:text></xsl:with-param>
			</xsl:call-template>
		
			<xsl:choose>
			    <xsl:when test="name()='Reg'">
					<xsl:text>&tab;ptr&lt;set&lt;tm_register_base::id_type&gt; &gt; reg_list_</xsl:text>
					<xsl:value-of select="@id"/>
					<xsl:text> = set&lt;tm_register_base::id_type&gt;::create();&nl;</xsl:text>
			
					<xsl:call-template name="tokenize_registers">
				    	<xsl:with-param name="input"><xsl:value-of select="@registers"/></xsl:with-param>
			    		<xsl:with-param name="function"><xsl:text>&tab;reg_list_</xsl:text><xsl:value-of select="@id"/></xsl:with-param>
					</xsl:call-template>
			
					<xsl:text>&tab;ptr&lt;tm_instr_op_reg&gt; op_</xsl:text>
					<xsl:value-of select="@id"/>
					<xsl:text> = </xsl:text>
					<xsl:text>tm_instr_op_reg::create(</xsl:text>
					<xsl:value-of select="@id"/>
					<xsl:text>, dt_list_</xsl:text>
					<xsl:value-of select="@id"/>
					<xsl:text>, reg_list_</xsl:text>
					<xsl:value-of select="@id"/>
					<xsl:text>);&nl;</xsl:text>
					
			    </xsl:when>
			    <xsl:when test="name()='Mem'">
					<xsl:text>&tab;ptr&lt;tm_instr_op_mem&gt; op_</xsl:text>
					<xsl:value-of select="@id"/>
					<xsl:text> = </xsl:text>
					<xsl:text>tm_instr_op_mem::create(</xsl:text>
					<xsl:value-of select="@id"/>
					<xsl:text>, dt_list_</xsl:text>
					<xsl:value-of select="@id"/>
					<xsl:text>);&nl;</xsl:text>
			    </xsl:when>
			    <xsl:otherwise>
					<xsl:text>&tab;ptr&lt;tm_instr_op_imm&gt; op_</xsl:text>
					<xsl:value-of select="@id"/>
					<xsl:text> = </xsl:text>
					<xsl:text>tm_instr_op_imm::create(</xsl:text>
					<xsl:value-of select="@id"/>
					<xsl:text>, dt_list_</xsl:text>
					<xsl:value-of select="@id"/>
					<xsl:text>);&nl;</xsl:text>
			    </xsl:otherwise>
			</xsl:choose>
			
			<xsl:for-each select="/md:TargetArchitecture/md:Instructions/md:Instruction[ @id=$instr_id ]/md:Out/*[ @destroys=$id ]" >
				<xsl:text>&tab;op_</xsl:text><xsl:value-of select="$id"/><xsl:text>-&gt;destroyed_by_get()-&gt;push_back(</xsl:text>
				<xsl:value-of select="@id"/> 
				<xsl:text>);&nl;</xsl:text>
			</xsl:for-each>
			
			
			<xsl:if test="@destroys">
				<xsl:text>&tab;op_</xsl:text><xsl:value-of select="$id"/><xsl:text>-&gt;destroys_set(</xsl:text>
				<xsl:value-of select="@destroys"/> 
				<xsl:text>);&nl;</xsl:text>
			</xsl:if>
			
			<xsl:text>&tab;</xsl:text>
			<xsl:value-of select="$function"/>
			<xsl:text>_operands-&gt;push_back(op_</xsl:text>
			<xsl:value-of select="@id"/>
			<xsl:text>);&nl;</xsl:text>
			
	</xsl:for-each>
</xsl:template>



<xsl:template mode="process_regs" match="*">
	<xsl:param name="function"/>
		
	<xsl:call-template name="tokenize_registers">
			<xsl:with-param name="input"><xsl:value-of select="@ids"/></xsl:with-param>
			<xsl:with-param name="function"><xsl:value-of select="$function"/></xsl:with-param>
	</xsl:call-template>
		
</xsl:template>



<xsl:template name="tokenize_types">
	<xsl:param name="input"/>
	<xsl:param name="function"/>
	
	<xsl:variable name="string" select="normalize-space($input)"/>
	<xsl:choose>
		<xsl:when test="contains($string,' ')">
			<xsl:variable name="id" select="substring-before($string,' ')"/>
    
			<xsl:apply-templates mode="print_type" select="//md:DataType[@id=$id]">
			    <xsl:with-param name="function"><xsl:value-of select="$function"/></xsl:with-param>
			</xsl:apply-templates>
			
			<xsl:apply-templates mode="print_type_group" select="//md:DataTypeGroup[@id=$id]">
			    <xsl:with-param name="function"><xsl:value-of select="$function"/></xsl:with-param>
			</xsl:apply-templates>
    
			<xsl:call-template name="tokenize_types">
				<xsl:with-param name="input"><xsl:value-of select="substring-after($string,' ')"/></xsl:with-param>
				<xsl:with-param name="function"><xsl:value-of select="$function"/></xsl:with-param>
			</xsl:call-template>
		</xsl:when>
		<xsl:otherwise>
			<xsl:apply-templates mode="print_type" select="//md:DataType[@id=$string]">
			    <xsl:with-param name="function"><xsl:value-of select="$function"/></xsl:with-param>
			</xsl:apply-templates>
			
			<xsl:apply-templates mode="print_type_group" select="//md:DataTypeGroup[@id=$string]">
			    <xsl:with-param name="function"><xsl:value-of select="$function"/></xsl:with-param>
			</xsl:apply-templates>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>



<xsl:template mode="print_type" match="*">
	<xsl:param name="function"/>
	<xsl:value-of select="$function"/>
	<xsl:text>(</xsl:text>
	<xsl:value-of select="@id"/>
	<xsl:text>);&nl; </xsl:text>
</xsl:template>

<xsl:template mode="print_type_group" match="*">
	<xsl:param name="function"/>
	<xsl:call-template name="tokenize_types">
       	<xsl:with-param name="input"><xsl:value-of select="@types"/></xsl:with-param>
		<xsl:with-param name="function"><xsl:value-of select="$function"/></xsl:with-param>
    </xsl:call-template>
</xsl:template>


<xsl:template name="tokenize_registers">
	<xsl:param name="input"/>
	<xsl:param name="function"/>
	
	<xsl:variable name="string" select="normalize-space($input)"/>
	<xsl:choose>
		<xsl:when test="contains($string,' ')">
			<xsl:variable name="id" select="substring-before($string,' ')"/>
			
			<xsl:choose>
				<xsl:when test="contains(@id,'-')">
					<xsl:value-of select="$function"/>
					<xsl:text>-&gt;erase</xsl:text>
					<xsl:text>(</xsl:text>
					<xsl:value-of select="substring-after(@id,'-')"/>
					<xsl:text>);&nl; </xsl:text>
				</xsl:when>
				
				<xsl:otherwise>
				
					<xsl:apply-templates mode="print_register" select="//md:Register[@id=$id]">
			    		<xsl:with-param name="function"><xsl:value-of select="$function"/></xsl:with-param>
					</xsl:apply-templates>
			
					<xsl:apply-templates mode="print_register_group" select="//md:RegisterGroup[@id=$id]">
					    <xsl:with-param name="function"><xsl:value-of select="$function"/></xsl:with-param>
					</xsl:apply-templates>
    
				</xsl:otherwise>
			</xsl:choose>
			
			<xsl:call-template name="tokenize_registers">
				<xsl:with-param name="input"><xsl:value-of select="substring-after($string,' ')"/></xsl:with-param>
				<xsl:with-param name="function"><xsl:value-of select="$function"/></xsl:with-param>
			</xsl:call-template>

		</xsl:when>
		<xsl:otherwise>
			<xsl:choose>
				<xsl:when test="contains($string,'-')">
					<xsl:value-of select="$function"/>
					<xsl:text>-&gt;erase</xsl:text>
					<xsl:text>(</xsl:text>
					<xsl:value-of select="substring-after($string,'-')"/>
					<xsl:text>);&nl; </xsl:text>
				</xsl:when>
				
				<xsl:otherwise>
					<xsl:apply-templates mode="print_register" select="//md:Register[@id=$string]">
					    <xsl:with-param name="function"><xsl:value-of select="$function"/></xsl:with-param>
					</xsl:apply-templates>
			
					<xsl:apply-templates mode="print_register_group" select="//md:RegisterGroup[@id=$string]">
					    <xsl:with-param name="function"><xsl:value-of select="$function"/></xsl:with-param>
					</xsl:apply-templates>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template mode="print_register" match="*">
	<xsl:param name="function"/>
	
	<xsl:value-of select="$function"/>
	<xsl:text>-&gt;insert</xsl:text>
	<xsl:text>(</xsl:text>
	<xsl:value-of select="@id"/>
	<xsl:text>);&nl; </xsl:text>
</xsl:template>

<xsl:template mode="print_register_group" match="*">
	<xsl:param name="function"/>
	<xsl:call-template name="tokenize_registers">
        <xsl:with-param name="input"><xsl:value-of select="@registers"/></xsl:with-param>
		<xsl:with-param name="function"><xsl:value-of select="$function"/></xsl:with-param>
    </xsl:call-template>
</xsl:template>

<xsl:template mode="asm_output" match="*">
	<xsl:for-each select="md:Asm" >
		<xsl:if test="position() != 1">
			<xsl:text> + tm_asm::ent_instruction_delimiter_get() + </xsl:text>
    	</xsl:if>	
		<xsl:text>"</xsl:text>
		<xsl:value-of select="."/>
		<xsl:text>"</xsl:text>
		
	</xsl:for-each>
</xsl:template>

<xsl:template mode="execution_info" match="*">
		<xsl:text>&tab;einfo = execution_info::create(</xsl:text>
		<xsl:value-of select="@etime"/>
		<xsl:text>);&nl;</xsl:text>
</xsl:template>

</xsl:transform>



