<?xml version="1.0"?>
<!DOCTYPE xsl:transform [
    <!ENTITY nl "&#x0A;">
    <!ENTITY tab "&#x09;">
]>

<!--
	This file contains xslt templates that process machine description XML file and generate
	/target/machine/${CPU}/lestes/md/instructions/pi_pi2ge_pi.mdg.cc file. The generated file
	contains visitor methods that converts pi-level intercode class instance to its ge-level
	equivalent.
-->

<xsl:transform	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:md="http://lestes.jikos.cz/machinedescription">
<xsl:output method="text" encoding="ISO-8859-1"/>

<xsl:template match="/md:TargetArchitecture">
    <xsl:text>/* This file is generated from machine description */
#include &lt;lestes/md/instructions/instruction_group.g.hh&gt;
#include &lt;lestes/md/instructions/pi_pi2ge_pi_template.g.hh&gt;
#include &lt;lestes/md/instructions/pi_pi2ge_pi.g.hh&gt;

/*! \file
	\author jaz
*/

package(lestes);
package(md);
package(instructions);

</xsl:text>

<xsl:apply-templates mode="unique_visitors" select="md:Pi2Ge"/> 

<xsl:text>

end_package(instructions);
end_package(md);
end_package(lestes);
</xsl:text>
</xsl:template>


<xsl:key name="templates" match="md:Pi2Ge/md:Template" use="@ref_pi" />

<xsl:template mode="unique_visitors" match="*">
	<xsl:for-each select="md:Template[ generate-id(.)=generate-id(key('templates', @ref_pi))]">
		<xsl:call-template name="visitor">
			<xsl:with-param name="pi"><xsl:value-of select="@ref_pi"/></xsl:with-param>
		</xsl:call-template>
	</xsl:for-each>
</xsl:template>

<xsl:template name="visitor">
	<xsl:param name="pi" />
	<xsl:text>ptr&lt; vector&lt; srp&lt; ::lestes::backend_v2::intercode::ge_pi&gt; &gt; &gt;  pi_pi2ge_pi::visit_</xsl:text><xsl:value-of select="@ref_pi"/><xsl:text>(ptr&lt; ::lestes::backend_v2::intercode::</xsl:text><xsl:value-of select="@ref_pi"/><xsl:text> &gt; pi) {
&tab;ptr&lt; vector&lt; srp&lt; ::lestes::backend_v2::intercode::ge_pi&gt; &gt; &gt; out;
&tab;ptr&lt; pi_pi2ge_pi_template &gt; pat;&nl;</xsl:text>
	
	<xsl:for-each select="/md:TargetArchitecture/md:Pi2Ge/md:Template[ @ref_pi=$pi ]">
	<xsl:text>
&tab;pat = pi_pi2ge_pi_template::instance(</xsl:text><xsl:value-of select="@id"/><xsl:text>);
&tab;out = convert(pi,pat);
	 
&tab;if ( out ) {
&tab;&tab;return out;
&tab;}&nl;</xsl:text>
	</xsl:for-each>	
	
	<xsl:text>
&tab;lassert(false);
}&nl;&nl;</xsl:text>
	
</xsl:template>




</xsl:transform>



