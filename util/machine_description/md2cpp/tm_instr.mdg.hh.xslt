<?xml version="1.0"?>
<!DOCTYPE xsl:transform [
    <!ENTITY nl "&#x0A;">
    <!ENTITY tab "&#x09;">
]>

<!--
	This file contains xslt templates that process machine description XML file and generate
	/target/machine/${CPU}/lestes/md/instructions/tm_instr.mdg.hh file. The generated file
	contains enumeration of instruction ids.
-->


<xsl:transform	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:md="http://lestes.jikos.cz/machinedescription">
<xsl:output method="text" encoding="ISO-8859-1"/>

<xsl:template match="/md:TargetArchitecture">
    <xsl:text>#ifndef lestes__md__instructions__tm_instr_mdg_hh__included
#define lestes__md__instructions__tm_instr_mdg_hh__included
/* This file is automaticaly generated from machine-description */

/*!
	\file
	\brief IDs used by ::lestes::md::instructions::tm_instr class.
	\author jaz
*/

#include &lt;lestes/md/common.hh&gt;
#include &lt;lestes/std/objectize_macros.hh&gt;

package(lestes);
package(md);
package(instructions);

/*!
	\brief Ids of target machine instructions.
*/
enum tm_instr_id_type {
	NO_INSTRUCTION_ID,</xsl:text>
    <xsl:apply-templates mode="tm_instr_id" select="md:Instructions" />
    <xsl:text>
	TIIT_TERMINATOR	
};


/*!
	\brief Id of instruction operand.
	
	Operand is referenced by its id within instruction group.
*/
enum tm_instr_op_id_type {
	NO_OPERAND_ID,
	I_1,
	I_2,
	I_3,
	I_4,
	I_5,
	I_6,
	I_7,
	I_8,
	I_9,
	O_1,
	O_2,
	O_3,
	O_4,
	O_5,
	O_6,
	O_7,
	O_8,
	O_9,
	TIOIT_TERMINATOR
};

/*!
	\brief Ids of user defined properties.
*/
enum tm_instr_property_key_type {</xsl:text>
	NO_PROPERTY_KEY,<xsl:apply-templates mode="tm_instr_property_key" select="md:Instructions/md:Instruction/md:Props" />
	<xsl:text>
	TIPKT_TERMINATOR
};


end_package(instructions);
end_package(md);

package(std);

specialize_objectize_for_enum( ::lestes::md::instructions::tm_instr_op_id_type );
specialize_objectize_for_enum( ::lestes::md::instructions::tm_instr_id_type );
specialize_objectize_for_enum( ::lestes::md::instructions::tm_instr_property_key_type );

end_package(std);
end_package(lestes);

#endif&nl;</xsl:text>
</xsl:template>


<xsl:template mode="tm_instr_id" match="*">
	<xsl:for-each select="md:Instruction" >
		<xsl:text>&nl;&tab;</xsl:text>
		<xsl:value-of select="@id"/>
		<xsl:text>,</xsl:text>
		<xsl:text>&tab;//</xsl:text>
		<xsl:value-of select="position()"/>
	</xsl:for-each>
</xsl:template>

<xsl:key name="operands" match="md:Instructions/md:Instruction/*/*" use="@id" />

<xsl:template mode="tm_instr_op_id" match="*">
	<xsl:for-each select="*[ generate-id(.)=generate-id(key('operands', @id))]">
	    <xsl:text>&nl;&tab;</xsl:text>
	    <xsl:value-of select="@id"/>
		<xsl:text>,</xsl:text>
	</xsl:for-each>
</xsl:template>

<xsl:key name="props" match="md:Instructions/md:Instruction/md:Props/md:P" use="@key" />

<xsl:template mode="tm_instr_property_key" match="*">
	<xsl:for-each select="md:P[ generate-id(.)=generate-id(key('props', @key))]">
	    <xsl:text>&nl;&tab;</xsl:text>
	    <xsl:value-of select="@key"/>
		<xsl:text>,</xsl:text>
	</xsl:for-each>
</xsl:template>



</xsl:transform>


