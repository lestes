<?xml version="1.0"?>
<!DOCTYPE xsl:transform [
    <!ENTITY nl "&#x0A;">
    <!ENTITY tab "&#x09;">
]>

<!--
	This file contains xslt templates that process machine description XML file and generate
	/target/machine/${CPU}/lestes/md/tasm/tm_asm.mdg.cc file. The generated file
	contains getter methods that provides access to asm language elements.
-->


<xsl:transform	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:md="http://lestes.jikos.cz/machinedescription">
<xsl:output method="text" encoding="ISO-8859-1"/>

<xsl:template match="/md:TargetArchitecture">
    <xsl:text>/* This file is generated from machine description */
#include &lt;lestes/md/tasm/tm_asm.mdg.hh&gt;

/*!
	\file
	\brief Target-machine asm description.
	\author jaz
*/

package(lestes);
package(md);
package(tasm);

</xsl:text>

<xsl:apply-templates mode="asm_impl_function" select="md:Assembler/md:Entities/md:Entity"/> 

<xsl:text>

end_package(tasm);
end_package(md);
end_package(lestes);
</xsl:text>
</xsl:template>

<xsl:template mode="asm_impl_function" match="*">
	<xsl:text>lstring tm_asm::</xsl:text><xsl:value-of select="@id"/><xsl:text>_get(){
&tab;return lstring("</xsl:text><xsl:value-of select="." /><xsl:text>");&nl;}&nl;</xsl:text>
</xsl:template>


</xsl:transform>



