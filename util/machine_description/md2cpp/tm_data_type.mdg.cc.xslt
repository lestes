<?xml version="1.0"?>
<!DOCTYPE xsl:transform [
    <!ENTITY nl "&#x0A;">
    <!ENTITY tab "&#x09;">
]>

<!--
	This file contains xslt templates that process machine description XML file and generate
	/target/machine/${CPU}/lestes/md/types/tm_data_type.mdg.cc file. The generated file
	contains definition of an instance method that creates instances of data types supported by
	target architecture.
-->


<xsl:transform	version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:md="http://lestes.jikos.cz/machinedescription">
<xsl:output method="text" encoding="ISO-8859-1"/>

<xsl:template match="/md:TargetArchitecture">
    <xsl:text>/* This file is generated from machine description */
#include &lt;lestes/std/map.hh&gt;
#include &lt;lestes/md/types/tm_data_type.g.hh&gt;
#include &lt;lestes/md/registers/tm_register.g.hh&gt;

/*! \file
	\brief Target-machine data-types.
	\author jaz
*/

package(lestes);
package(md);
package(types);

using ::lestes::md::registers::tm_register;

ptr&lt;tm_dt_simple&gt; tm_dt_simple::instance(dt_id_type id) {
&tab;map&lt;dt_id_type, srp&lt;tm_dt_simple&gt; &gt;::iterator existing_type = id_to_instance_get()-&gt;find(id);&nl;
&tab;if ( existing_type != id_to_instance_get()-&gt;end() ) {
&tab;&tab;return existing_type-&gt;second;
&tab;}&nl;
&tab;ptr&lt;tm_dt_simple&gt; new_type = NULL;
&tab;switch(id) {&nl;</xsl:text>

    <xsl:apply-templates mode="tm_type_generate" select="md:DataTypes"/> 
	
    <xsl:text>&tab;&tab;default: lassert(false);&nl;
&tab;}&nl;
&tab;lassert(new_type);
&tab;id_to_instance_get()-&gt;insert(::std::pair&lt;dt_id_type, srp&lt;tm_dt_simple&gt; &gt;(id,new_type));
&tab;return new_type;
}

end_package(types);
end_package(md);
end_package(lestes);
</xsl:text>
</xsl:template>

<xsl:template mode="tm_type_generate" match="*">
	
	<xsl:for-each select="md:DataType">
    	    <xsl:text>&tab;&tab;case </xsl:text>
	    <xsl:value-of select="@id"/>
	    <xsl:text>: {
&tab;&tab;&tab;new_type = tm_dt_simple::create(</xsl:text>
	    <xsl:value-of select="@id"/>
	    <xsl:text>, </xsl:text>
		<xsl:value-of select="@format"/>
	    <xsl:text>, </xsl:text>
	    <xsl:value-of select="@bitwidth"/>
	    <xsl:text>, </xsl:text>
	    <xsl:value-of select="@alignment"/>
	    <xsl:choose>
		<xsl:when test="@retreg">
		    <xsl:text>, tm_register::instance(::lestes::md::registers::</xsl:text>
		    <xsl:value-of select="@retreg"/>
		    <xsl:text>)</xsl:text>
		</xsl:when>
		<xsl:otherwise>
		    <xsl:text>, NULL</xsl:text>
		</xsl:otherwise>
	    </xsl:choose>
	    <xsl:text>, lstring("</xsl:text>
		<xsl:value-of select="@asm"/>
		<xsl:text>"), lstring("</xsl:text>
		<xsl:value-of select="@asm_decl"/>
	    <xsl:text>"));&nl;&tab;&tab;} break;&nl;</xsl:text>
	</xsl:for-each>
</xsl:template>

</xsl:transform>


