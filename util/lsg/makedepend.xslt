<?xml version="1.0"?>
<!--
	The lestes compiler suite
	Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
	Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
	Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
	Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
	Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
	Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
	Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; version 2 of the License.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	See the full text of the GNU General Public License version 2, and
	the limitations in the file doc/LICENSE.

	By accepting the license the licensee waives any and all claims
	against the copyright holder(s) related in whole or in part to the
	work, its use, and/or the inability to use it.
-->
<!DOCTYPE xsl:transform [
    <!ENTITY nl "&#x0A;">
    <!ENTITY tab "&#x09;">
]>
<xsl:transform version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:lsd="http://lestes.jikos.cz/schemas/lsd" xmlns:lvd="http://lestes.jikos.cz/schemas/lvd">
<!-- Search for dependencies between LSD files. -->
	<xsl:output method="text"/>

	<xsl:template match="/lsd:lsd | /lvd:lvd">
		<!-- targets -->
		<xsl:apply-templates select="lsd:file-name | lvd:file-name" />
		<xsl:text>: </xsl:text>
		<!-- prerequisites -->
		<xsl:apply-templates mode="deps-lookup" select="lsd:include | lvd:uses" />
		<xsl:text>&nl;</xsl:text>
	</xsl:template>

	<xsl:template match="lsd:file-name">
		<xsl:value-of select="concat(text(),'.g.hh ',text(),'.g.cc')" />
	</xsl:template>

	<xsl:template match="lvd:file-name">
		<xsl:value-of select="concat(text(),'.v.lsd ',text(),'.v.cc')" />
	</xsl:template>

	<xsl:template mode="deps-lookup" match="lsd:include | lvd:uses">
		<xsl:param name="rel-path" />
		<!-- search for a all included files -->
		<xsl:variable name="real-href">
			<xsl:call-template name="vlsd2lvd">
				<xsl:with-param name="str" select="@href" />
			</xsl:call-template>
		</xsl:variable>
		<xsl:if test="not($real-href = @href)">
			<xsl:value-of select="concat($rel-path,@href,' ')" />
		</xsl:if>
		<xsl:value-of select="concat($rel-path,$real-href,' ')"/>
		<xsl:variable name="next-rel-path">
			<xsl:value-of select="$rel-path" />
			<xsl:call-template name="dirname">
				<xsl:with-param name="fullname" select="$real-href" />
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="doc" select="document($real-href,.)" />
		<xsl:apply-templates mode="deps-lookup" select="$doc/lsd:lsd/lsd:include | $doc/lvd:lvd/lvd:uses">
			<xsl:with-param name="rel-path" select="$next-rel-path" />
		</xsl:apply-templates>
	</xsl:template>

	<xsl:template name="vlsd2lvd">
		<xsl:param name="str" />
		<xsl:choose>
			<xsl:when test="substring($str,string-length($str)-5) = '.v.lsd'">
				<xsl:value-of select="concat( substring($str,1,string-length($str)-5), 'lvd' )" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$str" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!--
  	returns directory part of the given (file) name
	if it contains a slash ('/'), the result will end with a slash
	if it does not contain a slash, the result will be empty
	-->
	<xsl:template name="dirname">
		<xsl:param name="fullname" />
		<xsl:if test="contains($fullname,'/')">
			<xsl:value-of select="substring-before($fullname,'/')" />
			<xsl:text>/</xsl:text>
			<xsl:call-template name="dirname">
				<xsl:with-param name="fullname" select="substring-after($fullname,'/')" />
			</xsl:call-template>
		</xsl:if>
	</xsl:template>

</xsl:transform>
