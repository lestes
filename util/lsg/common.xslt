<?xml version="1.0"?>
<!--
	The lestes compiler suite
	Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
	Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
	Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
	Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
	Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
	Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
	Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; version 2 of the License.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	See the full text of the GNU General Public License version 2, and
	the limitations in the file doc/LICENSE.

	By accepting the license the licensee waives any and all claims
	against the copyright holder(s) related in whole or in part to the
	work, its use, and/or the inability to use it.
-->
<!DOCTYPE xsl:transform [
<!ENTITY nl "&#x0A;">
<!ENTITY tab "&#x09;">
]>
<xsl:transform version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:lsd="http://lestes.jikos.cz/schemas/lsd">
	<!-- templates included by both xml2cc and xml2hh -->

	<!-- do NOT set the output method, this file is included from files that use different ones
	<xsl:output method="text" encoding="ISO-8859-1"/>
	-->
	<xsl:key name="any-class" match="/lsd:lsd/lsd:class | /lsd:lsd/lsd:forward-class | /lsd:lsd/lsd:using-class | /lsd:lsd/lsd:foreign-class | /lsd:lsd/lsd:class/lsd:visitor" use="@name"/>


	<xsl:template name="ptr">
		<xsl:param name="data-type"/>
		<xsl:text>ptr&lt; </xsl:text>
		<xsl:choose>
			<xsl:when test="/lsd:lsd/lsd:using-class[ @name = $data-type ]">
				<xsl:variable name="pkg" select="/lsd:lsd/lsd:using-class[ @name = $data-type ]/@packages"/>
				<xsl:value-of select="$pkg"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:for-each select="/lsd:lsd/lsd:foreign-class[ @name = $data-type ][1]">
					<xsl:for-each select="lsd:p">
						<xsl:value-of select="concat('::',.)"/>
					</xsl:for-each>
					<xsl:text>::</xsl:text>
				</xsl:for-each>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:value-of select="$data-type"/>
		<xsl:text> &gt;</xsl:text>
	</xsl:template>

	<xsl:template name="srp">
		<xsl:param name="data-type"/>
		<xsl:text>srp&lt; </xsl:text>
		<xsl:if test="/lsd:lsd/lsd:using-class[ @name = $data-type ]">
			<xsl:variable name="pkg" select="/lsd:lsd/lsd:using-class[ @name = $data-type ]/@packages"/>
			<xsl:value-of select="$pkg"/>
		</xsl:if>
		<xsl:for-each select="/lsd:lsd/lsd:foreign-class[ @name = $data-type ][1]">
			<xsl:for-each select="lsd:p">
				<xsl:value-of select="concat('::',.)"/>
			</xsl:for-each>
			<xsl:text>::</xsl:text>
		</xsl:for-each>
		<xsl:value-of select="$data-type"/>
		<xsl:text> &gt;</xsl:text>
	</xsl:template>


	<!-- two types of smart pointers follow -->
	<!-- simple types are passed and returned by value, complex by ptr<T> --> 
	<xsl:template name="val-or-ptr">
		<xsl:param name="data-type"/>
		<xsl:choose>
			<xsl:when test="key('any-class',$data-type)">
				<xsl:call-template name="ptr">
					<xsl:with-param name="data-type" select="$data-type"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$data-type"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>


	<!-- simple types are included by value, complex by srp<T> --> 
	<xsl:template name="val-or-srp">
		<xsl:param name="data-type"/>
		<xsl:choose>
			<xsl:when test="key('any-class',$data-type)">
				<xsl:call-template name="srp">
					<xsl:with-param name="data-type" select="$data-type"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$data-type"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- simple types are passed to set()/alt() by value, complex by const ptr<>ref --> 
	<xsl:template name="val-or-const-ptr-ref">
		<xsl:param name="data-type"/>
		<xsl:choose>
			<xsl:when test="key('any-class',$data-type)">
				<xsl:text>const </xsl:text>
				<xsl:call-template name="ptr">
					<xsl:with-param name="data-type" select="$data-type"/>
				</xsl:call-template>
				<xsl:text> &amp;</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$data-type"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- simple types are passed and returned by value, complex by ptr<T> --> 
	<xsl:template name="simple-or-ptr">
		<xsl:param name="data-type"/>
		<xsl:param name="simple"/>
		<xsl:choose>
			<xsl:when test="string($simple) != 'yes'">
				<xsl:value-of select="concat('ptr&lt; ',$data-type,' &gt;')"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$data-type"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>


	<xsl:template name="specifier">
		<xsl:value-of select="string(@specifier)"/>
		<xsl:if test="string-length(@specifier)">
			<xsl:text> </xsl:text>
		</xsl:if>
	</xsl:template>

	<xsl:template name="qualifier">
		<xsl:if test="string-length(@qualifier)">
			<xsl:text> </xsl:text>
		</xsl:if>
		<xsl:value-of select="string(@qualifier)"/>
	</xsl:template>

	<xsl:template name="resolve-base">
		<!-- returns the (qualified) name of the class, which current class is derived from -->
		<xsl:variable name="base">
			<xsl:choose>
				<xsl:when test="string-length(@base)">
					<xsl:value-of select="@base"/>			<!-- specified base -->
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="document('',.)/lsd:lsd/lsd:default-base/@type" />	<!-- or default base -->
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="../lsd:foreign-class[ @name = $base ]">
				<xsl:text>::</xsl:text>
				<xsl:for-each select="../lsd:foreign-class[@name = $base]/lsd:p">
					<xsl:value-of select="concat(./text(),'::')" />
				</xsl:for-each>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="../lsd:using-class[@name = $base][position()=1]/@packages" />
			</xsl:otherwise>
		</xsl:choose>

		<xsl:value-of select="$base"/>
	</xsl:template>

	<xsl:template name="stripped-base">
		<!-- returns the (unqualified!) name of the class, which current class is derived from -->
		<xsl:choose>
			<xsl:when test="string-length(@base)">
				<xsl:value-of select="@base"/>			<!-- specified base -->
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="document('',.)/lsd:lsd/lsd:default-base/@type" />	<!-- or default base -->
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="class-lookup">
		<!-- search for a file containing the class given by name -->
		<xsl:param name="doc"/>
		<xsl:param name="class"/>
		<xsl:for-each select="document($doc,.)/lsd:lsd/lsd:include">
			<xsl:choose>
				<xsl:when test="document(@href,.)/lsd:lsd/lsd:class[ @name = $class]">
					<xsl:value-of select="@href"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="class-lookup">
						<xsl:with-param name="doc" select="@href"/>
						<xsl:with-param name="class" select="$class"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:template>

	<xsl:template match="lsd:class" mode="ctor-args">
		<!-- comma separated list of constructor arguments  -->
		<!-- == all data members, including inherited ones -->
		<xsl:param name="with-types"/> <!-- include field types in list? -->
		<xsl:variable name="this-class-name" select="@name"/>
		<xsl:variable name="base">
			<xsl:call-template name="stripped-base"/>
		</xsl:variable>
		<xsl:variable name="root">
			<!-- search for a file containing the base class -->
			<xsl:call-template name="class-lookup">
				<xsl:with-param name="class" select="$base"/>
			</xsl:call-template>
		</xsl:variable>
		<!--xsl:value-of select="concat('/* ',$this-class-name,'|',$base,'|',$root,' */')"/-->
		<xsl:variable name="accum"> <!-- collect field from my parents -->
			<!-- apply recursively to the base class (either here or in external file) -->
			<xsl:apply-templates mode="ctor-args" select="(/lsd:lsd/lsd:class|document($root,.)/lsd:lsd/lsd:class)[@name = $base]">
				<xsl:with-param name="with-types" select="$with-types"/>
			</xsl:apply-templates>
		</xsl:variable>
		<xsl:value-of select="$accum"/> <!-- list of inherited fields -->
		<xsl:if test="string-length($accum)!=0 and ( (lsd:field | lsd:collection) [ string(@specifier) != 'static' ] )">
			<xsl:text>,&nl;&tab;&tab;</xsl:text>
		</xsl:if>
		<xsl:for-each select="(lsd:field | lsd:collection) [ string(@specifier) != 'static' ]">
			<!-- list of my own fields -->
			<xsl:if test="$with-types = 'yes'">
				<xsl:choose>
					<xsl:when test="local-name(.) = 'field'"> <!-- i'm a normal field -->
						<xsl:call-template name="val-or-ptr">
							<xsl:with-param name="data-type" select="@type"/>
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise> <!-- i'm a collection -->
						<xsl:call-template name="col-ptr"/>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:text> </xsl:text>
			</xsl:if> <!-- with-types -->
			<xsl:value-of select="concat('a__',$this-class-name,'__',@name)"/>
			<!-- mangled to be unique -->
			<xsl:if test="not(position()=last())">
				<xsl:text>,&nl;&tab;&tab;</xsl:text>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>

	<xsl:template match="*" mode="ctor-init">
		<!-- constructor initializer -->
		<xsl:variable name="this-class-name" select="@name"/>
		<xsl:variable name="base">
			<xsl:call-template name="stripped-base"/>
		</xsl:variable>
		<xsl:variable name="root">
			<!-- search for a file containing the base class -->
			<xsl:call-template name="class-lookup">
				<xsl:with-param name="class" select="$base"/>
			</xsl:call-template>
		</xsl:variable>
		<!-- call my father's constructor -->
		<xsl:value-of select="concat($base,'(')" />
		<xsl:apply-templates mode="ctor-args" select="(document($root,.)/lsd:lsd/lsd:class|/lsd:lsd/lsd:class)[ @name= $base]">
			<xsl:with-param name="with-types" select="'no'"/>
		</xsl:apply-templates>
		<xsl:text>)</xsl:text>
		<xsl:for-each select="(lsd:field | lsd:collection) [ string(@specifier) != 'static' ]">
			<!-- list my own members and check them -->
			<xsl:text>, </xsl:text>
			<xsl:value-of select="concat(@name,'(')"/>
			<xsl:choose>
				<xsl:when test="(local-name(.) = 'field') and not(key('any-class',@type)) and (count(@check) = 0)">
					<xsl:value-of select="concat('a__',$this-class-name,'__',@name,')')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:choose>
						<xsl:when test="count(@check) = 0">
							<xsl:value-of select="concat($default-checked,'(a__',$this-class-name,'__',@name,'))')"/>
						</xsl:when>
						<xsl:when test="string(@check) = ''">
							<xsl:value-of select="concat('a__',$this-class-name,'__',@name,')')"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="concat(@check,'(a__',$this-class-name,'__',@name,'))')"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:template>

	<xsl:template match="*" mode="fact-args">
		<!-- comma separated list of second factory method arguments  -->
		<!-- == all data members, including inherited ones, excluding initialized ones -->
		<xsl:param name="with-types"/> <!-- include field types in list? -->
		<xsl:variable name="this-class-name" select="@name"/>
		<xsl:variable name="base">
			<xsl:call-template name="stripped-base"/>
		</xsl:variable>
		<xsl:variable name="root">
			<!-- search for a file containing the base class -->
			<xsl:call-template name="class-lookup">
				<xsl:with-param name="class" select="$base"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="accum"> <!-- collect field from my parents -->
			<xsl:apply-templates select="(document($root,.)/lsd:lsd/lsd:class|/lsd:lsd/lsd:class)[ @name = $base]" mode="fact-args">
				<xsl:with-param name="with-types" select="$with-types"/>
			</xsl:apply-templates>
		</xsl:variable>
		<xsl:value-of select="$accum"/> <!-- list of inherited fields without initializer -->
		<xsl:if test="string-length($accum)!=0 and ( (lsd:field[ @init = 'void'] | lsd:field[ count(@init)=0 ] | lsd:collection[ @init = 'void'] | lsd:collection[ count(@init)=0 ]) [ string(@specifier) != 'static' ] )">
			<xsl:text>,&nl;&tab;&tab;</xsl:text>
		</xsl:if>
		<xsl:for-each select="(lsd:field [ @init='void' ] | lsd:field [ count(@init)=0 ] | lsd:collection[ @init = 'void' ] | lsd:collection[ count(@init)=0 ]) [ string(@specifier) != 'static' ]">
			<!-- list of my own fields -->
			<xsl:if test="$with-types = 'yes'">
				<xsl:choose>
					<xsl:when test="local-name(.) = 'field'"> <!-- i'm a normal field -->
						<xsl:call-template name="val-or-ptr">
							<xsl:with-param name="data-type" select="@type"/>
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise> <!-- i'm a collection -->
						<xsl:call-template name="col-ptr"/>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:text> </xsl:text>
			</xsl:if> <!-- with-types -->
			<xsl:value-of select="concat('a__',$this-class-name,'__',@name)"/>
			<!-- mangled to be unique -->
			<xsl:if test="not(position()=last())">
				<xsl:text>,&nl;&tab;&tab;</xsl:text>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>


	<xsl:template match="*" mode="fact-init">
		<!-- comma separated list of initializers used by the second factory method  -->
		<!-- == all data members xor initializers if present -->
		<xsl:variable name="this-class-name" select="@name"/>
		<xsl:variable name="base">
			<xsl:call-template name="stripped-base"/>
		</xsl:variable>
		<xsl:variable name="root">
			<!-- search for a file containing the base class -->
			<xsl:call-template name="class-lookup">
				<xsl:with-param name="class" select="$base"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="accum"> <!-- collect field from my parents -->
			<xsl:apply-templates select="(document($root,.)/lsd:lsd/lsd:class|/lsd:lsd/lsd:class)[@name = $base]" mode="fact-init"/>
		</xsl:variable>
		<xsl:value-of select="$accum"/> <!-- list of inherited fields -->
		<xsl:if test="string-length($accum)!=0 and ( (lsd:field | lsd:collection) [ string(@specifier) != 'static' ] )">
			<xsl:text>, </xsl:text>
		</xsl:if>
		<xsl:for-each select="(lsd:field | lsd:collection) [ string(@specifier) != 'static' ]">
			<!-- list of my own fields xor initializers -->
			<xsl:choose>
				<xsl:when test="count(@init) != 0 and @init != 'void'">
					<!-- when there is an initializer, use it -->
					<xsl:value-of select="@init"/>
					<xsl:if test="local-name(.) = 'collection' and @init = ''">
						<xsl:call-template name="col-type"/>
						<xsl:text> ::create()</xsl:text>
					</xsl:if>
				</xsl:when>
				<xsl:otherwise>
					<!-- no initializer -> use a factory method argument -->
					<xsl:value-of select="concat('a__',$this-class-name,'__',@name)"/>
					<!-- mangled to be unique -->
				</xsl:otherwise>
			</xsl:choose>
			<xsl:if test="not(position()=last())">
				<xsl:text>, </xsl:text>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>


	<xsl:template match="*" mode="check-init">
		<!-- gives nonempty output, if there is an initialized field or collection -->
		<xsl:variable name="this-class-name" select="@name"/>
		<xsl:variable name="base">
			<xsl:call-template name="stripped-base"/>
		</xsl:variable>
		<xsl:variable name="root">
			<!-- search for a file containing the base class -->
			<xsl:call-template name="class-lookup">
				<xsl:with-param name="class" select="$base"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="accum"> <!-- collect field from my parents -->
			<xsl:apply-templates select="(document($root,.)/lsd:lsd/lsd:class|/lsd:lsd/lsd:class)[@name = $base]" mode="check-init"/>
		</xsl:variable>
		<xsl:value-of select="$accum"/> <!-- list of inherited fields -->
		<xsl:for-each select="(lsd:field | lsd:collection) [ string(@specifier) != 'static' ]">
			<xsl:if test="count(@init) != 0 and @init != 'void'">
				<xsl:text>*</xsl:text>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>

	<xsl:template match="*" mode="html-text">
		<xsl:variable name="name">
			<!-- contains element name without prefix -->
			<xsl:choose>
				<xsl:when test="contains(name(),':')">
					<!-- strip namespace prefix -->
					<xsl:value-of select="substring-after(name(),':')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="name()"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="content">
			<xsl:apply-templates mode="html-text"/>
		</xsl:variable>
		<!--	<xsl:value-of select="concat('}}',$content,'{{')"/> -->
		<xsl:value-of select="concat('&lt;',$name)"/>
		<xsl:for-each select="@*">
			<xsl:value-of select="concat(' ',name(),'=&quot;',.,'&quot;')"/>
		</xsl:for-each>
		<xsl:choose>
			<xsl:when test="string($content)">
				<xsl:text>&gt;</xsl:text>
				<xsl:value-of select="$content"/>
				<xsl:value-of select="concat('&lt;/',$name,'&gt;')"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>/&gt;</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- doxygen comments anywhere in XML -->
	<xsl:template name="doxygen">
		<xsl:param name="indent"/>
		<xsl:for-each select="lsd:dox">
			<xsl:choose>
				<xsl:when test="string-length(lsd:det) = 0">
					<xsl:text>//! </xsl:text>
					<xsl:value-of select="normalize-space(lsd:bri)"/>
					<xsl:text>&nl;</xsl:text>
					<xsl:value-of select="$indent" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>/*! </xsl:text>
					<xsl:if test="string-length(lsd:bri)">
						<xsl:text>\brief </xsl:text>
						<xsl:value-of select="lsd:bri"/>
						<xsl:text>&nl;</xsl:text>
					</xsl:if>
					<xsl:text>&nl;</xsl:text>
					<xsl:value-of select="$indent" />
					<xsl:apply-templates mode="html-text" select="lsd:det/node()"/>
					<xsl:text>&nl;</xsl:text>
					<xsl:value-of select="$indent" />
					<xsl:text>*/&nl;</xsl:text>
					<xsl:value-of select="$indent" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="imports">	<!-- #include ... -->
		<xsl:for-each select="/lsd:lsd/lsd:imports/lsd:i">
			<xsl:value-of select="concat('#include &lt;',./text(),'&gt;&nl;')"/>
		</xsl:for-each>
		<xsl:text>&nl;</xsl:text>
	</xsl:template>

	<xsl:template name="implementation-imports">	<!-- #include ... -->
		<xsl:for-each select="/lsd:lsd/lsd:implementation-imports/lsd:i">
			<xsl:value-of select="concat('#include &lt;',./text(),'&gt;&nl;')"/>
		</xsl:for-each>
		<xsl:text>&nl;</xsl:text>
	</xsl:template>

	<xsl:template name="open-packages">	<!-- package(...); -->
		<xsl:for-each select="/lsd:lsd/lsd:packages/lsd:p">
			<xsl:value-of select="concat('package(', ./text(), ');&nl;')" />
		</xsl:for-each>
		<xsl:text>&nl;</xsl:text>
	</xsl:template>

	<xsl:template name="close-packages">
		<xsl:for-each select="/lsd:lsd/lsd:packages/lsd:p">
			<xsl:sort order="descending" data-type="number" select="position()" />
			<xsl:value-of select="concat('end_package(', ./text(), ');&nl;')" />
		</xsl:for-each>
		<xsl:text>&nl;</xsl:text>
	</xsl:template>

	<xsl:template name="open-header">
		<!-- classical preprocesor hack to avoid inclusion problems -->
		<xsl:value-of select="concat('#ifndef ',$binding-macro,'&nl;')"/>
		<xsl:value-of select="concat('#define ',$binding-macro,'&nl;')"/>
		<xsl:text>&nl;</xsl:text>
	</xsl:template>    

	<xsl:template name="close-header">
		<xsl:value-of select="concat('#endif // ',$binding-macro,'&nl;')"/>
	</xsl:template>    

	<xsl:variable name="binding-macro">
		<xsl:for-each select="/lsd:lsd/lsd:packages/lsd:p">
			<xsl:value-of select="concat(./text(),'__')"/>
		</xsl:for-each>
		<xsl:value-of select="translate(translate(/lsd:lsd/lsd:file-name,'.','_'),'-','_')"/>
		<xsl:text>_g_hh__included</xsl:text>
	</xsl:variable>

	<xsl:variable name="default-checked">
		<xsl:choose>
			<xsl:when test="string(/lsd:lsd/lsd:default-check/text()) = ''">
				<xsl:text>checked</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="/lsd:lsd/lsd:default-check/text()"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:template name="resolve-collection-kind">
		<xsl:choose>
			<xsl:when test="@kind">
				<xsl:value-of select="@kind"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="/lsd:lsd/lsd:default-collection/@kind"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="col-ptr">
		<!--  ptr<kind<type>>   -->
		<!-- requirement: actual node is <collection> element -->
		<xsl:text>ptr&lt; </xsl:text>
		<xsl:call-template name="col-type"/>
		<xsl:text> &gt;</xsl:text>
	</xsl:template>

	<xsl:template name="col-srp">
		<!--  srp<kind<type>>   -->
		<!-- requirement: actual node is <collection> element -->
		<xsl:text>srp&lt; </xsl:text>
		<xsl:call-template name="col-type"/>
		<xsl:text> &gt;</xsl:text>
	</xsl:template>

	<xsl:template name="col-type">
		<!-- requirement: actual node is <collection> element -->
		<xsl:variable name="kind">
			<xsl:call-template name="resolve-collection-kind"/>
		</xsl:variable>
		<xsl:text>::lestes::std::</xsl:text>
		<xsl:value-of select="concat($kind,'&lt; ')"/>
		<xsl:if test="$kind = 'map'">
			<xsl:call-template name="val-or-srp">
				<xsl:with-param name="data-type" select="@key"/>
			</xsl:call-template>
			<xsl:text>, </xsl:text>
		</xsl:if>
		<xsl:call-template name="val-or-srp">
			<xsl:with-param name="data-type" select="@type"/>
		</xsl:call-template>
		<xsl:if test="($kind = 'map' or $kind = 'set') and string(@comparator) != ''">
			<xsl:value-of select="concat(', ',@comparator)"/>
		</xsl:if>
		<xsl:text> &gt;</xsl:text>
	</xsl:template>

	<xsl:template name="col-metatype">
		<!-- requirement: actual node is <collection> element -->
		<xsl:variable name="kind">
			<xsl:call-template name="resolve-collection-kind"/>
		</xsl:variable>
		<xsl:value-of select="concat($kind,'&lt; ')"/>
		<xsl:if test="$kind = 'map'">
			<xsl:call-template name="val-or-srp">
				<xsl:with-param name="data-type" select="@key"/>
			</xsl:call-template>
			<xsl:text>, </xsl:text>
		</xsl:if>
		<xsl:call-template name="val-or-srp">
			<xsl:with-param name="data-type" select="@type"/>
		</xsl:call-template>
		<xsl:if test="($kind = 'map' or $kind = 'set') and string(@comparator) != ''">
			<xsl:value-of select="concat(', ',@comparator)"/>
		</xsl:if>
		<xsl:text> &gt;</xsl:text>
	</xsl:template>

	<!-- simple types are returned by value, complex by ptr<T>, enums are qualified --> 
	<xsl:template name="val-or-ptr-or-enum">
		<xsl:param name="data-type"/>
		<xsl:param name="class-name"/>
		<xsl:choose>
			<xsl:when test="key('any-class',$data-type)">
				<xsl:call-template name="ptr">
					<xsl:with-param name="data-type" select="$data-type"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:if test="/lsd:lsd/lsd:class[ @name = $class-name ]/lsd:enum[ @name = $data-type ]">
					<xsl:value-of select="concat($class-name,'::')"/>
				</xsl:if>
				<xsl:value-of select="$data-type"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template mode="resolve-field-type" match="lsd:class">
		<!--
		given field name, returns field type formatted as return type
		(ie. surrounded by ptr<> if it is not a simple type)
		apply on lsd:class in which you want to start searching
		name of the template that wraps the type in ptr<> depends on the file
		type we are generating. the type must be specified in the "type"
		parameters, valid values are "hh" and "cc"
		-->
		<xsl:param name="name" />
		<xsl:param name="type" />
		<xsl:param name="err-class" />
		<xsl:param name="err-method" />
		<xsl:choose>
			<xsl:when test="lsd:field[@name = $name]">
				<xsl:if test="lsd:field[@name = $name and @get='none']">
					<xsl:message terminate="yes">
						<xsl:value-of select="concat('Trying to return value of field &quot;',$name,'&quot; when processing visit-return-method &quot;',$err-method,'&quot; in class &quot;',$err-class,'&quot;.')" />
					</xsl:message>
				</xsl:if>
				<xsl:choose>
					<xsl:when test="$type = 'hh'">
						<xsl:call-template name="val-or-ptr">
							<xsl:with-param name="data-type" select="lsd:field[@name = $name]/@type"/>
							<xsl:with-param name="class-name" select="@name"/>
						</xsl:call-template>
					</xsl:when>
					<xsl:when test="$type = 'cc'">
						<xsl:call-template name="val-or-ptr-or-enum">
							<xsl:with-param name="data-type" select="lsd:field[@name = $name]/@type"/>
							<xsl:with-param name="class-name" select="@name"/>
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<xsl:message terminate="yes">BUG in the generator.</xsl:message>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="lsd:collection[@name = $name]">
				<xsl:if test="lsd:collection[@name = $name and @get='none']">
					<xsl:message terminate="yes">
						<xsl:value-of select="concat('Trying to return value of collection &quot;',$name,'&quot; when processing visit-return-method &quot;',$err-method,'&quot; in class &quot;',$err-class,'&quot;.')" />
					</xsl:message>
				</xsl:if>
				<xsl:for-each select="lsd:collection[@name = $name]">
					<xsl:call-template name="col-ptr" />
				</xsl:for-each>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="base">
					<xsl:call-template name="stripped-base"/>
				</xsl:variable>
				<xsl:variable name="doc">
					<!-- search for a file containing the base class -->
					<xsl:call-template name="class-lookup">
						<xsl:with-param name="class" select="$base"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- apply recursively to the base class (either here or in external file) -->
				<xsl:apply-templates mode="resolve-field-type" select="(/lsd:lsd/lsd:class|document($doc,.)/lsd:lsd/lsd:class)[@name = $base]">
					<xsl:with-param name="name" select="$name"/>
					<xsl:with-param name="type" select="$type"/>
					<xsl:with-param name="err-class" select="$err-class" />
					<xsl:with-param name="err-method" select="$err-method" />
				</xsl:apply-templates>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template mode="resolve-base-visitor-name" match="lsd:class">
		<!--
		matching a class, the template returns name of the base visitor it is derived from
		the base visitor is known to be derived from "::lestes::std::visitor_base"
		-->
		<xsl:choose>
			<xsl:when test="@base = '::lestes::std::visitor_base'">
				<xsl:value-of select="@name" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="base">
					<xsl:call-template name="stripped-base"/>
				</xsl:variable>
				<xsl:variable name="doc">
					<!-- search for a file containing the base class -->
					<xsl:call-template name="class-lookup">
						<xsl:with-param name="class" select="$base"/>
					</xsl:call-template>
				</xsl:variable>
				<!-- apply recursively to the base class (either here or in external file) -->
				<xsl:apply-templates mode="resolve-base-visitor-name" select="(/lsd:lsd/lsd:class|document($doc,.)/lsd:lsd/lsd:class)[@name = $base]" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template mode="get-class-with-visitor" match="lsd:lsd">
		<!--
		given visitor name, returns name of a class that contains its element
		class elements that are children of matched element are searched,
		as are those that are "included" by lsd:include
		-->
		<xsl:param name="name" />
		<xsl:variable name="result">
			<xsl:choose>
				<xsl:when test="lsd:class/lsd:visitor[@name = $name]">
					<xsl:for-each select="lsd:class/lsd:visitor[@name = $name]">
						<xsl:text>::</xsl:text>
						<xsl:for-each select="../../lsd:packages/lsd:p">
							<xsl:value-of select="concat(text(),'::')" />
						</xsl:for-each>
						<xsl:value-of select="../@name" />
						<xsl:text>|</xsl:text>
					</xsl:for-each>
				</xsl:when>
				<xsl:otherwise>
					<xsl:for-each select="lsd:include">
						<xsl:variable name="rec-result">
							<xsl:apply-templates mode="get-class-with-visitor" select="document(@href,.)/lsd:lsd">
								<xsl:with-param name="name" select="$name" />
							</xsl:apply-templates>
						</xsl:variable>
						<!-- append '|' only when recursive application returned non-empty result -->
						<xsl:if test="string-length($rec-result) > 0">
							<xsl:value-of select="$rec-result" />
							<xsl:text>|</xsl:text>
						</xsl:if>
					</xsl:for-each>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:value-of select="substring-before($result,'|')" />
	</xsl:template>

</xsl:transform>
