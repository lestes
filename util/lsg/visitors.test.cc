/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <util/lsg/visitors.test.g.hh>

package(util);
package(lsg);

int middle::visit_sf( ptr<sf> )
{
	return i+3;
}

int middle::visit_st( ptr<st> )
{
	return i+4;
}

int middle::go_johny_go( ptr<first_derived> )
{
	return i_get()+1;
}

int actual_visitor::ymca( ptr<third_derived> )
{
	return i_get()+2;
}

end_package(lsg);
end_package(util);

using namespace ::util::lsg;

int main()
{
	ptr<actual_visitor> v = actual_visitor::create(1);
	int result = tf::create()->accept_vejda(v);
	// ymca() should be called, result should be 1+2
	lassert( result == 1+2 );
	return 0;
}
