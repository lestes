<?xml version="1.0"?>
<!--
	The lestes compiler suite
	Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
	Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
	Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
	Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
	Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
	Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
	Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; version 2 of the License.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	See the full text of the GNU General Public License version 2, and
	the limitations in the file doc/LICENSE.

	By accepting the license the licensee waives any and all claims
	against the copyright holder(s) related in whole or in part to the
	work, its use, and/or the inability to use it.
-->
<!DOCTYPE xsl:transform [
	<!ENTITY nl "&#x0A;">
	<!ENTITY tab "&#x09;">
]>
<xsl:transform version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:lsd="http://lestes.jikos.cz/schemas/lsd" xmlns:lvd="http://lestes.jikos.cz/schemas/lvd">
	<!-- Generate abstract class for each visitor. -->

	<!-- do NOT set the output method, this file is included from files that use different ones
 	<xsl:output method="text" encoding="ISO-8859-1"/>
	-->

	<xsl:template name="ptr">
		<xsl:param name="data-type"/>
		<xsl:param name="doc"/>
		<xsl:text>ptr&lt; </xsl:text>
		<xsl:choose>
			<xsl:when test="document($doc,.)/lsd:lsd/lsd:using-class[ @name = $data-type ]">
				<xsl:variable name="pkg" select="document($doc,.)/lsd:lsd/lsd:using-class[ @name = $data-type ]/@packages"/>
				<xsl:value-of select="$pkg"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:for-each select="document($doc,.)/lsd:lsd/lsd:foreign-class[ @name = $data-type ][1]">
					<xsl:for-each select="lsd:p">
						<xsl:value-of select="concat('::',./text())"/>
					</xsl:for-each>
					<xsl:text>::</xsl:text>
				</xsl:for-each>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:value-of select="$data-type"/>
		<xsl:text> &gt;</xsl:text>
	</xsl:template>

	<xsl:template name="open-packages">	<!-- package(...); -->
		<xsl:for-each select="/lvd:lvd/lvd:packages/lvd:p">
			<xsl:value-of select="concat('package(', ./text(), ');&nl;')" />
		</xsl:for-each>
		<xsl:text>&nl;</xsl:text>
	</xsl:template>

	<xsl:template name="close-packages">
		<xsl:for-each select="/lvd:lvd/lvd:packages/lvd:p">
			<xsl:sort order="descending" data-type="number" select="position()" />
			<xsl:value-of select="concat('end_package(', ./text(), ');&nl;')" />
		</xsl:for-each>
		<xsl:text>&nl;</xsl:text>
	</xsl:template>

	<xsl:template name="open-header">
		<!-- classical preprocesor hack to avoid inclusion problems -->
		<xsl:value-of select="concat('#ifndef ',$binding-macro,'&nl;')"/>
		<xsl:value-of select="concat('#define ',$binding-macro,'&nl;')"/>
		<xsl:text>&nl;</xsl:text>
	</xsl:template>    

	<xsl:template name="close-header">
		<xsl:value-of select="concat('#endif // ',$binding-macro,'&nl;')"/>
	</xsl:template>    

	<xsl:variable name="binding-macro">
		<xsl:for-each select="/lvd:lvd/lvd:packages/lvd:p">
			<xsl:value-of select="concat(./text(),'__')"/>
		</xsl:for-each>
		<xsl:value-of select="translate(/lvd:lvd/lvd:file-name,'.','_')"/>
		<xsl:text>_v_hh__included</xsl:text>
	</xsl:variable>

	<xsl:template name="imports">	<!-- #include ... -->
		<xsl:for-each select="/lvd:lvd/lvd:imports/lvd:i">
			<xsl:value-of select="concat('#include &lt;', ./text(),'&gt;&nl;')"/>
		</xsl:for-each>
		<xsl:text>&nl;</xsl:text>
	</xsl:template>

	<xsl:template name="class-lookup">
		<!-- search for a file containing the class given by name -->
		<xsl:param name="doc"/>
		<xsl:param name="class"/>
		<!--xsl:message>
			<xsl:value-of select="concat('&tab;&tab;class-lookup for name: &quot;',$class,'&quot;&nl;')" />
		</xsl:message-->
		<xsl:variable name="result">
			<xsl:for-each select="document($doc,.)/lsd:lsd/lsd:include">
				<xsl:choose>
					<xsl:when test="document(@href,.)/lsd:lsd/lsd:class[ @name = $class ]">
						<xsl:value-of select="@href"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="class-lookup">
							<xsl:with-param name="doc" select="@href"/>
							<xsl:with-param name="class" select="$class"/>
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
			<xsl:text>|</xsl:text>
		</xsl:variable>
		<xsl:value-of select="substring-before($result,'|')" />
	</xsl:template>

	<xsl:template name="get-type">
		<xsl:param name="name"/>
		<xsl:param name="doc"/>
		<xsl:variable name="type" select="document($doc,.)/lsd:lsd/lsd:class/lsd:visitor[@name = $name]/@type"/>
		<xsl:choose>
			<xsl:when test="(document($doc,.)/lsd:lsd/lsd:class | document($doc,.)/lsd:lsd/lsd:forward-class |document($doc,.)/lsd:lsd/lsd:using-class |document($doc,.)/lsd:lsd/lsd:foreign-class)[ @name = $type ]">
				<!-- if collectible -->
				<xsl:call-template name="ptr">
					<xsl:with-param name="data-type" select="$type"/>
					<xsl:with-param name="doc" select="$doc"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$type"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="get-doc">
		<xsl:param name="name"/>
		<xsl:for-each select="/lvd:lvd/lvd:uses">
			<xsl:if test="document(@href,.)/lsd:lsd/lsd:class/lsd:visitor[@name = $name]">
				<xsl:value-of select="@href"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="get-class">
		<xsl:param name="name"/>
		<xsl:param name="doc"/>
		<xsl:for-each select="document($doc,.)/lsd:lsd/lsd:class">
			<xsl:if test="lsd:visitor[ @name = $name ]">
				<xsl:value-of select="@name"/>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>

	<xsl:template mode="visitor-present" match="lsd:class">
		<xsl:param name="name"/>
		<!--xsl:message>
			<xsl:value-of select="concat('&tab;visitor-present: clas/@name = &quot;',@name,'&quot;&nl;')" />
		</xsl:message-->
		<xsl:choose>
			<xsl:when test="lsd:visitor [ @name = $name ]">	<!-- do i have it ? -->
				<xsl:text>yes</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="base-class-name">
					<xsl:call-template name="stripped-base-in-doc"/>
				</xsl:variable>
				<!--xsl:message>
					<xsl:value-of select="concat('&tab;&tab;searching for base &quot;',$base-class-name,'&quot;&nl;')" />
				</xsl:message-->
				<xsl:choose>
					<!-- try to find it in this document -->
					<xsl:when test="document('',.)/lsd:lsd/lsd:class[@name = $base-class-name]">
						<xsl:apply-templates mode="visitor-present" select="document('',.)/lsd:lsd/lsd:class[@name = $base-class-name]">
							<xsl:with-param name="name" select="$name"/>
						</xsl:apply-templates>
					</xsl:when>
					<!-- otherwise, try the "included" ones -->
					<xsl:otherwise>
						<xsl:variable name="doc">
							<xsl:call-template name="class-lookup">
								<xsl:with-param name="class" select="$base-class-name"/>
							</xsl:call-template>
						</xsl:variable>
						<!--xsl:message>
							<xsl:value-of select="concat('&tab;&tab;base found in &quot;',$doc,'&quot;&nl;')" />
						</xsl:message-->
						<xsl:if test="string-length($doc) != 0">
							<xsl:apply-templates mode="visitor-present" select="document($doc,.)/lsd:lsd/lsd:class[@name = $base-class-name]">
								<xsl:with-param name="name" select="$name"/>
							</xsl:apply-templates>
						</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template mode="under-cut" match="lsd:class">
		<xsl:param name="cut-class"/>
<!--		<xsl:value-of select="concat('{{',@name,'}}{{',$cut-class,'}}')"/> -->
		<xsl:choose>
			<xsl:when test="@name = $cut-class">	<!-- is it me ? -->
				<xsl:text>yes</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="base-class-name">
					<xsl:call-template name="stripped-base-in-doc"/>
				</xsl:variable>
				<xsl:choose>
					<xsl:when test="document('',.)/lsd:lsd/lsd:class[@name = $base-class-name]">
						<xsl:apply-templates mode="under-cut" select="document('',.)/lsd:lsd/lsd:class[@name = $base-class-name]">
							<xsl:with-param name="cut-class" select="$cut-class"/>
						</xsl:apply-templates>
					</xsl:when>
					<xsl:otherwise>
						<xsl:variable name="doc">
							<xsl:call-template name="class-lookup">
								<xsl:with-param name="class" select="$base-class-name"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:if test="string-length($doc) != 0">
							<xsl:apply-templates mode="under-cut" select="document($doc,.)/lsd:lsd/lsd:class[@name = $base-class-name]">
								<xsl:with-param name="cut-class" select="$cut-class"/>
							</xsl:apply-templates>
						</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="stripped-base-in-doc">
		<!-- returns the (unqualified!) name of the class, which current class is derived from,
			document given in doc parameter is searched
		-->
		<xsl:param name="doc" />
		<xsl:choose>
			<xsl:when test="string-length(@base)">
				<xsl:value-of select="@base"/>			<!-- specified base -->
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="document($doc,.)/lsd:lsd/lsd:default-base/@type" />	<!-- or default base -->
			</xsl:otherwise>
		</xsl:choose>
 	</xsl:template>

</xsl:transform>
