<?xml version="1.0"?>
<!--
	The lestes compiler suite
	Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
	Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
	Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
	Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
	Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
	Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
	Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; version 2 of the License.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	See the full text of the GNU General Public License version 2, and
	the limitations in the file doc/LICENSE.

	By accepting the license the licensee waives any and all claims
	against the copyright holder(s) related in whole or in part to the
	work, its use, and/or the inability to use it.
-->
<!DOCTYPE xsl:transform [
    <!ENTITY nl "&#x0A;">
    <!ENTITY tab "&#x09;">
]>
<xsl:transform version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:lsd="http://lestes.jikos.cz/schemas/lsd">
<!-- Check LSD file for undeclared types -->
    <xsl:output method="text"/>
    <xsl:key name="collectible" match="/lsd:lsd/lsd:class | /lsd:lsd/lsd:forward-class | /lsd:lsd/lsd:using-class | /lsd:lsd:lsd:foreign-class" use="@name"/>

    <xsl:template match="lsd:lsd">
	<xsl:for-each select="*//@type|lsd:class/@base">
	    <xsl:if test="not(key('collectible', . ))">
		<xsl:value-of select="."/>
		<xsl:text>&nl;</xsl:text>
	    </xsl:if>
	</xsl:for-each>
    </xsl:template>

</xsl:transform>
