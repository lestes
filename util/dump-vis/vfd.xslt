<?xml version="1.0"?>
<!--
	The lestes compiler suite
	Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
	Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
	Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
	Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
	Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
	Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
	Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; version 2 of the License.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	See the full text of the GNU General Public License version 2, and
	the limitations in the file doc/LICENSE.

	By accepting the license the licensee waives any and all claims
	against the copyright holder(s) related in whole or in part to the
	work, its use, and/or the inability to use it.
-->
<!DOCTYPE xsl:transform [
	<!ENTITY nl "&#x0A;">
	<!ENTITY tab "&#x09;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:lsd="http://lestes.jikos.cz/schemas/lsd" xmlns:d="http://lestes.jikos.cz/schemas/dump">
	<!-- Generate C++ headers for data structures from XML description. -->
	<xsl:output method="xml" encoding="utf-8"
		doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
		doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
		/>

	<xsl:key name="obj" match="/d:dump/d:instance" use="@id"/>

	<xsl:template match="/">
		<!--
		<xsl:for-each select="lsd:lsd/lsd:class/lsd:collection[count(@kind) = 0]">
			<xsl:value-of select="concat('&tab;',../@name,'/',@name,'&nl;')" />
		</xsl:for-each>
		-->
		<!--
		<xsl:for-each select="lsd:lsd/lsd:class[count(@base) = 0]">
			<xsl:value-of select="concat(@name,'&nl;')" />
		</xsl:for-each>
		-->
		<html>
			<head>
				<title>
					<xsl:text>Visualized dump</xsl:text>
				</title>
				<style type="text/css">
					<xsl:text>
						.instance {
						border: thin solid black ;
						}
						.head {
						background-color: #aaf ;
						}
					</xsl:text>
				</style>
			</head>
			<body>
				<xsl:apply-templates select="d:dump/d:instance" />
			</body>
		</html>
	</xsl:template>

	<xsl:template match="d:instance">
		<div class="instance">
			<xsl:attribute name="id">
				<xsl:value-of select="@id"/>
			</xsl:attribute>
			<div class="head">
				<span class="id"><xsl:value-of select="@id"/></span>
				<span class="colon"><xsl:text>:</xsl:text></span>
				<span class="type"><xsl:value-of select="@type"/></span>
			</div>
			<div class="body">
				<xsl:apply-templates select="d:field" />
			</div>
		</div>
	</xsl:template>

	<xsl:template match="d:field">
		<div class="field">
			<span class="fname"><xsl:value-of select="@name"/></span>
			<span class="colon"><xsl:text>:</xsl:text></span>
			<span class="ftype"><xsl:value-of select="@type"/></span>
			<span class="eq"><xsl:text>=</xsl:text></span>
			<span class="fval">
				<xsl:apply-templates />
				<xsl:if test="d:pointer and count(key('obj',current()/d:pointer/@idref)/d:field) = 0">
					<xsl:text> [empty]</xsl:text>
				</xsl:if>
				<xsl:if test="string(key('obj', current()/d:pointer/@idref)/@type) = 'ss_ordinary_name'">
					<xsl:value-of select="concat(' [name = &quot;', key('obj', current()/d:pointer/@idref)/d:field[@name='name']/d:simple/@value, '&quot;]')" />
				</xsl:if>
			</span>
		</div>
	</xsl:template>

	<xsl:template match="d:simple">
		<xsl:value-of select="concat('(',@type,') ',@value)" />
	</xsl:template>

	<xsl:template match="d:null">
		<xsl:text>NULL</xsl:text>
	</xsl:template>

	<xsl:template match="d:nondumpable">
		<xsl:text>This value cannot be dumped.</xsl:text>
	</xsl:template>

	<xsl:template match="d:after-barrier">
		<xsl:text>This value is after barrier.</xsl:text>
	</xsl:template>

	<xsl:template match="d:pointer">
		<a>
			<xsl:attribute name="href">
				<xsl:value-of select="concat('#', @idref)" />
			</xsl:attribute>
			<xsl:value-of select="concat('(',key('obj', current()/@idref)/@type,' *) ',@idref)" />
		</a>
	</xsl:template>

</xsl:stylesheet>
