<?xml version="1.0"?>
<!--
	The lestes compiler suite
	Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
	Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
	Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
	Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
	Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
	Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
	Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; version 2 of the License.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	See the full text of the GNU General Public License version 2, and
	the limitations in the file doc/LICENSE.

	By accepting the license the licensee waives any and all claims
	against the copyright holder(s) related in whole or in part to the
	work, its use, and/or the inability to use it.
-->
<!DOCTYPE xsl:transform [
	<!ENTITY nl "&#x0A;">
	<!ENTITY tab "&#x09;">
]>

<!-- Use  xinclude parameter to run xslt template-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:lsd="http://lestes.jikos.cz/schemas/lsd" xmlns:d="http://lestes.jikos.cz/schemas/dump" xmlns:dia="http://www.lysator.liu.se/~alla/dia/">
	<!-- Generate C++ headers for data structures from XML description. -->
	<xsl:output method="xml" encoding="utf-8" indent="yes" />
	<!--doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
	doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
	/-->

	<xsl:template name="diagramdata">
		<!-- dia header -->
		<dia:diagramdata>
			<dia:attribute name="background">
				<dia:color val="#ffffff"/>
			</dia:attribute>
			<dia:attribute name="pagebreak">
				<dia:color val="#000099"/>
			</dia:attribute>
			<dia:attribute name="paper">
				<dia:composite type="paper">
					<dia:attribute name="name">
						<dia:string>#A4#</dia:string>
					</dia:attribute>
					<dia:attribute name="tmargin">
						<dia:real val="2.8222"/>
					</dia:attribute>
					<dia:attribute name="bmargin">
						<dia:real val="2.8222"/>
					</dia:attribute>
					<dia:attribute name="lmargin">
						<dia:real val="2.8222"/>
					</dia:attribute>
					<dia:attribute name="rmargin">
						<dia:real val="2.8222"/>
					</dia:attribute>
					<dia:attribute name="is_portrait">
						<dia:boolean val="false"/>
					</dia:attribute>
					<dia:attribute name="scaling">
						<dia:real val="0.2"/>
					</dia:attribute>
					<dia:attribute name="fitto">
						<dia:boolean val="false"/>
					</dia:attribute>
				</dia:composite>
			</dia:attribute>
			<dia:attribute name="grid">
				<dia:composite type="grid">
					<dia:attribute name="width_x">
						<dia:real val="1"/>
					</dia:attribute>
					<dia:attribute name="width_y">
						<dia:real val="1"/>
					</dia:attribute>
					<dia:attribute name="visible_x">
						<dia:int val="1"/>
					</dia:attribute>
					<dia:attribute name="visible_y">
						<dia:int val="1"/>
					</dia:attribute>
					<dia:composite type="color"/>
				</dia:composite>
			</dia:attribute>
			<dia:attribute name="color">
				<dia:color val="#d8e5e5"/>
			</dia:attribute>
			<dia:attribute name="guides">
				<dia:composite type="guides">
					<dia:attribute name="hguides"/>
					<dia:attribute name="vguides"/>
				</dia:composite>
			</dia:attribute>
		</dia:diagramdata>
	</xsl:template>

	<xsl:template match="/">
		<dia:diagram>
			<xsl:call-template name="diagramdata" />
			<dia:layer name="Background" visible="true">
				<xsl:apply-templates select="lsd:lsd//lsd:class[string(@abstract) != 'yes']" />
			</dia:layer>
		</dia:diagram>
	</xsl:template>

	<xsl:template match="lsd:class">
		<dia:object type="UML - Class" version="0">
			<xsl:attribute name="id">
				<xsl:value-of select="concat('O',position())" />
			</xsl:attribute>
			<dia:attribute name="obj_pos">
				<dia:point>
					<xsl:attribute name="val">
						<xsl:value-of select="concat(position()*10-10, ',0')" />
					</xsl:attribute>
				</dia:point>
			</dia:attribute>
			<dia:attribute name="obj_bb">
				<dia:rectangle>
					<xsl:attribute name="val">
						<xsl:value-of select="concat(position()*10-10.05, ',-0.05;',position()*10-0.95,',9.05')" />
					</xsl:attribute>
				</dia:rectangle>
			</dia:attribute>
			<dia:attribute name="elem_corner">
				<dia:point>
					<xsl:attribute name="val">
						<xsl:value-of select="concat(position()*10-10, ',0')" />
					</xsl:attribute>
				</dia:point>
			</dia:attribute>
			<dia:attribute name="elem_width">
				<dia:real val="9.0"/>
			</dia:attribute>
			<dia:attribute name="elem_height">
				<dia:real val="9.0"/>
			</dia:attribute>
			<dia:attribute name="name">
				<dia:string>
					<xsl:value-of select="concat('#',@name,'#')" />
				</dia:string>
			</dia:attribute>
			<dia:attribute name="stereotype">
				<dia:string>##</dia:string>
			</dia:attribute>
			<dia:attribute name="comment">
				<dia:string>##</dia:string>
			</dia:attribute>
			<dia:attribute name="abstract">
				<dia:boolean val="false"/>
			</dia:attribute>
			<dia:attribute name="suppress_attributes">
				<dia:boolean val="false"/>
			</dia:attribute>
			<dia:attribute name="suppress_operations">
				<dia:boolean val="false"/>
			</dia:attribute>
			<dia:attribute name="visible_attributes">
				<dia:boolean val="true"/>
			</dia:attribute>
			<dia:attribute name="visible_operations">
				<dia:boolean val="false"/>
			</dia:attribute>
			<dia:attribute name="visible_comments">
				<dia:boolean val="true"/>
			</dia:attribute>
			<dia:attribute name="line_color">
				<dia:color val="#000000"/>
			</dia:attribute>
			<dia:attribute name="fill_color">
				<dia:color val="#ffffff"/>
			</dia:attribute>
			<dia:attribute name="text_color">
				<dia:color val="#000000"/>
			</dia:attribute>
			<dia:attribute name="normal_font">
				<dia:font family="monospace" style="0" name="Courier"/>
			</dia:attribute>
			<dia:attribute name="abstract_font">
				<dia:font family="monospace" style="88" name="Courier"/>
			</dia:attribute>
			<dia:attribute name="polymorphic_font">
				<dia:font family="monospace" style="8" name="Courier"/>
			</dia:attribute>
			<dia:attribute name="classname_font">
				<dia:font family="sans" style="80" name="Helvetica"/>
			</dia:attribute>
			<dia:attribute name="abstract_classname_font">
				<dia:font family="sans" style="88" name="Helvetica"/>
			</dia:attribute>
			<dia:attribute name="comment_font">
				<dia:font family="sans" style="8" name="Helvetica"/>
			</dia:attribute>
			<dia:attribute name="font_height">
				<dia:real val="0.8"/>
			</dia:attribute>
			<dia:attribute name="polymorphic_font_height">
				<dia:real val="0.8"/>
			</dia:attribute>
			<dia:attribute name="abstract_font_height">
				<dia:real val="1"/>
			</dia:attribute>
			<dia:attribute name="classname_font_height">
				<dia:real val="1.2"/>
			</dia:attribute>
			<dia:attribute name="abstract_classname_font_height">
				<dia:real val="1.2"/>
			</dia:attribute>
			<dia:attribute name="comment_font_height">
				<dia:real val="0.8"/>
			</dia:attribute>
			<dia:attribute name="attributes">
				<xsl:apply-templates mode="class-attr" select="." />
			</dia:attribute>
			<dia:attribute name="operations"/>
			<dia:attribute name="template">
				<dia:boolean val="false"/>
			</dia:attribute>
			<dia:attribute name="templates"/>
		</dia:object>
	</xsl:template>

	<xsl:template mode="class-attr" match="lsd:class">
		<xsl:apply-templates mode="class-attr" select="(//lsd:class[@name=current()/@base])[position()=1]" />
		<!--
		<xsl:comment>
			<xsl:value-of select="concat('attrs from class ',@name)" />
		</xsl:comment>
		-->
		<xsl:apply-templates mode="class-attr" select="lsd:field | lsd:collection" />
	</xsl:template>

	<xsl:template mode="class-attr" match="lsd:field">
		<dia:composite type="umlattribute">
			<dia:attribute name="name">
				<dia:string>
					<xsl:value-of select="concat('#',@name,'#')" />
				</dia:string>
			</dia:attribute>
			<dia:attribute name="type">
				<dia:string>
					<xsl:value-of select="concat('#',@type,'#')" />
				</dia:string>
			</dia:attribute>
			<dia:attribute name="value">
				<dia:string>##</dia:string>
			</dia:attribute>
			<dia:attribute name="comment">
				<dia:string>##</dia:string>
			</dia:attribute>
			<dia:attribute name="visibility">
				<dia:enum val="0"/>
			</dia:attribute>
			<dia:attribute name="abstract">
				<dia:boolean val="false"/>
			</dia:attribute>
			<dia:attribute name="class_scope">
				<dia:boolean val="false"/>
			</dia:attribute>
		</dia:composite>
	</xsl:template>
	<xsl:template mode="class-attr" match="lsd:collection">
		<dia:composite type="umlattribute">
			<dia:attribute name="name">
				<dia:string>
					<xsl:value-of select="concat('#',@name,'#')" />
				</dia:string>
			</dia:attribute>
			<dia:attribute name="type">
				<dia:string>
					<xsl:value-of select="concat('#LIST&lt;',@type,'&gt;#')" />
				</dia:string>
			</dia:attribute>
			<dia:attribute name="value">
				<dia:string>##</dia:string>
			</dia:attribute>
			<dia:attribute name="comment">
				<dia:string>##</dia:string>
			</dia:attribute>
			<dia:attribute name="visibility">
				<dia:enum val="0"/>
			</dia:attribute>
			<dia:attribute name="abstract">
				<dia:boolean val="false"/>
			</dia:attribute>
			<dia:attribute name="class_scope">
				<dia:boolean val="false"/>
			</dia:attribute>
		</dia:composite>
	</xsl:template>

</xsl:stylesheet>
