#! /usr/bin/perl
#
# The lestes compiler suite
# Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
# Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
# Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
# Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
# Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
# Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
# Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# See the full text of the GNU General Public License version 2, and
# the limitations in the file doc/LICENSE.
#
# By accepting the license the licensee waives any and all claims
# against the copyright holder(s) related in whole or in part to the
# work, its use, and/or the inability to use it.
#

# howto run THIS SCRIPT (by jaz)
# cd $LESTES_ROOT
# make all-obj.nm
# ./util/objects_to_link.pl < all-obj.nm $RELATIVE_PATH_TO_OBJECT_WITH_MAIN

use strict;
use warnings;

# undefined symbols
my %symbols_undef;
# defined symbols of an object
my %symbols_def;
# where is a symbol defined
my %dsymbol;

my ($startfile,$filename);

#$startfile = <>;
#chomp $startfile;

while (<STDIN>) {
	chomp;
	if (/(.*):$/o) {
		$filename = $1;
		$symbols_def{$filename} = [];
		$symbols_undef{$filename} = [];
		next;
	}
	next unless $_;
	my ($type, $sym) = (/\S*\s*(\S) (.*)/o);
	push @{$symbols_undef{$filename}}, $sym if $type eq 'U';
	push @{$symbols_def{$filename}}, $sym unless $type eq 'U';
	$dsymbol{$sym} = $filename unless $type eq 'U';
}

# the undefined symbols
for my $startfile (@ARGV) {
	unless (defined $symbols_undef{$startfile}) {
		print "# Object file ``$startfile'' not found.\n";
		next;
	}
	my @undefs = (@{$symbols_undef{$startfile}});
	my @objects = ($startfile);

	my %resolved;
	$resolved{$_} = 1 for @{$symbols_def{$startfile}};

	while (defined ($_ = shift @undefs)) {
		unless ($resolved{$_}) {
			# file resolving this symbol
			my $rfile = $dsymbol{$_};
			if ($rfile) {
				push @objects, $rfile;
				$resolved{$_} = 1 for @{$symbols_def{$rfile}};
				push @undefs, @{$symbols_undef{$rfile}};
			}
		}
	}

	$_ = $startfile;
	s!.*/!!o;
	s!\.o!!o;

	$, = " \\\n\t";
	print "$_.objects = ", sort(@objects), "# END OF $_.objects\n"
}
