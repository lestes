/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes___host_types_hh___included
#define lestes___host_types_hh___included

/*! \file
  Definition of data types on host machine for i686.
  \todo pt Only the host machine estimate.
  \todo pt There shoulde be macros to check whether certain types
  really exist to allow substitution for helper classes.
  \author pt
*/
  
#include <lestes/package.hh>

package(lestes);

typedef signed char         lc_host_int_least8;
typedef lc_host_int_least8  lc_host_int_least1;
typedef lc_host_int_least8  lc_host_int_least2;
typedef lc_host_int_least8  lc_host_int_least3;
typedef lc_host_int_least8  lc_host_int_least4;
typedef lc_host_int_least8  lc_host_int_least5;
typedef lc_host_int_least8  lc_host_int_least6;
typedef lc_host_int_least8  lc_host_int_least7;

typedef signed short        lc_host_int_least16;
typedef lc_host_int_least16 lc_host_int_least9;
typedef lc_host_int_least16 lc_host_int_least10;
typedef lc_host_int_least16 lc_host_int_least11;
typedef lc_host_int_least16 lc_host_int_least12;
typedef lc_host_int_least16 lc_host_int_least13;
typedef lc_host_int_least16 lc_host_int_least14;
typedef lc_host_int_least16 lc_host_int_least15;

typedef signed int          lc_host_int_least32;
typedef lc_host_int_least32 lc_host_int_least17;
typedef lc_host_int_least32 lc_host_int_least18;
typedef lc_host_int_least32 lc_host_int_least19;
typedef lc_host_int_least32 lc_host_int_least20;
typedef lc_host_int_least32 lc_host_int_least21;
typedef lc_host_int_least32 lc_host_int_least22;
typedef lc_host_int_least32 lc_host_int_least23;
typedef lc_host_int_least32 lc_host_int_least24;
typedef lc_host_int_least32 lc_host_int_least25;
typedef lc_host_int_least32 lc_host_int_least26;
typedef lc_host_int_least32 lc_host_int_least27;
typedef lc_host_int_least32 lc_host_int_least28;
typedef lc_host_int_least32 lc_host_int_least29;
typedef lc_host_int_least32 lc_host_int_least30;
typedef lc_host_int_least32 lc_host_int_least31;

// TODO pt I don't know the macro syntax ...
#ifdef __HAVE_LONG_LONG
typedef signed long long    lc_host_int_least64;
typedef lc_host_int_least64 lc_host_int_least33;
typedef lc_host_int_least64 lc_host_int_least34;
typedef lc_host_int_least64 lc_host_int_least35;
typedef lc_host_int_least64 lc_host_int_least36;
typedef lc_host_int_least64 lc_host_int_least37;
typedef lc_host_int_least64 lc_host_int_least38;
typedef lc_host_int_least64 lc_host_int_least39;
typedef lc_host_int_least64 lc_host_int_least40;
typedef lc_host_int_least64 lc_host_int_least41;
typedef lc_host_int_least64 lc_host_int_least42;
typedef lc_host_int_least64 lc_host_int_least43;
typedef lc_host_int_least64 lc_host_int_least44;
typedef lc_host_int_least64 lc_host_int_least45;
typedef lc_host_int_least64 lc_host_int_least46;
typedef lc_host_int_least64 lc_host_int_least47;
typedef lc_host_int_least64 lc_host_int_least48;
typedef lc_host_int_least64 lc_host_int_least49;
typedef lc_host_int_least64 lc_host_int_least50;
typedef lc_host_int_least64 lc_host_int_least51;
typedef lc_host_int_least64 lc_host_int_least52;
typedef lc_host_int_least64 lc_host_int_least53;
typedef lc_host_int_least64 lc_host_int_least54;
typedef lc_host_int_least64 lc_host_int_least55;
typedef lc_host_int_least64 lc_host_int_least56;
typedef lc_host_int_least64 lc_host_int_least57;
typedef lc_host_int_least64 lc_host_int_least58;
typedef lc_host_int_least64 lc_host_int_least59;
typedef lc_host_int_least64 lc_host_int_least60;
typedef lc_host_int_least64 lc_host_int_least61;
typedef lc_host_int_least64 lc_host_int_least62;
typedef lc_host_int_least64 lc_host_int_least63;
#endif

typedef unsigned char        lc_host_uint_least8;
typedef lc_host_uint_least8  lc_host_uint_least1;
typedef lc_host_uint_least8  lc_host_uint_least2;
typedef lc_host_uint_least8  lc_host_uint_least3;
typedef lc_host_uint_least8  lc_host_uint_least4;
typedef lc_host_uint_least8  lc_host_uint_least5;
typedef lc_host_uint_least8  lc_host_uint_least6;
typedef lc_host_uint_least8  lc_host_uint_least7;

typedef unsigned short       lc_host_uint_least16;
typedef lc_host_uint_least16 lc_host_uint_least9;
typedef lc_host_uint_least16 lc_host_uint_least10;
typedef lc_host_uint_least16 lc_host_uint_least11;
typedef lc_host_uint_least16 lc_host_uint_least12;
typedef lc_host_uint_least16 lc_host_uint_least13;
typedef lc_host_uint_least16 lc_host_uint_least14;
typedef lc_host_uint_least16 lc_host_uint_least15;

typedef unsigned int         lc_host_uint_least32;
typedef lc_host_uint_least32 lc_host_uint_least17;
typedef lc_host_uint_least32 lc_host_uint_least18;
typedef lc_host_uint_least32 lc_host_uint_least19;
typedef lc_host_uint_least32 lc_host_uint_least20;
typedef lc_host_uint_least32 lc_host_uint_least21;
typedef lc_host_uint_least32 lc_host_uint_least22;
typedef lc_host_uint_least32 lc_host_uint_least23;
typedef lc_host_uint_least32 lc_host_uint_least24;
typedef lc_host_uint_least32 lc_host_uint_least25;
typedef lc_host_uint_least32 lc_host_uint_least26;
typedef lc_host_uint_least32 lc_host_uint_least27;
typedef lc_host_uint_least32 lc_host_uint_least28;
typedef lc_host_uint_least32 lc_host_uint_least29;
typedef lc_host_uint_least32 lc_host_uint_least30;
typedef lc_host_uint_least32 lc_host_uint_least31;

#ifdef __HAVE_LONG_LONG
typedef unsigned long long   lc_host_uint_least64;
typedef lc_host_uint_least64 lc_host_uint_least33;
typedef lc_host_uint_least64 lc_host_uint_least34;
typedef lc_host_uint_least64 lc_host_uint_least35;
typedef lc_host_uint_least64 lc_host_uint_least36;
typedef lc_host_uint_least64 lc_host_uint_least37;
typedef lc_host_uint_least64 lc_host_uint_least38;
typedef lc_host_uint_least64 lc_host_uint_least39;
typedef lc_host_uint_least64 lc_host_uint_least40;
typedef lc_host_uint_least64 lc_host_uint_least41;
typedef lc_host_uint_least64 lc_host_uint_least42;
typedef lc_host_uint_least64 lc_host_uint_least43;
typedef lc_host_uint_least64 lc_host_uint_least44;
typedef lc_host_uint_least64 lc_host_uint_least45;
typedef lc_host_uint_least64 lc_host_uint_least46;
typedef lc_host_uint_least64 lc_host_uint_least47;
typedef lc_host_uint_least64 lc_host_uint_least48;
typedef lc_host_uint_least64 lc_host_uint_least49;
typedef lc_host_uint_least64 lc_host_uint_least50;
typedef lc_host_uint_least64 lc_host_uint_least51;
typedef lc_host_uint_least64 lc_host_uint_least52;
typedef lc_host_uint_least64 lc_host_uint_least53;
typedef lc_host_uint_least64 lc_host_uint_least54;
typedef lc_host_uint_least64 lc_host_uint_least55;
typedef lc_host_uint_least64 lc_host_uint_least56;
typedef lc_host_uint_least64 lc_host_uint_least57;
typedef lc_host_uint_least64 lc_host_uint_least58;
typedef lc_host_uint_least64 lc_host_uint_least59;
typedef lc_host_uint_least64 lc_host_uint_least60;
typedef lc_host_uint_least64 lc_host_uint_least61;
typedef lc_host_uint_least64 lc_host_uint_least62;
typedef lc_host_uint_least64 lc_host_uint_least63;
#endif

end_package(lestes);

#endif
/* vim: set ft=lestes : */
