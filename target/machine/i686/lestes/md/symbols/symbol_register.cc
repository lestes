/*!
	\file
	\brief Symbol register.
	\author jaz
*/

#include <lestes/md/symbols/symbol_register.g.hh>
#include <lestes/md/symbols/name_mangler.g.hh>
#include <lestes/md/tasm/tm_asm.mdg.hh>
#include <lestes/backend_v2/intercode/pi.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>
#include <lestes/lang/cplus/sem/ss_statement.g.hh>
#include <lestes/lang/cplus/sem/ss_decl2ulint_base.g.hh>


package(lestes);
package(md);
package(symbols);

using namespace ::lestes::lang::cplus::sem;
using ::lestes::md::symbols::name_mangler;
using ::lestes::md::tasm::tm_asm;

typedef set<srp<ss_declaration> > decl_set_type;

ptr<symbol_register> symbol_register::instance() {
	if ( !singleton_instance_get() ) {
		singleton_instance_set(symbol_register::create());
	}
	return singleton_instance_get();
}

/*!
	\brief Registers symbol.
	
	\param symbol The symbol.
*/
void symbol_register::register_symbol(ptr<ss_declaration> symbol) {
	ptr<ss_linkage> linkage = symbol->linkage_get();
	
	if ( linkage->linkage_get()==ss_linkage::LINKAGE_INTERNAL ) {
		/*
			symbol has internal linkage
		*/
		symbol_register::instance()->register_internal(symbol);
	} else if ( linkage->linkage_get()==ss_linkage::LINKAGE_EXTERNAL ) {
		/*
			symbol with external linkage. it can be global or just external.
		*/
		ss_decl2id::id_type id = (ss_decl2id::id_type)symbol->accept_ss_decl2ulint_base(ss_decl2id::instance());
		
		if ( id==ss_decl2id::FUNCTION || id==ss_decl2id::METHOD || id==ss_decl2id::BUILTIN_OPERATOR) {
			if ( symbol.dncast<ss_function_declaration>()->body_get() ) {
				/*
					function with body defined in this translation unit.
				*/
				symbol_register::instance()->register_global(symbol);
			} else {
				/*
					function without body defined in this TU.
				*/
				symbol_register::instance()->register_external(symbol);
			}
		} else {
			symbol_register::instance()->register_global(symbol);
		}
		
	} // else NO_LINKAGE
}

/*!
	\brief Registers external symbol.
	
	\param symbol The symbol.
*/
void symbol_register::register_external(ptr<ss_declaration> symbol) {
	externals->insert(symbol);
}

/*!
	\brief Registers internal symbol.
	
	\param symbol The symbol.
*/
void symbol_register::register_global(ptr<ss_declaration> symbol) {
	globals->insert(symbol);
}

/*!
	\brief Registers internal symbol.
	
	\param symbol The symbol.
*/
void symbol_register::register_internal(ptr<ss_declaration> symbol) {
	internals->insert(symbol);
}


/*!
	\brief Emits global delarations for registered symbols.
	
	\param output Output.
*/
void symbol_register::emit_global_declarations(::std::ostream& output) {
	decl_set_type::iterator it;
	
	for(it=externals->begin(); it!=externals->end(); it++) {
		output << string_replace(tm_asm::ent_extern_symbol_decl_get(),"$name",name_mangler::instance()->mangle(*it));
	}
	
	for(it=globals->begin(); it!=globals->end(); it++) {
		output << string_replace(tm_asm::ent_global_symbol_decl_get(),"$name",name_mangler::instance()->mangle(*it));
	}
	
	for(it=internals->begin(); it!=internals->end(); it++) {
		output << string_replace(tm_asm::ent_internal_symbol_decl_get(),"$name",name_mangler::instance()->mangle(*it));
	}
}

end_package(symbols);
end_package(md);
end_package(lestes);

