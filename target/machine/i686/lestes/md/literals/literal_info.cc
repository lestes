/*!
	\file
	\brief Informations about pi_literal value.
	\author jaz
*/
#include <lestes/std/ucn_string.hh>
#include <lestes/md/types/tm_data_type.g.hh>
#include "literal_info.g.hh"

package(lestes);
package(md);
package(literals);

typedef list<srp<literal_info_base> > lit_list_type;

/*!
	\brief Returns asm string representing value of the literal.
*/
lstring li_compound::get_asm_definition_val() {
	::std::ostringstream oss;
	
	lassert(items_get()->size()>0);
	
	lit_list_type::iterator it;
	it=items_get()->begin();
	
	oss << (*it)->get_asm_definition_val();
	it++;
	for(; it!=items_get()->end(); it++) {
		oss <<	',' << (*it)->get_asm_definition_val();
	}	
	
	return oss.str();
}

/*!
	\brief Returns string representing datatype of the literal.
*/
lstring li_compound::get_asm_definition_type() {
	return (*items_get()->begin())->get_asm_definition_type();
}


/*!
	\brief Returns asm string representing value of the literal.
*/
lstring li_simple::get_asm_definition_val() {
	::std::ostringstream oss;
	oss << data_get();
	return oss.str();
}

/*!
	\brief Returnd asm string representing type of the literal.
*/
lstring li_simple::get_asm_definition_type() {
	return type_get()->asm_decl_get();
}


end_package(literals);
end_package(md);
end_package(lestes);

