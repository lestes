/*!
	\file
	\brief Literal loader.
	\author jaz
*/

#include <lestes/md/types/tm_data_type_base.g.hh>
#include <lestes/md/tasm/tm_asm.mdg.hh>
#include <lestes/md/mem/memory_allocators.g.hh>
#include <lestes/md/literals/literal_info_base.g.hh>
#include <lestes/backend_v2/intercode/pi.g.hh>
#include <lestes/backend_v2/intercode/pi_mem_factory.g.hh>

#include "literal_loader.g.hh"

package(lestes);
package(md);
package(literals);

using namespace ::lestes::backend_v2::intercode;

using ::lestes::md::tasm::tm_asm;

typedef map<srp<pi_lit>, ulint> lit2ulint_map_type;
typedef list<srp<pi_pi> > pi_list_type;

/*!
	\brief Returns instance.
*/
ptr<literal_loader> literal_loader::instance() {
	if ( !singleton_instance ) {
		//An singleton instance has not been created yet. Create it!
		singleton_instance_set(literal_loader::create());
	}
	//Return instance.
	return singleton_instance;
}

/*!
	\brief Tells whether a literal is managed.
	
	\param lit The literal.
	\return True if literal is managed. False otherwise.
*/
bool literal_loader::is_managed(ptr<pi_lit> lit) {
	if ( managed_literals->find(lit)!=managed_literals->end() ) {
		return true;
	}
	return false;
}

/*!
	\brief Returns id of managed literal.
	
	\param lit The literal.
	\return ID of the literal if the literal is managed. Empty string otherwise.
*/
lstring literal_loader::get_id_for_managed_literal(ptr<pi_lit> lit) {
	lassert(lit);

	//Find literal id.	
	lit2ulint_map_type::iterator it = managed_literals->find(lit);
	
	if ( it==managed_literals->end() ) {
		return lstring();
	}
	
	::std::ostringstream oss;
	oss << it->second;
	
	//Get template for literal name.
	lstring code = tm_asm::ent_literal_get();
	
	//Return literal id.
	return string_replace(code,"$id",oss.str());	
}


/*!
	\brief Emits declarations for managed literals to a stream.
	
	\param output The stream.
*/
void literal_loader::emit_global_declarations(::std::ostream& output) {
	//Go through managed literals and emit global declaration for them.
	lit2ulint_map_type::iterator it;
	for(it=managed_literals->begin(); it!=managed_literals->end(); it++) {
		//Get template for global declaration.
		lstring code = tm_asm::ent_global_simple_decl_get();
		
		//Get literal id.
		lstring id = get_id_for_managed_literal(it->first);
		
		//Get literal type.
		lstring type = it->first->value_get()->get_asm_definition_type();
		
		//Get literal initial value.
		lstring value = it->first->value_get()->get_asm_definition_val();
		
		code = string_replace(code,"$id",id);
		code = string_replace(code,"$type",type);
		code = string_replace(code, "$value", value);
		
		//Emit declaration to output stream.
		output << code;
	}
}

/*!
	\brief Generates list of pseudoinstructions that loads literal to a pseudoregister.
		
	Note: The function sets psp, nsp and level for generated pseudoinstruction according to given psp and nsp
	in order to place them between these sequencepoints.
	
	\param preg The destination pseudoregister for literal.
	\param lit The literal.
	\param psp The first boundary sequencepoint.
	\param nsp The second boundary sequencepoint.
	\return The list of pseudoinstructions.
*/
ptr<pi_list_type> literal_loader::generate_load(ptr<pi_preg> preg, ptr<pi_lit> lit, ptr<pi_sp> psp, ptr<pi_sp> nsp) {
	ptr<pi_list_type> pi_out_list = pi_list_type::create();
	
	//Compute level for load instruction.
	ulint level = (psp->level_get() > nsp->level_get() ? psp->level_get() : nsp->level_get()) + 1;
	
	//Get kind of literal info.
	literal_info_base::kind_type li_kind = lit->value_get()->kind_get();
	
	if ( li_kind==literal_info_base::SIMPLE ) {
		//Simple literal. It is loaded as immidiate.
		ptr<pi_ldi> ldi = pi_ldi::create(psp, nsp, level,preg,lit,lit->type_get());
		preg->origin_set(ldi);
		pi_out_list->push_back(ldi);
	} else {
		//Compound literal. Global declaration will be created and literal will be loaded as its address,
		ptr<pi_mem> mem = pi_mf_lit::create(NULL, lit->type_get(),lit)->get_mem(NULL);
		ptr<pi_lda> ld = pi_lda::create(psp, nsp, level,preg,mem, lit->type_get(), lit->type_get());
		mem->origin_set(psp);
		preg->origin_set(ld);
		pi_out_list->push_back(ld);
		
		//Register literal as managed literal.
		managed_literals->insert(::std::pair<srp<pi_lit>, ulint>(lit, managed_literals->size()));
	}
	
	return pi_out_list;
}

end_package(literals);
end_package(md);
end_package(lestes);

