/*!
	\file
	\author jaz
*/
#include <lestes/md/instructions/pi_operand2ge_operand.g.hh>
#include <lestes/backend_v2/intercode/pi.g.hh>
#include <lestes/backend_v2/intercode/ge.g.hh>
#include <lestes/backend_v2/intercode/pi_mem_factory.g.hh>

package(lestes);
package(md);
package(instructions);

using namespace ::lestes::backend_v2::intercode;

ptr< ::lestes::backend_v2::intercode::ge_operand > pi_operand2ge_operand::visit_pi_preg(ptr< ::lestes::backend_v2::intercode::pi_preg > pi) {
	return ge_operand_reg::create(pi->type_get(), NULL, pi);
}

ptr< ::lestes::backend_v2::intercode::ge_operand > pi_operand2ge_operand::visit_pi_lit(ptr< ::lestes::backend_v2::intercode::pi_lit > pi) {
	return ge_operand_imm::create(pi->type_get(), NULL, pi, pi->value_get());
}

ptr< ::lestes::backend_v2::intercode::ge_operand > pi_operand2ge_operand::visit_pi_mem_decl(ptr< ::lestes::backend_v2::intercode::pi_mem_decl > pi) {
	return ge_operand_mem::create(pi->type_get(), NULL, pi, pi->factory_get());
}

ptr< ::lestes::backend_v2::intercode::ge_operand > pi_operand2ge_operand::visit_pi_mem_lit(ptr< ::lestes::backend_v2::intercode::pi_mem_lit > pi){
	return ge_operand_mem::create(pi->type_get(), NULL, pi, pi->factory_get());
}

ptr< ::lestes::backend_v2::intercode::ge_operand > pi_operand2ge_operand::visit_pi_mem_member(ptr< ::lestes::backend_v2::intercode::pi_mem_member > pi){
	return ge_operand_mem::create(pi->type_get(), NULL, pi, pi->factory_get());
}

ptr< ::lestes::backend_v2::intercode::ge_operand > pi_operand2ge_operand::visit_pi_mem_stack(ptr< ::lestes::backend_v2::intercode::pi_mem_stack > pi){
	return ge_operand_mem::create(pi->type_get(), NULL, pi, pi->factory_get());
}

ptr< ::lestes::backend_v2::intercode::ge_operand > pi_operand2ge_operand::visit_pi_mem_preg(ptr< ::lestes::backend_v2::intercode::pi_mem_preg > pi){
	return ge_operand_reg::create(pi->type_get(), NULL, pi);
}

ptr< ::lestes::backend_v2::intercode::ge_operand > pi_operand2ge_operand::visit_pi_mem_temp(ptr< ::lestes::backend_v2::intercode::pi_mem_temp > pi){
	return ge_operand_mem::create(pi->type_get(), NULL, pi, pi->factory_get());
}

ptr< ::lestes::backend_v2::intercode::ge_operand > pi_operand2ge_operand::visit_pi_mem_ptr_deref(ptr< ::lestes::backend_v2::intercode::pi_mem_ptr_deref > pi) {
	ptr<pi_mf_ptr_deref> mf = pi->factory_get().dncast<pi_mf_ptr_deref>();
	
	if ( !mf->ge_addr_get() ) {
		mf->ge_addr_set(this->convert(mf->addr_get()).dncast<ge_operand_reg>());		
	}
	
	return ge_operand_mem::create(pi->type_get(), NULL, pi, mf);
}

end_package(instructions);
end_package(md);
end_package(lestes);

