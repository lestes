/*!
	\file
	\author jaz
*/
#include <lestes/md/instructions/pi_pi2ge_pi.g.hh>
#include <lestes/md/instructions/pi_operand2ge_operand.g.hh>
#include <lestes/md/instructions/tm_instr.g.hh>
#include <lestes/md/literals/literal_info.g.hh>
#include <lestes/md/types/tm_data_type.g.hh>
#include <lestes/backend_v2/intercode/pi.g.hh>
#include <lestes/backend_v2/structs/pi_operands.g.hh>
#include <lestes/backend_v2/intercode/visitor_pi_pi2pi_operands.g.hh>
#include <lestes/backend_v2/intercode/visitor_pi_pi2id.g.hh>
#include <lestes/backend_v2/debug/debug.hh>
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>
#include <sstream>
#include <lestes/std/ucn_string.hh>

package(lestes);
package(md);
package(instructions);

using ::lestes::lang::cplus::sem::ss_function_declaration;

using namespace ::lestes::backend_v2::intercode;
using namespace ::lestes::backend_v2::structs;
using namespace ::lestes::backend_v2::debug;
using namespace ::lestes::md::types;
using namespace ::lestes::md::literals;
using namespace ::lestes::msg;

typedef map<ulint,lstring> ulint2lstring__type;
typedef list< srp<pi_mem> > pi_mem_list__type;
typedef list< srp<pi_operand> > pi_operand_list__type;
typedef vector< srp< ::lestes::backend_v2::intercode::ge_pi> > ge_pi_vector__type;
typedef vector< srp < ge_pi > > ge_pi_vector__type;
typedef set< srp < ge_pi > > ge_pi_set__type;

declare_logger(log);
initialize_logger( log, "pi_pi2ge_pi", md_logger );


bool pi_pi2ge_pi::targetmachine__is_instruction_consistent(ptr<tm_instr_base> instr_version, ptr<pi_pi> pi) {
	ulint2lstring__type::iterator it = instr_version->properties_get()->find(PROPERTY_TEST_SIGNED);
	
	if ( it!=instr_version->properties_get()->end() ) {
		/*
			When evaluating result of a cmp instruction, it is needed to know whether compared numbers were
			signed or unsigned ( different condition registers are changed ). That can be determined from cmp operand's type.
			The instruction that evaluates result of a cmp instruction, should have property PROPERTY_TEST_SIGNED defined.
			The property says that the instruction takes result of a signed cmp or not.
		*/
	    //ptr<visitor_pi_pi2pi_operands> pi_operands_getter = visitor_pi_pi2pi_operands::create();
		ptr<pi_operands> pi_ops = pi->accept_visitor_pi_pi2pi_operands_gen_base(pi_operands_getter_get());
		
		//ptr<visitor_pi_pi2id> pi_id_getter = visitor_pi_pi2id::create();
		visitor_pi_pi2id::kind_type pi_id = (visitor_pi_pi2id::kind_type)pi->accept_visitor_pi_pi2ulint_gen_base(pi_id_getter_get());
		
		ptr<tm_data_type_base> type;
		
		switch ( pi_id ) {
			case visitor_pi_pi2id::PI_BT:
			case visitor_pi_pi2id::PI_BF:
			case visitor_pi_pi2id::PI_BG:
			case visitor_pi_pi2id::PI_BL:
			case visitor_pi_pi2id::PI_BNG:
			case visitor_pi_pi2id::PI_BNL: {
				ptr<pi_operand> cond = (*pi_ops->operands_input_get())[0];
				ptr<pi_pi> cond_origin_cmp = cond->origin_get();
				ptr<pi_operands> cmp_ops = cond_origin_cmp->accept_visitor_pi_pi2pi_operands_gen_base(pi_operands_getter_get());
				ptr<pi_operand> cmp_op = (*cmp_ops->operands_input_get())[0];
				type = cmp_op->type_get();
			} break;
			
			case visitor_pi_pi2id::PI_SBG:
			case visitor_pi_pi2id::PI_SBL:
			case visitor_pi_pi2id::PI_SBNG:
			case visitor_pi_pi2id::PI_SBNL:{
				ptr<pi_operand> cmp_op = (*pi_ops->operands_input_get())[0];
				type = cmp_op->type_get();
			} break;
			
			default: {
				lassert(false);
			}
			
		}
		
		ulint type_id = type->id_get();
		if ( it->second=="true" && !( type_id==DT_INT_8S || type_id==DT_INT_16S || type_id==DT_INT_32S ) ) {
			/*
				The property says signed=true but operand's types of the cmp are unsigned. The tested instruction version is not consistent!
			*/
			return false;
		}
	}
	return true;
}

ptr< ge_pi_vector__type > pi_pi2ge_pi::visit_pi_sp(ptr< ::lestes::backend_v2::intercode::pi_sp > pi) {
	
	ptr< ge_pi_vector__type > output = ge_pi_vector__type::create();
	
	ptr<ge_sp> ge = ge_sp::create(NULL,pi);
	
	//Find the first and the last sequencepoint of the processed function.
	if ( !pi->psp_get() ) {
		/*
			The first sequencepoint. Prepend function prologue.	
		*/
		ptr<ge_sp> extra_sp = ge_sp::create(NULL,pi);
		output->push_back(extra_sp);
		
		ptr<ge_pi> ge_prologue = ge_pi::create(tm_instr::instance(INSTRUCTION_PROLOGUE__1),pi);
		ge_prologue->dependencies_get()->insert(extra_sp);
		output->push_back(ge_prologue);
		
		/*
			Preserved registers.
			They are the first operands of the function, so they get required registers.
			If the register allocator allocates these register to someone else, they are
			spilled-out (preserved) and loaded back at the end of the function.
		*/
		ptr<tm_data_type_base> type_32u = tm_dt_simple::instance(DT_INT_32U);
		preserved_reg_edi = ge_operand_reg::create(type_32u,ge_prologue,NULL);
		ge_prologue->operands_output_get()->push_back(preserved_reg_edi);
		preserved_reg_esi = ge_operand_reg::create(type_32u,ge_prologue,NULL);
		ge_prologue->operands_output_get()->push_back(preserved_reg_esi);
		preserved_reg_ebx = ge_operand_reg::create(type_32u,ge_prologue,NULL);
		ge_prologue->operands_output_get()->push_back(preserved_reg_ebx);
		
		ge->dependencies_get()->insert(extra_sp);
		ge->dependencies_get()->insert(ge_prologue);
		output->push_back(ge);
		
		first_sp_set(ge);
	} else if ( !pi->nsp_get() ) {
		/*
			The last sequencepoint. Append function epilogue.	
		*/
		last_sp_set(ge);
		output->push_back(ge);
		
		ptr<ge_pi> ge_epilogue = ge_pi::create(tm_instr::instance(INSTRUCTION_EPILOGUE__1),pi);
		ge_epilogue->dependencies_get()->insert(ge);
		ge_epilogue->operands_input_get()->push_back(preserved_reg_edi);
		ge_epilogue->operands_input_get()->push_back(preserved_reg_esi);
		ge_epilogue->operands_input_get()->push_back(preserved_reg_ebx);
		output->push_back(ge_epilogue);
		
		ptr<ge_pi> ge_leave = ge_pi::create(tm_instr::instance(INSTRUCTION_LEAVE__1),pi);
		ge_leave->dependencies_get()->insert(ge_epilogue);
		output->push_back(ge_leave);
		
		ptr<ge_pi> ge_ret = ge_pi::create(tm_instr::instance(INSTRUCTION_RET__1),pi);
		ge_ret->dependencies_get()->insert(ge_leave);
		output->push_back(ge_ret);
		
		ptr<ge_sp> extra_sp = ge_sp::create(NULL,pi);
		extra_sp->dependencies_get()->insert(ge);
		extra_sp->dependencies_get()->insert(ge_epilogue);
		extra_sp->dependencies_get()->insert(ge_leave);
		extra_sp->dependencies_get()->insert(ge_ret);
		output->push_back(extra_sp);
	} else {	
		output->push_back(ge);
	}
	
	pi_sp2ge_sp_get()->insert(::std::pair<srp<pi_sp>,srp<ge_sp> >(pi,ge));
	
	return output;
}


ptr< ge_pi_vector__type > pi_pi2ge_pi::visit_pi_leave(ptr< ::lestes::backend_v2::intercode::pi_leave > pi) {
	
	//pi_leave is just regular jump that jumps to the last sequencepoint of the current function.
	ptr<ge_pi> ge = ge_pi::create(tm_instr::instance(INSTRUCTION_JMP__1),pi);
	ge->jmp_targets_set(vector<srp<ge_sp> >::create());
	last_sp_get()->is_jmp_target_set(true);
	ge->jmp_targets_get()->push_back(last_sp_get());
	
	ptr< ge_pi_vector__type > output = ge_pi_vector__type::create();
	output->push_back(ge);
	
	return output;
}

ptr< ge_pi_vector__type > pi_pi2ge_pi::visit_pi_call(ptr< ::lestes::backend_v2::intercode::pi_call > pi) {
	log << "visit_pi_call - start\n" << eolog;
	
	ptr< ge_pi_vector__type > output = ge_pi_vector__type::create();
	
	ptr<ge_operand> stack_pointer = get_stack_pointer(pi);
	
	ptr<ge_call> ge = ge_call::create(tm_instr::instance(INSTRUCTION_CALL__1),pi,pi->f_get());
	
	//non-preserved registers
	ptr<ge_operand> reg_eax = pi_op2ge_op_convertor_get()->convert(pi->rv_get());
	reg_eax->origin_set(ge);
	ge->operands_output_get()->push_back(reg_eax);
	
	ptr<tm_data_type_base> type_32u = tm_dt_simple::instance(DT_INT_32U);
	ptr<ge_operand> reg_ecx = ge_operand_reg::create(type_32u,ge,NULL);
	ge->operands_output_get()->push_back(reg_ecx);
	
	ptr<ge_operand> reg_edx = ge_operand_reg::create(type_32u,ge,NULL);
	ge->operands_output_get()->push_back(reg_edx);
	
	int pushed_args = 0;
	
	//push arguments
	push_operands(pi,stack_pointer,output);

	if ( !pi->type_get()->return_reg_get() ) {	
		//push destination for return value
		push_operand(pi,pi->rv_get(),stack_pointer,output);
		++pushed_args;
	}

	//call	
	if ( output->size()>0 ) {
		ptr<ge_pi> last = (*output)[output->size() - 1];
		ge->dependencies_get()->insert(last);
	}
	output->push_back(ge);
	
	pushed_args += pi->args_get()->size();
	
	//generate code that restores original value of stack pointer.
	restore_stack(pi,ge,stack_pointer,pushed_args,output);
	
	log << "visit_pi_call - end\n" << eolog;
	
	return output;
}

/*!
	\brief Creates register operand that hold stack pointer after function call.
	
	\param pi A function call.
	\return A operand that hold stack pointer.
*/
ptr<ge_operand> pi_pi2ge_pi::get_stack_pointer(ptr<pi_abstract_function_call > pi) {
	ptr<ge_pi> ge_psp = (*pi_sp2ge_sp_get())[pi->psp_get().dncast<pi_sp>()];
	return ge_operand_reg::create(tm_dt_simple::instance(DT_INT_32U),ge_psp,NULL);
}

/*!
	\brief Generates code that restores stack pointer after function call.
	
	\param pi A pi-level call pseudoinstruction.
	\param ge A ge-level call pseudoinstruction that corresponds to the pi.
	\param stack_pointer An operand that holds stack pointer.
	\param argc An argument count of the function call.
	\param output A output code where restore code is to be appended.
*/
void pi_pi2ge_pi::restore_stack(ptr<pi_abstract_function_call > pi, ptr<ge_call> ge, ptr<ge_operand> stack_pointer,ulint argc, ptr< ge_pi_vector__type > output) {
	log << "restore_stack - start\n" << eolog;
	
	if ( argc==0 ) {
		return;
	}
	
	ptr<ge_pi> ge_nsp = (*pi_sp2ge_sp_get())[pi->nsp_get().dncast<pi_sp>()];
	
	//restore stack pointer after call returns
	ptr<ge_pi> restore = ge_pi::create(tm_instr::instance(INSTRUCTION_ESPADD__1),pi);
	restore->dependencies_get()->insert(ge);
	restore->operands_input_get()->push_back(stack_pointer);
	
	ptr<tm_data_type_base> type_32u = tm_dt_simple::instance(DT_INT_32U);
	
	::std::ostringstream oss;
	oss << argc*4;
	
	ptr<li_simple> offset_value = li_simple::create(type_32u,ucn_string(oss.str().c_str()));
	ptr<ge_operand> offset = ge_operand_imm::create(type_32u,NULL,NULL,offset_value);
	restore->operands_input_get()->push_back(offset);
	
	ptr<ge_operand> new_stack_pointer = ge_operand_reg::create(type_32u,restore,NULL);
	restore->operands_output_get()->push_back(new_stack_pointer);
	
	ptr<ge_operand> cond = ge_operand_reg::create(tm_dt_simple::instance(DT_COND),restore,NULL);
	restore->operands_output_get()->push_back(cond);
	
	/*
		Add modified stack pointer to list of input operands of nsp in order to not allow dead-code elimination eliminate
		the restore instruction.
	*/	
	ge_nsp->operands_input_get()->push_back(new_stack_pointer);
	
	output->push_back(restore);
	
	log << "restore_stack - end\n" << eolog;
}

/*!
	\brief Generates code that pushes function's operands to the stack.
	
	\param pi A function call.
	\param stack_pointer An operand that holds stack pointer.
	\param output A output code where restore code is to be appended.
*/
void pi_pi2ge_pi::push_operands(ptr<pi_abstract_function_call > pi, ptr<ge_operand>& stack_pointer, ptr< ge_pi_vector__type > output) {
	log << "push_operands - start\n" << eolog;
	
	ptr<pi_operand_list__type> args = pi->args_get();
	
	for(pi_operand_list__type::reverse_iterator it = args->rbegin(); it!=args->rend(); ++it) {
		ptr<pi_operand> op = *it;
		push_operand(pi,*it,stack_pointer,output);	
	}		
	
	log << "push_operands - end\n" << eolog;
}

/*!
	\brief Generates code that pushes function's operand to the stack.
	
	\param pi A function call.
	\param op An operand.
	\param stack_pointer An operand that holds stack pointer.
	\param output A output code where restore code is to be appended.
*/
void pi_pi2ge_pi::push_operand(ptr<pi_abstract_function_call > pi, ptr<pi_operand> op, ptr<ge_operand>& stack_pointer, ptr< ge_pi_vector__type > output) {
	log << "push_operand - start\n" << eolog;
	
	ptr<ge_operand> ge_op1 = pi_op2ge_op_convertor_get()->convert(op);
	
	ulint type = ge_op1->type_get()->id_get();		
	
	ptr<ge_pi> prev = output->size()>0 ? (*output)[output->size() - 1] : NULL;
	
	ptr<ge_pi> ge_push;
	ptr<ge_operand> new_stack_pointer = ge_operand_reg::create(tm_dt_simple::instance(DT_INT_32U),NULL,NULL);
	
	switch (ge_op1->kind_get()) {
		case ge_operand::MEMORY: {
					
			if ( type!=DT_INT_32U && type!=DT_INT_32S ) {
				/*
					Only 32-bit entities are pushed to the stack. If the operand's type
					is less then 32-bites, then we need to convert it.
				*/
				ptr<ge_pi> ge_prep = ge_pi::create(tm_instr::instance(INSTRUCTION_MOVZX__5),pi);
				ge_prep->operands_input_get()->push_back(ge_op1);
				ptr<ge_operand> ge_op_to_push = ge_operand_reg::create(tm_dt_simple::instance(DT_INT_32U),ge_prep,NULL);
				ge_prep->operands_output_get()->push_back(ge_op_to_push);
				output->push_back(ge_prep);
				
				ge_push = ge_pi::create(tm_instr::instance(INSTRUCTION_PUSH__2),pi);
				ge_push->operands_input_get()->push_back(stack_pointer);
				ge_push->operands_input_get()->push_back(ge_op_to_push);			
				ge_push->dependencies_get()->insert(ge_prep);
			} else {
				ge_push = ge_pi::create(tm_instr::instance(INSTRUCTION_PUSH__1),pi);
				ge_push->operands_input_get()->push_back(stack_pointer);
				ge_push->operands_input_get()->push_back(ge_op1);
			}	
		} break;
		
		case ge_operand::REGISTER: {
			
			if ( type!=DT_INT_32U && type!=DT_INT_32S ) {
				/*
					Only 32-bit entities are pushed to the stack. If the operand's type
					is less then 32-bites, then we need to convert it.
				*/
				ptr<ge_pi> ge_prep = ge_pi::create(tm_instr::instance(INSTRUCTION_MOVZX__4),pi);
				ge_prep->operands_input_get()->push_back(ge_op1);
				ptr<ge_operand> ge_op_to_push = ge_operand_reg::create(tm_dt_simple::instance(DT_INT_32U),ge_prep,NULL);
				ge_prep->operands_output_get()->push_back(ge_op_to_push);
				output->push_back(ge_prep);
				
				ge_push = ge_pi::create(tm_instr::instance(INSTRUCTION_PUSH__2),pi);
				ge_push->operands_input_get()->push_back(stack_pointer);
				ge_push->operands_input_get()->push_back(ge_op_to_push);
				ge_push->dependencies_get()->insert(ge_prep);
			} else {
				ge_push = ge_pi::create(tm_instr::instance(INSTRUCTION_PUSH__2),pi);
				ge_push->operands_input_get()->push_back(stack_pointer);
				ge_push->operands_input_get()->push_back(ge_op1);
			}
		} break;
		
		default: lassert(false);
	}
	
	
	if ( prev ) {
		ge_push->dependencies_get()->insert(prev);
	}
	
	ge_push->operands_output_get()->push_back(new_stack_pointer);
	new_stack_pointer->origin_set(ge_push);
	
	stack_pointer = new_stack_pointer;
	
	output->push_back(ge_push);
	
	log << "push_operand - end\n" << eolog;
}

ptr< ge_pi_vector__type > pi_pi2ge_pi::visit_pi_callv(ptr< ::lestes::backend_v2::intercode::pi_callv > pi) {
	log << "visit_pi_callv - start\n" << eolog;
	
	ptr< ge_pi_vector__type > output = ge_pi_vector__type::create();
	
	ptr<ge_operand> stack_pointer = get_stack_pointer(pi);
	
	ptr<ge_call> ge = ge_call::create(tm_instr::instance(INSTRUCTION_CALL__1),pi,pi->f_get());
	
	//non-preserved registers
	ptr<tm_data_type_base> type_32u = tm_dt_simple::instance(DT_INT_32U);
	ptr<ge_operand> reg_eax = ge_operand_reg::create(type_32u,ge,NULL);
	reg_eax->origin_set(ge);
	ge->operands_output_get()->push_back(reg_eax);
	
	ptr<ge_operand> reg_ecx = ge_operand_reg::create(type_32u,ge,NULL);
	reg_ecx->origin_set(ge);
	ge->operands_output_get()->push_back(reg_ecx);
	
	ptr<ge_operand> reg_edx = ge_operand_reg::create(type_32u,ge,NULL);
	reg_edx->origin_set(ge);
	ge->operands_output_get()->push_back(reg_edx);
	
	//push arguments
	push_operands(pi,stack_pointer,output);
	
	//call	
	if ( output->size()>0 ) {
		ptr<ge_pi> last = (*output)[output->size() - 1];
		ge->dependencies_get()->insert(last);
	}
	output->push_back(ge);
	
	if ( pi->args_get()->size()>0 ) {
		//generate code that restores original value of stack pointer.
		restore_stack(pi,ge,stack_pointer,pi->args_get()->size(),output);
	}
	
	log << "visit_pi_callv - end\n" << eolog;
	
	return output;
}

ptr< ge_pi_vector__type > pi_pi2ge_pi::visit_pi_icall(ptr< ::lestes::backend_v2::intercode::pi_icall > ) {
	lassert2(false,msg_not_implemented_yet);
}

ptr< ge_pi_vector__type > pi_pi2ge_pi::visit_pi_icallv(ptr< ::lestes::backend_v2::intercode::pi_icallv > ){
	lassert2(false,msg_not_implemented_yet);
}

ptr< ge_pi_vector__type > pi_pi2ge_pi::visit_pi_iba(ptr< ::lestes::backend_v2::intercode::pi_iba > ){
	lassert2(false,msg_not_implemented_yet);
}

ptr< ge_pi_vector__type > pi_pi2ge_pi::visit_pi_ibn(ptr< ::lestes::backend_v2::intercode::pi_ibn > ){
	lassert2(false,msg_not_implemented_yet);
}

ptr< ge_pi_vector__type > pi_pi2ge_pi::visit_pi_ibm(ptr< ::lestes::backend_v2::intercode::pi_ibm > ){
	lassert2(false,msg_not_implemented_yet);
}

ptr< ge_pi_vector__type > pi_pi2ge_pi::visit_pi_ibg(ptr< ::lestes::backend_v2::intercode::pi_ibg > ){
	lassert2(false,msg_not_implemented_yet);
}

ptr< ge_pi_vector__type > pi_pi2ge_pi::visit_pi_ibl(ptr< ::lestes::backend_v2::intercode::pi_ibl > ){
	lassert2(false,msg_not_implemented_yet);
}

ptr< ge_pi_vector__type > pi_pi2ge_pi::visit_pi_ibe(ptr< ::lestes::backend_v2::intercode::pi_ibe > ){
	lassert2(false,msg_not_implemented_yet);
}

ptr< ge_pi_vector__type > pi_pi2ge_pi::visit_pi_ibng(ptr< ::lestes::backend_v2::intercode::pi_ibng > ){
	lassert2(false,msg_not_implemented_yet);
}

ptr< ge_pi_vector__type > pi_pi2ge_pi::visit_pi_ibnl(ptr< ::lestes::backend_v2::intercode::pi_ibnl > ){
	lassert2(false,msg_not_implemented_yet);
}

ptr< ge_pi_vector__type > pi_pi2ge_pi::visit_pi_ibne(ptr< ::lestes::backend_v2::intercode::pi_ibne > ){
	lassert2(false,msg_not_implemented_yet);
}

ptr< ge_pi_vector__type > pi_pi2ge_pi::visit_pi_bm(ptr< ::lestes::backend_v2::intercode::pi_bm > ){
	lassert2(false,msg_not_implemented_yet);
}

ptr< ge_pi_vector__type > pi_pi2ge_pi::visit_pi_divrni(ptr< ::lestes::backend_v2::intercode::pi_divrni > ){
	lassert2(false,msg_not_implemented_yet);
}

ptr< ge_pi_vector__type > pi_pi2ge_pi::visit_pi_divrpi(ptr< ::lestes::backend_v2::intercode::pi_divrpi > ){
	lassert2(false,msg_not_implemented_yet);
}

ptr< ge_pi_vector__type > pi_pi2ge_pi::visit_pi_divrz(ptr< ::lestes::backend_v2::intercode::pi_divrz > ){
	lassert2(false,msg_not_implemented_yet);
}



end_package(instructions);
end_package(md);
end_package(lestes);

