#include <lestes/backend_v2/structs/func_data.g.hh>
#include <lestes/backend_v2/debug/debug.hh>
#include <lestes/backend_v2/intercode/visitor_ge_pi2asm.g.hh>
#include <lestes/md/tasm/asm_generator.g.hh>
#include <lestes/md/tasm/tm_asm.mdg.hh>
#include <lestes/md/mem/memory_allocators.g.hh>
#include <lestes/md/literals/literal_loader.g.hh>
#include <lestes/md/symbols/name_mangler.g.hh>
#include <lestes/md/symbols/symbol_register.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>

package(lestes);
package(md);
package(tasm);

using namespace ::lestes::msg;
using namespace ::lestes::backend_v2::structs;
using namespace ::lestes::backend_v2::intercode;
using namespace ::lestes::backend_v2::debug;
using namespace ::lestes::md::instructions;
using namespace ::lestes::md::mem;
using namespace ::lestes::md::literals;
using namespace ::lestes::md::symbols;
using namespace ::lestes::lang::cplus::sem;

typedef list< srp<ge_pi> > ge_pi_list__type;
typedef vector<srp< ::lestes::backend_v2::structs::func_data> >  func_data_vector__type;

void asm_generator::generate_tu_prologue() {
	/* Generate asm file header */
	lstring banner = tm_asm::ent_one_line_comment_get();
	output_get() <<  string_replace(banner, "$text", "This file is an output of Lestes C++ compiler.\n");
	
	//Get singleton instance of the allocator for global variables
	ptr<global_variable_allocator> glob_var_allocator = global_variable_allocator::instance();
	
	/* Generate uninitialized data section header */
	output_get() << tm_asm::ent_section_uninitialized_data_get();
	
	//Generate asm declarations for global variables
	glob_var_allocator->emit_asm_declarations_for_uninitialized(output_get());
	
	output_get() << ::std::endl;
	
	/* Generate data section header */
	output_get() << tm_asm::ent_section_data_get();
	
	//Generate asm declarations for global variables
	glob_var_allocator->emit_asm_declarations_for_initialized(output_get());
	
	//Generate asm declarations for managed literals
	literal_loader::instance()->emit_global_declarations(output_get());
	
	output_get() << ::std::endl;
}

void asm_generator::generate_tu_body(ptr<func_data_vector__type> tu_data) {
	/* Generate code section */
	output_get() << tm_asm::ent_section_text_get();
	
	/* Generate declarations for functions */
	symbol_register::instance()->emit_global_declarations(output_get());	
	
	output_get() << ::std::endl;
	
	/* Generate function bodies */
	for(ulint i=0; i<tu_data->size();++i) {
		process_function((*tu_data)[i]);
	}
	
	output_get() << ::std::endl;
}

void asm_generator::generate_tu_epilogue() {
	output_get() << ::std::endl;
}

/*!
	\brief Generates asm code for single function body.
	
	\param data A function data.
*/
void asm_generator::process_function(ptr<func_data> data) {
	/* Generate function header */
	lstring mangled_name = name_mangler::instance()->mangle(data->function_decl_get());
	lstring header = tm_asm::ent_function_label_get();
	output_get() <<  string_replace(header, "$name", mangled_name);
	
	/* Generate function body */
	ptr<visitor_ge_pi2asm> ge2asm = visitor_ge_pi2asm::create();

	ptr<ge_pi_list__type> ge_pi_list = data->ge_body_get();
	
	for(ge_pi_list__type::iterator it_ge_list = ge_pi_list->begin(); it_ge_list!=ge_pi_list->end(); ++it_ge_list) {
		lstring asm_code = (*it_ge_list)->accept_visitor_ge_pi2lstring_gen_base(ge2asm);
		
		if ( !asm_code.empty() ) {
			//Code has been emited.	Flush it to the output.
			output_get() << tm_asm::ent_instruction_delimiter_get() << asm_code;
		}
	}
	
	output_get() << ::std::endl;
}



end_package(tasm);
end_package(md);
end_package(lestes);

