/*!
	\file
	\brief Function parameter acessor.
	\author jaz
*/
#include <lestes/md/functions/function_parameter_accessor.g.hh>
#include <lestes/md/types/tm_data_type_base.g.hh>
#include <lestes/md/registers/tm_register.g.hh>
#include <lestes/md/types/ss_type2tm_type.g.hh>
#include <lestes/md/mem/memory_allocator_bases.g.hh>
#include <lestes/backend_v2/intercode/pi.g.hh>
#include <lestes/backend_v2/intercode/pi_mem_factory.g.hh>
#include <lestes/lang/cplus/sem/ss_type.g.hh>
#include <lestes/lang/cplus/sem/ss_misc.g.hh>
#include <lestes/lang/cplus/sem/ss_type2id.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>
#include <lestes/lang/cplus/sem/visitor.v.g.hh>

package(lestes);
package(md);
package(functions);

using namespace ::lestes::backend_v2::intercode;
using namespace ::lestes::md::types;
using namespace ::lestes::md::registers;
using namespace ::lestes::lang::cplus::sem;

typedef map<ulint, srp<pi_mem_factory> > params_map_type;
typedef list<srp<ss_type> > ss_type_list_type;

/*!
	\brief Gets parameter with given index.
	
	Note:
	 - index starts from 0
	 
	\param index The index.
	\return Memory space with parameter.
*/
ptr< pi_mem_factory > function_parameter_accessor::get_parameter(ulint index)
{
	//Gen parameter with given index.
	params_map_type::iterator it = params_get()->find(index);
	
	if ( it!=params_get()->end() ) {
		//Parameter has already been accessed. Return cached result.
		return it->second;
	}
	
	//Get ss_type of the function. It contains also list of parameter types.
	ptr<ss_function> fnc_type = function_get()->type_get().dncast<ss_function>();

	//Get offset of the 1st parameter.
	t_size offset = 64;
	
	if ( get_ret_val() ) {
		offset = ret_val_end_offset;
	}

	if ( get_this() ) {
		offset = this_end_offset;
	}
	
	ptr<tm_data_type_base> param_dt_type;
	ulint param_index = 0;
	ss_type_list_type::iterator it1;
	for(it1=fnc_type->params_get()->begin(); it1!=fnc_type->params_get()->end(); it1++, param_index++) {
		param_dt_type = (*it1)->accept_ss_type2tm_type_gen_base(ss_type2tm_type::instance());
		
		if ( param_index == index ) {
			break;
		}
		
		if ( param_dt_type->bitwidth_get() % 32 == 0 ) {
			offset += param_dt_type->bitwidth_get();
		} else {
			offset += (param_dt_type->bitwidth_get()/32 +1)*32;
		}
	}
	
	lassert(param_dt_type);

	//Create factory representing space where the parameter is located.
	ptr<pi_mem_factory> ret_val = pi_mf_stack::create(NULL,param_dt_type, offset);
	
	//Add parameter factory to parameter cache.
	(*params_get())[index] = ret_val;
	
	return ret_val;	
}

/*!
	\brief Gets secret "this" parameter.
	
	\return If function is member function then it returns memory space with "this" parameter. NULL otherwise. 
*/
ptr< pi_mem_factory > function_parameter_accessor::get_this()
{
	if ( this_par_get() ) {
		//Parameter has already been accessed. Return cached result.
		return this_par_get();
	}

	if ( ss_type2id::instance()->process(function_get()->type_get()) != ss_type2id::MEMBER_FUNCTION ) {
		//The function is NOT member function.
		return NULL;
	}
	
	//Get type of this pointer.
	ptr<ss_member_function> fnc_type = function_get()->type_get().dncast<ss_member_function>();	
	ptr<tm_data_type_base> this_type = fnc_type->this_type_get()->accept_ss_type2tm_type_gen_base(ss_type2tm_type::instance());
	
	/*
		Initial offset is 64, because EIP is pushed on the stack during call instruction
		and EBP is pushed during function prologue.
	*/
	t_size offset = 64;
	
	if ( !fnc_type->is_void() ) {
		//Get return type.
		ptr<tm_data_type_base> ret_type = fnc_type->returns_get()->accept_ss_type2tm_type_gen_base(ss_type2tm_type::instance());
	
		if ( !ret_type->return_reg_get() ) {
			//Type is not returned in register.
			offset += 32;
		}
	}
	
	this_end_offset = offset+32;
	this_par_set(pi_mf_stack::create(NULL,this_type, offset));
	return this_par_get();
}


/*!
	\brief Gets secret parameter with pointer to space for returning value from the function.	
	
	\return If function returns non-void type then it returns memory space with pointer. NULL otherwise. 
*/
ptr< pi_mem_factory > function_parameter_accessor::get_ret_val()
{
	if ( ret_val_par_get() ) {
		//Parameter has already been accessed. Return cached result.
		return ret_val_par_get();
	}
	
	//Get ss_type of function.
	ptr<ss_function> fnc_type = function_get()->type_get().dncast<ss_function>();
	
	if ( fnc_type->returns_get()->is_void() ) {
		//Function returns void.
		return NULL;
	}
	lassert(fnc_type->returns_get());
	
	//Get returned type.
	ptr<tm_data_type_base> type = fnc_type->returns_get()->accept_ss_type2tm_type_gen_base(ss_type2tm_type::instance());
	
	ptr<pi_mem_factory> ret_val;
	if (type->return_reg_get() ){
		/*
			Type is returned in register.
		*/
		ret_val = pi_mf_preg::create(NULL, type, pi_preg::create(NULL, type)); 
		ret_val_end_offset = 64;
	} else {
		/*
			Type is returned in memory as 1st parameter.
			Starting offset is 64, because of EIP and EBP are pushed on the stack during call instruction.
		*/
		ret_val = pi_mf_stack::create(NULL,type, 64);
		ret_val_end_offset = 96;
	}
	
	ret_val_par_set(ret_val);
	return ret_val_par_get();
}

ptr< function_parameter_accessor > function_parameter_accessor::instance(ptr<ss_function_declaration> function)
{
	map< srp< ss_function_declaration >, srp< function_parameter_accessor > >::iterator it;
	it = singleton_instances_get()->find(function);
	
	if ( it!=singleton_instances_get()->end() ) {
		return it->second;
	} else {
		ptr< function_parameter_accessor > new_instance = function_parameter_accessor::create(function);
		singleton_instances_get()->insert(::std::pair<srp< ss_function_declaration >, srp< function_parameter_accessor> >(function,new_instance));
		return new_instance;
	}
	
	return NULL;	
}



end_package(functions);
end_package(md);
end_package(lestes);

