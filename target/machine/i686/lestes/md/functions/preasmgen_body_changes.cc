#include <lestes/backend_v2/intercode/ge.g.hh>
#include <lestes/backend_v2/intercode/pi.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>
#include <lestes/md/functions/preasmgen_body_changes.g.hh>
#include <lestes/md/registers/tm_register.g.hh>
#include <lestes/md/types/tm_data_type.g.hh>
#include <lestes/md/instructions/tm_instr.g.hh>
#include <lestes/md/literals/literal_info.g.hh>
#include <lestes/md/mem/memory_allocators.g.hh>
#include <sstream>
#include <lestes/std/ucn_string.hh>

package(lestes);
package(md);
package(functions);

using namespace ::lestes::backend_v2::intercode;
using namespace ::lestes::md::registers;
using namespace ::lestes::md::types;
using namespace ::lestes::md::instructions;
using namespace ::lestes::md::literals;
using namespace ::lestes::md::mem;
using ::lestes::lang::cplus::sem::ss_function_declaration;

typedef list<srp<ge_pi> > ge_pi_list__type;

void preasmgen_body_changes::process_body() {
	ptr<ge_pi> psp = ge_sp::create(NULL, NULL);
	ptr<ge_pi> nsp = body_get()->front().dncast<ge_sp>();
	
	ptr<tm_data_type_base> type_32u = tm_dt_simple::instance(DT_INT_32U);
	ptr<ge_operand_reg> reg_ebp = ge_operand_reg::create(type_32u,psp,NULL);
	ptr<ge_operand_reg> reg_esp = ge_operand_reg::create(type_32u,psp,NULL);
	
	//PUSH EBP
	ptr<ge_pi> push_ebp = ge_pi::create(tm_instr::instance(INSTRUCTION_PUSH__2),NULL);
	push_ebp->operands_input_get()->push_back(reg_esp);
	(*reg_esp->assigned_registers_get())[push_ebp] = R_ESP;
	push_ebp->operands_input_get()->push_back(reg_ebp);
	(*reg_ebp->assigned_registers_get())[push_ebp] = R_EBP;
	push_ebp->operands_output_get()->push_back(reg_esp);
	
	
	//MOV EBP,ESP
	ptr<ge_pi> mov_esp_to_ebp = ge_pi::create(tm_instr::instance(INSTRUCTION_BACKUPESP__1),NULL);
	mov_esp_to_ebp->operands_input_get()->push_back(reg_esp);
	(*reg_esp->assigned_registers_get())[mov_esp_to_ebp] = R_ESP;
	mov_esp_to_ebp->operands_output_get()->push_back(reg_ebp);
	(*reg_ebp->assigned_registers_get())[mov_esp_to_ebp] = R_EBP;
	
	ptr<local_variable_allocator> allocator = local_variable_allocator::instance(function_get());
	lint vars_size = allocator->local_end_offset_get();
	
	if (vars_size!=0) {
		//ADD ESP, $VARS_SPACE_SIZE
		::std::ostringstream oss;
		oss << vars_size/8;
	
		ptr<li_simple> vars_size_value = li_simple::create(type_32u,ucn_string(oss.str().c_str()));
		ptr<ge_operand> vars_size = ge_operand_imm::create(type_32u,NULL,NULL,vars_size_value);
	
		ptr<ge_operand_reg> cond = ge_operand_reg::create(tm_dt_simple::instance(DT_COND),NULL,NULL);
	
		ptr<ge_pi> add = ge_pi::create(tm_instr::instance(INSTRUCTION_ESPADD__1),NULL);
		add->operands_input_get()->push_back(reg_esp);
		(*reg_esp->assigned_registers_get())[add] = R_ESP;
		add->operands_input_get()->push_back(vars_size);
		add->operands_output_get()->push_back(reg_esp);
		add->operands_output_get()->push_back(cond);
		(*cond->assigned_registers_get())[add] = R_COND;
		cond->origin_set(add);
		
		body_get()->push_front(add);
		add->dependencies_get()->insert(mov_esp_to_ebp);
		nsp->dependencies_get()->insert(add);
	}
	
	body_get()->push_front(mov_esp_to_ebp);
	mov_esp_to_ebp->dependencies_get()->insert(push_ebp);
	nsp->dependencies_get()->insert(mov_esp_to_ebp);
	
	body_get()->push_front(push_ebp);
	push_ebp->dependencies_get()->insert(psp);
	nsp->dependencies_get()->insert(push_ebp);
	
	body_get()->push_front(psp);
	nsp->dependencies_get()->insert(psp);
}

ptr<ge_pi_list__type> preasmgen_body_changes::get_result() {
	return body_get();
}

end_package(functions);
end_package(md);
end_package(lestes);

