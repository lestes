<?xml version="1.0" encoding="UTF-8"?>
<lsd xmlns="http://lestes.jikos.cz/schemas/lsd" xmlns:h="http://www.w3.org/TR/REC-html40">

	<dox file="both">
		<bri>Function parameter accessor.</bri>
		<det>\author jaz</det>
	</dox>
	
    <file-name>function_parameter_accessor</file-name>
    
    <packages>
 		<p>lestes</p>
		<p>md</p>
		<p>functions</p>
    </packages>
    
    <imports>
		<i>lestes/std/map.hh</i>
		<i>lestes/md/common.hh</i>
		<i>lestes/md/functions/function_parameter_accessor_base.g.hh</i>
    </imports>
	
	<implementation-imports>
		<i>lestes/backend_v2/intercode/pi_mem_factory.g.hh</i>
		<i>lestes/lang/cplus/sem/ss_type.g.hh</i>
		<i>lestes/lang/cplus/sem/ss_declaration.g.hh</i>
    </implementation-imports>
    
	<foreign-class name="object">
		<p>lestes</p>
		<p>std</p>
	</foreign-class>

	<foreign-class name="ss_function_declaration">
		<p>lestes</p>
		<p>lang</p>
		<p>cplus</p>
		<p>sem</p>
	</foreign-class>
	
	<foreign-class name="pi_mem_factory">
		<p>lestes</p>
		<p>backend_v2</p>
		<p>intercode</p>
	</foreign-class>

	<include href="../../../../../../lestes/md/functions/function_parameter_accessor_base.lsd" />
    
    <class name="function_parameter_accessor" base="function_parameter_accessor_base">
		<dox>
			<bri>Interface used for accessing function's parameters within the function.</bri>
		</dox>

		<method name="get_parameter" type="pi_mem_factory" specifier="virtual" >
			<dox>
				<bri>Gets parameter with given index.</bri>
			</dox>
	    	<param name="index" type="ulint" />
		</method>	
		
		<method name="get_this" type="pi_mem_factory" specifier="virtual" >
			<dox>
				<bri>Gets secret "this" parameter.</bri>
			</dox>
		</method>	
		
		<method name="get_ret_val" type="pi_mem_factory" specifier="virtual" >
			<dox>
				<bri>Gets secret parameter with pointer to space for returning value from the function.</bri>
			</dox>
		</method>	
		
		<field name="this_par" type="pi_mem_factory" get="private"  set="private" alt="private" init="NULL" check="" >
			<dox>
				<bri>"this" parameter.</bri>
			</dox>
		</field>
		
		<field name="ret_val_par" type="pi_mem_factory" get="private"  set="private" alt="private" init="NULL" check="" >
			<dox>
				<bri>Secret parameter with pointer to space for returning value from the function.</bri>
			</dox>
		</field>
		
		<field name="this_end_offset" type="t_ptrdiff" init="0" >
			<dox>
				<bri>Stack offset where "this" parameter ends.</bri>
			</dox>
		</field>
		
		<field name="ret_val_end_offset" type="t_ptrdiff" init="0" >
			<dox>
				<bri>Stack offset where ret_val parameter ends.</bri>
			</dox>
		</field>
		
		<collection name="params" kind="map" key="ulint" type="pi_mem_factory" get="private"  set="private" alt="private" init="" >
			<dox>
				<bri>Mapping from parameter index to parameter.</bri>
			</dox>
		</collection>
		
		<method name="instance" type="function_parameter_accessor" specifier="static">
			<param name="function" type="ss_function_declaration" />
		</method>
		
		<collection name="singleton_instances" kind="map" key="ss_function_declaration" type="function_parameter_accessor" get="private"  set="private" alt="private" init="" specifier="static"/>
    </class>
    
    
</lsd>    

