#include <lestes/md/types/tm_data_type.g.hh>
#include <lestes/md/registers/tm_register.g.hh>

package(lestes);
package(md);
package(types);


/*!
	\brief Creates new simple_dt_bitfield object of given type and width.
	
	\param type The type of bitfield.
	\param width The width of bitfield.
	\return New bitfield object.
*/
ptr<tm_dt_bitfield> tm_dt_bitfield::create(ptr<tm_dt_simple> type, ulint width) {
	return tm_dt_bitfield::create(type->id_get(), type->format_get(), type->bitwidth_get(), type->alignment_get(), type->return_reg_get(), type->asm_output_get(), "",width);
}



end_package(types);
end_package(md);
end_package(lestes);
