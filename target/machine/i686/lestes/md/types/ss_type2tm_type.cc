/*!
	\file 
	\brief ss_type to tm_data type convertor.
	\author jaz
*/
#include <lestes/md/types/ss_type2tm_type.g.hh>
#include <lestes/md/types/tm_data_type.g.hh>
#include <lestes/md/registers/tm_register.g.hh>
#include <lestes/lang/cplus/sem/ss_misc.g.hh>
#include <lestes/lang/cplus/sem/ss_type_builtin.g.hh>
#include <lestes/lang/cplus/sem/ss_type.g.hh>
#include <lestes/std/list.hh>


package(lestes);
package(md);
package(types);

using namespace ::lestes::md::types;
using namespace ::lestes::lang::cplus::sem;

ptr< tm_dt_simple_base > ss_type2tm_type::conditional_preg_type_get(){
	return tm_dt_simple::instance(DT_COND);
}


ptr< tm_data_type_base > ss_type2tm_type::visit_ss_union(ptr<ss_union>){
    lassert(false);
	return NULL;
}

ptr< tm_data_type_base > ss_type2tm_type::visit_ss_class(ptr<ss_class>){
	lassert(false);
	return NULL;
}



ptr< tm_data_type_base > ss_type2tm_type::visit_ss_array(ptr<ss_array>){
	return visit_ss_type_ulong(ss_type_ulong::create());
}


ptr< tm_data_type_base > ss_type2tm_type::visit_ss_const(ptr<ss_const> type){
	lassert(type);
	//target type of const is target type of suffix
	return type->what_get()->accept_ss_type2tm_type_gen_base(this);
}

ptr< tm_data_type_base > ss_type2tm_type::visit_ss_volatile(ptr<ss_volatile> type){
	lassert(type);
	//target type of volatile is target type of suffix
	return type->what_get()->accept_ss_type2tm_type_gen_base(this);
}

ptr< tm_data_type_base > ss_type2tm_type::visit_ss_const_volatile(ptr<ss_const_volatile> type){
	lassert(type);
	//target type of const volatile is target type of suffix
	return type->what_get()->accept_ss_type2tm_type_gen_base(this);
}

ptr< tm_data_type_base > ss_type2tm_type::visit_ss_pointer(ptr<ss_pointer> type){
	lassert(type);
	//pointer is unsigned long
	return visit_ss_type_ulong(ss_type_ulong::create());
}

ptr< tm_data_type_base > ss_type2tm_type::visit_ss_reference(ptr<ss_reference> type){
	lassert(type);
	//reference is unsigned long
	return visit_ss_type_ulong(ss_type_ulong::create());
}

ptr< tm_data_type_base > ss_type2tm_type::visit_ss_enum(ptr<ss_enum> type){
	lassert(type);
	//enum is equal unsigned integer
	return visit_ss_type_uint(ss_type_uint::create());
}


ptr< tm_data_type_base > ss_type2tm_type::visit_ss_member_pointer(ptr<ss_member_pointer> type){
	lassert(type);
	/*
		TODO:
		member pointer is a special structure according to ABI.
	*/
	lassert(false);
	return (ptr< tm_data_type_base >)NULL;
}

ptr< tm_data_type_base > ss_type2tm_type::visit_ss_function(ptr<ss_function> type){
	lassert(type);
	//what type ss_function is? nobody should convert just ss_function (without pointer) to target type
	lassert(false);
	return (ptr< tm_data_type_base >)NULL;
}

ptr< tm_data_type_base > ss_type2tm_type::visit_ss_member_function(ptr<ss_member_function> type){
	lassert(type);
	//what type ss_member_function is? 
	lassert(false);
	return (ptr< tm_data_type_base >)NULL;
}

ptr< tm_data_type_base > ss_type2tm_type::visit_ss_type_wchar_t(ptr<ss_type_wchar_t> type){
	lassert(type);
	return visit_ss_type_ushort(ss_type_ushort::create());
}

ptr< tm_data_type_base > ss_type2tm_type::visit_ss_void(ptr<ss_void> type){
	lassert(type);
	//what type ss_void is? nobody should convert just ss_void (without pointer) to target type
	lassert(false);
	return (ptr< tm_data_type_base >)NULL;
}

ptr< tm_data_type_base > ss_type2tm_type::visit_ss_pseudoreference(ptr<ss_pseudoreference> type){
	lassert(type);
	//tm_data_type_base of pseudoreference is tm_data_type_base of inner ss_type
	return type->what_get()->accept_ss_type2tm_type_gen_base(this);
}


ptr< tm_data_type_base > ss_type2tm_type::visit_ss_type_float(ptr<ss_type_float>){
	lassert(false);
}

ptr< tm_data_type_base > ss_type2tm_type::visit_ss_type_double(ptr<ss_type_double>){
	lassert(false);
}

ptr< tm_data_type_base > ss_type2tm_type::visit_ss_type_ldouble(ptr<ss_type_ldouble>){
	lassert(false);
}


ptr< ss_type2tm_type > ss_type2tm_type::instance()
{
	if ( !singleton_instance_get()) {
		singleton_instance_set(ss_type2tm_type::create());
	}	
	
	return singleton_instance_get();
}

end_package(types);
end_package(md);
end_package(lestes);

