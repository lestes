/*!
	\file
	\author jaz
*/

#include <lestes/backend_v2/intercode/ge.g.hh>
#include <lestes/backend_v2/intercode/pi.g.hh>
#include <lestes/md/types/copy_constructor_call_generator.g.hh>
#include <lestes/md/instructions/tm_instr.g.hh>

package(lestes);
package(md);
package(types);

using namespace ::lestes::backend_v2::intercode;
using namespace ::lestes::md::instructions;

typedef vector< srp < ge_operand > > ge_operand_vector__type;
typedef vector< srp < ge_pi > > ge_pi_vector__type;

/*!
	\brief Returns instance.
*/
ptr<copy_constructor_call_generator> copy_constructor_call_generator::instance() {
	if ( !singleton_instance_get() ) {
		singleton_instance_set(copy_constructor_call_generator::create());
	}
	return singleton_instance_get();
}


ptr<ge_operand> copy_constructor_call_generator::generate_copy(ptr<ge_operand > op, ptr<vector<srp<ge_pi> > > copy_code) {
	ptr<ge_operand_reg> op_res;
	
	switch (op->kind_get()) {
		case ge_operand::REGISTER: {
			ptr<tm_instr> tm = tm_instr::instance(INSTRUCTION_MOV__1);
			ptr<ge_pi> ge = ge_pi::create(tm,NULL);
			
			op_res = ge_operand_reg::create(op->type_get(),ge,NULL);
			
			ge->operands_input_get()->push_back(op);
			ge->operands_output_get()->push_back(op_res);
			
			copy_code->push_back(ge);
		} break;
		
		case ge_operand::MEMORY: {
			lassert(false);
		} break;
		
		case ge_operand::IMMEDIATE: {
			lassert(false);
		} break;
	}
	
	return op_res;
}

end_package(types);
end_package(md);
end_package(lestes);

