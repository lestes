/*!
	\file
	\brief Ranges of numeric types.
	\author jaz
*/

#include <lestes/md/types/tm_data_type.g.hh>
#include <lestes/md/types/num_range.g.hh>
#include <lestes/md/types/num_range_getter.g.hh>

package(lestes);
package(md);
package(types);

using namespace ::lestes::md::types;

ptr<num_range_getter> num_range_getter::instance() {
	if ( !singleton_instance_get() ) {
		singleton_instance_set(num_range_getter::create());
	}
	return singleton_instance_get();
}


/*!
	\brief Returns range for given type.
	
	\param type The type.
	\return Range of the type.
*/
ptr<num_range> num_range_getter::get_range(ptr<tm_data_type_base> type) {
	ptr<num_range> range;
	
	switch ( type->id_get() ) {
		case DT_INT_8P :
			range = num_range::create("255", false, 3, type->bitwidth_get(), NULL, NULL);
		break;
		
		case DT_INT_8U :
			range = num_range::create("255", false, 3, type->bitwidth_get(), NULL, NULL);
		break;
		
		case DT_INT_8S :
			range = num_range::create("127", true, 3, type->bitwidth_get(), NULL, NULL);
		break;
		
		case DT_INT_16U :
			range = num_range::create("65535", false, 5, type->bitwidth_get(), NULL, NULL);
		break;
		
		case DT_INT_16S :
			range = num_range::create("32767", true, 5, type->bitwidth_get(), NULL, NULL);
		break;
		
		case DT_INT_32U :
			range = num_range::create("4294967295", false, 10, type->bitwidth_get(), NULL, NULL);
		break;
		
		case DT_INT_32S :
			range = num_range::create("2147483647", true, 10, type->bitwidth_get(), NULL, NULL);
		break;
		
		default:
			lassert(false);
		break;
	}
	
	return range;
}

end_package(types);
end_package(md);
end_package(lestes);

