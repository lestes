/*!
	\file
	\brief Type informations.
	\author jaz
*/

#include <lestes/lang/cplus/sem/ss_type.g.hh>
#include <lestes/lang/cplus/sem/ss_type_builtin.g.hh>
#include <lestes/md/types/tm_data_type.g.hh>
#include <lestes/md/types/num_range.g.hh>
#include <lestes/md/types/num_range_getter.g.hh>
#include <lestes/md/types/type_info.g.hh>
#include <lestes/md/types/ss_type2tm_type.g.hh>

#include <regex.h>

package(lestes);
package(md);
package(types);

using namespace ::lestes::md::types;
using namespace ::lestes::lang::cplus::sem;

ptr<type_info> type_info::instance() {
	if ( !singleton_instance_get() ) {
		singleton_instance_set(type_info::create());
	}
	return singleton_instance_get();
}

/*!
	\brief Returns ss_type corresponding to ptrdiff_t type.
*/
ptr<ss_type> type_info::get_ptrdiff_t_type() {
	return ss_type_sint::instance();
}


/*!
	\brief Returns ss_type corresponding to size_t type.
*/
ptr<ss_type> type_info::get_size_t_type() {
	return ss_type_uint::instance();
}

/*!
	\brief Tells whether a value is of given type.
	
	\param The value.
	\param The type.
	\return True if the value is of the type. False otherwise.
*/
bool type_info::does_value_match_type(ucn_string value, ptr<ss_type> type) {
	lassert(type);

	//Get target type fro type.
	ptr<tm_data_type_base> tm_type = type->accept_ss_type2tm_type_gen_base(ss_type2tm_type::instance());
	
	//Non-basic types are not accepted.
	lassert(tm_type->kind_get()==tm_data_type_base::SIMPLE);
	
	//Get range for type.
	ptr<num_range> range = num_range_getter::instance()->get_range(tm_type);
	lassert(range);
	
	switch ( tm_type->id_get() ) {
		case DT_INT_8P :
		case DT_INT_8U :
		case DT_INT_8S :
		case DT_INT_16U :
		case DT_INT_16S :
		case DT_INT_32U :
		case DT_INT_32S :{
			//Get decimal representation.
			lstring decimal_value = get_decimal_representation(value,tm_type);
			
			//Check number length.
			ulint max_digits = range->digits_get();
		
			if ( decimal_value.length() > max_digits ) {
				//Number is too long. Too many digits.
				return false;
			}
			
			/*
				Check if value is less than maximum value for type. 
				This can be done as string comparation, because length of the value
				is the same or shorter than length of maximum.
			*/
			if ( decimal_value.length() == max_digits && decimal_value.compare(range->max_get())>0 ) {
				//Number is out of range.
				return false;
			}
			
			//Check number format.
			char* pattern = "^[0-9]+$";
			
			regex_t re;
			lassert(regcomp(&re,pattern,REG_EXTENDED|REG_NOSUB)==0);
			int status = regexec(&re,decimal_value.c_str(),(size_t)0,NULL,0);
			regfree(&re);
			
			return (status==0);
			
		} break;		
		
		default:
		break;
	}
	lassert(false);
	return false;
}

end_package(types);
end_package(md);
end_package(lestes);

