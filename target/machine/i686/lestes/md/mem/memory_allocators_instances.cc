/*!
	\file
	\brief Implementation of memory allocators for i686 architecture.
	\author jaz
*/

#include <lestes/md/mem/memory_allocators.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>
#include <lestes/lang/cplus/sem/visitor.v.g.hh>
#include <lestes/backend_v2/intercode/pi.g.hh>

package(lestes);
package(md);
package(mem);

using ::lestes::backend_v2::intercode::pi_abstract_function_call;
using ::lestes::backend_v2::intercode::pi_mem_decl;
using ::lestes::lang::cplus::sem::ss_function_declaration;


ptr< local_variable_allocator > local_variable_allocator::instance(ptr<ss_function_declaration> function)
{
	map< srp< ss_function_declaration >, srp< local_variable_allocator > >::iterator it;
	it = singleton_instances_get()->find(function);
	
	if ( it!=singleton_instances_get()->end() ) {
		return it->second;
	} else {
		ptr< local_variable_allocator > new_instance = local_variable_allocator::create(function);
		singleton_instances_get()->insert(::std::pair<srp< ss_function_declaration >, srp< local_variable_allocator> >(function,new_instance));
		return new_instance;
	}
	
	return NULL;	
}

ptr< global_variable_allocator > global_variable_allocator::instance() {
	if ( !singleton_instance ) {
		//A singleton instance has not been created yet. Create it!
		singleton_instance = global_variable_allocator::create();
	}
	//Return instance
	return singleton_instance;
}

end_package(mem);
end_package(md);
end_package(lestes);

