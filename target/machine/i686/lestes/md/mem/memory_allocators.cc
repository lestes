/*!
	\file
	\brief Implementation of memory allocators for i686 architecture.
	\author jaz
*/

#include <lestes/std/set.hh>
#include <lestes/md/common.hh>
#include <lestes/md/types/tm_data_type_base.g.hh>
#include <lestes/md/registers/tm_register_base.g.hh>
#include <lestes/md/tasm/tm_asm.mdg.hh>
#include <lestes/md/symbols/symbol_register.g.hh>
#include <lestes/md/symbols/name_mangler.g.hh>
#include <lestes/md/literals/literal_info_base.g.hh>
#include <lestes/md/types/ss_type2tm_type.g.hh>
#include <lestes/backend_v2/structs/pi_operands.g.hh>
#include <lestes/backend_v2/intercode/pi_mem_factory.g.hh>
#include <lestes/backend_v2/intercode/pi.g.hh>
#include <lestes/backend_v2/intercode/visitor_pi_pi2id.g.hh>
#include <lestes/backend_v2/intercode/visitor_pi_pi2pi_operands.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>
#include <lestes/lang/cplus/sem/ss_type.g.hh>
#include <lestes/md/mem/memory_allocators.g.hh>

package(lestes);
package(md);
package(mem)

using namespace ::lestes::backend_v2::intercode;
using namespace ::lestes::backend_v2::structs;
using namespace ::lestes::md::types;
using namespace ::lestes::lang::cplus::sem;

using ::lestes::md::symbols::name_mangler;
using ::lestes::md::symbols::symbol_register;
using ::lestes::md::types::ss_type2tm_type;
using ::lestes::md::literals::literal_info_base;
using ::lestes::md::tasm::tm_asm;

typedef list< srp< pi_mf_stack > >			pi_mf_stack_list_type;
typedef set< srp< pi_mf_stack > >			pi_mf_stack_set_type;
typedef list< srp< pi_pi > > 				pi_pi_list_type;
typedef list< srp< pi_operand > > 			pi_operand_list_type;
typedef map< srp< ss_object_declaration >, srp< pi_mf_decl > > decl2mf_decl_map_type;
typedef map< srp< ss_object_declaration >, srp< pi_mf_stack > > decl2mf_stack_map_type;
typedef map< srp< pi_mem_factory >, srp< literal_info_base > > mf2pi_li_map_type;
typedef list< srp< pi_mem > > 				pi_mem_list_type;


declare_logger(log);
initialize_logger( log, "memory_allocators", md_logger );

#define STACK_ALIGNMENT 32


/***************************** GLOBAL_VARIABLE_ALLOCATOR *******************************************/

/*!
	\brief Calculates placement of allocated variables in memory.
*/
void global_variable_allocator::calculate_placement() {
	//TODO
}

/*!
	\brief Generates asm declarations for initialized global variables.
	
	\param output Output stream.
*/
void global_variable_allocator::emit_asm_declarations_for_initialized(::std::ostream& output) {
	lassert(output);
	
	decl2mf_decl_map_type::iterator it;
	for(it=variables_get()->begin(); it!=variables_get()->end(); it++) {
	
		mf2pi_li_map_type::iterator it2 = values_get()->find(it->second);
		
		if ( it2==values_get()->end() ) {
			/*
				Variable is unitialized. 
				It is emited by emit_asm_declarations_for_uninitialized call.
			*/
			continue;
		}
		
		lstring code = tm_asm::ent_global_simple_decl_get();
		
		lstring id = name_mangler::instance()->mangle(it->first);
		lstring type = it2->second->get_asm_definition_type();
		lstring value = it2->second->get_asm_definition_val();
		
		code = string_replace(code,"$id",id);
		code = string_replace(code,"$type",type);
		code = string_replace(code, "$value", value);
		output << code;
	}
}

/*!
	\brief Generates asm declarations for uninitialized global variables.
	
	\param output Output stream.
*/
void global_variable_allocator::emit_asm_declarations_for_uninitialized(::std::ostream& output) {
	lassert(output);
	
	decl2mf_decl_map_type::iterator it1;
	for(it1=variables_get()->begin(); it1!=variables_get()->end(); it1++) {
	
		mf2pi_li_map_type::iterator it2 = values_get()->find(it1->second);
		
		if ( it2!=values_get()->end() ) {
			/*
				Variable is itialized.
				It is emited by emit_asm_declarations_for_initialized call.
			*/
			continue;
		}
		
		lstring code = tm_asm::ent_global_simple_decl_get();
		lstring id = name_mangler::instance()->mangle(it1->first);
		lstring resv = tm_asm::ent_space_reserve_get();
		
		ptr<tm_data_type_base> dt = it1->second->type_get();
		
		::std::ostringstream oss;
		oss << dt->bitwidth_get()/8; 
		lstring type = string_replace(resv, "$size", oss.str());
		
		code = string_replace(code, "$id", id);
		code = string_replace(code, "$type", type);
		code = string_replace(code, "$value", "");
		output << code;
	}
}

/*!
	\brief Deallocates space for variable.
	
	The right moment for dealocation is given by two sequencepoints. Dealocation is performed between them.
	
	\param mem The space to be dealocated.
	\param psp The first bound sequencepoint.
	\param nsp The second bound sequencepoint.
*/
void global_variable_allocator::deallocate(ptr<pi_mem_factory>, ptr<pi_sp>, ptr<pi_sp>) {
}

/*!
	\brief Allocates space for variable in global data storage.
	
	\param decl Declaration of the variable.
	\return The memory space for variable.
*/
ptr< pi_mem_factory > global_variable_allocator::allocate(ptr< ss_object_declaration > decl) {
	lassert(decl);
	
	//Check whether variable for the declaration has been already allocated.
	decl2mf_decl_map_type::iterator it = variables_get()->find(decl);
	if ( it!=variables_get()->end()) {
		//Variable has been already allocated!
		return it->second;
	}
	
	//Allocate new global variable.
	ptr<pi_mf_decl> new_var = pi_mf_decl::create(this, decl->type_get()->accept_ss_type2tm_type_gen_base(ss_type2tm_type::instance()), decl);
	
	//Add it to the global variable map.
	(*variables_get())[decl] = new_var;

	//Register new symbol in symbol register
	symbol_register::instance()->register_symbol(decl);
		
	log << "Global variable(" << new_var->uid_get() << ") has been declared for ss_object_declaration(" << decl->uid_get() << ")\n";
	
	return  new_var;
}

/*!
	\brief Initialiaze memory space in global data storage with given value.
	
	\param mem The memory space.
	\param value The value.
*/
void global_variable_allocator::init_variable(ptr<pi_mem_factory> mem, ptr<literal_info_base> value) {
	lassert(mem);
	lassert(value);
	
	//Check  that variable has been regularly allocated by this allocator.
	decl2mf_decl_map_type::iterator it = variables_get()->find(mem.dncast<pi_mf_decl>()->decl_get());
	lassert(it!=variables_get()->end());
	
	//Assign value to variable that occupies space mem.
	(*values_get())[mem] = value;
	
	log << "Global variable(" << mem->uid_get() << ") has been initialized with value " <<  value->get_asm_definition_val()<< "\n";
}


/************************* LOCAL_VARIABLE_ALLOCATOR **********************************************/
/*!
	\brief Deallocates space for variable on the local stack.
	
	The right moment for dealocation is given by two sequencepoints. Dealocation is performed between them.
	
	\param mem The space to be dealocated.
	\param psp The first bound sequencepoint.
	\param nsp The second bound sequencepoint.
*/
void local_variable_allocator::deallocate(ptr<pi_mem_factory>, ptr<pi_sp>, ptr<pi_sp>) {
}

/*!
	\brief Allocates space for temporal variable on the local stack.
	
	\param type Type of variable.
	\return The memory space for variable.
*/
ptr< pi_mem_factory > local_variable_allocator::allocate_tmp(ptr< tm_data_type_base > type) {
	lassert(type);
	
	//Create new pi_mem.
	ptr<pi_mf_stack> new_var = pi_mf_stack::create(this,type,0);
	
	//Add it to the local variable list for the function.
	tmps->push_back(new_var);	
	
	return  new_var;
}


/*!
	\brief Allocates space for regular variable on the local stack.
	
	\param decl Declaration of the variable.
	\return The memory space for variable.
*/

ptr< pi_mem_factory > local_variable_allocator::allocate(ptr< ss_object_declaration > decl) {
	lassert(decl);

	//Check whether variable for the declaration has been already allocated.
	decl2mf_stack_map_type::iterator it = variables_get()->find(decl);
	if ( it!=variables_get()->end()) {
		//Variable has been already allocated.
		return it->second;
	}
	
	//Allocate new local variable.
	ptr<pi_mf_stack> new_var = pi_mf_stack::create(this, decl->type_get()->accept_ss_type2tm_type_gen_base(ss_type2tm_type::instance()), 0);
	
	//Add it to the local variable map.
	(*variables_get())[decl] = new_var;
	
	return  new_var;
}


/*
	\brief Calculates placement of local variables and parameters in memory.
	
	Note:
	Stack arrangement before function call
	 ------------------ EBP
		local vars
	------------------- EBP+tmps_space_start_offset
		tmp local vars
	-------------------	EBP+pars_space_start_offset-preserved_space_size
		EDI, ESI, EBX
	------------------- EBP+pars_space_start_offset
		parameters
	------------------- ESP	
	
	\param pi_list Function's body.
*/
void local_variable_allocator::calculate_placement() {
	if ( placement_calculated_get() ) {
		lassert(false);
		return;
	}

	preserved_space_size_set(0);
	calculate_vars_placement();
	calculate_tmps_placement();
	placement_calculated_set(true);
}


/*!
	\brief Calculates placement of regular local variables in memory.
*/
void local_variable_allocator::calculate_vars_placement() {	
	
	vars_space_start_offset_set(-preserved_space_size_get());
	
	t_ssize offset = vars_space_start_offset_get();
	
	decl2mf_stack_map_type::iterator it;	
	for(it=variables->begin(); it!=variables->end(); it++) {
		ptr<pi_mf_stack> mem = it->second;
		
		ulint item_size = mem->type_get()->bitwidth_get();
		
		if ( item_size % STACK_ALIGNMENT != 0 ){
			item_size = (item_size/STACK_ALIGNMENT + 1)*STACK_ALIGNMENT;
		}
				
		offset -= item_size;
		
		mem->offset_set(offset);
	}
	
	tmps_space_start_offset_set(offset);
}

/*!
	\brief Calculates placement of temporal local variables in memory.
*/
void local_variable_allocator::calculate_tmps_placement() {	
	
	t_ssize offset = tmps_space_start_offset_get();
	
	pi_mf_stack_list_type::iterator it;	
	for(it=tmps->begin(); it!=tmps->end(); it++) {
		ptr<pi_mf_stack> mem = *it;
		
		ulint item_size = mem->type_get()->bitwidth_get();
		
		if ( item_size % STACK_ALIGNMENT != 0 ){
			item_size = (item_size/STACK_ALIGNMENT + 1)*STACK_ALIGNMENT;
		}
		offset -= item_size;
		
		mem->offset_set(offset);
	}
	
	local_end_offset_set(offset);
}

end_package(mem)
end_package(md);
end_package(lestes);

