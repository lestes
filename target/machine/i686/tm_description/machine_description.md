<?xml version="1.0"?>
<TargetArchitecture name="i686" xsi:schemaLocation="http://lestes.jikos.cz/machinedescription ../../schema/target_architecture.xsd" xmlns="http://lestes.jikos.cz/machinedescription" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

<DataTypes>
		<DataType bitwidth="8" id="DT_COND"  format="FORMAT_C2" alignment="8" asm="BYTE" asm_decl="db"/>
		<DataType bitwidth="8" id="DT_BOOL" format="FORMAT_C2" mapped_types="ss_bool" retreg="R_AL" alignment="8" asm="BYTE" asm_decl="db"/>
		<DataType bitwidth="8" id="DT_INT_8P" format="FORMAT_C2" mapped_types="ss_type_pchar" retreg="R_AL" alignment="8" asm="BYTE" asm_decl="db"/>
		<DataType bitwidth="8" id="DT_INT_8U" format="FORMAT_C2" mapped_types="ss_type_uchar" retreg="R_AL" alignment="8" asm="BYTE" asm_decl="db"/>
		<DataType bitwidth="8" id="DT_INT_8S" format="FORMAT_C2" mapped_types="ss_type_schar" retreg="R_AL" alignment="8" asm="BYTE" asm_decl="db"/>
		<DataType bitwidth="16" id="DT_INT_16S" format="FORMAT_C2" mapped_types="ss_type_sshort" retreg="R_AX" alignment="16" asm="WORD" asm_decl="dw"/>
		<DataType bitwidth="16" id="DT_INT_16U" format="FORMAT_C2" mapped_types="ss_type_ushort" retreg="R_AX" alignment="16" asm="WORD" asm_decl="dw"/>
		<DataType bitwidth="32" id="DT_INT_32S" format="FORMAT_C2" mapped_types="ss_type_sint ss_type_slong" retreg="R_EAX" alignment="32" asm="DWORD" asm_decl="dd"/>
		<DataType bitwidth="32" id="DT_INT_32U" format="FORMAT_C2" mapped_types="ss_type_uint ss_type_ulong" retreg="R_EAX" alignment="32" asm="DWORD" asm_decl="dd"/>
		
		<DataTypeGroup id="DTG_INT_8" types="DT_INT_8P DT_INT_8U DT_INT_8S" />
		<DataTypeGroup id="DTG_INT_8U" types="DT_INT_8P DT_INT_8U" />
		<DataTypeGroup id="DTG_INT_16" types="DT_INT_16U DT_INT_16S" />
		<DataTypeGroup id="DTG_INT_32" types="DT_INT_32U DT_INT_32S" />
		<DataTypeGroup id="DTG_INT" types="DTG_INT_8 DTG_INT_16 DTG_INT_32" />
		<DataTypeGroup id="DTG_INT_S" types="DT_INT_8S DT_INT_16S DT_INT_32S" />
		<DataTypeGroup id="DTG_INT_U" types="DT_INT_8U DT_INT_8P DT_INT_16U DT_INT_32U" />
		
</DataTypes>


<Registers>
		<Register id="R_AL" bitwidth="8" types="DTG_INT_8 DT_BOOL" aliases="R_AX R_EAX" asm="AL" move_templates="TEM_REGISTER_MOVE_1"/>
		<Register id="R_AH" bitwidth="8" types="DTG_INT_8  DT_BOOL DT_COND" aliases="R_AX R_EAX" asm="AH" move_templates="TEM_REGISTER_MOVE_1"/>
		<Register id="R_AX" bitwidth="16" types="DTG_INT_16" aliases="R_AH R_AL R_EAX" asm="AX" move_templates="TEM_REGISTER_MOVE_1"/>
		<Register id="R_EAX" bitwidth="32" types="DTG_INT_32" aliases="R_AH R_AL R_AX" asm="EAX" move_templates="TEM_REGISTER_MOVE_1"/>
		<Register id="R_BL" bitwidth="8" types="DTG_INT_8  DT_BOOL" aliases="R_BX R_EBX" asm="BL" move_templates="TEM_REGISTER_MOVE_1"/>
		<Register id="R_BH" bitwidth="8" types="DTG_INT_8  DT_BOOL" aliases="R_BX R_EBX" asm="BH" move_templates="TEM_REGISTER_MOVE_1"/>
		<Register id="R_BX" bitwidth="16" types="DTG_INT_16" aliases="R_BH R_BL R_EBX" asm="BX" move_templates="TEM_REGISTER_MOVE_1"/>
		<Register id="R_EBX" bitwidth="32" types="DTG_INT_32" aliases="R_BH R_BL R_BX" asm="EBX" move_templates="TEM_REGISTER_MOVE_1"/>
		<Register id="R_CL" bitwidth="8" types="DTG_INT_8  DT_BOOL" aliases="R_CX R_ECX" asm="CL" move_templates="TEM_REGISTER_MOVE_1"/>
		<Register id="R_CH" bitwidth="8" types="DTG_INT_8  DT_BOOL" aliases="R_CX R_ECX" asm="CH" move_templates="TEM_REGISTER_MOVE_1"/>
		<Register id="R_CX" bitwidth="16" types="DTG_INT_16" aliases="R_CL R_CH R_ECX" asm="CX" move_templates="TEM_REGISTER_MOVE_1"/>
		<Register id="R_ECX" bitwidth="32" types="DTG_INT_32" aliases="R_CL R_CH R_CX" asm="ECX" move_templates="TEM_REGISTER_MOVE_1"/>
		<Register id="R_DL" bitwidth="8" types="DTG_INT_8  DT_BOOL" aliases="R_DX R_EDX" asm="DL" move_templates="TEM_REGISTER_MOVE_1"/>
		<Register id="R_DH" bitwidth="8" types="DTG_INT_8  DT_BOOL" aliases="R_DX R_EDX" asm="DH" move_templates="TEM_REGISTER_MOVE_1"/>
		<Register id="R_DX" bitwidth="16" types="DTG_INT_16" aliases="R_DL R_DH R_EDX" asm="DX" move_templates="TEM_REGISTER_MOVE_1"/>
		<Register id="R_EDX" bitwidth="32" types="DTG_INT_32" aliases="R_DL R_DH R_DX" asm="EDX" move_templates="TEM_REGISTER_MOVE_1"/>
		<Register id="R_DI" bitwidth="16" types="DTG_INT_16" aliases="R_EDI" asm="DI" move_templates="TEM_REGISTER_MOVE_1"/>
		<Register id="R_EDI" bitwidth="32" types="DTG_INT_32" aliases="R_DI" asm="EDI" move_templates="TEM_REGISTER_MOVE_1"/>
		<Register id="R_SI" bitwidth="16" types="DTG_INT_16" aliases="R_ESI" asm="SI" move_templates="TEM_REGISTER_MOVE_1"/>
		<Register id="R_ESI" bitwidth="32" types="DTG_INT_32" aliases="R_SI" asm="ESI" move_templates="TEM_REGISTER_MOVE_1"/>
		
		<Register id="R_COND" bitwidth="8" types="DT_COND" move_templates="TEM_REGISTER_MOVE_2 TEM_REGISTER_MOVE_3 TEM_REGISTER_MOVE_4"/>
		
		<Register id="R_ESP" bitwidth="32" types="DT_INT_32U" asm="ESP" move_templates="TEM_REGISTER_MOVE_5" flags="RF_NOSPILL"/>
		<Register id="R_EBP" bitwidth="32" types="DT_INT_32U" asm="EBP" move_templates="TEM_REGISTER_MOVE_5" flags="RF_NOSPILL"/>
		
		<RegisterGroup id="RG_GP_8" registers="R_AL R_AH R_BL R_BH R_CL R_CH R_DL R_DH" />
		<RegisterGroup id="RG_GP_16" registers="R_AX R_BX R_CX R_DX R_DI R_SI" />
		<RegisterGroup id="RG_GP_32" registers="R_EAX R_EBX R_ECX R_EDX R_EDI R_ESI" />
		<RegisterGroup id="RG_GP" registers="RG_GP_32 RG_GP_16 RG_GP_8" />
		


</Registers>


<Instructions>

	<Instruction id="INSTRUCTION_MOV__1" flags="IF_COPY">
		<In>
			<Reg id="I_1" registers="RG_GP" types="DTG_INT DT_COND DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP" types="DTG_INT DT_COND DT_BOOL" />
		</Out>
		<Exec etime="10" units="U_ALU"/>
		<Asm>mov $O_1, $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MOV__2" flags="IF_STORE">
		<In>
			<Reg id="I_1" registers="RG_GP" types="DTG_INT DT_COND DT_BOOL" />
		</In>
		<Out>
			<Mem id="O_1" types="DTG_INT DT_COND DT_BOOL" />
		</Out>	
		<Exec etime="30" units="U_ALU U_MEM_STORE"/>
		<Asm>mov $TYPE_O_1 $O_1, $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MOV__3" flags="IF_LOAD">
		<In>
			<Mem id="I_1" types="DTG_INT DT_COND DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP" types="DTG_INT DT_COND DT_BOOL" />
		</Out>	
		<Exec etime="30" units="U_ALU U_MEM_LOAD"/>
		<Asm>mov $O_1, $TYPE_I_1 $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MOV__4" flags="IF_LOAD | IF_STORE">
		<In>
			<Imm id="I_1" types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Mem id="O_1" types="DTG_INT DT_BOOL" />
		</Out>	
		<Exec etime="30" units="U_ALU U_MEM_STORE"/>
		<Asm>mov $TYPE_O_1 $O_1, $TYPE_I_1 $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MOV__5" flags="IF_LOAD">
		<In>
			<Imm id="I_1" types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP" types="DTG_INT DT_BOOL" />
		</Out>	
		<Exec etime="10" units="U_ALU"/>
		<Asm>mov $O_1, $TYPE_I_1 $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MOV__7" flags="IF_STORE">
		<In>
			<Reg id="I_1" registers="RG_GP" types="DTG_INT DT_COND DT_BOOL" />
			<Reg id="I_2" registers="RG_GP_32" types="DT_INT_32U" />
		</In>
		<Out>
			<Mem id="O_1" types="DTG_INT DT_COND DT_BOOL" />
		</Out>	
		<Exec etime="30" units="U_ALU U_MEM_STORE"/>
		<Asm>mov $TYPE_O_1 $O_1, $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MOV__71" flags="IF_STORE">
		<In>
			<Imm id="I_1" types="DTG_INT DT_COND DT_BOOL" />
			<Reg id="I_2" registers="RG_GP_32" types="DT_INT_32U" />
		</In>
		<Out>
			<Mem id="O_1" types="DTG_INT DT_COND DT_BOOL" />
		</Out>	
		<Exec etime="30" units="U_ALU U_MEM_STORE"/>
		<Asm>mov $TYPE_O_1 $O_1, $TYPE_I_1 $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MOV__8"  flags="IF_LOAD">
		<In>
			<Mem id="I_1" types="DTG_INT DT_COND DT_BOOL" />
			<Reg id="I_2" registers="RG_GP_32" types="DT_INT_32U" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP" types="DTG_INT DT_COND DT_BOOL" />
		</Out>	
		<Exec etime="30" units="U_ALU U_MEM_LOAD"/>
		<Asm>mov $O_1, $TYPE_I_1 $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_SYSMOV__9" flags="IF_COPY">
		<In>
			<Reg id="I_1" registers="R_ESP R_EBP" types="DTG_INT" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_ESP R_EBP" types="DTG_INT" />
		</Out>
		<Exec etime="10" units="U_ALU"/>
		<Asm>mov $O_1, $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_SYSMOV__10" flags="IF_STORE">
		<In>
			<Reg id="I_1" registers="R_ESP R_EBP" types="DTG_INT" />
		</In>
		<Out>
			<Mem id="O_1" types="DTG_INT" />
		</Out>	
		<Exec etime="30" units="U_ALU U_MEM_STORE"/>
		<Asm>mov $TYPE_O_1 $O_1, $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_SYSMOV__11" flags="IF_LOAD">
		<In>
			<Mem id="I_1" types="DTG_INT" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_ESP R_EBP" types="DTG_INT" />
		</Out>	
		<Exec etime="30" units="U_ALU U_MEM_LOAD"/>
		<Asm>mov $O_1, $TYPE_I_1 $I_1</Asm>	
	</Instruction>
	<Instruction id="INSTRUCTION_SAHF__1">
		<In>
			<Reg id="I_1" registers="R_AH" types="DT_COND" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_COND" types="DT_COND" />
		</Out>	
		<Exec etime="10" units="U_ALU"/>
		<Asm>sahf</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_LAHF__1">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_AH" types="DT_COND" />
		</Out>	
		<Exec etime="10" units="U_ALU"/>
		<Asm>lahf</Asm>	
	</Instruction>
	
	
	<Instruction id="INSTRUCTION_ADD__1">
		<In>
			<Reg id="I_1" registers="RG_GP"  types="DTG_INT" />
			<Reg id="I_2" registers="RG_GP"  types="DTG_INT" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>
		<Asm>add $O_1, $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_ADD__11">
		<In>
			<Reg id="I_1" registers="RG_GP"  types="DTG_INT" />
			<Mem id="I_2" types="DTG_INT" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="30" units="U_ALU"/>
		<Asm>add $O_1, $TYPE_I_2 $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_ADD__12">
		<In>
			<Mem id="I_1" types="DTG_INT" />
			<Reg id="I_2" registers="RG_GP"  types="DTG_INT" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT" destroys="I_2"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="30" units="U_ALU"/>
		<Asm>add $O_1, $TYPE_I_1 $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_ADD__13">
		<In>
			<Reg id="I_1" registers="RG_GP"  types="DTG_INT" />
			<Imm id="I_2" types="DTG_INT" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>
		<Asm>add $O_1, $TYPE_I_2 $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_ADD__14">
		<In>
			<Imm id="I_1" types="DTG_INT" />
			<Reg id="I_2" registers="RG_GP"  types="DTG_INT" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT" destroys="I_2"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>
		<Asm>add $O_1, $TYPE_I_1 $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_SUB__1">
		<In>
			<Reg id="I_1" registers="RG_GP"  types="DTG_INT" />
			<Reg id="I_2" registers="RG_GP"  types="DTG_INT" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>
		<Asm>sub $O_1, $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_SUB__11">
		<In>
			<Reg id="I_1" registers="RG_GP"  types="DTG_INT" />
			<Mem id="I_2" types="DTG_INT" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="30" units="U_ALU"/>
		<Asm>sub $O_1, $TYPE_I_2 $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_SUB__12">
		<In>
			<Reg id="I_1" registers="RG_GP"  types="DTG_INT" />
			<Imm id="I_2" types="DTG_INT" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>
		<Asm>sub $O_1, $TYPE_I_2 $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_IMUL__1">
		<In>
			<Reg id="I_1" registers="R_AL"  types="DT_INT_8S" />
			<Reg id="I_2" registers="RG_GP_8 -R_AL"  types="DT_INT_8S" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_AL"  types="DT_INT_8S" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="100" units="U_FP_MUL"/>
		<Asm>imul $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_IMUL__2">
		<In>
			<Reg id="I_1" registers="RG_GP_16 RG_GP_32"  types="DT_INT_16S DT_INT_32S" />
			<Reg id="I_2" registers="RG_GP_16 RG_GP_32"  types="DT_INT_16S DT_INT_32S" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_16 RG_GP_32"  types="DT_INT_16S DT_INT_32S" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="100" units="U_FP_MUL"/>
		<Asm>imul $O_1, $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_IMUL__21">
		<In>
			<Reg id="I_1" registers="RG_GP_16 RG_GP_32"  types="DT_INT_16S DT_INT_32S" />
			<Imm id="I_2" types="DT_INT_16S DT_INT_32S" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_16 RG_GP_32"  types="DT_INT_16S DT_INT_32S" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="100" units="U_FP_MUL"/>
		<Asm>imul $O_1, $TYPE_I_2 $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_IMUL__22">
		<In>
			<Imm id="I_1" types="DT_INT_16S DT_INT_32S" />
			<Reg id="I_2" registers="RG_GP_16 RG_GP_32"  types="DT_INT_16S DT_INT_32S" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_16 RG_GP_32"  types="DT_INT_16S DT_INT_32S" destroys="I_2"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="100" units="U_FP_MUL"/>
		<Asm>imul $O_1, $TYPE_I_1 $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_IMUL__23">
		<In>
			<Reg id="I_1" registers="RG_GP_16 RG_GP_32"  types="DT_INT_16S DT_INT_32S" />
			<Mem id="I_2" types="DT_INT_16S DT_INT_32S" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_16 RG_GP_32"  types="DT_INT_16S DT_INT_32S" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="120" units="U_FP_MUL"/>
		<Asm>imul $O_1, $TYPE_I_2 $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_IMUL__24">
		<In>
			<Mem id="I_1" types="DT_INT_16S DT_INT_32S" />
			<Reg id="I_2" registers="RG_GP_16 RG_GP_32"  types="DT_INT_16S DT_INT_32S" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_16 RG_GP_32"  types="DT_INT_16S DT_INT_32S" destroys="I_2"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="120" units="U_FP_MUL"/>
		<Asm>imul $O_1, $TYPE_I_1 $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MUL__1">
		<In>
			<Reg id="I_1" registers="R_AL"  types="DT_INT_8U" />
			<Reg id="I_2" registers="RG_GP_8 -R_AL"  types="DT_INT_8U" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_AL"  types="DT_INT_8U" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="100" units="U_FP_MUL"/>
		<Asm>mul $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MUL__11">
		<In>
			<Reg id="I_1" registers="R_AL"  types="DT_INT_8U" />
			<Mem id="I_2" types="DT_INT_8U" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_AL"  types="DT_INT_8U" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="120" units="U_FP_MUL"/>
		<Asm>mul $TYPE_I_2 $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MUL__12">
		<In>
			<Mem id="I_1" types="DT_INT_8U" />
			<Reg id="I_2" registers="R_AL"  types="DT_INT_8U" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_AL"  types="DT_INT_8U" destroys="I_2"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="120" units="U_FP_MUL"/>
		<Asm>mul $TYPE_I_1 $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MUL__2">
		<In>
			<Reg id="I_1" registers="R_AX"  types="DT_INT_16U" />
			<Reg id="I_2" registers="RG_GP_16 -R_AX -R_DX"  types="DT_INT_16U" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_AX"  types="DT_INT_16U" destroys="I_1" />
			<Reg id="O_2" registers="R_DX"  types="DT_INT_16U"/>
			<Reg id="O_3" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="100" units="U_FP_MUL"/>
		<Asm>mul $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MUL__21">
		<In>
			<Reg id="I_1" registers="R_AX"  types="DT_INT_16U" />
			<Mem id="I_2" types="DT_INT_16U" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_AX"  types="DT_INT_16U" destroys="I_1" />
			<Reg id="O_2" registers="R_DX"  types="DT_INT_16U"/>
			<Reg id="O_3" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="120" units="U_FP_MUL"/>
		<Asm>mul $TYPE_I_2 $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MUL__22">
		<In>
			<Mem id="I_1" types="DT_INT_16U" />
			<Reg id="I_2" registers="R_AX"  types="DT_INT_16U" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_AX"  types="DT_INT_16U" destroys="I_2" />
			<Reg id="O_2" registers="R_DX"  types="DT_INT_16U"/>
			<Reg id="O_3" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="120" units="U_FP_MUL"/>
		<Asm>mul $TYPE_I_1 $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MUL__3">
		<In>
			<Reg id="I_1" registers="R_EAX"  types="DT_INT_32U" />
			<Reg id="I_2" registers="RG_GP_32 -R_EAX -R_EDX"  types="DT_INT_32U" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_EAX"  types="DT_INT_32U" destroys="I_1" />
			<Reg id="O_2" registers="R_EDX"  types="DT_INT_32U"/>
			<Reg id="O_3" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="100" units="U_FP_MUL"/>
		<Asm>mul $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MUL__31">
		<In>
			<Reg id="I_1" registers="R_EAX"  types="DT_INT_32U" />
			<Mem id="I_2" types="DT_INT_32U" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_EAX"  types="DT_INT_32U" destroys="I_1" />
			<Reg id="O_2" registers="R_EDX"  types="DT_INT_32U"/>
			<Reg id="O_3" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="120" units="U_FP_MUL"/>
		<Asm>mul $TYPE_I_2 $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MUL__32">
		<In>
			<Mem id="I_1" types="DT_INT_32U" />
			<Reg id="I_2" registers="R_EAX"  types="DT_INT_32U" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_EAX"  types="DT_INT_32U" destroys="I_2" />
			<Reg id="O_2" registers="R_EDX"  types="DT_INT_32U"/>
			<Reg id="O_3" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="120" units="U_FP_MUL"/>
		<Asm>mul $TYPE_I_1 $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_CBW__1">
		<In>
			<Reg id="I_1" registers="R_AL" types="DT_INT_8S" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_AX" types="DT_INT_16S" destroys="I_1"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>
		<Asm>cbw</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_CWD__1">
		<In>
			<Reg id="I_1" registers="R_AX" types="DT_INT_16S" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_DX" types="DT_INT_16S" />
			<Reg id="O_2" registers="R_AX" types="DT_INT_16S" destroys="I_1"/>
			
		</Out>
		<Exec etime="10" units="U_ALU"/>
		<Asm>cwd</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_CDQ__1">
		<In>
			<Reg id="I_1" registers="R_EAX" types="DT_INT_32S" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_EDX" types="DT_INT_32S" />
			<Reg id="O_2" registers="R_EAX" types="DT_INT_32S" destroys="I_1"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>
		<Asm>cdq</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_DIV__1">
		<In>
			<Reg id="I_1" registers="R_AH" types="DT_INT_8U" />
			<Reg id="I_2" registers="R_AL" types="DT_INT_8U" />
			<Reg id="I_3" registers="RG_GP_8 -R_AH -R_AL" types="DT_INT_8U" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_AL" types="DT_INT_8U" destroys="I_2"/>
			<Reg id="O_2" registers="R_AH" types="DT_INT_8U" destroys="I_1" />
			<Reg id="O_3" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="700" units="U_FP_DIV"/>
		<Asm>div $I_3</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_DIV__11">
		<In>
			<Reg id="I_1" registers="R_AH" types="DT_INT_8U" />
			<Reg id="I_2" registers="R_AL" types="DT_INT_8U" />
			<Mem id="I_3" types="DT_INT_8U" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_AL" types="DT_INT_8U" destroys="I_2"/>
			<Reg id="O_2" registers="R_AH" types="DT_INT_8U" destroys="I_1" />
			<Reg id="O_3" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="720" units="U_FP_DIV"/>
		<Asm>div $TYPE_I_3 $I_3</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_DIV__2">
		<In>
			<Reg id="I_1" registers="R_DX" types="DT_INT_16U" />
			<Reg id="I_2" registers="R_AX" types="DT_INT_16U" />
			<Reg id="I_3" registers="RG_GP_16 -R_AX -R_DX" types="DT_INT_16U" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_AX" types="DT_INT_16U" destroys="I_2"/>
			<Reg id="O_2" registers="R_DX" types="DT_INT_16U" destroys="I_1" />
			<Reg id="O_3" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="700" units="U_FP_DIV"/>
		<Asm>div $I_3</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_DIV__21">
		<In>
			<Reg id="I_1" registers="R_DX" types="DT_INT_16U" />
			<Reg id="I_2" registers="R_AX" types="DT_INT_16U" />
			<Mem id="I_3" types="DT_INT_16U" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_AX" types="DT_INT_16U" destroys="I_2"/>
			<Reg id="O_2" registers="R_DX" types="DT_INT_16U" destroys="I_1" />
			<Reg id="O_3" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="720" units="U_FP_DIV"/>
		<Asm>div $TYPE_I_3 $I_3</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_DIV__3">
		<In>
			<Reg id="I_1" registers="R_EDX" types="DT_INT_32U" />
			<Reg id="I_2" registers="R_EAX" types="DT_INT_32U" />
			<Reg id="I_3" registers="RG_GP_32 -R_EAX -R_EDX" types="DT_INT_32U" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_EAX" types="DT_INT_32U" destroys="I_2"/>
			<Reg id="O_2" registers="R_EDX" types="DT_INT_32U" destroys="I_1" />
			<Reg id="O_3" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="700" units="U_FP_DIV"/>
		<Asm>div $I_3</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_DIV__31">
		<In>
			<Reg id="I_1" registers="R_EDX" types="DT_INT_32U" />
			<Reg id="I_2" registers="R_EAX" types="DT_INT_32U" />
			<Mem id="I_3" types="DT_INT_32U" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_EAX" types="DT_INT_32U" destroys="I_2"/>
			<Reg id="O_2" registers="R_EDX" types="DT_INT_32U" destroys="I_1" />
			<Reg id="O_3" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="720" units="U_FP_DIV"/>
		<Asm>div $TYPE_I_3 $I_3</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_NULLAH__1">
		<Out>
			<Reg id="O_1" registers="R_AH" types="DT_INT_8U"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>xor $O_1, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_NULLDX__1">
		<Out>
			<Reg id="O_1" registers="R_DX" types="DT_INT_16U"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>xor $O_1, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_NULLEDX__1">
		<Out>
			<Reg id="O_1" registers="R_EDX" types="DT_INT_32U"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>xor $O_1, $O_1</Asm>		
	</Instruction>
	
	<Instruction id="INSTRUCTION_IDIV__1">
		<In>
			<Reg id="I_1" registers="R_AX" types="DT_INT_16S" />
			<Reg id="I_2" registers="RG_GP_8 -R_AL -R_AH" types="DT_INT_8S" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_AL" types="DT_INT_8S" destroys="I_1"/>
			<Reg id="O_2" registers="R_AH" types="DT_INT_8S" destroys="I_1" />
			<Reg id="O_3" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="700" units="U_FP_DIV"/>	
		<Asm>idiv $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_IDIV__11">
		<In>
			<Reg id="I_1" registers="R_AX" types="DT_INT_16S" />
			<Mem id="I_2" types="DT_INT_8S" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_AL" types="DT_INT_8S" destroys="I_1"/>
			<Reg id="O_2" registers="R_AH" types="DT_INT_8S" destroys="I_1" />
			<Reg id="O_3" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="720" units="U_FP_DIV"/>	
		<Asm>idiv $TYPE_I_2 $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_IDIV__2">
		<In>
			<Reg id="I_1" registers="R_DX" types="DT_INT_16S" />
			<Reg id="I_2" registers="R_AX" types="DT_INT_16S" />
			<Reg id="I_3" registers="RG_GP_16 -R_AX -R_DX" types="DT_INT_16S" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_AX" types="DT_INT_16S" destroys="I_2"/>
			<Reg id="O_2" registers="R_DX" types="DT_INT_16S" destroys="I_1" />
			<Reg id="O_3" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="700" units="U_FP_DIV"/>	
		<Asm>idiv $I_3</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_IDIV__21">
		<In>
			<Reg id="I_1" registers="R_DX" types="DT_INT_16S" />
			<Reg id="I_2" registers="R_AX" types="DT_INT_16S" />
			<Mem id="I_3" types="DT_INT_16S" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_AX" types="DT_INT_16S" destroys="I_2"/>
			<Reg id="O_2" registers="R_DX" types="DT_INT_16S" destroys="I_1" />
			<Reg id="O_3" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="720" units="U_FP_DIV"/>	
		<Asm>idiv $TYPE_I_3 $I_3</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_IDIV__3">
		<In>
			<Reg id="I_1" registers="R_EDX" types="DT_INT_32S" />
			<Reg id="I_2" registers="R_EAX" types="DT_INT_32S" />
			<Reg id="I_3" registers="RG_GP_32 -R_EAX -R_EDX" types="DT_INT_32S" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_EAX" types="DT_INT_32S" destroys="I_2"/>
			<Reg id="O_2" registers="R_EDX" types="DT_INT_32S" destroys="I_1" />
			<Reg id="O_3" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="700" units="U_FP_DIV"/>	
		<Asm>idiv $I_3</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_IDIV__31">
		<In>
			<Reg id="I_1" registers="R_EDX" types="DT_INT_32S" />
			<Reg id="I_2" registers="R_EAX" types="DT_INT_32S" />
			<Mem id="I_3" types="DT_INT_32S" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_EAX" types="DT_INT_32S" destroys="I_2"/>
			<Reg id="O_2" registers="R_EDX" types="DT_INT_32S" destroys="I_1" />
			<Reg id="O_3" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="720" units="U_FP_DIV"/>	
		<Asm>idiv $TYPE_I_3 $I_3</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_NEG__1">
		<In>
			<Reg id="I_1" registers="RG_GP"  types="DTG_INT" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP" types="DTG_INT" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>neg $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_SHL__1">
		<In>
			<Reg id="I_1" registers="RG_GP -R_CL"  types="DTG_INT" />
			<Reg id="I_2" registers="R_CL"  types="DTG_INT_8" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP -R_CL" types="DTG_INT" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>shl $I_1, $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_SHR__1">
		<In>
			<Reg id="I_1" registers="RG_GP -R_CL"  types="DTG_INT" />
			<Reg id="I_2" registers="R_CL"  types="DTG_INT_8" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP -R_CL" types="DTG_INT" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>shr $I_1, $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_SAL__1">
		<In>
			<Reg id="I_1" registers="RG_GP -R_CL"  types="DTG_INT" />
			<Reg id="I_2" registers="R_CL"  types="DTG_INT_8" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP -R_CL" types="DTG_INT" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>sal $I_1, $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_SAR__1">
		<In>
			<Reg id="I_1" registers="RG_GP -R_CL"  types="DTG_INT" />
			<Reg id="I_2" registers="R_CL"  types="DTG_INT_8" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP -R_CL" types="DTG_INT" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>sar $I_1, $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_AND__1">
		<In>
			<Reg id="I_1" registers="RG_GP"  types="DTG_INT DT_BOOL" />
			<Reg id="I_2" registers="RG_GP"  types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT DT_BOOL" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10"  units="U_ALU"/>
		<Asm>and $O_1, $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_AND__11">
		<In>
			<Reg id="I_1" registers="RG_GP"  types="DTG_INT DT_BOOL" />
			<Imm id="I_2" types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT DT_BOOL" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10"  units="U_ALU"/>
		<Asm>and $O_1, $TYPE_I_2 $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_AND__12">
		<In>
			<Imm id="I_1" types="DTG_INT DT_BOOL" />
			<Reg id="I_2" registers="RG_GP"  types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT DT_BOOL" destroys="I_2"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10"  units="U_ALU"/>
		<Asm>and $O_1, $TYPE_I_1 $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_AND__13">
		<In>
			<Reg id="I_1" registers="RG_GP"  types="DTG_INT DT_BOOL" />
			<Mem id="I_2" types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT DT_BOOL" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="30"  units="U_ALU"/>
		<Asm>and $O_1, $TYPE_I_2 $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_AND__14">
		<In>
			<Mem id="I_1" types="DTG_INT DT_BOOL" />
			<Reg id="I_2" registers="RG_GP"  types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT DT_BOOL" destroys="I_2"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="30"  units="U_ALU"/>
		<Asm>and $O_1, $TYPE_I_1 $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_AND__2">
		<In>
			<Reg id="I_1" registers="RG_GP_8"  types="DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DT_INT" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10"  units="U_ALU"/>
		<Asm>and $O_1, 0x1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_OR__1">
		<In>
			<Reg id="I_1" registers="RG_GP"  types="DTG_INT DT_BOOL" />
			<Reg id="I_2" registers="RG_GP"  types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT DT_BOOL" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10"  units="U_ALU"/>
		<Asm>or $O_1, $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_OR__11">
		<In>
			<Reg id="I_1" registers="RG_GP"  types="DTG_INT DT_BOOL" />
			<Imm id="I_2" types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT DT_BOOL" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10"  units="U_ALU"/>
		<Asm>or $O_1, $TYPE_I_2 $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_OR__12">
		<In>
			<Imm id="I_1" types="DTG_INT DT_BOOL" />
			<Reg id="I_2" registers="RG_GP"  types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT DT_BOOL" destroys="I_2"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10"  units="U_ALU"/>
		<Asm>or $O_1, $TYPE_I_1 $I_1</Asm>	
	</Instruction>
	
	
	<Instruction id="INSTRUCTION_OR__13">
		<In>
			<Reg id="I_1" registers="RG_GP"  types="DTG_INT DT_BOOL" />
			<Mem id="I_2" types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT DT_BOOL" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="30"  units="U_ALU"/>
		<Asm>or $O_1, $TYPE_I_2 $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_OR__14">
		<In>
			<Mem id="I_1" types="DTG_INT DT_BOOL" />
			<Reg id="I_2" registers="RG_GP"  types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT DT_BOOL" destroys="I_2"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="30"  units="U_ALU"/>
		<Asm>or $O_1, $TYPE_I_1 $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_XOR__1">
		<In>
			<Reg id="I_1" registers="RG_GP"  types="DTG_INT DT_BOOL" />
			<Reg id="I_2" registers="RG_GP"  types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT DT_BOOL" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10"  units="U_ALU"/>
		<Asm>xor $O_1, $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_XOR__11">
		<In>
			<Reg id="I_1" registers="RG_GP"  types="DTG_INT DT_BOOL" />
			<Imm id="I_2" types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT DT_BOOL" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10"  units="U_ALU"/>
		<Asm>xor $O_1, $TYPE_I_2 $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_XOR__12">
		<In>
			<Imm id="I_1"  types="DTG_INT DT_BOOL" />
			<Reg id="I_2" registers="RG_GP"  types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT DT_BOOL" destroys="I_2"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10"  units="U_ALU"/>
		<Asm>xor $O_1, $TYPE_I_1 $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_XOR__13">
		<In>
			<Reg id="I_1" registers="RG_GP"  types="DTG_INT DT_BOOL" />
			<Mem id="I_2" types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT DT_BOOL" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="30"  units="U_ALU"/>
		<Asm>xor $O_1, $TYPE_I_2 $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_XOR__14">
		<In>
			<Mem id="I_1"  types="DTG_INT DT_BOOL" />
			<Reg id="I_2" registers="RG_GP"  types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT DT_BOOL" destroys="I_2"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="30"  units="U_ALU"/>
		<Asm>xor $O_1, $TYPE_I_1 $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_XOR__2">
		<In>
			<Reg id="I_1" registers="RG_GP"  types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT DT_BOOL" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10"  units="U_ALU"/>
		<Asm>xor $O_1, 0x1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_NOT__1">
		<In>
			<Reg id="I_1" registers="RG_GP"  types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT DT_BOOL" destroys="I_1"/>
		</Out>
		<Exec etime="10"  units="U_ALU"/>
		<Asm>not $O_1</Asm>	
	</Instruction>
	
	
	<Instruction id="INSTRUCTION_CONVERT__1">
		<In>
			<Reg id="I_1" registers="RG_GP" types="DTG_INT" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_8" types="DT_INT_8U"  destroys="I_1"/>
		</Out>
		<Exec etime="0" units=""/>	
		<Asm></Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_CONVERT__5">
		<In>
			<Reg id="I_1" registers="RG_GP" types="DTG_INT" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_8" types="DTG_INT_8"  destroys="I_1"/>
		</Out>
		<Exec etime="0" units=""/>	
		<Asm></Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_CONVERT__2">
		<In>
			<Reg id="I_1" registers="RG_GP_32 RG_GP_16" types="DTG_INT_32 RG_GP_16" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_16" types="DTG_INT_16"  destroys="I_1"/>
		</Out>
		<Exec etime="0" units=""/>	
		<Asm></Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_CONVERT__3">
		<In>
			<Reg id="I_1" registers="RG_GP_32" types="DTG_INT_32" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_32" types="DTG_INT_32"  destroys="I_1"/>
		</Out>
		<Exec etime="0" units=""/>	
		<Asm></Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MOVZX__1">
		<In>
			<Reg id="I_1" registers="RG_GP_8" types="DTG_INT_8" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_32 RG_GP_16" types="DT_INT_32 DT_INT_16"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>movzx $O_1, $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MOVZX__2">
		<In>
			<Reg id="I_1" registers="RG_GP_16" types="DT_INT_16" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_32" types="DTG_INT_32"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>movzx $O_1, $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MOVZX__3">
		<In>
			<Reg id="I_1" registers="RG_GP_8" types="DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP" types="DTG_INT"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>movzx $O_1, $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MOVZX__4">
		<In>
			<Reg id="I_1" registers="RG_GP_8 RG_GP_16" types="DT_BOOL DTG_INT_8 DTG_INT_16" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_32" types="DTG_INT_32"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>movzx $O_1, $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MOVZX__5">
		<In>
			<Mem id="I_1" types="DT_BOOL DTG_INT_8 DTG_INT_16" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_32" types="DTG_INT_32"/>
		</Out>
		<Exec etime="30" units="U_ALU"/>	
		<Asm>movzx $O_1, $TYPE_I_1 $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MOVZX__6">
		<In>
			<Mem id="I_1" types="DT_BOOL DTG_INT_8" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_16" types="DTG_INT_16"/>
		</Out>
		<Exec etime="30" units="U_ALU"/>	
		<Asm>movzx $O_1, $TYPE_I_1 $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MOVSX__1">
		<In>
			<Reg id="I_1" registers="RG_GP_8" types="DTG_INT_8" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_32 RG_GP_16" types="DT_INT_32S DT_INT_16S"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>movsx $O_1, $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MOVSX__11">
		<In>
			<Mem id="I_1" types="DTG_INT_8" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_32 RG_GP_16" types="DT_INT_32S DT_INT_16S"/>
		</Out>
		<Exec etime="30" units="U_ALU"/>	
		<Asm>movsx $O_1, $TYPE_I_1 $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MOVSX__2">
		<In>
			<Reg id="I_1" registers="RG_GP_16" types="DT_INT_16" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_32" types="DTG_INT_32S"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>movsx $O_1, $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MOVSX__21">
		<In>
			<Mem id="I_1" types="DT_INT_16" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_32" types="DTG_INT_32S"/>
		</Out>
		<Exec etime="30" units="U_ALU"/>	
		<Asm>movsx $O_1, $TYPE_I_1 $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_LEA__1">
		<In>
			<Mem id="I_1" types="DTG_INT DT_COND DT_BOOL"/>
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_32"  types="DT_INT_32U" />
		</Out>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>lea $O_1, $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_CMP__1">
		<In>
			<Reg id="I_1" registers="RG_GP"  types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>cmp $I_1, 0</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_CMP__2">
		<In>
			<Reg id="I_1" registers="RG_GP"  types="DTG_INT DT_BOOL" />
			<Reg id="I_2" registers="RG_GP"  types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>cmp $I_1, $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_CMP__21">
		<In>
			<Reg id="I_1" registers="RG_GP"  types="DTG_INT DT_BOOL" />
			<Imm id="I_2" types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>cmp $I_1, $TYPE_I_2 $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_CMP__22">
		<In>
			<Reg id="I_1" registers="RG_GP"  types="DTG_INT DT_BOOL" />
			<Mem id="I_2" types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="30" units="U_ALU"/>	
		<Asm>cmp $I_1, $TYPE_I_2 $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_CMP__23">
		<In>
			<Mem id="I_1" types="DTG_INT DT_BOOL" />
			<Reg id="I_2" registers="RG_GP"  types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="30" units="U_ALU"/>	
		<Asm>cmp $I_1, $TYPE_I_2 $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_CMP__24">
		<In>
			<Mem id="I_1" types="DTG_INT DT_BOOL" />
			<Imm id="I_2" types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="30" units="U_ALU"/>	
		<Asm>cmp $TYPE_I_1 $I_1, $TYPE_I_2 $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_JMP__1" flags="IF_JUMP">
		<Exec etime="10" units="U_ALU"/>	
		<Asm>jmp NEAR $label_1</Asm>	
	</Instruction>
	
	
	<Instruction id="INSTRUCTION_JA__1" flags="IF_JUMP">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>ja NEAR $label_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_JZ__1" flags="IF_JUMP">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>jz NEAR $label_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_JG__1" flags="IF_JUMP">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Asm>jg NEAR $label_1</Asm>	
		<Exec etime="10" units="U_ALU"/>	
		<Props>
			<P key="PROPERTY_TEST_SIGNED" value="true" />
		</Props>
	</Instruction>
	
	<Instruction id="INSTRUCTION_JL__1" flags="IF_JUMP">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Asm>jl NEAR $label_1</Asm>	
		<Exec etime="10" units="U_ALU"/>	
		<Props>
			<P key="PROPERTY_TEST_SIGNED" value="true" />
		</Props>
	</Instruction>
	
	<Instruction id="INSTRUCTION_JB__1" flags="IF_JUMP">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>jb NEAR $label_1</Asm>	
		<Props>
			<P key="PROPERTY_TEST_SIGNED" value="false" />
		</Props>
	</Instruction>
	
	<Instruction id="INSTRUCTION_JE__1" flags="IF_JUMP">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>je NEAR $label_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_JNG__1" flags="IF_JUMP">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>jng NEAR $label_1</Asm>	
		<Props>
			<P key="PROPERTY_TEST_SIGNED" value="true" />
		</Props>
	</Instruction>
	
	<Instruction id="INSTRUCTION_JNA__1" flags="IF_JUMP">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>jna NEAR $label_1</Asm>	
		<Props>
			<P key="PROPERTY_TEST_SIGNED" value="false" />
		</Props>
	</Instruction>
	
	<Instruction id="INSTRUCTION_JNL__1" flags="IF_JUMP">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Asm>jnl NEAR $label_1</Asm>	
		<Exec etime="10" units="U_ALU"/>	
		<Props>
			<P key="PROPERTY_TEST_SIGNED" value="true" />
		</Props>
	</Instruction>
	
	<Instruction id="INSTRUCTION_JNB__1" flags="IF_JUMP">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Asm>jnb NEAR $label_1</Asm>	
		<Exec etime="10" units="U_ALU"/>	
		<Props>
			<P key="PROPERTY_TEST_SIGNED" value="false" />
		</Props>
	</Instruction>
	
	<Instruction id="INSTRUCTION_JNE__1" flags="IF_JUMP">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>jne NEAR $label_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_SETG__1">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_8" types="DT_BOOL"/>
		</Out>
		<Asm>setg $O_1</Asm>	
		<Exec etime="50" units="U_ALU"/>	
		<Props>
			<P key="PROPERTY_TEST_SIGNED" value="true" />
		</Props>
	</Instruction>
	
	<Instruction id="INSTRUCTION_SETA__1">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_8" types="DT_BOOL"/>
		</Out>
		<Exec etime="50" units="U_ALU"/>	
		<Asm>seta $O_1</Asm>	
		<Props>
			<P key="PROPERTY_TEST_SIGNED" value="false" />
		</Props>
	</Instruction>
	
	<Instruction id="INSTRUCTION_SETL__1">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_8" types="DT_BOOL"/>
		</Out>
		<Exec etime="50" units="U_ALU"/>	
		<Asm>setl $O_1</Asm>	
		<Props>
			<P key="PROPERTY_TEST_SIGNED" value="true" />
		</Props>
	</Instruction>
	
	<Instruction id="INSTRUCTION_SETB__1">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_8" types="DT_BOOL"/>
		</Out>
		<Exec etime="50" units="U_ALU"/>	
		<Asm>setb $O_1</Asm>	
		<Props>
			<P key="PROPERTY_TEST_SIGNED" value="false" />
		</Props>
	</Instruction>
	
	<Instruction id="INSTRUCTION_SETE__1">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_8" types="DT_BOOL"/>
		</Out>
		<Exec etime="50" units="U_ALU"/>	
		<Asm>sete $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_SETNG__1">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_8" types="DT_BOOL"/>
		</Out>
		<Asm>setng $O_1</Asm>	
		<Exec etime="50" units="U_ALU"/>	
		<Props>
			<P key="PROPERTY_TEST_SIGNED" value="true" />
		</Props>
	</Instruction>
	
	<Instruction id="INSTRUCTION_SETNA__1">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_8" types="DT_BOOL"/>
		</Out>
		<Asm>setna $O_1</Asm>	
		<Exec etime="50" units="U_ALU"/>	
		<Props>
			<P key="PROPERTY_TEST_SIGNED" value="false" />
		</Props>
	</Instruction>
	
	<Instruction id="INSTRUCTION_SETNL__1">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_8" types="DT_BOOL"/>
		</Out>
		<Asm>setnl $O_1</Asm>	
		<Exec etime="50" units="U_ALU"/>	
		<Props>
			<P key="PROPERTY_TEST_SIGNED" value="true" />
		</Props>
	</Instruction>
	
	<Instruction id="INSTRUCTION_SETNB__1">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_8" types="DT_BOOL"/>
		</Out>
		<Exec etime="50" units="U_ALU"/>	
		<Asm>setnb $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_SETNE__1">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_8" types="DT_BOOL"/>
		</Out>
		<Exec etime="50" units="U_ALU"/>	
		<Asm>setne $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_SETNZ__1">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_8" types="DT_BOOL"/>
		</Out>
		<Exec etime="50" units="U_ALU"/>	
		<Asm>setnz $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_PUSH__1" flags="IF_STORE | IF_SYS">
		<In>
			<Reg id="I_1" registers="R_ESP"  types="DT_INT_32U" />
			<Mem id="I_2" types="DTG_INT_32"/>
		</In>
		<Out>
			<Reg id="O_1" registers="R_ESP"  types="DT_INT_32U" destroys="I_1" />	
		</Out>
		<Exec etime="15" units="U_MEM_STORE U_ALU"/>		
		<Asm>push $TYPE_I_2 $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_PUSH__2" flags="IF_STORE | IF_SYS">
		<In>
			<Reg id="I_1" registers="R_ESP"  types="DT_INT_32U" />
			<Reg id="I_2" registers="RG_GP"  types="DTG_INT_32" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_ESP"  types="DT_INT_32U" destroys="I_1" />	
		</Out>
		<Exec etime="15" units="U_MEM_STORE U_ALU"/>	
		<Asm>push $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_PUSH__3" flags="IF_STORE | IF_SYS">
		<In>
			<Reg id="I_1" registers="R_ESP"  types="DT_INT_32U" />
			<Imm id="I_2" types="DTG_INT DT_BOOL"/>
		</In>
		<Out>
			<Reg id="O_1" registers="R_ESP"  types="DT_INT_32U" destroys="I_1" />	
		</Out>
		<Exec etime="15" units="U_MEM_STORE U_ALU"/>	
		<Asm>push DWORD $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_POP__1" flags="IF_SYS">
		<In>
			<Reg id="I_1" registers="R_ESP"  types="DT_INT_32U" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_ESP"  types="DT_INT_32U" destroys="I_1" />
			<Reg id="O_2" registers="RG_GP" types="DTG_INT DT_COND DT_BOOL"/>
		</Out>
		<Exec etime="15" units="U_MEM_LOAD U_ALU"/>	
		<Asm>pop $O_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_NOP__1" flags="IF_SYS">
		<Asm>nop</Asm>	
		<Exec etime="10" units="U_ALU"/>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_ESPADD__1" flags="IF_SYS">
		<In>
			<Reg id="I_1" registers="R_ESP"  types="DT_INT_32" />
			<Imm id="I_2" types="DTG_INT" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_ESP"  types="DT_INT_32" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>
		<Asm>add $O_1, $TYPE_I_2 $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_BACKUPESP__1" flags="IF_SYS">
		<In>
			<Reg id="I_1" registers="R_ESP R_EBP" types="DT_INT_32U" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_ESP R_EBP" types="DT_INT_32U" />
		</Out>
		<Exec etime="10" units="U_ALU"/>
		<Asm>mov $O_1, $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_CALL__1" flags="IF_SYS">
		<Out>
			<Reg id="O_1" registers="R_EAX" types="DTG_INT"/>
			<Reg id="O_2" registers="R_ECX" types="DTG_INT"/>
			<Reg id="O_3" registers="R_EDX" types="DTG_INT"/>
		</Out>
		<Exec etime="50" units="U_ALU MEM_STORE"/>	
		<Asm>call $name</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_PROLOGUE__1" flags="IF_SYS">
		<Out>
			<Reg id="O_1" registers="R_EDI" types="DTG_INT"/>
			<Reg id="O_2" registers="R_ESI" types="DTG_INT"/>
			<Reg id="O_3" registers="R_EBX" types="DTG_INT"/>
		</Out>
		<Exec etime="0" units=""/>	
		<Asm></Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_EPILOGUE__1" flags="IF_SYS">
		<In>
			<Reg id="I_1" registers="R_EDI" types="DTG_INT"/>
			<Reg id="I_2" registers="R_ESI" types="DTG_INT"/>
			<Reg id="I_3" registers="R_EBX" types="DTG_INT"/>
		</In>
		<Exec etime="0" units=""/>	
		<Asm></Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_LEAVE__1" flags="IF_SYS">
		<Asm>leave</Asm>	
		<Exec etime="10" units="U_ALU"/>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_RET__1" flags="IF_SYS">
		<Asm>ret</Asm>
		<Exec etime="80" units="U_MEM_LOAD U_ALU"/>		
	</Instruction>
	
</Instructions>


<Assembler>
		<Entities>
			<!-- common -->
			<Entity id="ent_instruction_delimiter">\n\t</Entity>
			<Entity id="ent_one_line_comment">\n\t;$text</Entity>
			<Entity id="ent_inline_comment">\t;$text</Entity>
			
			<!--declarations -->
			<Entity id="ent_global_simple_decl">\n$id: $type $value</Entity>
			<Entity id="ent_global_symbol_decl">\nglobal $name</Entity>
			<Entity id="ent_extern_symbol_decl">\nextern $name</Entity>
			<Entity id="ent_internal_symbol_decl"></Entity>
			
			<!-- mem access -->
			<Entity id="ent_address_disp">[$disp]</Entity>
			<Entity id="ent_address_disp_base">[$base$psign$disp]</Entity>
			<Entity id="ent_address_base">[$base]</Entity>
			
			<!-- function -->
			<Entity id="ent_function_label">\n$name:</Entity>
			<Entity id="ent_main_name">main</Entity>
			
			<!-- label -->
			<Entity id="ent_label">@Lbl_$id</Entity>
			<Entity id="ent_label_def">$label:</Entity>
			
			<!-- literal -->
			<Entity id="ent_literal">Lit_$id</Entity>
			
			<!-- section names -->
			<Entity id="ent_section_data">\nsection .data\n</Entity>
			<Entity id="ent_section_text">\nsection .text\n</Entity>
			<Entity id="ent_section_uninitialized_data">\nsection .bss\n</Entity>

			<!-- space reservation -->
			<Entity id="ent_space_reserve">resb $size</Entity>
		</Entities>
</Assembler>
	


<Pi2Ge>
	
	<Template id="TEM_ADD_1" ref_pi="pi_add">
		<Instr ref_instr="INSTRUCTION_ADD__*">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />	
				<Op ref_op="O_2" pid="ADD_O_2" />	
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_SUB_1" ref_pi="pi_sub">
		<Instr ref_instr="INSTRUCTION_SUB__*">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />	
				<Op ref_op="O_2" pid="SUB_O_2" />	
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_MUL_1" ref_pi="pi_mul">
		<Instr ref_instr="INSTRUCTION_MUL__1 INSTRUCTION_IMUL__*">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />	
				<Op ref_op="O_2" pid="MUL_O_2" />	
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_MUL_2" ref_pi="pi_mul">
		<Instr ref_instr="INSTRUCTION_MUL__2 INSTRUCTION_MUL__3">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />	
				<Op ref_op="O_2" pid="MUL_O_2" />	
				<Op ref_op="O_3" pid="MUL_O_3" />	
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_DIV_1" ref_pi="pi_div">
		<Instr ref_instr="INSTRUCTION_CBW__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="CBW_O_1" />
			</Out>
		</Instr>
		
		<Instr ref_instr="INSTRUCTION_IDIV__1">
			<In>
				<Op ref_op="I_1" pid="CBW_O_1" />
				<Op ref_op="I_2" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />	
				<Op ref_op="O_2" pid="DIV_O_2" />
				<Op ref_op="O_3" pid="DIV_O_3" />		
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_DIV_2" ref_pi="pi_div">
		<Instr ref_instr="INSTRUCTION_CWD__1 INSTRUCTION_CDQ__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="CWD_O_1" />
				<Op ref_op="O_2" pid="CWD_O_2" />
			</Out>
		</Instr>
		
		<Instr ref_instr="INSTRUCTION_IDIV__2 INSTRUCTION_IDIV__3">
			<In>
				<Op ref_op="I_1" pid="CWD_O_1" />
				<Op ref_op="I_2" pid="CWD_O_2" />
				<Op ref_op="I_3" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />	
				<Op ref_op="O_2" pid="DIV_O_2" />
				<Op ref_op="O_3" pid="DIV_O_3" />		
			</Out>
		</Instr>
	</Template>
	
	
	<Template id="TEM_DIV_3" ref_pi="pi_div">
		<Instr ref_instr="INSTRUCTION_NULLAH__1 INSTRUCTION_NULLDX__1 INSTRUCTION_NULLEDX__1">
			<Out>
				<Op ref_op="O_1" pid="NULL_O_1" />
			</Out>
		</Instr>
		
		<Instr ref_instr="INSTRUCTION_DIV__1 INSTRUCTION_DIV__2 INSTRUCTION_DIV__3">
			<In>
				<Op ref_op="I_1" pid="NULL_O_1" />
				<Op ref_op="I_2" pid="PIN_1" />
				<Op ref_op="I_3" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />	
				<Op ref_op="O_2" pid="DIV_O_2" />
				<Op ref_op="O_3" pid="DIV_O_3" />		
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_MOD_1" ref_pi="pi_mod">
		<Instr ref_instr="INSTRUCTION_CBW__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="CBW_O_1" />
			</Out>
		</Instr>
		
		<Instr ref_instr="INSTRUCTION_IDIV__1">
			<In>
				<Op ref_op="I_1" pid="CBW_O_1" />
				<Op ref_op="I_2" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="DIV_O_1" />	
				<Op ref_op="O_2" pid="POUT_1" />
				<Op ref_op="O_3" pid="DIV_O_3" />		
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_MOD_2" ref_pi="pi_mod">
		<Instr ref_instr="INSTRUCTION_CWD__1 INSTRUCTION_CDQ__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="CWD_O_1" />
				<Op ref_op="O_2" pid="CWD_O_2" />
			</Out>
		</Instr>
		
		<Instr ref_instr="INSTRUCTION_IDIV__2 INSTRUCTION_IDIV__3">
			<In>
				<Op ref_op="I_1" pid="CWD_O_1" />
				<Op ref_op="I_2" pid="CWD_O_2" />
				<Op ref_op="I_3" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="DIV_O_1" />	
				<Op ref_op="O_2" pid="POUT_1" />	
				<Op ref_op="O_3" pid="DIV_O_3" />	
			</Out>
		</Instr>
	</Template>
	
	
	<Template id="TEM_MOD_3" ref_pi="pi_mod">
		<Instr ref_instr="INSTRUCTION_NULLAH__1 INSTRUCTION_NULLDX__1 INSTRUCTION_NULLEDX__1">
			<Out>
				<Op ref_op="O_1" pid="NULL_O_1" />
			</Out>
		</Instr>
		
		<Instr ref_instr="INSTRUCTION_DIV__1 INSTRUCTION_DIV__2 INSTRUCTION_DIV__3">
			<In>
				<Op ref_op="I_1" pid="NULL_O_1" />
				<Op ref_op="I_2" pid="PIN_1" />
				<Op ref_op="I_3" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="DIV_O_1" />	
				<Op ref_op="O_2" pid="POUT_1" />
				<Op ref_op="O_3" pid="DIV_O_3" />		
			</Out>
		</Instr>
	</Template>
	
	
	<Template id="TEM_REM_1" ref_pi="pi_rem">
		<Instr ref_instr="INSTRUCTION_CBW__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="CBW_O_1" />
			</Out>
		</Instr>
		
		<Instr ref_instr="INSTRUCTION_IDIV__1">
			<In>
				<Op ref_op="I_1" pid="CBW_O_1" />
				<Op ref_op="I_2" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="DIV_O_1" />	
				<Op ref_op="O_2" pid="POUT_1" />
				<Op ref_op="O_3" pid="DIV_O_3" />		
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_REM_2" ref_pi="pi_rem">
		<Instr ref_instr="INSTRUCTION_CWD__1 INSTRUCTION_CDQ__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="CWD_O_1" />
				<Op ref_op="O_2" pid="CWD_O_2" />
			</Out>
		</Instr>
		
		<Instr ref_instr="INSTRUCTION_IDIV__2 INSTRUCTION_IDIV__3">
			<In>
				<Op ref_op="I_1" pid="CWD_O_1" />
				<Op ref_op="I_2" pid="CWD_O_2" />
				<Op ref_op="I_3" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="DIV_O_1" />	
				<Op ref_op="O_2" pid="POUT_1" />
				<Op ref_op="O_3" pid="DIV_O_3" />		
			</Out>
		</Instr>
	</Template>
	
	
	<Template id="TEM_REM_3" ref_pi="pi_rem">
		<Instr ref_instr="INSTRUCTION_NULLAH__1 INSTRUCTION_NULLDX__1 INSTRUCTION_NULLEDX__1">
			<Out>
				<Op ref_op="O_1" pid="NULL_O_1" />
			</Out>
		</Instr>
		
		<Instr ref_instr="INSTRUCTION_DIV__1 INSTRUCTION_DIV__2 INSTRUCTION_DIV__3">
			<In>
				<Op ref_op="I_1" pid="NULL_O_1" />
				<Op ref_op="I_2" pid="PIN_1" />
				<Op ref_op="I_3" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="DIV_O_1" />	
				<Op ref_op="O_2" pid="POUT_1" />	
				<Op ref_op="O_3" pid="DIV_O_3" />		
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_NEG_1" ref_pi="pi_neg">
		<Instr ref_instr="INSTRUCTION_NEG__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
				<Op ref_op="O_2" pid="NEG_O_2" />		
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_GAT_1" ref_pi="pi_gat">
		<Instr ref_instr="INSTRUCTION_MOV__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	
	<Template id="TEM_SHL_1" ref_pi="pi_shl">
		<Instr ref_instr="INSTRUCTION_CONVERT__1">
			<In>
				<Op ref_op="I_1" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="CVT_O_1" />
			</Out>
		</Instr>
		<Instr ref_instr="INSTRUCTION_SHL__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="CVT_O_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
				<Op ref_op="O_2" pid="SHL_O_2" />		
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_SHR_1" ref_pi="pi_shr">
		<Instr ref_instr="INSTRUCTION_CONVERT__1">
			<In>
				<Op ref_op="I_1" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="CVT_O_1" />
			</Out>
		</Instr>
		<Instr ref_instr="INSTRUCTION_SHR__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="CVT_O_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
				<Op ref_op="O_2" pid="SHR_O_2" />		
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_SAL_1" ref_pi="pi_sal">
		<Instr ref_instr="INSTRUCTION_CONVERT__1">
			<In>
				<Op ref_op="I_1" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="CVT_O_1" />
			</Out>
		</Instr>
		<Instr ref_instr="INSTRUCTION_SAL__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="CVT_O_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
				<Op ref_op="O_2" pid="SAL_O_2" />		
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_SAR_1" ref_pi="pi_sar">
		<Instr ref_instr="INSTRUCTION_CONVERT__1">
			<In>
				<Op ref_op="I_1" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="CVT_O_1" />
			</Out>
		</Instr>
		<Instr ref_instr="INSTRUCTION_SAR__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="CVT_O_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
				<Op ref_op="O_2" pid="SAR_O_2" />		
			</Out>
		</Instr>
	</Template>
	
	
	<Template id="TEM_BAND_1" ref_pi="pi_band">
		<Instr ref_instr="INSTRUCTION_AND__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />	
				<Op ref_op="O_2" pid="AND_O_2" />	
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_BOR_1" ref_pi="pi_bor">
		<Instr ref_instr="INSTRUCTION_OR__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />	
				<Op ref_op="O_2" pid="OR_O_2" />	
			</Out>
		</Instr>
	</Template>
	
	
	<Template id="TEM_BXOR_1" ref_pi="pi_bxor">
		<Instr ref_instr="INSTRUCTION_XOR__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />	
				<Op ref_op="O_2" pid="XOR_O_2" />	
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_BNOT_1" ref_pi="pi_bnot">
		<Instr ref_instr="INSTRUCTION_NOT__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />	
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_LAND_1" ref_pi="pi_land">
		<Instr ref_instr="INSTRUCTION_AND__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />	
				<Op ref_op="O_2" pid="AND_O_2" />	
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_LOR_1" ref_pi="pi_lor">
		<Instr ref_instr="INSTRUCTION_OR__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />	
				<Op ref_op="O_2" pid="OR_O_2" />	
			</Out>
		</Instr>
	</Template>
	
	
	<Template id="TEM_LXOR_1" ref_pi="pi_lxor">
		<Instr ref_instr="INSTRUCTION_XOR__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />	
				<Op ref_op="O_2" pid="XOR_O_2" />	
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_LNOT_1" ref_pi="pi_lnot">
		<Instr ref_instr="INSTRUCTION_XOR__2">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
				<Op ref_op="O_2" pid="XOR_O_2" />		
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_CVT_1" ref_pi="pi_cvt">
		<Instr ref_instr="INSTRUCTION_CONVERT__* INSTRUCTION_MOVZX__* INSTRUCTION_MOVSX__*">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_CVT_2" ref_pi="pi_cvt">
		<Instr ref_instr="INSTRUCTION_CMP__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="CMP_O_1" />
			</Out>
		</Instr>
		<Instr ref_instr="INSTRUCTION_SETNZ__1">
			<In>
				<Op ref_op="I_1" pid="CMP_O_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_CVT_3" ref_pi="pi_cvt">
		<Instr ref_instr="INSTRUCTION_AND__2">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
				<Op ref_op="O_2" pid="AND_O_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_MOV_1" ref_pi="pi_mov">
		<Instr ref_instr="INSTRUCTION_MOV__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_ST_1" ref_pi="pi_st">
		<Instr ref_instr="INSTRUCTION_MOV__2 INSTRUCTION_MOV__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_STV_1" ref_pi="pi_stv">
		<Instr ref_instr="INSTRUCTION_MOV__2 INSTRUCTION_MOV__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_STI_1" ref_pi="pi_sti">
		<Instr ref_instr="INSTRUCTION_MOV__4">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_STIV_1" ref_pi="pi_stiv">
		<Instr ref_instr="INSTRUCTION_MOV__4">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
		
	<Template id="TEM_STP_1" ref_pi="pi_stp">
		<Instr ref_instr="INSTRUCTION_MOV__7">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_STPV_1" ref_pi="pi_stpv">
		<Instr ref_instr="INSTRUCTION_MOV__7">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_LD_1" ref_pi="pi_ld">
		<Instr ref_instr="INSTRUCTION_MOV__3 INSTRUCTION_MOV__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_LDV_1" ref_pi="pi_ldv">
		<Instr ref_instr="INSTRUCTION_MOV__3 INSTRUCTION_MOV__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_LDI_1" ref_pi="pi_ldi">
		<Instr ref_instr="INSTRUCTION_MOV__5">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_LDP_1" ref_pi="pi_ldp">
		<Instr ref_instr="INSTRUCTION_MOV__8">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="PIN_2" />	
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_LDPV_1" ref_pi="pi_ldpv">
		<Instr ref_instr="INSTRUCTION_MOV__8">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="PIN_2" />	
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_LDA_1" ref_pi="pi_lda">
		<Instr ref_instr="INSTRUCTION_LEA__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_CMP_1" ref_pi="pi_cmp">
		<Instr ref_instr="INSTRUCTION_CMP__2">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_BA_1" ref_pi="pi_ba">
		<Instr ref_instr="INSTRUCTION_JMP__1">
		</Instr>
	</Template>
	
	<Template id="TEM_BN_1" ref_pi="pi_bn">
		<Instr ref_instr="INSTRUCTION_NOP__1">
		</Instr>
	</Template>
	
	<Template id="TEM_BT_1" ref_pi="pi_bt">
		<Instr ref_instr="INSTRUCTION_CMP__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="CMP_O_1" />
			</Out>
		</Instr>
		<Instr ref_instr="INSTRUCTION_JA__1">
			<In>
				<Op ref_op="I_1" pid="CMP_O_1" />
			</In>
		</Instr>
	</Template>
	
	<Template id="TEM_BF_1" ref_pi="pi_bf">
		<Instr ref_instr="INSTRUCTION_CMP__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="CMP_O_1" />
			</Out>
		</Instr>
		<Instr ref_instr="INSTRUCTION_JZ__1">
			<In>
				<Op ref_op="I_1" pid="CMP_O_1" />
			</In>
		</Instr>
	</Template>
	
	<Template id="TEM_BG_1" ref_pi="pi_bg">
		<Instr ref_instr="INSTRUCTION_JG__1 INSTRUCTION_JA__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
		</Instr>
	</Template>
	
	<Template id="TEM_BL_1" ref_pi="pi_bl">
		<Instr ref_instr="INSTRUCTION_JL__1 INSTRUCTION_JB__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
		</Instr>
	</Template>
	
	<Template id="TEM_BE_1" ref_pi="pi_be">
		<Instr ref_instr="INSTRUCTION_JE__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
		</Instr>
	</Template>
	
	<Template id="TEM_BNG_1" ref_pi="pi_bng">
		<Instr ref_instr="INSTRUCTION_JNG__1 INSTRUCTION_JNA__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
		</Instr>
	</Template>
	
	<Template id="TEM_BNL_1" ref_pi="pi_bnl">
		<Instr ref_instr="INSTRUCTION_JNL__1 INSTRUCTION_JNB__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
		</Instr>
	</Template>
	
	<Template id="TEM_BNE_1" ref_pi="pi_bne">
		<Instr ref_instr="INSTRUCTION_JNE__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
		</Instr>
	</Template>
	
	<Template id="TEM_SBG_1" ref_pi="pi_sbg">
		<Instr ref_instr="INSTRUCTION_CMP__2">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="CMP_O_1" />
			</Out>
		</Instr>
		<Instr ref_instr="INSTRUCTION_SETG__1 INSTRUCTION_SETA__1">
			<In>
				<Op ref_op="I_1" pid="CMP_O_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_SBL_1" ref_pi="pi_sbl">
		<Instr ref_instr="INSTRUCTION_CMP__2">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="CMP_O_1" />
			</Out>
		</Instr>
		<Instr ref_instr="INSTRUCTION_SETL__1 INSTRUCTION_SETB__1">
			<In>
				<Op ref_op="I_1" pid="CMP_O_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_SBE_1" ref_pi="pi_sbe">
		<Instr ref_instr="INSTRUCTION_CMP__2">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="CMP_O_1" />
			</Out>
		</Instr>
		<Instr ref_instr="INSTRUCTION_SETE__1">
			<In>
				<Op ref_op="I_1" pid="CMP_O_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_SBNG_1" ref_pi="pi_sbng">
		<Instr ref_instr="INSTRUCTION_CMP__2">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="CMP_O_1" />
			</Out>
		</Instr>
		<Instr ref_instr="INSTRUCTION_SETNG__1 INSTRUCTION_SETNA__1">
			<In>
				<Op ref_op="I_1" pid="CMP_O_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_SBNL_1" ref_pi="pi_sbnl">
		<Instr ref_instr="INSTRUCTION_CMP__2">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="CMP_O_1" />
			</Out>
		</Instr>
		<Instr ref_instr="INSTRUCTION_SETNL__1 INSTRUCTION_SETNB__1">
			<In>
				<Op ref_op="I_1" pid="CMP_O_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_SBNE_1" ref_pi="pi_sbne">
		<Instr ref_instr="INSTRUCTION_CMP__2">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="CMP_O_1" />
			</Out>
		</Instr>
		<Instr ref_instr="INSTRUCTION_SETNE__1">
			<In>
				<Op ref_op="I_1" pid="CMP_O_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_REGISTER_MOVE_1" >
		<Instr ref_instr="INSTRUCTION_MOV__1 INSTRUCTION_MOV__2 INSTRUCTION_MOV__3 ">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_REGISTER_MOVE_2" >
		<Instr ref_instr="INSTRUCTION_LAHF__1 INSTRUCTION_SAHF__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_REGISTER_MOVE_3" >
		<Instr ref_instr="INSTRUCTION_LAHF__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="LAHF_O_1" />
			</Out>
		</Instr>
		
		<Instr ref_instr="INSTRUCTION_MOV__1 INSTRUCTION_MOV__2">
			<In>
				<Op ref_op="I_1" pid="LAHF_O_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_REGISTER_MOVE_4" >
		<Instr ref_instr="INSTRUCTION_MOV__1 INSTRUCTION_MOV__3">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="MOV_O_1" />
			</Out>
		</Instr>
		
		<Instr ref_instr="INSTRUCTION_SAHF__1">
			<In>
				<Op ref_op="I_1" pid="MOV_O_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_REGISTER_MOVE_5" >
		<Instr ref_instr="INSTRUCTION_SYSMOV__9 INSTRUCTION_SYSMOV__10 INSTRUCTION_SYSMOV__11 ">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
</Pi2Ge>


</TargetArchitecture>
