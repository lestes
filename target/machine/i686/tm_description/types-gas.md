<?xml version="1.0"?>
<DataTypes>
	<DataType bitwidth="8" id="DT_COND"  format="FORMAT_C2" alignment="8" asm="b" asm_decl=".byte"/>
	<DataType bitwidth="8" id="DT_BOOL" format="FORMAT_C2" mapped_types="ss_bool" retreg="R_AL" alignment="8" asm="b" asm_decl=".byte"/>
	<DataType bitwidth="8" id="DT_INT_8P" format="FORMAT_C2" mapped_types="ss_type_pchar" retreg="R_AL" alignment="8" asm="b" asm_decl=".byte"/>
	<DataType bitwidth="8" id="DT_INT_8U" format="FORMAT_C2" mapped_types="ss_type_uchar" retreg="R_AL" alignment="8" asm="b" asm_decl=".byte"/>
	<DataType bitwidth="8" id="DT_INT_8S" format="FORMAT_C2" mapped_types="ss_type_schar" retreg="R_AL" alignment="8" asm="b" asm_decl=".byte"/>
	<DataType bitwidth="16" id="DT_INT_16S" format="FORMAT_C2" mapped_types="ss_type_sshort" retreg="R_AX" alignment="16" asm="w" asm_decl=".word"/>
	<DataType bitwidth="16" id="DT_INT_16U" format="FORMAT_C2" mapped_types="ss_type_ushort" retreg="R_AX" alignment="16" asm="w" asm_decl=".word"/>
	<DataType bitwidth="32" id="DT_INT_32S" format="FORMAT_C2" mapped_types="ss_type_sint ss_type_slong" retreg="R_EAX" alignment="32" asm="l" asm_decl=".long"/>
	<DataType bitwidth="32" id="DT_INT_32U" format="FORMAT_C2" mapped_types="ss_type_uint ss_type_ulong" retreg="R_EAX" alignment="32" asm="l" asm_decl=".long"/>

	<DataTypeGroup id="DTG_INT_8" types="DT_INT_8P DT_INT_8U DT_INT_8S" />
	<DataTypeGroup id="DTG_INT_8U" types="DT_INT_8P DT_INT_8U" />
	<DataTypeGroup id="DTG_INT_16" types="DT_INT_16U DT_INT_16S" />
	<DataTypeGroup id="DTG_INT_32" types="DT_INT_32U DT_INT_32S" />
	<DataTypeGroup id="DTG_INT" types="DTG_INT_8 DTG_INT_16 DTG_INT_32" />
	<DataTypeGroup id="DTG_INT_S" types="DT_INT_8S DT_INT_16S DT_INT_32S" />
	<DataTypeGroup id="DTG_INT_U" types="DT_INT_8U DT_INT_8P DT_INT_16U DT_INT_32U" />
</DataTypes>
