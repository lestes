<?xml version="1.0"?>
<Instructions>

	<Instruction id="INSTRUCTION_MOV__1" flags="IF_COPY">
		<In>
			<Reg id="I_1" registers="RG_GP" types="DTG_INT DT_COND DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP" types="DTG_INT DT_COND DT_BOOL" />
		</Out>
		<Exec etime="10" units="U_ALU"/>
		<Asm>mov $I_1, $O_1</Asm>
	</Instruction>
	
	<Instruction id="INSTRUCTION_MOV__2" flags="IF_STORE">
		<In>
			<Reg id="I_1" registers="RG_GP" types="DTG_INT DT_COND DT_BOOL" />
		</In>
		<Out>
			<Mem id="O_1" types="DTG_INT DT_COND DT_BOOL" />
		</Out>	
		<Exec etime="30" units="U_ALU U_MEM_STORE"/>
		<Asm>mov$TYPE_O_1 $I_1, $O_1</Asm>
	</Instruction>
	
	<Instruction id="INSTRUCTION_MOV__3" flags="IF_LOAD">
		<In>
			<Mem id="I_1" types="DTG_INT DT_COND DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP" types="DTG_INT DT_COND DT_BOOL" />
		</Out>	
		<Exec etime="30" units="U_ALU U_MEM_LOAD"/>
		<Asm>mov$TYPE_I_1 $I_1, $O_1</Asm>
	</Instruction>
	
	<Instruction id="INSTRUCTION_MOV__4" flags="IF_LOAD | IF_STORE">
		<In>
			<Imm id="I_1" types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Mem id="O_1" types="DTG_INT DT_BOOL" />
		</Out>	
		<Exec etime="30" units="U_ALU U_MEM_STORE"/>
		<Asm>mov$TYPE_I_1 $$I_1, $O_1</Asm>
	</Instruction>
	
	<Instruction id="INSTRUCTION_MOV__5" flags="IF_LOAD">
		<In>
			<Imm id="I_1" types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP" types="DTG_INT DT_BOOL" />
		</Out>	
		<Exec etime="10" units="U_ALU"/>
		<Asm>mov$TYPE_O_1 $$I_1, $O_1</Asm>
	</Instruction>
	
	<Instruction id="INSTRUCTION_MOV__7" flags="IF_STORE">
		<In>
			<Reg id="I_1" registers="RG_GP" types="DTG_INT DT_COND DT_BOOL" />
			<Reg id="I_2" registers="RG_GP_32" types="DT_INT_32U" />
		</In>
		<Out>
			<Mem id="O_1" types="DTG_INT DT_COND DT_BOOL" />
		</Out>	
		<Exec etime="30" units="U_ALU U_MEM_STORE"/>
		<Asm>mov$TYPE_O_1 $I_1, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MOV__71" flags="IF_STORE">
		<In>
			<Imm id="I_1" types="DTG_INT DT_COND DT_BOOL" />
			<Reg id="I_2" registers="RG_GP_32" types="DT_INT_32U" />
		</In>
		<Out>
			<Mem id="O_1" types="DTG_INT DT_COND DT_BOOL" />
		</Out>	
		<Exec etime="30" units="U_ALU U_MEM_STORE"/>
		<Asm>mov$TYPE_O_1 $$I_1, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MOV__8"  flags="IF_LOAD">
		<In>
			<Mem id="I_1" types="DTG_INT DT_COND DT_BOOL" />
			<Reg id="I_2" registers="RG_GP_32" types="DT_INT_32U" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP" types="DTG_INT DT_COND DT_BOOL" />
		</Out>	
		<Exec etime="30" units="U_ALU U_MEM_LOAD"/>
		<Asm>mov$TYPE_I_1 $I_1, $O_1</Asm>
	</Instruction>
	
	<Instruction id="INSTRUCTION_SYSMOV__9" flags="IF_COPY">
		<In>
			<Reg id="I_1" registers="R_ESP R_EBP" types="DTG_INT" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_ESP R_EBP" types="DTG_INT" />
		</Out>
		<Exec etime="10" units="U_ALU"/>
		<Asm>mov $I_1, $O_1</Asm>
	</Instruction>
	
	<Instruction id="INSTRUCTION_SYSMOV__10" flags="IF_STORE">
		<In>
			<Reg id="I_1" registers="R_ESP R_EBP" types="DTG_INT" />
		</In>
		<Out>
			<Mem id="O_1" types="DTG_INT" />
		</Out>	
		<Exec etime="30" units="U_ALU U_MEM_STORE"/>
		<Asm>mov$TYPE_O_1 $I_1, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_SYSMOV__11" flags="IF_LOAD">
		<In>
			<Mem id="I_1" types="DTG_INT" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_ESP R_EBP" types="DTG_INT" />
		</Out>	
		<Exec etime="30" units="U_ALU U_MEM_LOAD"/>
		<Asm>mov$TYPE_I_1 $I_1, $O_1</Asm>	
	</Instruction>
	<Instruction id="INSTRUCTION_SAHF__1">
		<In>
			<Reg id="I_1" registers="R_AH" types="DT_COND" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_COND" types="DT_COND" />
		</Out>	
		<Exec etime="10" units="U_ALU"/>
		<Asm>sahf</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_LAHF__1">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_AH" types="DT_COND" />
		</Out>	
		<Exec etime="10" units="U_ALU"/>
		<Asm>lahf</Asm>	
	</Instruction>
	
	
	<Instruction id="INSTRUCTION_ADD__1">
		<In>
			<Reg id="I_1" registers="RG_GP"  types="DTG_INT" />
			<Reg id="I_2" registers="RG_GP"  types="DTG_INT" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>
		<Asm>add $I_2, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_ADD__11">
		<In>
			<Reg id="I_1" registers="RG_GP"  types="DTG_INT" />
			<Mem id="I_2" types="DTG_INT" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="30" units="U_ALU"/>
		<Asm>add$TYPE_I_2 $I_2, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_ADD__12">
		<In>
			<Mem id="I_1" types="DTG_INT" />
			<Reg id="I_2" registers="RG_GP"  types="DTG_INT" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT" destroys="I_2"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="30" units="U_ALU"/>
		<Asm>add$TYPE_I_1 $I_1, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_ADD__13">
		<In>
			<Reg id="I_1" registers="RG_GP"  types="DTG_INT" />
			<Imm id="I_2" types="DTG_INT" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>
		<Asm>add$TYPE_I_2 $$I_2, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_ADD__14">
		<In>
			<Imm id="I_1" types="DTG_INT" />
			<Reg id="I_2" registers="RG_GP"  types="DTG_INT" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT" destroys="I_2"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>
		<Asm>add$TYPE_I_1 $$I_1, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_SUB__1">
		<In>
			<Reg id="I_1" registers="RG_GP"  types="DTG_INT" />
			<Reg id="I_2" registers="RG_GP"  types="DTG_INT" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>
		<Asm>sub $I_2, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_SUB__11">
		<In>
			<Reg id="I_1" registers="RG_GP"  types="DTG_INT" />
			<Mem id="I_2" types="DTG_INT" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="30" units="U_ALU"/>
		<Asm>sub$TYPE_I_2 $I_2, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_SUB__12">
		<In>
			<Reg id="I_1" registers="RG_GP"  types="DTG_INT" />
			<Imm id="I_2" types="DTG_INT" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>
		<Asm>sub$TYPE_I_2 $$I_2, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_IMUL__1">
		<In>
			<Reg id="I_1" registers="R_AL"  types="DT_INT_8S" />
			<Reg id="I_2" registers="RG_GP_8 -R_AL"  types="DT_INT_8S" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_AL"  types="DT_INT_8S" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="100" units="U_FP_MUL"/>
		<Asm>imul $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_IMUL__2">
		<In>
			<Reg id="I_1" registers="RG_GP_16 RG_GP_32"  types="DT_INT_16S DT_INT_32S" />
			<Reg id="I_2" registers="RG_GP_16 RG_GP_32"  types="DT_INT_16S DT_INT_32S" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_16 RG_GP_32"  types="DT_INT_16S DT_INT_32S" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="100" units="U_FP_MUL"/>
		<Asm>imul $I_2, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_IMUL__21">
		<In>
			<Reg id="I_1" registers="RG_GP_16 RG_GP_32"  types="DT_INT_16S DT_INT_32S" />
			<Imm id="I_2" types="DT_INT_16S DT_INT_32S" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_16 RG_GP_32"  types="DT_INT_16S DT_INT_32S" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="100" units="U_FP_MUL"/>
		<Asm>imul$TYPE_I_2 $$I_2, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_IMUL__22">
		<In>
			<Imm id="I_1" types="DT_INT_16S DT_INT_32S" />
			<Reg id="I_2" registers="RG_GP_16 RG_GP_32"  types="DT_INT_16S DT_INT_32S" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_16 RG_GP_32"  types="DT_INT_16S DT_INT_32S" destroys="I_2"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="100" units="U_FP_MUL"/>
		<Asm>imul$TYPE_I_1 $$I_1, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_IMUL__23">
		<In>
			<Reg id="I_1" registers="RG_GP_16 RG_GP_32"  types="DT_INT_16S DT_INT_32S" />
			<Mem id="I_2" types="DT_INT_16S DT_INT_32S" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_16 RG_GP_32"  types="DT_INT_16S DT_INT_32S" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="120" units="U_FP_MUL"/>
		<Asm>imul$TYPE_I_2 $I_2, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_IMUL__24">
		<In>
			<Mem id="I_1" types="DT_INT_16S DT_INT_32S" />
			<Reg id="I_2" registers="RG_GP_16 RG_GP_32"  types="DT_INT_16S DT_INT_32S" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_16 RG_GP_32"  types="DT_INT_16S DT_INT_32S" destroys="I_2"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="120" units="U_FP_MUL"/>
		<Asm>imul$TYPE_I_1 $I_1, $O_1</Asm>
	</Instruction>
	
	<Instruction id="INSTRUCTION_MUL__1">
		<In>
			<Reg id="I_1" registers="R_AL"  types="DT_INT_8U" />
			<Reg id="I_2" registers="RG_GP_8 -R_AL"  types="DT_INT_8U" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_AL"  types="DT_INT_8U" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="100" units="U_FP_MUL"/>
		<Asm>mul $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MUL__11">
		<In>
			<Reg id="I_1" registers="R_AL"  types="DT_INT_8U" />
			<Mem id="I_2" types="DT_INT_8U" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_AL"  types="DT_INT_8U" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="120" units="U_FP_MUL"/>
		<Asm>mul$TYPE_I_2 $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MUL__12">
		<In>
			<Mem id="I_1" types="DT_INT_8U" />
			<Reg id="I_2" registers="R_AL"  types="DT_INT_8U" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_AL"  types="DT_INT_8U" destroys="I_2"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="120" units="U_FP_MUL"/>
		<Asm>mul$TYPE_I_1 $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MUL__2">
		<In>
			<Reg id="I_1" registers="R_AX"  types="DT_INT_16U" />
			<Reg id="I_2" registers="RG_GP_16 -R_AX -R_DX"  types="DT_INT_16U" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_AX"  types="DT_INT_16U" destroys="I_1" />
			<Reg id="O_2" registers="R_DX"  types="DT_INT_16U"/>
			<Reg id="O_3" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="100" units="U_FP_MUL"/>
		<Asm>mul $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MUL__21">
		<In>
			<Reg id="I_1" registers="R_AX"  types="DT_INT_16U" />
			<Mem id="I_2" types="DT_INT_16U" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_AX"  types="DT_INT_16U" destroys="I_1" />
			<Reg id="O_2" registers="R_DX"  types="DT_INT_16U"/>
			<Reg id="O_3" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="120" units="U_FP_MUL"/>
		<Asm>mul$TYPE_I_2 $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MUL__22">
		<In>
			<Mem id="I_1" types="DT_INT_16U" />
			<Reg id="I_2" registers="R_AX"  types="DT_INT_16U" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_AX"  types="DT_INT_16U" destroys="I_2" />
			<Reg id="O_2" registers="R_DX"  types="DT_INT_16U"/>
			<Reg id="O_3" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="120" units="U_FP_MUL"/>
		<Asm>mul$TYPE_I_1 $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MUL__3">
		<In>
			<Reg id="I_1" registers="R_EAX"  types="DT_INT_32U" />
			<Reg id="I_2" registers="RG_GP_32 -R_EAX -R_EDX"  types="DT_INT_32U" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_EAX"  types="DT_INT_32U" destroys="I_1" />
			<Reg id="O_2" registers="R_EDX"  types="DT_INT_32U"/>
			<Reg id="O_3" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="100" units="U_FP_MUL"/>
		<Asm>mul $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MUL__31">
		<In>
			<Reg id="I_1" registers="R_EAX"  types="DT_INT_32U" />
			<Mem id="I_2" types="DT_INT_32U" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_EAX"  types="DT_INT_32U" destroys="I_1" />
			<Reg id="O_2" registers="R_EDX"  types="DT_INT_32U"/>
			<Reg id="O_3" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="120" units="U_FP_MUL"/>
		<Asm>mul$TYPE_I_2 $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MUL__32">
		<In>
			<Mem id="I_1" types="DT_INT_32U" />
			<Reg id="I_2" registers="R_EAX"  types="DT_INT_32U" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_EAX"  types="DT_INT_32U" destroys="I_2" />
			<Reg id="O_2" registers="R_EDX"  types="DT_INT_32U"/>
			<Reg id="O_3" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="120" units="U_FP_MUL"/>
		<Asm>mul$TYPE_I_1 $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_CBW__1">
		<In>
			<Reg id="I_1" registers="R_AL" types="DT_INT_8S" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_AX" types="DT_INT_16S" destroys="I_1"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>
		<Asm>cbw</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_CWD__1">
		<In>
			<Reg id="I_1" registers="R_AX" types="DT_INT_16S" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_DX" types="DT_INT_16S" />
			<Reg id="O_2" registers="R_AX" types="DT_INT_16S" destroys="I_1"/>
			
		</Out>
		<Exec etime="10" units="U_ALU"/>
		<Asm>cwd</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_CDQ__1">
		<In>
			<Reg id="I_1" registers="R_EAX" types="DT_INT_32S" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_EDX" types="DT_INT_32S" />
			<Reg id="O_2" registers="R_EAX" types="DT_INT_32S" destroys="I_1"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>
		<Asm>cdq</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_DIV__1">
		<In>
			<Reg id="I_1" registers="R_AH" types="DT_INT_8U" />
			<Reg id="I_2" registers="R_AL" types="DT_INT_8U" />
			<Reg id="I_3" registers="RG_GP_8 -R_AH -R_AL" types="DT_INT_8U" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_AL" types="DT_INT_8U" destroys="I_2"/>
			<Reg id="O_2" registers="R_AH" types="DT_INT_8U" destroys="I_1" />
			<Reg id="O_3" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="700" units="U_FP_DIV"/>
		<Asm>div $I_3</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_DIV__11">
		<In>
			<Reg id="I_1" registers="R_AH" types="DT_INT_8U" />
			<Reg id="I_2" registers="R_AL" types="DT_INT_8U" />
			<Mem id="I_3" types="DT_INT_8U" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_AL" types="DT_INT_8U" destroys="I_2"/>
			<Reg id="O_2" registers="R_AH" types="DT_INT_8U" destroys="I_1" />
			<Reg id="O_3" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="720" units="U_FP_DIV"/>
		<Asm>div$TYPE_I_3 $I_3</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_DIV__2">
		<In>
			<Reg id="I_1" registers="R_DX" types="DT_INT_16U" />
			<Reg id="I_2" registers="R_AX" types="DT_INT_16U" />
			<Reg id="I_3" registers="RG_GP_16 -R_AX -R_DX" types="DT_INT_16U" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_AX" types="DT_INT_16U" destroys="I_2"/>
			<Reg id="O_2" registers="R_DX" types="DT_INT_16U" destroys="I_1" />
			<Reg id="O_3" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="700" units="U_FP_DIV"/>
		<Asm>div $I_3</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_DIV__21">
		<In>
			<Reg id="I_1" registers="R_DX" types="DT_INT_16U" />
			<Reg id="I_2" registers="R_AX" types="DT_INT_16U" />
			<Mem id="I_3" types="DT_INT_16U" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_AX" types="DT_INT_16U" destroys="I_2"/>
			<Reg id="O_2" registers="R_DX" types="DT_INT_16U" destroys="I_1" />
			<Reg id="O_3" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="720" units="U_FP_DIV"/>
		<Asm>div$TYPE_I_3 $I_3</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_DIV__3">
		<In>
			<Reg id="I_1" registers="R_EDX" types="DT_INT_32U" />
			<Reg id="I_2" registers="R_EAX" types="DT_INT_32U" />
			<Reg id="I_3" registers="RG_GP_32 -R_EAX -R_EDX" types="DT_INT_32U" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_EAX" types="DT_INT_32U" destroys="I_2"/>
			<Reg id="O_2" registers="R_EDX" types="DT_INT_32U" destroys="I_1" />
			<Reg id="O_3" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="700" units="U_FP_DIV"/>
		<Asm>div $I_3</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_DIV__31">
		<In>
			<Reg id="I_1" registers="R_EDX" types="DT_INT_32U" />
			<Reg id="I_2" registers="R_EAX" types="DT_INT_32U" />
			<Mem id="I_3" types="DT_INT_32U" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_EAX" types="DT_INT_32U" destroys="I_2"/>
			<Reg id="O_2" registers="R_EDX" types="DT_INT_32U" destroys="I_1" />
			<Reg id="O_3" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="720" units="U_FP_DIV"/>
		<Asm>div$TYPE_I_3 $I_3</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_NULLAH__1">
		<Out>
			<Reg id="O_1" registers="R_AH" types="DT_INT_8U"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>xor $O_1, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_NULLDX__1">
		<Out>
			<Reg id="O_1" registers="R_DX" types="DT_INT_16U"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>xor $O_1, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_NULLEDX__1">
		<Out>
			<Reg id="O_1" registers="R_EDX" types="DT_INT_32U"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>xor $O_1, $O_1</Asm>		
	</Instruction>
	
	<Instruction id="INSTRUCTION_IDIV__1">
		<In>
			<Reg id="I_1" registers="R_AX" types="DT_INT_16S" />
			<Reg id="I_2" registers="RG_GP_8 -R_AL -R_AH" types="DT_INT_8S" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_AL" types="DT_INT_8S" destroys="I_1"/>
			<Reg id="O_2" registers="R_AH" types="DT_INT_8S" destroys="I_1" />
			<Reg id="O_3" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="700" units="U_FP_DIV"/>	
		<Asm>idiv $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_IDIV__11">
		<In>
			<Reg id="I_1" registers="R_AX" types="DT_INT_16S" />
			<Mem id="I_2" types="DT_INT_8S" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_AL" types="DT_INT_8S" destroys="I_1"/>
			<Reg id="O_2" registers="R_AH" types="DT_INT_8S" destroys="I_1" />
			<Reg id="O_3" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="720" units="U_FP_DIV"/>	
		<Asm>idiv$TYPE_I_2 $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_IDIV__2">
		<In>
			<Reg id="I_1" registers="R_DX" types="DT_INT_16S" />
			<Reg id="I_2" registers="R_AX" types="DT_INT_16S" />
			<Reg id="I_3" registers="RG_GP_16 -R_AX -R_DX" types="DT_INT_16S" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_AX" types="DT_INT_16S" destroys="I_2"/>
			<Reg id="O_2" registers="R_DX" types="DT_INT_16S" destroys="I_1" />
			<Reg id="O_3" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="700" units="U_FP_DIV"/>	
		<Asm>idiv $I_3</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_IDIV__21">
		<In>
			<Reg id="I_1" registers="R_DX" types="DT_INT_16S" />
			<Reg id="I_2" registers="R_AX" types="DT_INT_16S" />
			<Mem id="I_3" types="DT_INT_16S" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_AX" types="DT_INT_16S" destroys="I_2"/>
			<Reg id="O_2" registers="R_DX" types="DT_INT_16S" destroys="I_1" />
			<Reg id="O_3" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="720" units="U_FP_DIV"/>	
		<Asm>idiv$TYPE_I_3 $I_3</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_IDIV__3">
		<In>
			<Reg id="I_1" registers="R_EDX" types="DT_INT_32S" />
			<Reg id="I_2" registers="R_EAX" types="DT_INT_32S" />
			<Reg id="I_3" registers="RG_GP_32 -R_EAX -R_EDX" types="DT_INT_32S" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_EAX" types="DT_INT_32S" destroys="I_2"/>
			<Reg id="O_2" registers="R_EDX" types="DT_INT_32S" destroys="I_1" />
			<Reg id="O_3" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="700" units="U_FP_DIV"/>	
		<Asm>idiv $I_3</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_IDIV__31">
		<In>
			<Reg id="I_1" registers="R_EDX" types="DT_INT_32S" />
			<Reg id="I_2" registers="R_EAX" types="DT_INT_32S" />
			<Mem id="I_3" types="DT_INT_32S" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_EAX" types="DT_INT_32S" destroys="I_2"/>
			<Reg id="O_2" registers="R_EDX" types="DT_INT_32S" destroys="I_1" />
			<Reg id="O_3" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="720" units="U_FP_DIV"/>	
		<Asm>idiv$TYPE_I_3 $I_3</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_NEG__1">
		<In>
			<Reg id="I_1" registers="RG_GP"  types="DTG_INT" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP" types="DTG_INT" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>neg $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_SHL__1">
		<In>
			<Reg id="I_1" registers="RG_GP -R_CL"  types="DTG_INT" />
			<Reg id="I_2" registers="R_CL"  types="DTG_INT_8" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP -R_CL" types="DTG_INT" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>shl $I_2, $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_SHR__1">
		<In>
			<Reg id="I_1" registers="RG_GP -R_CL"  types="DTG_INT" />
			<Reg id="I_2" registers="R_CL"  types="DTG_INT_8" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP -R_CL" types="DTG_INT" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>shr $I_2, $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_SAL__1">
		<In>
			<Reg id="I_1" registers="RG_GP -R_CL"  types="DTG_INT" />
			<Reg id="I_2" registers="R_CL"  types="DTG_INT_8" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP -R_CL" types="DTG_INT" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>sal $I_2, $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_SAR__1">
		<In>
			<Reg id="I_1" registers="RG_GP -R_CL"  types="DTG_INT" />
			<Reg id="I_2" registers="R_CL"  types="DTG_INT_8" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP -R_CL" types="DTG_INT" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>sar $I_2, $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_AND__1">
		<In>
			<Reg id="I_1" registers="RG_GP"  types="DTG_INT DT_BOOL" />
			<Reg id="I_2" registers="RG_GP"  types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT DT_BOOL" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10"  units="U_ALU"/>
		<Asm>and $I_2, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_AND__11">
		<In>
			<Reg id="I_1" registers="RG_GP"  types="DTG_INT DT_BOOL" />
			<Imm id="I_2" types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT DT_BOOL" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10"  units="U_ALU"/>
		<Asm>and$TYPE_I_2 $$I_2, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_AND__12">
		<In>
			<Imm id="I_1" types="DTG_INT DT_BOOL" />
			<Reg id="I_2" registers="RG_GP"  types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT DT_BOOL" destroys="I_2"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10"  units="U_ALU"/>
		<Asm>and$TYPE_I_1 $$I_1, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_AND__13">
		<In>
			<Reg id="I_1" registers="RG_GP"  types="DTG_INT DT_BOOL" />
			<Mem id="I_2" types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT DT_BOOL" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="30"  units="U_ALU"/>
		<Asm>and$TYPE_I_2 $I_2, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_AND__14">
		<In>
			<Mem id="I_1" types="DTG_INT DT_BOOL" />
			<Reg id="I_2" registers="RG_GP"  types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT DT_BOOL" destroys="I_2"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="30"  units="U_ALU"/>
		<Asm>and$TYPE_I_1 $I_1, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_AND__2">
		<In>
			<Reg id="I_1" registers="RG_GP_8"  types="DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DT_INT" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10"  units="U_ALU"/>
		<Asm>and $0x1, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_OR__1">
		<In>
			<Reg id="I_1" registers="RG_GP"  types="DTG_INT DT_BOOL" />
			<Reg id="I_2" registers="RG_GP"  types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT DT_BOOL" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10"  units="U_ALU"/>
		<Asm>or $I_2, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_OR__11">
		<In>
			<Reg id="I_1" registers="RG_GP"  types="DTG_INT DT_BOOL" />
			<Imm id="I_2" types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT DT_BOOL" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10"  units="U_ALU"/>
		<Asm>or$TYPE_I_2 $$I_2, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_OR__12">
		<In>
			<Imm id="I_1" types="DTG_INT DT_BOOL" />
			<Reg id="I_2" registers="RG_GP"  types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT DT_BOOL" destroys="I_2"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10"  units="U_ALU"/>
		<Asm>or$TYPE_I_1 $$I_1, $O_1</Asm>	
	</Instruction>
	
	
	<Instruction id="INSTRUCTION_OR__13">
		<In>
			<Reg id="I_1" registers="RG_GP"  types="DTG_INT DT_BOOL" />
			<Mem id="I_2" types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT DT_BOOL" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="30"  units="U_ALU"/>
		<Asm>or$TYPE_I_2 $I_2, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_OR__14">
		<In>
			<Mem id="I_1" types="DTG_INT DT_BOOL" />
			<Reg id="I_2" registers="RG_GP"  types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT DT_BOOL" destroys="I_2"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="30"  units="U_ALU"/>
		<Asm>or$TYPE_I_1 $I_1, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_XOR__1">
		<In>
			<Reg id="I_1" registers="RG_GP"  types="DTG_INT DT_BOOL" />
			<Reg id="I_2" registers="RG_GP"  types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT DT_BOOL" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10"  units="U_ALU"/>
		<Asm>xor $I_2, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_XOR__11">
		<In>
			<Reg id="I_1" registers="RG_GP"  types="DTG_INT DT_BOOL" />
			<Imm id="I_2" types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT DT_BOOL" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10"  units="U_ALU"/>
		<Asm>xor$TYPE_I_2 $$I_2, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_XOR__12">
		<In>
			<Imm id="I_1"  types="DTG_INT DT_BOOL" />
			<Reg id="I_2" registers="RG_GP"  types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT DT_BOOL" destroys="I_2"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10"  units="U_ALU"/>
		<Asm>xor$TYPE_I_1 $$I_1, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_XOR__13">
		<In>
			<Reg id="I_1" registers="RG_GP"  types="DTG_INT DT_BOOL" />
			<Mem id="I_2" types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT DT_BOOL" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="30"  units="U_ALU"/>
		<Asm>xor$TYPE_I_2 $I_2, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_XOR__14">
		<In>
			<Mem id="I_1"  types="DTG_INT DT_BOOL" />
			<Reg id="I_2" registers="RG_GP"  types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT DT_BOOL" destroys="I_2"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="30"  units="U_ALU"/>
		<Asm>xor$TYPE_I_1 $I_1, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_XOR__2">
		<In>
			<Reg id="I_1" registers="RG_GP"  types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT DT_BOOL" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10"  units="U_ALU"/>
		<Asm>xor $0x1, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_NOT__1">
		<In>
			<Reg id="I_1" registers="RG_GP"  types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP"  types="DTG_INT DT_BOOL" destroys="I_1"/>
		</Out>
		<Exec etime="10"  units="U_ALU"/>
		<Asm>not $O_1</Asm>	
	</Instruction>
	
	
	<Instruction id="INSTRUCTION_CONVERT__1">
		<In>
			<Reg id="I_1" registers="RG_GP" types="DTG_INT" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_8" types="DT_INT_8U"  destroys="I_1"/>
		</Out>
		<Exec etime="0" units=""/>	
		<Asm></Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_CONVERT__5">
		<In>
			<Reg id="I_1" registers="RG_GP" types="DTG_INT" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_8" types="DTG_INT_8"  destroys="I_1"/>
		</Out>
		<Exec etime="0" units=""/>	
		<Asm></Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_CONVERT__2">
		<In>
			<Reg id="I_1" registers="RG_GP_32 RG_GP_16" types="DTG_INT_32 RG_GP_16" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_16" types="DTG_INT_16"  destroys="I_1"/>
		</Out>
		<Exec etime="0" units=""/>	
		<Asm></Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_CONVERT__3">
		<In>
			<Reg id="I_1" registers="RG_GP_32" types="DTG_INT_32" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_32" types="DTG_INT_32"  destroys="I_1"/>
		</Out>
		<Exec etime="0" units=""/>	
		<Asm></Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MOVZX__1">
		<In>
			<Reg id="I_1" registers="RG_GP_8" types="DTG_INT_8" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_32 RG_GP_16" types="DT_INT_32 DT_INT_16"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>movzx $I_1, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MOVZX__2">
		<In>
			<Reg id="I_1" registers="RG_GP_16" types="DT_INT_16" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_32" types="DTG_INT_32"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>movzx $I_1, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MOVZX__3">
		<In>
			<Reg id="I_1" registers="RG_GP_8" types="DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP" types="DTG_INT"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>movzx $I_1, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MOVZX__4">
		<In>
			<Reg id="I_1" registers="RG_GP_8 RG_GP_16" types="DT_BOOL DTG_INT_8 DTG_INT_16" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_32" types="DTG_INT_32"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>movzx $I_1, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MOVZX__5">
		<In>
			<Mem id="I_1" types="DT_BOOL DTG_INT_8 DTG_INT_16" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_32" types="DTG_INT_32"/>
		</Out>
		<Exec etime="30" units="U_ALU"/>	
		<Asm>movzx$TYPE_I_1 $I_1, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MOVZX__6">
		<In>
			<Mem id="I_1" types="DT_BOOL DTG_INT_8" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_16" types="DTG_INT_16"/>
		</Out>
		<Exec etime="30" units="U_ALU"/>	
		<Asm>movzx$TYPE_I_1 $I_1, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MOVSX__1">
		<In>
			<Reg id="I_1" registers="RG_GP_8" types="DTG_INT_8" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_32 RG_GP_16" types="DT_INT_32S DT_INT_16S"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>movsx $I_1, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MOVSX__11">
		<In>
			<Mem id="I_1" types="DTG_INT_8" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_32 RG_GP_16" types="DT_INT_32S DT_INT_16S"/>
		</Out>
		<Exec etime="30" units="U_ALU"/>	
		<Asm>movsx$TYPE_I_1 $I_1, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MOVSX__2">
		<In>
			<Reg id="I_1" registers="RG_GP_16" types="DT_INT_16" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_32" types="DTG_INT_32S"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>movsx $I_1, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_MOVSX__21">
		<In>
			<Mem id="I_1" types="DT_INT_16" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_32" types="DTG_INT_32S"/>
		</Out>
		<Exec etime="30" units="U_ALU"/>	
		<Asm>movsx$TYPE_I_1 $I_1, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_LEA__1">
		<In>
			<Mem id="I_1" types="DTG_INT DT_COND DT_BOOL"/>
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_32"  types="DT_INT_32U" />
		</Out>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>lea $I_1, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_CMP__1">
		<In>
			<Reg id="I_1" registers="RG_GP"  types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>cmp $0, $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_CMP__2">
		<In>
			<Reg id="I_1" registers="RG_GP"  types="DTG_INT DT_BOOL" />
			<Reg id="I_2" registers="RG_GP"  types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>cmp $I_2, $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_CMP__21">
		<In>
			<Reg id="I_1" registers="RG_GP"  types="DTG_INT DT_BOOL" />
			<Imm id="I_2" types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>cmp$TYPE_I_2 $$I_2, $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_CMP__22">
		<In>
			<Reg id="I_1" registers="RG_GP"  types="DTG_INT DT_BOOL" />
			<Mem id="I_2" types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="30" units="U_ALU"/>	
		<Asm>cmp$TYPE_I_2 $I_2, $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_CMP__23">
		<In>
			<Mem id="I_1" types="DTG_INT DT_BOOL" />
			<Reg id="I_2" registers="RG_GP"  types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="30" units="U_ALU"/>	
		<Asm>cmp$TYPE_I_2 $I_2, $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_CMP__24">
		<In>
			<Mem id="I_1" types="DTG_INT DT_BOOL" />
			<Imm id="I_2" types="DTG_INT DT_BOOL" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="30" units="U_ALU"/>	
		<Asm>cmp$TYPE_I_2 $$I_2, $I_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_JMP__1" flags="IF_JUMP">
		<Exec etime="10" units="U_ALU"/>	
		<Asm>jmp $label_1</Asm>	
	</Instruction>
	
	
	<Instruction id="INSTRUCTION_JA__1" flags="IF_JUMP">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>ja $label_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_JZ__1" flags="IF_JUMP">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>jz $label_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_JG__1" flags="IF_JUMP">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Asm>jg $label_1</Asm>	
		<Exec etime="10" units="U_ALU"/>	
		<Props>
			<P key="PROPERTY_TEST_SIGNED" value="true" />
		</Props>
	</Instruction>
	
	<Instruction id="INSTRUCTION_JL__1" flags="IF_JUMP">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Asm>jl $label_1</Asm>	
		<Exec etime="10" units="U_ALU"/>	
		<Props>
			<P key="PROPERTY_TEST_SIGNED" value="true" />
		</Props>
	</Instruction>
	
	<Instruction id="INSTRUCTION_JB__1" flags="IF_JUMP">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>jb $label_1</Asm>	
		<Props>
			<P key="PROPERTY_TEST_SIGNED" value="false" />
		</Props>
	</Instruction>
	
	<Instruction id="INSTRUCTION_JE__1" flags="IF_JUMP">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>je $label_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_JNG__1" flags="IF_JUMP">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>jng $label_1</Asm>	
		<Props>
			<P key="PROPERTY_TEST_SIGNED" value="true" />
		</Props>
	</Instruction>
	
	<Instruction id="INSTRUCTION_JNA__1" flags="IF_JUMP">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>jna $label_1</Asm>	
		<Props>
			<P key="PROPERTY_TEST_SIGNED" value="false" />
		</Props>
	</Instruction>
	
	<Instruction id="INSTRUCTION_JNL__1" flags="IF_JUMP">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Asm>jnl $label_1</Asm>	
		<Exec etime="10" units="U_ALU"/>	
		<Props>
			<P key="PROPERTY_TEST_SIGNED" value="true" />
		</Props>
	</Instruction>
	
	<Instruction id="INSTRUCTION_JNB__1" flags="IF_JUMP">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Asm>jnb $label_1</Asm>	
		<Exec etime="10" units="U_ALU"/>	
		<Props>
			<P key="PROPERTY_TEST_SIGNED" value="false" />
		</Props>
	</Instruction>
	
	<Instruction id="INSTRUCTION_JNE__1" flags="IF_JUMP">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Exec etime="10" units="U_ALU"/>	
		<Asm>jne $label_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_SETG__1">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_8" types="DT_BOOL"/>
		</Out>
		<Asm>setg $O_1</Asm>	
		<Exec etime="50" units="U_ALU"/>	
		<Props>
			<P key="PROPERTY_TEST_SIGNED" value="true" />
		</Props>
	</Instruction>
	
	<Instruction id="INSTRUCTION_SETA__1">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_8" types="DT_BOOL"/>
		</Out>
		<Exec etime="50" units="U_ALU"/>	
		<Asm>seta $O_1</Asm>	
		<Props>
			<P key="PROPERTY_TEST_SIGNED" value="false" />
		</Props>
	</Instruction>
	
	<Instruction id="INSTRUCTION_SETL__1">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_8" types="DT_BOOL"/>
		</Out>
		<Exec etime="50" units="U_ALU"/>	
		<Asm>setl $O_1</Asm>	
		<Props>
			<P key="PROPERTY_TEST_SIGNED" value="true" />
		</Props>
	</Instruction>
	
	<Instruction id="INSTRUCTION_SETB__1">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_8" types="DT_BOOL"/>
		</Out>
		<Exec etime="50" units="U_ALU"/>	
		<Asm>setb $O_1</Asm>	
		<Props>
			<P key="PROPERTY_TEST_SIGNED" value="false" />
		</Props>
	</Instruction>
	
	<Instruction id="INSTRUCTION_SETE__1">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_8" types="DT_BOOL"/>
		</Out>
		<Exec etime="50" units="U_ALU"/>	
		<Asm>sete $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_SETNG__1">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_8" types="DT_BOOL"/>
		</Out>
		<Asm>setng $O_1</Asm>	
		<Exec etime="50" units="U_ALU"/>	
		<Props>
			<P key="PROPERTY_TEST_SIGNED" value="true" />
		</Props>
	</Instruction>
	
	<Instruction id="INSTRUCTION_SETNA__1">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_8" types="DT_BOOL"/>
		</Out>
		<Asm>setna $O_1</Asm>	
		<Exec etime="50" units="U_ALU"/>	
		<Props>
			<P key="PROPERTY_TEST_SIGNED" value="false" />
		</Props>
	</Instruction>
	
	<Instruction id="INSTRUCTION_SETNL__1">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_8" types="DT_BOOL"/>
		</Out>
		<Asm>setnl $O_1</Asm>	
		<Exec etime="50" units="U_ALU"/>	
		<Props>
			<P key="PROPERTY_TEST_SIGNED" value="true" />
		</Props>
	</Instruction>
	
	<Instruction id="INSTRUCTION_SETNB__1">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_8" types="DT_BOOL"/>
		</Out>
		<Exec etime="50" units="U_ALU"/>	
		<Asm>setnb $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_SETNE__1">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_8" types="DT_BOOL"/>
		</Out>
		<Exec etime="50" units="U_ALU"/>	
		<Asm>setne $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_SETNZ__1">
		<In>
			<Reg id="I_1" registers="R_COND" types="DT_COND" />
		</In>
		<Out>
			<Reg id="O_1" registers="RG_GP_8" types="DT_BOOL"/>
		</Out>
		<Exec etime="50" units="U_ALU"/>	
		<Asm>setnz $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_PUSH__1" flags="IF_STORE | IF_SYS">
		<In>
			<Reg id="I_1" registers="R_ESP"  types="DT_INT_32U" />
			<Mem id="I_2" types="DTG_INT_32"/>
		</In>
		<Out>
			<Reg id="O_1" registers="R_ESP"  types="DT_INT_32U" destroys="I_1" />	
		</Out>
		<Exec etime="15" units="U_MEM_STORE U_ALU"/>		
		<Asm>push$TYPE_I_2 $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_PUSH__2" flags="IF_STORE | IF_SYS">
		<In>
			<Reg id="I_1" registers="R_ESP"  types="DT_INT_32U" />
			<Reg id="I_2" registers="RG_GP"  types="DTG_INT_32" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_ESP"  types="DT_INT_32U" destroys="I_1" />	
		</Out>
		<Exec etime="15" units="U_MEM_STORE U_ALU"/>	
		<Asm>push $I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_PUSH__3" flags="IF_STORE | IF_SYS">
		<In>
			<Reg id="I_1" registers="R_ESP"  types="DT_INT_32U" />
			<Imm id="I_2" types="DTG_INT DT_BOOL"/>
		</In>
		<Out>
			<Reg id="O_1" registers="R_ESP"  types="DT_INT_32U" destroys="I_1" />	
		</Out>
		<Exec etime="15" units="U_MEM_STORE U_ALU"/>	
		<Asm>pushl $$I_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_POP__1" flags="IF_SYS">
		<In>
			<Reg id="I_1" registers="R_ESP"  types="DT_INT_32U" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_ESP"  types="DT_INT_32U" destroys="I_1" />
			<Reg id="O_2" registers="RG_GP" types="DTG_INT DT_COND DT_BOOL"/>
		</Out>
		<Exec etime="15" units="U_MEM_LOAD U_ALU"/>	
		<Asm>pop $O_2</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_NOP__1" flags="IF_SYS">
		<Asm>nop</Asm>	
		<Exec etime="10" units="U_ALU"/>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_ESPADD__1" flags="IF_SYS">
		<In>
			<Reg id="I_1" registers="R_ESP"  types="DT_INT_32" />
			<Imm id="I_2" types="DTG_INT" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_ESP"  types="DT_INT_32" destroys="I_1"/>
			<Reg id="O_2" registers="R_COND"  types="DT_COND"/>
		</Out>
		<Exec etime="10" units="U_ALU"/>
		<Asm>add$TYPE_I_2 $$I_2, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_BACKUPESP__1" flags="IF_SYS">
		<In>
			<Reg id="I_1" registers="R_ESP R_EBP" types="DT_INT_32U" />
		</In>
		<Out>
			<Reg id="O_1" registers="R_ESP R_EBP" types="DT_INT_32U" />
		</Out>
		<Exec etime="10" units="U_ALU"/>
		<Asm>mov $I_1, $O_1</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_CALL__1" flags="IF_SYS">
		<Out>
			<Reg id="O_1" registers="R_EAX" types="DTG_INT"/>
			<Reg id="O_2" registers="R_ECX" types="DTG_INT"/>
			<Reg id="O_3" registers="R_EDX" types="DTG_INT"/>
		</Out>
		<Exec etime="50" units="U_ALU MEM_STORE"/>	
		<Asm>call $name</Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_PROLOGUE__1" flags="IF_SYS">
		<Out>
			<Reg id="O_1" registers="R_EDI" types="DTG_INT"/>
			<Reg id="O_2" registers="R_ESI" types="DTG_INT"/>
			<Reg id="O_3" registers="R_EBX" types="DTG_INT"/>
		</Out>
		<Exec etime="0" units=""/>	
		<Asm></Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_EPILOGUE__1" flags="IF_SYS">
		<In>
			<Reg id="I_1" registers="R_EDI" types="DTG_INT"/>
			<Reg id="I_2" registers="R_ESI" types="DTG_INT"/>
			<Reg id="I_3" registers="R_EBX" types="DTG_INT"/>
		</In>
		<Exec etime="0" units=""/>	
		<Asm></Asm>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_LEAVE__1" flags="IF_SYS">
		<Asm>leave</Asm>	
		<Exec etime="10" units="U_ALU"/>	
	</Instruction>
	
	<Instruction id="INSTRUCTION_RET__1" flags="IF_SYS">
		<Asm>ret</Asm>
		<Exec etime="80" units="U_MEM_LOAD U_ALU"/>		
	</Instruction>
	
</Instructions>
