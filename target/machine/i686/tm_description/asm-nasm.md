<?xml version="1.0"?>
<Assembler>
		<Entities>
			<!-- common -->
			<Entity id="ent_instruction_delimiter">\n\t</Entity>
			<Entity id="ent_one_line_comment">\n\t;$text</Entity>
			<Entity id="ent_inline_comment">\t;$text</Entity>
			
			<!--declarations -->
			<Entity id="ent_global_simple_decl">\n$id: $type $value</Entity>
			<Entity id="ent_global_symbol_decl">\nglobal $name</Entity>
			<Entity id="ent_extern_symbol_decl">\nextern $name</Entity>
			<Entity id="ent_internal_symbol_decl"></Entity>
			
			<!-- mem access -->
			<Entity id="ent_address_disp">[$disp]</Entity>
			<Entity id="ent_address_disp_base">[$base$psign$disp]</Entity>
			<Entity id="ent_address_base">[$base]</Entity>
			
			<!-- function -->
			<Entity id="ent_function_label">\n$name:</Entity>
			<Entity id="ent_main_name">main</Entity>
			
			<!-- label -->
			<Entity id="ent_label">@Lbl_$id</Entity>
			<Entity id="ent_label_def">$label:</Entity>
			
			<!-- literal -->
			<Entity id="ent_literal">Lit_$id</Entity>
			
			<!-- section names -->
			<Entity id="ent_section_data">\nsection .data\n</Entity>
			<Entity id="ent_section_text">\nsection .text\n</Entity>
			<Entity id="ent_section_uninitialized_data">\nsection .bss\n</Entity>

			<!-- space reservation -->
			<Entity id="ent_space_reserve">resb $size</Entity>
		</Entities>
</Assembler>
	
