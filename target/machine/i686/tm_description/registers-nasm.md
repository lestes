<?xml version="1.0"?>
<Registers>
		<Register id="R_AL" bitwidth="8" types="DTG_INT_8 DT_BOOL" aliases="R_AX R_EAX" asm="AL" move_templates="TEM_REGISTER_MOVE_1"/>
		<Register id="R_AH" bitwidth="8" types="DTG_INT_8  DT_BOOL DT_COND" aliases="R_AX R_EAX" asm="AH" move_templates="TEM_REGISTER_MOVE_1"/>
		<Register id="R_AX" bitwidth="16" types="DTG_INT_16" aliases="R_AH R_AL R_EAX" asm="AX" move_templates="TEM_REGISTER_MOVE_1"/>
		<Register id="R_EAX" bitwidth="32" types="DTG_INT_32" aliases="R_AH R_AL R_AX" asm="EAX" move_templates="TEM_REGISTER_MOVE_1"/>
		<Register id="R_BL" bitwidth="8" types="DTG_INT_8  DT_BOOL" aliases="R_BX R_EBX" asm="BL" move_templates="TEM_REGISTER_MOVE_1"/>
		<Register id="R_BH" bitwidth="8" types="DTG_INT_8  DT_BOOL" aliases="R_BX R_EBX" asm="BH" move_templates="TEM_REGISTER_MOVE_1"/>
		<Register id="R_BX" bitwidth="16" types="DTG_INT_16" aliases="R_BH R_BL R_EBX" asm="BX" move_templates="TEM_REGISTER_MOVE_1"/>
		<Register id="R_EBX" bitwidth="32" types="DTG_INT_32" aliases="R_BH R_BL R_BX" asm="EBX" move_templates="TEM_REGISTER_MOVE_1"/>
		<Register id="R_CL" bitwidth="8" types="DTG_INT_8  DT_BOOL" aliases="R_CX R_ECX" asm="CL" move_templates="TEM_REGISTER_MOVE_1"/>
		<Register id="R_CH" bitwidth="8" types="DTG_INT_8  DT_BOOL" aliases="R_CX R_ECX" asm="CH" move_templates="TEM_REGISTER_MOVE_1"/>
		<Register id="R_CX" bitwidth="16" types="DTG_INT_16" aliases="R_CL R_CH R_ECX" asm="CX" move_templates="TEM_REGISTER_MOVE_1"/>
		<Register id="R_ECX" bitwidth="32" types="DTG_INT_32" aliases="R_CL R_CH R_CX" asm="ECX" move_templates="TEM_REGISTER_MOVE_1"/>
		<Register id="R_DL" bitwidth="8" types="DTG_INT_8  DT_BOOL" aliases="R_DX R_EDX" asm="DL" move_templates="TEM_REGISTER_MOVE_1"/>
		<Register id="R_DH" bitwidth="8" types="DTG_INT_8  DT_BOOL" aliases="R_DX R_EDX" asm="DH" move_templates="TEM_REGISTER_MOVE_1"/>
		<Register id="R_DX" bitwidth="16" types="DTG_INT_16" aliases="R_DL R_DH R_EDX" asm="DX" move_templates="TEM_REGISTER_MOVE_1"/>
		<Register id="R_EDX" bitwidth="32" types="DTG_INT_32" aliases="R_DL R_DH R_DX" asm="EDX" move_templates="TEM_REGISTER_MOVE_1"/>
		<Register id="R_DI" bitwidth="16" types="DTG_INT_16" aliases="R_EDI" asm="DI" move_templates="TEM_REGISTER_MOVE_1"/>
		<Register id="R_EDI" bitwidth="32" types="DTG_INT_32" aliases="R_DI" asm="EDI" move_templates="TEM_REGISTER_MOVE_1"/>
		<Register id="R_SI" bitwidth="16" types="DTG_INT_16" aliases="R_ESI" asm="SI" move_templates="TEM_REGISTER_MOVE_1"/>
		<Register id="R_ESI" bitwidth="32" types="DTG_INT_32" aliases="R_SI" asm="ESI" move_templates="TEM_REGISTER_MOVE_1"/>
		
		<Register id="R_COND" bitwidth="8" types="DT_COND" move_templates="TEM_REGISTER_MOVE_2 TEM_REGISTER_MOVE_3 TEM_REGISTER_MOVE_4"/>
		
		<Register id="R_ESP" bitwidth="32" types="DT_INT_32U" asm="ESP" move_templates="TEM_REGISTER_MOVE_5" flags="RF_NOSPILL"/>
		<Register id="R_EBP" bitwidth="32" types="DT_INT_32U" asm="EBP" move_templates="TEM_REGISTER_MOVE_5" flags="RF_NOSPILL"/>
		
		<RegisterGroup id="RG_GP_8" registers="R_AL R_AH R_BL R_BH R_CL R_CH R_DL R_DH" />
		<RegisterGroup id="RG_GP_16" registers="R_AX R_BX R_CX R_DX R_DI R_SI" />
		<RegisterGroup id="RG_GP_32" registers="R_EAX R_EBX R_ECX R_EDX R_EDI R_ESI" />
		<RegisterGroup id="RG_GP" registers="RG_GP_32 RG_GP_16 RG_GP_8" />
		


</Registers>
