<?xml version="1.0"?>
<Pi2Ge>
	
	<Template id="TEM_ADD_1" ref_pi="pi_add">
		<Instr ref_instr="INSTRUCTION_ADD__*">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />	
				<Op ref_op="O_2" pid="ADD_O_2" />	
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_SUB_1" ref_pi="pi_sub">
		<Instr ref_instr="INSTRUCTION_SUB__*">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />	
				<Op ref_op="O_2" pid="SUB_O_2" />	
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_MUL_1" ref_pi="pi_mul">
		<Instr ref_instr="INSTRUCTION_MUL__1 INSTRUCTION_IMUL__*">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />	
				<Op ref_op="O_2" pid="MUL_O_2" />	
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_MUL_2" ref_pi="pi_mul">
		<Instr ref_instr="INSTRUCTION_MUL__2 INSTRUCTION_MUL__3">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />	
				<Op ref_op="O_2" pid="MUL_O_2" />	
				<Op ref_op="O_3" pid="MUL_O_3" />	
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_DIV_1" ref_pi="pi_div">
		<Instr ref_instr="INSTRUCTION_CBW__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="CBW_O_1" />
			</Out>
		</Instr>
		
		<Instr ref_instr="INSTRUCTION_IDIV__1">
			<In>
				<Op ref_op="I_1" pid="CBW_O_1" />
				<Op ref_op="I_2" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />	
				<Op ref_op="O_2" pid="DIV_O_2" />
				<Op ref_op="O_3" pid="DIV_O_3" />		
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_DIV_2" ref_pi="pi_div">
		<Instr ref_instr="INSTRUCTION_CWD__1 INSTRUCTION_CDQ__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="CWD_O_1" />
				<Op ref_op="O_2" pid="CWD_O_2" />
			</Out>
		</Instr>
		
		<Instr ref_instr="INSTRUCTION_IDIV__2 INSTRUCTION_IDIV__3">
			<In>
				<Op ref_op="I_1" pid="CWD_O_1" />
				<Op ref_op="I_2" pid="CWD_O_2" />
				<Op ref_op="I_3" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />	
				<Op ref_op="O_2" pid="DIV_O_2" />
				<Op ref_op="O_3" pid="DIV_O_3" />		
			</Out>
		</Instr>
	</Template>
	
	
	<Template id="TEM_DIV_3" ref_pi="pi_div">
		<Instr ref_instr="INSTRUCTION_NULLAH__1 INSTRUCTION_NULLDX__1 INSTRUCTION_NULLEDX__1">
			<Out>
				<Op ref_op="O_1" pid="NULL_O_1" />
			</Out>
		</Instr>
		
		<Instr ref_instr="INSTRUCTION_DIV__1 INSTRUCTION_DIV__2 INSTRUCTION_DIV__3">
			<In>
				<Op ref_op="I_1" pid="NULL_O_1" />
				<Op ref_op="I_2" pid="PIN_1" />
				<Op ref_op="I_3" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />	
				<Op ref_op="O_2" pid="DIV_O_2" />
				<Op ref_op="O_3" pid="DIV_O_3" />		
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_MOD_1" ref_pi="pi_mod">
		<Instr ref_instr="INSTRUCTION_CBW__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="CBW_O_1" />
			</Out>
		</Instr>
		
		<Instr ref_instr="INSTRUCTION_IDIV__1">
			<In>
				<Op ref_op="I_1" pid="CBW_O_1" />
				<Op ref_op="I_2" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="DIV_O_1" />	
				<Op ref_op="O_2" pid="POUT_1" />
				<Op ref_op="O_3" pid="DIV_O_3" />		
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_MOD_2" ref_pi="pi_mod">
		<Instr ref_instr="INSTRUCTION_CWD__1 INSTRUCTION_CDQ__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="CWD_O_1" />
				<Op ref_op="O_2" pid="CWD_O_2" />
			</Out>
		</Instr>
		
		<Instr ref_instr="INSTRUCTION_IDIV__2 INSTRUCTION_IDIV__3">
			<In>
				<Op ref_op="I_1" pid="CWD_O_1" />
				<Op ref_op="I_2" pid="CWD_O_2" />
				<Op ref_op="I_3" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="DIV_O_1" />	
				<Op ref_op="O_2" pid="POUT_1" />	
				<Op ref_op="O_3" pid="DIV_O_3" />	
			</Out>
		</Instr>
	</Template>
	
	
	<Template id="TEM_MOD_3" ref_pi="pi_mod">
		<Instr ref_instr="INSTRUCTION_NULLAH__1 INSTRUCTION_NULLDX__1 INSTRUCTION_NULLEDX__1">
			<Out>
				<Op ref_op="O_1" pid="NULL_O_1" />
			</Out>
		</Instr>
		
		<Instr ref_instr="INSTRUCTION_DIV__1 INSTRUCTION_DIV__2 INSTRUCTION_DIV__3">
			<In>
				<Op ref_op="I_1" pid="NULL_O_1" />
				<Op ref_op="I_2" pid="PIN_1" />
				<Op ref_op="I_3" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="DIV_O_1" />	
				<Op ref_op="O_2" pid="POUT_1" />
				<Op ref_op="O_3" pid="DIV_O_3" />		
			</Out>
		</Instr>
	</Template>
	
	
	<Template id="TEM_REM_1" ref_pi="pi_rem">
		<Instr ref_instr="INSTRUCTION_CBW__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="CBW_O_1" />
			</Out>
		</Instr>
		
		<Instr ref_instr="INSTRUCTION_IDIV__1">
			<In>
				<Op ref_op="I_1" pid="CBW_O_1" />
				<Op ref_op="I_2" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="DIV_O_1" />	
				<Op ref_op="O_2" pid="POUT_1" />
				<Op ref_op="O_3" pid="DIV_O_3" />		
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_REM_2" ref_pi="pi_rem">
		<Instr ref_instr="INSTRUCTION_CWD__1 INSTRUCTION_CDQ__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="CWD_O_1" />
				<Op ref_op="O_2" pid="CWD_O_2" />
			</Out>
		</Instr>
		
		<Instr ref_instr="INSTRUCTION_IDIV__2 INSTRUCTION_IDIV__3">
			<In>
				<Op ref_op="I_1" pid="CWD_O_1" />
				<Op ref_op="I_2" pid="CWD_O_2" />
				<Op ref_op="I_3" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="DIV_O_1" />	
				<Op ref_op="O_2" pid="POUT_1" />
				<Op ref_op="O_3" pid="DIV_O_3" />		
			</Out>
		</Instr>
	</Template>
	
	
	<Template id="TEM_REM_3" ref_pi="pi_rem">
		<Instr ref_instr="INSTRUCTION_NULLAH__1 INSTRUCTION_NULLDX__1 INSTRUCTION_NULLEDX__1">
			<Out>
				<Op ref_op="O_1" pid="NULL_O_1" />
			</Out>
		</Instr>
		
		<Instr ref_instr="INSTRUCTION_DIV__1 INSTRUCTION_DIV__2 INSTRUCTION_DIV__3">
			<In>
				<Op ref_op="I_1" pid="NULL_O_1" />
				<Op ref_op="I_2" pid="PIN_1" />
				<Op ref_op="I_3" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="DIV_O_1" />	
				<Op ref_op="O_2" pid="POUT_1" />	
				<Op ref_op="O_3" pid="DIV_O_3" />		
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_NEG_1" ref_pi="pi_neg">
		<Instr ref_instr="INSTRUCTION_NEG__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
				<Op ref_op="O_2" pid="NEG_O_2" />		
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_GAT_1" ref_pi="pi_gat">
		<Instr ref_instr="INSTRUCTION_MOV__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	
	<Template id="TEM_SHL_1" ref_pi="pi_shl">
		<Instr ref_instr="INSTRUCTION_CONVERT__1">
			<In>
				<Op ref_op="I_1" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="CVT_O_1" />
			</Out>
		</Instr>
		<Instr ref_instr="INSTRUCTION_SHL__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="CVT_O_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
				<Op ref_op="O_2" pid="SHL_O_2" />		
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_SHR_1" ref_pi="pi_shr">
		<Instr ref_instr="INSTRUCTION_CONVERT__1">
			<In>
				<Op ref_op="I_1" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="CVT_O_1" />
			</Out>
		</Instr>
		<Instr ref_instr="INSTRUCTION_SHR__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="CVT_O_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
				<Op ref_op="O_2" pid="SHR_O_2" />		
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_SAL_1" ref_pi="pi_sal">
		<Instr ref_instr="INSTRUCTION_CONVERT__1">
			<In>
				<Op ref_op="I_1" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="CVT_O_1" />
			</Out>
		</Instr>
		<Instr ref_instr="INSTRUCTION_SAL__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="CVT_O_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
				<Op ref_op="O_2" pid="SAL_O_2" />		
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_SAR_1" ref_pi="pi_sar">
		<Instr ref_instr="INSTRUCTION_CONVERT__1">
			<In>
				<Op ref_op="I_1" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="CVT_O_1" />
			</Out>
		</Instr>
		<Instr ref_instr="INSTRUCTION_SAR__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="CVT_O_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
				<Op ref_op="O_2" pid="SAR_O_2" />		
			</Out>
		</Instr>
	</Template>
	
	
	<Template id="TEM_BAND_1" ref_pi="pi_band">
		<Instr ref_instr="INSTRUCTION_AND__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />	
				<Op ref_op="O_2" pid="AND_O_2" />	
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_BOR_1" ref_pi="pi_bor">
		<Instr ref_instr="INSTRUCTION_OR__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />	
				<Op ref_op="O_2" pid="OR_O_2" />	
			</Out>
		</Instr>
	</Template>
	
	
	<Template id="TEM_BXOR_1" ref_pi="pi_bxor">
		<Instr ref_instr="INSTRUCTION_XOR__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />	
				<Op ref_op="O_2" pid="XOR_O_2" />	
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_BNOT_1" ref_pi="pi_bnot">
		<Instr ref_instr="INSTRUCTION_NOT__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />	
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_LAND_1" ref_pi="pi_land">
		<Instr ref_instr="INSTRUCTION_AND__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />	
				<Op ref_op="O_2" pid="AND_O_2" />	
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_LOR_1" ref_pi="pi_lor">
		<Instr ref_instr="INSTRUCTION_OR__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />	
				<Op ref_op="O_2" pid="OR_O_2" />	
			</Out>
		</Instr>
	</Template>
	
	
	<Template id="TEM_LXOR_1" ref_pi="pi_lxor">
		<Instr ref_instr="INSTRUCTION_XOR__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />	
				<Op ref_op="O_2" pid="XOR_O_2" />	
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_LNOT_1" ref_pi="pi_lnot">
		<Instr ref_instr="INSTRUCTION_XOR__2">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
				<Op ref_op="O_2" pid="XOR_O_2" />		
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_CVT_1" ref_pi="pi_cvt">
		<Instr ref_instr="INSTRUCTION_CONVERT__* INSTRUCTION_MOVZX__* INSTRUCTION_MOVSX__*">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_CVT_2" ref_pi="pi_cvt">
		<Instr ref_instr="INSTRUCTION_CMP__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="CMP_O_1" />
			</Out>
		</Instr>
		<Instr ref_instr="INSTRUCTION_SETNZ__1">
			<In>
				<Op ref_op="I_1" pid="CMP_O_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_CVT_3" ref_pi="pi_cvt">
		<Instr ref_instr="INSTRUCTION_AND__2">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
				<Op ref_op="O_2" pid="AND_O_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_MOV_1" ref_pi="pi_mov">
		<Instr ref_instr="INSTRUCTION_MOV__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_ST_1" ref_pi="pi_st">
		<Instr ref_instr="INSTRUCTION_MOV__2 INSTRUCTION_MOV__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_STV_1" ref_pi="pi_stv">
		<Instr ref_instr="INSTRUCTION_MOV__2 INSTRUCTION_MOV__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_STI_1" ref_pi="pi_sti">
		<Instr ref_instr="INSTRUCTION_MOV__4">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_STIV_1" ref_pi="pi_stiv">
		<Instr ref_instr="INSTRUCTION_MOV__4">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
		
	<Template id="TEM_STP_1" ref_pi="pi_stp">
		<Instr ref_instr="INSTRUCTION_MOV__7">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_STPV_1" ref_pi="pi_stpv">
		<Instr ref_instr="INSTRUCTION_MOV__7">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_LD_1" ref_pi="pi_ld">
		<Instr ref_instr="INSTRUCTION_MOV__3 INSTRUCTION_MOV__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_LDV_1" ref_pi="pi_ldv">
		<Instr ref_instr="INSTRUCTION_MOV__3 INSTRUCTION_MOV__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_LDI_1" ref_pi="pi_ldi">
		<Instr ref_instr="INSTRUCTION_MOV__5">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_LDP_1" ref_pi="pi_ldp">
		<Instr ref_instr="INSTRUCTION_MOV__8">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="PIN_2" />	
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_LDPV_1" ref_pi="pi_ldpv">
		<Instr ref_instr="INSTRUCTION_MOV__8">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="PIN_2" />	
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_LDA_1" ref_pi="pi_lda">
		<Instr ref_instr="INSTRUCTION_LEA__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_CMP_1" ref_pi="pi_cmp">
		<Instr ref_instr="INSTRUCTION_CMP__2">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_BA_1" ref_pi="pi_ba">
		<Instr ref_instr="INSTRUCTION_JMP__1">
		</Instr>
	</Template>
	
	<Template id="TEM_BN_1" ref_pi="pi_bn">
		<Instr ref_instr="INSTRUCTION_NOP__1">
		</Instr>
	</Template>
	
	<Template id="TEM_BT_1" ref_pi="pi_bt">
		<Instr ref_instr="INSTRUCTION_CMP__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="CMP_O_1" />
			</Out>
		</Instr>
		<Instr ref_instr="INSTRUCTION_JA__1">
			<In>
				<Op ref_op="I_1" pid="CMP_O_1" />
			</In>
		</Instr>
	</Template>
	
	<Template id="TEM_BF_1" ref_pi="pi_bf">
		<Instr ref_instr="INSTRUCTION_CMP__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="CMP_O_1" />
			</Out>
		</Instr>
		<Instr ref_instr="INSTRUCTION_JZ__1">
			<In>
				<Op ref_op="I_1" pid="CMP_O_1" />
			</In>
		</Instr>
	</Template>
	
	<Template id="TEM_BG_1" ref_pi="pi_bg">
		<Instr ref_instr="INSTRUCTION_JG__1 INSTRUCTION_JA__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
		</Instr>
	</Template>
	
	<Template id="TEM_BL_1" ref_pi="pi_bl">
		<Instr ref_instr="INSTRUCTION_JL__1 INSTRUCTION_JB__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
		</Instr>
	</Template>
	
	<Template id="TEM_BE_1" ref_pi="pi_be">
		<Instr ref_instr="INSTRUCTION_JE__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
		</Instr>
	</Template>
	
	<Template id="TEM_BNG_1" ref_pi="pi_bng">
		<Instr ref_instr="INSTRUCTION_JNG__1 INSTRUCTION_JNA__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
		</Instr>
	</Template>
	
	<Template id="TEM_BNL_1" ref_pi="pi_bnl">
		<Instr ref_instr="INSTRUCTION_JNL__1 INSTRUCTION_JNB__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
		</Instr>
	</Template>
	
	<Template id="TEM_BNE_1" ref_pi="pi_bne">
		<Instr ref_instr="INSTRUCTION_JNE__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
		</Instr>
	</Template>
	
	<Template id="TEM_SBG_1" ref_pi="pi_sbg">
		<Instr ref_instr="INSTRUCTION_CMP__2">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="CMP_O_1" />
			</Out>
		</Instr>
		<Instr ref_instr="INSTRUCTION_SETG__1 INSTRUCTION_SETA__1">
			<In>
				<Op ref_op="I_1" pid="CMP_O_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_SBL_1" ref_pi="pi_sbl">
		<Instr ref_instr="INSTRUCTION_CMP__2">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="CMP_O_1" />
			</Out>
		</Instr>
		<Instr ref_instr="INSTRUCTION_SETL__1 INSTRUCTION_SETB__1">
			<In>
				<Op ref_op="I_1" pid="CMP_O_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_SBE_1" ref_pi="pi_sbe">
		<Instr ref_instr="INSTRUCTION_CMP__2">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="CMP_O_1" />
			</Out>
		</Instr>
		<Instr ref_instr="INSTRUCTION_SETE__1">
			<In>
				<Op ref_op="I_1" pid="CMP_O_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_SBNG_1" ref_pi="pi_sbng">
		<Instr ref_instr="INSTRUCTION_CMP__2">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="CMP_O_1" />
			</Out>
		</Instr>
		<Instr ref_instr="INSTRUCTION_SETNG__1 INSTRUCTION_SETNA__1">
			<In>
				<Op ref_op="I_1" pid="CMP_O_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_SBNL_1" ref_pi="pi_sbnl">
		<Instr ref_instr="INSTRUCTION_CMP__2">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="CMP_O_1" />
			</Out>
		</Instr>
		<Instr ref_instr="INSTRUCTION_SETNL__1 INSTRUCTION_SETNB__1">
			<In>
				<Op ref_op="I_1" pid="CMP_O_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_SBNE_1" ref_pi="pi_sbne">
		<Instr ref_instr="INSTRUCTION_CMP__2">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
				<Op ref_op="I_2" pid="PIN_2" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="CMP_O_1" />
			</Out>
		</Instr>
		<Instr ref_instr="INSTRUCTION_SETNE__1">
			<In>
				<Op ref_op="I_1" pid="CMP_O_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_REGISTER_MOVE_1" >
		<Instr ref_instr="INSTRUCTION_MOV__1 INSTRUCTION_MOV__2 INSTRUCTION_MOV__3 ">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_REGISTER_MOVE_2" >
		<Instr ref_instr="INSTRUCTION_LAHF__1 INSTRUCTION_SAHF__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_REGISTER_MOVE_3" >
		<Instr ref_instr="INSTRUCTION_LAHF__1">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="LAHF_O_1" />
			</Out>
		</Instr>
		
		<Instr ref_instr="INSTRUCTION_MOV__1 INSTRUCTION_MOV__2">
			<In>
				<Op ref_op="I_1" pid="LAHF_O_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_REGISTER_MOVE_4" >
		<Instr ref_instr="INSTRUCTION_MOV__1 INSTRUCTION_MOV__3">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="MOV_O_1" />
			</Out>
		</Instr>
		
		<Instr ref_instr="INSTRUCTION_SAHF__1">
			<In>
				<Op ref_op="I_1" pid="MOV_O_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
	<Template id="TEM_REGISTER_MOVE_5" >
		<Instr ref_instr="INSTRUCTION_SYSMOV__9 INSTRUCTION_SYSMOV__10 INSTRUCTION_SYSMOV__11 ">
			<In>
				<Op ref_op="I_1" pid="PIN_1" />
			</In>
			<Out>
				<Op ref_op="O_1" pid="POUT_1" />
			</Out>
		</Instr>
	</Template>
	
</Pi2Ge>
