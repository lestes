<?xml version="1.0"?>
<Registers>
	<Register id="R_AL" bitwidth="8" types="DTG_INT_8 DT_BOOL" aliases="R_AX R_EAX" asm="%al" move_templates="TEM_REGISTER_MOVE_1"/>
	<Register id="R_AH" bitwidth="8" types="DTG_INT_8  DT_BOOL DT_COND" aliases="R_AX R_EAX" asm="%ah" move_templates="TEM_REGISTER_MOVE_1"/>
	<Register id="R_AX" bitwidth="16" types="DTG_INT_16" aliases="R_AH R_AL R_EAX" asm="%ax" move_templates="TEM_REGISTER_MOVE_1"/>
	<Register id="R_EAX" bitwidth="32" types="DTG_INT_32" aliases="R_AH R_AL R_AX" asm="%eax" move_templates="TEM_REGISTER_MOVE_1"/>
	<Register id="R_BL" bitwidth="8" types="DTG_INT_8  DT_BOOL" aliases="R_BX R_EBX" asm="%bl" move_templates="TEM_REGISTER_MOVE_1"/>
	<Register id="R_BH" bitwidth="8" types="DTG_INT_8  DT_BOOL" aliases="R_BX R_EBX" asm="%bh" move_templates="TEM_REGISTER_MOVE_1"/>
	<Register id="R_BX" bitwidth="16" types="DTG_INT_16" aliases="R_BH R_BL R_EBX" asm="%bx" move_templates="TEM_REGISTER_MOVE_1"/>
	<Register id="R_EBX" bitwidth="32" types="DTG_INT_32" aliases="R_BH R_BL R_BX" asm="%ebx" move_templates="TEM_REGISTER_MOVE_1"/>
	<Register id="R_CL" bitwidth="8" types="DTG_INT_8  DT_BOOL" aliases="R_CX R_ECX" asm="%cl" move_templates="TEM_REGISTER_MOVE_1"/>
	<Register id="R_CH" bitwidth="8" types="DTG_INT_8  DT_BOOL" aliases="R_CX R_ECX" asm="%ch" move_templates="TEM_REGISTER_MOVE_1"/>
	<Register id="R_CX" bitwidth="16" types="DTG_INT_16" aliases="R_CL R_CH R_ECX" asm="%cx" move_templates="TEM_REGISTER_MOVE_1"/>
	<Register id="R_ECX" bitwidth="32" types="DTG_INT_32" aliases="R_CL R_CH R_CX" asm="%ecx" move_templates="TEM_REGISTER_MOVE_1"/>
	<Register id="R_DL" bitwidth="8" types="DTG_INT_8  DT_BOOL" aliases="R_DX R_EDX" asm="%dl" move_templates="TEM_REGISTER_MOVE_1"/>
	<Register id="R_DH" bitwidth="8" types="DTG_INT_8  DT_BOOL" aliases="R_DX R_EDX" asm="%dh" move_templates="TEM_REGISTER_MOVE_1"/>
	<Register id="R_DX" bitwidth="16" types="DTG_INT_16" aliases="R_DL R_DH R_EDX" asm="%dx" move_templates="TEM_REGISTER_MOVE_1"/>
	<Register id="R_EDX" bitwidth="32" types="DTG_INT_32" aliases="R_DL R_DH R_DX" asm="%edx" move_templates="TEM_REGISTER_MOVE_1"/>
	<Register id="R_DI" bitwidth="16" types="DTG_INT_16" aliases="R_EDI" asm="%di" move_templates="TEM_REGISTER_MOVE_1"/>
	<Register id="R_EDI" bitwidth="32" types="DTG_INT_32" aliases="R_DI" asm="%edi" move_templates="TEM_REGISTER_MOVE_1"/>
	<Register id="R_SI" bitwidth="16" types="DTG_INT_16" aliases="R_ESI" asm="%si" move_templates="TEM_REGISTER_MOVE_1"/>
	<Register id="R_ESI" bitwidth="32" types="DTG_INT_32" aliases="R_SI" asm="%esi" move_templates="TEM_REGISTER_MOVE_1"/>
		
	<Register id="R_COND" bitwidth="8" types="DT_COND" move_templates="TEM_REGISTER_MOVE_2 TEM_REGISTER_MOVE_3 TEM_REGISTER_MOVE_4"/>
		
	<Register id="R_ESP" bitwidth="32" types="DT_INT_32U" asm="%esp" move_templates="TEM_REGISTER_MOVE_5" flags="RF_NOSPILL"/>
	<Register id="R_EBP" bitwidth="32" types="DT_INT_32U" asm="%ebp" move_templates="TEM_REGISTER_MOVE_5" flags="RF_NOSPILL"/>
		
	<RegisterGroup id="RG_GP_8" registers="R_AL R_AH R_BL R_BH R_CL R_CH R_DL R_DH" />
	<RegisterGroup id="RG_GP_16" registers="R_AX R_BX R_CX R_DX R_DI R_SI" />
	<RegisterGroup id="RG_GP_32" registers="R_EAX R_EBX R_ECX R_EDX R_EDI R_ESI" />
	<RegisterGroup id="RG_GP" registers="RG_GP_32 RG_GP_16 RG_GP_8" />
</Registers>
