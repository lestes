#!/bin/bash

if [ "x$1" == "x" ];
then
	AS="nasm"
elif [ "$1" == "nasm" -o "$1" == "gas" ];
then
	AS=$1
else
	echo "Synopsis: $0 [nasm|gas]"
	exit 1
fi

MD="machine_description.md"
MDSRC="types-$AS.md registers-$AS.md instructions-$AS.md asm-$AS.md pi2ge.md"

echo -e "<?xml version=\"1.0\"?>\n<TargetArchitecture name=\"i686\" xsi:schemaLocation=\"http://lestes.jikos.cz/machinedescription ../../schema/target_architecture.xsd\" xmlns=\"http://lestes.jikos.cz/machinedescription\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n" > $MD

for file in $MDSRC;
do
	(( LINES =`cat $file | wc -l` - 1 ))
	tail -n	$LINES  $file >> $MD
	echo -e "\n" >> $MD	
done

echo "</TargetArchitecture>" >> $MD

