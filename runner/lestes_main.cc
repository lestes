/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/backend_v2/backend.g.hh>
#include <lestes/lang/cplus/lex/file_system.hh>
#include <lestes/lang/cplus/lex/preprocessor.hh>
#include <lestes/lang/cplus/sem/sa_statements.g.hh>
#include <lestes/lang/cplus/sem/ss_misc.g.hh>
#include <lestes/lang/cplus/sem/ss_ss2pi_base.hh>
#include <lestes/lang/cplus/syn/dump_at_lassert.g.hh>
#include <lestes/lang/cplus/syn/hinter.hh>
#include <lestes/lang/cplus/syn/manager.hh>
#include <lestes/lang/cplus/syn/parse_result.hh>
#include <lestes/lang/cplus/syn/parser.hh>
#include <lestes/lang/cplus/syn/prefixer.hh>
#include <lestes/msg/logger.hh>
#include <lestes/msg/report_end.hh>
#include <lestes/msg/report_error_flag.hh>
#include <lestes/msg/report_origin_filter.hh>
#include <lestes/msg/report_ostream.hh>
#include <lestes/msg/reporter.hh>
#include <lestes/package.hh>
#include <lestes/std/action.g.hh>
#include <lestes/std/data_types.hh>
#include <lestes/std/dumper.hh>
#include <vector>
#include <iostream>
#include <fstream>
#include <cstdlib>

using namespace ::lestes::std;

using namespace ::lestes::lang::cplus::sem;
using namespace ::lestes::lang::cplus::syn;

using ::lestes::std::lstring;

struct usage_and_exit {
	int retcode;
	usage_and_exit() : retcode(EXIT_FAILURE)
	{}
	usage_and_exit( int a_retcode ) : retcode(a_retcode)
	{}
};

void operator << ( ::std::ostream & os, const usage_and_exit & ex )
{
	os << "\n\n"
		"Usage: lestes [options] -\n"
		"       lestes [options] <input_file>\n"
		"\n"
		"Options:\n"
		"  -I <include directory>\n"
		"       Add specified directory to the search path.\n"
		"  -a, --as <filename>\n"
		"       Dump AS structures to given file; only when parser succeeded.\n"
		"  -A, --as-deep <filename>\n"
		"       Same as --as, but the dump is in \"human readable\" format.\n"
		"  -s, --ss <filename>\n"
		"       Dump SS structures to given file; also when assertion failed.\n"
		"  -c, --ctx <filename>\n"
		"       Dump SA context to given file; also when assertion failed.\n"
		"  -l, --llc <filename>\n"
		"       Load logger configuration from given file.\n"
		"  --llc-skeleton <filename>\n"
		"       Dump skeleton of logger configuration to given file.\n"
		"  -o, --output <filename>\n"
		"       Direct assembler output to given file.\n"
		"	If not specified, standard output is used.\n"
		"  --stop-after-ss\n"
		"	Stop processing when SS tables are filled, do not invoke backend.\n"
		"  --backend-opts <bitfield>\n"
		"       Turn a part of the backend on/off by setting corresponding field to 1/0.\n"
		"  		1. Conditional jump rewriter\n"
		"  		2. Immediate inliner\n"
		"  		3. Memory inliner\n"
		"  		4. Dead-code eliminator\n"
		"  		5. Register allocator\n"
		"  		6. Useless-code eliminator\n"
		"  		7. Code scheduler\n"
		"  		Default value is 1111111\n"
		"  		Note: This option is for backend testing only. Some bit combinations may cause an output code to be invalid asm-code.\n"
		"  -, --stdin\n"
		"	Read source from standard input.\n"
		"	When using \"-\", it must be the last argument.\n"
		"  -h, --help\n"
		"	Print this help message and exit.\n"
		"\n"
		<< ::std::endl;
	return exit( ex.retcode );
}

void multiple( bool & b, lstring option )
{
	if (b)
		::std::cerr << "Error: " << option << " option used multiple times." << usage_and_exit();
	else
		b = true;
}

void missing( lstring option )
{
	::std::cerr << "Argument to " << option << " option is missing." << usage_and_exit();
}

void nonempty( lstring arg, lstring option )
{
	if (arg.empty())
		::std::cerr << "Argument to " << option <<
			" option cannot be empty." << usage_and_exit();
}

void set_backend_opts(ptr<lestes::backend_v2::backend> bend, lstring opts) {
	const char off = '0';
	
	if ( opts[0]==off ) {
		bend->optimization_conditional_jumps_set(false);
	}
	
	if ( opts[1]==off ) {
		bend->optimization_immediate_inlining_set(false);
	}
	
	if ( opts[2]==off ) {
		bend->optimization_memory_inlining_set(false);
	}
	
	if ( opts[3]==off ) {
		bend->optimization_deadcode_elimination_set(false);
	}
	
	if ( opts[4]==off ) {
		bend->optimization_register_allocation_set(false);
	}
	
	if ( opts[5]==off ) {
		bend->optimization_uselesscode_elimination_set(false);
	}
	
	if ( opts[6]==off ) {
		bend->optimization_code_scheduling_set(false);
	}
}

int main( int argc, char * argv[] )
{
	ptr< ::lestes::lang::cplus::lex::file_system > fs =
		::lestes::lang::cplus::lex::file_system::create();

	::std::vector<lstring> args( argv, argv+argc );
	::std::vector<lstring>::iterator it = args.begin();
	++it;
	lstring as_filename = "";
	lstring as_deep_filename = "";
	lstring ss_filename = "";
	lstring context_filename = "";
	lstring output_filename = "";
	lstring input_filename = "";
	bool log_finish = false;
	bool as_seen = false;
	bool as_deep_seen = false;
	bool ss_seen = false;
	bool ctx_seen = false;
	bool llc_seen = false;
	bool o_seen = false;
	bool stdin_seen = false;
	bool stop_seen = false;
	bool pragma_seen = false;

	lstring bopts = "1111111";
	
	while (it != args.end() && it->length() > 1 && (*it)[0] == '-') {
	//while (it != args.end() && (*it)[0] == '-') {

		lstring curr_opt = *it++;

		// option found
		if (curr_opt == "-I") {
			if (it == args.end())
				missing(curr_opt);
			lstring dir = *it++;
			nonempty( dir, curr_opt );
			/*
			// no longer needed; the preprocessor handles this itself:
			if (dir[dir.length()-1] != '/')
				dir += '/';
			*/
			fs->add_search_path( dir );
		} else if (curr_opt == "-a" || curr_opt == "--as") {
			multiple( as_seen, curr_opt );
			if (it == args.end())
				missing(curr_opt);
			as_filename = *it++;
			nonempty( as_filename, curr_opt );
		} else if (curr_opt == "-A" || curr_opt == "--as-deep") {
			multiple( as_deep_seen, curr_opt );
			if (it == args.end())
				missing(curr_opt);
			as_deep_filename = *it++;
			nonempty( as_deep_filename, curr_opt );
		} else if (curr_opt == "-s" || curr_opt == "--ss") {
			multiple( ss_seen, curr_opt );
			if (it == args.end())
				missing(curr_opt);
			ss_filename = *it++;
			nonempty( ss_filename, curr_opt );
		} else if (curr_opt == "-c" || curr_opt == "-ctx") {
			multiple( ctx_seen, curr_opt );
			if (it == args.end())
				missing(curr_opt);
			context_filename = *it++;
			nonempty( context_filename, curr_opt );
		} else if (curr_opt == "-l" || curr_opt == "--llc") {
			multiple( llc_seen, curr_opt );
			if (it == args.end())
				missing(curr_opt);
			lstring fn = *it++;
			nonempty( fn, curr_opt );
			log_finish = ::lestes::msg::logger::init(fn);
			if (!log_finish)
				::std::cerr << "Error, loggers will be off." << ::std::endl;
		} else if (curr_opt == "--llc-skeleton") {
			// can be used multiple times
			if (it == args.end())
				missing(curr_opt);
			lstring fn = *it++;
			nonempty( fn, curr_opt );
			::std::ofstream of( fn.c_str() );
			::lestes::msg::logger::dump_skeleton(of);
			of.close();
		} else if (curr_opt == "-o" || curr_opt == "--output") {
			multiple( o_seen, curr_opt );
			if (it == args.end())
				missing(curr_opt);
			output_filename = *it++;
			nonempty( output_filename, curr_opt );
		} else if (curr_opt == "--stop-after-ss") {
			stop_seen = true;
		} else if (curr_opt == "--stdin") {
			multiple( stdin_seen, curr_opt );
		} else if (curr_opt == "-h" || curr_opt == "--help") {
			::std::cout << "Lestes C~~ compiler version 0.0.1" << usage_and_exit(EXIT_SUCCESS);
		} else if ( curr_opt == "--backend-opts") {
			bopts = *it++;
			
			if ( bopts.length()!=7 ) {
				::std::cerr << "Bad argument to " << curr_opt << " option." << usage_and_exit();
			}
			
			for(ulint i=0; i<bopts.length(); ++i) {
				char c = bopts[i];
				if ( c!='0' && c!='1' ) {
					::std::cerr << "Bad argument to " << curr_opt << " option." << usage_and_exit();
				}
			}
		} else
			::std::cerr << "Unknown option '" << curr_opt << "'." << usage_and_exit();
	}
	if (it != args.end()) {
		if (stdin_seen)
			::std::cerr << "Both input file and --stdin specified." << usage_and_exit();
		input_filename = *it++;
		if (input_filename.empty())
			::std::cerr << "Input filename cannot be empty." << usage_and_exit();
		if (it != args.end())
			::std::cerr << "Extra arguments after input file \"" <<
				input_filename << "\"." << usage_and_exit();
		// if we are supposed to read stdin,
		//   the file argument to the preprocessor must be empty
		if (input_filename == "-")
			input_filename = "";
	} else {
		// no input file specified
		if (!stdin_seen)
			::std::cerr << "No input file (nor standard input) specified." << usage_and_exit();
	}

	/* error reporter init */
	ptr< ::lestes::msg::report_error_flag > flag =
		::lestes::msg::report_error_flag::create( 
			::lestes::msg::report_ostream::create(
				::lestes::msg::report_end::create(),
				ostream_wrapper::create(&::std::cerr,false)
			)
		);
	::lestes::msg::reporter::instance()->filter_set(
			::lestes::msg::report_origin_filter::create(flag)
		);

	/* lex init */
	ptr<prefixer::preprocessor> pp =
		prefixer::preprocessor::create( fs, input_filename );

	/* syn init */
	prefixer::init( prefixer::PREFIX_OFF, pp );
	manager::init();
	hinter::init( hinter::HINTER_NORMAL );
	parser::init();

	/* sa_statements init */
	sa_statements::instance();

	/* dump at lassert time init */
	lassert_event->attach( dump_at_lassert::create(ss_filename) );
	lassert_event->attach( dump_context_at_lassert::create(context_filename) );

	ptr<parse_result_type> parse_result = parser::parse();
	bool success = parse_result->success_get();;
	if (success)
		success = !flag->error_get();

	if (!pragma_seen)
		pragma_seen = pp->pragma_flag_get();

	ptr< ::lestes::intercode::as_base > as_result = parse_result->as_result_get();
	if (as_filename != "" && as_result) {
		::std::ofstream f(as_filename.c_str());
		::lestes::std::dumper::dump( f, as_result );
		f.close();
	}
	if (as_deep_filename != "" && as_result) {
		::std::ofstream f(as_deep_filename.c_str());
		::lestes::std::readable_dumper::dump( f, as_result );
		f.close();
	}

	if (ss_filename != "") {
		::std::ofstream f(ss_filename.c_str());
		::lestes::std::dumper::dump(f, ss_decl_seq::root_instance());
		f.close();
	}

	if (context_filename != "") {
		::std::ofstream f(context_filename.c_str());
		::lestes::std::dumper::dump(f, sa_context_manager::instance());
		f.close();
	}

	if (success && !stop_seen && !pragma_seen) {
		ss2pi_start(ss_translation_unit::create(ss_decl_seq::root_instance()));
		
		ptr<lestes::backend_v2::backend> bend;
		
		if (o_seen) {
			::std::ofstream ofs(output_filename.c_str());
			bend = lestes::backend_v2::backend::create(ofs);
			set_backend_opts(bend,bopts);
			bend->main();
			ofs.close();
		} else {
			bend = lestes::backend_v2::backend::create(::std::cout);
			set_backend_opts(bend,bopts);
			bend->main();
		}
	}

	if (log_finish)
		::lestes::msg::logger::finish();
	return success ? EXIT_SUCCESS : EXIT_FAILURE;
}


