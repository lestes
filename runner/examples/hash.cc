/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/* 
    The hash algorithm used in the UNIX ELF format for object files.
    The input is a pointer to a string to be hashed 
    
    It demonstrates:
	- aritmetics
 
*/

#include <lio.hh>

#define HASHSIZE 997

unsigned long elf_hash( char *name)
{
	unsigned long   h = 0, g;

	while (*name)
	{
		h = ( h << 4 ) + *name++;
		if (g = h & 4026531840)
			h ^= g >> 24;

		h &= ~g;
	}
	return h % HASHSIZE;
}


int main() {
    char* s = string_alloc(100);
	
    print("Enter a string to be hashed:");
	
    s = get_string_from_stdin(s,99);
    
    print(elf_hash(s));
    
    return 0;
}

