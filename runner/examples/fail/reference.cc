/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*
 * Example demonstrating as->ss expressions transformation and overload resolution, specifically
 * for the reference binding.
 *
 * This example demonstrates that the overload resolution will correctly fail in the case that one of
 * candidate functions requires 'exact' conversion for parameter (the 'int z' one) and the second one
 * requires direct reference binding, which has also exact rank.
 *
 */

int a;
int *p;

int f(int z)
{
	return a;
}

int f(int &z) 
{
	return a;
}

int main(int argc, char *argv[])
{ 
	f(a);
	// this is the second way how to crash this test program. It demonstrates
	// that unary & can't have rvalue as it's argument.
	p = &(a+a);	
}

