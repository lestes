/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*
    Computes first N digits of pi.
    
    It demonstrates:
    - pointer aritmetics
    - regular aritmetics
    - pointer dereference
    
*/
#include <lio.hh>

#define N 10000
#define LEN 10*N/3
#define MAKE_PTR(a) (a + 0)

int* tmp;

int main()
{
	int i, j, k, q, x, nines, predigit;
	int* a  = int_array_alloc(LEN);
	
	int tv;
	
	j = 0;
	while(j<LEN) {
	     tmp = a + MAKE_PTR(j);
	     *tmp=2;
	     j++;
	}
	
	nines=0; 
	predigit=0;
	j = 0;
	while(j<N) {
		q=0;
		
		i = LEN;
		while(i) {
			tmp = a + (i - 1); 
			
			tv = *tmp;
			x = 10*tv + q * i;
			
			*tmp = x % (2*i-1);
			q = x / (2*i-1);
			i--;
		}
		
		*a = q % 10; 
		q = q / 10;
		
		if (q==9) { 
		    nines++;
		}
		else if (q==10) { 
		    print1(predigit+1);
		    predigit=0;
		    
		    while(nines){
			 print1(0);
			 nines--;
		    }
		}
		else { 
		    print1(predigit); 
		    predigit = q;
		    
		    while(nines) {
			 print1(9);
			 nines--;
		    }
		};
		
		j++;
	};
	print1( predigit );
	
	print("");
}

