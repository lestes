/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*
 * Example demonstrating as->ss expressions transformation and overload resolution, specifically
 * for the comma operator.
 * 
 * As specified in [13.3] and [13.6], there are no builtin candidates for operator comma. So if
 * lookup fails for user-defined operator comma, the implicit one (for every combination of types of 
 * the expressions in question) must take place. 
 *
 * This example demonstrates that even when the lookup and overload resolution process fails for this
 * specific operator, the implicit one is used and the structures are built as required.
 *
 * In the dump of the structures, the important things to note are the connections between sequence
 * points (comma introduces new sequence point, one level deeper than what the maximum of levels of the
 * sps in question have) and also the sideeffect of the assign operator, which must not be forgotten 
 * when handling operator comma (generally, when handling comma expression, the left expression is thrown
 * away completely, up to sideeffects, which are connected to the sequence points in question to be evaluated
 * later). This implies that the expressions are evaluated in the correct order.
 *
 */

//#include <lio.hh>

int a=3,b=2;

int main(int argc, char *argv[])
{ 
	a = a++, b--;
	a = a--, b--;
	a = a+=a, b+=b;
	/* This returns 6 */
	return a;
}

