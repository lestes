/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*
 * Example demonstrating as->ss expressions transformation and overload resolution.
 * 
 * This particular example demonstrates that simple class declarations work and also
 * that overload resolution is able to choose the best viable function when function
 * parameters are classes.
 *
 * This example won't compile to the assembler source, as the backend is not ready for
 * classes yet. But instructing lestes binary to give dump of Semantic Structures and 
 * inspecting these structures proves that the function is chosen correctly by the
 * overload resolution
 */

#include <lio.hh>

/* this instructs the compiler to stop before backend */
#pragma lestes

/* == class hierarchy == */
class A {
	int i;
};

class B : public A {
	int o;
};

class C: public B {
	int p;
};

class D: public C {
	int q;
};

/* == end of class hierarchy == */

A a;
C c;
D d;
int p;
char s;
float t;

signed int f(A a)
{
	return 1;
}

unsigned int f(B b)
{
	return 2;
}

unsigned short f(D d)
{
	return 3;
}

unsigned short f(D d, int i)
{
	return 4;
}

unsigned short f(D d, long i)
{
	return 5;
}

int main()
{ 
	/* this calls unsigned int f(B b); */
	f(c); 

	/* this calls signed int f(A a); */

	/* this calls signed int f(A a); */
	f(a); 

	/* this calls unsigned short f(D d, int i) */
	f(d, p); 

	/* this also calls unsigned short f(D d, int i), because for char -> int, integral promotion is used,
	 * which has better rank than integral conversion int -> long
	 */
	f(d, s); 

	/* this is ambiguous - in both cases floating-integral conversion has to be used. */
	/* uncomment if you want to see compilation failing */
	
	//f(d, t); 
}

