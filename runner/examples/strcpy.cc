/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*
 * This is simple working strcpy(). It demonstrates correct semantics of pointer
 * dereference, increment and assignment.
 *
 * Also working dynamic initialization (by calling string_alloc() and string_duplicate()
 * in initialization) is shown.
 *
 * The strings have to be allocated in this way, as the qualification conversions as specified
 * in chapter [4] of the norm (as documented) are not fully implemented yet, so the 
 * pointer(const(char)) -> pointer(char) doesn't work yet, so the char* can't be initialized
 * directly.
 */ 

#include <lio.hh>

unsigned int xstrcpy( char * dst, char * src )
{
	int result = 0;
	while (*dst++ = *src++)
		++result;
	return result;
}

int main()
{
	char * s = string_alloc( 100 );
	char * x = string_duplicate( "Foo bar" );
	unsigned int len = xstrcpy(s, x);
	print(s);
	print(len);
	return 0;
}

