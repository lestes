/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lio.hh>

char* answer; 

int get_answer(int a, int b ,int c, int d) {
	int i_answer;
	bool stop = false;
	
	while(!stop) {
		stop = true;
		
		get_string_from_stdin(answer,99);

		i_answer = *answer - '0';
	
	
		if ( i_answer > 4 ) {
			print("Enter number 1-4.");
			stop = false;
			
		}
		
		if ( i_answer < 1 ) {
			print("Enter number 1-4.");
			stop = false;
			
		}
	}
	
	if ( i_answer== 1 ) {
		return a;
	} else if ( i_answer==2 ) {
		return b;
		
	} else if ( i_answer==3 ) {
		return c;
		
	} else if ( i_answer==4 ) {
		return d;
	}

}

int main() {
	int i_answer, score = 0;
		
	
	answer = string_alloc(100);
   
	print("If you are under 18, please leave this game by pressing n, y otherwise.");
	
	get_string_from_stdin(answer,99);
	if ( *answer != 'y' ) {
		print("Go to bed, sucker!");
		return 0;
	}
	
	print("\nDoes your sex life put a mile-wide smile on your face? Or is there something seriously lacking in the duvet department? Take our intimate quiz to find out whether you're sexually satisfied - and discover how to make your love life orgasmic.\n");
	
	print("A.What kind of situation is most conducive to sex?\n\t1.None in particular. I just get a feeling and that's when we get it on.\n\t2.Any time, any place. There's nothing more tedious than a once-a-week-only routine.\n\t3.When you're both relaxed. Usually late evening or when you've just woken up.\n\t4.Weekend nights.");
   
	i_answer += get_answer(4,1,2,3);

	print("\nWhen you're in a relationship, how often do you tend to have sex?\n\t1.Around once a week.\n\t2.Less than once a week.\n\t3.At least five times a week.\n\t4.Two to four times a week.");

	i_answer += get_answer(3,4,1,2);

	print("\nHow experimental are you when it comes to sexual positions?\n\t1.We stick to three or four favourites that rock our world.\n\t2.If I'm feeling really energetic, I might hop on top.\n\t3.It's the missionary way or no way.\n\t4.The trickier the better - every bit of furniture has been put to good use.");

	i_answer += get_answer(2,3,4,1);

	print("\nDiscussing sex with your partner is:\n\t1.Essential - how else will he know what you want?\n\t2.Fun and helpful - a saucy exchange of ideas.\n\t3.Something you should probably do - if only it didn't feel so contrived.\n\t4.Worrying - you'd rather bury your head under the pillow.");

	i_answer += get_answer(3,4,1,2);

	print("\nHow important is it for you to have an orgasm every time?\n\t1.You never or rarely orgasm.\n\t2.Essential. If he climaxes first, he'd better get that tongue busy.\n\t3.Pretty important, though occasionally you're too shattered to have an 'O'.\n\t4.You'd love to, of course, but you're climaxing less often these days.");

	i_answer += get_answer(2,3,4,1);

	print("\nSaucy underwear: sex enhancer or tacky turn-off?\n\t1.I'd wear something my partner bought me, but I'd feel a bit silly.\n\t2.I'd be too embarrassed to wear it.\n\t3.My drawers are a feast of lacy delights.\n\t4.It's fun for occasional fantasy play.");

	i_answer += get_answer(4,1,2,3);

	print("\nHave you ever used a vibrator?\n\t1.It's like a member of the family.\n\t2.Yes. It's been part of my sex life from time to time.\n\t3.Occasionally, though I wouldn't tell my partner.\n\t4.Wash your mouth out!");

	i_answer += get_answer(2,3,4,1);

	print("\nHow do you feel when girlfriends discuss juicy details of their sex lives?\n\t1.Embarrassed - you wish they wouldn't be so explicit\n\t2.They hardly get a chance - everyone's too busy listening to your exploits.\n\t3.It's a great time to share secrets without flapping male ears listening in.\n\t4.Jealous - their sexual antics are much raunchier than yours");

	i_answer += get_answer(4,1,2,3);
	
	print("\nJust as things are getting interesting, your partner loses his erection. Do you?\n\t1.Feel secretly pleased that you can go to sleep?\n\t2.Assume he's gone off you or is having an affair?\n\t3.Give it a slap and yell, 'Come on! I haven't got all night!\n\t4.Feel disappointed but cuddle and stroke him instead?");
	
	i_answer += get_answer(3,4,1,2);

	print("\nAfter sex, you tend to?\n\t1.Hug and kiss - you can't get enough of each other.\n\t2.Resolve to be more adventurous next time.\n\t3.Feel glad it's over and done with for the next couple of weeks.\n\t4.Lie awake, planning what sexual tricks to try next.");
	
	i_answer += get_answer(1,2,3,4);


	print("\nResult:\n");
	
	if ( i_answer <= 20  ) {
		print("You have a so-so sex life.\n\nYour sex life has fallen into a familiar pattern, and it's been heading this way for so long that you've forgotten that lovemaking can be heaps more satisfying. It's not that you're prissy or can't enjoy sex. Real life is to blame here: your job, niggling chores and everyday stresses. Sex can feel like something else to add to your 'to do' list. And it shows.\n\nWhat's hot: the fact that deep down in your sexual psyche, there's a wild woman waiting to get out. To revive her, spend time getting to know you and your partner's 'O'-zones all over again.\n\nWhat's not: your lazy streak. It's less effort, sometimes, for bedtime to mean sleep time.");
	} else {
		print("You're totally satisfied.\n\nLucky you. Enviably relaxed about sex, you know you and your partner's hot spots intimately. This doesn't mean sex is run-of-the-mill. It's warm and intimate and rarely routine - simply because you communicate easily and freely. Okay, you may rely on a repertoire of favourite sexual shenanigans, but they're what turn you on. And if it ain't broke, don't fix it.\nWhat's hot: your fun and playful approach to sex. Your ability to express what you want - and willingness to listen.\n What's not: sorry, but you're simply sizzling all over.");
	}; 

	return 0;
}
