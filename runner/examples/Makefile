#
# The lestes compiler suite
# Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
# Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
# Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
# Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
# Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
# Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
# Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# See the full text of the GNU General Public License version 2, and
# the limitations in the file doc/LICENSE.
#
# By accepting the license the licensee waives any and all claims
# against the copyright holder(s) related in whole or in part to the
# work, its use, and/or the inability to use it.
#
sinclude Makefile.local

MAKEFLAGS += --no-builtin-variables --no-builtin-rules

CXX=../lestes
CXXFLAGS=-I ../../lib/ $(LOGCFG) --backend-opts 1111111

LESTES_SPITS_GAS =$(shell echo 'void main(void) {}' | $(CXX) - | grep 'push %ebp' | wc -l)

ifneq ($(LESTES_SPITS_GAS), 1)
	ASM = nasm
	ASMFLAGS = -g -f elf
else
	ASM = as
	ASMFLAGS =
endif

LD=g++

LDLIBS=../../lib/lio.o

TESTS= \
	quick_sort \
	pi \
	big_factorial \
	arit \
	sex \
	strcpy \
	game \
	comma \
	overload \
	ptr \
	reference \
	hash \
	# end of TESTS

all: $(TESTS)

%:%.cc

%.o:%.cc

.PRECIOUS: %.asm %.o

%.asm:%.cc
	$(CXX) $(CXXFLAGS) -o $@ $<

%.o:%.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

%:%.o
	$(LD) $(LDFLAGS) -o $@ $< $(LDLIBS)

clean:
	@rm -f $(TESTS) *.asm *.o dumb.xml

.PHONY: all
