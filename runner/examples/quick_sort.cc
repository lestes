#include <lio.hh>

//#define N 50000
#define N 50
#define MAKE_PTR(a) (a + 0)

void print_array(int* arr,int start,int end) {
	int i = start;
	
	while (i <= end) {
		int* helper = arr + MAKE_PTR(i);
		print1(*helper);
		print1(",");
		++i;
	}
}

void qsort(int* arr, int beg, int end) 
{
	int temp;
	
	if (end > beg) {
		int* helper1 = arr + MAKE_PTR(beg);
		
		int piv = *helper1; 
		int l = beg + 1;
		int r = end;
		
		while (l < r) {
			int* helper2 = arr + MAKE_PTR(l);
			int* helper3 = arr + MAKE_PTR(r);
			
			if (*helper2 <= piv) 
				l++;
			else if(*helper3 >= piv)
				r--;
			else {
				temp = *helper2;
				*helper2 = *helper3;
				*helper3 = temp;
			}
		}
		
		int* helper4 = arr + MAKE_PTR(l);
		
		
		if(*helper4 < piv) {
			temp = *helper4;
			*helper4 = *helper1;
			*helper1 = temp;
			l--;
		} else {
			l--;
			int* helper5 = arr + MAKE_PTR(l);
			temp = *helper5;
			*helper5 = *helper1;
			*helper1 = temp;
		}
	
	  	qsort(arr, beg, l);
		qsort(arr, r, end);
	}
	
}



int main() {
	int* a  = int_array_alloc(N);

	int i = N-1;
	
	while (i>=0 ) {
		int* helper = a + MAKE_PTR(i);
		*helper = N-i;
		--i;
	}
	
	
	print ("Unsorted array:");
	print_array(a,0,N-1);	
	print("");
	
	
	qsort(a,0,N-1);
	
	
	print ("Sorted array:");
	print_array(a,0,N-1);	
	print("");
	
}

