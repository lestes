#ifndef lestes__md__common_hh__included
#define lestes__md__common_hh__included

#include <lestes/common.hh>
#include <lestes/std/dumper.hh>
#include <lestes/msg/logger.hh>
#include <lestes/msg/logger_util.hh>
#include <lestes/std/reflect.hh>
#include <fstream>
#include <iostream>
#include <sstream>

package(lestes);
package(md);

//Root logger for md
declare_logger(md_logger);

/*!
	\brief Replaces subtring in a string.
*/
lstring string_replace(lstring str, lstring replaced_string, lstring replacement);

end_package(md);
end_package(lestes);

#endif 
