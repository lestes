/*!
	\file
	\brief Name mangler.
	\author jaz
*/

#include <lestes/md/symbols/name_mangler.g.hh>
#include <lestes/lang/cplus/sem/ss_decl2lstring_base.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>
#include <lestes/lang/cplus/sem/ss_decl_name.g.hh>
#include <lestes/lang/cplus/sem/ss_decl_name2lstring_base.g.hh>
#include <iostream>
#include <sstream>

package(lestes);
package(md);
package(symbols);

/*

A grammar used in GNU gcc in name mangling process

------------
   Check whether a substitution should be used to represent NODE in
   the mangling.

   First, check standard special-case substitutions.

*** <substitution> ::= St     # ::std

                    ::= Sa   # ::std::allocator

                    ::= Sb   # ::std::basic_string

                    ::= Ss   # ::std::basic_string<char, ::std::char_traits<char>, ::std::allocator<char> >

                    ::= Si   # ::std::basic_istream<char, ::std::char_traits<char> >

                    ::= So   # ::std::basic_ostream<char, ::std::char_traits<char> >

                    ::= Sd   # ::std::basic_iostream<char, ::std::char_traits<char> >   

   Then examine the stack of currently available substitution
   candidates for entities appearing earlier in the same mangling

   If a substitution is found, write its mangled representation and
   return nonzero.  If none is found, just return zero.
-------------
   
*** <mangled-name>   ::= _Z <encoding>  

-------------

*** <encoding>		::= <function name> <bare-function-type>
					::= <data name> 
-------------
			
*** <name> 			::= <unscoped-name>
          			::= <unscoped-template-name> <template-args>
	  				::= <nested-name>
	  				::= <local-name>  

   If IGNORE_LOCAL_SCOPE is nonzero, this production of <name> is
   called from <local-name>, which mangles the enclosing scope
   elsewhere and then uses this function to mangle just the part
   underneath the function scope.  So don't use the <local-name>
   production, to avoid an infinite recursion.  
-------------
   
*** <unscoped-name> ::= <unqualified-name>
                   	::= St <unqualified-name>   # ::std::  
-------------

*** <unscoped-template-name> 	::= <unscoped-name>
                            	::= <substitution>  
------------
			 
  Write the nested name, including CV-qualifiers, of DECL.

***  <nested-name> 	::= N [<CV-qualifiers>] <prefix> <unqualified-name> E  
                 	::= N [<CV-qualifiers>] <template-prefix> <template-args> E

***   <CV-qualifiers> ::= [r] [V] [K] 
------------
   
*** <prefix> 	::= <prefix> <unqualified-name>
            	::= <template-param>
            	::= <template-prefix> <template-args>
	    		::= # empty
	    		::= <substitution> 
------------

*** <template-prefix> 	::= <prefix> <template component>
                     	::= <template-param>
                     	::= <substitution>  
------------

   We don't need to handle thunks, vtables, or VTTs here.  Those are
   mangled through special entry points.  

*** <unqualified-name>  ::= <operator-name>
						::= <special-name>  
						::= <source-name>
						
____________
			
  Non-termial <source-name>.  IDENTIFIER is an IDENTIFIER_NODE.  

***  <source-name> ::= </length/ number> <identifier> 

------------
     
  Non-terminal <number>.

***  <number> ::= [n] </decimal integer/> 

------------
     
  Non-terminal <identifier>.

     <identifier> ::= </unqualified source code identifier> 
------------

  Handle constructor productions of non-terminal <special-name>.
   CTOR is a constructor FUNCTION_DECL. 

***  <special-name> ::= C1   # complete object constructor
                    ::= C2   # base object constructor
                    ::= C3   # complete object allocating constructor

   Currently, allocating constructors are never used. 

   We also need to provide mangled names for the maybe-in-charge
   constructor, so we treat it here too.  mangle_decl_string will
   append *INTERNAL* to that, to make sure we never emit it. 
   
------------  
   
   Handle destructor productions of non-terminal <special-name>.
   DTOR is a destructor FUNCTION_DECL. 

***  <special-name> ::= D0 # deleting (in-charge) destructor
                    ::= D1 # complete object (in-charge) destructor
                    ::= D2 # base object (not-in-charge) destructor

   We also need to provide mangled names for the maybe-incharge
   destructor, so we treat it here too.  mangle_decl_string will
   append *INTERNAL* to that, to make sure we never emit it.  
   
--------------

***   <discriminator> := _ <number>   

   The discriminator is used only for the second and later occurrences
   of the same name within a single function. In this case <number> is
   n - 2, if this is the nth occurrence, in lexical order.  
   
---------------
   
   Mangle the name of a function-scope entity.  FUNCTION is the
   FUNCTION_DECL for the enclosing function.  ENTITY is the decl for
   the entity itself.  LOCAL_ENTITY is the entity that's directly
   scoped in FUNCTION_DECL, either ENTITY itself or an enclosing scope
   of ENTITY.

***  <local-name> := Z <function encoding> E <entity name> [<discriminator>]
                  := Z <function encoding> E s [<discriminator>]  
				  
-------------

 Non-terminals <type> and <CV-qualifier>.  

***  <type> ::= <builtin-type>
            ::= <function-type>
            ::= <class-enum-type>
            ::= <array-type>
            ::= <pointer-to-member-type>
            ::= <template-param>
            ::= <substitution>
            ::= <CV-qualifier>
            ::= P <type>    # pointer-to
            ::= R <type>    # reference-to
            ::= C <type>    # complex pair (C 2000)
            ::= G <type>    # imaginary (C 2000)     [not supported]
            ::= U <source-name> <type>   # vendor extended type qualifier 

   TYPE is a type node.  
   
---------------
   
   Non-terminal <CV-qualifiers> for type nodes.  Returns the number of
   CV-qualifiers written for TYPE.

*** <CV-qualifiers> ::= [r] [V] [K]  
	 
---------------
     
  Non-terminal <builtin-type>. 

***  <builtin-type> ::= v   # void 
                    ::= b   # bool
                    ::= w   # wchar_t
                    ::= c   # char
                    ::= a   # signed char
                    ::= h   # unsigned char
                    ::= s   # short
                    ::= t   # unsigned short
                    ::= i   # int
                    ::= j   # unsigned int
                    ::= l   # long
                    ::= m   # unsigned long
                    ::= x   # long long, __int64
                    ::= y   # unsigned long long, __int64  
                    ::= n   # __int128
                    ::= o   # unsigned __int128
                    ::= f   # float
                    ::= d   # double
                    ::= e   # long double, __float80 
                    ::= g   # __float128          [not supported]
                    ::= u <source-name>  # vendor extended type 
---------------
   Non-terminal <function-type>.  NODE is a FUNCTION_TYPE or
   METHOD_TYPE.  The return type is mangled before the parameter
   types.

*** <function-type> ::= F [Y] <bare-function-type> E  

--------------     
   Non-terminal <bare-function-type>.  TYPE is a FUNCTION_TYPE or
   METHOD_TYPE.  If INCLUDE_RETURN_TYPE is nonzero, the return value
   is mangled before the parameter types.  If non-NULL, DECL is
   FUNCTION_DECL for the function whose type is being emitted.

***  <bare-function-type> ::= </signature/ type>+  

-------------

*** <class-enum-type> ::= <name>  

-------------

  Non-terminal <template-args>.  ARGS is a TREE_VEC of template
   arguments.

*** <template-args> ::= I <template-arg>+ E  

-------------
*** <expression> 	::= <unary operator-name> <expression>
					::= <binary operator-name> <expression> <expression>
					::= <expr-primary>

***  <expr-primary> 	::= <template-param>
		  			::= L <type> <value number> E  # literal
		  			::= L <mangled-name> E         # external name  
                  	::= sr <type> <unqualified-name>
                  	::= sr <type> <unqualified-name> <template-args> 

--------------
		 
 Non-terminal <tempalate-arg>.  

***  <template-arg> ::= <type>                        # type
                    ::= L <type> </value/ number> E   # literal
                    ::= LZ <name> E                   # external name
                    ::= X <expression> E              # expression  
---------------
		    
*** <template-template-arg> ::= <name>
							::= <substitution> 

---------------
		
  Non-terminal <array-type>.  TYPE is an ARRAY_TYPE.  

     <array-type> ::= A [</dimension/ number>] _ </element/ type>  
                  ::= A <expression> _ </element/ type>

     "Array types encode the dimension (number of elements) and the
     element type. For variable length arrays, the dimension (but not
     the '_' separator) is omitted."  
	 
---------------
   Non-terminal <pointer-to-member-type> for pointer-to-member
   variables.  TYPE is a pointer-to-member POINTER_TYPE.

*** <pointer-to-member-type> ::= M </class/ type> </member/ type> 

---------------
   Non-terminal <template-param>.  PARM is a TEMPLATE_TYPE_PARM,
   TEMPLATE_TEMPLATE_PARM, BOUND_TEMPLATE_TEMPLATE_PARM or a
   TEMPLATE_PARM_INDEX.

***  <template-param> ::= T </parameter/ number> _

   If we are internally mangling then we distinguish level and, for
   non-type parms, type too. The mangling appends
   
     </level/ number> _ </non-type type/ type> _

   This is used by mangle_conv_op_name_for_type. 
   
--------------

   
*** <template-template-param>  	::= <template-param> 
								::= <substitution>  

---------------
  Non-terminal <substitution>.  

***  <substitution>	::= S <seq-id> _
                    ::= S_  
					
--------------

   Return an identifier for a construction vtable group.  TYPE is
   the most derived class in the hierarchy; BINFO is the base
   subobject for which this construction vtable group will be used.  

   This mangling isn't part of the ABI specification; in the ABI
   specification, the vtable group is dumped in the same COMDAT as the
   main vtable, and is referenced only from that vtable, so it doesn't
   need an external name.  For binary formats without COMDAT sections,
   though, we need external names for the vtable groups.  

   We use the production

*** <special-name> ::= CT <type> <offset number> _ <base type>  

-----------------
    
   Return an identifier for the mangled name of a thunk to FN_DECL.
   OFFSET is the initial adjustment to this used to find the vptr.  If
   VCALL_OFFSET is non-NULL, this is a virtual thunk, and it is the
   vtbl offset in bytes.  

***  <special-name> ::= Th <offset number> _ <base encoding>
                   	::= Tv <offset number> _ <vcall offset number> _ <base encoding>
-----------------

*/

using ::lestes::lang::cplus::sem::ss_declaration;
using ::lestes::lang::cplus::sem::ss_decl2mangled_name;
using ::lestes::lang::cplus::sem::ss_decl_name2mangled_name;

/*!
	\brief Returns a singleton instance.
*/
ptr<name_mangler> name_mangler::instance() {
	if ( !singleton_instance_get() ) {
		singleton_instance_set(name_mangler::create());
	}
	return singleton_instance_get();
}


/*!
	\brief Replaces wide character escapes after its uppercase form.
	
	\param name The string.
	\return String with repacements.
*/
lstring name_mangler::wchar_escapes_replace(lstring name)
{
	return ::lestes::md::string_replace(::lestes::md::string_replace( name,"\\u",".u"),"\\U",".U");
}


/*!
	\brief Mangles declaration.
	
	It mangles C++ declarations only. Declarations for other languages objects are not mangled - just declaration name is 
	returned.
	
	\param decl The declaration.
	\return String identification of the declaration.
*/
lstring name_mangler::mangle(ptr<ss_declaration> decl)
{
	lassert(decl);
	lstring mangled_name;
	
	if ( decl->linkage_get()->language_get()==ucn_string("C++") ) {
		/*
			 C++ object. 
			 Mangle its name according to GNU GCC mangling.
		*/
		ptr<ss_decl2mangled_name> mangler = ss_decl2mangled_name::instance();
		mangled_name = mangler->process(decl);
	} else {
		/*
			 Other language extern object. 
			 Do not mangle it and return its name instead. ( It works for C object. )
		*/
		mangled_name = decl->name_get()->accept_ss_decl_name2lstring_base(ss_decl_name2mangled_name::instance());
	}
	return wchar_escapes_replace(mangled_name);
}

end_package(symbols);
end_package(md);
end_package(lestes);

