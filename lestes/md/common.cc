
/*! \file 
	\brief Implementations of standard functions for md tree.
	\author jaz
*/

#include <lestes/md/common.hh>
#include <sstream>

package(lestes);
package(md);

initialize_top_logger(md_logger, "md" );

/*!
	Replaces substring in a string.
	
	\param str The string where replacement is performed.
	\param replaced_string The replaced string.
	\param replacement The replacement.
*/
lstring string_replace(lstring str, lstring replaced_string, lstring replacement) {
	lstring::size_type replaced_size = replaced_string.length();
	lstring::size_type i = 0;
	while( (i=str.find(replaced_string,i))!=lstring::npos) {
		str = str.replace(i, replaced_size, replacement);
	}
	return str;
}

end_package(md);
end_package(lestes);
