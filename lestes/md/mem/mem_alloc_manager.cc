/*!
	\file
	\brief Memory allocator.
	\author jaz
*/

#include <lestes/lang/cplus/sem/ss_declaration.g.hh>
#include <lestes/lang/cplus/sem/ss_type.g.hh>
#include <lestes/md/types/tm_data_type_base.g.hh>
#include <lestes/md/mem/memory_allocators.g.hh>
#include <lestes/md/literals/literal_info_base.g.hh>
#include <lestes/md/types/ss_type2tm_type.g.hh>
#include <lestes/backend_v2/intercode/pi_mem_factory.g.hh>
#include <lestes/backend_v2/intercode/pi.g.hh>
#include "mem_alloc_manager.g.hh"

package(lestes);
package(md);
package(mem);

using namespace ::lestes::backend_v2::intercode;
using namespace ::lestes::md::types;
using namespace ::lestes::md::literals;
using namespace ::lestes::md::mem;
using namespace ::lestes::lang::cplus::sem;

/*!
	Returns instance.
*/
ptr<mem_alloc_manager> mem_alloc_manager::instance() {
	if ( !singleton_instance_get() ) {
		singleton_instance_set(mem_alloc_manager::create());
	}
	return singleton_instance_get();
}

/*!
	\brief Allocates space for variable on the local stack.
	
	\param function The function where the new variable is allocated.
	\param decl Declaration of the variable.
	\return pi_mem_factory object representing memory space for variable.
*/
ptr<pi_mem_factory> mem_alloc_manager::allocate_local_var(ptr<ss_function_declaration> function, ptr<ss_object_declaration > decl){
	return local_variable_allocator::instance(function)->allocate(decl);
}

/*!
	\brief Allocates space for temporal variable on the local stack.
	
	\param function The function where the new variable is allocated.
	\param type Data type of the variable.
	\return pi_mem_factory object representing memory space for variable.
*/
ptr<pi_mem_factory> mem_alloc_manager::allocate_local_tmp(ptr<ss_function_declaration> function, ptr<ss_type > type){
	return local_variable_allocator::instance(function)->allocate_tmp(type->accept_ss_type2tm_type_gen_base(ss_type2tm_type::instance()));
}


/*!
	\brief Allocates space for temporal variable on the local stack.
	
	\param function The function where the new variable is allocated.
	\param type Data type of the variable.
	\return pi_mem_factory object representing memory space for variable.
*/
ptr<pi_mem_factory> mem_alloc_manager::allocate_local_tmp(ptr<ss_function_declaration> function, ptr<tm_data_type_base> type){
	return local_variable_allocator::instance(function)->allocate_tmp(type);
}

/*!
	\brief Deallocates previously allocated space.
	
	Deallocates previously (by one of the mem_alloc_manager's functions) allocated memory space. Time of
	dealocation is bound by given sequencepoints. 	
	\param mem The memory to be dealocated.
	\param psp The first boundary sequencepoint.
	\param nsp The second boundary sequencepoint.
*/
void mem_alloc_manager::deallocate(ptr<pi_mem_factory> mem, ptr<pi_sp> psp, ptr<pi_sp> nsp){
	if ( mem->allocator_get() )
		mem->allocator_get()->deallocate(mem, psp, nsp);
}

/*!
	\brief Allocates space for variable in global data storage.
	
	\param decl Declaration of the variable.
	\return pi_mem_factory object representing memory space for variable.
*/
ptr<pi_mem_factory> mem_alloc_manager::allocate_global_var(ptr<ss_object_declaration> decl){
	return global_variable_allocator::instance()->allocate(decl);
}

/*!
	\brief Initialises given global memory with literal.
	
	\param mem The memory.
	\param value The literal.
*/
void mem_alloc_manager::init_global_var(ptr<pi_mem_factory> mem, ptr<literal_info_base> value) {
	return global_variable_allocator::instance()->init_variable(mem, value);
}


end_package(mem);
end_package(md);
end_package(lestes);

