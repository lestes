<?xml version="1.0" encoding="UTF-8"?>
<lsd xmlns="http://lestes.jikos.cz/schemas/lsd" xmlns:h="http://www.w3.org/TR/REC-html40">

	<dox file="both">
		<bri>Memory allocator.</bri>
		<det>\author jaz</det>
	</dox>
	
    <file-name>mem_alloc_manager</file-name>
    
    <packages>
		<p>lestes</p>
		<p>md</p>
		<p>mem</p>
    </packages>
    
    <imports>
		<i>lestes/md/common.hh</i>		
    </imports>

	<implementation-imports>
		<i>lestes/lang/cplus/sem/ss_declaration.g.hh</i>
		<i>lestes/lang/cplus/sem/ss_type.g.hh</i>
		<i>lestes/backend_v2/intercode/pi.g.hh</i>
		<i>lestes/backend_v2/intercode/pi_mem_factory.g.hh</i>
		<i>lestes/md/literals/literal_info_base.g.hh</i>
		<i>lestes/md/types/tm_data_type_base.g.hh</i>
	</implementation-imports>

	<foreign-class name="object">
		<p>lestes</p>
		<p>std</p>
	</foreign-class>

	<foreign-class name="ss_function_declaration">
		<p>lestes</p>
		<p>lang</p>
		<p>cplus</p>
		<p>sem</p>
	</foreign-class>
	
	<foreign-class name="ss_object_declaration">
		<p>lestes</p>
		<p>lang</p>
		<p>cplus</p>
		<p>sem</p>
	</foreign-class>
	
	<foreign-class name="pi_abstract_function_call">
		<p>lestes</p>
		<p>backend_v2</p>
		<p>intercode</p>
	</foreign-class>
	
	<foreign-class name="pi_sp">
		<p>lestes</p>
		<p>backend_v2</p>
		<p>intercode</p>
	</foreign-class>
	
	<foreign-class name="literal_info_base">
		<p>lestes</p>
		<p>md</p>
		<p>literals</p>
	</foreign-class>
	
	<foreign-class name="pi_mem_factory">
		<p>lestes</p>
		<p>backend_v2</p>
		<p>intercode</p>
	</foreign-class>
	
	<foreign-class name="tm_data_type_base">
		<p>lestes</p>
		<p>md</p>
		<p>types</p>
	</foreign-class>
	
	<foreign-class name="ss_type">
		<p>lestes</p>
		<p>lang</p>
		<p>cplus</p>
		<p>sem</p>
	</foreign-class>
	
	
	<class name="mem_alloc_manager" base="::lestes::std::object">
		<dox>
	    	<bri>Manager for variable allocation.</bri>
	    	<det>The backend provides interface for allocating and accessing memory for variables and parameters. 
The allocation means that somebody lets the backend know about new variable, the backend returns object representing
 corresponding place in memory and at the end it generates assembler definition for this object.			
			</det>
		</dox>
		
		<method name="allocate_local_tmp" type="pi_mem_factory" specifier="virtual" >
	    	<dox>
				<bri>Allocates space for variable on the local stack.</bri>
			</dox>
			<param name="function" type="ss_function_declaration"/>
	    	<param name="decl" type="ss_type" />
		</method>
		
		<method name="allocate_local_tmp" type="pi_mem_factory" specifier="virtual" >
	    	<dox>
				<bri>Allocates space for variable on the local stack.</bri>
			</dox>
			<param name="function" type="ss_function_declaration"/>
	    	<param name="decl" type="tm_data_type_base" />
		</method>
		
		<method name="allocate_local_var" type="pi_mem_factory" specifier="virtual" >
	    	<dox>
				<bri>Allocates space for variable on the local stack.</bri>
			</dox>
			<param name="function" type="ss_function_declaration"/>
	    	<param name="decl" type="ss_object_declaration" />
		</method>
		
		<method name="allocate_global_var" type="pi_mem_factory" specifier="virtual" >
	    	<dox>
				<bri>Allocates space for variable in global data storage.</bri>
			</dox>
	    	<param name="decl" type="ss_object_declaration" />
		</method>
		
		<method name="init_global_var" type="void" specifier="virtual" >
	    	<dox>
				<bri>Initialises space in global data storage by given value.</bri>
			</dox>
	    	<param name="mem" type="pi_mem_factory" />
			<param name="value" type="literal_info_base" />
		</method>
		
		<method name="deallocate" type="void" specifier="virtual" >
	    	<dox>
				<bri>Deallocates previously allocated space.</bri>
			</dox>
	    	<param name="mem" type="pi_mem_factory" />
			<param name="psp" type="pi_sp" />
			<param name="nsp" type="pi_sp" />	
		</method>
		
		<field name="singleton_instance" type="mem_alloc_manager" specifier="static" init="NULL" >
			<dox>
				<bri>Singleton instance.</bri>
			</dox>
		</field>
		
		<method name="instance" type="mem_alloc_manager" specifier="static" >
			<dox>
				<bri>Returns singleton instance.</bri>
			</dox>
		</method>			
	</class>

</lsd>

