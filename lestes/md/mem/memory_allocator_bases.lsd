<?xml version="1.0" encoding="UTF-8"?>
<lsd xmlns="http://lestes.jikos.cz/schemas/lsd" xmlns:h="http://www.w3.org/TR/REC-html40">
	<dox file="both">
		<bri>Base classes for memory allocators.</bri>
		<det>\author jaz</det>
	</dox>

    <file-name>memory_allocator_bases</file-name>
    
    <packages>
		<p>lestes</p>
		<p>md</p>
		<p>mem</p>
    </packages>
    
    <imports>
		<i>lestes/md/common.hh</i>		
    </imports>

	<implementation-imports>
		<i>lestes/lang/cplus/sem/ss_declaration.g.hh</i>
		<i>lestes/lang/cplus/sem/ss_type.g.hh</i>
		<i>lestes/md/types/tm_data_type_base.g.hh</i>
		<i>lestes/md/literals/literal_info_base.g.hh</i>
		<i>lestes/backend_v2/intercode/pi.g.hh</i>
		<i>lestes/backend_v2/intercode/pi_mem_factory.g.hh</i>
	</implementation-imports>

	<foreign-class name="object">
		<p>lestes</p>
		<p>std</p>
	</foreign-class>

	<foreign-class name="ss_function_declaration">
		<p>lestes</p>
		<p>lang</p>
		<p>cplus</p>
		<p>sem</p>
	</foreign-class>
	
	<foreign-class name="ss_object_declaration">
		<p>lestes</p>
		<p>lang</p>
		<p>cplus</p>
		<p>sem</p>
	</foreign-class>
	
	<foreign-class name="pi_mem_factory">
		<p>lestes</p>
		<p>backend_v2</p>
		<p>intercode</p>
	</foreign-class>
	
	<foreign-class name="pi_abstract_function_call">
		<p>lestes</p>
		<p>backend_v2</p>
		<p>intercode</p>
	</foreign-class>
	
	<foreign-class name="literal_info_base">
		<p>lestes</p>
		<p>md</p>
		<p>literals</p>
	</foreign-class>
	
	<foreign-class name="pi_pi">
		<p>lestes</p>
		<p>backend_v2</p>
		<p>intercode</p>
	</foreign-class>
	
	<foreign-class name="pi_sp">
		<p>lestes</p>
		<p>backend_v2</p>
		<p>intercode</p>
	</foreign-class>
	
	<foreign-class name="tm_data_type_base">
		<p>lestes</p>
		<p>md</p>
		<p>types</p>
	</foreign-class>
	

	<class name="allocator_base" base="::lestes::std::object" abstract="yes" >
	
		<dox>
	    	<bri>Base class for memory allocators.</bri>
	    	<det>Memory allocators allows to allocate variables.</det>
		</dox>
		
		<method name="deallocate" type="void" specifier="virtual" qualifier="abstract" >
	    	<dox>
				<bri>Deallocates previously allocated space.</bri>
				<det>Dealocates space occupied by given pi_mem_factory. Point in time, when the dealocation tooks place, is
between psp and nsp sequencepoints.
					
				\param mem A memory space to be dealloacated.
				\param psp A sequencepoint that is set as psp to any generated deallocation code.
				\param nsp A sequencepoint that is set as nsp to any generated deallocation code.
				</det>
			</dox>
	    	<param name="mem" type="pi_mem_factory" />
			<param name="psp" type="pi_sp"/>
			<param name="nsp" type="pi_sp"/>
		</method>
		
		<method name="calculate_placement" type="void" specifier="virtual" qualifier="abstract">
			<dox>
				<bri>Calculates placement of allocated variables.</bri>
			</dox>
		</method>
		
	</class>
	
    <class name="local_variable_allocator_base" base="allocator_base" abstract="yes" >
		<dox>
	    	<bri>Manager for local variable allocation.</bri>
	    	<det>Note: A concrete implementation is target machine-dependent and it is placed in /target/machine/${TARGET_CPU}/lestes/md/memory/ directory.</det>
		</dox>
	
		<field name="function" type="ss_function_declaration">
			<dox>
	    		<bri>Declaration of the function in which's body the allocation is performed.</bri>
			</dox>
		</field>
	
		<method name="allocate" type="pi_mem_factory" specifier="virtual" qualifier="abstract" >
	    	<dox>
				<bri>Allocates space for variable on the local stack.</bri>
			</dox>
	    	<param name="decl" type="ss_object_declaration" />
		</method>
		
		<method name="allocate_tmp" type="pi_mem_factory" specifier="virtual" qualifier="abstract" >
	    	<dox>
				<bri>Allocates space for variable on the local stack.</bri>
			</dox>
	    	<param name="type" type="tm_data_type_base" />
		</method>
	
		<method name="deallocate" type="void" specifier="virtual" qualifier="abstract" >
	    	<dox>
				<bri>Deallocates space occupied by variable.</bri>
			</dox>
	    	<param name="mem" type="pi_mem_factory" />
			<param name="psp" type="pi_sp"/>
			<param name="nsp" type="pi_sp"/>
		</method>
		
		<method name="calculate_placement" type="void" specifier="virtual" qualifier="abstract">
			<dox>
				<bri>Calculates placement of local variables and parameters in memory.</bri>
			</dox>
		</method>
    </class>
    
    
    <class name="global_variable_allocator_base" base="allocator_base" abstract="yes">
		<dox>
	    	<bri>Manager for global variable allocation.</bri>
	    	<det>Note: A concrete implementation is target machine-dependent and it is placed in /target/machine/${TARGET_CPU}/lestes/md/memory/ directory.</det>
		</dox>
	
		<method name="allocate" type="pi_mem_factory" specifier="virtual" qualifier="abstract" >
	    	<dox>
				<bri>Allocates space for variable in global data storage.</bri>
	    	</dox>
	    	<param name="decl" type="ss_object_declaration" />
		</method>
		
		<method name="init_variable" type="void" specifier="virtual" qualifier="abstract" >
	    	<dox>
				<bri>Initialiaze memory space in global data storage with given value.</bri>
	    	</dox>
	    	<param name="mem" type="pi_mem_factory" />
			<param name="value" type="literal_info_base" />
		</method>

		<method name="deallocate" type="void" specifier="virtual" qualifier="abstract" >
	    	<dox>
				<bri><bri>Deallocates space occupied by variable.</bri></bri>
			</dox>
	    	<param name="mem" type="pi_mem_factory" />
			<param name="psp" type="pi_sp"/>
			<param name="nsp" type="pi_sp"/>
		</method>
		
		<method name="calculate_placement" type="void" specifier="virtual" qualifier="abstract">
			<dox>
				<bri>Calculates placement of local variables and parameters in memory.</bri>
			</dox>
		</method>
    </class>
    
</lsd>

