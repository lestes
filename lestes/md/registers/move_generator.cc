/*!
	\file
	\author jaz
*/

#include <lestes/backend_v2/debug/debug.hh>
#include <lestes/backend_v2/intercode/pi.g.hh>
#include <lestes/backend_v2/intercode/visitor_pi_pi2pi_operands.g.hh>
#include <lestes/backend_v2/structs/pi_operands.g.hh>
#include <lestes/md/instructions/pi_pi2ge_pi_template.g.hh>
#include <lestes/md/instructions/instruction_group.g.hh>
#include <lestes/md/instructions/tm_instr.g.hh>
#include <lestes/md/types/tm_data_type.g.hh>
#include <lestes/md/registers/move_generator.g.hh>

package(lestes);
package(md);
package(registers);

using namespace ::lestes::backend_v2::debug;
using namespace ::lestes::backend_v2::intercode;
using namespace ::lestes::msg;
using namespace ::lestes::md::instructions;
using namespace ::lestes::md::types;


typedef list< ulint > ulint_list__type;
typedef vector< ulint > ulint_vector__type;
typedef set< ulint > id_set__type;
typedef map< ulint, srp<tm_instr_op_base> > ulint2tm_instr_op_base__type;
typedef map< ulint, srp<ge_operand> > ulint2ge_operand__type;
typedef map< ulint, ulint > ulint2ulint__type;
typedef list< srp < instruction_group_base > > instruction_group_base_list__type;
typedef vector< srp < tm_instr_op_base > > tm_instr_op_base_vector__type;
typedef vector< srp < ge_operand > > ge_operand_vector__type;
typedef vector< srp < ge_pi > > ge_pi_vector__type;
typedef set< srp < ge_pi > > ge_pi_set__type;
typedef vector< srp < ge_sp > > ge_sp_vector__type;
typedef vector< srp < pi_sp > > pi_sp_vector__type;


declare_logger(log1);
initialize_logger( log1, "move_generator", md_logger );

/*!
	\brief Generates code that loads a value from a memory to a register.
	
	\param op_mem A memory operand.
	\param op_reg A pseudoregister operand.
	\param reg A target-machine register allocated to the pseudoregister operand.
	\return A load code.
*/
ptr<ge_pi_vector__type> move_generator::generate_load_from_memory(ptr<ge_operand_mem> op_mem, ptr<ge_operand_reg> op_reg, ptr<tm_register_base> reg) {
	lassert(reg);
	return generate_move(op_mem,NULL,op_reg,reg);
}

/*!
	\brief Generates code that stores a value from a register to a memory.
	
	\param op_reg A pseudoregister operand.
	\param reg A target-machine register allocated to the pseudoregister operand.
	\param op_mem A memory operand.
	\return A store code.
*/
ptr<ge_pi_vector__type> move_generator::generate_store_to_memory(ptr<ge_operand_reg> op_reg, ptr<tm_register_base> reg, ptr<ge_operand_mem> op_mem) {
	lassert(reg);
	return generate_move(op_reg,reg,op_mem,NULL);
}


/*!
	\brief Generates code that copies a value from a register to another register.
	
	\param op_reg_src A source pseudoregister operand.
	\param reg_src A target-machine register allocated to the source pseudoregister operand.
	\param op_reg_dst A destination pseudoregister operand.
	\param reg_dst A target-machine register allocated to the destination pseudoregister operand.
	\return A copy code.
*/
ptr<ge_pi_vector__type> move_generator::generate_copy_to_register(ptr<ge_operand_reg> op_reg_src, ptr<tm_register_base> reg_src, ptr<ge_operand_reg> op_reg_dst, ptr<tm_register_base> reg_dst) {
	lassert(reg_src);
	lassert(reg_dst);
	return generate_move(op_reg_src, reg_src, op_reg_dst, reg_dst);
}

/*!
	\brief Generates code that moves value from a source operand to a destination operand.
	
	One of operands has to be pseudoregister operand with a target-machine register allocated. The target-machine register contains list of templates that defines
	possibilities how to move value of the register to other storage spaces (register, memory). The function selects one matching template and generates output move code.
	
	\param input_operand An input operand.
	\param input_reg If the input operand is pseudoregister, then parameter gets register allocated to the operand. NULL otherwise.
	\param output_operand An output operand.
	\param output_reg If the output operand is pseudoregister, then parameter gets register allocated to the operand. NULL otherwise.
	\return A copy code.
*/
ptr<ge_pi_vector__type> move_generator::generate_move(ptr<ge_operand> input_op, ptr<tm_register_base> input_reg, ptr<ge_operand> output_op, ptr<tm_register_base> output_reg) {
	log1 << "generate_move - start\n" << eolog;
	lassert(input_op);
	lassert(!input_reg || input_reg->bitwidth_get()>=input_op->type_get()->bitwidth_get());
	lassert(output_op);
	lassert(!output_reg || output_reg->bitwidth_get()>=output_op->type_get()->bitwidth_get());
	
	ptr<ulint_vector__type> selected_instrs = ulint_vector__type::create();
	ptr<ulint2tm_instr_op_base__type> selected_operands = ulint2tm_instr_op_base__type::create();
	ptr<ulint2ulint__type> selected_op_types = ulint2ulint__type::create();
	
	ptr<id_set__type> move_templates = input_reg ? input_reg->move_templates_get() : output_reg->move_templates_get();
	
	for(id_set__type::iterator it = move_templates->begin(); it!=move_templates->end(); ++it) {
		ptr<pi_pi2ge_pi_template> pat = pi_pi2ge_pi_template::instance(*it);
		
		if ( find_consistent_instructions(input_op,input_reg,output_op,output_reg,pat,selected_instrs,selected_operands,selected_op_types,0) ) {
			log1 << "generate_move - end 1 ( move generated successfuly )\n" << eolog;
			return generate_ge_code(input_op,input_reg,output_op,output_reg,pat,selected_instrs,selected_op_types);
		}
	}
	
	lassert(false);
	log1 << "generate_move - end ( no consistent template found )\n" << eolog;
	return NULL;
	
}

/*!
	\brief Gets list of selected target machine instruction ids for each instruction_group and generates output ge-code.
	
	\param input_operand An input operand.
	\param input_reg If the input operand is pseudoregister, then parameter gets register allocated to the operand. NULL otherwise.
	\param output_operand An output operand.
	\param output_reg If the output operand is pseudoregister, then parameter gets register allocated to the operand. NULL otherwise.
	\param pat A list of instruction_group instances.
	\param choosed_instructions Selected instructions.
	\param  choosed_op_types Types of operands used within output code.
	\return Vector of ge-pi instructions.
*/
ptr<ge_pi_vector__type> move_generator::generate_ge_code(ptr<ge_operand> input_op, ptr<tm_register_base> input_reg, ptr<ge_operand> output_op, ptr<tm_register_base> output_reg, ptr<pi_pi2ge_pi_template_base> pat, ptr<ulint_vector__type> choosed_instrs,  ptr<ulint2ulint__type>) {
	log1 << "generate_ge_code - start\n" << eolog;
	
	ptr<ge_pi_vector__type> output = ge_pi_vector__type::create();
	ptr<ulint2ge_operand__type> tm_op2ge_op = ulint2ge_operand__type::create(); 	
	
	ptr<ge_pi> prev;
	
	/*
		Go throught the pi2ge template's instruction templates and instantiate them according to the selected versions.
	*/
	for(ulint i=0; i<pat->instruction_groups_get()->size(); ++i) {
		//The instruction template
		ptr<instruction_group_base> instr_group = (*pat->instruction_groups_get())[i];
		//The selected version.
		ptr<tm_instr_base> instr_version = tm_instr::instance((*choosed_instrs)[i]);
				
		//Output ge-code
		ptr<ge_pi> ge = ge_pi::create(instr_version,NULL);
		
		ptr<ge_operand_vector__type> input_ops = ge->operands_input_get();
		ptr<ge_operand_vector__type> output_ops = ge->operands_output_get();
		ptr<ge_pi_set__type> dependencies = ge->dependencies_get();
		
		if ( prev ) {
			/*
				Set dependence on a preceeding output ge-pseudoinstruction of the teplate in order to force the output
				code to be ordered in the same way as instruction templates in the input template.	
			*/
			
			dependencies->insert(prev);
		}
		
		//Input operands.
		for(ulint j=0; j<instr_group->input_operand_pids_get()->size(); ++j) {	
			//Pid of an output ge-operand that is passed to this input opperand from a preceeding ge-pseudoinstruction.
			ulint operand_pid = (*instr_group->input_operand_pids_get())[j];
			//Id of an input operand of the selected target instruction.
			ulint operand_ref = (*instr_group->input_operand_refs_get())[j];

			ptr<tm_instr_op_base> tm_op = (*instr_version->operands_input_get())[j];
			lassert(tm_op->id_get()==operand_ref);
			
			ptr<ge_operand> ge_op;
			
			if ( operand_pid <= PIN_4 ) {
				//The operand coresponds to the input pi-operand of the input pi-pseudoinstruction.
				ulint i_op_pos = operand_pid - PIN_1;
				
				lassert(i_op_pos==0);
				
				ge_op = input_op;
				
				if ( input_reg ) {
					(*ge_op.dncast<ge_operand_reg>()->assigned_registers_get())[ge] = input_reg->id_get();
				}
			} else {
				//The operand's source is an output operand of preceeding ge-pseudoinstruction. 
				//Find the output operand.
				ulint2ge_operand__type::iterator it_ge = tm_op2ge_op->find(operand_pid);
				lassert(it_ge!=tm_op2ge_op->end());
				ge_op = it_ge->second;
			}
			
			
			input_ops->push_back(ge_op);
		}	
		
		//Output operands.
		for(ulint j=0; j<instr_group->output_operand_pids_get()->size(); ++j) {	
			//Pid of an output ge-operand.
			ulint operand_pid = (*instr_group->output_operand_pids_get())[j];
			//Id of an output operand of the selected target instruction.
			ulint operand_ref = (*instr_group->output_operand_refs_get())[j];

			ptr<ge_operand> ge_op;
						
			if ( operand_pid <= POUT_4 ) {
				//The operand coresponds to the output pi-operand of the input pi-pseudoinstruction.
				ulint i_op_pos = operand_pid - POUT_1;
				
				lassert(i_op_pos==0);

				//Convert pi-operand to ge-operand
				ge_op = output_op;
				
				if ( output_reg ) {
					(*ge_op.dncast<ge_operand_reg>()->assigned_registers_get())[ge] = output_reg->id_get();
				}	
			} else {
				//Regular output operand need to be generated.
				ptr<tm_instr_op_base> tm_op = (*instr_version->operands_output_get())[j];
				lassert(tm_op->id_get()==operand_ref);
				
				//Data type of the operand.
				ulint type;
				
				if ( tm_op->allowed_types_get()->size()==1 ) {
					//The operand has only single type allowed.
					type = *tm_op->allowed_types_get()->begin();
				} else {
					//The operand has multiple types allowed.
					lassert(instr_group->input_operand_pids_get()->size()>0);
				
					type = input_op->type_get()->id_get();
					
					//Check that the type is among allowed ones.
					lassert(tm_op->allowed_types_get()->find(type)!=tm_op->allowed_types_get()->end());
				}
				
				//Convert operand of the tm_inst template to ge-operand.
				ge_op = tm_op->convert_to_ge(tm_dt_simple::instance((::lestes::md::types::dt_id_type)type));
				 
				tm_op2ge_op->insert(::std::pair<ulint, srp<ge_operand> >(operand_pid,ge_op));
			}
			
			//Set origin of the operand.
			ge_op->origin_set(ge);
			
			//add operand to list of output operands
			output_ops->push_back(ge_op);
		}
		prev = ge;
		output->push_back(ge);
	}
	
	log1 << "generate_ge_code - end\n" << eolog;
	
	return output;
}

/*!
	\brief Recursively traverses list of instruction_groups and chooses one instruction from each instruction_group that is consistent with the ones previously choosed.

	\param input_operand An input operand.
	\param input_reg If the input operand is pseudoregister, then parameter gets register allocated to the operand. NULL otherwise.
	\param output_operand An output operand.
	\param output_reg If the output operand is pseudoregister, then parameter gets register allocated to the operand. NULL otherwise.
	\param pat An examinated list of instruction_groups.
	\param selected_instrs A list of selected instructions from preceeding steps.
	\param selected_operands A list of operands of selected instructions from preceding steps.
	\param selected_op_types A list of datatypes of selected_operands.
	\param curr_instrs_pat_idx An index of the currentky examinated instruction_group.
	\return False if no one instruction from the examinated instruction_group is consistent with the ones choosed in previous steps.
*/
bool move_generator::find_consistent_instructions(ptr<ge_operand> input_op, ptr<tm_register_base> input_reg, ptr<ge_operand> output_op, ptr<tm_register_base> output_reg, ptr<pi_pi2ge_pi_template_base> pat, ptr<ulint_vector__type> selected_instrs, ptr<ulint2tm_instr_op_base__type> selected_operands, ptr<ulint2ulint__type> selected_op_types, ulint curr_instrs_pat_idx) {
	log1 << "find_consisten_instructions - start\n" << eolog;
	
	if ( curr_instrs_pat_idx==pat->instruction_groups_get()->size() ) {
		//All the instruction templates has been visited. Terminate recursion.
		log1 << "find_consisten_instructions - end 1\n" << eolog;
		return true;
	}
	
	//Inspected instruction template.
	ptr<instruction_group_base> instr_group = (*pat->instruction_groups_get())[curr_instrs_pat_idx];
	
	//Go through possible instruction versions and find the suitable one.
	for(ulint i=0; i<instr_group->instructions_get()->size(); ++i) {
		ulint instr_version_id = (*instr_group->instructions_get())[i];
		ptr<tm_instr_base> instr_version = tm_instr::instance(instr_version_id);
		
		if ( !is_instruction_consistent(input_op,input_reg,output_op,output_reg,instr_group,instr_version,selected_operands,selected_op_types) ) {
			//The instruction is not suitable.
			continue;
		}
		
		//Add operands of the suitable instruction version to the selected_operands list.
		add_selected_operands(input_op,output_op,instr_group,instr_version,selected_operands,selected_op_types);
		selected_instrs->push_back(instr_version_id);
		
		//Check whether exists version of all consequent instruction templates that are suitable with the instructio version.
		if ( find_consistent_instructions(input_op,input_reg,output_op,output_reg,pat,selected_instrs,selected_operands,selected_op_types,curr_instrs_pat_idx+1) ) {
			//Yes, they exists.
			log1 << "find_consisten_instructions - end 2 (good)\n" << eolog;
			return true;
		}
		
		//No, for some of the sonsequent instructions templates don't exist suitable instruction versions. Rollback and try another version.
		remove_selected_operands(instr_group,selected_operands,selected_op_types);
		selected_instrs->pop_back();
	}
	
	log1 << "find_consisten_instructions - end (bad)\n" << eolog;
	return false;
}


/*!
	\brief Checks whether a tm_instr is compatible with operands that are passed to it from preceding instructions.

	\param input_operand An input operand.
	\param input_reg If the input operand is pseudoregister, then parameter gets register allocated to the operand. NULL otherwise.
	\param output_operand An output operand.
	\param output_reg If the output operand is pseudoregister, then parameter gets register allocated to the operand. NULL otherwise.	
	\param instr_group A group of instructions.
	\param instr_version A instruction to be tested.
	\param pi A source pi-pseudoinstruction.
	\param selected_operands A list of operands of preceding instructions.
	\param selected_types A list of datatypes of the selected_operands.
	\return True if the instruction is consistent. False otherwise.
*/
bool move_generator::is_instruction_consistent(ptr<ge_operand> input_op, ptr<tm_register_base> input_reg, ptr<ge_operand> output_op, ptr<tm_register_base> output_reg,ptr<instruction_group_base> instr_group, ptr<tm_instr_base> instr_version, ptr<ulint2tm_instr_op_base__type> selected_operands,ptr<ulint2ulint__type> selected_types) {
	log1 << "is_instruction_consistent - start (tm_instr=" << instr_version->id_get() << ")\n" << eolog;
	
	/*
		Go through input operands and check that are compatible with selected output operands of preceeding instructions.
	*/
	for(ulint i=0; i<instr_group->input_operand_pids_get()->size(); ++i) {	
		ulint operand_pid = (*instr_group->input_operand_pids_get())[i];
		ulint operand_ref = (*instr_group->input_operand_refs_get())[i];
		
		lassert(operand_pid<=PIN_4 || operand_pid>POUT_4 );
		
		ptr<tm_instr_op_base> op = find_op_by_id(instr_version->operands_input_get(),operand_ref);
		
		if ( operand_pid <= PIN_4 ) {
			//The operand coresponds to the input pi-operand of the input pi-pseudoinstruction.
			ulint i_op_pos = operand_pid - PIN_1;
			
			lassert(i_op_pos==0);
			
			if ( !is_operand_compatible1(op,input_op,input_op->type_get()->id_get(),input_reg) ) {
				log1 << "is_instruction_consistent - end 2 (bad)\n" << eolog;
				return false;
			}
			 
		} else {
			//Check compatibility with selected output operands of preceeding instructions.
			ulint2tm_instr_op_base__type::iterator it_op = selected_operands->find(operand_pid);
			ulint2ulint__type::iterator it_t = selected_types->find(operand_pid);
			
			lassert(it_op!=selected_operands->end());
			lassert(it_t!=selected_types->end());
			
			if ( !is_operand_compatible2(op,it_op->second,it_t->second) ) {
				log1 << "is_instruction_consistent - end 3 (bad)\n" << eolog;
				return false;
			}
		}
	}
	
	/*
		Go through output operands and check that are compatible with output operands of the pi-pseudoinstruction.
	*/
	for(ulint i=0; i<instr_group->output_operand_pids_get()->size(); ++i) {	
		ulint operand_ref = (*instr_group->output_operand_refs_get())[i];
		ulint operand_pid = (*instr_group->output_operand_pids_get())[i];
		
		lassert(operand_pid > PIN_4);
		
		if ( operand_pid>=POUT_1 && operand_pid <= POUT_4 ) {
			ulint i_op_pos = operand_pid - POUT_1;
			lassert(i_op_pos==0);
			
			ptr<tm_instr_op_base> op = find_op_by_id(instr_version->operands_output_get(),operand_ref);
			
			if ( !is_operand_compatible1(op,output_op,output_op->type_get()->id_get(),output_reg) ) {
				log1 << "is_instruction_consistent - end 4 (bad)\n" << eolog;
				return false;
			}
			 
		} 
	}
	
	log1 << "is_instruction_consistent - end\n" << eolog;
	return true;
}

/*!
	\brief Checks whether a tm_instr_op is compatible with a pi_operand (i.e. the tm_instr_op can be converted to the pi_operand).
	
	\param instr_op A tm_instr_op instance.
	\param pi_op A pi_operand instance.
	\param pi_type A datataype of the pi_operand.
	\return True if operands are compatible. False otherwise.
*/
bool move_generator::is_operand_compatible1(ptr<tm_instr_op_base> instr_op, ptr<ge_operand> ge_op, ulint type, ptr<tm_register_base> reg) {
	log1 << "is_operand_compatible1 - start (instr_op=" << instr_op->id_get() << ")\n" << eolog;
	
	if ( instr_op->allowed_types_get()->find(type)==instr_op->allowed_types_get()->end() ) {
		log1 << "is_operand_compatible1 - end1 (bad)\n" << eolog;
		return false;
	}
	
	ulint kind1 = instr_op->kind_get();
	ulint kind2 = ge_op->kind_get();
	
	if ( kind1==tm_instr_op_base::REGISTER ) {
		if ( kind2!=ge_operand::REGISTER ) {
			return false;
		}
		
		lassert(reg);
		
		ptr<tm_instr_op_reg_base> tmp_op = instr_op.dncast<tm_instr_op_reg_base>();
		
		return tmp_op->allowed_registers_get()->find(reg->id_get())!=tmp_op->allowed_registers_get()->end();
	}
	
	
	if ( ( kind1==tm_instr_op_base::IMMEDIATE && kind2!=ge_operand::IMMEDIATE ) ||
		 ( kind1==tm_instr_op_base::MEMORY && kind2!=ge_operand::MEMORY ) ) {
		 log1 << "is_operand_compatible1 - end2 (bad)\n" << eolog;
		 return false;
	}
	 
	log1 << "is_operand_compatible1 - end\n" << eolog;
	return true;
}

/*!
	\brief Checks whether two operands have the same kind, nonempty intersection of allowed datatypes and nonempty set of allowed registers in case that the kind of the operands is register.
	
	\param op1 An operand.
	\param op2 An operand.
	\type type A type of the op2.
	\return True it the operands are compatible. False otherwise.
*/
bool move_generator::is_operand_compatible2(ptr<tm_instr_op_base> op1, ptr<tm_instr_op_base> op2, ulint type) {
	log1 << "is_operand_compatible2 - start (instr_op=" << op1->id_get() << ")\n" << eolog;
	
	if ( op1->allowed_types_get()->find(type)==op1->allowed_types_get()->end() ) {
		log1 << "is_operand_compatible2 - end 1 (bad)\n" << eolog;
		return false;
	}
	
	if ( op1->kind_get()!=op2->kind_get() ) {
		log1 << "is_operand_compatible2 - end 2 (bad)\n" << eolog;
		return false;
	}
	
	if ( op1->kind_get()==tm_instr_op_base::REGISTER ) {
		ptr<id_set__type> allowed_regs1 = op1.dncast<tm_instr_op_reg_base>()->allowed_registers_get();
		ptr<id_set__type> allowed_regs2 = op2.dncast<tm_instr_op_reg_base>()->allowed_registers_get();
		ptr<id_set__type> allowed_intersec = id_set__type::create();
		
		::std::set_intersection(
			allowed_regs1->begin(),
			allowed_regs1->end(),
			allowed_regs2->begin(),
			allowed_regs2->end(),
			::std::inserter(*allowed_intersec, allowed_intersec->begin()));
			
		
		if ( allowed_intersec->size()!=0 ) {
			log1 << "is_operand_compatible2 - end 3 (good)\n" << eolog;
			return true;
		}
	}
	
	log1 << "is_operand_compatible2 - end (bad)\n" << eolog;
	return false;
}

/*!
	\brief Adds instruction's operands to a list of selected operands.
	
	\param instr_group A group of instructions.
	\param instr_version A parent instruction of the operands.
	\param pi A source pi-pseudoinstruction.
	\param selected_operands A list of operands of preceding instructions.
	\param selected_types A list of datatypes of the selected_operands.
*/
void move_generator::add_selected_operands(ptr<ge_operand> input_op,ptr<ge_operand>, ptr<instruction_group_base> instr_group, ptr<tm_instr_base> instr_version, ptr<ulint2tm_instr_op_base__type> selected_operands,ptr<ulint2ulint__type> selected_types) {
	
	// Add output operands of the selected instruction version to the selected_operands list.  The types of the selected operands add to the selected_types list. 
	for(ulint i=0; i<instr_group->output_operand_pids_get()->size(); ++i) {
		ulint operand_pid = (*instr_group->output_operand_pids_get())[i];
		ulint operand_ref = (*instr_group->output_operand_refs_get())[i];
		
		lassert(operand_pid > PIN_4);
		
		ptr<tm_instr_op_base> op = find_op_by_id(instr_version->operands_output_get(),operand_ref);
		selected_operands->insert(::std::pair<ulint, srp<tm_instr_op_base> >(operand_pid, op));		
		
		ulint type;
		
		type = input_op->type_get()->id_get();
		
		selected_types->insert(::std::pair<ulint, ulint>(operand_pid, type));		
	}
}

/*!
	\brief Removes instruction's operands to a list of selected operands.
	
	\param instr_group A group of instructions.
	\param selected_operands A list of operands of preceding instructions.
	\param selected_types A list of datatypes of the selected_operands.
*/
void move_generator::remove_selected_operands(ptr<instruction_group_base> instr_group, ptr<ulint2tm_instr_op_base__type> selected_operands, ptr<ulint2ulint__type> selected_types) {
	
	for(ulint i=0; i<instr_group->output_operand_pids_get()->size(); ++i) {
		ulint operand_pid = (*instr_group->output_operand_pids_get())[i];
		selected_operands->erase(operand_pid);		
		selected_types->erase(operand_pid);		
	}
}

/*!
	\brief Traverses a list of operands and returns an operand with given id.
	
	\param list A list of operands.
	\param id An id of operand.
	\return A operand.
*/
ptr<tm_instr_op_base> move_generator::find_op_by_id(ptr<tm_instr_op_base_vector__type> list, ulint id) {
	for(uint i=0; i<list->size(); ++i) {
		ptr<tm_instr_op_base> op = (*list)[i];
		
		if ( op->id_get()==id ) {
			return op;
		}
	}
	
	return NULL;
}


end_package(registers);
end_package(md);
end_package(lestes);

