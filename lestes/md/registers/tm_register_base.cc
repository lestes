#include <lestes/md/registers/tm_register_base.g.hh>

package(lestes);
package(md);
package(registers);

/*!
	\brief Tells whether the current register can't be spilled.
*/
bool tm_register_base::is_unspillable() {
	return flags && RF_NOSPILL;
}

end_package(registers);
end_package(md);
end_package(lestes);

