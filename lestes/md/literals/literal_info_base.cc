/*!
	\file
	\brief Literal info base.
	\author jaz
*/
#include "literal_info_base.g.hh"

package(lestes);
package(md);
package(literals);

/*!
	\brief Returns kind of literal info.
*/
literal_info_base::kind_type li_compound_base::kind_get() {
	return COMPOUND;
}

/*!
	\brief Returns kind of literal info.
*/
literal_info_base::kind_type li_simple_base::kind_get() {
	return SIMPLE;
}

end_package(literals);
end_package(md);
end_package(lestes);

