<?xml version="1.0" encoding="UTF-8"?>
<lsd xmlns="http://lestes.jikos.cz/schemas/lsd" xmlns:h="http://www.w3.org/TR/REC-html40">

<dox file="both">
	<det>\author jaz</det>
</dox>

<file-name>literal_info_base</file-name>

<packages>
	<p>lestes</p>
	<p>md</p>
	<p>literals</p>
</packages>

<imports>
	<i>lestes/std/list.hh</i>
	<i>lestes/md/common.hh</i>
</imports>

<implementation-imports>
	<i>lestes/std/ucn_string.hh</i>
	<i>lestes/md/types/tm_data_type_base.g.hh</i>
</implementation-imports>

<foreign-class name="object">
	<p>lestes</p>
	<p>std</p>
</foreign-class>

<foreign-class name="tm_data_type_base">
	<p>lestes</p>
	<p>md</p>
	<p>types</p>
</foreign-class>

<class name="literal_info_base" abstract="yes" base="object">

	<dox>
		<bri>Informations about literal - type and value.</bri>
	</dox>

	<enum name="kind_type">
		<e n="COMPOUND" />
		<e n="SIMPLE" />
	</enum>
	
	<field name="type" type="tm_data_type_base">
		<dox>
			<bri>Data type of the literal.</bri>
		</dox>
	</field>
	
	<method name="get_asm_definition_val" type="lstring" specifier="virtual" qualifier="abstract">
		<dox>
			<bri>Returns asm string representing value of the literal.</bri>
		</dox>
	</method>
	
	<method name="get_asm_definition_type" type="lstring" specifier="virtual" qualifier="abstract">
		<dox>
			<bri>Returnd asm string representing datatype of the literal.</bri>
		</dox>
	</method>
	
	<method name="kind_get" type="kind_type" specifier="virtual" qualifier="abstract">
		<dox>
			<bri>Returns kind of the literal.</bri>
		</dox>
	</method>
</class>

<class name="li_simple_base" abstract="yes" base="literal_info_base">
	<dox>
		<bri>Simple literal.</bri>
		<det>Its datatype is simple type ( e.g. integer(1), double(2.45e3) ).
		
Note: A concrete implementation is target-machine dependent and it is placed in /target/machine/${TARGET_CPU}/lestes/md/literals/ directory.		
</det>
	</dox>

	<field name="data" type="ucn_string">
		<dox>
			<bri>Value of the literal.</bri>
		</dox>
	</field>
	
	<method name="get_asm_definition_val" type="lstring" specifier="virtual" qualifier="abstract" />
	<method name="get_asm_definition_type" type="lstring" specifier="virtual" qualifier="abstract" />
	<method name="kind_get" type="kind_type" specifier="virtual" />
</class>

<class name="li_compound_base" abstract="yes" base="literal_info_base">
	<dox>
		<bri>Compound literal.</bri>
		<det>It is composed of several literals (e.g. string "Hello" is composed of 'h','e','l','l','o' literals).
		
Note: A concrete implementation is target-machine dependent and it is placed in /target/machine/${TARGET_CPU}/lestes/md/literals/ directory.</det>
	</dox>

	<collection name="items" kind="list" type="literal_info_base">
		<dox>
			<bri>List of literals composing the compound literal.</bri>
		</dox>
	</collection>
	
	<method name="get_asm_definition_val" type="lstring" specifier="virtual" qualifier="abstract"/>
	<method name="get_asm_definition_type" type="lstring" specifier="virtual" qualifier="abstract"/>
	<method name="kind_get" type="kind_type" specifier="virtual" />
</class>

</lsd>

