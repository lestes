#include <lestes/md/instructions/tm_instr_base.g.hh>
#include <lestes/backend_v2/intercode/ge.g.hh>
#include <lestes/backend_v2/intercode/pi.g.hh>

package(lestes);
package(md);
package(instructions);

using namespace ::lestes::backend_v2::intercode;

/*!
	\brief Tells whether the current instruction is jump.
*/
bool tm_instr_base::is_jump()
{
    return flags & IF_JUMP;
}

/*!
	\brief Tells whether the current instruction is load.
*/
bool tm_instr_base::is_load()
{
    return flags & IF_LOAD;
}

/*!
	\brief Tells whether the current instruction is store.
*/
bool tm_instr_base::is_store()
{
    return flags & IF_STORE;
}

/*!
	\brief Tells whether the current instruction is system instruction.
*/
bool tm_instr_base::is_system()
{
    return flags & IF_SYS;
}

/*!
	\brief Tells whether the current instruction is copy instruction.
*/
bool tm_instr_base::is_copy()
{
    return flags & IF_COPY;
}


/*!
	\brief Tells kind of current operand.
*/
tm_instr_op_base::kind_type tm_instr_op_mem_base::kind_get()
{
    return tm_instr_op_base::MEMORY;
}

/*!
	\brief Creates new instance of ge_operand on the basis of the current tm_instr_op.
	
	\param type A datatype of the created operand.
	\return A new ge_operand.
*/
ptr< ::lestes::backend_v2::intercode::ge_operand > tm_instr_op_mem_base::convert_to_ge(ptr< ::lestes::md::types::tm_data_type_base >) {
	lassert(false);
}

/*!
	\brief Tells kind of current operand.
*/
tm_instr_op_base::kind_type tm_instr_op_imm_base::kind_get()
{
    return tm_instr_op_base::IMMEDIATE;
}

/*!
	\brief Creates new instance of ge_operand on the basis of the current tm_instr_op.
	
	\param type A datatype of the created operand.
	\return A new ge_operand.
*/
ptr< ::lestes::backend_v2::intercode::ge_operand > tm_instr_op_imm_base::convert_to_ge(ptr< ::lestes::md::types::tm_data_type_base >) {
	lassert(false);
}


/*!
	\brief Tells kind of current operand.
*/
tm_instr_op_base::kind_type tm_instr_op_reg_base::kind_get()
{
    return tm_instr_op_base::REGISTER;
}

/*!
	\brief Creates new instance of ge_operand on the basis of the current tm_instr_op.
	
	\param type A datatype of the created operand.
	\return A new ge_operand.
*/
ptr< ::lestes::backend_v2::intercode::ge_operand > tm_instr_op_reg_base::convert_to_ge(ptr< ::lestes::md::types::tm_data_type_base > type) {
	return ge_operand_reg::create(type,NULL,NULL);
}


end_package(insturctions);
end_package(md);
end_package(lestes);

