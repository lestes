<?xml version="1.0" encoding="UTF-8"?>
<lsd xmlns="http://lestes.jikos.cz/schemas/lsd" xmlns:h="http://www.w3.org/TR/REC-html40">

<dox file="both">
	<det>\author jaz</det>
</dox>

<file-name>instruction_group_base</file-name>

<packages>
	<p>lestes</p>
	<p>md</p>
	<p>instructions</p>
</packages>

<imports>
	<i>lestes/std/vector.hh</i>
	<i>lestes/md/common.hh</i>
	<i>lestes/md/instructions/tm_instr_base.g.hh</i>
</imports>

<foreign-class name="object">
	<p>lestes</p>
	<p>std</p>
</foreign-class>


<class name="instruction_group_base" abstract="yes" base="::lestes::std::object">
	<dox>
		<bri>Represents a group of instructions that perform the same operation but they differ in type or kind of operands or execution background (used register, execution units ).</bri>
		<det>Note: A concrete derived class is target dependent and it is placed in 
/target/machine/${TARGET_CPU}/lestes/md/instructions/ directory. </det>
	</dox>
	
	<collection name="instructions" type="ulint" kind="vector" get="protected" set="protected" alt="protected" init="void" >
		<dox>
			<bri>Grouped instructions.</bri>
		</dox>
	</collection>
	
	<collection name="input_operand_pids" type="ulint" kind="vector" get="protected" set="protected" alt="protected" init="void" >
		<dox>
			<bri>Ids of output operands of another instruction_group that this instruction_group receives as input operands.</bri>
		</dox>
	</collection>
	
	<collection name="output_operand_pids" type="ulint" kind="vector" get="protected" set="protected" alt="protected" init="void" >
		<dox>
			<bri>Ids of group's output operands that allows other instruction_groups to reference them.</bri>
		</dox>
	</collection>
	
	<collection name="input_operand_refs" type="ulint" kind="vector" get="protected" set="protected" alt="protected" init="void" >
		<dox>
			<bri>An item within this list marks operand of grouped instructions that corresponds to an id within input_operand_pids at the same index.</bri>
		</dox>
	</collection>
	
	<collection name="output_operand_refs" type="ulint" kind="vector" get="protected" set="protected" alt="protected" init="void" >
		<dox>
			<bri>An item within this list marks operand of grouped instructions that corresponds to an id within output_operand_pids at the same index.</bri>
		</dox>
	</collection>

</class>

</lsd>
