/*!
	\file
	\author jaz
*/

#include <lestes/backend_v2/debug/debug.hh>
#include <lestes/backend_v2/intercode/pi.g.hh>
#include <lestes/backend_v2/intercode/visitor_pi_pi2pi_operands.g.hh>
#include <lestes/backend_v2/structs/pi_operands.g.hh>
#include <lestes/md/instructions/pi_pi2ge_pi_template_base.g.hh>
#include <lestes/md/instructions/instruction_group.g.hh>
#include <lestes/md/instructions/tm_instr.g.hh>
#include <lestes/md/types/tm_data_type.g.hh>
#include <lestes/md/types/copy_constructor_call_generator.g.hh>
#include <lestes/md/instructions/pi_pi2ge_pi_base.g.hh>

package(lestes);
package(md);
package(instructions);

using namespace ::lestes::backend_v2::debug;
using namespace ::lestes::msg;
using ::lestes::backend_v2::structs::pi_operands;
using ::lestes::md::types::tm_dt_simple;
using ::lestes::md::types::copy_constructor_call_generator;
using namespace ::lestes::backend_v2::intercode;

typedef list< ulint > ulint_list__type;
typedef vector< ulint > ulint_vector__type;
typedef set< ulint > ulint_set__type;
typedef map< ulint, srp<tm_instr_op_base> > ulint2tm_instr_op_base__type;
typedef map< ulint, ulint > ulint2ulint__type;
typedef list< srp < instruction_group_base > > instruction_group_base_list__type;
typedef vector< srp < tm_instr_op_base > > tm_instr_op_base_vector__type;
typedef vector< srp < ge_operand > > ge_operand_vector__type;
typedef vector< srp < ge_pi > > ge_pi_vector__type;
typedef set< srp < ge_pi > > ge_pi_set__type;
typedef vector< srp < ge_sp > > ge_sp_vector__type;
typedef vector< srp < pi_sp > > pi_sp_vector__type;
typedef map< ulint, srp<ge_operand> > ulint2ge_operand__type;
typedef map< srp<pi_operand>, srp<ge_operand> > pi_op2ge_op__type;
typedef map< srp<pi_sp>, srp<ge_sp> > pi_sp2ge_sp__type;


declare_logger(log1);
initialize_logger( log1, "pi_pi2ge_pi_base", md_logger );


/*!
	\brief Converts a pi-pseudoinstruction to a corresponding list of ge-pseudoinstructions.
	
	The function takes an instance of pi_pi2ge_pi_template that contains list of instruction_group instances. It traverses the list
	and chooses one instruction from every instruction_group so that the choosed instruction are compatible ( operands that are passed
	among them are legal for every instruction ) and they together perform the requested operation.
	
	\param pi A pi-pseudoinstruction.
	\param pat A pi_pi2ge_pi template that describes possible conversions.
*/
ptr<ge_pi_vector__type> pi_pi2ge_pi_base::convert(ptr<pi_pi> pi, ptr<pi_pi2ge_pi_template_base> pat) {
	log1 << "convert - start (pi=" << pi->reflection_get()->back()->name_get() << ")\n" << eolog;
	
	lassert(pi);
	lassert(pat);
	
	ptr<ulint_vector__type> selected_instrs = ulint_vector__type::create();
	ptr<ulint2tm_instr_op_base__type> selected_operands = ulint2tm_instr_op_base__type::create();
	ptr<ulint2ulint__type> selected_op_types = ulint2ulint__type::create();
	
	if ( !find_consistent_instructions(pi,pat,selected_instrs,selected_operands,selected_op_types,0) ) {
		log1 << "convert - end 1 (bad)\n" << eolog;
		return NULL;
	}
	
	log1 << "convert - end\n" << eolog;
	
	return generate_ge_code(pi,pat,selected_instrs,selected_op_types);
}

/*!
	\brief Setups dependencies among generated ge-pseudoinstructions.
	
	\param ge A ge-pseudoinstruction that needs dependenices to be set.
*/
void pi_pi2ge_pi_base::setup_dependencies(ptr<ge_pi> ge) {
	log1 << "setup_dependencies - start\n" << eolog;
	
	ptr<pi_pi> pi = ge->pi_source_get();	
	ptr<ge_pi_set__type> dependencies = ge->dependencies_get();
	
	//Add the ge-pi that corresponds to the original pi-pseudoinstruction's psp to the dependency list.
	if ( pi->psp_get() ) {
		//Set dependence on the last ge-pi that corresponds to the psp
		ptr<ge_pi> ge_psp = (*pi_sp2ge_sp)[pi->psp_get().dncast<pi_sp>()];
		lassert(ge_psp);
		dependencies->insert(ge_psp);
	}
	
	//Add the ge-pi that corresponds to the original pi-pseudoinstruction's nsp to the dependency list.
	if ( pi->nsp_get() && pi->nsp_get()->psp_get()!=pi ) {
		//Set dependence on the first ge-pi that corresponds to the nsp
		ptr<ge_pi> ge_nsp = (*pi_sp2ge_sp)[pi->nsp_get().dncast<pi_sp>()];
		lassert(ge_nsp);
		ge_nsp->dependencies_get()->insert(ge);
	}
	
	
	//Add origins of input ge-operands to the dependency list.
	ptr<ge_operand_vector__type> in_ops = ge->operands_input_get();
	for(ge_operand_vector__type::size_type i=0; i<in_ops->size(); ++i) {
		ptr<ge_operand> op = (*in_ops)[i];
		
		/*
			Immediate has ho origin. 
		*/
		if ( op->kind_get()!=ge_operand::IMMEDIATE ) {
			ptr<ge_pi> origin = op->origin_get();
			
			if ( !origin ) {
				/*
					The operand is not output operand of any ge-instruction.
					It can happen if the operand is memmory operand referencing local variable or
					pi_mem_ptr_deref. 
				*/
				ptr<pi_operand> pi_source = op->pi_source_get();
				
				if ( pi_source->kind_get()==pi_operand::MEM_PTR_DEREF ) {
					/*
						Ptr_mem_deref is wrapper of a register. Its origin is the 
						origin of the wrapped register.	
					*/
					ptr<ge_operand> addr = pi_source.dncast<pi_mem>()->factory_get().dncast<pi_mf_ptr_deref>()->ge_addr_get();
					origin = addr->origin_get();
				} else {
					/*
						Pi-source of memmory operand referencing local variable has pi-origin set to preceding pi-sequencepoint.
						So we take ge-pseudoinstruction that corresponds to the pi-sp and set it as
						origin of the ge-operand.
					*/
					origin = (*pi_sp2ge_sp)[pi_source->origin_get().dncast<pi_sp>()];
				}
				op->origin_set(origin);
			}
			
			lassert(origin);
			dependencies->insert(origin);
		}
	}	
	
	
	log1 << "setup_dependencies - end\n" << eolog;
	
}

/*!
	\brief Gets list of selected target machine instruction ids for each instruction_group and generates output ge-code.
	
	\param pi A pi-pseudoinstruction.
	\param pat A list of instruction_group instances.
	\param choosed_instructions Selected instructions.
	\param  choosed_op_types Types of operands used within output code.
	\return Vector of ge-pi instructions.
*/
ptr<ge_pi_vector__type> pi_pi2ge_pi_base::generate_ge_code(ptr<pi_pi> pi, ptr<pi_pi2ge_pi_template_base> pat, ptr<ulint_vector__type> choosed_instrs,  ptr<ulint2ulint__type> choosed_op_types) {
	log1 << "generate_ge_code - start\n" << eolog;
	
	ptr<ge_pi_vector__type> output = ge_pi_vector__type::create();
	ptr<ulint2ge_operand__type> tm_op2ge_op = ulint2ge_operand__type::create(); 	
	ptr<pi_operands> pi_ops = pi->accept_visitor_pi_pi2pi_operands_gen_base(pi_operands_getter);
	
	ptr<ge_pi> prev;
	
	/*
		Go throught the pi2ge templates's instruction groups and generate output code according to the selected versions.
	*/
	for(::lestes::std::vector< srp< instruction_group_base > >::size_type i=0; i<pat->instruction_groups_get()->size(); ++i) {
		//The instruction group
		ptr<instruction_group_base> instr_group = (*pat->instruction_groups_get())[i];
		//The selected version.
		ptr<tm_instr_base> instr_version = tm_instr::instance((*choosed_instrs)[i]);
				
		//Output ge-code
		ptr<ge_pi> ge = ge_pi::create(instr_version,pi);
		
		ptr<ge_operand_vector__type> input_ops = ge->operands_input_get();
		ptr<ge_operand_vector__type> output_ops = ge->operands_output_get();
		ptr<ge_pi_set__type> dependencies = ge->dependencies_get();
		
		if ( prev ) {
			/*
				Set dependence on a preceeding output ge-pseudoinstruction of the teplate in order to force the output
				code to be ordered in the same way as instruction groups in the input template.	
			*/
			
			dependencies->insert(prev);
		}
		
		//Input operands.
		for(::lestes::std::vector< ulint >::size_type j=0; j<instr_group->input_operand_pids_get()->size(); ++j) {	
			//Pid of an output ge-operand that is passed to this input opperand from a preceeding ge-pseudoinstruction.
			ulint operand_pid = (*instr_group->input_operand_pids_get())[j];
			//Id of an input operand of the selected target instruction.
			ulint operand_ref = (*instr_group->input_operand_refs_get())[j];

			ptr<tm_instr_op_base> tm_op = (*instr_version->operands_input_get())[j];
			lassert(tm_op->id_get()==operand_ref);
			
			ptr<ge_operand> ge_op;
			
			if ( operand_pid <= PIN_4 ) {
				//The operand coresponds to the input pi-operand of the input pi-pseudoinstruction.
				ulint i_op_pos = operand_pid - PIN_1;
				ptr<pi_operand> pi_op =	(*pi_ops->operands_input_get())[i_op_pos];

				//Convert pi-operand to ge-operand
				ge_op = pi_op2ge_op_convertor->convert(pi_op);
				
				if ( tm_op->destroyed_by_get()->size()!=0 && (*pi_op2count)[pi_op] > 1 ) {
					/*
						The input operand is destroyes(overwritten) by an output operand. When the instruction is performed, the operand's
						value is overwritten by output value. But the original value 
						will be required somewhere later. So we must preserve the original value by copying to
						the another destroyable operand. Then the copy of the operand will be used instead the preserved one.
					*/
					ptr<ge_pi_vector__type> copy_code = ge_pi_vector__type::create();

					ge_op = copy_constructor_call_generator::instance()->generate_copy(ge_op,copy_code);
				
					//Append copy-code to the output code.
					for(ulint k=0; k<copy_code->size(); ++k) {
						ptr<ge_pi> cp = (*copy_code)[k];
						cp->pi_source_set(pi);
						output->push_back(cp);
					
						/*
							Note: The ge-pseudoinstruction has to be executed after the copy instructions.
							It is accomplished by adding origin of the copy of the operand to list of dependecies
							of the ge-pseudoinstruction that uses it. This is done during the setup_dependencies phase.
						*/
					} 				
				}
			} else {
				//The operand's source is an output operand of preceeding ge-pseudoinstruction. 
				//Find the output operand.
				ulint2ge_operand__type::iterator it_ge = tm_op2ge_op->find(operand_pid);
				lassert(it_ge!=tm_op2ge_op->end());
				ge_op = it_ge->second;
			}		
			
			input_ops->push_back(ge_op);
		}	
		
		//Output operands.
		for(ulint j=0; j<instr_group->output_operand_pids_get()->size(); ++j) {	
			//Pid of an output ge-operand.
			ulint operand_pid = (*instr_group->output_operand_pids_get())[j];
			//Id of an output operand of the selected target instruction.
			ulint operand_ref = (*instr_group->output_operand_refs_get())[j];

			ptr<ge_operand> ge_op;
						
			if ( operand_pid <= POUT_4 ) {
				//The operand coresponds to the output pi-operand of the input pi-pseudoinstruction.
				ulint i_op_pos = operand_pid - POUT_1;
				ptr<pi_operand> pi_op =	(*pi_ops->operands_output_get())[i_op_pos];

				//Convert pi-operand to ge-operand
				ge_op = pi_op2ge_op_convertor->convert(pi_op);
					
			} else {
				//Regular output operand need to be generated.
				ptr<tm_instr_op_base> tm_op = (*instr_version->operands_output_get())[j];
				lassert(tm_op->id_get()==operand_ref);
				
				//Data type of the operand.
				ulint type;
				
				if ( tm_op->allowed_types_get()->size()==1 ) {
					//The operand has only single type allowed.
					type = *tm_op->allowed_types_get()->begin();
				} else {
					//The operand has multiple types allowed.
					lassert(instr_group->input_operand_pids_get()->size()>0);
				
					/*
						Which one of the allowed types should we take?
						
						This is the tricky part. Possibilities:
							1. Only single allowed type has to be allowed for an operand.
							2. New attribute that tells which one of the input operands has the same type.
							3. The type is the same one as the allowed type of the first input operand.
							4. Something else.	
						
						The selected solution is 4:
							- the output type is the same as the type of the first input operand.
							
						The writer of the machine description file should be aware of this mechanism, so the instruction
						that has multiple allowed output types and doesn'd fulfil the requirement should be rewritten into
						multiple instructions with single output type. 
					*/
					ulint iop_pid = (*instr_group->input_operand_pids_get())[0];
					ulint2ulint__type::iterator it_t = choosed_op_types->find(iop_pid);
					lassert(it_t!=choosed_op_types->end());
					
					type = it_t->second;
					
					//Check that the type is among allowed ones.
					lassert(tm_op->allowed_types_get()->find(type)!=tm_op->allowed_types_get()->end());
				}
				
				//Convert operand of the tm_inst template to ge-operand.
				ge_op = tm_op->convert_to_ge(tm_dt_simple::instance((::lestes::md::types::dt_id_type)type));
				 
				tm_op2ge_op->insert(::std::pair<ulint, srp<ge_operand> >(operand_pid,ge_op));
			}
			
			//Set origin of the operand.
			ge_op->origin_set(ge);
			
			//add operand to list of output operands
			output_ops->push_back(ge_op);
		}
		
		if ( instr_version->is_jump() ) {
			//Setup jump targets
			ptr<ge_sp_vector__type> jmp_targets = ge_sp_vector__type::create();
		
			ptr<pi_sp_vector__type> pi_targets = pi_ops->jmp_targets_get();
			for(ulint j=0; j<pi_targets->size(); ++j) {
				ptr<ge_sp> jmp_target = (*pi_sp2ge_sp)[(*pi_targets)[j]];
				jmp_target->is_jmp_target_set(true);
				jmp_targets->push_back(jmp_target);
			}	
		
			ge->jmp_targets_set(jmp_targets);
		}
		ge->validate();	
		output->push_back(ge);
	}
	
	log1 << "generate_ge_code - end\n" << eolog;
	
	return output;
}


/*!
	\brief Recursively traverses list of instruction_groups and chooses one instruction from each instruction_group that is consistent with the ones previously choosed.
	
	\param pi A converted pseudoinstruction.
	\param pat An examinated list of instruction_groups.
	\param selected_instrs A list of selected instructions from preceeding steps.
	\param selected_operands A list of operands of selected instructions from preceding steps.
	\param selected_op_types A list of datatypes of selected_operands.
	\param curr_instrs_pat_idx An index of the currentky examinated instruction_group.
	\return False if no one instruction from the examinated instruction_group is consistent with the ones choosed in previous steps.
*/
bool pi_pi2ge_pi_base::find_consistent_instructions(ptr<pi_pi> pi, ptr<pi_pi2ge_pi_template_base> pat, ptr<ulint_vector__type> selected_instrs, ptr<ulint2tm_instr_op_base__type> selected_operands, ptr<ulint2ulint__type> selected_op_types, ulint curr_instrs_pat_idx) {
	log1 << "find_consisten_instructions - start\n" << eolog;
	
	if ( curr_instrs_pat_idx==pat->instruction_groups_get()->size() ) {
		//All the instruction groups has been visited. Terminate recursion.
		log1 << "find_consisten_instructions - end 1\n" << eolog;
		return true;
	}
	
	//Testedd instruction group.
	ptr<instruction_group_base> instr_group = (*pat->instruction_groups_get())[curr_instrs_pat_idx];
	
	//Go through possible instructions and find the suitable one.
	for(ulint i=0; i<instr_group->instructions_get()->size(); ++i) {
		ulint instr_version_id = (*instr_group->instructions_get())[i];
		ptr<tm_instr_base> instr_version = tm_instr::instance(instr_version_id);
		
		if ( !is_instruction_consistent(instr_group,instr_version,pi,selected_operands,selected_op_types) ) {
			//The instruction is not suitable.
			continue;
		}
		
		//Add operands of the suitable instruction to the selected_operands list.
		add_selected_operands(instr_group,instr_version,pi,selected_operands,selected_op_types);
		selected_instrs->push_back(instr_version_id);
		
		//Check whether exists instruction for every consequent instruction_groups that are suitable with the instructio version.
		if ( find_consistent_instructions(pi,pat,selected_instrs,selected_operands,selected_op_types,curr_instrs_pat_idx+1) ) {
			//Yes, they exists.
			log1 << "find_consisten_instructions - end 2 (good)\n" << eolog;
			return true;
		}
		
		//No, for some of the consequent instructions group don't exist suitable instruction. Rollback and try another version.
		remove_selected_operands(instr_group,selected_operands,selected_op_types);
		selected_instrs->pop_back();
	}
	
	log1 << "find_consisten_instructions - end (bad)\n" << eolog;
	return false;
}

/*!
	\brief Checks whether a tm_instr is compatible with operands that are passed to it from preceding instructions.
	
	\param instr_group A group of instructions.
	\param instr_version A instruction to be tested.
	\param pi A source pi-pseudoinstruction.
	\param selected_operands A list of operands of preceding instructions.
	\param selected_types A list of datatypes of the selected_operands.
	\return True if the instruction is consistent. False otherwise.
*/
bool pi_pi2ge_pi_base::is_instruction_consistent(ptr<instruction_group_base> instr_group, ptr<tm_instr_base> instr_version, ptr<pi_pi> pi, ptr<ulint2tm_instr_op_base__type> selected_operands,ptr<ulint2ulint__type> selected_types) {
	log1 << "is_instruction_consistent - start (tm_instr=" << instr_version->id_get() << ")\n" << eolog;
	
	/*
		pi_pi2ge_pi_base::is_instruction_consistent is target-machine independent code. The following line is the way to
		run any target dependent code required.
	*/
	if ( !targetmachine__is_instruction_consistent(instr_version,pi) ) {
		log1 << "is_instruction_consistent - end 1 (bad)\n" << eolog;
		return false;
	}
	
	ptr<pi_operands> operands = pi->accept_visitor_pi_pi2pi_operands_gen_base(pi_operands_getter);
	/*
		Go through input operands and check that are compatible with selected output operands of preceeding instructions.
	*/
	for(ulint i=0; i<instr_group->input_operand_pids_get()->size(); ++i) {	
		ulint operand_pid = (*instr_group->input_operand_pids_get())[i];
		ulint operand_ref = (*instr_group->input_operand_refs_get())[i];
		
		lassert(operand_pid<=PIN_4 || operand_pid>POUT_4 );
		
		ptr<tm_instr_op_base> op = find_op_by_id(instr_version->operands_input_get(),operand_ref);
		
		if ( operand_pid <= PIN_4 ) {
			//The operand coresponds to the input pi-operand of the input pi-pseudoinstruction.
			ulint i_op_pos = operand_pid - PIN_1;
			
			lassert(operands->operands_input_types_get()->size()>i_op_pos);
			if ( !is_operand_compatible1(op,(*operands->operands_input_get())[i_op_pos],(*operands->operands_input_types_get())[i_op_pos]->id_get()) ) {
				log1 << "is_instruction_consistent - end 2 (bad)\n" << eolog;
				return false;
			}
			 
		} else {
			//Check compatibility with selected output operands of preceeding instructions.
			ulint2tm_instr_op_base__type::iterator it_op = selected_operands->find(operand_pid);
			ulint2ulint__type::iterator it_t = selected_types->find(operand_pid);
			
			lassert(it_op!=selected_operands->end());
			lassert(it_t!=selected_types->end());
			
			if ( !is_operand_compatible2(op,it_op->second,it_t->second) ) {
				log1 << "is_instruction_consistent - end 3 (bad)\n" << eolog;
				return false;
			}
		}
	}
	
	/*
		Go through output operands and check that are compatible with output operands of the pi-pseudoinstruction.
	*/
	for(ulint i=0; i<instr_group->output_operand_pids_get()->size(); ++i) {	
		ulint operand_ref = (*instr_group->output_operand_refs_get())[i];
		ulint operand_pid = (*instr_group->output_operand_pids_get())[i];
		
		lassert(operand_pid > PIN_4);
		
		if ( operand_pid>=POUT_1 && operand_pid <= POUT_4 ) {
			ptr<tm_instr_op_base> op = find_op_by_id(instr_version->operands_output_get(),operand_ref);
			ulint i_op_pos = operand_pid - POUT_1;
			
			lassert(operands->operands_output_types_get()->size()>i_op_pos);
			if ( !is_operand_compatible1(op,(*operands->operands_output_get())[i_op_pos],(*operands->operands_output_types_get())[i_op_pos]->id_get()) ) {
				log1 << "is_instruction_consistent - end 4 (bad)\n" << eolog;
				return false;
			}
			 
		} 
	}
	
	log1 << "is_instruction_consistent - end\n" << eolog;
	return true;
}

/*!
	\brief Checks whether a tm_instr_op is compatible with a pi_operand (i.e. the tm_instr_op can be converted to the pi_operand).
	
	\param instr_op A tm_instr_op instance.
	\param pi_op A pi_operand instance.
	\param pi_type A datataype of the pi_operand.
	\return True if operands are compatible. False otherwise.
*/
bool pi_pi2ge_pi_base::is_operand_compatible1(ptr<tm_instr_op_base> instr_op, ptr<pi_operand> pi_op, ulint pi_type) {
	log1 << "is_operand_compatible1 - start (instr_op=" << instr_op->id_get() << ")\n" << eolog;
	
	if ( instr_op->allowed_types_get()->find(pi_type)==instr_op->allowed_types_get()->end() ) {
		log1 << "is_operand_compatible1 - end1 (bad)\n" << eolog;
		return false;
	}
	
	ulint kind1 = instr_op->kind_get();
	ulint kind2 = pi_op->kind_get();
	
	if ( ( kind1==tm_instr_op_base::REGISTER && ( kind2!=pi_operand::PREG && kind2!=pi_operand::MEM_PREG)) ||
		 ( kind1==tm_instr_op_base::IMMEDIATE && kind2!=pi_operand::LIT ) ||
		 ( kind1==tm_instr_op_base::MEMORY && (kind2==pi_operand::MEM_PREG || kind2==pi_operand::PREG || kind2==pi_operand::LIT) ) ) {
		 log1 << "is_operand_compatible1 - end2 (bad)\n" << eolog;
		 return false;
	}
	 
	log1 << "is_operand_compatible1 - end\n" << eolog;
	return true;
}

/*!
	\brief Traverses a list and tells whether a value is contained.
	
	\param list A list.
	\param value A value.
	\return True if the value is in the list. False otherwise.
*/
bool pi_pi2ge_pi_base::is_in(ptr<vector<ulint> > list, ulint value) {
	for(uint i=0; i<list->size(); ++i) {
		if ( (*list)[i]==value ) {
			return true;
		}
	}
	return false;
}

/*!
	Checks whether two operands have the same kind, nonempty intersection of allowed datatypes and nonempty set of allowed registers in case that the kind of the operands is register.
	
	\param op1 An operand.
	\param op2 An operand.
	\type type A type of the op2.
	\return True it the operands are compatible. False otherwise.
*/
bool pi_pi2ge_pi_base::is_operand_compatible2(ptr<tm_instr_op_base> op1, ptr<tm_instr_op_base> op2, ulint type) {
	log1 << "is_operand_compatible2 - start (instr_op=" << op1->id_get() << ")\n" << eolog;
	
	if ( op1->allowed_types_get()->find(type)==op1->allowed_types_get()->end() ) {
		log1 << "is_operand_compatible2 - end 1 (bad)\n" << eolog;
		return false;
	}
	
	if ( op1->kind_get()!=op2->kind_get() ) {
		log1 << "is_operand_compatible2 - end 2 (bad)\n" << eolog;
		return false;
	}
	
	if ( op1->kind_get()==tm_instr_op_base::REGISTER ) {
		ptr<ulint_set__type> allowed_regs1 = op1.dncast<tm_instr_op_reg_base>()->allowed_registers_get();
		ptr<ulint_set__type> allowed_regs2 = op2.dncast<tm_instr_op_reg_base>()->allowed_registers_get();
		ptr<ulint_set__type> allowed_intersec = ulint_set__type::create();
		
		::std::set_intersection(
			allowed_regs1->begin(),
			allowed_regs1->end(),
			allowed_regs2->begin(),
			allowed_regs2->end(),
			::std::inserter(*allowed_intersec, allowed_intersec->begin()));
			
		
		if ( allowed_intersec->size()!=0 ) {
			log1 << "is_operand_compatible2 - end 3 (good)\n" << eolog;
			return true;
		}
	}
	
	log1 << "is_operand_compatible2 - end (bad)\n" << eolog;
	return false;
}

/*!
	\brief Adds instruction's operands to a list of selected operands.
	
	\param instr_group A group of instructions.
	\param instr_version A parent instruction of the operands.
	\param pi A source pi-pseudoinstruction.
	\param selected_operands A list of operands of preceding instructions.
	\param selected_types A list of datatypes of the selected_operands.
*/
void pi_pi2ge_pi_base::add_selected_operands(ptr<instruction_group_base> instr_group, ptr<tm_instr_base> instr_version, ptr<pi_pi> pi, ptr<ulint2tm_instr_op_base__type> selected_operands,ptr<ulint2ulint__type> selected_types) {
	ptr<pi_operands> operands = pi->accept_visitor_pi_pi2pi_operands_gen_base(pi_operands_getter);
	
	// Add output operands of the selected instruction version to the selected_operands list.  The types of the selected operands add to the selected_types list. 
	for(ulint i=0; i<instr_group->output_operand_pids_get()->size(); ++i) {
		ulint operand_pid = (*instr_group->output_operand_pids_get())[i];
		ulint operand_ref = (*instr_group->output_operand_refs_get())[i];
		
		lassert(operand_pid > PIN_4);
		
		ptr<tm_instr_op_base> op = find_op_by_id(instr_version->operands_output_get(),operand_ref);
		selected_operands->insert(::std::pair<ulint, srp<tm_instr_op_base> >(operand_pid, op));		
		
		ulint type;
		
		if ( operand_pid>=POUT_1 && operand_pid <= POUT_4 ) {
			//The type of the output operand that corresponds to the output operand of pi-pseudoinstruction is type od pi-operand.
			ulint i_op_pos = operand_pid - POUT_1;
			ptr<pi_operand> pi_op = (*operands->operands_output_get())[i_op_pos];
			type = pi_op->type_get()->id_get();
		} else {			
			if ( op->allowed_types_get()->size()==1 ) {
				//Operand has the only one type allowed.
				type = *op->allowed_types_get()->begin();
			} else {
				//Operand has multiple types allowed. Take type of the first input operand.
				lassert(instr_group->input_operand_pids_get()->size()>0);
				ulint iop_pid = (*instr_group->input_operand_pids_get())[0];
				
				ulint2ulint__type::iterator it_t = selected_types->find(iop_pid);
				
				lassert(it_t!=selected_types->end());
				
				type = it_t->second;
			}
		}
		
		selected_types->insert(::std::pair<ulint, ulint>(operand_pid, type));		
	}
}

/*!
	\brief Removes instruction's operands to a list of selected operands.
	
	\param instr_group A group of instructions.
	\param selected_operands A list of operands of preceding instructions.
	\param selected_types A list of datatypes of the selected_operands.
*/
void pi_pi2ge_pi_base::remove_selected_operands(ptr<instruction_group_base> instr_group, ptr<ulint2tm_instr_op_base__type> selected_operands, ptr<ulint2ulint__type> selected_types) {
	
	for(ulint i=0; i<instr_group->output_operand_pids_get()->size(); ++i) {
		ulint operand_pid = (*instr_group->output_operand_pids_get())[i];
		selected_operands->erase(operand_pid);		
		selected_types->erase(operand_pid);		
	}
}

/*!
	\brief Traverses a list of operands and returns an operand with given id.
	
	\param list A list of operands.
	\param id An id of operand.
	\return A operand.
*/
ptr<tm_instr_op_base> pi_pi2ge_pi_base::find_op_by_id(ptr<tm_instr_op_base_vector__type> list, ulint id) {
	for(uint i=0; i<list->size(); ++i) {
		ptr<tm_instr_op_base> op = (*list)[i];
		
		if ( op->id_get()==id ) {
			return op;
		}
	}
	
	return NULL;;
}


end_package(instructions);
end_package(md);
end_package(lestes);

