/*!
	\file
	\brief Base for visitor returning tm_pi corresponding to a pi_pi.
	\author jaz
*/

#include <lestes/backend_v2/intercode/ge.g.hh>
#include <lestes/backend_v2/intercode/pi.g.hh>
#include <lestes/md/instructions/pi_operand2ge_operand_base.g.hh>

package(lestes);
package(md);
package(instructions);

using namespace ::lestes::backend_v2::intercode;

typedef map< srp<pi_operand>, srp<ge_operand> > pi_op2ge_op__type;

/*!
	\brief Converts pi_operand to ge_operand.
	
	It caches results of previous conversion. So if conversion of an already converted pi_operand instance is requested,
	the function returns the same instance of ge_operand class as the last time.
	
	\param pi A pi_operand instance.
	\return A ge_operand instance.
*/
ptr< ::lestes::backend_v2::intercode::ge_operand > pi_operand2ge_operand_base::convert(ptr< ::lestes::backend_v2::intercode::pi_operand > pi) {
	ptr<ge_operand> ge;

	//Has pi-operand been converted to ge-operand?
	pi_op2ge_op__type::iterator it_ge_op = pi_op2ge_op->find(pi);
	
	if ( it_ge_op==pi_op2ge_op->end() ) {
		//No, it hasn't. Convert it.
		ge = pi->accept_visitor_pi_operand2ge_operand_gen_base(this);
		pi_op2ge_op->insert(::std::pair<srp<pi_operand>,srp<ge_operand> >(pi, ge));
	} else {
		//Yes, it has. Use the already converted result.
		ge = it_ge_op->second;	
	}
	
	return ge;
}

end_package(instructions);
end_package(md);
end_package(lestes);

