/*!
	\file
	\brief Type informations..
	\author jaz
*/

#include <lestes/lang/cplus/sem/ss_type.g.hh>
#include <lestes/md/types/ss_type2tm_type.g.hh>
#include <lestes/md/types/tm_data_type_base.g.hh>
#include <lestes/md/types/num_range.g.hh>
#include <lestes/md/types/num_range_getter.g.hh>
#include <lestes/md/types/type_info_base.g.hh>

#include <regex.h>

package(lestes);
package(md);
package(types);

using namespace ::lestes::md::types;
using namespace ::lestes::lang::cplus::sem;

/*!
	\brief Gets ranges of given type.
	
	\param type The type.
	\return Object representing ranges of the type.
*/
ptr<num_range> type_info_base::get_range_for_numeric_type(ptr<ss_type> type) {
	ptr<tm_data_type_base> tm_type = type->accept_ss_type2tm_type_gen_base(ss_type2tm_type::instance());
	return num_range_getter::instance()->get_range(tm_type);
}



/*!
	\brief Converts string representing number with octal/hexa/decimal base to decimal based number.
	
	\param value The number to be converted.
	\param type Type of value.
	\return The converted number. 
*/
lstring type_info_base::get_decimal_representation(ucn_string value, ptr<tm_data_type_base> type) {
	lassert(type);
	::std::ostringstream oss;
	oss << value;
	return oss.str();		
}

end_package(types);
end_package(md);
end_package(lestes);

