/*!
	\file
	\brief sizeof evaluator.
	\author jaz
*/
#include <lestes/md/types/tm_data_type_base.g.hh>
#include <lestes/md/types/ss_type2tm_type_convertor.g.hh>
#include <lestes/lang/cplus/sem/ss_type.g.hh>
#include "ss_type_size_evaluator.g.hh"

package(lestes);
package(md);
package(types);

using ::lestes::lang::cplus::sem::ss_type;
using ::lestes::md::types::tm_data_type_base;

ptr< ss_type_size_evaluator > ss_type_size_evaluator::instance() {
	if ( !singleton_instance_get() ) {
		singleton_instance_set(ss_type_size_evaluator::create());
	}
	return singleton_instance_get();
}

/*!
	\brief Returns sizeof for given ss_type.
	
	\param type The ss_type.
	\return The size of ss_type in bits.
*/
t_size ss_type_size_evaluator::size_get(ptr<ss_type> type) {
	return ss_type2tm_type_convertor::instance()->convert(type)->bitwidth_get();
}

end_package(types);
end_package(md);
end_package(lestes);

