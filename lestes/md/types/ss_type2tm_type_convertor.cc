/*!
	\file
	\brief ss_type to tm_data_type conversion.
	\author jaz
*/
#include <lestes/md/types/tm_data_type_base.g.hh>
#include <lestes/md/types/ss_type2tm_type.g.hh>
#include <lestes/lang/cplus/sem/ss_type.g.hh>
#include "ss_type2tm_type_convertor.g.hh"

package(lestes);
package(md);
package(types);

using ::lestes::md::types::tm_data_type_base;
using ::lestes::lang::cplus::sem::ss_type;

/*!
	\brief Returns instance.
	\return The instance.
*/
ptr< ss_type2tm_type_convertor > ss_type2tm_type_convertor::instance() {
	if ( !singleton_instance_get() ) {
		singleton_instance_set(ss_type2tm_type_convertor::create());
	}
	return singleton_instance_get();
}

/*!
	\brief Converts ss_type to tm_data type.
	\param type The ss_type instance.
	\return tm_data_type object.
*/
ptr<tm_data_type_base> ss_type2tm_type_convertor::convert(ptr<ss_type> type) {
	return type->accept_ss_type2tm_type_gen_base(convertor_get());
}

/*!
	\brief Returns tm_data_type of conditional pseudoregister.
	\return The datatype.
*/
ptr<tm_data_type_base> ss_type2tm_type_convertor::conditional_preg_type_get() {
	return convertor_get()->conditional_preg_type_get();
}

end_package(types);
end_package(md);
end_package(lestes);

