/*
	\file
	\brief Return value passing info.
	\author jaz
*/
#include <lestes/md/types/ss_type_returned_in_reg.g.hh>
#include <lestes/md/types/ss_type2tm_type_convertor.g.hh>
#include <lestes/md/types/tm_data_type_base.g.hh>
#include <lestes/md/registers/tm_register_base.g.hh>
#include <lestes/lang/cplus/sem/ss_type.g.hh>

package(lestes);
package(md);
package(types);

using ::lestes::lang::cplus::sem::ss_type;

ptr< ss_type_returned_in_reg > ss_type_returned_in_reg::instance()
{
	if ( !singleton_instance_get()) {
		singleton_instance_set(ss_type_returned_in_reg::create());
	}	
	
	return singleton_instance_get();
}


/*!
	\brief Tells whether return value of type ss_type is returned from function in pseudoregister or in memory.
	
	\param type The type.
	\return True if the type os returned in register. False otherwise.
*/
bool ss_type_returned_in_reg::type_returned_in_reg(ptr<ss_type> type){
    lassert(type);
    return ss_type2tm_type_convertor::instance()->convert(type)->return_reg_get();
}

end_package(types);
end_package(md);
end_package(lestes);
