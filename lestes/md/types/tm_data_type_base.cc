/*! \file
	\brief Target-machine data-types.
	
	\author jaz
*/

#include <lestes/md/types/tm_data_type_base.g.hh>

package(lestes);
package(md);
package(types);

/*!
	Returns kind of the data type.
	\return Data type kind.
*/
tm_data_type_base::kind_type tm_dt_simple_base::kind_get() {
	return tm_data_type_base::SIMPLE;
}

/*!
	Returns kind of the data type.
	\return Data type kind.
*/
tm_data_type_base::kind_type tm_dt_bitfield_base::kind_get() {
	return tm_data_type_base::BITFIELD;
}


/*!
	Returns kind of the data type.
	\return Data type kind.
*/
tm_data_type_base::kind_type tm_dt_struct_base::kind_get() {
	return tm_data_type_base::STRUCT;
}


/*!
	Returns kind of the data type.
	\return Data type kind.
*/
tm_data_type_base::kind_type tm_dt_union_base::kind_get() {
	return tm_data_type_base::UNION;
}

/*!
	Returns kind of the data type.
	\return Data type kind.
*/
tm_data_type_base::kind_type tm_dt_array_base::kind_get() {
	return tm_data_type_base::ARRAY;
}

end_package(types);
end_package(md);
end_package(lestes);

