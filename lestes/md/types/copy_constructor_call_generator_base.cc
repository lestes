/*!
	\file
	\brief Generator of copy-constructor call.
	\author jaz
*/

#include <lestes/md/types/copy_constructor_call_generator_base.g.hh>
#include <lestes/md/types/tm_data_type_base.g.hh>
#include <lestes/backend_v2/intercode/pi.g.hh>

package(lestes);
package(md);
package(types);

using ::lestes::md::types::tm_data_type_base;
using namespace ::lestes::backend_v2::intercode;

typedef list<srp<pi_pi> > pi_list;


/*!
	\brief Generates list of pseudoinstructions that copies operand to memory.

	Note: types of both parameters have to be the same.
	
	\param preg A memory destination.
	\param lit A literal source.
	\param psp A first boundary sequencepoint.
	\param nsp A second boundary sequencepoint.
	\return A list of pseudoinstructions.
*/
ptr<pi_list> copy_constructor_call_generator_base::generate(ptr< pi_mem > into, ptr< pi_operand > from, ptr<pi_sp> psp, ptr<pi_sp> nsp)
{
	ptr<pi_list> copy_pis = pi_list::create();
	
	switch (from->type_get()->kind_get()) {
		case tm_data_type_base::UNION: {
			lassert2(false,"Not implemented.");		
		} break;
		
		case tm_data_type_base::SIMPLE: {
			lassert(from->kind_get()!=pi_operand::LIT);
			
			/*
				Simple datatype. It is copied by pseudoinstructions. 
			*/
			
			//Compute level = max(psp->level,nsp->level,from->origin_get()->level)
			ulint level = (psp->level_get() > nsp->level_get() ? psp->level_get() : nsp->level_get());
			
			if ( from->origin_get() ) {
				level = level > from->origin_get()->level_get() ? level : from->origin_get()->level_get();			
			}
			
			level++;
			
			if ( from->kind_get()!=pi_operand::PREG && from->kind_get()!=pi_operand::MEM_PREG ) {
				/*
					Source operand is NOT register - it is mem. It has to be loaded to a register first.
				*/
				
				// Create tmp register.
				ptr<pi_preg> tmp = pi_preg::create(NULL,from->type_get());
				
				//Load data from memory to the tmp register.
				ptr<pi_ld> ld = pi_ld::create(psp, nsp, level, tmp, from, from->type_get());	
				tmp->origin_set(ld);
				copy_pis->push_back(ld);
				
				//Set tmp register as source operand.
				from = tmp;
			}
			
			//Copy source operand (register) to the destination operand (memory).
			ptr<pi_st> st = pi_st::create(psp, nsp, level, into, from, from->type_get());	
			into->origin_set(st);
			copy_pis->push_back(st);
		} break;
		
		case tm_data_type_base::STRUCT: {
			lassert2(false,"Not implemented.");		
		} break;
		
		default: lassert(false);
	}
	
	return copy_pis;
}


end_package(types);
end_package(md);
end_package(lestes);

