/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
 * Overload resolution for operators is needed to determine if builtin or 
 * non-builtin operator will be done. After selecting best function the resulting
 * best function call creating functional is applied to yield the resulting ss_expression
 *
 * This file contains implementation of handling all builtin operators.
 *
 * \author tma
 * \author jikos
 */

#include <lestes/lang/cplus/sem/or_or.g.hh>
#include <lestes/lang/cplus/sem/visitor.v.g.hh>
#include <lestes/lang/cplus/sem/ss_decl_name.g.hh>
#include <lestes/lang/cplus/sem/sa_deconstruct_spse.g.hh>
#include <lestes/lang/cplus/sem/or_ics.g.hh>
#include <lestes/lang/cplus/sem/or_ics_actual_visitors.g.hh>
#include <lestes/lang/cplus/sem/or_ics.hh>
#include <lestes/std/source_location.hh>
#include <lestes/lang/cplus/sem/ss_expr_binary_op.g.hh>
#include <lestes/lang/cplus/sem/sa_deconstruct_spse.m.hh>
#include <lestes/lang/cplus/sem/as_expr.g.hh>
#include <lestes/std/source_location.hh>
#include <lestes/lang/cplus/sem/as_id_to_declaration_set.g.hh>
#include <lestes/lang/cplus/sem/as_decl.g.hh>
#include <lestes/lang/cplus/sem/sa_statements.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>
#include <lestes/lang/cplus/sem/or_actual_visitors.g.hh>
#include <lestes/lang/cplus/sem/ss_literal_info.g.hh>
#include <lestes/lang/cplus/lex/cpp_token.hh>
#include <lestes/lang/cplus/sem/lex_literal_to_ss_literal_info.g.hh>
#include <lestes/lang/cplus/sem/sa_declarator_type.g.hh>
#include <lestes/lang/cplus/sem/sa_declaration_specifier_list.g.hh>
#include <lestes/lang/cplus/sem/sa_declaration_specifiers.g.hh>
#include <lestes/md/types/ss_type_size_evaluator.g.hh>
#include <lestes/md/types/type_info.g.hh>
#include <algorithm>
#include <iterator>

package(lestes);
package(lang);
package(cplus);
package(sem);

using namespace ::lestes::md::types;

typedef ::lestes::std::list< srp< or_or_functional > > exprlist;
typedef ::lestes::std::set< srp< ss_function_declaration > > func_decl_set;

ptr< or_or_functional > overload_resolution (ptr< exprlist > exprs, ptr< ss_operator > func_name);
ptr< or_or_functional > overload_resolution (ptr< exprlist > exprs, ptr< func_decl_set > candidates);

/*!
 * This template convers AS expressions/operators, having the for of binary op
 *
 * It doesn't matter here if creation of new sequence point is needed ('&&', '||', ',') or not.
 * This is done later, when creating the actual ss_expression, in operator() of the creator.
 * This also covers some op + equal operators. (which has sideeffect, this is also handled later, 
 * in the creator)
 *
 * \param AS visited as expression (concrete type)
 * \param OP determines the operator type for as function id
 */
template <typename AS, typename OP> void sa_deconstruct_spse::construct_bin_op(ptr< AS > as)
{
	ptr< sa_deconstruct_spse > vl = sa_deconstruct_spse::create();
	ptr< sa_deconstruct_spse > vr = sa_deconstruct_spse::create();
	ptr< or_or_functional > l, r;
	ptr< ss_operator > op_name; 
	ptr< ::lestes::std::source_location > loc = as->location_get();
	ptr< ::lestes::std::list< srp< or_or_functional > > > expr_list = ::lestes::std::list< srp< or_or_functional > >::create();

	/* recursively on arguments */
	as->left_get()->accept_as_expr_visitor(vl);
	l = vl->result_get();
	
	as->right_get()->accept_as_expr_visitor(vr);
	r = vr->result_get();

	expr_list->push_back(l);
	expr_list->push_back(r);

	/* perform overload resolution on the name of the op and return the obtained creator */	
	op_name = OP::create(loc);

	/* this returns the creator of the expression */
	result_set(overload_resolution(expr_list, op_name));
	if(!(result_get())) {
		report << no_viable_found << loc;
		exit(1);
	}
}

/*!
 * This template covers operators which have form of unuary operator.
 * 
 * This includes handling not only prefix and postfix {in|de}rement, but also
 * handling unary plus and minus (which are handled in the same way as _prefix_
 * operators). 
 * 
 * Prefix and postfix themselves differ only in the number of parameters (see [13.5.7]
 * for more detailed explanation). In short: in the postfix case, we have to create the
 * second parameter as literal manually (postfix operators have only the left operand), 
 * in opposition to prefix situation, which is having just one parameter (the left one).
 *
 * AS - visited as expression (concrete type)
 * OP - determines the operator type for as function id
 */
template <typename AS, typename OP> void sa_deconstruct_spse::construct_unary_op_nocreate(ptr< AS > as)
{
	ptr< sa_deconstruct_spse > vl = sa_deconstruct_spse::create();
	ptr< or_or_functional > l, e;
	ptr< ::lestes::std::source_location > loc = as->location_get();
	ptr< ss_operator > op_name; 
	ptr< ::lestes::std::list< srp< or_or_functional > > > expr_list = ::lestes::std::list< srp< or_or_functional > >::create();
	
	as->expr_get()->accept_as_expr_visitor(vl);
	/* result of the visitor stored in the result field */
	l = vl->result_get();

	expr_list->push_back(l);

	/* perform overload resolution on the name of the op */
	op_name = OP::create(loc);

	/* this returns the creator of the expression */
	result_set(overload_resolution(expr_list, op_name));
	if(!(result_get())) {
		report << no_viable_found << loc;
		exit(1);
	}
}
template <typename AS, typename OP> void sa_deconstruct_spse::construct_unary_op_create(ptr< AS > as)
{
	ptr< sa_deconstruct_spse > vl = sa_deconstruct_spse::create();
	ptr< or_or_functional > l, r;
	ptr< ::lestes::std::source_location > loc = as->location_get();
	ptr< ss_operator > op_name; 
	ptr< ::lestes::std::list< srp< or_or_functional > > > expr_list = ::lestes::std::list< srp< or_or_functional > >::create();
	
	as->expr_get()->accept_as_expr_visitor(vl);

	l = vl->result_get();
	/* create the literal representing int 1 */
	r = or_or_functional_literal::create(ss_type_sint::instance(), ss_integral_literal_info::create_from_number(ss_type_sint::instance(), 1));

	expr_list->push_back(l);
	expr_list->push_back(r);

	/* perform overload resolution on the name of the op */
	op_name = OP::create(loc);

	/* this returns the creator of the expression */
	result_set(overload_resolution(expr_list, op_name));
	if(!(result_get())) {
		report << no_viable_found << loc;
		exit(1);
	}
}

/*!
 * This function handles the case of '=' as expression. There is need to create new 
 * sideeffect, which will be handled later in the operator() of the creator. So, for 
 * the deconstruct itself, the equal is basically the same as any other binary op. This
 * is left here just not to forget for the new SE.
 */
void sa_deconstruct_spse::visit_as_expression_equal( ptr< as_expression_equal > as)
{
	ptr< sa_deconstruct_spse > v = sa_deconstruct_spse::create();;
	ptr< ::lestes::std::list< srp< or_or_functional > > > expr_list = ::lestes::std::list< srp< or_or_functional > >::create();
	ptr< ::lestes::std::source_location > loc = as->location_get();
	ptr< or_or_functional > l, r;
	ptr< ss_operator > op_name;	

	as->left_get()->accept_as_expr_visitor(v);
	/* result of the visitor stored in the result field */
	l = v->result_get();
	as->right_get()->accept_as_expr_visitor(v);
	r = v->result_get();

	expr_list->push_back(l);
	expr_list->push_back(r);

	/* perform overload resolution on the name of the op */
	op_name = ss_operator_assign::create(loc);
	
	/* the result of the op will be l, which will be set later, in the creator */
	result_set(overload_resolution(expr_list, op_name));
	if(!(result_get())) {
		report << no_viable_found << loc;
		exit(1);
	}
}

void sa_deconstruct_spse::visit_as_expression_comma( ptr< as_expression_comma > as)
{
	ptr< sa_deconstruct_spse > vl = sa_deconstruct_spse::create();
	ptr< sa_deconstruct_spse > vr = sa_deconstruct_spse::create();
	ptr< or_or_functional > l, r;
	ptr< ss_operator > op_name; 
	ptr< ::lestes::std::source_location > loc = as->location_get();
	ptr< ::lestes::std::list< srp< or_or_functional > > > expr_list = ::lestes::std::list< srp< or_or_functional > >::create();

	/* recursively on arguments */
	as->left_get()->accept_as_expr_visitor(vl);
	l = vl->result_get();
	
	as->right_get()->accept_as_expr_visitor(vr);
	r = vr->result_get();

	expr_list->push_back(l);
	expr_list->push_back(r);

	/* perform overload resolution on the name of the op and return the obtained creator */	
	op_name = ss_operator_comma::create(loc);

	/* this returns the creator of the expression */
	result_set(overload_resolution(expr_list, op_name));
	if(!(result_get())) {
		/* there was no either builtin ([13.3.1.2/3]) or user-defined operator, */
		result_set(or_or_functional_comma::create(r->type_get(), expr_list));
	}
}

void sa_deconstruct_spse::visit_as_empty_expression( ptr< as_empty_expression > )
{
	/* there is no such thing */
	lassert2(false, "BUG: hit empty expression in expression transformation");
}
void sa_deconstruct_spse::visit_as_expression_plus_equal( ptr< as_expression_plus_equal > as )
{
	construct_bin_op<as_expression_plus_equal, ss_operator_assign_add>(as);
}
void sa_deconstruct_spse::visit_as_expression_minus_equal( ptr< as_expression_minus_equal > as )
{
	construct_bin_op<as_expression_minus_equal, ss_operator_assign_sub>(as);
}
void sa_deconstruct_spse::visit_as_expression_star_equal( ptr< as_expression_star_equal > as )
{
	construct_bin_op<as_expression_star_equal, ss_operator_assign_mul>(as);
}
void sa_deconstruct_spse::visit_as_expression_slash_equal( ptr< as_expression_slash_equal > as )
{
	construct_bin_op<as_expression_slash_equal, ss_operator_assign_div>(as);
}
void sa_deconstruct_spse::visit_as_expression_percent_equal( ptr< as_expression_percent_equal > as )
{
	construct_bin_op<as_expression_percent_equal, ss_operator_assign_mod>(as);
}
void sa_deconstruct_spse::visit_as_expression_hat_equal( ptr< as_expression_hat_equal > as )
{
	construct_bin_op<as_expression_hat_equal, ss_operator_assign_bxor>(as);
}
void sa_deconstruct_spse::visit_as_expression_amp_equal( ptr< as_expression_amp_equal > as )
{
	construct_bin_op<as_expression_amp_equal, ss_operator_assign_band>(as);
}
void sa_deconstruct_spse::visit_as_expression_vbar_equal( ptr< as_expression_vbar_equal > as )
{
	construct_bin_op<as_expression_vbar_equal, ss_operator_assign_bor>(as);
}
void sa_deconstruct_spse::visit_as_expression_less_less_equal( ptr< as_expression_less_less_equal > as )
{
	construct_bin_op<as_expression_less_less_equal, ss_operator_assign_shl>(as);
}
void sa_deconstruct_spse::visit_as_expression_greater_greater_equal( ptr< as_expression_greater_greater_equal > as )
{
	construct_bin_op<as_expression_greater_greater_equal, ss_operator_assign_shr>(as);
}
void sa_deconstruct_spse::visit_as_expression_equal_equal( ptr< as_expression_equal_equal > as )
{
	construct_bin_op<as_expression_equal_equal, ss_operator_sbe>(as);
}
void sa_deconstruct_spse::visit_as_expression_exclam_equal( ptr< as_expression_exclam_equal > as )
{
	construct_bin_op<as_expression_exclam_equal, ss_operator_sbne>(as);
}
void sa_deconstruct_spse::visit_as_expression_less_equal( ptr< as_expression_less_equal > as )
{
	construct_bin_op<as_expression_less_equal, ss_operator_sbng>(as);
}
void sa_deconstruct_spse::visit_as_expression_greater_equal( ptr< as_expression_greater_equal > as )
{
	construct_bin_op<as_expression_greater_equal, ss_operator_sbnl>(as);
}
void sa_deconstruct_spse::visit_as_expression_less( ptr< as_expression_less > as )
{
	construct_bin_op<as_expression_less, ss_operator_sbl>(as);
}
void sa_deconstruct_spse::visit_as_expression_greater( ptr< as_expression_greater > as )
{
	construct_bin_op<as_expression_greater, ss_operator_sbg>(as);
}
void sa_deconstruct_spse::visit_as_expression_vbar( ptr< as_expression_vbar > as )
{
	construct_bin_op<as_expression_vbar, ss_operator_bor>(as);
}
void sa_deconstruct_spse::visit_as_expression_amp( ptr< as_expression_amp > as )
{
	construct_bin_op<as_expression_amp, ss_operator_band>(as);
}
void sa_deconstruct_spse::visit_as_expression_hat( ptr< as_expression_hat > as )
{
	construct_bin_op<as_expression_hat, ss_operator_bxor>(as);
}
void sa_deconstruct_spse::visit_as_expression_less_less( ptr< as_expression_less_less > as )
{
	construct_bin_op<as_expression_less_less, ss_operator_shl>(as);
}
void sa_deconstruct_spse::visit_as_expression_greater_greater( ptr< as_expression_greater_greater > as )
{
	construct_bin_op<as_expression_greater_greater, ss_operator_shr>(as);
}
void sa_deconstruct_spse::visit_as_expression_plus( ptr< as_expression_plus > as )
{
	construct_bin_op<as_expression_plus, ss_operator_add>(as);
}
void sa_deconstruct_spse::visit_as_expression_minus( ptr< as_expression_minus > as )
{
        construct_bin_op<as_expression_minus, ss_operator_sub>(as);
}
void sa_deconstruct_spse::visit_as_expression_star( ptr< as_expression_star > as )
{
	construct_bin_op<as_expression_star, ss_operator_mul>(as);
}
void sa_deconstruct_spse::visit_as_expression_slash( ptr< as_expression_slash > as )
{
	construct_bin_op<as_expression_slash, ss_operator_div>(as);
}
void sa_deconstruct_spse::visit_as_expression_percent( ptr< as_expression_percent > as )
{
	construct_bin_op<as_expression_percent, ss_operator_mod>(as);
}
void sa_deconstruct_spse::visit_as_expression_vbar_vbar( ptr< as_expression_vbar_vbar > as )
{
	construct_bin_op<as_expression_vbar_vbar, ss_operator_lor>(as);
}
void sa_deconstruct_spse::visit_as_expression_amp_amp( ptr< as_expression_amp_amp > as )
{
	construct_bin_op<as_expression_amp_amp, ss_operator_land>(as);
}
void sa_deconstruct_spse::visit_as_expression_dot_star( ptr< as_expression_dot_star >)
{
	lassert2(false, ".* expression not supported yet\n");
}
void sa_deconstruct_spse::visit_as_expression_minus_greater_star( ptr< as_expression_minus_greater_star > as )
{
	construct_bin_op<as_expression_minus_greater_star, ss_operator_access_member>(as);
}
void sa_deconstruct_spse::visit_as_expression_brackets( ptr< as_expression_brackets > )
{
	lassert2(false, "as_expression_brackets\n");
	//construct_bin_op<as_expression_brackets, ss_operator_function_call>(as);
}
void sa_deconstruct_spse::visit_as_expression_plus_plus_pre( ptr< as_expression_plus_plus_pre > as )
{
	construct_unary_op_nocreate<as_expression_plus_plus_pre, ss_operator_inc>(as);
}
void sa_deconstruct_spse::visit_as_expression_plus_plus_post( ptr< as_expression_plus_plus_post > as )
{
	construct_unary_op_create<as_expression_plus_plus_post, ss_operator_inc>(as);
}
void sa_deconstruct_spse::visit_as_expression_minus_minus_pre( ptr< as_expression_minus_minus_pre > as )
{
	construct_unary_op_nocreate<as_expression_minus_minus_pre, ss_operator_dec>(as);
}
void sa_deconstruct_spse::visit_as_expression_minus_minus_post( ptr< as_expression_minus_minus_post > as )
{
	construct_unary_op_create<as_expression_minus_minus_post, ss_operator_dec>(as);
}
/* The following unary operators can be handled in the very same way as prefix in/decrement,
 * because they have one parameter and sideffect doesn't matter here
 */ 
void sa_deconstruct_spse::visit_as_expression_unary_amp( ptr< as_expression_unary_amp > as )
{
	/* this is not binary and, but the ss_operator is the same (it is not distinguishable
	 * here, whether we hold binary (bit) and, or address_of operator - it can be 
	 * distinguished later according to number of arguments
	 */
	ptr< sa_deconstruct_spse > vl = sa_deconstruct_spse::create();
	ptr< or_or_functional > l;
	ptr< ss_operator > op_name; 
	ptr< ::lestes::std::source_location > loc = as->location_get();
	ptr< ::lestes::std::list< srp< or_or_functional > > > expr_list = ::lestes::std::list< srp< or_or_functional > >::create();

	/* recursively on arguments */
	as->expr_get()->accept_as_expr_visitor(vl);
	l = vl->result_get();
	
	expr_list->push_back(l);

	/* perform overload resolution on the name of the op and return the obtained creator */	
	op_name = ss_operator_band::create(loc);

	/* this returns the creator of the expression */
	result_set(overload_resolution(expr_list, op_name));
	if(!(result_get())) {
		/* there was no either builtin ([13.3.1.2/3]) or user-defined operator, */
		/* The the type of l must be de-referenced and de-pseudoreferenced and then pointerized, so that for example
		 *
		 *	int *;
		 * 	int x;
		 * 	i = &x;
		 *
		 * will work (the candidate function in this case for assignment is taking two
		 * int* arguments
		 */
	        ptr< or_ics_visitor_cv > v = or_ics_visitor_cv::create();
		/* pseudoreference for reference is possible
		 * pseudoreference for pseudoreference is strange thing, not handled FIXME
		 * reference for (pseudo)reference is not supported
		 */
	        if(l->type_get()->accept_or_ics_base_cv(v) == OR_CV_PSEUDOREFERENCE) {
			if(l->type_get().dncast<ss_pseudoreference>()->what_get()->accept_or_ics_base_cv(v) == OR_CV_REFERENCE)
				result_set(or_or_functional_addrof::create(ss_pointer::instance(l->type_get().dncast<ss_pseudoreference>()->what_get().dncast<ss_reference>()->what_get()), expr_list));
			else
				result_set(or_or_functional_addrof::create(ss_pointer::instance(l->type_get().dncast<ss_pseudoreference>()->what_get()), expr_list));
		} else if(l->type_get()->accept_or_ics_base_cv(v) == OR_CV_REFERENCE) {
			result_set(or_or_functional_addrof::create(ss_pointer::instance(l->type_get().dncast<ss_reference>()->what_get()), expr_list));
		} else {
			report << non_lvalue_in_unary_amp << loc;
			exit(1);
		}
	}

}
void sa_deconstruct_spse::visit_as_expression_unary_plus( ptr< as_expression_unary_plus > as )
{
	construct_unary_op_nocreate<as_expression_unary_plus, ss_operator_add>(as);
}
void sa_deconstruct_spse::visit_as_expression_unary_minus( ptr< as_expression_unary_minus > as )
{
	construct_unary_op_nocreate<as_expression_unary_minus, ss_operator_sub>(as);
}
void sa_deconstruct_spse::visit_as_expression_unary_star( ptr< as_expression_unary_star > as )
{
	construct_unary_op_nocreate<as_expression_unary_star, ss_operator_mul>(as);
}
void sa_deconstruct_spse::visit_as_expression_tilde( ptr< as_expression_tilde > as )
{
	construct_unary_op_nocreate<as_expression_tilde, ss_operator_bnot>(as);
}
void sa_deconstruct_spse::visit_as_expression_exclam( ptr< as_expression_exclam > as )
{
	construct_unary_op_nocreate<as_expression_exclam, ss_operator_lnot>(as);
}
/* It is enough for now to handle it whis way */
void sa_deconstruct_spse::visit_as_constant_expression( ptr< as_constant_expression > as)
{
	/* this is enough for now, but should be later FIXME */
	ptr< sa_deconstruct_spse > v = sa_deconstruct_spse::create();
	as->expr_get()->accept_as_expr_visitor(v);
	result_set(v->result_get());
	
}
/*!
 * This function handles the case of function call expression
 */
void sa_deconstruct_spse::visit_as_expression_function_call( ptr< as_expression_function_call > as )
{
	ptr< or_or_functional > l, pref_res;
	ptr< as_expression > pref = as->prefix_get();
	ptr< ::lestes::std::source_location > loc = as->location_get();
	ptr< ::lestes::std::list< srp< or_or_functional > > > expr_list = ::lestes::std::list< srp< or_or_functional > >::create();
	::lestes::std::list< srp< as_expression > >::iterator it = as->arguments_get()->begin();

	/* recursion on the arguments */	
	for(; it != as->arguments_get()->end(); *it++) {
		ptr< sa_deconstruct_spse > v = sa_deconstruct_spse::create();

		(*it)->accept_as_expr_visitor(v);
		l = v->result_get();
		expr_list->push_back(l);
	}
	
	/* on the prefix */
	ptr< sa_deconstruct_spse > v = sa_deconstruct_spse::create();
	pref->accept_as_expr_visitor(v);
	pref_res = v->result_get();

	/* let's see what is the type of the returned or_or_functional */
	ptr< or_or_functional_to_enum > v_f = or_or_functional_to_enum::create();
	pref_res->accept_or_or_functional_visitor(v_f);
	or_or_functional_enum e = v_f->result_get();
	
	/* the first two cases mean that prefix is as_name */
	if (e == OR_OR_FUNCTIONAL_FUNC_DECL_SET) {
		/* "unpack" the declarations and let the overload resolution (by it's second interface)
		 * choose the right function
		 */
		ptr< ::lestes::std::set< srp< ss_function_declaration > > > fl = ::lestes::std::set< srp< ss_function_declaration > >::create();
		ptr< ::lestes::std::set< srp< ss_declaration > > > dl = pref_res.dncast<or_or_functional_func_decl_set>()->declarations_get();
		::lestes::std::set< srp< ss_declaration > >::iterator it = dl->begin();
		
		for (; it != dl->end(); it++)
			fl->insert((*it).dncast<ss_function_declaration>());
		result_set(overload_resolution(expr_list, fl));
	} else if (e == OR_OR_FUNCTIONAL_DECL) {
		/* in case of object declaration - lookup the operator() in it
		 * in case of function declaration - create or_or_functional representing functionl call expr
		 * in case of typename we are in functional-style-cast situation. This is TODO for someone in future
		 * ...
		 */
		ptr< ss_decl_to_enum > v_d = ss_decl_to_enum::create();
		ss_decl_enum e_d;
		
		pref_res.dncast<or_or_functional_decl>()->declaration_get()->accept_ss_declaration_visitor(v_d);
		e_d = v_d->result_get();

		if (e_d == FUNCTION_DECLARATION) {
			/* this is basically the same situation as in OR_OR_FUNCTIONAL_FUNC_DECL_SET case */
			result_set(or_or_functional_concrete::create(pref_res.dncast<or_or_functional_decl>()->declaration_get()->type_get().dncast<ss_function>()->returns_get(),
						expr_list, pref_res.dncast<or_or_functional_decl>()->declaration_get()));
		} else if (e_d == OBJECT_DECLARATION || e_d == STRUCTURE_DECLARATION) {
			/* lookup operator(), chose the proper one by overload and create funcall */
		} else if (e_d == NAMESPACE_DEFINITION || e_d == USING_DECLARATION) {
			/* this definitely shouldn't happen, as the test is already in or_or_functional_decl::operator() */
			lassert2(false, "Impossible thing occured, the test should be already done");
		} else if (e_d == METHOD_DECLARATION) {
			/* create mfuncall */
			lassert2(false, "Method declaration in operator (). To be implemented");
		} else if (e_d == BUILTIN_OPERATOR_DECLARATION) {
			/* can this happen? */
			lassert2(false, "This shouldn't happen, we can't distinguish between builtin and non-builtin here");
		} else {
			lassert2(false, "Unimplemented or impossible function call prefix hit");
		}
		
	}
	/* this means that prefix is expression */
	else if (e == OR_OR_FUNCTIONAL_CONCRETE) {
		/* the prefix is expression. quite a lot of cases to handle ... */
	}
	/* prefix is ambiguous */
	else if (e == OR_OR_FUNCTIONAL_AMBIGUOUS) {
		/* FIXME do not lassert, report using lmd */
		report << ambiguous_function_prefix << as->location_get();
		exit(1);;
	}
}
void sa_deconstruct_spse::visit_as_name_expression( ptr< as_name_expression > as)
{
	/* use tma's visitor on as_id (as_name->identifier_get()). This returns (set of)
	 * declaration(s). If the declaration is typename, it is an error (-> lassert/lmd).
	 * If the declaration is declration of object/variable, create pseudoreference for this.
	 * If the set contains declarations of functions, just create functional which will hold the
	 * set of functions for later use.
	 */
	ptr< as_id_to_declaration_set > v = as_id_to_declaration_set::instance();
	ptr< ::lestes::std::set< srp< ss_declaration > > > decls;
	decls = v->process(as->name_get()->identifier_get());
	::lestes::std::set< srp< ss_declaration > >::iterator it = decls->begin();

	if (!decls || decls->size() == 0) {
		report << unrecognized_identifier << as->location_get();
		exit(1);
	}
	
	/* now check if we received typename, declaration of variable or declaration of function */
	if (decls->size() > 1) {
		/* this is the case that we can be (I hope :) ) sure that what we got is list of functions.
		 * successor of or_or_functional, containing the list of function declaration has to be created
		 */
		ptr< or_or_functional_func_decl_set > funcial = or_or_functional_func_decl_set::create((*it)->type_get(), decls);
		result_set(funcial);
	} else {
		/* here we have just to create the appropriate functional which will later be used to create
	   	 * ss_expression according to obtained declaration
      		 */
		result_set(or_or_functional_decl::create(ss_pseudoreference::instance((*it)->type_get()), (*it)));
	}
}
/*! 
 * This handles the case of literal constant. Only integral constant is now
 * supported, adding other should be straightforward, as the type can be
 * obtained from ss_literal_info::type_get()
 * \bug doesn't support other than integral literals so far
 */
  
void sa_deconstruct_spse::visit_as_literal( ptr< as_literal > as)
{
	ptr< ss_literal_info > info = lex_literal_to_ss_literal_info::process_token(as->value_get());
	result_set(or_or_functional_literal::create(info->type_get(), info));
}

void sa_deconstruct_spse::visit_as_this_expression( ptr< as_this_expression >)
{
	result_set(or_or_functional_this::create(
				sa_statements::instance()->current_function_get()->type_get().dncast<ss_member_function>()->this_type_get()));
}

void sa_deconstruct_spse::visit_as_expression_pseudo_destruct_dot( ptr< as_expression_pseudo_destruct_dot > as)
{
	ptr< sa_deconstruct_spse > vl = sa_deconstruct_spse::create();
	ptr< or_or_functional > l;

	/* recursively on argument */
	as->prefix_get()->accept_as_expr_visitor(vl);
	l = vl->result_get();

	/* nothing else (maybe some check) can be done here */
	result_set(l);
}
void sa_deconstruct_spse::visit_as_expression_pseudo_destruct_arrow( ptr< as_expression_pseudo_destruct_arrow > as)
{
	ptr< sa_deconstruct_spse > vl = sa_deconstruct_spse::create();
	ptr< or_or_functional > l;

	/* recursively on argument */
	as->prefix_get()->accept_as_expr_visitor(vl);
	l = vl->result_get();

	/* nothing else (maybe some check) can be done here */
	result_set(l);
}

/*!
 * This is helper function for the sizeof type for converting list of type specifiers
 * to declaration specifiers
 */
ptr< ::lestes::std::list< srp<as_declaration_specifier> > > ts2ds(ptr< ::lestes::std::list< srp<as_type_specifier> > > tsl)
{
	ptr< list< srp<as_declaration_specifier> > > dsl = ::lestes::std::list< srp<as_declaration_specifier> >::create();
	::std::copy(tsl->begin(),tsl->end(),::std::back_inserter(*dsl));
	return dsl;
}
	
/*! 
 * This function handles sizeof(type)
 */
void sa_deconstruct_spse::visit_as_expression_sizeof_type( ptr< as_expression_sizeof_type > a)
{
	/* first, get the real size from ss_type_size_evaluator and the create
	 * and return the corresponding literal. 
	 */
	
	ptr< as_type_id > as = a->type_id_get();
	ptr< ss_type > t = sa_declarator_type::create()->process(sa_declaration_specifier_list::create()->process(
            a->location_get(),ts2ds(as->type_specifiers_get()))->type_get(),
			as->declarator_get());

	ptr < ss_type > size_t_type_rep = 
		type_info::instance()->get_size_t_type();
		 
	result_set(or_or_functional_literal::create(
				size_t_type_rep,
				ss_integral_literal_info::create_from_number(
					size_t_type_rep,
					ss_type_size_evaluator::instance()->size_get(t)/8)));

}
void sa_deconstruct_spse::visit_as_expression_sizeof_expr( ptr< as_expression_sizeof_expr > a)
{
	ptr< sa_deconstruct_spse > vl = sa_deconstruct_spse::create();
	ptr< or_or_functional > l;

	/* recursively on arguments */
	a->expr_get()->accept_as_expr_visitor(vl);
	l = vl->result_get();
	
	ptr < ss_type > size_t_type_rep = 
		type_info::instance()->get_size_t_type();

	result_set(or_or_functional_literal::create(
				size_t_type_rep,
				ss_integral_literal_info::create_from_number(
					size_t_type_rep,
					ss_type_size_evaluator::instance()->size_get(l->type_get())/8)));;
}
void sa_deconstruct_spse::visit_as_expression_typeid_expr( ptr< as_expression_typeid_expr >)
{
	lassert2(false, "typeid expression not implemented yet\n");
}
void sa_deconstruct_spse::visit_as_expression_delete_base( ptr< as_expression_delete_base >)
{
	lassert2(false, "delete base not implemented yet\n");
}
void sa_deconstruct_spse::visit_as_expression_delete( ptr< as_expression_delete >)
{
	lassert2(false, "delete not implemented yet\n");
}
void sa_deconstruct_spse::visit_as_expression_delete_array( ptr< as_expression_delete_array >)
{
	lassert2(false, "delete array not implemented yet\n");
}
void sa_deconstruct_spse::visit_as_expression_throw( ptr< as_expression_throw >)
{
	lassert2(false, "throw not implemented yet\n");
}
void sa_deconstruct_spse::visit_as_expression_typeid_type( ptr< as_expression_typeid_type >)
{
	lassert2(false, "typeid_type not implemented yet\n");
}
void sa_deconstruct_spse::visit_as_expression_new( ptr< as_expression_new >)
{
	lassert2(false, "new not implemented yet\n");
}
void sa_deconstruct_spse::visit_as_expression_reinterpret_cast( ptr< as_expression_reinterpret_cast >)
{
	lassert2(false, "reinterpret cast not implemented yet\n");
}
void sa_deconstruct_spse::visit_as_expression_dynamic_cast( ptr< as_expression_dynamic_cast >)
{
	lassert2(false, "dynamic cast not implemented yet\n");
}
void sa_deconstruct_spse::visit_as_expression_static_cast( ptr< as_expression_static_cast >)
{
	lassert2(false, "static cast not implemented yet\n");
}
void sa_deconstruct_spse::visit_as_expression_const_cast( ptr< as_expression_const_cast >)
{
	lassert2(false, "const cast not implemented yet\n");
}
void sa_deconstruct_spse::visit_as_expression_old_style_cast( ptr< as_expression_old_style_cast >)
{
	lassert2(false, "old-style cast not implemented yet\n");
}
void sa_deconstruct_spse::visit_as_expression_functional_style_cast( ptr< as_expression_functional_style_cast >)
{
	lassert2(false, "functional-style cast not implemented yet\n");
}
void sa_deconstruct_spse::visit_as_expression_member_access_dot( ptr< as_expression_member_access_dot >)
{
	lassert2(false, "member access not implemented yet\n");
}
void sa_deconstruct_spse::visit_as_expression_member_access_arrow( ptr< as_expression_member_access_arrow >)
{
	lassert2(false, "member access not implemented yet\n");
}
void sa_deconstruct_spse::visit_as_expression_qmark( ptr< as_expression_qmark >)
{
	lassert2(false, "qmark not implemented yet\n");
}

/* the following contains visitor converting as_declaration to enum. This is used to determine if
 * we are holding the type declaration, variable declaration or function declaration (in 
 * sa_deconstruct_spse::visit_as_name above
 */

void ss_decl_to_enum::visit_ss_injected_class_declaration( ptr< ss_injected_class_declaration >)
{
	result_set(INJECTED_CLASS_DECLARATION);
}

void ss_decl_to_enum::visit_ss_structure_declaration( ptr< ss_structure_declaration >)
{
	result_set(STRUCTURE_DECLARATION);
}

void ss_decl_to_enum::visit_ss_namespace_definition( ptr< ss_namespace_definition >)
{
	result_set(NAMESPACE_DEFINITION);;
}

void ss_decl_to_enum::visit_ss_object_declaration( ptr< ss_object_declaration >)
{
	result_set(OBJECT_DECLARATION);
}

void ss_decl_to_enum::visit_ss_bitfield_declaration( ptr< ss_bitfield_declaration >)
{
	result_set(BITFIELD_DECLARATION);
}

void ss_decl_to_enum::visit_ss_parameter_declaration( ptr< ss_parameter_declaration >)
{
	result_set(PARAMETER_DECLARATION);
}

void ss_decl_to_enum::visit_ss_enumerator_declaration( ptr< ss_enumerator_declaration >)
{
	result_set(ENUMERATOR_DECLARATION);
}

void ss_decl_to_enum::visit_ss_enum_definition( ptr< ss_enum_definition >)
{
	result_set(ENUM_DEFINITION);
}

void ss_decl_to_enum::visit_ss_typedef_definition( ptr< ss_typedef_definition >)
{
	result_set(TYPEDEF_DEFINITION);
}

void ss_decl_to_enum::visit_ss_using_declaration( ptr< ss_using_declaration >)
{
	result_set(USING_DECLARATION);
}

void ss_decl_to_enum::visit_ss_compound_stmt_declaration( ptr< ss_compound_stmt_declaration >)
{
	result_set(COMPOUND_STMT_DECLARATION);
}
void ss_decl_to_enum::visit_ss_function_declaration( ptr< ss_function_declaration >)
{
	result_set(FUNCTION_DECLARATION);
}
void ss_decl_to_enum::visit_ss_method_declaration( ptr< ss_method_declaration >)
{
	result_set(METHOD_DECLARATION);
}
void ss_decl_to_enum::visit_ss_builtin_operator_declaration( ptr< ss_builtin_operator_declaration >)
{
	result_set(BUILTIN_OPERATOR_DECLARATION);
}
void ss_decl_to_enum::visit_ss_fake_declaration( ptr< ss_fake_declaration >)
{
	result_set(FAKE_DECLARATION);
}


end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);

