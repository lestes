/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/lang/cplus/sem/ss_type.g.hh>
#include <lestes/lang/cplus/sem/ss_misc.g.hh>
#include <lestes/lang/cplus/sem/ss_expression.g.hh>
#include <lestes/lang/cplus/sem/ss_decl_name.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>
#include <lestes/lang/cplus/sem/ss_statement.g.hh>
#include <lestes/lang/cplus/sem/ss_enums.g.hh>
#include <lestes/lang/cplus/sem/sa_decl_seq_compound_pair_creator.g.hh>
#include <lestes/std/pair.hh>

#include <lestes/std/dumper.hh>
#include <fstream>

/*! \file
  \author TMA
 */

package(lestes);
package(lang);
package(cplus);
package(sem);

void run()
{
	/* these are conceivably comming from the context */
	ptr < source_location > loc = source_location::create(file_info::create("",NULL),1,1);
	ptr < ss_compound_stmt > parent_stmt = ss_compound_stmt::root_instance();
	ptr < ss_decl_seq > parent_scope = ss_decl_seq::root_instance();
	ss_access_specifier::type access_specifier = ss_access_specifier::ACCESS_PUBLIC;
	

	/* these could be comming from the handled thing */
	/* their form is not fixed. they might come in different flavor */
	ptr < ss_decl_name > name = ss_ordinary_name::create(loc, "name_of_the_class");
	ptr < ::lestes::std::list < srp < ss_base_specifier > > > base_class_list = ::lestes::std::list < srp < ss_base_specifier > >::create();

	/* these are the ``true'' local variables */

	ptr < pair < srp < ss_decl_seq >, srp < ss_compound_stmt > > > p = sa_decl_seq_compound_pair_creator::instance()->process(loc, parent_scope, parent_stmt);

	ptr < ss_decl_seq > decl_seq = p->first;
	ptr < ss_compound_stmt > stmt = p->second;


	ptr < ss_class > a_class = ss_class::create(
			decl_seq, /* members */
			ss_decl_seq::root_instance()->declared_by_get(), /* declaration */
			// FIXME more complicated approach needed
			// to handle the POD requirements wrt/ to base classes
			true, /* POD */
			ss_declaration_time::infinity(), /* completition time */
			base_class_list /* bases */
			/* descendants */
			);



	ptr < ss_structure_declaration > class_decl = ss_structure_declaration::create(
			loc, // location
			ss_declaration_time::create(loc->order_get()), // visible
			ss_declaration_time::create(loc->order_get()), // decl time
			name, //name
			parent_scope, // contained in
			a_class, // type
			ss_linkage::create("C++", ss_linkage::LINKAGE_EXTERNAL), // linkage
			// FIXME: lookup needed here:
			access_specifier, // access specifier
			ss_storage_class::ST_NONE, // storage class
			::lestes::std::set< srp< ss_struct_base > >::create(), // friends
			decl_seq
			);

	ptr < ss_injected_class_declaration > class_decl_alias = ss_injected_class_declaration::create(
			loc, // location
			ss_declaration_time::create(loc->order_get()), // visible
			ss_declaration_time::create(loc->order_get()), // decl time
			name, //name
			parent_scope, // contained in
			a_class, // type
			ss_linkage::create("C++", ss_linkage::LINKAGE_EXTERNAL), // linkage
			ss_access_specifier::ACCESS_PUBLIC, // access specifier
			ss_storage_class::ST_NONE, // storage class
			class_decl // real class
			);

	/* fill 'em with THE RIGHT values */
	a_class->decl_set(class_decl);
	decl_seq->declared_by_set(class_decl);
	decl_seq->contents_get()->push_back(class_decl_alias);
	parent_scope->contents_get()->push_back(class_decl);

	::std::ofstream f("sa_class_creation.test.xml");
	::lestes::std::dumper::dump(f, a_class);
	f.close();
	//lassert2(false, "AAAAAAAAAAAAAAAAAAAAAAA");
	//lassert2(false, "BBBBBBBBBBBBBBBBBBBBBBB");
}

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);

int main()
{
	::lestes::lang::cplus::sem::run();
}
