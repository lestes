/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Parameter declaration analyser.

  Definition of classes performing parameter declaration structural analysis.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/sem/sa_param_declaration.g.hh>
#include <lestes/std/source_location.hh>
#include <lestes/lang/cplus/sem/as_id_to_ss_decl_name.g.hh>
#include <lestes/lang/cplus/sem/as_decl.g.hh>
#include <lestes/lang/cplus/sem/sa_declarator_type.g.hh>
#include <lestes/lang/cplus/sem/sa_declaration_specifiers.g.hh>
#include <lestes/lang/cplus/sem/sa_declaration_specifier_list.g.hh>
#include <lestes/lang/cplus/sem/ss_misc.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>
#include <lestes/lang/cplus/sem/ss_type.g.hh>
#include <lestes/lang/cplus/sem/ss_decl_name.g.hh>
#include <lestes/lang/cplus/sem/or_or.g.hh>
#include <lestes/lang/cplus/sem/sa_loggers.hh>
#include <lestes/msg/logger.hh>
#include <lestes/msg/logger_util.hh>
/*
#include <lestes/lang/cplus/sem/ss_expression.g.hh>
#include <lestes/lang/cplus/sem/sa_declarator.g.hh>
#include <lestes/lang/cplus/sem/as_other.g.hh>
#include <lestes/lang/cplus/sem/ss_type_builtin.g.hh>
*/

package(lestes);
package(lang);
package(cplus);
package(sem);

// TODO pt remove
#if 0
/*!
  Returns as_declarator_op visitor for parameter type analyser.
  \pre type != NULL
  \param type  The type to start building from.
  \return A visitor appropriate for the context.
*/
ptr<as_declarator_op2ss_type> sa_param_type_context::create_as_declarator_op2ss_type(ptr<ss_type> type)
{
	lassert(type);

	return as_declarator_op2ss_type_param::create(type);
}

/*!
  Returns as_declarator_op visitor for parameter declaration analyser.
  \pre type != NULL
  \param type  The type to start building from.
  \return A visitor appropriate for the context.
*/
ptr<as_declarator_op2ss_type> sa_param_decl_context::create_as_declarator_op2ss_type(ptr<ss_type> type)
{
	lassert(type);

	return as_declarator_op2ss_type_param::create(type);
}
#endif

/*!
  Processes the parameter declaration within the given context.
  Creates the ss_parameter_declaration and inserts it in the given scope.
  TODO pt sads
  \pre decl != NULL
  \param decl  The parameter declaration to process.
*/
void sa_param_declaration::process(/*ptr<sa_declaration_specifiers> sads,*/ ptr<as_param_declaration> decl)
{
	sa_param_declaration_logger << "sa_param_declaration::process()\n" << msg::eolog;

	lassert(decl);

	// decl specs will be coming from somewhere else???
	// TODO
#if 1

	// analyse the declaration specifiers
	ptr<sa_declaration_specifiers> sads =
		sa_declaration_specifier_list::create()->process(decl->location_get(),decl->declaration_specifiers_get());
	
	sa_param_declaration_logger << "checking specifiers\n" << msg::eolog;
	// check disallowed declaration specifiers
	if (sads->virtual_flag_get() || sads->explicit_flag_get() || sads->friend_flag_get() ||
		 sads->inline_flag_get()) {
		// TODO pt report error: invalid specifier
		sa_param_declaration_logger << "invalid specifier flag\n" << msg::eolog;
	}
#endif
		 
	// check correct storage specifier
	ss_storage_class::type storage_class = sads->storage_class_get();
	switch (storage_class) {
		case ss_storage_class::ST_NONE:
			break;
		case ss_storage_class::ST_AUTO:
		case ss_storage_class::ST_REGISTER:
			sa_param_declaration_logger << "got storage class\n" << msg::eolog;
			break;
		case ss_storage_class::ST_STATIC:
		case ss_storage_class::ST_EXTERN:
		case ss_storage_class::ST_MUTABLE:
		case ss_storage_class::ST_TYPEDEF:
			// TODO pt report error: invalid storage specifiers
			sa_param_declaration_logger << "got invalid storage class specifier\n" << msg::eolog;
			break;
		default:
			lassert2(false,"You should never get here");
	}

	sa_param_declaration_logger << "analysing the declarator\n" << msg::eolog;
  
	ptr<as_declarator> declarator = decl->declarator_get();
	// get the type from the declarator
	ptr<sa_declarator_type> sadt = sa_declarator_type::create();
	ptr<ss_type> type = sadt->process(sads->type_get(),declarator);


	ptr<source_location> loc = declarator->location_get();
	ptr<ss_declaration_time> tm = ss_declaration_time::create(loc->order_get());

	ptr<as_name> name = declarator->name_get();

	ptr<ss_decl_name> decl_name;

	if (name)
		decl_name = as_id_to_ss_decl_name::instance()->process(name->identifier_get());
	else 
		decl_name = ss_dummy_name::create(loc);
	
	/*
		TODO pt by tma
	we do not yet know the type of ss_object_declaration::initializer
	
	ptr<typeof(ss_object_declaration::initializer)> init = jikos->process(decl->initializer_get());

	if (typeof(ss_object_declaration::initializer) == ss_expression)
		init0 = (*init)(psp,nsp);
	else
		init0 = init;

	*/

	// the scope for the parameters
	ptr<ss_decl_seq> param_scope = scope_get();

	sa_param_declaration_logger << "creating the parameter declaration\n" << msg::eolog;

	ptr<ss_declaration> declaration = ss_parameter_declaration::create(
		loc, // location
		tm, // visible since
		tm, // declaration time 
		decl_name, // decl name
		param_scope, // contained in
		type, // type 
		// TODO ??
		ss_linkage::create("C++",ss_linkage::LINKAGE_NO), // linkage 
		ss_access_specifier::ACCESS_PUBLIC, // access specifier
		storage_class, // storage
		// TODO pt : tm/infinity() depends on initializer presence
		ss_declaration_time::infinity(), // initialized since
		// TODO pt fill initializer
		NULL, // initializer
		index++ // position
		);
	
	sa_param_declaration_logger << "adding the parameter\n" << msg::eolog;

	param_scope->contents_get()->push_back(declaration);

	sa_param_declaration_logger << "sa_param_declaration::process() end\n" << msg::eolog;
}

// TODO pt remove
#if 0
	// create the appropriate visitor 
	ptr<as_declarator_op2ss_type> v = context->create_as_declarator_op2ss_type(sads->type_get());

	ptr<as_declarator> declarator = decl->declarator_get();

	typedef ::lestes::std::list< srp<as_declarator_op> > as_declarator_op_list_type;
	ptr<as_declarator_op_list_type> lst = declarator->declarator_ops_get();
	
	{
		as_declarator_op_list_type::reverse_iterator rit = lst->rbegin(), rend = lst->rend(),
			tit = lst->rbegin();

		++tit;
		
		// walk through all declarator ops in reverse direction
		for ( ; tit != rend; ++rit, ++tit) {
			// classify individual declarator ops
			v->process(*rit,false);
		}
		
		// classify the first declarator op
		v->process(*rit,true);
	}
#endif


#if 0
/*!
  Processes the parameter declaration within the given context.
  Creates the ss_type.
  \pre decl != NULL
  \param decl  The parameter declaration to process.
*/
void sa_param_type::process(ptr<as_param_declaration> decl)
{
	
	lassert(decl);

	ptr<sa_decl_spec_context> sadsc = sa_decl_spec_context::create(declaration_context::CTX_PARAMETER);
	ptr<sa_decl_spec> sads = sa_decl_spec::create(sadsc);

	sads->process(decl->declaration_specifiers_get());

	// create the appropriate visitor 
	ptr<as_declarator_op2ss_type> v = context->create_as_declarator_op2ss_type(sads->type_get());

	ptr<as_declarator> declarator = decl->declarator_get();

	typedef ::lestes::std::list< srp<as_declarator_op> > as_declarator_op_list_type;
	ptr<as_declarator_op_list_type> lst = declarator->declarator_ops_get();
	
	{
		as_declarator_op_list_type::reverse_iterator rit = lst->rbegin(), rend = lst->rend(),
			tit = lst->rbegin();
		++tit;
		
		// walk through all declarator ops in reverse direction
		for ( ; tit != rend; ++rit, ++tit) {
			// classify individual declarator ops
			v->process(*rit,false);
		}
		
		// classify the first declarator op
		v->process(*rit,true);
	}

	if (sads->virtual_flag_get() ||
		 sads->explicit_flag_get() ||
		 sads->inline_flag_get()) {
		// TODO pt report error: invalid specifiers
	}

	ss_storage_class::type stc = sads->storage_class_get();
	
	switch (stc) {
		case ss_storage_class::ST_NONE:
		case ss_storage_class::ST_AUTO:
		case ss_storage_class::ST_REGISTER:
			// TODO pt ignore ???
			break;
		case ss_storage_class::ST_STATIC:
		case ss_storage_class::ST_EXTERN:
		case ss_storage_class::ST_MUTABLE:
		case ss_storage_class::ST_TYPEDEF:
			// TODO pt report error: invalid storage specifiers
			break;
		default:
			lassert2(false,"You should never get here");
	}

	type = v->type_get();
}
#endif

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);
/* vim: set ft=lestes : */
