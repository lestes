<?xml version="1.0" encoding="UTF-8"?>
<!--
	The lestes compiler suite
	Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
	Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
	Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
	Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
	Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
	Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
	Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; version 2 of the License.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	See the full text of the GNU General Public License version 2, and
	the limitations in the file doc/LICENSE.

	By accepting the license the licensee waives any and all claims
	against the copyright holder(s) related in whole or in part to the
	work, its use, and/or the inability to use it.
-->
<lsd xmlns="http://lestes.jikos.cz/schemas/lsd" xmlns:h="http://www.w3.org/TR/REC-html40">

	<file-name>or_ics_actual_visitors</file-name>

	<packages>
		<p>lestes</p>
		<p>lang</p>
		<p>cplus</p>
		<p>sem</p>
	</packages>

	<imports>
		<i>lestes/common.hh</i>
		<i>lestes/std/list.hh</i>
		<i>lestes/lang/cplus/sem/or_visitor.v.g.hh</i>
	</imports>

	<implementation-imports>
		<i>lestes/lang/cplus/sem/or_ics.g.hh</i>
		<i>lestes/lang/cplus/sem/or_or.g.hh</i>
	</implementation-imports>

	<include href="or_visitor.v.lsd" />
	<include href="or_ics.lsd" />

	<default-collection kind="list" />

	<forward-class name="ss_type"/>
	<forward-class name="or_ics_functional" />
	<forward-class name="or_or_functional" />

	<class name="or_ics_functional_visitor" base="or_ics_functional_base" >
		<collection name="conv_list" type="ss_type" >
			<dox><bri>Contains list of types, representing conversion sequences constructed so far</bri></dox>
		</collection>
		<declare-abstract-methods />
	</class>

	<class name="or_ics_visitor_cv" base="or_ics_base_cv" >
		<declare-abstract-methods />
	</class>

	<class name="or_ics_visitor_tgts" base="or_ics_base" >
		<declare-abstract-methods />

		<field name="source" type="or_or_functional">	
			<dox><bri>Determines the source type for conversion</bri></dox>
		</field>

		<field name="scs_phase" type="lint">	
			<dox><bri>Determines the phase of the SCS construction (1,2,3)</bri></dox>
		</field>

		<collection name="seq_list" type="or_ics_functional" >	
			<dox><bri>Contains list of expressions, representing conversion sequences constructed so far</bri></dox>
		</collection>
	</class>

</lsd>
