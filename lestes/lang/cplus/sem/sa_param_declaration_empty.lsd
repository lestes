<?xml version="1.0" encoding="iso-8859-1"?>
<!--
	The lestes compiler suite
	Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
	Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
	Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
	Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
	Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
	Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
	Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; version 2 of the License.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	See the full text of the GNU General Public License version 2, and
	the limitations in the file doc/LICENSE.

	By accepting the license the licensee waives any and all claims
	against the copyright holder(s) related in whole or in part to the
	work, its use, and/or the inability to use it.
-->
<lsd xmlns="http://lestes.jikos.cz/schemas/lsd" xmlns:h="http://www.w3.org/TR/REC-html40">

	<file-name>sa_param_declaration_empty</file-name>

	<dox file="both">
		<bri>Empty parameter declaration test.</bri>
		<det>
			Checking whether a parameter declaration is a special case equivalent to empty list.
		</det>
	</dox>

	<packages>
		<p>lestes</p>
		<p>lang</p>
		<p>cplus</p>
		<p>sem</p>
	</packages>

	<imports>
		<i>lestes/common.hh</i>
	</imports>

	<implementation-imports>
		<i>lestes/lang/cplus/sem/as_decl.g.hh</i>
		<i>lestes/lang/cplus/sem/ss_misc.g.hh</i>
		<i>lestes/lang/cplus/sem/ss_type.g.hh</i>
	</implementation-imports>

	<forward-class name="as_param_declaration" />
	<forward-class name="ss_type" />

	<using-class name="object" packages="::lestes::std::"/>

	<class name="sa_param_declaration_empty" base="object">
		<dox>
			<bri>Empty parameter declaration checker.</bri>
			<det>Tests whether a parameter declaration is a special case of bare (void)
				representing empty parameter list. It should be run only in case there is
				exactly one parameter declaration in the parameter declaration list,
				in other cases this kind of declaration is disallowed.
			</det>
		</dox>
		<method name="process" type="bool">
			<dox>
				<bri>Checks empty parameter declaration.</bri>
			</dox>
			<param name="decl" type="as_param_declaration" />
		</method>
	</class>

</lsd>
