/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*!
	\file
	\brief Visitors returning lstring for ss_decl_name.
	\author jaz
*/

#include <lestes/lang/cplus/sem/ss_decl_name2lstring_base.g.hh>
#include <lestes/lang/cplus/sem/ss_decl_name.g.hh>
#include <sstream>

package(lestes);
package(lang);
package(cplus);
package(sem);

ptr< ss_decl_name2mangled_name > ss_decl_name2mangled_name::instance(){
	if ( !singleton_instance ) {
		singleton_instance = ss_decl_name2mangled_name::create();
	}
	return singleton_instance;
}

lstring ss_decl_name2mangled_name::visit_ss_dummy_name(ptr< ::lestes::lang::cplus::sem::ss_dummy_name > decl_name) { 
	lassert(decl_name);
	return lstring();
}


lstring ss_decl_name2mangled_name::visit_ss_ordinary_name(ptr< ::lestes::lang::cplus::sem::ss_ordinary_name > decl_name) {
	lassert(decl_name);
	
	::std::ostringstream oss;
	
	ucn_string name = decl_name->name_get();
	lassert(name.length());
	
	oss << name;
	
	return oss.str();
}

lstring ss_decl_name2mangled_name::visit_ss_conversion_name(ptr< ::lestes::lang::cplus::sem::ss_conversion_name > decl_name) { 
	lassert(decl_name);
	return lstring("cv"); //TODO + type name
}

lstring ss_decl_name2mangled_name::visit_ss_operator_new(ptr< ::lestes::lang::cplus::sem::ss_operator_new > decl_name){ 
	lassert(decl_name);
	return lstring("nw");
}

lstring ss_decl_name2mangled_name::visit_ss_operator_delete(ptr< ::lestes::lang::cplus::sem::ss_operator_delete > decl_name){ 
	lassert(decl_name);
	return lstring("dl");
}

lstring ss_decl_name2mangled_name::visit_ss_operator_new_array(ptr< ::lestes::lang::cplus::sem::ss_operator_new_array > decl_name){ 
	lassert(decl_name);
	return lstring("na");
}

lstring ss_decl_name2mangled_name::visit_ss_operator_delete_array(ptr< ::lestes::lang::cplus::sem::ss_operator_delete_array > decl_name){ 
	lassert(decl_name);
	return lstring("da");
}

lstring ss_decl_name2mangled_name::visit_ss_operator_add(ptr< ::lestes::lang::cplus::sem::ss_operator_add > decl_name){ 
	lassert(decl_name);
	return lstring("pl");
}

lstring ss_decl_name2mangled_name::visit_ss_operator_sub(ptr< ::lestes::lang::cplus::sem::ss_operator_sub > decl_name){ 
	lassert(decl_name);
	return lstring("mi");
}

lstring ss_decl_name2mangled_name::visit_ss_operator_mul(ptr< ::lestes::lang::cplus::sem::ss_operator_mul > decl_name){ 
	lassert(decl_name);
	return lstring("ml");
}

lstring ss_decl_name2mangled_name::visit_ss_operator_div(ptr< ::lestes::lang::cplus::sem::ss_operator_div > decl_name){ 
	lassert(decl_name);
	return lstring("dv");
}

lstring ss_decl_name2mangled_name::visit_ss_operator_mod(ptr< ::lestes::lang::cplus::sem::ss_operator_mod > decl_name){ 
	lassert(decl_name);
	return lstring("rm");
}

lstring ss_decl_name2mangled_name::visit_ss_operator_bxor(ptr< ::lestes::lang::cplus::sem::ss_operator_bxor > decl_name){ 
	lassert(decl_name);
	return lstring("eo");
}

lstring ss_decl_name2mangled_name::visit_ss_operator_band(ptr< ::lestes::lang::cplus::sem::ss_operator_band > decl_name){ 
	lassert(decl_name);
	return lstring("an");
}

lstring ss_decl_name2mangled_name::visit_ss_operator_bor(ptr< ::lestes::lang::cplus::sem::ss_operator_bor > decl_name){ 
	lassert(decl_name);
	return lstring("or");
}

lstring ss_decl_name2mangled_name::visit_ss_operator_bnot(ptr< ::lestes::lang::cplus::sem::ss_operator_bnot > decl_name){ 
	lassert(decl_name);
	return lstring("co");
}

lstring ss_decl_name2mangled_name::visit_ss_operator_lnot(ptr< ::lestes::lang::cplus::sem::ss_operator_lnot > decl_name){ 
	lassert(decl_name);
	return lstring("nt");
}

lstring ss_decl_name2mangled_name::visit_ss_operator_assign(ptr< ::lestes::lang::cplus::sem::ss_operator_assign > decl_name){ 
	lassert(decl_name);
	return lstring("aS");
}

lstring ss_decl_name2mangled_name::visit_ss_operator_assign_add(ptr< ::lestes::lang::cplus::sem::ss_operator_assign_add > decl_name){ 
	lassert(decl_name);
	return lstring("pL");
}

lstring ss_decl_name2mangled_name::visit_ss_operator_assign_sub(ptr< ::lestes::lang::cplus::sem::ss_operator_assign_sub > decl_name){ 
	lassert(decl_name);
	return lstring("mI");
}

lstring ss_decl_name2mangled_name::visit_ss_operator_assign_mul(ptr< ::lestes::lang::cplus::sem::ss_operator_assign_mul > decl_name){ 
	lassert(decl_name);
	return lstring("mL");
}

lstring ss_decl_name2mangled_name::visit_ss_operator_assign_div(ptr< ::lestes::lang::cplus::sem::ss_operator_assign_div > decl_name){ 
	lassert(decl_name);
	return lstring("dV");
}

lstring ss_decl_name2mangled_name::visit_ss_operator_assign_mod(ptr< ::lestes::lang::cplus::sem::ss_operator_assign_mod > decl_name){ 
	lassert(decl_name);
	return lstring("rM");
}

lstring ss_decl_name2mangled_name::visit_ss_operator_assign_bxor(ptr< ::lestes::lang::cplus::sem::ss_operator_assign_bxor > decl_name){ 
	lassert(decl_name);
	return lstring("eO");
}

lstring ss_decl_name2mangled_name::visit_ss_operator_assign_band(ptr< ::lestes::lang::cplus::sem::ss_operator_assign_band > decl_name){ 
	lassert(decl_name);
	return lstring("aN");
}

lstring ss_decl_name2mangled_name::visit_ss_operator_assign_bor(ptr< ::lestes::lang::cplus::sem::ss_operator_assign_bor > decl_name){ 
	lassert(decl_name);
	return lstring("oR");
}

lstring ss_decl_name2mangled_name::visit_ss_operator_assign_shl(ptr< ::lestes::lang::cplus::sem::ss_operator_assign_shl > decl_name){ 
	lassert(decl_name);
	return lstring("lS");
}

lstring ss_decl_name2mangled_name::visit_ss_operator_assign_shr(ptr< ::lestes::lang::cplus::sem::ss_operator_assign_shr > decl_name){ 
	lassert(decl_name);
	return lstring("rS");
}

lstring ss_decl_name2mangled_name::visit_ss_operator_shr(ptr< ::lestes::lang::cplus::sem::ss_operator_shr > decl_name){ 
	lassert(decl_name);
	return lstring("rs");
}

lstring ss_decl_name2mangled_name::visit_ss_operator_shl(ptr< ::lestes::lang::cplus::sem::ss_operator_shl > decl_name){ 
	lassert(decl_name);
	return lstring("ls");
}

lstring ss_decl_name2mangled_name::visit_ss_operator_sbl(ptr< ::lestes::lang::cplus::sem::ss_operator_sbl > decl_name){ 
	lassert(decl_name);
	return lstring("lt");
}

lstring ss_decl_name2mangled_name::visit_ss_operator_sbg(ptr< ::lestes::lang::cplus::sem::ss_operator_sbg > decl_name){ 
	lassert(decl_name);
	return lstring("gt");
}

lstring ss_decl_name2mangled_name::visit_ss_operator_sbng(ptr< ::lestes::lang::cplus::sem::ss_operator_sbng > decl_name){ 
	lassert(decl_name);
	return lstring("le");
}

lstring ss_decl_name2mangled_name::visit_ss_operator_sbnl(ptr< ::lestes::lang::cplus::sem::ss_operator_sbnl > decl_name){ 
	lassert(decl_name);
	return lstring("ge");
}

lstring ss_decl_name2mangled_name::visit_ss_operator_sbe(ptr< ::lestes::lang::cplus::sem::ss_operator_sbe > decl_name){ 
	lassert(decl_name);
	return lstring("eq");
}

lstring ss_decl_name2mangled_name::visit_ss_operator_sbne(ptr< ::lestes::lang::cplus::sem::ss_operator_sbne > decl_name){ 
	lassert(decl_name);
	return lstring("ne");
}

lstring ss_decl_name2mangled_name::visit_ss_operator_land(ptr< ::lestes::lang::cplus::sem::ss_operator_land > decl_name){ 
	lassert(decl_name);
	return lstring("aa");
}

lstring ss_decl_name2mangled_name::visit_ss_operator_lor(ptr< ::lestes::lang::cplus::sem::ss_operator_lor > decl_name){ 
	lassert(decl_name);
	return lstring("oo");
}

lstring ss_decl_name2mangled_name::visit_ss_operator_inc(ptr< ::lestes::lang::cplus::sem::ss_operator_inc > decl_name){ 
	lassert(decl_name);
	return lstring("pp");
}

lstring ss_decl_name2mangled_name::visit_ss_operator_dec(ptr< ::lestes::lang::cplus::sem::ss_operator_dec > decl_name){ 
	lassert(decl_name);
	return lstring("mm");
}

lstring ss_decl_name2mangled_name::visit_ss_operator_comma(ptr< ::lestes::lang::cplus::sem::ss_operator_comma > decl_name){ 
	lassert(decl_name);
	return lstring("cm");
}

lstring ss_decl_name2mangled_name::visit_ss_operator_access(ptr< ::lestes::lang::cplus::sem::ss_operator_access > decl_name){ 
	lassert(decl_name);
	return lstring("pt");
}

lstring ss_decl_name2mangled_name::visit_ss_operator_access_member(ptr< ::lestes::lang::cplus::sem::ss_operator_access_member > decl_name){ 
	lassert(decl_name);
	return lstring("pm");
}

lstring ss_decl_name2mangled_name::visit_ss_operator_function_call(ptr< ::lestes::lang::cplus::sem::ss_operator_function_call > decl_name){ 
	lassert(decl_name);
	return lstring("cl");
}

lstring ss_decl_name2mangled_name::visit_ss_operator_array(ptr< ::lestes::lang::cplus::sem::ss_operator_array > decl_name){ 
	lassert(decl_name);
	return lstring("ix");
}

lstring ss_decl_name2mangled_name::visit_ss_operator_ternary(ptr< ::lestes::lang::cplus::sem::ss_operator_ternary > decl_name){ 
	lassert(decl_name);
	return lstring("qu");
}



end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);

