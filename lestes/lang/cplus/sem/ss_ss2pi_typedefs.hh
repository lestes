/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__sem___ss_ss2pi_typedefs_hh___included
#define lestes__lang__cplus__sem___ss_ss2pi_typedefs_hh___included

/*! \file
  Declarations used for ss2pi transformation.
  \author ps
  \author pt
  \todo pt Doxyment the content.
*/
#include <lestes/common.hh>
#include <lestes/std/map.hh>
#include <lestes/std/pair.hh>

package(lestes);
package(backend_v2);
package(intercode);

class pi_operand;
class pi_mem;
class pi_mem_factory;

end_package(intercode);
end_package(backend_v2);

package(lang);
package(cplus);
package(sem);


ptr< ::lestes::backend_v2::intercode::pi_sp > ss_sp2pi_sp(ptr < ss_sp > sp);


//typedef  ::lestes::std::list< srp< ::lestes::backend_v2::intercode::pi_operand > >  list_oper_type;
typedef  ::lestes::std::list< srp< ::lestes::backend_v2::intercode::pi_mem > >  list_mem_type;
typedef ::lestes::std::map< srp< ss_declaration >, srp< ::lestes::backend_v2::intercode::pi_mem_factory> >  variable_map_type ;
typedef ::lestes::std::pair< srp< ss_function_declaration >, srp< ::lestes::backend_v2::intercode::pi_mem_factory > > destructor_entry_type ;
typedef ::lestes::std::map< srp< ss_expression >, srp< ::lestes::backend_v2::intercode::pi_operand > > expression_results_map_type ;

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif

