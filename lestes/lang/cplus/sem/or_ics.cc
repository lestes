/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
 * Implementation of overload resolution algorithm (see chapters 4,8,13)
 *
 * TODO create a map for purposes of caching of functionals, representing
 * conversions to the same type
 *
 * \author jikos
 */
#include <lestes/lang/cplus/sem/or_ics.g.hh>
#include <lestes/lang/cplus/sem/or_ics_actual_visitors.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>
#include <lestes/lang/cplus/sem/ss_misc.g.hh>
#include <lestes/lang/cplus/sem/or_or.g.hh>
#include <lestes/std/source_location.hh>
#include <lestes/msg/logger.hh>
#include <lestes/lang/cplus/sem/or_ics_logger.hh>
#include <lestes/lang/cplus/sem/or_actual_visitors.g.hh>

package(lestes);
package(lang);
package(cplus);
package(sem);

declare_logger( or_ics_log );
initialize_logger( or_ics_log, "or_ics_finder", or_ics_logger );

#define IS_PTR_BOOL_CONV(t1,t2)	((t1) == OR_CV_PTR || (t1) == OR_CV_MEMBER_PTR) && ((t2) == OR_CV_BOOL)
#define IS_PTR(t1) ((t1) == OR_CV_PTR)
#define IS_MEMBER_PTR(t1) ((t1) == OR_CV_MEMBER_PTR)
#define IS_CLASS(t1) t1 == OR_CV_CLASS
#define IS_VOID(t1) t1 == OR_CV_VOID
#define DEREF_PTR(t1) (t1.dncast<ss_pointer>()->what_get()->accept_or_ics_base_cv(v))
#define DEREF_PTR_NOVIS(t1) ((t1).dncast<ss_pointer>()->what_get())
#define MEMBER_PTR_BASE(t1) ((t1).dncast<ss_member_pointer>()->base_get()).dncast<ss_class>()
#define IS_VOLATILE(x) (((x) == OR_CV_VOLATILE) || (x) == OR_CV_CONST_VOLATILE)
#define IS_PSEUDOREFERENCE(x) (((x) == OR_CV_PSEUDOREFERENCE))
#define IS_REFERENCE(x) (((x) == OR_CV_REFERENCE))
#define IS_CONST(x) (((x) == OR_CV_CONST))

/*!
 * This function gets two conversions c1 and c2, represented by
 * their "left" and "rights" SCS and uc (l1,u1,r1 and l2,u2,r2 
 * respectively).
 *
 * the passed expression is the source expression of the conversion
 * 
 * \return true if conversion c1 is better than c2, else returns false
 */
bool is_better_conv_seq (/*ptr< or_ics_functional > l1, ptr< or_ics_functional > u1,*/ ptr< or_ics_functional > r1, ptr< or_or_functional > e1,
			 /*ptr< or_ics_functional > l2, ptr< or_ics_functional > u2,*/ ptr< or_ics_functional > r2, ptr< or_or_functional > e2)
{
	typedef ::lestes::std::list< srp< ss_type > > typelist;
	ptr< typelist > conv_list = typelist::create();
	ptr< typelist > list1, list2;
	typelist::iterator it1, it2;
	ptr< or_ics_functional_visitor > v_t = or_ics_functional_visitor::create(conv_list);

	 /* if u1 != u2 return false;
	  * rationale: 13.3.3.2/3

	  if (!(u1->equals(u2)))
		return false;

	  */	

	if (r1->rank_get() < r2->rank_get()) {
		return true;
	}
	/* 
	 * now linearize the functionals into list of target types, for easy comparsion. This is done
   	 * using visitor on or_ics_functional. 
	 * 
	 * in this list, lval->rval conversions are already excluded
	 */	 
	list1 = r1->accept_or_ics_functional_base(v_t);
	list2 = r2->accept_or_ics_functional_base(v_t);
	
	/* now check if r1 is proper subsequence of r2 */
	it1 = list1->begin();
	it2 = list2->end();

	while (it1 != list1->end() && it2 != list2->end() && (*it1)->equals(*it2)) {
		it1++;
		it2++;
	}
	
	if (it1 == list1->end() && it2 != list2->end()) {
		return true;
	}
	
	return false;
	
	/* The following code is not reached now. Will be used when user types work */
	/* TODO qualification and reference binding conversions tests */

	/* a conversion of (member) ptr to bool is worse than conversion which is not such 
	 * conversion 
	 */
	/* source and destination types */
       	ptr< ss_type > s1 = e1->type_get();
	ptr< ss_type > s2 = e2->type_get();
	ptr< ss_type > t1 = (*(list1->end()));
	ptr< ss_type > t2 = (*(list2->end()));
	
       	ptr< or_ics_visitor_cv > v = or_ics_visitor_cv::create();
        or_cv_enum ts1 = t1->accept_or_ics_base_cv(v);
	or_cv_enum ts2 = t1->accept_or_ics_base_cv(v);
	or_cv_enum tt1 = t1->accept_or_ics_base_cv(v);
	or_cv_enum tt2 = t2->accept_or_ics_base_cv(v);
	if (IS_PTR_BOOL_CONV(ts2,tt2) && !IS_PTR_BOOL_CONV(ts1,tt1))
		return true;

	/* if *ts1 derived from *tt1, then ts1 ->tt1 is better than ts1* -> void*  when ts1 == ts2 */
	if (IS_PTR(ts1) && IS_PTR(tt1) && 
	    	IS_CLASS(DEREF_PTR(s1)) && IS_CLASS(DEREF_PTR(t1)) && DEREF_PTR_NOVIS(t1).dncast<ss_class>()->is_ancestor(DEREF_PTR_NOVIS(s1).dncast<ss_class>()) &&
		s1->equals(s2) && IS_PTR(tt2) && IS_VOID(DEREF_PTR(t2)))
		return true;
	
	/* if *ts2 derived from *ts1, then ts1 -> void* is better than ts2 -> void* */
	if (IS_PTR(ts1) && IS_PTR(ts2) && IS_PTR(tt1) && IS_PTR(tt2) &&
		IS_CLASS(DEREF_PTR(s1)) && IS_CLASS(DEREF_PTR(s2)) && IS_VOID(DEREF_PTR(t1)) && IS_VOID(DEREF_PTR(t2)) && 
		DEREF_PTR_NOVIS(s1).dncast<ss_class>()->is_ancestor(DEREF_PTR_NOVIS(s2).dncast<ss_class>()))
		return true;
	
	/* if *ts1 derived from *tt1 and *tt1 derived from *tt2 and ts1 == ts2 then tt1->ts1 better than tt2->ts2 */

	if (IS_PTR(ts1) && IS_PTR(tt1) && IS_PTR(tt2) &&
		IS_CLASS(DEREF_PTR(s1)) && IS_CLASS(DEREF_PTR(t1)) && IS_CLASS(DEREF_PTR(t2)) &&
		DEREF_PTR_NOVIS(t2).dncast<ss_class>()->is_ancestor(DEREF_PTR_NOVIS(t1).dncast<ss_class>()) 
		&& DEREF_PTR_NOVIS(t1).dncast<ss_class>()->is_ancestor(DEREF_PTR_NOVIS(s1).dncast<ss_class>()) &&
		s1->equals(s2))
		return true;

	/* TODO binding C -> &B better than C->&A */

	/* if *tt1 derived from *ts1 and *tt2 derived from *tt1 and ts1 == ts2 then
	 * ts1::* -> tt1::* is better than ts2::* -> tt2::x */ 

	if (IS_MEMBER_PTR(ts1) && IS_MEMBER_PTR(tt1) && IS_MEMBER_PTR(tt2) && 
		MEMBER_PTR_BASE(s1)->is_ancestor(MEMBER_PTR_BASE(t1)) &&
		MEMBER_PTR_BASE(t1)->is_ancestor(MEMBER_PTR_BASE(t2)) &&
		s1->equals(s2))
		return true;
	
	/* ts1 derived from tt1 and tt1 derived from tt2, then ts1->tt1 is better than ts2->tt2 (if ts1==ts2) */
	if (IS_CLASS(ts1) && IS_CLASS(tt1) && IS_CLASS(tt2) &&
		s1->equals(s2) && 
		t1.dncast<ss_class>()->is_ancestor(s1.dncast<ss_class>()) && t2.dncast<ss_class>()->is_ancestor(t1.dncast<ss_class>()))
		return true;
		
	/* if (*tt1 == *tt2) and *ts1 derived from *tt1 and *ts2 derived from *ts1 then ts1->tt1 is better than ts2->tt2 */
	if (IS_PTR(tt1) && IS_PTR(ts1) && IS_PTR(ts2) &&
		IS_CLASS(DEREF_PTR(t1)) && IS_CLASS(DEREF_PTR(s1)) && IS_CLASS(DEREF_PTR(s2)) &&
		DEREF_PTR_NOVIS(t1).dncast<ss_class>()->is_ancestor(DEREF_PTR_NOVIS(s2).dncast<ss_class>()) &&
		DEREF_PTR_NOVIS(s1).dncast<ss_class>()->is_ancestor(DEREF_PTR_NOVIS(s2).dncast<ss_class>()) &&
		t1->equals(t2))
		return true;
	/* TODO binding B->A& is better than binding C->A& */

	/* if *ts1 is derived from *ts2 and *tt1 is derived from *ts1 and tt1 == tt2, then ts1::*->tt1::* is better than 
	 * ts2::*->tt2::*
	 */
	
	if (IS_MEMBER_PTR(ts2) && IS_MEMBER_PTR(ts1) && IS_MEMBER_PTR(tt1) &&
		MEMBER_PTR_BASE(s2)->is_ancestor(MEMBER_PTR_BASE(s1)) &&
		MEMBER_PTR_BASE(s1)->is_ancestor(MEMBER_PTR_BASE(t1)) &&
		t1->equals(t2))
		return true;

	/* this happens only when initialization of user-defined conversion happens (13.3.3). this is the only case when
   	 * source types are diferrent 
	 */

	if (IS_CLASS(tt1) && IS_CLASS(ts1) && IS_CLASS(ts2) &&
		t1.dncast<ss_class>()->is_ancestor(s1.dncast<ss_class>()) &&
		s1.dncast<ss_class>()->is_ancestor(s2.dncast<ss_class>())
		&& t1->equals(t2))
		return true;

	return false;	
}

bool is_not_worse_conv_seq(/*ptr< or_ics_functional > l1, ptr< or_ics_functional > u1,*/ ptr< or_ics_functional > r1, ptr< or_or_functional > e1,
                         /*ptr< or_ics_functional > l2, ptr< or_ics_functional > u2,*/ ptr< or_ics_functional > r2, ptr< or_or_functional > e2)
{
	return (!(is_better_conv_seq(/*l2, u2, */r2, e2, /* l1, u1,*/ r1, e1)));
}

 
/*!
 * This function filters out those conversions not converting to the type
 * of the target
 *
 * \return the filtered list
 */

ptr< ::lestes::std::list< srp< or_ics_functional > > > filter_type(ptr< ::lestes::std::list< srp< or_ics_functional > > > lst, ptr< ss_type > tgt)
{
	::lestes::std::list< srp< or_ics_functional > >::iterator it, it_del;
	ptr< or_ics_visitor_cv > v = or_ics_visitor_cv::create();
	ptr< ::lestes::std::list< srp< or_ics_functional > > > ret = ::lestes::std::list< srp< or_ics_functional > >::create();
	
	/* this cycle erases all functionals from list, not having target_type of tgt
	 *
	 * this takes care of (pseudo)references automatically
	 */
	llog(or_ics_log) << "Filtering ...\n";
	for(it = lst->begin(); it != lst->end();it++){
		/* are the types not references && are the same? */
		if ( (*it)->target_type_get()->equals(tgt) ) {
			ret->push_back(*it);
			llog(or_ics_log) << "Found identical type\n";
		}
	}
	return ret;
}

ptr< or_ics_functional > get_best_conversion(ptr< ::lestes::std::list< srp< or_ics_functional > > > lst, ptr< or_or_functional > e)
{
	::lestes::std::list< srp< or_ics_functional > >::iterator it = lst->begin();
	ptr< or_ics_functional > best = *it;

	for(; it != lst->end(); it++) {
		if (is_better_conv_seq(*it, e, best, e)) 
			best = *it;
	}

	return best;
}

/*!
 * This is the main function called by outside world - used to find implicit 
 * conversion sequence between source and target. It fires up visitor, depending
 * on the source type, which then constructs the conversion sequence.
TODO: if the source expression is funcall && to vraci rval (neni referencni)
      potom identita je get (rank IDENTITY)
 * \return expression representing the implicit conversion sequence
 */
ptr< or_ics_functional > or_find_ics(ptr< or_or_functional > source, ptr< ss_type > target)
{
	typedef ::lestes::std::list< srp< or_ics_functional > > funclist;

	llog(or_ics_log) << "Entering or_find_ics()\n";

	ptr< funclist > scs_tgts_seq = funclist::create();
	ptr< funclist > scs_srcs_seq = funclist::create();
	ptr< funclist > filtered_list;
	
	ptr< or_ics_visitor_tgts > v_t = or_ics_visitor_tgts::create(source, 1, scs_tgts_seq);
	ptr< or_ics_functional > best_conv;
	
	ptr< funclist > scs_imaginable_tgts, scs_imaginable_srcs;
	
	/* this finds all SCS-imaginable types for source type. It can remember the phase 
	 * so calling it three times causes multiple phases to be called.
	 */

	llog(or_ics_log) << "Executing first phase\n";
	scs_imaginable_tgts = source->type_get()->accept_or_ics_base(v_t);

	/* the pseudoreference performs the recursion itself, so we must not to run it here again */
	ptr< or_ics_visitor_cv > v = or_ics_visitor_cv::create(); 
	if(source->type_get()->accept_or_ics_base_cv(v) != OR_CV_PSEUDOREFERENCE) {
		/* for non-pseudoreferences, the identity is handled here */
                ptr< ss_type > type1 = source->type_get();
                ptr< or_ics_functional_for_std_conversion > conversion1 = or_ics_functional_for_std_conversion::create(RANK_EXACT, type1);
                scs_tgts_seq->push_back(conversion1);
		
		llog(or_ics_log) << "Source is not pseudoreference, executing second and third phases\n";
		scs_imaginable_tgts = source->type_get()->accept_or_ics_base(v_t);
		scs_imaginable_tgts = source->type_get()->accept_or_ics_base(v_t);
	}
		
	llog(or_ics_log) << "Found " << scs_imaginable_tgts->size() << " imaginable target types\n";
	
	/* for each element of the target, 
	 * - if the imaginable tgt is builtin type, lookup is performed and user-defined conversions (by constructor)
	 * possible and create list of pairs {l1, u1}, where l1\elem scs_imaginable_tgts and
	 * u1 \elem user_defined_conversions from type l1 
	 *
	 * - if the imaginable tgt is class (and therefore the source was also class and the conversion
	 * was just derived-to-base standard conversion), it is now neccessary to search for user-defined
	 * conversion functions, generate appropriate conversion by this conversion function, and continue
	 * with generating 'left' part of the ICS
	 * 
	 * this is TODO
	 */
	
	/* filter out those not having the type of target */

	filtered_list = filter_type(scs_imaginable_tgts, target);
	
	llog(or_ics_log) << "After filtering " << filtered_list->size() << " left\n";
	
	/* now get the best conversion from those remaining in the list.
	 * this will be more useful as soon as user-def conversions come
	 * on scene. 
	 *
	 * Just for now, not having user types and user conversions 
	 * so far, return the first from filtered_list (if we are
	 * in builtin type situation, there should be just one anyway).
	 * 
	 * this is TODO FIXME as long as we are going to handle user types and conversions
	 */
//	best_conv = get_best_conversion(filtered_list, source);
//	return best_conv;	
	
	if (filtered_list->size() > 0) 
		return filtered_list->front();
	else
		return NULL;
}

/*!
 * Represents the functional for standard conversion (chap. 4)
 * \return ss_conversion structure representing the conversion
 */
ptr< ss_expression > or_ics_functional_for_std_conversion::operator()(
						ptr< ss_expression > arg, ptr< ss_sp > psp, ptr< ss_sp > nsp)
{
	if (target_type_get()->equals(arg->type_get()))
		return arg;
	else	
		return ss_conversion::create	(arg->location_get(),
				target_type_get(), 
				psp,
				nsp,
				arg,
				arg->type_get()
				);
}

/*!
 * Represents the functional for array-to-pointer conversion (chap. 4)
 * \return ss_conversion structure representing the conversion
 */
ptr< ss_expression > or_ics_functional_for_arr_ptr_conversion::operator()(
						ptr< ss_expression > arg, ptr< ss_sp > psp, ptr< ss_sp > nsp)
{
	return ss_array_to_pointer::create	(arg->location_get(),
			target_type_get(), 
			psp,
			nsp,
			arg,
			arg->type_get()
			);
}

/*!
 * Represents the functional for reference binding conversion.
 * \return ss_ structure representing the conversion
 */
ptr< ss_expression > or_ics_functional_for_reference_bind_conversion::operator()(
						ptr< ss_expression > arg, ptr< ss_sp > psp, ptr< ss_sp > nsp)
{
	return ss_bind_reference::create	(arg->location_get(),
				target_type_get(), 
				psp,
				nsp,
				arg,
				arg->type_get()
				);
}

/*!
 * Represents the functional for rval to const reference binding (using temporary).
 * \return ss_ structure representing the conversion
 */
ptr< ss_expression > or_ics_functional_for_bind_to_temporary_conversion::operator()(
						ptr< ss_expression > arg, ptr< ss_sp > psp, ptr< ss_sp > nsp)
{
	return ss_bind_to_temporary::create	(arg->location_get(),
				target_type_get(), 
				psp,
				nsp,
				arg,
				arg->type_get()
				);
}


/*!
 * Represents the functional for lvalue-rvalue (chap. 3)
FIXME: should be singleton, as all lval->rval are reperented by the
       very same functional.
 * \return ss_conversion structure representing the conversion
 */
ptr< ss_expression > or_ics_functional_for_lval_rval_conversion::operator()(
						ptr< ss_expression > arg,ptr< ss_sp > psp, ptr< ss_sp > nsp)
{
	ptr< or_ics_visitor_cv > v = or_ics_visitor_cv::create();

	if(IS_VOLATILE(arg->type_get()->accept_or_ics_base_cv(v))) 
		return ss_vol_get::create(arg->location_get(), 
			arg->type_get(), 
			psp, 
			nsp, 
			arg);
	else if (IS_PSEUDOREFERENCE(arg->type_get()->accept_or_ics_base_cv(v))) 
		if(IS_CONST(arg->type_get().dncast<ss_pseudoreference>()->what_get()->accept_or_ics_base_cv(v)))
			return ss_get::create(arg->location_get(),
				arg->type_get().dncast<ss_pseudoreference>()->what_get().dncast<ss_const>()->what_get(),
				psp,
				nsp,
				arg);
		else
			return ss_get::create(arg->location_get(),
				arg->type_get().dncast<ss_pseudoreference>()->what_get(),
				psp,
				nsp,
				arg);
	else if (IS_REFERENCE(arg->type_get()->accept_or_ics_base_cv(v))) 
		return ss_get::create(arg->location_get(),
			arg->type_get().dncast<ss_reference>()->what_get(),
			psp,
			nsp,
			arg);
	else
		lassert2(false, "Don't know how to do lval->rval conversion for this type\n");
		return NULL;
}

/*!
 * Represents the functional for user conversion by constructor (12.3.1)
 * \return funcall, representing the call of converting constructor
 */
ptr< ss_expression > or_ics_functional_for_user_conversion_by_constructor::operator()(
						ptr< ss_expression > arg, ptr< ss_sp > psp, ptr< ss_sp > nsp)
{
	ptr< ::lestes::std::list< srp< ss_expression > > >	arg_list;
	/* there is only one argument to constructor */
	arg_list->push_back(arg);

	return ss_funcall::create(arg->location_get(),
				target_type_get(),
				psp,
				nsp,
				arg_list,
				used_constructor);	

}

/*!
 * Represents the functional for user conversion by conversion functions 
 * (12.3.2)
 * \return funcall, representing the call of conversion function
 */
ptr< ss_expression > or_ics_functional_for_user_conversion_by_conversion_function::operator()(
						ptr< ss_expression > arg, ptr< ss_sp > psp, ptr< ss_sp > nsp)
{
	ptr< ::lestes::std::list< srp< ss_expression > > >	arg_list;
	/* there is only one argument to constructor */
	arg_list->push_back(arg);

	/* check whether the chosen conversion function is virtual */
	/* conversion funtion is a method, hence the dncast is correct */
	if (used_conversion_function_get().dncast<ss_method_declaration>()->virtuality_get())
		return ss_vfuncall::create(arg->location_get(),
				target_type_get(),
				psp,
				nsp,
				arg_list,
				used_conversion_function,
				arg);	
	else
		return ss_funcall::create(arg->location_get(),
				target_type_get(),
				psp,
				nsp,
				arg_list,
				used_conversion_function);	
}

/*!
 * Represents the functional for compound conversion
 * (12.3.2)
 * \return the result of calling the 'outter' functional on the
 *         inner functional
 */
ptr< ss_expression > or_ics_functional_for_compound_conversion::operator() (
							ptr< ss_expression > arg, ptr< ss_sp > psp, ptr< ss_sp> nsp)
{
	return (*outter_conversion)((*inner_conversion)(arg, psp, nsp), psp, nsp);
}

/*!
 * Dummy. Never called. To make gcc happy.
 */ 
ptr< ss_expression > or_ics_functional::operator() (ptr< ss_expression >, ptr< ss_sp >, ptr< ss_sp>)
{
	return NULL;
}

/*!
 * This functions converts the expression, which has type of pointer to cv1 P, to
 * pointer to cv2 P, c2 being more cv-qualified than c1. See 4.4/1
 *
 * We rely on source being expression of ss_pointer type
 * 
 * Definition of relation "to be more qualified":
 * none < const
 * none < volatile
 * none < const volatile
 * const < const volatile
 * volatile < const volatile
 * \returns list of conversion expressions
 */
ptr< ss_expression > convert_ptr_to_cv(ptr< or_or_functional > source)
{
	/* this is to store the result passed to caller */
	typedef ::lestes::std::list< srp< ss_expression > > funclist;
        ptr< funclist > seq = funclist::create();

	ptr< ss_type > p = source->type_get();
	ptr< or_ics_visitor_cv > v = or_ics_visitor_cv::create();
	/* this gets the qualification of the type pointed to by the pointer */
	or_cv_enum q = p.dncast<ss_pointer>()->what_get()->accept_or_ics_base_cv(v);

	/* TODO this is unfinished */	
	switch (q){
		case OR_CV_NONE:;	
		case OR_CV_CONST:;
		case OR_CV_VOLATILE:;
		case OR_CV_CONST_VOLATILE:;
		default:;
	}
		
	/* FIXME */
	return NULL;
}

/* Following are the functions (implemented visitors), which perform the construction
 * of SCS from the source type.
 * Many of them could possibly be collapsed to one function (for example uint/sint etc), but 
 * for future extensibility it is kept as separate as possible.
 */

/* ----- Here the implementations of visitors finding _targets_ for type begin ----- */

#define CONVERT_TO(type,rank){	\
		for(it = it_orig; it != seq_list_end; ++it){	\
                        ptr< ss_type > type1 = ss_type_##type::instance();	\
                        ptr< or_ics_functional_for_std_conversion > conversion1 = or_ics_functional_for_std_conversion::create(rank, type1);	\
			ptr< or_ics_functional_for_compound_conversion > conversion2 = or_ics_functional_for_compound_conversion::create(	\
									MAX((*it)->rank_get(), rank),					\
									conversion1->target_type_get(),					\
									conversion1,							\
									*it);								\
			new_list->push_back(conversion2);										\
                }\
		funclist::iterator it_f = new_list->begin();\
}

#define CONVERT_TO_POINTER(_type,rank)	\
			::lestes::std::list< srp< or_ics_functional > >::iterator it = seq_list->begin();					\
			ptr< ss_type > type1 = ss_pointer::instance(_type);	\
                        ptr< or_ics_functional_for_std_conversion > conversion1 = or_ics_functional_for_std_conversion::create(rank, type1);	\
			ptr< or_ics_functional_for_compound_conversion > conversion2 = or_ics_functional_for_compound_conversion::create(	\
									MAX((*it)->rank_get(), rank),					\
									conversion1->target_type_get(),					\
									conversion1,							\
									*it);								\
			seq_list->push_back(conversion2);										\

#define CONVERT_TO_NONTYPE(type,rank){	\
		for(it = it_orig; it != seq_list_end; it++){	\
                        ptr< ss_type > type1 = ss_##type::instance();	\
                        ptr< or_ics_functional_for_std_conversion > conversion1 = or_ics_functional_for_std_conversion::create(rank, type1);	\
			ptr< or_ics_functional_for_compound_conversion > conversion2 = or_ics_functional_for_compound_conversion::create(	\
									MAX((*it)->rank_get(), rank),					\
									conversion1->target_type_get(),					\
									conversion1,							\
									*it);								\
			new_list->push_back(conversion2);										\
                }\
}


#define CONVERT_LVAL_RVAL() \
		ptr< or_ics_visitor_cv > v = or_ics_visitor_cv::create();	\
		llog(or_ics_log) << "Performing lval->rval conversion\n"; \
		if(source_get()->type_get()->accept_or_ics_base_cv(v) == OR_CV_REFERENCE ) {	\
			llog(or_ics_log) << "We are holding reference type\n"; \
		\
			/* the first field is identity - the functional we can use for lval->rval composition */	\
			::lestes::std::list< srp< or_ics_functional > >::iterator it = seq_list->begin();			\
			ptr< or_ics_functional_for_lval_rval_conversion > conversion1 = 					\
						or_ics_functional_for_lval_rval_conversion::create(RANK_EXACT, source_get()->type_get());	\
			ptr< or_ics_functional_for_compound_conversion > conversion2 = or_ics_functional_for_compound_conversion::create(	\
								RANK_EXACT,	\
								conversion1->target_type_get(),					\
								conversion1,	\
								*it);		\
			seq_list->push_back(conversion2);	\
			/* the lvalue will no more be needed - only in ss_function and ss_array cases, \
			 * and they are handled separately \
			 */	\
			seq_list->pop_front();	\
		} \
		
ptr< ::lestes::std::list< srp< or_ics_functional > > > or_ics_visitor_tgts::visit_ss_class(ptr< ss_class > type)
{
	if (scs_phase_get() == 1) {
	}

	if (scs_phase_get() == 2) {

		::lestes::std::list< srp< or_ics_functional > >::iterator it, it_orig = seq_list->begin();
		::lestes::std::list< srp< or_ics_functional > >::iterator seq_list_end = seq_list->end();

		::lestes::std::list< srp< ss_class > >::iterator it_cl, it_cl_orig, it_cl_end;
		::lestes::std::list< srp< ss_base_specifier > >::iterator it_b;
		ptr< ::lestes::std::list< srp< or_ics_functional > > > new_list = ::lestes::std::list< srp< or_ics_functional > >::create();
		
		/* for all classes in seq_list, push all bases to the list - conversion-to-base, 13.3.3.1/6 */
		for (it = it_orig; it != seq_list_end; it++) {
			for (it_b = type->bases_get()->begin(); it_b != type->bases_get()->end(); it_b++) {
				ptr< or_ics_functional_for_std_conversion > conversion1 = or_ics_functional_for_std_conversion::create(RANK_CONVERSION,(*it_b)->base_class_get());
				ptr< or_ics_functional_for_compound_conversion > conversion2 = or_ics_functional_for_compound_conversion::create(
												MAX(RANK_CONVERSION, (*it)->rank_get()), 
												conversion1->target_type_get(),
												conversion1, 
												*it);
				new_list->push_back(conversion2);
			}
		}
		for(it = new_list->begin(); it != new_list->end(); it++) 
			seq_list->push_back(*it);
	}

	
	if (scs_phase_get() == 3) {
		/* qualification conversion doesn't take place for non-ptr type */
	}

	/* phase++ */
	scs_phase_set(scs_phase_get()+1);

	return seq_list;

}

/*!
 * This performs just a very basic conversion for const type. TODO FIXME make it more
 * elaborate and according to the norm
 */
ptr< ::lestes::std::list< srp< or_ics_functional > > > or_ics_visitor_tgts::visit_ss_const(ptr< ss_const > /*type*/)
{
	if (scs_phase_get() == 1) {
	}
	if (scs_phase_get() == 2) {
		::lestes::std::list< srp< or_ics_functional > >::iterator it, it_orig = seq_list->begin();
		::lestes::std::list< srp< or_ics_functional > >::iterator seq_list_end = seq_list->end();
		typedef ::lestes::std::list< srp< or_ics_functional > > funclist; 
		ptr< funclist > new_list = funclist::create(); 

		/* TODO FIXME the whole 4.10 and 4.11 thing */

		/* 4.12 boolean conversion */
		CONVERT_TO_NONTYPE(bool, RANK_CONVERSION);

		funclist::iterator it_f = new_list->begin();
		for(; it_f != new_list->end(); ++it_f) 
			seq_list->push_back(*it_f);

		/* The removal of const is handled in visit_ss_psedoref */
/*
		::lestes::std::list< srp< or_ics_functional > >::iterator it, it_orig = seq_list->begin();
		::lestes::std::list< srp< or_ics_functional > >::iterator seq_list_end = seq_list->end();
		typedef ::lestes::std::list< srp< or_ics_functional > > funclist;
		ptr< funclist > new_list = funclist::create();
		for(it = it_orig; it != seq_list_end; ++it){	
                        ptr< ss_type > type1 = type->what_get();
			ptr< or_ics_functional_for_std_conversion > conversion1 = or_ics_functional_for_std_conversion::create(RANK_CONVERSION, type1);	
			ptr< or_ics_functional_for_compound_conversion > conversion2 = or_ics_functional_for_compound_conversion::create(
									MAX((*it)->rank_get(), RANK_CONVERSION),
									conversion1->target_type_get(),	
									conversion1,	
									*it);	
			new_list->push_back(conversion2);
                }
		funclist::iterator it_f = new_list->begin();
		for(; it_f != new_list->end(); ++it_f)
			seq_list->push_back(*it_f);
*/	
	}

	if (scs_phase_get() == 3) {
		/* qualification conversion doesn't take place for non-ptr type */
	}

	/* phase++ */
	scs_phase_set(scs_phase_get()+1);

	return seq_list;


}

ptr< ::lestes::std::list< srp< or_ics_functional > > > or_ics_visitor_tgts::visit_ss_volatile(ptr< ss_volatile > /*type*/)
{
	lassert2(false, "conversions for volatile types not implemented yet\n");
	if (scs_phase_get() == 1) {
	}
	if (scs_phase_get() == 2) {

		typedef ::lestes::std::list< srp< or_ics_functional > > funclist;
		ptr< funclist > tgts_seq = funclist::create();
		ptr< or_ics_visitor_tgts > v_t = or_ics_visitor_tgts::create(source, 1, tgts_seq);
		ptr< funclist > tgts;
		funclist::iterator it;

		/* this finds all SCS-imaginable types for c-unqualified source type */
		tgts = source->type_get()->accept_or_ics_base(v_t);

		for (it = tgts->begin(); it != tgts->end(); it++)
			seq_list->push_back(*it);
		
		
	}

	if (scs_phase_get() == 3) {
		/* qualification conversion doesn't take place for non-ptr type */
	}

	/* phase++ */
	scs_phase_set(scs_phase_get()+1);

	return seq_list;
}


ptr< ::lestes::std::list< srp< or_ics_functional > > > or_ics_visitor_tgts::visit_ss_const_volatile(ptr< ss_const_volatile > /*type*/)
{
	lassert2(false, "conversions for const volatile types not implemented yet\n");
	/* the identity conversion is always OK */
	if (scs_phase_get() == 1) {
		/* nothing more (array->ptr, func->ptr) can be done in 1st phase for this type */

	}
	if (scs_phase_get() == 2) {

		typedef ::lestes::std::list< srp< or_ics_functional > > funclist;
		ptr< funclist > tgts_seq = funclist::create();
		ptr< or_ics_visitor_tgts > v_t = or_ics_visitor_tgts::create(source, 1, tgts_seq);
		ptr< funclist > tgts;
		funclist::iterator it;

		/* this finds all SCS-imaginable types for c-unqualified source type */
		tgts = source->type_get()->accept_or_ics_base(v_t);

		for (it = tgts->begin(); it != tgts->end(); it++)
			seq_list->push_back(*it);
		
		
	}

	if (scs_phase_get() == 3) {
		/* qualification conversion doesn't take place for non-ptr type */
	}

	/* phase++ */
	scs_phase_set(scs_phase_get()+1);

	return seq_list;
}

ptr< ::lestes::std::list< srp< or_ics_functional > > > or_ics_visitor_tgts::visit_ss_bool(ptr< ss_bool > /*type*/)
{
	/* the identity conversion is always OK */
	if (scs_phase_get() == 1) {
	}
	if (scs_phase_get() == 2) {

		::lestes::std::list< srp< or_ics_functional > >::iterator it, it_orig = seq_list->begin();
		::lestes::std::list< srp< or_ics_functional > >::iterator seq_list_end = seq_list->end();
		typedef ::lestes::std::list< srp< or_ics_functional > > funclist; 
		ptr< funclist > new_list = funclist::create(); 

		/* floating point conversions */
		CONVERT_TO(float,RANK_CONVERSION);
		CONVERT_TO(ldouble,RANK_CONVERSION);
		CONVERT_TO(ldouble,RANK_CONVERSION);

		/* floating-integral conversions */
		CONVERT_TO(sshort,RANK_CONVERSION);
		CONVERT_TO(ushort,RANK_CONVERSION);
		CONVERT_TO(uchar,RANK_CONVERSION);
		CONVERT_TO(schar,RANK_CONVERSION);
		CONVERT_TO(pchar,RANK_CONVERSION);
		CONVERT_TO(uint,RANK_CONVERSION);
		CONVERT_TO(sint,RANK_PROMOTION);
		CONVERT_TO(slong,RANK_CONVERSION);
		CONVERT_TO(ulong,RANK_CONVERSION);
		funclist::iterator it_f = new_list->begin();
		for(; it_f != new_list->end(); ++it_f) 
			seq_list->push_back(*it_f);

	}

	if (scs_phase_get() == 3) {
		/* qualification conversion doesn't take place for non-ptr type */
	}

	/* phase++ */
	scs_phase_set(scs_phase_get()+1);

	return seq_list;
}

ptr< ::lestes::std::list< srp< or_ics_functional > > > or_ics_visitor_tgts::visit_ss_array(ptr< ss_array > type)
{

	if (scs_phase_get() == 1) {
		/* pointer to the type of array element */
		//CONVERT_TO_POINTER(type, RANK_EXACT);
		
		
		/* nothing more (func->ptr, lval->rval) can be done in 1st phase for this type */

	}
	if (scs_phase_get() == 2) {
		::lestes::std::list< srp< or_ics_functional > >::iterator it, it_orig = seq_list->begin();
		::lestes::std::list< srp< or_ics_functional > >::iterator seq_list_end = seq_list->end();
		typedef ::lestes::std::list< srp< or_ics_functional > > funclist;
		ptr< funclist > new_list = funclist::create();
		for(it = it_orig; it != seq_list_end; ++it){	
                        ptr< ss_type > type1 = ss_pointer::instance(type->type_get());
			ptr< or_ics_functional_for_arr_ptr_conversion > conversion1 = or_ics_functional_for_arr_ptr_conversion::create(RANK_CONVERSION, type1);	
			ptr< or_ics_functional_for_compound_conversion > conversion2 = or_ics_functional_for_compound_conversion::create(
									MAX((*it)->rank_get(), RANK_CONVERSION),
									conversion1->target_type_get(),	
									conversion1,	
									*it);	
			new_list->push_back(conversion2);
                }
		funclist::iterator it_f = new_list->begin();
		for(; it_f != new_list->end(); ++it_f)
			seq_list->push_back(*it_f);

		/* TODO FIXME the whole 4.10 and 4.11 thing */
	}

	if (scs_phase_get() == 3) {
		/* FIXME FIXME qualification conversion */
			
	}

	/* phase++ */
	scs_phase_set(scs_phase_get()+1);

	return seq_list;
	
}

ptr< ::lestes::std::list< srp< or_ics_functional > > > or_ics_visitor_tgts::visit_ss_function(ptr< ss_function > type)
{
	lassert2(false, "conversions of function type are not implemented yet\n");
	if (scs_phase_get() == 1) {

		/* pointer to the function type */

		/* if lvalue (reference or pseudoreference) */
		ptr< or_ics_visitor_cv > v = or_ics_visitor_cv::create();
		if(! (source_get()->type_get()->accept_or_ics_base_cv(v) == OR_CV_REFERENCE || source_get()->type_get()->accept_or_ics_base_cv(v) == OR_CV_PSEUDOREFERENCE)) { 
			CONVERT_TO_POINTER(type, RANK_EXACT);
		}
		/* nothing more (func->ptr, lval->rval) can be done in 1st phase for this type */

	}
	if (scs_phase_get() == 2) {

		::lestes::std::list< srp< or_ics_functional > >::iterator it, it_orig = seq_list->begin();
		::lestes::std::list< srp< or_ics_functional > >::iterator seq_list_end = seq_list->end();
		typedef ::lestes::std::list< srp< or_ics_functional > > funclist; 
		ptr< funclist > new_list = funclist::create(); 

                /* TODO FIXME the whole 4.10 and 4.11 thing */
		
	}

	if (scs_phase_get() == 3) {
		/* FIXME FIXME qualification conversion */
	}

	/* phase++ */
	scs_phase_set(scs_phase_get()+1);

	return seq_list;

}

ptr< ::lestes::std::list< srp< or_ics_functional > > > or_ics_visitor_tgts::visit_ss_pointer(ptr< ss_pointer > type)
{
	if (scs_phase_get() == 1) {
	}
	if (scs_phase_get() == 2) {

		::lestes::std::list< srp< or_ics_functional > >::iterator it, it_orig = seq_list->begin();
		::lestes::std::list< srp< or_ics_functional > >::iterator seq_list_end = seq_list->end();
		typedef ::lestes::std::list< srp< or_ics_functional > > funclist; 
		ptr< funclist > new_list = funclist::create(); 

		/* TODO FIXME the whole 4.10 and 4.11 thing */

		/* 4.12 boolean conversion */
		CONVERT_TO_NONTYPE(bool, RANK_CONVERSION);
		funclist::iterator it_f = new_list->begin();

		for(it = it_orig; it != seq_list_end; ++it){	
			ptr< ss_type > type1 = ss_pointer::instance(ss_const::instance(type->what_get()));
			ptr< or_ics_functional_for_std_conversion > conversion1 = or_ics_functional_for_std_conversion::create(RANK_CONVERSION, type1);	
			ptr< or_ics_functional_for_compound_conversion > conversion2 = or_ics_functional_for_compound_conversion::create(
					MAX((*it)->rank_get(), RANK_CONVERSION),
					conversion1->target_type_get(),	
					conversion1,	
					*it);	
			new_list->push_back(conversion2);
		}

		
		for(; it_f != new_list->end(); ++it_f) {
			seq_list->push_back(*it_f);
			
		}

	}

	if (scs_phase_get() == 3) {
		/* FIXME FIXME qualification conversion */
	}

	/* phase++ */
	scs_phase_set(scs_phase_get()+1);

	return seq_list;
}

ptr< ::lestes::std::list< srp< or_ics_functional > > > or_ics_visitor_tgts::visit_ss_member_pointer(ptr< ss_member_pointer > /*type*/)
{
	/* in first phase, there can be lval->rval */
	if (scs_phase_get() == 1) {
	}
	if (scs_phase_get() == 2) {

		::lestes::std::list< srp< or_ics_functional > >::iterator it, it_orig = seq_list->begin();
		::lestes::std::list< srp< or_ics_functional > >::iterator seq_list_end = seq_list->end();
		typedef ::lestes::std::list< srp< or_ics_functional > > funclist; 
		ptr< funclist > new_list = funclist::create(); 

		/* TODO FIXME 4.11 (member ptr) */

		/* 4.12 boolean conversion */
		CONVERT_TO_NONTYPE(bool, RANK_CONVERSION);
	
		funclist::iterator it_f = new_list->begin();
		for(; it_f != new_list->end(); ++it_f) 
			seq_list->push_back(*it_f);
	}

	if (scs_phase_get() == 3) {
		/* FIXME FIXME qualification conversion */
	}

	/* phase++ */
	scs_phase_set(scs_phase_get()+1);

	return seq_list;
}

ptr< ::lestes::std::list< srp< or_ics_functional > > > or_ics_visitor_tgts::visit_ss_union(ptr< ss_union > /*type*/)
{
	if (scs_phase_get() == 1) {
	}
	if (scs_phase_get() == 2) {

		::lestes::std::list< srp< or_ics_functional > >::iterator it, it_orig = seq_list->begin();
		::lestes::std::list< srp< or_ics_functional > >::iterator seq_list_end = seq_list->end();

		/* TODO FIXME union conversions */
	}

	if (scs_phase_get() == 3) {
		/* TODO FIXME qualification conversion? */
	}

	/* phase++ */
	scs_phase_set(scs_phase_get()+1);

	return seq_list;
}

ptr< ::lestes::std::list< srp< or_ics_functional > > > or_ics_visitor_tgts::visit_ss_void(ptr< ss_void > /*type*/)
{
	if (scs_phase_get() == 1) {
	}
	if (scs_phase_get() == 2) {
	}

	if (scs_phase_get() == 3) {
	}

	/* phase++ */
	scs_phase_set(scs_phase_get()+1);

	return seq_list;
}

ptr< ::lestes::std::list< srp< or_ics_functional > > > or_ics_visitor_tgts::visit_ss_reference(ptr< ss_reference > type)
{

	typedef ::lestes::std::list< srp< or_ics_functional > > funclist;

	/* identity conversion */
	ptr< or_ics_functional_for_std_conversion > conversion1 = or_ics_functional_for_std_conversion::create(RANK_EXACT, type);
	seq_list->push_back(conversion1);

	/* lvalue->rvalue conversion */
	funclist::iterator id_it = seq_list->begin();
	ptr< or_ics_functional_for_lval_rval_conversion > id_conversion1 =
		or_ics_functional_for_lval_rval_conversion::create(RANK_EXACT, type->what_get());
	ptr< or_ics_functional_for_compound_conversion > id_conversion2 = or_ics_functional_for_compound_conversion::create(
			RANK_EXACT,
			type->what_get(),
			id_conversion1,
			*id_it);
	seq_list->push_back(id_conversion2);

	return seq_list;
}
ptr< ::lestes::std::list< srp< or_ics_functional > > > or_ics_visitor_tgts::visit_ss_type_sint(ptr< ss_type_sint > /*type*/)
{
	if (scs_phase_get() == 1) {
	}
	if (scs_phase_get() == 2) {
		/* we perform the conversion for every type in seq_list */

		::lestes::std::list< srp< or_ics_functional > >::iterator it, it_orig = seq_list->begin();
		::lestes::std::list< srp< or_ics_functional > >::iterator seq_list_end = seq_list->end();
		typedef ::lestes::std::list< srp< or_ics_functional > > funclist; 
		ptr< funclist > new_list = funclist::create(); 
					
		CONVERT_TO(sshort,RANK_CONVERSION);
		CONVERT_TO(ushort,RANK_CONVERSION);
		CONVERT_TO(schar,RANK_CONVERSION);
		CONVERT_TO(uchar,RANK_CONVERSION);
		CONVERT_TO(pchar,RANK_CONVERSION);
		CONVERT_TO(uint,RANK_CONVERSION);
		CONVERT_TO(ulong,RANK_CONVERSION);
		CONVERT_TO(slong,RANK_CONVERSION);
		CONVERT_TO(double,RANK_CONVERSION);
		CONVERT_TO(ldouble,RANK_CONVERSION);
		CONVERT_TO(float,RANK_CONVERSION);
		CONVERT_TO_NONTYPE(bool,RANK_CONVERSION);

		funclist::iterator it_f = new_list->begin();
		for(; it_f != new_list->end(); ++it_f) 
			seq_list->push_back(*it_f);
		
	}

	if (scs_phase_get() == 3) {
		/* qualification conversion doesn't take place for non-ptr type */
	}

	/* phase++ */
	scs_phase_set(scs_phase_get()+1);

	return seq_list;

}

ptr< ::lestes::std::list< srp< or_ics_functional > > > or_ics_visitor_tgts::visit_ss_type_uint(ptr< ss_type_uint > /*type*/)
{
	if (scs_phase_get() == 1) {
	}
	if (scs_phase_get() == 2) {
		/* integral conversions */

		::lestes::std::list< srp< or_ics_functional > >::iterator it, it_orig = seq_list->begin();
		::lestes::std::list< srp< or_ics_functional > >::iterator seq_list_end = seq_list->end();
		typedef ::lestes::std::list< srp< or_ics_functional > > funclist; 
		ptr< funclist > new_list = funclist::create(); 

		CONVERT_TO(sshort,RANK_CONVERSION);
		CONVERT_TO(ushort,RANK_CONVERSION);
		CONVERT_TO(schar,RANK_CONVERSION);
		CONVERT_TO(uchar,RANK_CONVERSION);
		CONVERT_TO(pchar,RANK_CONVERSION);
		CONVERT_TO(sint,RANK_CONVERSION);
		CONVERT_TO(ulong,RANK_CONVERSION);
		CONVERT_TO(slong,RANK_CONVERSION);
		CONVERT_TO(double,RANK_CONVERSION);
		CONVERT_TO(ldouble,RANK_CONVERSION);
		CONVERT_TO(float,RANK_CONVERSION);
		CONVERT_TO_NONTYPE(bool,RANK_CONVERSION);

		funclist::iterator it_f = new_list->begin();
		for(; it_f != new_list->end(); ++it_f) 
			seq_list->push_back(*it_f);

	}

	if (scs_phase_get() == 3) {
		/* qualification conversion doesn't take place for non-ptr type */
	}

	/* phase++ */
	scs_phase_set(scs_phase_get()+1);

	return seq_list;
}

ptr< ::lestes::std::list< srp< or_ics_functional > > > or_ics_visitor_tgts::visit_ss_type_slong(ptr< ss_type_slong > /*type*/)
{
	if (scs_phase_get() == 1) {
	}
	if (scs_phase_get() == 2) {
		/* integral conversions */
		::lestes::std::list< srp< or_ics_functional > >::iterator it, it_orig = seq_list->begin();
		::lestes::std::list< srp< or_ics_functional > >::iterator seq_list_end = seq_list->end();
		typedef ::lestes::std::list< srp< or_ics_functional > > funclist; 
		ptr< funclist > new_list = funclist::create(); 

		CONVERT_TO(sshort,RANK_CONVERSION);
		CONVERT_TO(ushort,RANK_CONVERSION);
		CONVERT_TO(schar,RANK_CONVERSION);
		CONVERT_TO(uchar,RANK_CONVERSION);
		CONVERT_TO(pchar,RANK_CONVERSION);
		CONVERT_TO(sint,RANK_CONVERSION);
		CONVERT_TO(uint,RANK_CONVERSION);
		CONVERT_TO(ulong,RANK_CONVERSION);
		CONVERT_TO(double,RANK_CONVERSION);
		CONVERT_TO(ldouble,RANK_CONVERSION);
		CONVERT_TO(float,RANK_CONVERSION);
		CONVERT_TO_NONTYPE(bool,RANK_CONVERSION);

		funclist::iterator it_f = new_list->begin();
		for(; it_f != new_list->end(); ++it_f) 
			seq_list->push_back(*it_f);
	}

	if (scs_phase_get() == 3) {
		/* qualification conversion doesn't take place for non-ptr type */
	}

	/* phase++ */
	scs_phase_set(scs_phase_get()+1);

	return seq_list;

}

ptr< ::lestes::std::list< srp< or_ics_functional > > > or_ics_visitor_tgts::visit_ss_type_ulong(ptr< ss_type_ulong > /*type*/)
{
	if (scs_phase_get() == 1) {
	}
	if (scs_phase_get() == 2) {
		/* integral conversions */

		::lestes::std::list< srp< or_ics_functional > >::iterator it, it_orig = seq_list->begin();
		::lestes::std::list< srp< or_ics_functional > >::iterator seq_list_end = seq_list->end();
		typedef ::lestes::std::list< srp< or_ics_functional > > funclist; 
		ptr< funclist > new_list = funclist::create(); 

		CONVERT_TO(sshort,RANK_CONVERSION);
		CONVERT_TO(ushort,RANK_CONVERSION);
		CONVERT_TO(schar,RANK_CONVERSION);
		CONVERT_TO(uchar,RANK_CONVERSION);
		CONVERT_TO(pchar,RANK_CONVERSION);
		CONVERT_TO(sint,RANK_CONVERSION);
		CONVERT_TO(uint,RANK_CONVERSION);
		CONVERT_TO(slong,RANK_CONVERSION);
		CONVERT_TO(double,RANK_CONVERSION);
		CONVERT_TO(ldouble,RANK_CONVERSION);
		CONVERT_TO(float,RANK_CONVERSION);
		CONVERT_TO_NONTYPE(bool,RANK_CONVERSION);
		funclist::iterator it_f = new_list->begin();

		for(; it_f != new_list->end(); ++it_f) 
			seq_list->push_back(*it_f);
	}

	if (scs_phase_get() == 3) {
		/* qualification conversion doesn't take place for non-ptr type */
	}

	/* phase++ */
	scs_phase_set(scs_phase_get()+1);

	return seq_list;
}

ptr< ::lestes::std::list< srp< or_ics_functional > > > or_ics_visitor_tgts::visit_ss_type_sshort(ptr< ss_type_sshort > /*type*/)
{
	if (scs_phase_get() == 1) {
	}
	if (scs_phase_get() == 2) {

		::lestes::std::list< srp< or_ics_functional > >::iterator it, it_orig = seq_list->begin();
		::lestes::std::list< srp< or_ics_functional > >::iterator seq_list_end = seq_list->end();
		typedef ::lestes::std::list< srp< or_ics_functional > > funclist; 
		ptr< funclist > new_list = funclist::create(); 

		/* integral promotion */
		CONVERT_TO(sint,RANK_PROMOTION);
		
		/* integral conversions */
		CONVERT_TO(ushort,RANK_CONVERSION);
		CONVERT_TO(schar,RANK_CONVERSION);
		CONVERT_TO(uchar,RANK_CONVERSION);
		CONVERT_TO(pchar,RANK_CONVERSION);
		CONVERT_TO(uint,RANK_CONVERSION);
		CONVERT_TO(slong,RANK_CONVERSION);
		CONVERT_TO(ulong,RANK_CONVERSION);
		CONVERT_TO(double,RANK_CONVERSION);
		CONVERT_TO(ldouble,RANK_CONVERSION);
		CONVERT_TO(float,RANK_CONVERSION);
		CONVERT_TO_NONTYPE(bool,RANK_CONVERSION);
		funclist::iterator it_f = new_list->begin();
		for(; it_f != new_list->end(); ++it_f) 
			seq_list->push_back(*it_f);
	}

	if (scs_phase_get() == 3) {
		/* qualification conversion doesn't take place for non-ptr type */
	}

	/* phase++ */
	scs_phase_set(scs_phase_get()+1);

	return seq_list;

}

ptr< ::lestes::std::list< srp< or_ics_functional > > > or_ics_visitor_tgts::visit_ss_type_ushort(ptr< ss_type_ushort > /*type*/)
{
	if (scs_phase_get() == 1) {
	}
	if (scs_phase_get() == 2) {

		::lestes::std::list< srp< or_ics_functional > >::iterator it, it_orig = seq_list->begin();
		::lestes::std::list< srp< or_ics_functional > >::iterator seq_list_end = seq_list->end();
		typedef ::lestes::std::list< srp< or_ics_functional > > funclist; 
		ptr< funclist > new_list = funclist::create(); 

		/* integral promotion */
		CONVERT_TO(sint,RANK_PROMOTION);
		
		/* integral conversions */
		CONVERT_TO(sshort,RANK_CONVERSION);
		CONVERT_TO(schar,RANK_CONVERSION);
		CONVERT_TO(uchar,RANK_CONVERSION);
		CONVERT_TO(pchar,RANK_CONVERSION);
		CONVERT_TO(uint,RANK_CONVERSION);
		CONVERT_TO(slong,RANK_CONVERSION);
		CONVERT_TO(ulong,RANK_CONVERSION);
		CONVERT_TO(double,RANK_CONVERSION);
		CONVERT_TO(ldouble,RANK_CONVERSION);
		CONVERT_TO(float,RANK_CONVERSION);
		CONVERT_TO_NONTYPE(bool,RANK_CONVERSION);
		
		funclist::iterator it_f = new_list->begin();
		for(; it_f != new_list->end(); ++it_f) 
			seq_list->push_back(*it_f);
	}

	if (scs_phase_get() == 3) {
		/* qualification conversion doesn't take place for non-ptr type */
	}

	/* phase++ */
	scs_phase_set(scs_phase_get()+1);

	return seq_list;
}

ptr< ::lestes::std::list< srp< or_ics_functional > > > or_ics_visitor_tgts::visit_ss_type_schar(ptr< ss_type_schar > /*type*/)
{
	if (scs_phase_get() == 1) {
	}
	if (scs_phase_get() == 2) {

		::lestes::std::list< srp< or_ics_functional > >::iterator it, it_orig = seq_list->begin();
		::lestes::std::list< srp< or_ics_functional > >::iterator seq_list_end = seq_list->end();
		typedef ::lestes::std::list< srp< or_ics_functional > > funclist; 
		ptr< funclist > new_list = funclist::create(); 

		/* integral promotion */
		CONVERT_TO(sint,RANK_PROMOTION);
		
		/* integral conversions */
		CONVERT_TO(sshort,RANK_CONVERSION);
		CONVERT_TO(ushort,RANK_CONVERSION);
		CONVERT_TO(uchar,RANK_CONVERSION);
		CONVERT_TO(pchar,RANK_CONVERSION);
		CONVERT_TO(uint,RANK_CONVERSION);
		CONVERT_TO(slong,RANK_CONVERSION);
		CONVERT_TO(ulong,RANK_CONVERSION);
		CONVERT_TO(double,RANK_CONVERSION);
		CONVERT_TO(ldouble,RANK_CONVERSION);
		CONVERT_TO(float,RANK_CONVERSION);
		CONVERT_TO_NONTYPE(bool,RANK_CONVERSION);

		funclist::iterator it_f = new_list->begin();
		for(; it_f != new_list->end(); ++it_f) 
			seq_list->push_back(*it_f);
	}

	if (scs_phase_get() == 3) {
		/* qualification conversion doesn't take place for non-ptr type */
	}

	/* phase++ */
	scs_phase_set(scs_phase_get()+1);

	return seq_list;
}

ptr< ::lestes::std::list< srp< or_ics_functional > > > or_ics_visitor_tgts::visit_ss_type_uchar(ptr< ss_type_uchar > /*type*/)
{
	if (scs_phase_get() == 1) {
		/* we can perform lval->rval conversion */
		CONVERT_LVAL_RVAL();
		/* nothing more (array->ptr, func->ptr) can be done in 1st phase for this type */

	}
	if (scs_phase_get() == 2) {

		::lestes::std::list< srp< or_ics_functional > >::iterator it, it_orig = seq_list->begin();
		::lestes::std::list< srp< or_ics_functional > >::iterator seq_list_end = seq_list->end();
		typedef ::lestes::std::list< srp< or_ics_functional > > funclist; 
		ptr< funclist > new_list = funclist::create(); 

		/* integral promotion */
		CONVERT_TO(sint,RANK_PROMOTION);
		
		/* integral conversions */
		CONVERT_TO(sshort,RANK_CONVERSION);
		CONVERT_TO(ushort,RANK_CONVERSION);
		CONVERT_TO(schar,RANK_CONVERSION);
		CONVERT_TO(pchar,RANK_CONVERSION);
		CONVERT_TO(uint,RANK_CONVERSION);
		CONVERT_TO(slong,RANK_CONVERSION);
		CONVERT_TO(ulong,RANK_CONVERSION);
		CONVERT_TO(double,RANK_CONVERSION);
		CONVERT_TO(ldouble,RANK_CONVERSION);
		CONVERT_TO(float,RANK_CONVERSION);
		CONVERT_TO_NONTYPE(bool,RANK_CONVERSION);

		funclist::iterator it_f = new_list->begin();
		for(; it_f != new_list->end(); ++it_f) 
			seq_list->push_back(*it_f);
	}

	if (scs_phase_get() == 3) {
		/* qualification conversion doesn't take place for non-ptr type */
	}

	/* phase++ */
	scs_phase_set(scs_phase_get()+1);

	return seq_list;
}

ptr< ::lestes::std::list< srp< or_ics_functional > > > or_ics_visitor_tgts::visit_ss_type_pchar(ptr< ss_type_pchar > /*type*/)
{
	if (scs_phase_get() == 1) {
		/* we can perform lval->rval conversion */
		CONVERT_LVAL_RVAL();
		/* nothing more (array->ptr, func->ptr) can be done in 1st phase for this type */

	}
	if (scs_phase_get() == 2) {

		::lestes::std::list< srp< or_ics_functional > >::iterator it, it_orig = seq_list->begin();
		::lestes::std::list< srp< or_ics_functional > >::iterator seq_list_end = seq_list->end();
		typedef ::lestes::std::list< srp< or_ics_functional > > funclist; 
		ptr< funclist > new_list = funclist::create(); 

		/* integral promotion */
		CONVERT_TO(sint,RANK_PROMOTION);
		
		/* integral conversions */
		CONVERT_TO(sshort,RANK_CONVERSION);
		CONVERT_TO(ushort,RANK_CONVERSION);
		CONVERT_TO(uchar,RANK_CONVERSION);
		CONVERT_TO(schar,RANK_CONVERSION);
		CONVERT_TO(uint,RANK_CONVERSION);
		CONVERT_TO(slong,RANK_CONVERSION);
		CONVERT_TO(ulong,RANK_CONVERSION);
		CONVERT_TO(double,RANK_CONVERSION);
		CONVERT_TO(ldouble,RANK_CONVERSION);
		CONVERT_TO(float,RANK_CONVERSION);
		CONVERT_TO_NONTYPE(bool,RANK_CONVERSION);

		funclist::iterator it_f = new_list->begin();
		for(; it_f != new_list->end(); ++it_f) 
			seq_list->push_back(*it_f);
	}

	if (scs_phase_get() == 3) {
		/* qualification conversion doesn't take place for non-ptr type */
	}

	/* phase++ */
	scs_phase_set(scs_phase_get()+1);

	return seq_list;
}

ptr< ::lestes::std::list< srp< or_ics_functional > > > or_ics_visitor_tgts::visit_ss_type_float(ptr< ss_type_float > /*type*/)
{
	if (scs_phase_get() == 1) {
		/* we can perform lval->rval conversion */
		CONVERT_LVAL_RVAL();
		/* nothing more (array->ptr, func->ptr) can be done in 1st phase for this type */

	}
	if (scs_phase_get() == 2) {

		::lestes::std::list< srp< or_ics_functional > >::iterator it, it_orig = seq_list->begin();
		::lestes::std::list< srp< or_ics_functional > >::iterator seq_list_end = seq_list->end();
		typedef ::lestes::std::list< srp< or_ics_functional > > funclist; 
		ptr< funclist > new_list = funclist::create(); 

		/* floating point promotion */
		CONVERT_TO(double,RANK_PROMOTION);
		
		/* floating point conversions */
		CONVERT_TO(ldouble,RANK_CONVERSION);

		/* floating-integral conversions */
		CONVERT_TO(sshort,RANK_CONVERSION);
		CONVERT_TO(ushort,RANK_CONVERSION);
		CONVERT_TO(uchar,RANK_CONVERSION);
		CONVERT_TO(schar,RANK_CONVERSION);
		CONVERT_TO(pchar,RANK_CONVERSION);
		CONVERT_TO(uint,RANK_CONVERSION);
		CONVERT_TO(sint,RANK_CONVERSION);
		CONVERT_TO(slong,RANK_CONVERSION);
		CONVERT_TO(ulong,RANK_CONVERSION);
		CONVERT_TO_NONTYPE(bool,RANK_CONVERSION);

		funclist::iterator it_f = new_list->begin();
		for(; it_f != new_list->end(); ++it_f) 
			seq_list->push_back(*it_f);
	
	}

	if (scs_phase_get() == 3) {
		/* qualification conversion doesn't take place for non-ptr type */
	}

	/* phase++ */
	scs_phase_set(scs_phase_get()+1);

	return seq_list;
}

ptr< ::lestes::std::list< srp< or_ics_functional > > > or_ics_visitor_tgts::visit_ss_type_double(ptr< ss_type_double > /*type*/)
{
	if (scs_phase_get() == 1) {
		/* we can perform lval->rval conversion */
		CONVERT_LVAL_RVAL();
		/* nothing more (array->ptr, func->ptr) can be done in 1st phase for this type */

	}
	if (scs_phase_get() == 2) {

		::lestes::std::list< srp< or_ics_functional > >::iterator it, it_orig = seq_list->begin();
		::lestes::std::list< srp< or_ics_functional > >::iterator seq_list_end = seq_list->end();
		typedef ::lestes::std::list< srp< or_ics_functional > > funclist; 
		ptr< funclist > new_list = funclist::create(); 

		/* floating point conversions */
		CONVERT_TO(float,RANK_CONVERSION);
		CONVERT_TO(ldouble,RANK_CONVERSION);

		/* floating-integral conversions */
		CONVERT_TO(sshort,RANK_CONVERSION);
		CONVERT_TO(ushort,RANK_CONVERSION);
		CONVERT_TO(uchar,RANK_CONVERSION);
		CONVERT_TO(schar,RANK_CONVERSION);
		CONVERT_TO(pchar,RANK_CONVERSION);
		CONVERT_TO(uint,RANK_CONVERSION);
		CONVERT_TO(sint,RANK_CONVERSION);
		CONVERT_TO(slong,RANK_CONVERSION);
		CONVERT_TO(ulong,RANK_CONVERSION);
		CONVERT_TO_NONTYPE(bool,RANK_CONVERSION);

		funclist::iterator it_f = new_list->begin();
		for(; it_f != new_list->end(); ++it_f) 
			seq_list->push_back(*it_f);

	}

	if (scs_phase_get() == 3) {
		/* qualification conversion doesn't take place for non-ptr type */
	}

	/* phase++ */
	scs_phase_set(scs_phase_get()+1);

	return seq_list;
}

ptr< ::lestes::std::list< srp< or_ics_functional > > > or_ics_visitor_tgts::visit_ss_type_ldouble(ptr< ss_type_ldouble > /*type*/)
{
	if (scs_phase_get() == 1) {
	}
	if (scs_phase_get() == 2) {

		::lestes::std::list< srp< or_ics_functional > >::iterator it, it_orig = seq_list->begin();
		::lestes::std::list< srp< or_ics_functional > >::iterator seq_list_end = seq_list->end();
		typedef ::lestes::std::list< srp< or_ics_functional > > funclist; 
		ptr< funclist > new_list = funclist::create(); 

		/* floating point conversions */
		CONVERT_TO(float,RANK_CONVERSION);
		CONVERT_TO(double,RANK_CONVERSION);

		/* floating-integral conversions */
		CONVERT_TO(sshort,RANK_CONVERSION);
		CONVERT_TO(ushort,RANK_CONVERSION);
		CONVERT_TO(uchar,RANK_CONVERSION);
		CONVERT_TO(schar,RANK_CONVERSION);
		CONVERT_TO(pchar,RANK_CONVERSION);
		CONVERT_TO(uint,RANK_CONVERSION);
		CONVERT_TO(sint,RANK_CONVERSION);
		CONVERT_TO(slong,RANK_CONVERSION);
		CONVERT_TO(ulong,RANK_CONVERSION);
		CONVERT_TO_NONTYPE(bool,RANK_CONVERSION);

		funclist::iterator it_f = new_list->begin();
		for(; it_f != new_list->end(); ++it_f) 
			seq_list->push_back(*it_f);
	}

	if (scs_phase_get() == 3) {
		/* qualification conversion doesn't take place for non-ptr type */
	}

	/* phase++ */
	scs_phase_set(scs_phase_get()+1);

	return seq_list;
}
 
ptr< ::lestes::std::list< srp< or_ics_functional > > > or_ics_visitor_tgts::visit_ss_enum(ptr< ss_enum > /*type*/)
{
	if (scs_phase_get() == 1) {
	}
	if (scs_phase_get() == 2) {

		::lestes::std::list< srp< or_ics_functional > >::iterator it, it_orig = seq_list->begin();
		::lestes::std::list< srp< or_ics_functional > >::iterator seq_list_end = seq_list->end();
		typedef ::lestes::std::list< srp< or_ics_functional > > funclist; 
		ptr< funclist > new_list = funclist::create(); 

		/* FIXME - here should be considered, according to number of enumerator, the first
		   type which can be the target of the conversion - can be bigger than uint */
		CONVERT_TO(uint,RANK_CONVERSION);

		/* 4.12 - boolean conversion */
		CONVERT_TO_NONTYPE(bool, RANK_CONVERSION);

		funclist::iterator it_f = new_list->begin();
		for(; it_f != new_list->end(); ++it_f) 
			seq_list->push_back(*it_f);

	}

	if (scs_phase_get() == 3) {
		/* qualification conversion doesn't take place for non-ptr type */
	}

	/* phase++ */
	scs_phase_set(scs_phase_get()+1);

	return seq_list;

}
/*!
 * This handles the case of pseudoreference. The pseudoreference type is representing those lvalues which are not 
 * references. The conversion between pseudoreference and reference is identity. So we process the type to which
 * we hold the pseudoreference as usual and then we add to the list of imaginable types those which are references,
 * because pseudoreference->reference conversion is for free.
 *
 * Let's have the following source for demonstration:
 *
	int f(int a);
	int f(int &a)

	int main()
	{
	        int z = 1;
	        f(z);
	}

 * In this case, we get pseudoreference for 'z' parameter (because z is lavalue but not reference). pseudoreference->reference
 * conversion is identity. It has the same rank as lval->rval (which would happen for calling of int f(int a)). So the example
 * above is ambiguous.
 * BUT if the call would be 
		f(z-1);
 * then the ambiguity is gone, as z-1 is rvalue (and therefore we hold directly ss_int, not ss_(pseudo)reference) and rval->ref
 * doesn't exist, but rval->rval (int->int in this case) is identity, and int f(int a) is called
 *
 * See [4.1,2,3], [13.3] with special respect to [13.3.3.1.4].  
 */

ptr< ::lestes::std::list< srp< or_ics_functional > > > or_ics_visitor_tgts::visit_ss_pseudoreference(ptr< ss_pseudoreference > type)
{
	typedef ::lestes::std::list< srp< or_ics_functional > > funclist;
	ptr< funclist > tgts_seq = funclist::create();
	
	ptr< funclist > tgts;
	funclist::iterator it, it_sav_begin, it_sav_end;
	ptr< or_ics_functional > first;
	ptr< or_or_functional > fake_func;

	llog(or_ics_log) << "visit_ss_pseudoreference\n";
	
	/* we need 'fake' or_or_functional, which will be the same as 'source', but will be of
	 * the type for which we hold pseudoreference. This is needed for proper generation
	 * of scs-imaginables
	 */
	
	/* We must create the fake functional according to the type of functional we currently hold */
	ptr< or_or_functional_to_enum > v_f = or_or_functional_to_enum::create();
	source->accept_or_or_functional_visitor(v_f);
	or_or_functional_enum e = v_f->result_get();

	if (e == OR_OR_FUNCTIONAL_DECL) 
		fake_func = or_or_functional_decl::create(type->what_get(), source.dncast<or_or_functional_decl>()->declaration_get());
	else if (e == OR_OR_FUNCTIONAL_CONCRETE) 
		fake_func = or_or_functional_decl::create(type->what_get(), source.dncast<or_or_functional_concrete>()->declaration_get());
	else if (e == OR_OR_FUNCTIONAL_COMMA) 
		fake_func = or_or_functional_this::create(type->what_get());
	else
		/* any functional holding just type will help here */
		fake_func = or_or_functional_this::create(type->what_get());
//	else
//		lassert2(false, "Hit unsupported type of or_or_functional when handling ICS for pseudoreference\n");
	ptr< or_ics_visitor_tgts > v_t = or_ics_visitor_tgts::create(fake_func, 1, tgts_seq);

	/* identity conversion */
//	ptr< or_ics_functional_for_std_conversion > i_conversion1 = or_ics_functional_for_std_conversion::create(RANK_EXACT, type->what_get());
//	tgts_seq->push_back(i_conversion1);
	// this is from time when lval->rval was used as identity conversion
	// FIXME is this correct or the one above??

	/* For the const type, the lval->rval should remove the const */
	ptr< or_ics_visitor_cv > v = or_ics_visitor_cv::create();
	ptr< or_ics_functional_for_lval_rval_conversion > i_conversion1;
	if (type->what_get()->accept_or_ics_base_cv(v) == OR_CV_CONST) {
		i_conversion1 = or_ics_functional_for_lval_rval_conversion::create(RANK_EXACT, type->what_get().dncast<ss_const>()->what_get());
	}
	else {
		i_conversion1 = or_ics_functional_for_lval_rval_conversion::create(RANK_EXACT, type->what_get());
	}
	
	tgts_seq->push_back(i_conversion1);
	
	/* This performs all 3 phases of SCS-imagination (e.g finds all SCS-imaginable
	 * target for the type we hold pseudoreference for
	 */
	tgts = type->what_get()->accept_or_ics_base(v_t);
	tgts = type->what_get()->accept_or_ics_base(v_t);
	tgts = type->what_get()->accept_or_ics_base(v_t);
	llog(or_ics_log) << tgts->size() << " targets generated\n";

	/* identity conversion */
	ptr< or_ics_functional_for_std_conversion > conversion1 = or_ics_functional_for_std_conversion::create(RANK_EXACT, type);
	seq_list->push_back(conversion1);

	/* reference binding conversion */
	ptr< or_ics_functional_for_reference_bind_conversion > r_conversion1 = or_ics_functional_for_reference_bind_conversion::create(RANK_EXACT, 
										ss_reference::instance(type->what_get()));
	seq_list->push_back(r_conversion1);
	
	/* lvalue->rvalue conversion */
	funclist::iterator id_it = seq_list->begin();
	ptr< or_ics_functional_for_lval_rval_conversion > id_conversion1 = 
				or_ics_functional_for_lval_rval_conversion::create(RANK_EXACT, type->what_get());
	ptr< or_ics_functional_for_compound_conversion > id_conversion2 = or_ics_functional_for_compound_conversion::create(
			RANK_EXACT,
			type->what_get(),
			id_conversion1,
			*id_it);
	seq_list->push_back(id_conversion2);
	
	/* now we have in tgts list all the types to which the type for which we hold the reference can be converted.
	 * Now add for every type the reference type, as the pseudoreference->reference conversion is identity for our
	 * purposes
	 */
	it_sav_begin = tgts->begin(); 
	it_sav_end = tgts->end();
	for (it = it_sav_begin; it != it_sav_end; it++) {
		/* now add the conversions making 'references' for types */
/*
		ptr< ss_type > type1 = ss_reference::instance((*it)->target_type_get());	
		ptr< or_ics_functional_for_reference_bind_conversion > conversion1 = or_ics_functional_for_reference_bind_conversion::create(RANK_EXACT, type1);	
		ptr< or_ics_functional_for_compound_conversion > conversion2 = or_ics_functional_for_compound_conversion::create(	
								(*it)->rank_get(),
								conversion1->target_type_get(),					
								conversion1,							
								*it);								
		seq_list->push_back(conversion2);	
*/
		seq_list->push_back(*it);
	}

	llog(or_ics_log) << "references for all these types added to imaginables. returning " << seq_list->size() << " seq_list elements\n";

	return seq_list;	
}

ptr< ::lestes::std::list< srp< or_ics_functional > > > or_ics_visitor_tgts::visit_ss_type_wchar_t(ptr< ss_type_wchar_t > /*type*/)
{
	lassert2(false, "Conversions for wchar_t are not implemented yet\n");
}
ptr< ::lestes::std::list< srp< or_ics_functional > > > or_ics_visitor_tgts::visit_ss_member_function(ptr< ss_member_function > /*type*/)
{
	lassert2(false, "Conversions for member functions are not implemented yet\n");
}

#if the_class_is_no_longer_abstract
ptr< ::lestes::std::list< srp< or_ics_functional > > > or_ics_visitor_tgts::visit_ss_typename_type(ptr< ss_typename_type > /*type*/)
{
	lassert2(false, "Conversions for typename_type are not implemented yet\n");
}
#endif

/* ----- Here the implementation of visitors for returning qualification, begin ----- */

or_cv_enum or_ics_visitor_cv::visit_ss_const(ptr< ss_const > )
{
	return OR_CV_CONST;
}

or_cv_enum or_ics_visitor_cv::visit_ss_volatile(ptr< ss_volatile > )
{
	return OR_CV_VOLATILE;
}

or_cv_enum or_ics_visitor_cv::visit_ss_const_volatile(ptr< ss_const_volatile > )
{
	return OR_CV_CONST_VOLATILE;
}

or_cv_enum or_ics_visitor_cv::visit_ss_union(ptr< ss_union > )
{
	return OR_CV_NONE;
}

or_cv_enum or_ics_visitor_cv::visit_ss_pointer(ptr< ss_pointer > )
{
	return OR_CV_PTR;
}

or_cv_enum or_ics_visitor_cv::visit_ss_enum(ptr< ss_enum > )
{
	return OR_CV_NONE;
}

or_cv_enum or_ics_visitor_cv::visit_ss_array(ptr< ss_array > )
{
	return OR_CV_NONE;
}

or_cv_enum or_ics_visitor_cv::visit_ss_member_pointer(ptr< ss_member_pointer > )
{
	return OR_CV_MEMBER_PTR;
}

or_cv_enum or_ics_visitor_cv::visit_ss_function(ptr< ss_function > )
{
	return OR_CV_NONE;
}

or_cv_enum or_ics_visitor_cv::visit_ss_class(ptr< ss_class > )
{
	return OR_CV_NONE;
}

or_cv_enum or_ics_visitor_cv::visit_ss_bool(ptr< ss_bool > )
{
	return OR_CV_BOOL;
}

or_cv_enum or_ics_visitor_cv::visit_ss_void(ptr< ss_void > )
{
	return OR_CV_VOID;
}

or_cv_enum or_ics_visitor_cv::visit_ss_type_sint(ptr< ss_type_sint > )
{
	return OR_CV_SINT;
}

or_cv_enum or_ics_visitor_cv::visit_ss_type_uint(ptr< ss_type_uint > )
{
	return OR_CV_NONE;
}

or_cv_enum or_ics_visitor_cv::visit_ss_type_slong(ptr< ss_type_slong > )
{
	return OR_CV_NONE;
}

or_cv_enum or_ics_visitor_cv::visit_ss_type_ulong(ptr< ss_type_ulong > )
{
	return OR_CV_NONE;
}

or_cv_enum or_ics_visitor_cv::visit_ss_type_sshort(ptr< ss_type_sshort > )
{
	return OR_CV_NONE;
}

or_cv_enum or_ics_visitor_cv::visit_ss_type_ushort(ptr< ss_type_ushort > )
{
	return OR_CV_NONE;
}

or_cv_enum or_ics_visitor_cv::visit_ss_type_schar(ptr< ss_type_schar > )
{
	return OR_CV_NONE;
}

or_cv_enum or_ics_visitor_cv::visit_ss_type_uchar(ptr< ss_type_uchar > )
{
	return OR_CV_NONE;
}

or_cv_enum or_ics_visitor_cv::visit_ss_type_pchar(ptr< ss_type_pchar > )
{
	return OR_CV_NONE;
}

or_cv_enum or_ics_visitor_cv::visit_ss_type_float(ptr< ss_type_float > )
{
	return OR_CV_NONE;
}

or_cv_enum or_ics_visitor_cv::visit_ss_type_double(ptr< ss_type_double > )
{
	return OR_CV_NONE;
}

or_cv_enum or_ics_visitor_cv::visit_ss_type_ldouble(ptr< ss_type_ldouble > )
{
	return OR_CV_NONE;
}

or_cv_enum or_ics_visitor_cv::visit_ss_reference(ptr< ss_reference > )
{
	return OR_CV_REFERENCE;
}
or_cv_enum or_ics_visitor_cv::visit_ss_type_wchar_t(ptr< ss_type_wchar_t > )
{
	return OR_CV_NONE;
}
or_cv_enum or_ics_visitor_cv::visit_ss_member_function(ptr< ss_member_function > )
{
	return OR_CV_NONE;
}
or_cv_enum or_ics_visitor_cv::visit_ss_pseudoreference(ptr< ss_pseudoreference > )
{
	return OR_CV_PSEUDOREFERENCE;
}
#if the_class_is_no_longer_abstract
or_cv_enum or_ics_visitor_cv::visit_ss_typename_type(ptr< ss_typename_type > )
{
	return OR_CV_NONE;
}
#endif

/* ----- Here the implementation of visitors on or functionals begin (used to linearize the conversions) ----- */
ptr< ::lestes::std::list< srp< ss_type > > > or_ics_functional_visitor::visit_or_ics_functional_for_std_conversion(ptr< or_ics_functional_for_std_conversion > func) 
{
	conv_list->push_front(func->target_type_get());
	return conv_list;	
}

ptr< ::lestes::std::list< srp< ss_type > > > or_ics_functional_visitor::visit_or_ics_functional_for_arr_ptr_conversion(ptr< or_ics_functional_for_arr_ptr_conversion > func) 
{
	conv_list->push_front(func->target_type_get());
	return conv_list;	
}


ptr< ::lestes::std::list< srp< ss_type > > > or_ics_functional_visitor::visit_or_ics_functional_for_reference_bind_conversion(ptr< or_ics_functional_for_reference_bind_conversion > func) 
{
	conv_list->push_front(func->target_type_get());
	return conv_list;	
}

ptr< ::lestes::std::list< srp< ss_type > > > or_ics_functional_visitor::visit_or_ics_functional_for_bind_to_temporary_conversion(ptr< or_ics_functional_for_bind_to_temporary_conversion > func) 
{
	conv_list->push_front(func->target_type_get());
	return conv_list;	
}
ptr< ::lestes::std::list< srp< ss_type > > > or_ics_functional_visitor::visit_or_ics_functional_for_lval_rval_conversion(ptr< or_ics_functional_for_lval_rval_conversion >)
{
	return conv_list;
}

ptr< ::lestes::std::list< srp< ss_type > > > or_ics_functional_visitor::visit_or_ics_functional_for_user_conversion_by_conversion_function(ptr< or_ics_functional_for_user_conversion_by_conversion_function > func)
{
	conv_list->push_front(func->target_type_get());
	return conv_list;	
}

ptr< ::lestes::std::list< srp< ss_type > > > or_ics_functional_visitor::visit_or_ics_functional_for_user_conversion_by_constructor(ptr< or_ics_functional_for_user_conversion_by_constructor > func)
{
	conv_list->push_front(func->target_type_get());
	return conv_list;
}

ptr< ::lestes::std::list< srp< ss_type > > > or_ics_functional_visitor::visit_or_ics_functional_for_compound_conversion(ptr< or_ics_functional_for_compound_conversion > func)
{
	ptr< or_ics_functional_visitor > v_t = or_ics_functional_visitor::create(conv_list);
	func->outter_conversion_get()->accept_or_ics_functional_base(v_t);
	func->inner_conversion_get()->accept_or_ics_functional_base(v_t);
	return conv_list;
	
}

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);

