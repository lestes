/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief  Declaration convertor.

  Definition of visitor converting declaration to typedef.
  \author pt
*/
#include <lestes/lang/cplus/sem/ss_declaration2ss_typedef_definition.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>

package(lestes);
package(lang);
package(cplus);
package(sem);

/*!
  Visits object of type ss_declaration. 
  Default action for non-typedef, sets result to NULL.
  \pre decl != NULL
  \param decl  The declaration to visit.
*/
void ss_declaration2ss_typedef_definition::default_action(ptr<ss_declaration> decl)
{
   lassert(decl);
   result = NULL;
}

/*!
  Visits object of type ss_typedef_defitinon. 
  Sets result to the parameter.
  \pre decl != NULL
  \param decl  The declaration to visit.
*/
void ss_declaration2ss_typedef_definition::visit_ss_typedef_definition(ptr<ss_typedef_definition> decl)
{
   lassert(decl);
   result = decl;
}

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);

