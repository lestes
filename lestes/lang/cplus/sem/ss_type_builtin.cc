/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/common.hh>
#include "ss_type_builtin.g.hh"

package(lestes);
package(lang);
package(cplus);
package(sem);

#define	builtin_instace_method(TYPE)		\
ptr< ss_type > TYPE::instance()			\
{						\
	if (!the_instance) {			\
		the_instance = TYPE::create();	\
	}					\
	return the_instance;			\
}

builtin_instace_method(ss_bool)
builtin_instace_method(ss_void)
builtin_instace_method(ss_type_sint)
builtin_instace_method(ss_type_uint)
builtin_instace_method(ss_type_slong)
builtin_instace_method(ss_type_ulong)
builtin_instace_method(ss_type_sshort)
builtin_instace_method(ss_type_ushort)
builtin_instace_method(ss_type_schar)
builtin_instace_method(ss_type_uchar)
builtin_instace_method(ss_type_pchar)
builtin_instace_method(ss_type_wchar_t)
builtin_instace_method(ss_type_float)
builtin_instace_method(ss_type_double)
builtin_instace_method(ss_type_ldouble)

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);
