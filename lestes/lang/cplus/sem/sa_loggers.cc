/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/msg/logger.hh>
#include <lestes/lang/cplus/sem/sa_loggers.hh>
#include <lestes/lang/cplus/sem/sem.hh>

/*! \file
  \author TMA
 */

package(lestes);
package(lang);
package(cplus);
package(sem);

initialize_logger(sa_logger, "sa", sem_logger);
initialize_logger(sa_parser_action_logger, "sa_parser_action", sa_logger);
initialize_logger(sa_class_logger, "sa_class", sa_logger);
initialize_logger(sa_class_type_logger, "sa_class_type", sa_class_logger);
initialize_logger(sa_class_base_logger, "sa_class_base", sa_class_logger);
initialize_logger(sa_class_declaration_logger, "sa_class_declaration", sa_class_logger);
initialize_logger(sa_simple_declaration_logger, "sa_simple_declaration", sa_logger);
initialize_logger(sa_declarator_type_logger, "sa_declarator_type", sa_simple_declaration_logger);
initialize_logger(sa_param_declaration_type_logger, "sa_param_declaration_type", sa_simple_declaration_logger);
initialize_logger(sa_param_declaration_empty_logger, "sa_param_declaration_empty", sa_simple_declaration_logger);
initialize_logger(sa_param_declaration_logger, "sa_param_declaration", sa_simple_declaration_logger);
initialize_logger(sa_statements_logger, "sa_statements", sa_logger);

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);

