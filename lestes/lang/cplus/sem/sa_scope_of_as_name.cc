/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/common.hh>
#include <lestes/lang/cplus/sem/as_decl.g.hh>
//#include <lestes/lang/cplus/sem/as_id_to_ss_decl_name.g.hh>
#include <lestes/lang/cplus/sem/as_id_to_declaration_set.g.hh>
//#include <lestes/lang/cplus/sem/li_by_name_in_single_scope.g.hh>
//#include <lestes/lang/cplus/sem/lu_typedef.hh>
//#include <lestes/lang/cplus/sem/sa_context.g.hh>
//#include <lestes/lang/cplus/sem/sa_loggers.hh>
#include <lestes/lang/cplus/sem/sa_scope_of_as_name.g.hh>
//#include <lestes/lang/cplus/sem/sa_usings.g.hh>
//#include <lestes/lang/cplus/sem/ss_decl_name.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration_to_ss_decl_seq.g.hh>
#include <lestes/lang/cplus/sem/ss_misc.g.hh>
//#include <lestes/lang/cplus/sem/ss_type.g.hh>
//#include <lestes/lang/cplus/sem/ss_using_target.g.hh>
//#include <lestes/lang/cplus/syn/manager.hh>
//#include <lestes/std/lassert.hh>
//#include <lestes/msg/logger.hh>
//#include <lestes/msg/logger_util.hh>


/*! \file
  \brief retrieve declaration sequence nominated by the qualification of an as_name class instance
  
  \author TMA
 */

package(lestes);
package(lang);
package(cplus);
package(sem);

/*!
 */
ptr < ss_decl_seq > sa_scope_of_as_name::extract(ptr < ::lestes::lang::cplus::sem::as_name > n)
{
	//lassert(!syn::manager::in_disambiguation());

	lassert2(n->was_qualified_get(),"only qualified names qualify");

	ptr < ::lestes::std::set < srp < ss_declaration > > > qual_declarations =
		as_id_to_declaration_set::instance()->process(
			n->qualification_get()->identifier_get());
	lassert2(qual_declarations->size() == 1,
		"Qualification should unambiguously name a single scope declaration");
	ptr < ss_decl_seq > seq =
		ss_declaration_to_ss_decl_seq::instance()->process(*qual_declarations->begin());

	return seq;
}

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);

