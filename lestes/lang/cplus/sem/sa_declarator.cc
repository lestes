/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Declarator analyser.

  Definition of classes performing declarator structural analysis.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/std/source_location.hh>
#include <lestes/lang/cplus/sem/as_decl.g.hh>
#include <lestes/lang/cplus/sem/as_declarator_op2ss_type.g.hh>
#include <lestes/lang/cplus/sem/sa_declarator.g.hh>
#include <lestes/lang/cplus/sem/sa_decl_spec.g.hh>
#include <lestes/lang/cplus/sem/as_other.g.hh>
#include <lestes/lang/cplus/sem/ss_type_builtin.g.hh>
#include <lestes/lang/cplus/sem/ss_type.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>
#include <lestes/lang/cplus/sem/ss_misc.g.hh>
#include <lestes/lang/cplus/sem/ss_decl_name.g.hh>
#include <lestes/lang/cplus/sem/ss_expression.g.hh>
#include <lestes/lang/cplus/sem/or_or.g.hh>
#include <lestes/lang/cplus/sem/ss_statement.g.hh>

#include <iostream>

package(lestes);
package(lang);
package(cplus);
package(sem);

/*!
  Returns as_declarator_op visitor.
  \return A visitor appropriate for the context.
*/
ptr<as_declarator_op2ss_type> sa_declarator_context::create_as_declarator_op2ss_type(void)
{
	return as_declarator_op2ss_type::create(decl_specs->type_get()
	// FIXME , decl_specs->storage_class_get() != ss_storage_class::ST_TYPEDEF
	
	);
}

/*!
  Processes the declarator within the given context.
  Creates the ss_declaration.
  \pre declarator != NULL
  \param declarator  The the declarator to process.
*/
void sa_declarator::process(ptr<as_declarator> declarator)
{
	lassert(declarator);
	
	// create the appropriate visitor 
	ptr<as_declarator_op2ss_type> v = context->create_as_declarator_op2ss_type();

	typedef ::lestes::std::list< srp<as_declarator_op> > as_declarator_op_list_type;
	ptr<as_declarator_op_list_type> lst = declarator->declarator_ops_get();
	
	{
		as_declarator_op_list_type::reverse_iterator rit = lst->rbegin(), 
			rend = lst->rend(), tit = lst->rbegin();
			++tit;
		
		// walk through all declarator ops in reverse direction
		for ( ; tit != rend; ++rit, ++tit) {
			// classify individual declarator ops
			v->process(*rit,false);
		}
		
		// classify the first declarator op
		v->process(*rit,true);
	}

	// TODO pt enable after declarator has correct location type
	//ptr<source_location> loc = declarator->location_get();
	ptr<source_location> loc = source_location::create(file_info::create("",NULL),1,1);
	ptr<ss_type> type = v->type_get();
	ptr<ss_decl_seq> parent_scope = context->parent_scope_get();
	ptr<sa_decl_spec> decl_specs = context->decl_specs_get();
	
	// create the appropriate ss_declaration
	// check specifiers with knowledge of context
	if (v->parameters_get()) {
		// TODO decl_seq_compound_pair 
#if 0
		ptr<ss_sp> psp = ss_sp::create(loc,NULL,NULL,0);
		ptr<ss_sp> nsp = ss_sp::create(loc,NULL,NULL,0);
		ptr<ss_sp> dsp = ss_sp::create(loc,NULL,NULL,0);
		psp->nsp_set(dsp);
		dsp->nsp_set(nsp);
		dsp->psp_set(psp);
		nsp->psp_set(dsp);
		
		ptr<ss_decl_seq> decl_seq = ss_decl_seq::create(
			loc, // location 
			v->parameters_get(), // parameter declarations 
			context->parent_scope_get(), // parent
			NULL, // compound
			list< srp<ss_using_directive> >::create(), // using directives
			NULL // declared by
			);
 
		ptr<ss_compound_stmt> stmt = ss_compound_stmt::create(
			loc, list< srp<ss_label> >::create(),
			// FIXME parent_stmt,
			NULL,
			psp,nsp,list< srp<ss_sp> >::create(),
			decl_seq,
			list< srp<ss_statement> >::create(), dsp);
			
		decl_seq->compound_stmt_set(stmt);
		decl_seq->declared_by_set(declaration);
	decl_seq->contents_get()->push_back(class_decl_alias);
	parent_scope->contents_get()->push_back(class_decl);
#endif

		// TODO method declaration separately
	
		declaration = ss_function_declaration::create(
			loc, // location 
			// TODO
			ss_declaration_time::create(loc->order_get()), // visible time
			ss_declaration_time::create(loc->order_get()), // declaration time
			// TODO see sa_param_decl
			NULL, // declaration name
			parent_scope, // parent_scope scope
			v->type_get(), // type
			// TODO pt fill
			ss_linkage::create("C++",ss_linkage::LINKAGE_EXTERNAL), // linkage
			// TODO params
			NULL, // parameters
			NULL // ptr< ss_compound_stmt > a__ss_function_declaration__body
			);

	} else {
		if (decl_specs->virtual_flag_get() ||
			 decl_specs->explicit_flag_get() ||
			 decl_specs->inline_flag_get()) {
			// TODO pt report error: invalid specifiers
		}
		
		if (decl_specs->storage_class_get() == ss_storage_class::ST_TYPEDEF) {
		//if (specifiers->storage_get() == ss_storage_class::ST_TYPEDEF) {
			ptr < ss_declaration_time > dt = ss_declaration_time::create(loc->order_get());
			
			declaration = ss_typedef_definition::create(
				loc,
				dt, // visible since
				dt, // declaration time
				// TODO see sa_param_decl
				NULL, // declaration name
				parent_scope, // parent_scope scope
				type, // ss type
				ss_linkage::create("C++",ss_linkage::LINKAGE_NO), // linkage
				context->access_specifier_get(), // access specifier
				ss_storage_class::ST_TYPEDEF // storage class
				);

		} else {
			ptr < ss_declaration_time > dt = ss_declaration_time::create(loc->order_get());
			declaration = ss_object_declaration::create(
				loc,
				dt, // visible since
				dt, // declaration time
				// TODO
				NULL, // declaration name
				parent_scope, // parent_scope scope
				type, // ss type
				// TODO from context
				NULL, // linkage
				ss_access_specifier::ACCESS_PUBLIC,
				// TODO from context
				ss_storage_class::ST_NONE, // storage class
				ss_declaration_time::infinity(), // initialized since
				NULL // initializer
				);
		}
	}

	// add the declaration into the parent_scope scope
	parent_scope->contents_get()->push_back(declaration);
}

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);
/* vim: set ft=lestes : */
