<?xml version="1.0" encoding="iso-8859-1"?>
<!--
	The lestes compiler suite
	Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
	Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
	Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
	Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
	Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
	Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
	Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; version 2 of the License.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	See the full text of the GNU General Public License version 2, and
	the limitations in the file doc/LICENSE.

	By accepting the license the licensee waives any and all claims
	against the copyright holder(s) related in whole or in part to the
	work, its use, and/or the inability to use it.
-->
<lsd xmlns="http://lestes.jikos.cz/schemas/lsd" xmlns:h="http://www.w3.org/TR/REC-html40">

	<file-name>sa_context</file-name>

	<packages>
		<p>lestes</p>
		<p>lang</p>
		<p>cplus</p>
		<p>sem</p>
	</packages>

	<imports>
		<i>lestes/common.hh</i>
		<i>lestes/std/list.hh</i>
		<i>lestes/std/stack.hh</i>
		<i>lestes/lang/cplus/sem/ss_enums.g.hh</i>
	</imports>

	<implementation-imports>
		<i>lestes/lang/cplus/sem/as_decl.g.hh</i>
		<i>lestes/lang/cplus/sem/as_other.g.hh</i>
		<i>lestes/lang/cplus/sem/ss_misc.g.hh</i>
		<i>lestes/lang/cplus/sem/sa_declaration_specifiers.g.hh</i>
		<i>lestes/lang/cplus/sem/sa_statements.g.hh</i>
	</implementation-imports>

	<using-class name="object" packages="::lestes::std::" />
	<forward-class name="sa_statements" />

	<foreign-class name="as_name">
		<p>lestes</p>
		<p>lang</p>
		<p>cplus</p>
		<p>sem</p>
	</foreign-class>
	<foreign-class name="ss_decl_seq">
		<p>lestes</p>
		<p>lang</p>
		<p>cplus</p>
		<p>sem</p>
	</foreign-class>
	<foreign-class name="as_access_specifier">
		<p>lestes</p>
		<p>lang</p>
		<p>cplus</p>
		<p>sem</p>
	</foreign-class>
	<foreign-class name="as_declaration_specifier_seq">
		<p>lestes</p>
		<p>lang</p>
		<p>cplus</p>
		<p>sem</p>
	</foreign-class>
	<foreign-class name="sa_declaration_specifiers">
		<p>lestes</p>
		<p>lang</p>
		<p>cplus</p>
		<p>sem</p>
	</foreign-class>

	<comment>Other stuff, used because of the grammar layout</comment>

	<default-collection kind="list" />

	sa_context_stack_marker : object [ context : sa_context ]

	sa_context_manager : object [
	contexts ... sa_context;
	+void push_clone();
	+void pop();
	+sa_context_stack_marker mark();
	+void release(sa_context_stack_marker marker);
	]

	<class name="sa_context_stack_marker" base="object">
		<field name="context" type="sa_context">
		</field>
	</class>

	<class name="sa_context_manager" base="object" abstract="yes">
		<dox>
			<bri>The context manager for sa.</bri>
			<det>Provides interface for manipulationg the sa_context by the parser.</det>
		</dox>
		<field name="singleton" type="sa_context_manager" specifier="static"
			get="none" set="none" alt="none">
		</field>
		<!--
		<method name="push_clone" type="void" specifier="virtual" qualifier="abstract">
		</method>
		<method name="mark" type="sa_context_stack_marker" specifier="virtual" qualifier="abstract">
		</method>
		<method name="release" type="void" specifier="virtual" qualifier="abstract">
			<param name="marker" type="sa_context_stack_marker" />
		</method>
		-->
		<method name="current" type="sa_context" specifier="virtual" qualifier="abstract">
		</method>
		<method name="push" type="void" specifier="virtual" qualifier="abstract" >
			<param name="context" type="sa_context" />
		</method>
		<method name="pop" type="void" specifier="virtual" qualifier="abstract">
		</method>
		<method name="instance" type="sa_context_manager" specifier="static">
		</method>
	</class>

	<class name="sa_context_manager_concrete" base="sa_context_manager">
		<declare-abstract-methods skip-defined="yes" />
		<collection name="contexts" type="sa_context" kind="stack" get="none" set="none" alt="none">
		</collection>
		<field name="singleton" type="sa_context_manager_concrete" specifier="static"
			get="none" set="none" alt="none">
		</field>
		<method name="instance" type="sa_context_manager_concrete" specifier="static">
		</method>
	</class>

	<class name="sa_context" base="object">
		<dox>
			<bri>The context for sa.</bri>
			<det>Represents context for the entire structural analysis passed through the parser.</det>
		</dox>
		<field name="as" type="sa_as_context">
			<dox>
				<bri>AS context.</bri>
				<det>The as part of the context.</det>
			</dox>
		</field>
		<field name="ss" type="sa_ss_context">
			<dox>
				<bri>SS context.</bri>
				<det>The ss part of the context.</det>
			</dox>
		</field>
		<field name="sa" type="sa_sa_context">
			<dox>
				<bri>SA context.</bri>
				<det>The sa part of the context.</det>
			</dox>
		</field>
	</class>

	<class name="sa_as_context" base="object">
		<dox>
			<bri>The as context.</bri>
			<det>The as part of the context.</det>
		</dox>
		<field name="scope" type="as_name">
			<dox>
				<bri>The current scope.</bri>
			</dox>
		</field>
		<field name="access_specifier" type="as_access_specifier">
			<dox>
				<bri>The current access specifier.</bri>
			</dox>
		</field>
		<field name="declaration_specifier_seq" type="as_declaration_specifier_seq" check="" init="NULL">
			<dox>
				<bri>The current declaration specifier sequence.</bri>
			</dox>
		</field>
	</class>

	<class name="sa_ss_context" base="object">
		<dox>
			<bri>The ss context.</bri>
			<det>The ss part of the context.</det>
		</dox>
		<field name="scope" type="ss_decl_seq">
			<dox>
				<bri>The current scope.</bri>
			</dox>
		</field>
		<field name="access_specifier" type="ss_access_specifier::type">
			<dox>
				<bri>The cached access specifier.</bri>
			</dox>
		</field>
	</class>

	<class name="sa_sa_context" base="object">
		<dox>
			<bri>The ss context.</bri>
			<det>The ss part of the context.</det>
		</dox>
		<field name="declaration_specifiers" type="sa_declaration_specifiers" check="" init="NULL">
			<dox>
				<bri>The specifiers.</bri>
				<det>The current declaration specifiers.</det>
			</dox>
		</field>
	</class>

</lsd>

