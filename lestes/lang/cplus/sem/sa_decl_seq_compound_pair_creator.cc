/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/lang/cplus/sem/ss_type.g.hh>
#include <lestes/lang/cplus/sem/ss_misc.g.hh>
#include <lestes/lang/cplus/sem/ss_expression.g.hh>
#include <lestes/lang/cplus/sem/ss_decl_name.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>
#include <lestes/lang/cplus/sem/ss_statement.g.hh>
#include <lestes/lang/cplus/sem/ss_enums.g.hh>
#include <lestes/lang/cplus/sem/sa_decl_seq_compound_pair_creator.g.hh>
#include <lestes/std/pair.hh>

#include <lestes/std/dumper.hh>
#include <fstream>

/*! \file
  \author TMA
  \author jikos
 */

package(lestes);
package(lang);
package(cplus);
package(sem);

typedef list < srp < ss_sp > > sp_list;

ptr < pair < srp < ss_decl_seq >, srp < ss_compound_stmt > > >sa_decl_seq_compound_pair_creator::process (ptr < source_location > loc, ptr < ss_decl_seq > parent_scope, ptr < ss_compound_stmt > parent_stmt)
{
	/* create some sequence points */
	ptr < ss_sp > psp = ss_sp::create(loc, NULL, NULL, 0);
	ptr < ss_sp > nsp = ss_sp::create(loc, NULL, NULL, 0);
	ptr < ss_sp > dsp = ss_sp::create(loc, NULL, NULL, 0);
	/* link them together */
	psp->nsp_set(dsp);
	dsp->nsp_set(nsp);
	dsp->psp_set(psp);
	nsp->psp_set(dsp);

	ptr<sp_list> sps = sp_list::create();
	sps->push_back(psp);
	sps->push_back(dsp);
	sps->push_back(nsp);

	ptr < ss_decl_seq > decl_seq = ss_decl_seq::create(
			loc,
			list< srp<ss_declaration> >::create(), /* contents */
			parent_scope, /* parent */
			NULL, /* compound */
			list< srp<ss_using_directive> >::create(), /* using directives */
			NULL /* declared by */);

	ptr < ss_compound_stmt > stmt = ss_compound_stmt::create(
			loc, list < srp < ss_label > >::create(),
			parent_stmt, psp, nsp, sps,
			decl_seq,
			list < srp < ss_statement > >::create(), dsp);

	decl_seq->compound_stmt_set(stmt);

	return pair < srp < ss_decl_seq >, srp < ss_compound_stmt > >::create(decl_seq, stmt);
}

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);

