/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
 *
 * This file contains implementaion of visitors used in overload resoltuion.
 * Now this means only visitor on or_or_functional
 * \author jikos
 */

#include <lestes/lang/cplus/sem/or_visitor.v.g.hh>
#include <lestes/lang/cplus/sem/or_or.g.hh>
#include <lestes/std/source_location.hh>
#include <lestes/lang/cplus/sem/or_actual_visitors.g.hh>

package(lestes);
package(lang);
package(cplus);
package(sem);

void or_or_functional_to_enum::visit_or_or_functional_concrete(ptr< or_or_functional_concrete> )
{
	result_set(OR_OR_FUNCTIONAL_CONCRETE);
}

void or_or_functional_to_enum::visit_or_or_functional_comma(ptr< or_or_functional_comma> )
{
	result_set(OR_OR_FUNCTIONAL_COMMA);
}

void or_or_functional_to_enum::visit_or_or_functional_addrof(ptr< or_or_functional_addrof> )
{
	result_set(OR_OR_FUNCTIONAL_ADDROF);
}

void or_or_functional_to_enum::visit_or_or_functional_literal(ptr< or_or_functional_literal> )
{
	result_set(OR_OR_FUNCTIONAL_LITERAL);
}

void or_or_functional_to_enum::visit_or_or_functional_this(ptr< or_or_functional_this> )
{
	result_set(OR_OR_FUNCTIONAL_THIS);
}

void or_or_functional_to_enum::visit_or_or_functional_ambiguous(ptr< or_or_functional_ambiguous> )
{
	result_set(OR_OR_FUNCTIONAL_AMBIGUOUS);
}

void or_or_functional_to_enum::visit_or_or_functional_noviable(ptr< or_or_functional_noviable> )
{
	result_set(OR_OR_FUNCTIONAL_NOVIABLE);
}

void or_or_functional_to_enum::visit_or_or_functional_func_decl_set(ptr< or_or_functional_func_decl_set> )
{
	result_set(OR_OR_FUNCTIONAL_FUNC_DECL_SET);
}

void or_or_functional_to_enum::visit_or_or_functional_decl(ptr< or_or_functional_decl> )
{
	result_set(OR_OR_FUNCTIONAL_DECL);
}




end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);

