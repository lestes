<?xml version="1.0" encoding="UTF-8"?>
<!--
	The lestes compiler suite
	Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
	Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
	Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
	Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
	Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
	Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
	Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; version 2 of the License.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	See the full text of the GNU General Public License version 2, and
	the limitations in the file doc/LICENSE.

	By accepting the license the licensee waives any and all claims
	against the copyright holder(s) related in whole or in part to the
	work, its use, and/or the inability to use it.
-->
<lsd xmlns="http://lestes.jikos.cz/schemas/lsd" xmlns:h="http://www.w3.org/TR/REC-html40">

	<file-name>or</file-name>

	<packages>
	<p>lestes</p>
	<p>lang</p>
	<p>cplus</p>
	<p>sem</p>
	</packages>

	<imports>
		<i>lestes/common.hh</i>
		<i>lestes/std/list.hh</i>
		<i>lestes/std/set.hh</i>
		<i>lestes/lang/cplus/sem/ss_decl_name_visitor.v.g.hh</i>
	</imports>

	<implementation-imports>
		<i>lestes/lang/cplus/sem/or_visitor.v.g.hh</i>
		<i>lestes/lang/cplus/sem/ss_declaration.g.hh</i>
		<i>lestes/lang/cplus/sem/ss_misc.g.hh</i>
		<i>lestes/std/source_location.hh</i>
	</implementation-imports>

	<hh-header></hh-header>

	<hh-footer>
	</hh-footer>

	<cc-header></cc-header>

	<cc-footer></cc-footer>

	<forward-class name="ss_declaration"/>
	<forward-class name="ss_expression"/>
	<forward-class name="ss_builtin_operator_declaration"/>
	<forward-class name="ss_function_declaration"/>
	<forward-class name="ss_declaration_time"/>
	<forward-class name="ss_decl_seq" />
	<forward-class name="or_or_functional" />

	<foreign-class name="object">
		<p>lestes</p>
		<p>std</p>
	</foreign-class>

	<foreign-class name="source_location">
		<p>lestes</p>
		<p>std</p>
	</foreign-class>


	<!-- <visitor-file href="or_visitor.lvd" /> -->

	<!-- <include href="or_visitor.v.lsd" /> -->
	<include href="ss_decl_name_visitor.v.lsd" />

	<class name="or_builtin_operator_declaration_creator" base="ss_decl_name_visitor" create="none" >
		<dox>
			<bri>	Singleton class for purposes of creation of buitltin operator delcarations</bri>
			<det>	This class contains a visitor on ss_operator_name and also servers as creator
				of declarations for builtin operators. It is implemented as singleton - in it's
				instance() method the decl seq for builtin op declarations is created and then 
				proper declarations according to [13.6] are inserted into it.
				
				It is visitor on all ss_operators. First, the lookup in the builtin_op scope 
				is performed. Then we have a few special builtin ops, which take place for every
				type. If the overload resolution finds empty set of candidates, for assign, unary
				and and comma, we have to create the builtin operator declaration for these operators,
				which is then used. This is done in this visitor. All other function bodies should be nops. 
				See [13.3.1.2/3] and [13.3.1.2/9]
				</det>
                </dox>

		<declare-abstract-methods />
		<field name="builtin_op_decl_seq" type="ss_decl_seq" />
		<method name="construct_builtin_op" type="void" >
			<param name="op" type="ptr &lt; ss_decl_name &gt;"/>
			<param name="types" type="ptr &lt; ::lestes::std::list&lt; srp&lt; ss_type &gt; &gt; &gt;"/>
			<param name="return_type" type="ptr &lt; ss_type &gt;"/>
		</method>
		<field name="the_instance" specifier="static" type="or_builtin_operator_declaration_creator" get="none" set="none" alt="none" />
		<method name="instance" specifier="static" type="or_builtin_operator_declaration_creator" />
		<method name="add_vq_ref_vq_ref" type="void" specifier="template &lt;typename op, typename type1&gt;" />
		<method name="add_nonref_vq_ref_int" type="void" specifier="template &lt;typename op, typename type1&gt;" />
		<method name="add_vq_ptr_ref_vq_ptr_ref" type="void" specifier="template &lt;typename op, typename type1&gt;" />
		<method name="add_vq_ptr_nonref_vq_ptr_ref_int" type="void" specifier="template &lt;typename op, typename type1&gt;" />
		<method name="add_ref_ptr" type="void" specifier="template &lt;typename op, typename type1&gt;" />
		<method name="add_ref_ptr_const" type="void" specifier="template &lt;typename op, typename type1&gt;" />
		<method name="add_ptr_ptr" type="void" specifier="template &lt;typename op&gt;" />
		<method name="add_nonref_nonref" type="void" specifier="template &lt;typename op, typename type1, typename type2&gt;" />
		<method name="add_nonref_nonref_nonref" type="void" specifier="template &lt;typename op, typename type1, typename type2, typename type3&gt;" />
		<method name="add_vq_ref_vq_ref_nonref" type="void" specifier="template &lt;typename op, typename type1, typename type2&gt;" />
		<method name="add_vq_ref_vq_ref_nonref_REF" type="void" specifier="template &lt;typename op, typename type1, typename type2&gt;" />
		<method name="add_nonref_nonref_nonref_nonref" type="void" specifier="template &lt;typename op, typename type1, typename type2, typename type3, typename type4&gt;" />
		<method name="add_ptr_ptr_int" type="void" specifier="template &lt;typename op, typename type1&gt;" />
		<method name="add_ptr_int_ptr" type="void" specifier="template &lt;typename op, typename type1&gt;" />
		<method name="add_int_ptr_ptr" type="void" specifier="template &lt;typename op, typename type1&gt;" />
		<method name="add_bool_ptr_ptr" type="void" specifier="template &lt;typename op, typename type1&gt;" />
		<method name="add_vq_ptr_ref_vq_ptr_ref_ptr" type="void" specifier="template &lt;typename op, typename type1&gt;" />
		<method name="add_vq_ptr_ref_vq_ptr_ref_int" type="void" specifier="template &lt;typename op, typename type1&gt;" />
	</class>
</lsd>

