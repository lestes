/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*!
	\file
	\brief Visitors returning lstring for ss_type.
	\author jaz
*/

#include <lestes/lang/cplus/sem/ss_decl_name2lstring_base.g.hh>
#include <lestes/lang/cplus/sem/ss_type.g.hh>
#include <lestes/lang/cplus/sem/ss_type_builtin.g.hh>
#include <lestes/lang/cplus/sem/ss_misc.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>
#include <lestes/lang/cplus/sem/ss_decl_name.g.hh>
#include <lestes/lang/cplus/sem/ss_type2lstring_base.g.hh>
#include <sstream>

package(lestes);
package(lang);
package(cplus);
package(sem);


ptr< ss_type2mangled_id > ss_type2mangled_id::instance()
{
	if ( !singleton_instance_get()) {
		singleton_instance_set(ss_type2mangled_id::create());
	}	
	
	return singleton_instance_get();
}


void ss_type2mangled_id::visit_ss_const(ptr< ::lestes::lang::cplus::sem::ss_const > type) {
	lassert(type);
	::std::ostringstream oss;
	oss << "K" << process(type->what_get());
	result_set(oss.str());
}

void ss_type2mangled_id::visit_ss_volatile(ptr< ::lestes::lang::cplus::sem::ss_volatile > type){
	lassert(type);
	::std::ostringstream oss;
	oss << "V" << process(type->what_get());
	result_set(oss.str());
}

void ss_type2mangled_id::visit_ss_const_volatile(ptr< ::lestes::lang::cplus::sem::ss_const_volatile > type){
	lassert(type);
	::std::ostringstream oss;
	oss << "VK" << process(type->what_get());
	result_set(oss.str());
}

void ss_type2mangled_id::visit_ss_reference(ptr< ::lestes::lang::cplus::sem::ss_reference > type){
	lassert(type);
	::std::ostringstream oss;
	oss << "R" << process(type->what_get());
	result_set(oss.str());
}

void ss_type2mangled_id::visit_ss_pseudoreference(ptr< ::lestes::lang::cplus::sem::ss_pseudoreference > type){
	lassert(type);
	::std::ostringstream oss;
	oss << "R" << process(type->what_get());
	result_set(oss.str());
}

void ss_type2mangled_id::visit_ss_pointer(ptr< ::lestes::lang::cplus::sem::ss_pointer > type){
	lassert(type);
	::std::ostringstream oss;
	oss << "P" << process(type->what_get());
	result_set(oss.str());
}

void ss_type2mangled_id::visit_ss_enum(ptr< ::lestes::lang::cplus::sem::ss_enum > type){
	lassert(type);
	ptr<ss_object_declaration> decl = (type.dncast<ss_class>())->decl_get().dncast<ss_object_declaration>();
	lstring name  = decl->name_get()->accept_ss_decl_name2lstring_base(ss_decl_name2mangled_name::instance());
	result_set(name);
}

void ss_type2mangled_id::visit_ss_array(ptr< ::lestes::lang::cplus::sem::ss_array > type){
	lassert(type);
	//TODO
	lassert2(false,"Not implemented yet.");
	result_set("FIXME");
}

void ss_type2mangled_id::visit_ss_member_pointer(ptr< ::lestes::lang::cplus::sem::ss_member_pointer > type){
	lassert(type);
	//TODO
	lassert2(false,"Not implemented yet.");
	result_set("FIXME");
}

void ss_type2mangled_id::visit_ss_function(ptr< ::lestes::lang::cplus::sem::ss_function > type){
	lassert(type);
	//TODO
	lassert2(false,"Not implemented yet.");
	result_set("FIXME");
}

void ss_type2mangled_id::visit_ss_member_function(ptr< ::lestes::lang::cplus::sem::ss_member_function > type){
	lassert(type);
	//TODO
	lassert2(false,"Not implemented yet.");
	result_set("FIXME");
}



void ss_type2mangled_id::visit_ss_class(ptr< ::lestes::lang::cplus::sem::ss_class > type){
	lassert(type);
	ptr<ss_object_declaration> decl = (type.dncast<ss_class>())->decl_get().dncast<ss_object_declaration>();
	lstring name  = decl->name_get()->accept_ss_decl_name2lstring_base(ss_decl_name2mangled_name::instance());
	result_set(name);
}

void ss_type2mangled_id::visit_ss_union(ptr< ::lestes::lang::cplus::sem::ss_union > type){
	lassert(type);
	//TODO:   uint?
	result_set(lstring("j"));
}

void ss_type2mangled_id::visit_ss_bool(ptr< ::lestes::lang::cplus::sem::ss_bool > type){
	lassert(type);
	result_set(lstring("b"));
}

void ss_type2mangled_id::visit_ss_void(ptr< ::lestes::lang::cplus::sem::ss_void > type){
	lassert(type);
	result_set(lstring("v"));
}

void ss_type2mangled_id::visit_ss_type_sint(ptr< ::lestes::lang::cplus::sem::ss_type_sint > type){
	lassert(type);
	result_set(lstring("i"));
}

void ss_type2mangled_id::visit_ss_type_uint(ptr< ::lestes::lang::cplus::sem::ss_type_uint > type) {
	lassert(type);
	result_set(lstring("j"));
}

void ss_type2mangled_id::visit_ss_type_slong(ptr< ::lestes::lang::cplus::sem::ss_type_slong > type) {
	lassert(type);
	result_set(lstring("l"));
}

void ss_type2mangled_id::visit_ss_type_ulong(ptr< ::lestes::lang::cplus::sem::ss_type_ulong > type){
	lassert(type);
	result_set(lstring("m"));
}

void ss_type2mangled_id::visit_ss_type_sshort(ptr< ::lestes::lang::cplus::sem::ss_type_sshort > type){
	lassert(type);
	result_set(lstring("s"));
}

void ss_type2mangled_id::visit_ss_type_ushort(ptr< ::lestes::lang::cplus::sem::ss_type_ushort > type){
	lassert(type);
	result_set(lstring("t"));
}

void ss_type2mangled_id::visit_ss_type_wchar_t(ptr< ::lestes::lang::cplus::sem::ss_type_wchar_t > type){
	lassert(type);
	result_set(lstring("w"));
}

void ss_type2mangled_id::visit_ss_type_schar(ptr< ::lestes::lang::cplus::sem::ss_type_schar > type){
	lassert(type);
	result_set(lstring("a"));
}

void ss_type2mangled_id::visit_ss_type_uchar(ptr< ::lestes::lang::cplus::sem::ss_type_uchar > type){
	lassert(type);
	result_set(lstring("h"));
}

void ss_type2mangled_id::visit_ss_type_pchar(ptr< ::lestes::lang::cplus::sem::ss_type_pchar > type){
	lassert(type);
	result_set(lstring("c"));
}

void ss_type2mangled_id::visit_ss_type_float(ptr< ::lestes::lang::cplus::sem::ss_type_float > type){
	lassert(type);
	result_set(lstring("f"));
}

void ss_type2mangled_id::visit_ss_type_double(ptr< ::lestes::lang::cplus::sem::ss_type_double > type){
	lassert(type);
	result_set(lstring("d"));
}

void ss_type2mangled_id::visit_ss_type_ldouble(ptr< ::lestes::lang::cplus::sem::ss_type_ldouble > type){
	lassert(type);
	result_set(lstring("e"));
}



end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);

