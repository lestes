/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/common.hh>

#include <lestes/std/list.hh>
#include <lestes/lang/cplus/sem/ss_misc.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>
#include <lestes/lang/cplus/sem/ss_decl_name.g.hh>
#include <lestes/lang/cplus/sem/ss_statement.g.hh>
#include <lestes/lang/cplus/sem/ss_type.g.hh>
#include <lestes/lang/cplus/sem/ss_type_builtin.g.hh>
#include <lestes/lang/cplus/sem/ss_expression.g.hh>
#include <lestes/lang/cplus/sem/ss_expr_binary_op.g.hh>
#include <lestes/lang/cplus/sem/ss_expr_unary_op.g.hh>
#include <lestes/lang/cplus/sem/ss_expr_funcall.g.hh>
#include <lestes/lang/cplus/sem/ss_enums.g.hh>
#include <lestes/lang/cplus/sem/ss_literal_info.g.hh>
#include <lestes/lang/cplus/sem/or_or.g.hh>

#include <lestes/backend/backend.g.hh>

#include <lestes/std/dumper.hh>
#include <lestes/msg/logger.hh>
#include <fstream>
#include <iostream>

using namespace ::lestes::msg;

package(lestes);
package(lang);
package(cplus);
package(sem);


using namespace ::lestes::std;

extern void ss2pi_start(ptr< ss_translation_unit >);
extern void summary_seq_points(ptr< ss_sp > x);
int man(){

//ptr<ss_compound_stmt> cmps1;// = ss_compound_stmt::create();

ptr<ss_translation_unit> tu = ss_translation_unit::create( ss_decl_seq::root_instance() );

#define NOINIT ss_decl_stmt::IK_DEFAULT_INITIALIZATION

typedef  ::lestes::std::list< srp< ss_declaration > >  listdecl;
typedef  ::lestes::std::list< srp< ss_type > >  listtype;
typedef  ::lestes::std::list< srp< ss_statement > >  liststmt;
typedef  ::lestes::std::list< srp< ss_label > >  listlbl;
typedef  ::lestes::std::list< srp< ss_expression > >  listexpr;
typedef  ::lestes::std::list< srp< ss_se > >  listse;
typedef  ::lestes::std::list< srp< ss_sp > >  listsp;


ptr<ss_sp> sp13= ss_sp::create(NULL,NULL,NULL,listse::create(),listse::create(),NULL,0);
ptr<ss_sp> sp12= ss_sp::create(NULL,NULL,NULL,listse::create(),listse::create(),NULL,0);
ptr<ss_sp> sp11= ss_sp::create(NULL,NULL,NULL,listse::create(),listse::create(),NULL,0);
ptr<ss_sp> sp10= ss_sp::create(NULL,NULL,NULL,listse::create(),listse::create(),NULL,0);
ptr<ss_sp> sp9 = ss_sp::create(NULL,NULL,NULL,listse::create(),listse::create(),NULL,1);
ptr<ss_sp> sp8 = ss_sp::create(NULL,NULL,NULL,listse::create(),listse::create(),NULL,1);
ptr<ss_sp> sp7 = ss_sp::create(NULL,NULL,NULL,listse::create(),listse::create(),NULL,0);
ptr<ss_sp> sp6 = ss_sp::create(NULL,NULL,NULL,listse::create(),listse::create(),NULL,0);
ptr<ss_sp> sp5 = ss_sp::create(NULL,NULL,NULL,listse::create(),listse::create(),NULL,0);
ptr<ss_sp> sp4 = ss_sp::create(NULL,NULL,NULL,listse::create(),listse::create(),NULL,0);
ptr<ss_sp> sp3 = ss_sp::create(NULL,NULL,NULL,listse::create(),listse::create(),NULL,0);
ptr<ss_sp> sp2 = ss_sp::create(NULL,NULL,NULL,listse::create(),listse::create(),NULL,0);
ptr<ss_sp> sp1 = ss_sp::create(NULL,NULL,NULL,listse::create(),listse::create(),NULL,0);


ptr<ss_decl_seq> ds1 = ss_decl_seq::root_instance();
/* commented out by Rudo
ptr<ss_decl_seq> ds1 = ss_decl_seq::create(
		NULL,	//location
		listdecl::create(),	//decl_seq
		NULL,	//parent
		NULL,	//compound
		NULL,	//using dir
		NULL	//declaration
	);
*/

ptr<ss_compound_stmt> cs1 = ss_compound_stmt::root_instance();
/* commented out by Rudo
ptr<ss_compound_stmt> cs1 = ss_compound_stmt::create(
		NULL,	//location
		listlbl::create(),	//labels
		NULL,	//parent
		NULL,	//psp
		NULL,	//nsp
		listsp::create(),	//sps
		NULL,	//decl
		liststmt::create()	//statements
			);
*/
			
ptr<ss_function_declaration> fd1 = ss_function_declaration::create(
	NULL,	//loc
	ss_declaration_time::create(0),	//visible
	ss_declaration_time::create(0),	//declt time
	NULL,	//name
	NULL,	//contain scope
	NULL,	//type
	NULL,	//linkage
	NULL,	//params
	NULL	//body
	//false,	//pure virtual
	//false,	//virtuality
	//false 	//ellipsis
	);

ptr<ss_ordinary_name> on1 = ss_ordinary_name::create(NULL,"main");

ptr<ss_type> sint = ss_type_sint::instance();

ptr<ss_function> tfun1 = ss_function::create(sint,listtype::create(),false);

ptr<ss_decl_seq> ds3 = ss_decl_seq::create(NULL,listdecl::create(),ds1,NULL,NULL,NULL);

ptr<ss_decl_seq> ds2 = ss_decl_seq::create(NULL,listdecl::create(),ds3,NULL,NULL,NULL);

ptr<ss_compound_stmt> cs3 = ss_compound_stmt::create(NULL,listlbl::create(),cs1,NULL,NULL,listsp::create(),ds3,liststmt::create(),NULL);

ptr<ss_compound_stmt> cs2 = ss_compound_stmt::create(NULL,listlbl::create(),cs3,sp1,sp7,listsp::create(),ds2,liststmt::create(),sp6);

ptr<ss_decl_stmt> dst1 = ss_decl_stmt::create(NULL,listlbl::create(),cs2,sp2,sp3,listsp::create(),NULL,listexpr::create(),NOINIT);

ptr<ss_object_declaration> od1 = ss_object_declaration::create(NULL,ss_declaration_time::create(0),ss_declaration_time::create(0),NULL,NULL,sint,NULL,ss_declaration_time::infinity(),NULL);

ptr<ss_ordinary_name> on2 = ss_ordinary_name::create(NULL,"i");

ptr<ss_expr_stmt> est1 = ss_expr_stmt::create(NULL,listlbl::create(),NULL,sp4,sp5,listsp::create(),NULL);

ptr<ss_assign> as1 = ss_assign::create(NULL,sint,sp4,sp5,NULL,NULL);

ptr<ss_se> ase1 = ss_se::create(NULL,NULL,sp4,sp5);

ptr<ss_var_ref> var1 = ss_var_ref::create(NULL,sint,sp4,sp5,NULL);

ptr<ss_literal_info> linfo1 = ss_integral_literal_info::create(sint,"1");

ptr<ss_literal> lit1 = ss_literal::create(NULL,sint,sp4,sp5,NULL); 

ptr<ss_decl_stmt> dst2 = ss_decl_stmt::create(NULL,listlbl::create(),cs1,NULL,NULL,listsp::create(),NULL,listexpr::create(),NOINIT);

ptr< ss_se > fse2 = ss_se::create(NULL,NULL,sp8,sp9);

ptr< ss_funcall > fc1 = ss_funcall::create(NULL,sint,sp8,sp9,listexpr::create(),NULL);

ptr<ss_expr_stmt> est2 = ss_expr_stmt::create(NULL,listlbl::create(),cs2,sp6,sp7,listsp::create(),NULL);





ptr<ss_decl_stmt> dst3 = ss_decl_stmt::create(NULL,listlbl::create(),cs3,NULL,NULL,listsp::create(),NULL,listexpr::create(),NOINIT);

ptr< ss_parameter_declaration > dpar1 = ss_parameter_declaration::create(NULL,ss_declaration_time::create(0),ss_declaration_time::create(0),NULL,NULL,sint,NULL,ss_access_specifier::ACCESS_PUBLIC,ss_storage_class::ST_NONE,ss_declaration_time::infinity(),NULL,0);

ptr< ss_ordinary_name > on3 = ss_ordinary_name::create(NULL,"j");

ptr< ss_get > get1 = ss_get::create(NULL,sint,sp8,sp9,NULL);

ptr< ss_var_ref > var2 = ss_var_ref::create(NULL,sint,sp6,sp8,NULL);

ptr< ss_fake_declaration > fake1 = ss_fake_declaration::create(NULL,ss_declaration_time::create(0),ss_declaration_time::create(0),NULL,NULL,sint,NULL,ss_access_specifier::ACCESS_PUBLIC,ss_storage_class::ST_NONE,NULL);

ptr< ss_compound_stmt_declaration > csd1 = ss_compound_stmt_declaration::create(NULL,ss_declaration_time::create(0),ss_declaration_time::create(0),NULL,NULL,NULL,NULL,cs2);


ptr< ss_get > get2 = ss_get::create(NULL,sint,sp10,sp11,NULL);

ptr< ss_var_ref > var3 = ss_var_ref::create(NULL,sint,sp10,sp11,NULL);

ptr< ss_return > ret1 = ss_return::create(NULL,NULL,cs2,sp10,sp11,listsp::create(),NULL);

ptr< ss_get > get3 = ss_get::create(NULL,sint,sp10,sp11,NULL);

ptr< ss_var_ref > var4 = ss_var_ref::create(NULL,sint,sp10,sp11,NULL);

ptr< ss_add > add1 = ss_add::create(NULL,sint,sp10,sp11,NULL,NULL);

ptr< ss_linkage > link1 = ss_linkage::create("C",ss_linkage::LINKAGE_EXTERNAL);

//**************************************************************************************************************************

//Rudo: tu->global_scope_set(ds1);

tfun1->params_get()->push_back(sint);

ds1->contents_get()->push_back(fd1);
//Rudo: ds1->compound_stmt_set(cs1);

//Rudo: cs1->decl_seq_set(ds1);
cs1->statements_get()->push_back(dst2);

//Rudo: dst2->parent_set(cs1);
dst2->decl_set(fd1);

fd1->name_set(on1);
fd1->type_set(tfun1);
fd1->body_set(cs2);
fd1->contained_in_set(ds1);
fd1->parameters_set(ds3);
fd1->linkage_set(link1);

//Rudo: cs2->parent_set(cs1);
//Rudo: cs2->decl_seq_set(ds2);
cs2->statements_get()->push_back(dst1);
cs2->statements_get()->push_back(est1);
cs2->statements_get()->push_back(est2);
cs2->statements_get()->push_back(ret1);


//Rudo: ds2->parent_set(ds1);
ds2->compound_stmt_set(cs2);
ds2->contents_get()->push_back(fake1);
ds2->contents_get()->push_back(od1);
ds2->declared_by_set(csd1);

//Rudo: dst1->parent_set(cs2);
dst1->decl_set(od1);

od1->contained_in_set(ds2);
od1->name_set(on2);

est1->expression_set(as1);
est1->sequence_points_get()->push_back(sp4);
est1->sequence_points_get()->push_back(sp5);

as1->left_set(var1);
as1->right_set(lit1);
//as1->side_effect_set(ase1);

ase1->from_set(as1);

var1->var_decl_set(od1);

lit1->value_set(linfo1);

est2->expression_set(fc1);
est2->sequence_points_get()->push_back(sp6);
est2->sequence_points_get()->push_back(sp7);
est2->sequence_points_get()->push_back(sp8);
est2->sequence_points_get()->push_back(sp9);

fc1->function_set(fd1);
fc1->args_get()->push_back(get1);

fse2->from_set(fc1);



ds3->compound_stmt_set(cs3);
ds3->contents_get()->push_back(dpar1);
ds3->declared_by_set(fd1);

cs3->statements_get()->push_back(dst3);

dst3->decl_set(dpar1);

dpar1->name_set(on3);
dpar1->contained_in_set(ds3);

get1->expr_set(var2);

var2->var_decl_set(fake1);

fake1->real_decl_set(dpar1);
fake1->name_set(on3);
fake1->contained_in_set(ds2);


get2->expr_set(var3);

var3->var_decl_set(fake1);

ret1->result_set(add1);
ret1->sequence_points_get()->push_back(sp10);
ret1->sequence_points_get()->push_back(sp11);

get3->expr_set(var4);

var4->var_decl_set(fake1);

add1->left_set(get2);
add1->right_set(get3);


		  sp1->nsp_set(sp2);
sp2->psp_set(sp1);sp2->nsp_set(sp3);
sp3->psp_set(sp2);sp3->nsp_set(sp4);
sp4->psp_set(sp3);sp4->nsp_set(sp5);sp4->nse_get()->push_back(ase1);
sp5->psp_set(sp4);sp5->nsp_set(sp6);sp5->pse_get()->push_back(ase1);
sp6->psp_set(sp5);sp6->nsp_set(sp7);
sp7->psp_set(sp6);sp7->nsp_set(sp10);
sp8->psp_set(sp6);sp8->nsp_set(sp9);sp8->nse_get()->push_back(fse2);
sp9->psp_set(sp8);sp9->nsp_set(sp7);sp9->pse_get()->push_back(fse2);
sp10->psp_set(sp7);sp10->nsp_set(sp11);
sp11->psp_set(sp10);sp11->nsp_set(sp12);
sp12->psp_set(sp11);sp12->nsp_set(sp13);
sp13->psp_set(sp12);



			//FIXME chybi vyplnene filedy ss.statement.sequence-points

::std::ofstream f("example1.xml");
dumper::dump( f, tu );
f.close();

ss2pi_start(tu);
summary_seq_points(sp1);
lestes::backend::backend::create(::std::cout)->main();

return 1;
}//man


end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);


int main(){

#ifndef unchecked_pointers
//CXXFLAGS+=-Dunchecked_pointers
::std::cerr<<"Example skipped, checked pointers set.\n";
return 0;
#endif

 logger::init("example1.log.cfg");

 lestes::lang::cplus::sem::man();
}
