/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Unit test.
  
  Unit test for class ss_type2info.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/sem/ss_type2info.g.hh>
#include <lestes/lang/cplus/sem/ss_type.g.hh>
#include <lestes/lang/cplus/sem/ss_type_builtin.g.hh>

package(lestes);
package(lang);
package(cplus);
package(sem);

using namespace ::std;

/*!
  \brief Tests ss_type2info class.

  Performs testing of ss_type2info class.
*/
void ss_type2info_test(void)
{
	ptr<ss_type2info> ssti = ss_type2info::create();
	ptr<ss_type> sint = ss_type_sint::instance();
	ptr<ss_type> st;
	
	st = sint;
	st->accept_ss_type_visitor(ssti);
	lassert(ssti->info_get() == ss_type2info::UNKNOWN);
	
	st = ss_const::instance(sint);
	st->accept_ss_type_visitor(ssti);
	lassert(ssti->info_get() == ss_type2info::SS_CONST);

	st = ss_volatile::instance(sint);
	st->accept_ss_type_visitor(ssti);
	lassert(ssti->info_get() == ss_type2info::SS_VOLATILE);

	st = ss_const::instance(ss_volatile::instance(sint));
	st->accept_ss_type_visitor(ssti);
	lassert(ssti->info_get() == ss_type2info::SS_CONST_VOLATILE);

	st = ss_pointer::instance(sint);
	st->accept_ss_type_visitor(ssti);
	lassert(ssti->info_get() == ss_type2info::SS_POINTER);

	st = ss_member_pointer::instance(sint,sint);
	st->accept_ss_type_visitor(ssti);
	lassert(ssti->info_get() == ss_type2info::SS_MEMBER_POINTER);

	st = ss_reference::instance(sint);
	st->accept_ss_type_visitor(ssti);
	lassert(ssti->info_get() == ss_type2info::SS_REFERENCE);
}

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);

/*!
  \brief Main function.

  Runs the unit test in different namespace.
*/
int main(void)
{
	::lestes::lang::cplus::sem::ss_type2info_test();
	return 0;
}
/* vim: set ft=lestes : */
