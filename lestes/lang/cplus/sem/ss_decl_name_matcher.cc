/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/lang/cplus/sem/ss_decl_name.g.hh>
#include <lestes/lang/cplus/sem/ss_decl_name_matcher.g.hh>

package(lestes);
package(lang);
package(cplus);
package(sem);

bool ss_decl_name_matcher::match( ptr< ss_decl_name > n1, ptr< ss_decl_name > n2 )
{
	lassert( n1 && n2 );
	/* can be instance() when suported */
	ptr<ss_decl_name_matcher> m = ss_decl_name_matcher::create();

	lassert( m->state == READY );

	n1->accept_ss_decl_name_visitor(m);
	n2->accept_ss_decl_name_visitor(m);
	n1->accept_ss_decl_name_visitor(m);

	lassert( m->state == HAVE_RESULT );
	m->state = READY;
	return m->result;
}

#define define_matcher_visit( type )				\
	void ss_decl_name_matcher::visit_##type( ptr< type > o )\
	{							\
		static ptr<type> arg1 = NULL;			\
		switch (state) {				\
		case READY:					\
			lassert( !arg1 );			\
			arg1 = o;				\
			state = GOT_ARG1;			\
			break;					\
		case GOT_ARG1:					\
			if (arg1)				\
				result = arg1->matches_same(o);	\
			else					\
				result = false;			\
			state = NEED_CLEANUP;			\
			break;					\
		case NEED_CLEANUP:				\
			lassert( arg1 );			\
			arg1 = NULL;				\
			state = HAVE_RESULT;			\
			break;					\
		case HAVE_RESULT:				\
			lassert(false);				\
			break;					\
		}						\
	}

define_matcher_visit( ss_ordinary_name );
define_matcher_visit( ss_conversion_name );
define_matcher_visit( ss_dummy_name );
define_matcher_visit( ss_operator_new );
define_matcher_visit( ss_operator_delete );
define_matcher_visit( ss_operator_new_array );
define_matcher_visit( ss_operator_delete_array );
define_matcher_visit( ss_operator_add );
define_matcher_visit( ss_operator_sub );
define_matcher_visit( ss_operator_mul );
define_matcher_visit( ss_operator_div );
define_matcher_visit( ss_operator_mod );
define_matcher_visit( ss_operator_bxor );
define_matcher_visit( ss_operator_band );
define_matcher_visit( ss_operator_bor );
define_matcher_visit( ss_operator_bnot );
define_matcher_visit( ss_operator_lnot );
define_matcher_visit( ss_operator_assign );
define_matcher_visit( ss_operator_assign_add );
define_matcher_visit( ss_operator_assign_sub );
define_matcher_visit( ss_operator_assign_mul );
define_matcher_visit( ss_operator_assign_div );
define_matcher_visit( ss_operator_assign_mod );
define_matcher_visit( ss_operator_assign_bxor );
define_matcher_visit( ss_operator_assign_band );
define_matcher_visit( ss_operator_assign_bor );
define_matcher_visit( ss_operator_assign_shl );
define_matcher_visit( ss_operator_assign_shr );
define_matcher_visit( ss_operator_shr );
define_matcher_visit( ss_operator_shl );
define_matcher_visit( ss_operator_sbl );
define_matcher_visit( ss_operator_sbg );
define_matcher_visit( ss_operator_sbng );
define_matcher_visit( ss_operator_sbnl );
define_matcher_visit( ss_operator_sbe );
define_matcher_visit( ss_operator_sbne );
define_matcher_visit( ss_operator_land );
define_matcher_visit( ss_operator_lor );
define_matcher_visit( ss_operator_inc );
define_matcher_visit( ss_operator_dec );
define_matcher_visit( ss_operator_comma );
define_matcher_visit( ss_operator_access );
define_matcher_visit( ss_operator_access_member );
define_matcher_visit( ss_operator_function_call );
define_matcher_visit( ss_operator_array );
define_matcher_visit( ss_operator_ternary );

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);
