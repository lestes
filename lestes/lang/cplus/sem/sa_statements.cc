/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*!
	\file
	\brief SS statements creation called from parser.y
	\author egg
*/

#include <lestes/std/list.hh>
#include <lestes/std/source_location.hh>
#include <lestes/lang/cplus/sem/declaration_broadcasting.hh>
#include <lestes/lang/cplus/sem/sa_statements.g.hh>
#include <lestes/lang/cplus/sem/sa_context.g.hh>
#include <lestes/lang/cplus/sem/sa_decl_seq_compound_pair_creator.g.hh>
#include <lestes/lang/cplus/sem/sa_declaration_specifiers.g.hh>
#include <lestes/lang/cplus/sem/sa_deconstruct_spse.g.hh>
#include <lestes/lang/cplus/sem/as_expr.g.hh>
#include <lestes/lang/cplus/sem/as_decl.g.hh>
#include <lestes/lang/cplus/sem/as_other.g.hh>
#include <lestes/lang/cplus/sem/as_statements.g.hh>
#include <lestes/lang/cplus/sem/or_or.g.hh>
#include <lestes/lang/cplus/sem/or_ics.g.hh>
#include <lestes/lang/cplus/sem/ss_statement.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>
#include <lestes/lang/cplus/sem/ss_decl_name.g.hh>
#include <lestes/lang/cplus/sem/ss_literal_info.g.hh>
#include <lestes/lang/cplus/sem/ss_type.g.hh>
#include <lestes/lang/cplus/sem/ss_type_builtin.g.hh>
#include <lestes/lang/cplus/sem/ss_misc.g.hh>
#include <lestes/lang/cplus/syn/token.hh>
#include <lestes/lang/cplus/syn/manager.hh>
#include <lestes/lang/cplus/sem/sa_loggers.hh>
#include <lestes/lang/cplus/sem/visitor.v.g.hh>
#include <lestes/msg/logger.hh>
#include <lestes/msg/logger_util.hh>
#include <lestes/lang/cplus/sem/sa_statements.m.hh>
#include <cstdlib>


package(lestes);
package(lang);
package(cplus);
package(sem);

typedef ::lestes::std::list< srp< ss_sp > > sp_list;
typedef ::lestes::std::list< srp< ss_label > > label_list;
typedef ::lestes::std::list< srp< ss_expression > > expr_list;
typedef ::lestes::std::list< srp< ss_statement > > stmt_list;
typedef ::lestes::std::list< srp< ss_declaration > > decl_list;
typedef ::lestes::std::source_location location;

#define current_context sa_context_manager::instance()->current()
#define current_ss	current_context->ss_get()
#define current_as	current_context->as_get()
#define current_sa	current_context->sa_get()
#define current_decl_seq	current_ss->scope_get()
#define current_compound	current_decl_seq->compound_stmt_get()
#define current_statements	current_compound->statements_get()
#define current_psp current_compound->destructor_sp_get()->psp_get()
#define current_nsp current_compound->destructor_sp_get()

/* if in disambiguation, bail out immediately.
 * this should be the first line in all actions called from parser.y */
#define DISAMB if (::lestes::lang::cplus::syn::manager::in_disambiguation()) return

// 2 new sequence points
#define ALPHABETASPS(loc) \
	ptr<ss_sp> alpha = ss_sp::create(loc,current_psp,NULL,0);\
	ptr<ss_sp> beta = ss_sp::create(loc,alpha,current_nsp,0);\
	alpha->nsp_set(beta);\
	current_psp->nsp_set(alpha);\
	current_nsp->psp_set(beta);\
	ptr<sp_list> sps = sp_list::create();\
	sps->push_back(alpha);\
	sps->push_back(beta);

ptr<sa_statements> sa_statements::the_instance = the_instance;
	
ptr<sa_statements> sa_statements::instance()
{
	if (the_instance) return the_instance;
	sa_statements_logger << "creating sa_statements instance\n" << msg::eolog;
	declaration_finished->attach(decl_stmt_listener::instance());
	return the_instance = new sa_statements(NULL,NULL);
}

void decl_stmt_listener::run(ptr<ss_declaration> decl)
{
	DISAMB;
	sa_statements_logger << "object declaration\n" << msg::eolog;
	lassert(decl);
	decl->accept_ss_declaration_visitor(last_declaration_memorizer::instance());
}

void last_declaration_memorizer::default_action(ptr<ss_declaration> decl)
{
	lassert(decl);
	sa_statements_logger << "non-object declaration => creating declaration statement\n" << msg::eolog;
	sa_statements::instance()->last_declaration_set(decl);
	ALPHABETASPS(decl->location_get());
	ptr<ss_decl_stmt> stmt = ss_decl_stmt::create(
			decl->location_get(), // location
			label_list::create(), // labels pointing at me
			current_compound, // parent
			alpha, // psp
			beta, // nsp
			sps, // list of SP
			decl, // declaration
			expr_list::create(), // args
			ss_decl_stmt::IK_NOT_APPLY_INITIALIZATION // initializer kind
			);
	current_statements->push_back(stmt);
}

void last_declaration_memorizer::visit_ss_object_declaration(ptr<ss_object_declaration> decl)
{
	sa_statements::instance()->last_declaration_set(decl);
}

void sa_statements::declarator_simple(ptr<location> loc)
{
	DISAMB;
	sa_statements_logger << "declarator simple -> inserting decl stmt\n" << msg::eolog;
	lassert(loc);
	lassert2(last_declaration,"declarator without declaration?");
	ALPHABETASPS(loc);
	ptr<ss_decl_stmt> stmt = ss_decl_stmt::create(
			loc, // location
			label_list::create(), // labels pointing at me
			current_compound, // parent
			alpha, // psp
			beta, // nsp
			sps, // list of SP
			last_declaration_get(), // declaration
			expr_list::create(), // args
			ss_decl_stmt::IK_DEFAULT_INITIALIZATION // initializer kind
			);
	current_statements->push_back(stmt);
}

void sa_statements::declarator_init(ptr<location> loc, ptr<as_initializer_clause> as)
{
	DISAMB;
	sa_statements_logger << "declarator with initializer -> inserting decl stmt\n" << msg::eolog;
	lassert(as);
	lassert(loc);
	lassert2(last_declaration,"declarator without declaration?");
	ALPHABETASPS(loc);
	ptr<expr_list> args = initializer_clause_converter::create(
			alpha,
			beta,
			sps,
			last_declaration->type_get()
			)->process(as);
	ptr<ss_decl_stmt> stmt = ss_decl_stmt::create(
			loc, // location
			label_list::create(), // labels pointing at me
			current_compound, // parent
			alpha, // psp
			beta, // nsp
			sps, // list of SP
			last_declaration_get(), // declaration
			args, // args
			ss_decl_stmt::IK_COPY_INITIALIZATION // initializer kind
			);
	current_statements->push_back(stmt);
}

void sa_statements::declarator_ctor(ptr<location> loc, ptr<as_expression_list> as)
{
	DISAMB;
	sa_statements_logger << "declarator with constructor -> inserting decl stmt\n" << msg::eolog;
	lassert(as);
	lassert(!as->l_get()->empty());
	lassert(loc);
	lassert2(last_declaration,"declarator without declaration?");
	ptr<expr_list> args = expr_list::create();
	ALPHABETASPS(loc);
	ptr<ss_sp> psp = alpha;
	list< srp<as_expression> >::iterator i = as->l_get()->begin();
	for(; i != as->l_get()->end(); i++) {
		// convert each expression
		ptr<ss_expression> expr = sa_expression_converter::create(psp,beta,sps)->process(*i);
		// the next one will be between beta and it's predecessor
		psp = beta->psp_get();
		// remember the expression
		args->push_back(expr);
	}
	ptr<ss_decl_stmt> stmt = ss_decl_stmt::create(
			loc, // location
			label_list::create(), // labels pointing at me
			current_compound, // parent
			alpha, // psp
			beta, // nsp
			sps, // list of SP
			last_declaration_get(), // declaration
			args, // args
			ss_decl_stmt::IK_DIRECT_INITIALIZATION // initializer kind
			);
	current_statements->push_back(stmt);
}

void initializer_clause_converter::visit_as_initializer_clause_expression(ptr<as_initializer_clause_expression> as)
{
	ptr<ss_expression> expr =
		// allways require a conversion to target type
		sa_expression_converter::create(psp,nsp,sps,type,NULL)->process(as->expression_get());
	result_set(expr_list::create());
	result_get()->push_back(expr);
}

void initializer_clause_converter::visit_as_initializer_clause_braced(ptr<as_initializer_clause_braced> as)
{
	lassert(as);
	result_set(expr_list::create());
	ptr<ss_sp> actual_psp = nsp;
	::lestes::std::list< srp< as_initializer_clause > >::iterator i = as->initializers_get()->begin();
	for(; i != as->initializers_get()->end(); i++) {
		// convert recursively each sub-brace
		ptr<expr_list> expressions =
			initializer_clause_converter::create(actual_psp,nsp,sps,type)->process(*i);
		// the next one will be between beta and it's predecessor
		actual_psp = nsp->psp_get();
		// append each expression to output
		expr_list::iterator j = expressions->begin();
		// FIXME! it's not quite OK to connect all of it into one list
		for(; j != expressions->end(); j++){
			result_get()->push_back(*j);
		}
	}
}

void sa_statements::enter_function(ptr< location > loc, ptr<as_function_definition> as_func)
{
	DISAMB;
	sa_statements_logger << "entering function\n" << msg::eolog;
	lassert(as_func);
	ptr<ss_function_declaration> ss_func = last_declaration_get().dncast<ss_function_declaration>();
	lassert(ss_func);

	// create new compound with all the stuff
	ptr<ss_decl_seq> decl_seq = sa_decl_seq_compound_pair_creator::instance()->process(
			loc, // location
			ss_func->parameters_get(), // parent scope (function parameters scope)
			ss_func->parameters_get()->compound_stmt_get() // parent statement
			)->first;
	
	// fill in a scope declaration
	ptr< ss_compound_stmt_declaration > decl = ss_compound_stmt_declaration::create(
			loc,
			ss_declaration_time::infinity(), // visible since
			ss_declaration_time::create(loc->order_get()), // decl time got from location
			ss_dummy_name::create(loc),// ss_name
			ss_func->parameters_get(), // contained in
			ss_void::instance(), // type
			ss_linkage::create("C++",ss_linkage::LINKAGE_NO), // linkage
			decl_seq->compound_stmt_get() // compound stmt
			);
	decl_seq->declared_by_set(decl);

	// ss_func is current from now
	current_function_set(ss_func);

	// fill in a new context
	ptr<sa_as_context> as = sa_as_context::create(
			as_func->declarator_get()->name_get(), // the function name denotes scope
			current_as->access_specifier_get() // access specifier comes from context
			); // create the as part of the new context
	ptr<sa_ss_context> ss = sa_ss_context::create(
			decl_seq,
			current_ss->access_specifier_get()
			); // create the ss part of the new context
	
	ptr<sa_context> ctx = sa_context::create(as,ss,current_sa);
	sa_context_manager::instance()->push(ctx); // switch to the new context

	ss_func->body_set(current_compound);

	// for each parameter add a fake declaration into the new scope (to avoid redeclaration)
	ptr<decl_list> params=ss_func->parameters_get()->contents_get();
	for (decl_list::iterator i = params->begin() ; i!=params->end(); i++){
		sa_statements_logger << "creating fake declaration for parameter\n" << msg::eolog;
		insert_fake_declaration(*i);
	}
}

void sa_statements::leave_function()
{
	DISAMB;
	sa_statements_logger << "leaving function\n" << msg::eolog;
	current_function_set(NULL);
	sa_context_manager::instance()->pop();
}

void sa_statements::enter_scope(ptr< location > loc)
{
	DISAMB;
	if (current_compound->behavior_get() == ss_compound_stmt::NO_CREATE ) {
		current_compound->behavior_set(ss_compound_stmt::NO_LEAVE);
		sa_statements_logger << "not entering scope\n" << msg::eolog;
		return;
	}
	sa_statements_logger << "entering scope\n" << msg::eolog;
	ptr<ss_decl_seq> decl_seq = sa_decl_seq_compound_pair_creator::instance()->process(
			loc, // location
			current_decl_seq, // parent scope
			current_compound // parent statement
			)->first; // create new compound with all the stuff

	ptr< ss_compound_stmt_declaration > decl = ss_compound_stmt_declaration::create(
			loc,
			ss_declaration_time::infinity(), // visible since
			ss_declaration_time::create(loc->order_get()), // decl time got from location
			ss_dummy_name::create(loc),// ss_name
			current_decl_seq, // contained in
			ss_void::instance(), // type
			ss_linkage::create("C++",ss_linkage::LINKAGE_NO), // linkage
			decl_seq->compound_stmt_get() // compound stmt
			);
	decl_seq->declared_by_set(decl); // fill in a scope declaration (dummy)

	// add the new compound statement into current scope
	current_statements->push_back(decl_seq->compound_stmt_get());

	// connect sequence points
	ptr<ss_sp> alpha = current_psp;
	ptr<ss_sp> beta = current_nsp;
	ptr<ss_sp> gamma = decl_seq->compound_stmt_get()->psp_get();
	ptr<ss_sp> delta = decl_seq->compound_stmt_get()->nsp_get();
	alpha->nsp_set(gamma);
	gamma->psp_set(alpha);
	delta->nsp_set(beta);
	beta->psp_set(delta);
	
	ptr<sa_as_context> as = sa_as_context::create(
			as_name::create(loc,NULL,as_empty_id::create(loc)), // FIXME? is dummy name all right?
			current_as->access_specifier_get()
			); // create the as part of the new context
	ptr<sa_ss_context> ss = sa_ss_context::create(
			decl_seq,
			current_ss->access_specifier_get()
			); // create the ss part of the new context
	
	ptr<sa_context> ctx = sa_context::create(as,ss,current_sa);
	sa_context_manager::instance()->push(ctx); // push the new context on context stack
}

void sa_statements::leave_scope()
{
	DISAMB;
	if (current_compound->behavior_get() == ss_compound_stmt::NO_LEAVE ) {
		current_compound->behavior_set(ss_compound_stmt::NORMAL);
		sa_statements_logger << "not leaving scope\n" << msg::eolog;
		return;
	}
	sa_statements_logger << "leaving scope\n" << msg::eolog;
	sa_context_manager::instance()->pop();
}

void sa_statements::insert_fake_declaration(ptr<ss_declaration> input_decl)
{
	ptr<ss_declaration> decl = get_real_declaration::instance()->process(input_decl);
	// fake is a copy of the original declaration
	ptr<ss_fake_declaration> fake = ss_fake_declaration::create(
			decl->location_get(),
			decl->visible_since_get(),
			decl->decl_time_get(),
			decl->name_get(),
			decl->contained_in_get(),
			decl->type_get(),
			decl->linkage_get(),
			decl.dncast<ss_object_declaration>()
			);
	current_decl_seq->contents_get()->push_back(fake);
}

void sa_statements::expression_stmt(ptr< ::lestes::lang::cplus::sem::as_expression> as )
{
	DISAMB;
	sa_statements_logger << "inserting expression statement\n" << msg::eolog;
	lassert(current_function);
	
	ALPHABETASPS(as->location_get());

	// visitor gives me ss_expression
	ptr<ss_expression> expr = sa_expression_converter::create(alpha,beta,sps)->process(as);
	if (!expr) return; // empty expression -> the statement is discarded

	// create the resulting SS statement
	ptr<ss_expr_stmt> stmt = ss_expr_stmt::create(
		as->location_get(), // location
		label_list::create(), // labels pointing at me
		current_compound, // parent scope
		alpha, // psp
		beta, // nsp
		sps, // list of sp in this statement
		expr // transformed expression
	);
	
	// append the SS statement into resulting scope
	current_statements->push_back(stmt);
}

void sa_statements::return_stmt(ptr< ::lestes::lang::cplus::sem::as_return_statement > as )
{
	DISAMB;
	sa_statements_logger << "inserting return statement\n" << msg::eolog;
	lassert(current_function);
	
	ALPHABETASPS(as->location_get());

	// resolve return type of current function
	ptr<ss_type> type = current_function->type_get().dncast<ss_function>()->returns_get();
	
	// visitor gives me ss_expression of that type
	ptr<ss_expression> expr = sa_expression_converter::create(alpha,beta,sps,type,NULL)->process(as->value_get());
	
	// create the resulting SS statement
	ptr<ss_return> stmt = ss_return::create(
		as->location_get(), // location
		label_list::create(), // labels pointing at me
		current_compound, // parent scope
		alpha, // psp
		beta, // nsp
		sps, // list of sp in this statement
		expr // transformed expression
	);
	
	// append the SS statement into resulting scope
	current_statements->push_back(stmt);
}

void sa_expression_converter::convert(ptr<as_expression> as)
{
	sa_statements_logger << "converting AS expression to SS\n" << msg::eolog;
	lassert(as);
	// transform the AS expression to functional
	ptr<or_or_functional> functional = sa_deconstruct_spse::create()->process(as);

	// transform the functional to SS expression
	ptr<ss_expression> expr = (*functional)(alpha,beta,as->location_get(),sps);	

	if (type) {
		sa_statements_logger << "type conversion\n" << msg::eolog;

		// resolve the conversion sequence
		ptr<or_ics_functional> ics = or_find_ics(functional,type);
		if (!ics){
			report << conversion_not_found << as->location_get() ;
			exit(1);
		}

		// proceed the conversion sequence
		expr = (*ics)(expr, expr->psp_get(), expr->nsp_get());
	}
	
	result = expr;
}

void sa_expression_converter::visit_as_empty_expression(ptr<as_empty_expression> as)
{
	sa_statements_logger << "empty expression\n" << msg::eolog;
	lassert(as);
	result = NULL;
}

ptr<ss_compound_stmt> sa_statements::new_compound(ptr<location> loc)
{
	ptr<ss_compound_stmt> stmt = sa_decl_seq_compound_pair_creator::instance()->process(
			loc, // location
			current_decl_seq, // parent scope
			current_compound // parent statement
			)->second; // create new compound with all the stuff

	ptr< ss_compound_stmt_declaration > decl = ss_compound_stmt_declaration::create(
			loc,
			ss_declaration_time::infinity(), // visible since
			ss_declaration_time::create(loc->order_get()), // decl time got from location
			ss_dummy_name::create(loc),// ss_name
			current_decl_seq, // contained in
			ss_void::instance(), // type
			ss_linkage::create("C++",ss_linkage::LINKAGE_NO), // linkage
			stmt // compound stmt
			);
	stmt->decl_seq_get()->declared_by_set(decl); // fill in a scope declaration (dummy)
	return stmt;
}

void sa_statements::if_head(ptr<location> loc, ptr<as_condition> cond)
{
	DISAMB;
	sa_statements_logger << "if head\n" << msg::eolog;
	lassert(loc);
	lassert(cond);
	ALPHABETASPS(loc);
	ptr<ss_expression> expr = condition_to_expression::create(alpha,beta,sps)->process(cond);

	/*! FIXME
		Location of the 'then' and 'else' compound statements is set to the 'if' token location.
		It would be difficult to set it properly because the statements must be created earlier
		than the parser reaches them. Who cares, anyway...
	 */
	ptr<ss_compound_stmt> cthen = new_compound(loc);
	cthen->behavior_set(ss_compound_stmt::NO_CREATE);
	ptr<ss_compound_stmt> celse = new_compound(loc);
	celse->behavior_set(ss_compound_stmt::NO_CREATE);
	// connect sequence points
	beta->nsp_set(cthen->psp_get());
	cthen->psp_get()->psp_set(beta);
	cthen->nsp_get()->nsp_set(celse->psp_get());
	celse->psp_get()->psp_set(cthen->nsp_get());
	celse->nsp_get()->nsp_set(current_nsp);
	current_nsp->psp_set(celse->nsp_get());
	
	ptr<ss_if_stmt> stmt = ss_if_stmt::create(
			loc, // location
			label_list::create(), // labels pointing at me
			current_compound, // parent
			alpha, // psp
			beta, // nsp
			sps, // list of SP
			expr, // condition
			cthen, // cthen
			celse // celse
			);
	current_statements->push_back(stmt);
	sa_statements_logger << "inserting if statement\n" << msg::eolog;
	current_statements->push_back(cthen);
	sa_statements_logger << "inserting then compound\n" << msg::eolog;
	current_statements->push_back(celse);
	sa_statements_logger << "inserting else compound\n" << msg::eolog;
	
	enter_subcompound(cthen);
}

void sa_statements::while_head(ptr<location> loc, ptr<as_condition> cond)
{
	DISAMB;
	sa_statements_logger << "while head\n" << msg::eolog;
	lassert(loc);
	lassert(cond);
	ALPHABETASPS(loc);
	ptr<ss_expression> expr = condition_to_expression::create(alpha,beta,sps)->process(cond);

	/*! FIXME
		The location is not quite correct. See if statement.
	 */
	ptr<ss_compound_stmt> body = new_compound(loc);
	body->behavior_set(ss_compound_stmt::NO_CREATE);
	// connect sequence points
	beta->nsp_set(body->psp_get());
	body->psp_get()->psp_set(beta);
	
	ptr<ss_while> stmt = ss_while::create(
			loc, // location
			label_list::create(), // labels pointing at me
			current_compound, // parent
			alpha, // psp
			beta, // nsp
			sps, // list of SP
			body, // body
			expr // condition
			);
	current_statements->push_back(stmt);
	sa_statements_logger << "inserting while statement\n" << msg::eolog;
	current_statements->push_back(body);
	sa_statements_logger << "inserting while body compound\n" << msg::eolog;
	
	enter_subcompound(body);
}

void sa_statements::for_head(ptr<source_location> loc, ptr<as_condition> cond, ptr<as_expression> iter)
{
	DISAMB;
	sa_statements_logger << "for head\n" << msg::eolog;
	lassert(loc);
	lassert(cond);
	ALPHABETASPS(loc);
	ptr<ss_expression> expr = condition_to_expression::create(alpha,beta,sps)->process(cond);

	ptr<ss_sp> iter_alpha = ss_sp::create(iter->location_get(), NULL, NULL, 0);
	ptr<ss_sp> iter_beta = ss_sp::create(iter->location_get(), NULL, NULL, 0);
	ptr<sp_list> iter_sps = sp_list::create();
	iter_sps->push_back(iter_alpha);
	iter_sps->push_back(iter_beta);
	ptr<ss_expression> iter_expr = sa_expression_converter::create(iter_alpha,iter_beta,iter_sps)->process(iter);
	ptr<ss_expr_stmt> iter_stmt;
	if (iter_expr) iter_stmt= ss_expr_stmt::create(
			iter->location_get(), // location
			label_list::create(), // labels pointing at me
			current_compound, // parent
			iter_alpha,
			iter_beta,
			iter_sps,
			iter_expr
			);
	
	/*! FIXME
		The location is not correct. See if statement.
	 */
	ptr<ss_compound_stmt> body = new_compound(loc);
	body->behavior_set(ss_compound_stmt::NO_CREATE);
	// connect sequence points
	beta->nsp_set(body->psp_get());
	body->psp_get()->psp_set(beta);
	
	ptr<ss_for> stmt = ss_for::create(
			loc, // location
			label_list::create(), // labels pointing at me
			current_compound, // parent
			alpha, // psp
			beta, // nsp
			sps, // list of SP
			body, // body
			expr, // condition
			iter_stmt // iteration
			);

	// connect sequence points of the iteration statement { {for(;;iter_stmt){..} __here__ } }
	if (iter_stmt) {
		iter_stmt->psp_get()->psp_set(stmt->nsp_get());
		stmt->nsp_get()->nsp_set(iter_stmt->psp_get());
		iter_stmt->nsp_get()->nsp_set(current_compound->destructor_sp_get());
		current_compound->destructor_sp_get()->psp_set(iter_stmt->nsp_get());
	}
	
	sa_statements_logger << "inserting for statement\n" << msg::eolog;
	current_statements->push_back(stmt);
	sa_statements_logger << "inserting for body compound\n" << msg::eolog;
	current_statements->push_back(body);
	current_statements->push_back(iter_stmt);

	enter_subcompound(body);
}

void sa_statements::for_inner_action(ptr<source_location> loc)
{
	DISAMB;
	lassert(loc);
	sa_statements_logger << "entering 'for' inner compound\n" << msg::eolog;
	ptr<ss_compound_stmt> inner = new_compound(loc);
	current_statements->push_back(inner);
	enter_subcompound(inner);
}

void condition_to_expression::visit_as_empty_condition(ptr<as_empty_condition> cond)
{
	lassert(cond);
	sa_statements_logger << "condition - empty\n" << msg::eolog;
	// Missing condition.
	// In for-cycle it's ok and the 'true' expression is created.
	// Otherwise the error was already reported by parser and this is recovery.
	result = ss_literal::create(cond->location_get(),ss_bool::instance(),psp,nsp,
				ss_integral_literal_info::create_from_number(ss_bool::instance(),1));
}

void condition_to_expression::visit_as_condition_expression(ptr<as_condition_expression> cond)
{
	lassert(cond);
	sa_statements_logger << "condition - expression\n" << msg::eolog;
	result = sa_expression_converter::create(psp,nsp,sps,ss_bool::instance(),NULL)->process(cond->expression_get());
}

void condition_to_expression::visit_as_condition_declaration(ptr<as_condition_declaration> cond)
{
	lassert(cond);
	sa_statements_logger << "condition - declaration\n" << msg::eolog;
	// declaration of the variable is the last in current scope
	ptr<ss_declaration> decl = current_decl_seq->contents_get()->back();
	// create a functional (needed for conversion purposes)
	ptr<or_or_functional_decl> functional = or_or_functional_decl::create(
			ss_pseudoreference::instance(decl->type_get()),
			decl
			);
	// transform the functional to SS expression
	ptr<ss_expression> expr = (*functional)(psp,nsp,cond->location_get(),sps);
	sa_statements_logger << "type conversion to bool\n" << msg::eolog;
	// resolve the conversion sequence
	ptr<or_ics_functional> ics = or_find_ics(functional,ss_bool::instance());
	if (!ics){
		report << conversion_not_found << cond->location_get() ;
		exit(1);
	}
	// proceed the conversion sequence
	expr = (*ics)(expr, expr->psp_get(), expr->nsp_get());

	result = expr;
}

void sa_statements::condition_decl(ptr<location> loc, ptr<as_expression> inizer)
{
	lassert(loc);
	lassert(inizer);
	lassert2(last_declaration,"declarator without declaration?");
	sa_statements_logger << "creating declaration statement from condition\n" << msg::eolog;
	ALPHABETASPS(loc);
	ptr<ss_expression> expr = sa_expression_converter::create(alpha,beta,sps)->process(inizer);
	ptr<expr_list> exprs = expr_list::create();
	exprs->push_back(expr);

	ptr<ss_decl_stmt> stmt = ss_decl_stmt::create(
			loc, // location
			label_list::create(), // labels pointing at me
			current_compound, // parent
			alpha, // psp
			beta, // nsp
			sps, // list of SP
			last_declaration_get(), // declaration
			exprs, // args
			ss_decl_stmt::IK_COPY_INITIALIZATION // initializer kind
			);
	current_statements->push_back(stmt);
}

void sa_statements::enter_subcompound(ptr<ss_compound_stmt> scope)
{
	lassert(scope);
	sa_statements_logger << "entering subcompound\n" << msg::eolog;
	
	// remember the declarations, if there are some
	ptr<decl_list> decls = scope->parent_get()->decl_seq_get()->contents_get();

	// switch context to the given scope
	ptr<sa_as_context> as = sa_as_context::create(
			as_name::create(
				scope->location_get(),
				NULL,
				as_empty_id::create(scope->location_get())
				), // FIXME? is dummy name all right?
			current_as->access_specifier_get()
			); // create the as part of the new context
	ptr<sa_ss_context> ss = sa_ss_context::create(
			scope->decl_seq_get(),
			current_ss->access_specifier_get()
			); // create the ss part of the new context
	ptr<sa_context> ctx = sa_context::create(as,ss,current_sa);
	sa_context_manager::instance()->push(ctx); // push the new context on context stack

	for( decl_list::iterator i = decls->begin(); i!=decls->end(); i++ ) {
			sa_statements_logger << "creating fake declaration\n" << msg::eolog;
			// fake is a copy of the original declaration
			insert_fake_declaration(*i);
	}
}

void sa_statements::enter_else()
{
	DISAMB;
	enter_subcompound(
		sa_context_manager::instance()->current()->ss_get()->scope_get()
			->compound_stmt_get()->statements_get()->back().dncast<ss_compound_stmt>()
	);
}

void get_real_declaration::visit_ss_fake_declaration(ptr<ss_fake_declaration> fake)
{
	lassert(fake);
	result = fake->real_decl_get();
}

void get_real_declaration::default_action(ptr<ss_declaration> decl)
{
	lassert(decl);
	result = decl;
}

/* NOTE:
		as->identifier_get()->token_get()->wrapped_token_get()->value_get(), // ucn_string extracted from bison token
*/


end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);

