/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Declarator operator visitor.

  Definition of classes performing declarator operator analysis.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/std/source_location.hh>
#include <lestes/lang/cplus/lex/cpp_token.hh>
#include <lestes/lang/cplus/sem/as_declarator_op2ss_type.g.hh>
#include <lestes/lang/cplus/sem/sa_param_declaration_type.g.hh>
#include <lestes/lang/cplus/sem/sa_param_declaration_empty.g.hh>
#include <lestes/lang/cplus/sem/as_cv_qualifier2ss_type.g.hh>
#include <lestes/lang/cplus/sem/ss_type.g.hh>
#include <lestes/lang/cplus/sem/ss_type2cv_unqualified.g.hh>
#include <lestes/lang/cplus/sem/as_decl.g.hh>
#include <lestes/lang/cplus/sem/ss_type2info.g.hh>
#include <lestes/lang/cplus/sem/ss_type_builtin.g.hh>
#if 0
#include <lestes/lang/cplus/sem/ss_misc.g.hh>
#include <lestes/lang/cplus/sem/sa_declarator.g.hh>
#include <lestes/lang/cplus/sem/sa_decl_spec.g.hh>
#include <lestes/lang/cplus/sem/as_other.g.hh>
#include <lestes/lang/cplus/sem/ss_decl_name.g.hh>
#include <lestes/lang/cplus/sem/ss_expression.g.hh>
#endif

package(lestes);
package(lang);
package(cplus);
package(sem);

/*!
  Processes the declarator operator, updating the fields.
  \pre dop != NULL
  \param dop  The declarator operator to process.
*/
void as_declarator_op2ss_type::process(ptr<as_declarator_op> dop)
{
	dop->accept_as_declarator_op_visitor(this);
}

/*!
  Visits the star declarator operator.
  \pre dop != NULL
  \dop  The declarator operator to visit.
*/
void as_declarator_op2ss_type::visit_as_ptr_op_star(ptr<as_ptr_op_star> dop)
{
	lassert(dop);

	typedef list< srp<as_cv_qualifier> > as_cv_qualifier_list;
	
	ptr<as_cv_qualifier2ss_type> acqst =
		as_cv_qualifier2ss_type::create(ss_pointer::instance(type));

	ptr<as_cv_qualifier_list> lst = dop->cv_qualifiers_get();
	
	for (as_cv_qualifier_list::iterator it = lst->begin(), end = lst->end();
			it != end; ++it) {
		acqst->process(*it);
	}

	type = acqst->type_get();
}

/*!
  Visits the ampersand declarator operator.
  \pre dop != NULL
  \dop  The declarator operator to visit.
*/
void as_declarator_op2ss_type::visit_as_ptr_op_amp(ptr<as_ptr_op_amp> dop)
{
	lassert(dop);
	
	// TODO pt make own visitor for is_void???
	if (ss_type2cv_unqualified::create()->process(type)->is_void()) {
		// TODO pt report error: reference to void
		return;
	}

	if (ss_type2info::create()->process(type) == ss_type2info::SS_REFERENCE) {
		// TODO pt report error: reference to reference
		return;
	}
	
	type = ss_reference::instance(type);
}

/*!
  Visits the function declarator operator.
  \pre dop != NULL
  \dop  The declarator operator to visit.
*/
void as_declarator_op2ss_type::visit_as_declarator_op_func(ptr<as_declarator_op_func> dop)
{
	lassert(dop);
  
	typedef ::lestes::std::list< srp<as_param_declaration> > as_param_declaration_list_type;
	typedef ::lestes::std::list< srp<ss_type> > ss_type_list_type;
  
	if (ss_type2info::create()->process(type) == ss_type2info::SS_FUNCTION) {
		// TODO pt report error: function returning function
	}

	ptr<as_param_decl_clause> apdc = dop->parameters_get();
	bool ellipsis = apdc->ellipsis_get();

	ptr<ss_type_list_type> sstl = ss_type_list_type::create();

	ptr<as_param_declaration_list_type> apdl = apdc->parameters_get();
	ptr<sa_param_declaration_type> sapdt = sa_param_declaration_type::create();

	//  special (void) parameter 
	if (apdl->size() != 1 || !sa_param_declaration_empty::create()->process(apdl->front())) {
		for (as_param_declaration_list_type::iterator it = apdl->begin(), end = apdl->end();
					it != end; ++it) {
			
			// fixes types for function type use
			sapdt->process(*it);
			sstl->push_back(sapdt->type_get());
		}
	}

	// create the type from the op
	type = ss_function::instance(type,sstl,ellipsis);
}

#if 0
/*!
  Visits the function declarator operator.
  \pre dop != NULL
  \dop  The declarator operator to visit.
*/
void as_declarator_op2ss_type::visit_as_declarator_op_func(ptr<as_declarator_op_func> dop)
{
	lassert(dop);
  
	typedef ::lestes::std::list< srp<as_param_declaration> > as_param_declaration_list_type;
	typedef ::lestes::std::list< srp<ss_type> > ss_type_list_type;
  // TODO pt remove typedef ::lestes::std::list< srp<ss_parameter_declaration> > ss_parameter_declaration_list_type;
  
	ptr<ss_type2info> ssti = ss_type2info::create();
	ssti->process(type);

	if (ssti->info_get() == ss_type2info::SS_FUNCTION) {
		// TODO pt report error: function returning function
	}

	ptr<as_param_decl_clause> apdc = dop->parameters_get();
	bool ellipsis = apdc->ellipsis_get();

	ptr<ss_type_list_type> sstl = ss_type_list_type::create();
	parameters = ss_type_list_type::create();

	ptr<as_param_declaration_list_type> apdl = apdc->parameters_get();
	ptr<sa_param_declaration_type> sapdt = sa_param_declaration_type::create(apdl->size() == 1/*TODO context*/);
	
	{
		as_param_declaration_list_type::iterator it = apdl->begin(), end = apdl->end();
		
		if (apdl->size() == 1)
		if (it != end) {
			sapdt->process(*it);
			++it;

			if (it
		}

			
	for (as_param_declaration_list_type::iterator it = apdl->begin(), end = apdl->end();
				it != end; ++it) {
		
		sapdt->process(*it);
		// fixes types for function type use
		sstl->push_back(sapdt->norm_type_get());
		parameters->push_back(sapdt->param_type_get());
	}
	
	if (sstl->size() == 1 && sslt->begin()->is_void()) {
		// one void parameter means no parameters at all
		sstl->clear();
		parameters->clear();
	} else {
		// check for void
		for (ss_type_list_type::iterator it =  sstl->begin(), end = sstl->end();
				it != end; ++it) {
			if ((*it)->is_void()) {
				// TODO pt report error: parameter declared void
				*it = ss_sint::instance();
			}
		}
	}

	// create the type from the op

	// TODO pt how to distinguish ss_function from ss_member_function ???
	// by looking at the name and context
	// not in this place, here only ss_function will be possible, as this is not used at toplevel
	type = ss_function::instance(type,sstl,ellipsis);
	type = ss_member_function::instance(type,sstl,ellipsis);
}
#endif

/*!
  Visits the array declarator operator.
  \pre dop != NULL
  \dop  The declarator operator to visit.
*/
void as_declarator_op2ss_type::visit_as_declarator_op_array(ptr<as_declarator_op_array> dop)
{
	lassert(dop);
	
	if (type->is_void()) {
		// TODO pt report error: array of void
		// make it int defaultly
		type = ss_type_sint::instance();
	} else {
		ptr<ss_type2info> sst2i = ss_type2info::create();
		sst2i->process(type);
		switch (ss_type2info::create()->process(type)) {
			case ss_type2info::SS_REFERENCE:
				// TODO pt report error: array of references
				type = ss_type_sint::instance();
				break;
			case ss_type2info::SS_FUNCTION:
				// TODO pt report error: array of functions
				type = ss_type_sint::instance();
				break;
			default:
				break;
		}
	}
	// TODO pt check type is not abstract class

	// TODO pt get the actulal size, check 0 ??
	// t_size size = evaluate(dop->constant_expression_get());

	t_size size = 1;
	type = ss_array::instance(size,type);
}

/*!
  Visits the array declarator operator.
  \pre dop != NULL
  \dop  The declarator operator to visit.
*/
void as_declarator_op2ss_type::visit_as_declarator_op_non_constant_array(ptr<as_declarator_op_non_constant_array> dop)
{
	lassert(dop);

	// TODO pt
	lassert2(false,"Non constant array is not supported.");
}

/*!
  Visits the member pointer declarator operator.
  \pre dop != NULL
  \dop  The declarator operator to visit.
  \todo pt Not supported yet.
*/
void as_declarator_op2ss_type::visit_as_ptr_op_member_ptr(ptr<as_ptr_op_member_ptr> dop)
{
	lassert(dop);

	// TODO pt Not supported
	lassert2(false,"Member pointer not supported.");
}

#if 0
	FUCK THIS ALL
	// TODO pt containing scope? where from?
	// no scope needed for processing of types
	// otherwise create the scope for parameters
	// with parent

	ptr<sa_param_decl_context> sapdc = sa_param_decl_context::create(declaration_context::CTX_PARAMETER,
			
			//ss_decl_seq::root_instance()
			// FIXME
			NULL
			
			);
	ptr<sa_param_decl> sapd = sa_param_decl::create(sapdc);
	
	// only process parameters thoroughly when actually creating function declaration
	// TODO pt what the fuck? only processing types, 
	if (last_flag) {
		parameters = ss_parameter_declaration_list_type::create();
		ulint position = 0;

		for (as_param_declaration_list_type::iterator it = apdl->begin(), end = apdl->end();
				it != end; ++it) {
			
			sapd->process(*it,++position);
			parameters->push_back(sapd->declaration_get());
			sstl->push_back(sapd->type_get());
		}
	
	} else {
	for (as_param_declaration_list_type::iterator it = apdl->begin(), end = apdl->end();
				it != end; ++it) {
		
		sapd->process(*it);
		sstl->push_back(sapd->type_get());
	}
	
	}
#endif
	
#if 0
/*!
  Visits the function declarator operator in parameter.
  \pre dop != NULL
  \dop  The declarator operator to visit.
*/
void as_declarator_op2ss_type_param::visit_as_declarator_op_func(ptr<as_declarator_op_func> dop)
{
	lassert(dop);
  
	typedef ::lestes::std::list< srp<as_param_declaration> > as_param_declaration_list_type;
	typedef ::lestes::std::list< srp<ss_type> > ss_type_list_type;
	typedef ::lestes::std::list< srp<ss_parameter_declaration> > ss_parameter_declaration_list_type;
  
	// TODO pt check type for function and return error: function returning function
	// but how? we lost the function flag, shall it be restored??
	
	ptr<as_param_decl_clause> apdc = dop->parameters_get();
	bool ellipsis = apdc->ellipsis_get();
	ptr<as_param_declaration_list_type> apdl = apdc->parameters_get();
	ptr<ss_type_list_type> sstl = ss_type_list_type::create();
	
	// TODO pt containing scope
	ptr<sa_param_decl_context> sapdc = sa_param_decl_context::create(declaration_context::CTX_PARAMETER,
			
			//ss_decl_seq::root_instance()
			// FIXME
			NULL
			);

	ptr<sa_param_decl> sapd = sa_param_decl::create(sapdc);
	
	for (as_param_declaration_list_type::iterator it = apdl->begin(), end = apdl->end();
			it != end; ++it) {
		
		sapd->process(*it);
		sstl->push_back(sapd->type_get());
	}
	
	ptr<ss_type> sst = type_get();
	// create the type from the op
	sst = ss_function::instance(sst,sstl,ellipsis);
	// in parameters function is equivalent to pointer to function
	if (last_flag_get()) sst = ss_pointer::instance(sst);
	type_set(sst);
}

/*!
  Visits the function declarator operator without creating parameter declarations.
  \pre dop != NULL
  \dop  The declarator operator to visit.
*/
void as_declarator_op2ss_type_plain::visit_as_declarator_op_func(ptr<as_declarator_op_func> dop)
{
	lassert(dop);
  
	typedef ::lestes::std::list< srp<as_param_declaration> > as_param_declaration_list_type;
	typedef ::lestes::std::list< srp<ss_type> > ss_type_list_type;
  
	// TODO pt check type for function and return error: function returning function
	// but how? we lost the function flag, shall it be restored??
	
	ptr<as_param_decl_clause> apdc = dop->parameters_get();
	bool ellipsis = apdc->ellipsis_get();
	ptr<as_param_declaration_list_type> apdl = apdc->parameters_get();
	ptr<ss_type_list_type> sstl = ss_type_list_type::create();
	ptr<sa_param_decl_context> sapdc = sa_param_decl_context::create(
			declaration_context::CTX_PARAMETER,
			//TODO
			NULL
			);
	ptr<sa_param_decl> sapd = sa_param_decl::create(sapdc);
	
	// only process types of parameters
	for (as_param_declaration_list_type::iterator it = apdl->begin(), end = apdl->end();
			it != end; ++it) {
		
		sapd->process(*it);
		sstl->push_back(sapd->type_get());
	}
	
	// create the type from the op
	type_set(ss_function::instance(type_get(),sstl,ellipsis));
}

#endif

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);

/* vim: set ft=lestes : */
