/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/lang/cplus/sem/as_access_specifier_to_ss_access_specifier.g.hh>
#include <lestes/lang/cplus/sem/as_base_specifier_to_ss_base_specifier.g.hh>
#include <lestes/lang/cplus/sem/as_class_key_to_ss_access_specifier.g.hh>
#include <lestes/lang/cplus/sem/as_class_key_to_ss_struct_base.g.hh>
#include <lestes/lang/cplus/sem/as_decl.g.hh>
#include <lestes/lang/cplus/sem/as_id_to_declaration_set.g.hh>
#include <lestes/lang/cplus/sem/as_id_to_ss_decl_name.g.hh>
#include <lestes/lang/cplus/sem/li_class_by_name_in_single_scope.g.hh>
#include <lestes/lang/cplus/sem/sa_class_declaration.g.hh>
#include <lestes/lang/cplus/sem/sa_class_declaration.m.hh>
#include <lestes/lang/cplus/sem/sa_context.g.hh>
#include <lestes/lang/cplus/sem/sa_decl_seq_compound_pair_creator.g.hh>
#include <lestes/lang/cplus/sem/sa_loggers.hh>
#include <lestes/lang/cplus/sem/ss_decl_name.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration_to_ss_decl_seq.g.hh>
#include <lestes/lang/cplus/sem/ss_enums.g.hh>
#include <lestes/lang/cplus/sem/ss_misc.g.hh>
#include <lestes/lang/cplus/sem/ss_statement.g.hh>
#include <lestes/lang/cplus/sem/ss_type.g.hh>
#include <lestes/lang/cplus/sem/ss_type2info.g.hh>
#include <lestes/lang/cplus/syn/manager.hh>
#include <lestes/lang/cplus/syn/token.hh>
#include <lestes/msg/logger.hh>
#include <lestes/msg/logger_util.hh>

/*! \file
  \brief Handling of class declarations.
  
  Hint: when processing undeclared: (1) pretend forward (2) fixup as in already
  declared case
  
  \todo TMA declared/undeclared distinction
  \todo TMA class name; and similar handling (forward declaration)
  \todo TMA int foo(class A * x) handling (invisible forward)
  \author TMA
 */

package(lestes);
package(lang);
package(cplus);
package(sem);

/*!
  This method installs the context of the class to the current context by
  pushing the new context over the previous.

  The method only wraps its arguments into an acceptable sa_context
  instance that is subsequently pushed on the sa_context_manager's stack.
 */
void sa_class_declaration::new_context(ptr< source_location > location, ss_access_specifier::type access_specifier, ptr< ss_decl_seq > scope, ptr< as_name > name)
{
	ptr < as_access_specifier > acc_spec;

	switch (access_specifier) {
		case ss_access_specifier::ACCESS_PUBLIC:
			acc_spec = as_access_specifier_public::create(location);
			break;
		case ss_access_specifier::ACCESS_PRIVATE:
			acc_spec = as_access_specifier_private::create(location);
			break;
		default:
			lassert2(false, "Default access specifier for a class has to be either public or private.");
			
	}

	ptr < sa_context > new_ctx = sa_context::create(
			sa_as_context::create(name, acc_spec), 
			sa_ss_context::create(scope, access_specifier),
			sa_sa_context::create());
	sa_context_manager::instance()->push(new_ctx);
}

/*!
  This method processes a class header (up to the class' opening brace) and
  adds all the structures necessary to the context. Then the context is
  switched, so that the subsequent declarations found in the class body are
  inserted to the proper context.

  A special care has to be observed, because the class might be already forward
  declared. In such a case no new ss_class object shall be created, because for
  every class there shall be just one such object.  Also the issue is further
  complicated by the visibility/invisibility of such previous declaration.
 */
void sa_class_declaration::process(ptr < as_class_specifier > cs)
{
	sa_class_logger << "sa_class_declaration::process begin\n" << msg::eolog;
	if (::lestes::lang::cplus::syn::manager::in_disambiguation()) {
		sa_class_logger << "nop -- in disambiguation\n" << msg::eolog;
	} else {
		sa_class_logger << "sa_class_declaration:: location\n" << msg::eolog;
		ptr < source_location > location = cs->location_get();

		//sa_class_logger << "sa_class_declaration:: name\n" << msg::eolog;
		//ptr < ss_decl_name > decl_name = as_id_to_ss_decl_name::instance()->process(cs->name_get()->identifier_get());;
		//lassert(decl_name);

		sa_class_logger << "sa_class_declaration:: former declaration?\n" << msg::eolog;
		
		sa_class_logger << "sa_class_declaration:: name\n" << msg::eolog;
		ptr < ss_decl_name > decl_name = as_id_to_ss_decl_name::instance()->process(cs->name_get()->identifier_get());;
		lassert(decl_name);

		sa_class_logger << "sa_class_declaration:: determine scope into which the declaration belongs\n" << msg::eolog;
		ptr < ss_decl_seq > seq;
		if (cs->name_get()->was_qualified_get()) {
			ptr < ::lestes::std::set < srp < ss_declaration > > > qual_declarations =
				as_id_to_declaration_set::instance()->process(
					cs->name_get()->qualification_get()->identifier_get());
			lassert2(qual_declarations->size() == 1,
				"Qualification should unambiguously name a single scope declaration");
			seq = ss_declaration_to_ss_decl_seq::instance()->process(*qual_declarations->begin());
			/* FIXME: shall qualified declaration of invisible classes be handled differently? */
		} else {
			seq = sa_context_manager::instance()->current()->ss_get()->scope_get();
		}
		lassert(seq);

		/* FIXME: maybe heed visibility in the qname case... */
		sa_class_logger << "sa_class_declaration:: lookup for potentionaly invisible former declaration\n" << msg::eolog;
		ptr < ss_structure_declaration > decl = li_class_by_name_in_single_scope::create()->process(decl_name, seq);

		if (decl) {
			sa_class_logger << "sa_class_declaration:: process_declared\n" << msg::eolog;
			process_declared(cs, decl);
		} else {
			sa_class_logger << "sa_class_declaration:: process_undeclared\n" << msg::eolog;
			process_undeclared(cs);
		}
	}
	sa_class_logger << "sa_class_declaration::process end\n" << msg::eolog;
}

/*!
  \brief Handles the already declared case.

  The declaration is made visible, based on the assumption that the class
  specifier containing the body of the class cannot introduce an invisible
  name, because such class specifiers are forbidden in the contexts leading to
  a invisible declaration introduction.

  \invariant !syn::manager::in_disambiguation()
  \invariant the type does not name a complete type
 */
void sa_class_declaration::process_declared(ptr < as_class_specifier > cs, ptr < ss_structure_declaration > class_decl)
{
	lassert(!syn::manager::in_disambiguation());

	sa_class_declaration_logger << "sa_class_declaration::process_declared begin\n" << msg::eolog;
	/* location */
	ptr < source_location > location = cs->location_get();

	/* visibility */
	sa_class_declaration_logger << "visibility\n" << msg::eolog;
	ptr < ss_declaration_time > dt
		= ss_declaration_time::create(cs->location_get()->order_get());

	lassert2(dt->is_before(class_decl->type_get().dncast<ss_struct_base>()->completion_time_get()), "FIXME: report error");
	if (!class_decl->visible_since_get()->is_before(dt)) {
		sa_class_declaration_logger << "visibility adjusted\n" << msg::eolog;
		class_decl->visible_since_set(dt);
	}

	/* handle default access */
	/* access specifier that is the default for this kind of struct/class/union */
	sa_class_declaration_logger << "determine default access specifier\n" << msg::eolog;
	ss_access_specifier::type access_specifier =
		as_class_key_to_ss_access_specifier::create()->process(cs->key_get());
	/* handle bases */
	/* bases */
	sa_class_type_logger << "bases: AS->SS transform\n" << msg::eolog;
	ptr < ::lestes::std::list < srp < ss_base_specifier > > > bases =
		as_base_specifier_to_ss_base_specifier::create(
			access_specifier)->process_list(cs->base_specifiers_get());
	/* modify the class to reflect the supplied bases */
	sa_class_type_logger << "bases: fill in right place\n" << msg::eolog;
	ptr < ss_type > type = class_decl->type_get();
	// DIE reflections maimer, DIE
	if (type && dynamic_cast<ss_class *>(type.operator->())) {
		ptr < ss_class > a_class = type.dncast<ss_class>();
		a_class->bases_get()->splice(
				a_class->bases_get()->end(), *bases);
	}

	/* switch context */
	sa_class_declaration_logger << "context switch\n" << msg::eolog;
	new_context(location, access_specifier, class_decl->body_get(), cs->name_get());

	sa_class_declaration_logger << "sa_class_declaration::process_declared end\n" << msg::eolog;
}

/*!
  \invariant !syn::manager::in_disambiguation()
 */
void sa_class_declaration::process_undeclared(ptr < as_class_specifier > cs)
{
	lassert(!syn::manager::in_disambiguation());

	sa_class_declaration_logger << "sa_class_declaration::process_undeclared begin\n" << msg::eolog;

	sa_class_declaration_logger << "parent declseq get\n" << msg::eolog;
	ptr < ss_decl_seq > parent_scope = sa_context_manager::instance()->current()->ss_get()->scope_get();

	ptr < ss_structure_declaration > class_decl =
		insert_class_forward_into_scope(cs, true, parent_scope);

	process_declared(cs, class_decl);

	sa_class_declaration_logger << "sa_class_declaration::process_undeclared end\n" << msg::eolog;
}


/*!
  \invariant !syn::manager::in_disambiguation()
 */
void sa_class_declaration::process_end(ptr < source_location > location)
{
	sa_class_logger << "sa_class_declaration::process_end begin\n" << msg::eolog;
	lassert(!syn::manager::in_disambiguation());

	sa_class_logger << "mark class as complete\n" << msg::eolog;
	sa_context_manager::instance()->current()->ss_get()->scope_get()->declared_by_get()->type_get().dncast<ss_struct_base>()->completion_time_set(ss_declaration_time::create(location->order_get()));
	sa_class_logger << "context pop\n" << msg::eolog;
	sa_context_manager::instance()->pop();
	sa_class_logger << "sa_class_declaration::process_end end\n" << msg::eolog;
}

/*!
  \invariant !syn::manager::in_disambiguation()
 */
ptr < ::lestes::lang::cplus::sem::ss_structure_declaration > sa_class_declaration::insert_class_forward_into_scope(ptr < ::lestes::lang::cplus::sem::as_class_specifier > cs, bool visibly, ptr < ::lestes::lang::cplus::sem::ss_decl_seq > parent_scope)
{
	lassert(!syn::manager::in_disambiguation());

	if (syn::manager::in_disambiguation())
		return NULL;
	/* location */
	ptr < source_location > location = cs->location_get();

	sa_class_logger << "sa_class_declaration:: name\n" << msg::eolog;
	ptr < ss_decl_name > decl_name = as_id_to_ss_decl_name::instance()->process(cs->name_get()->identifier_get());;
	lassert(decl_name);

	sa_class_declaration_logger << "declseq & compound\n" << msg::eolog;
	ptr < ::lestes::std::pair < srp < ss_decl_seq >, srp < ss_compound_stmt > > > p = 
		sa_decl_seq_compound_pair_creator::instance()->process(location, 
			parent_scope,
			parent_scope->compound_stmt_get()
		);
	lassert(p);

	ptr < ss_decl_seq > decl_seq = p->first;
	ptr < ss_compound_stmt > stmt = p->second;
	lassert(decl_seq && stmt);

	sa_class_declaration_logger << "type\n" << msg::eolog;
	ptr < ss_struct_base > type =
		as_class_key_to_ss_struct_base::create(
			decl_seq,
			::lestes::std::list < srp < ss_base_specifier > >::create()
			)->process(cs->key_get());

	sa_class_declaration_logger << "proper\n" << msg::eolog;
	ptr < ss_structure_declaration > class_decl = ss_structure_declaration::create(
			location, // location
			visibly
				? ss_declaration_time::create(location->order_get())
				: ss_declaration_time::infinity(), // visible
			ss_declaration_time::create(location->order_get()), // decl time
			decl_name, //name
			parent_scope, // contained in
			type, // type
			// FIXME: determine linkage according to parent scope
			ss_linkage::create("C++",ss_linkage::LINKAGE_EXTERNAL), // linkage
			sa_context_manager::instance()->current()->ss_get()->access_specifier_get(), // access specifier
			ss_storage_class::ST_NONE, // storage class
			::lestes::std::set< srp< ss_struct_base > >::create(), // friends
			decl_seq
			);

	sa_class_declaration_logger << "injected\n" << msg::eolog;
	ptr < ss_injected_class_declaration > class_decl_alias = ss_injected_class_declaration::create(
			location, // location
			ss_declaration_time::create(location->order_get()), // visible
			ss_declaration_time::create(location->order_get()), // decl time
			decl_name, //name
			decl_seq, // contained in
			type, // type
			// FIXME: correct?
			class_decl->linkage_get(), // linkage
			ss_access_specifier::ACCESS_PUBLIC, // access specifier
			ss_storage_class::ST_NONE, // storage class
			class_decl // real class
			);

	sa_class_declaration_logger << "fixup circular structures\n" << msg::eolog;
	/* fill right values */
	type->decl_set(class_decl);
	decl_seq->declared_by_set(class_decl);
	decl_seq->contents_get()->push_back(class_decl_alias);
	parent_scope->contents_get()->push_back(class_decl);

	return class_decl;

}

/*!
  \todo TMA invisible former declarations shall be made visible, visible former
  declarations shall be left untouched. In the remaining cases, a new visible
  declaration shall be inserted.
 */
void sa_class_declaration::process_forward_declaration(ptr < as_class_specifier > cs)
{
	if (!syn::manager::in_disambiguation()) {
		ptr<ss_decl_seq> ds =
			sa_context_manager::instance()->current()->ss_get()->scope_get();
		insert_class_forward_into_scope(cs, true, ds);
	}
}

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);

