/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__sem___or_hh___included
#define lestes__lang__cplus__sem___or_hh___included

/*! \file
  Header file for purposes of overload resolution
  \author jikos
*/
package(lestes);
package(lang);
package(cplus);
package(sem);

typedef ::lestes::std::list< srp< or_or_functional > > exprlist;
typedef ::lestes::std::set< srp< ss_function_declaration > > func_decl_set;

ptr< or_or_functional > overload_resolution (ptr< exprlist > exprs, ptr< ss_operator > func_name);
ptr< or_or_functional > overload_resolution (ptr< exprlist > exprs, ptr< func_decl_set > candidates);

template<typename op, typename type1> void or_builtin_operator_declaration_creator::add_vq_ref_vq_ref()
{
	ptr< ::lestes::std::set< srp< ss_builtin_operator_declaration > > > r = ::lestes::std::set< srp< ss_builtin_operator_declaration > >::create(); 
	ptr< ::lestes::std::list< srp< ss_type > > > types = ::lestes::std::list< srp< ss_type > >::create(); 
	ptr< ss_type > return_type; 
	ptr< ss_decl_name > _op = op::create(source_location::zero());
	
	types->push_back(type1::instance());
	return_type = ss_pseudoreference::instance(type1::instance());
	the_instance->construct_builtin_op(_op, types, return_type);
}

template<typename op, typename type1> void or_builtin_operator_declaration_creator::add_nonref_vq_ref_int()
{
	ptr< ::lestes::std::set< srp< ss_builtin_operator_declaration > > > r = ::lestes::std::set< srp< ss_builtin_operator_declaration > >::create();
	ptr< ::lestes::std::list< srp< ss_type > > > types = ::lestes::std::list< srp< ss_type > >::create();
	ptr< ss_type > return_type;
	ptr< ss_decl_name > _op = op::create(source_location::zero());

	types->push_back(type1::instance());
	types->push_back(ss_type_sint::instance());
	return_type = type1::instance();
	the_instance->construct_builtin_op(_op, types, return_type);
}

template<typename op, typename type1> void or_builtin_operator_declaration_creator::add_vq_ptr_ref_vq_ptr_ref()
{
	ptr< ::lestes::std::set< srp< ss_builtin_operator_declaration > > > r = ::lestes::std::set< srp< ss_builtin_operator_declaration > >::create();
	ptr< ::lestes::std::list< srp< ss_type > > > types = ::lestes::std::list< srp< ss_type > >::create();
	ptr< ss_type > return_type;
	ptr< ss_decl_name > _op = op::create(source_location::zero());

	types->push_back(ss_pointer::instance(type1::instance()));
	return_type = ss_pseudoreference::instance(ss_pointer::instance(type1::instance()));
	the_instance->construct_builtin_op(_op, types, return_type);
}

template<typename op, typename type1> void or_builtin_operator_declaration_creator::add_vq_ptr_nonref_vq_ptr_ref_int()
{
	ptr< ::lestes::std::set< srp< ss_builtin_operator_declaration > > > r = ::lestes::std::set< srp< ss_builtin_operator_declaration > >::create();
	ptr< ::lestes::std::list< srp< ss_type > > > types = ::lestes::std::list< srp< ss_type > >::create();
	ptr< ss_type > return_type;
	ptr< ss_decl_name > _op = op::create(source_location::zero());

	types->push_back(ss_pointer::instance(type1::instance()));
	types->push_back(ss_type_sint::instance());
	return_type = ss_pointer::instance(type1::instance());
	the_instance->construct_builtin_op(_op, types, return_type);
}

template<typename op, typename type1> void or_builtin_operator_declaration_creator::add_ref_ptr()
{
	ptr< ::lestes::std::set< srp< ss_builtin_operator_declaration > > > r = ::lestes::std::set< srp< ss_builtin_operator_declaration > >::create();
	ptr< ::lestes::std::list< srp< ss_type > > > types = ::lestes::std::list< srp< ss_type > >::create();
	ptr< ss_type > return_type;
	ptr< ss_decl_name > _op = op::create(source_location::zero());

	types->push_back(ss_pointer::instance(type1::instance()));
	return_type = ss_pseudoreference::instance(type1::instance());
	the_instance->construct_builtin_op(_op, types, return_type);
}

template<typename op, typename type1> void or_builtin_operator_declaration_creator::add_ref_ptr_const()
{
	ptr< ::lestes::std::set< srp< ss_builtin_operator_declaration > > > r = ::lestes::std::set< srp< ss_builtin_operator_declaration > >::create();
	ptr< ::lestes::std::list< srp< ss_type > > > types = ::lestes::std::list< srp< ss_type > >::create();
	ptr< ss_type > return_type;
	ptr< ss_decl_name > _op = op::create(source_location::zero());

	types->push_back(ss_pointer::instance(ss_const::instance(type1::instance())));
	return_type = ss_pseudoreference::instance(type1::instance());
	the_instance->construct_builtin_op(_op, types, return_type);
}

template<typename op> void or_builtin_operator_declaration_creator::add_ptr_ptr()
{
	ptr< ::lestes::std::set< srp< ss_builtin_operator_declaration > > > r = ::lestes::std::set< srp< ss_builtin_operator_declaration > >::create();
	ptr< ::lestes::std::list< srp< ss_type > > > types = ::lestes::std::list< srp< ss_type > >::create();
	ptr< ss_type > return_type;
	ptr< ss_decl_name > _op = op::create(source_location::zero());

	types->push_back(ss_pointer::instance(ss_void::instance()));
	return_type = ss_pointer::instance(ss_void::instance());
	the_instance->construct_builtin_op(_op, types, return_type);
}

template<typename op, typename type1, typename type2> void or_builtin_operator_declaration_creator::add_nonref_nonref()
{
	ptr< ::lestes::std::set< srp< ss_builtin_operator_declaration > > > r = ::lestes::std::set< srp< ss_builtin_operator_declaration > >::create(); 
	ptr< ::lestes::std::list< srp< ss_type > > > types = ::lestes::std::list< srp< ss_type > >::create(); 
	ptr< ss_type > return_type; 
	ptr< ss_decl_name > _op = op::create(source_location::zero());

	types->push_back(type2::instance());
	return_type = type1::instance();
	the_instance->construct_builtin_op(_op, types, return_type);
}

template<typename op, typename type1, typename type2, typename type3> void or_builtin_operator_declaration_creator::add_nonref_nonref_nonref()
{
	ptr< ::lestes::std::set< srp< ss_builtin_operator_declaration > > > r = ::lestes::std::set< srp< ss_builtin_operator_declaration > >::create();
	ptr< ::lestes::std::list< srp< ss_type > > > types = ::lestes::std::list< srp< ss_type > >::create();
	ptr< ss_type > return_type;
	ptr< ss_decl_name > _op = op::create(source_location::zero());

	types->push_back(type2::instance());
	types->push_back(type3::instance());
	return_type = type1::instance();
	the_instance->construct_builtin_op(_op, types, return_type);
}

template<typename op, typename type1, typename type2> void or_builtin_operator_declaration_creator::add_vq_ref_vq_ref_nonref()
{
	ptr< ::lestes::std::set< srp< ss_builtin_operator_declaration > > > r = ::lestes::std::set< srp< ss_builtin_operator_declaration > >::create();
	ptr< ::lestes::std::list< srp< ss_type > > > types = ::lestes::std::list< srp< ss_type > >::create();
	ptr< ss_type > return_type;
	ptr< ss_decl_name > _op = op::create(source_location::zero());

	types->push_back(ss_pseudoreference::instance(type1::instance()));
	types->push_back(type2::instance());
	return_type = ss_pseudoreference::instance(type1::instance());
	the_instance->construct_builtin_op(_op, types, return_type);
}

template<typename op, typename type1, typename type2> void or_builtin_operator_declaration_creator::add_vq_ref_vq_ref_nonref_REF()
{
/*	ptr< ::lestes::std::set< srp< ss_builtin_operator_declaration > > > r = ::lestes::std::set< srp< ss_builtin_operator_declaration > >::create();
	ptr< ::lestes::std::list< srp< ss_type > > > types = ::lestes::std::list< srp< ss_type > >::create();
	ptr< ss_type > return_type;
	ptr< ss_decl_name > _op = op::create(source_location::zero());

	types->push_back(ss_pseudoreference::instance(type1::instance()));
	types->push_back(type2::instance());
	return_type = ss_pseudoreference::instance(type1::instance());
	the_instance->construct_builtin_op(_op, types, return_type);
*/
	ptr< ::lestes::std::set< srp< ss_builtin_operator_declaration > > > c_r = ::lestes::std::set< srp< ss_builtin_operator_declaration > >::create();
	ptr< ::lestes::std::list< srp< ss_type > > > c_types = ::lestes::std::list< srp< ss_type > >::create();
	ptr< ss_type > c_return_type;
	ptr< ss_decl_name > c__op = op::create(source_location::zero());

	c_types->push_back(ss_reference::instance(type1::instance()));
	c_types->push_back(type2::instance());
	c_return_type = ss_pseudoreference::instance(type1::instance());
	the_instance->construct_builtin_op(c__op, c_types, c_return_type);
}

template<typename op, typename type1, typename type2, typename type3, typename type4> void or_builtin_operator_declaration_creator::add_nonref_nonref_nonref_nonref()
{
	ptr< ::lestes::std::set< srp< ss_builtin_operator_declaration > > > r = ::lestes::std::set< srp< ss_builtin_operator_declaration > >::create(); 
	ptr< ::lestes::std::list< srp< ss_type > > > types = ::lestes::std::list< srp< ss_type > >::create();
	ptr< ss_type > return_type;
	ptr< ss_decl_name > _op = op::create(source_location::zero());

	types->push_back(type2::instance());
	types->push_back(type3::instance());
	types->push_back(type4::instance());
	return_type = type1::instance();
	the_instance->construct_builtin_op(_op, types, return_type);
}

template<typename op, typename type1> void or_builtin_operator_declaration_creator::add_ptr_ptr_int()
{
	ptr< ::lestes::std::set< srp< ss_builtin_operator_declaration > > > r = ::lestes::std::set< srp< ss_builtin_operator_declaration > >::create();
	ptr< ::lestes::std::list< srp< ss_type > > > types = ::lestes::std::list< srp< ss_type > >::create();
	ptr< ss_type > return_type;
	ptr< ss_decl_name > _op = op::create(source_location::zero());

	types->push_back(ss_pointer::instance(type1::instance()));
	types->push_back(ss_type_sint::instance());
	return_type = ss_pointer::instance(type1::instance());
	the_instance->construct_builtin_op(_op, types, return_type);
}

template<typename op, typename type1> void or_builtin_operator_declaration_creator::add_ptr_int_ptr()
{
	ptr< ::lestes::std::set< srp< ss_builtin_operator_declaration > > > r = ::lestes::std::set< srp< ss_builtin_operator_declaration > >::create();
	ptr< ::lestes::std::list< srp< ss_type > > > types = ::lestes::std::list< srp< ss_type > >::create();
	ptr< ss_type > return_type;
	ptr< ss_decl_name > _op = op::create(source_location::zero());

	types->push_back(ss_type_sint::instance());
	types->push_back(ss_pointer::instance(type1::instance()));
	return_type = ss_pointer::instance(type1::instance());
	the_instance->construct_builtin_op(_op, types, return_type);
}

template<typename op, typename type1> void or_builtin_operator_declaration_creator::add_int_ptr_ptr()
{
	ptr< ::lestes::std::set< srp< ss_builtin_operator_declaration > > > r = ::lestes::std::set< srp< ss_builtin_operator_declaration > >::create();
	ptr< ::lestes::std::list< srp< ss_type > > > types = ::lestes::std::list< srp< ss_type > >::create();
	ptr< ss_type > return_type;
	ptr< ss_decl_name > _op = op::create(source_location::zero());

	types->push_back(ss_pointer::instance(type1::instance()));
	types->push_back(ss_pointer::instance(type1::instance()));
	return_type = ss_type_sint::instance();
	the_instance->construct_builtin_op(_op, types, return_type);
}

template<typename op, typename type1> void or_builtin_operator_declaration_creator::add_bool_ptr_ptr()
{
	ptr< ::lestes::std::set< srp< ss_builtin_operator_declaration > > > r = ::lestes::std::set< srp< ss_builtin_operator_declaration > >::create();
	ptr< ::lestes::std::list< srp< ss_type > > > types = ::lestes::std::list< srp< ss_type > >::create();
	ptr< ss_type > return_type;
	ptr< ss_decl_name > _op = op::create(source_location::zero());

	types->push_back(ss_pointer::instance(type1::instance()));
	types->push_back(ss_pointer::instance(type1::instance()));
	return_type = ss_bool::instance();
	the_instance->construct_builtin_op(_op, types, return_type);
}

template<typename op, typename type1> void or_builtin_operator_declaration_creator::add_vq_ptr_ref_vq_ptr_ref_ptr()
{
	ptr< ::lestes::std::set< srp< ss_builtin_operator_declaration > > > r = ::lestes::std::set< srp< ss_builtin_operator_declaration > >::create();
	ptr< ::lestes::std::list< srp< ss_type > > > types = ::lestes::std::list< srp< ss_type > >::create();
	ptr< ss_type > return_type;
	ptr< ss_decl_name > _op = op::create(source_location::zero());

	types->push_back(ss_pointer::instance(type1::instance()));
	types->push_back(ss_pointer::instance(type1::instance()));
	return_type = ss_pseudoreference::instance(ss_pointer::instance(type1::instance()));
	the_instance->construct_builtin_op(_op, types, return_type);
}

template<typename op, typename type1> void or_builtin_operator_declaration_creator::add_vq_ptr_ref_vq_ptr_ref_int()
{
	ptr< ::lestes::std::set< srp< ss_builtin_operator_declaration > > > r = ::lestes::std::set< srp< ss_builtin_operator_declaration > >::create();
	ptr< ::lestes::std::list< srp< ss_type > > > types = ::lestes::std::list< srp< ss_type > >::create();
	ptr< ss_type > return_type;
	ptr< ss_decl_name > _op = op::create(source_location::zero());

	types->push_back(ss_pointer::instance(type1::instance()));
	types->push_back(ss_type_sint::instance());
	return_type = ss_pseudoreference::instance(ss_pointer::instance(type1::instance()));
	the_instance->construct_builtin_op(_op, types, return_type);
}

/*! 
 * The following macros are usied when filling the builtin-op decl seq with builtin operator
 * declarations, to make it easier.
 */

/* TODO All the following macros having 'VQ' in their name should be adjusted to use const/volatile */

/*! Return type is type1&, 1st argument type is type1& (both the same types) */
#define ADD_VQ_REF_VQ_REF(op, type1) the_instance->add_vq_ref_vq_ref<op, type1>();

/*! Return type is type1, 1st argument type is type1&, 2nd argument is int */
#define ADD_NONREF_VQ_REF_INT(op, type1) the_instance->add_nonref_vq_ref_int<op, type1>();
	
/*! Return type is pointer, 1st argument is pointer. This is FIXME, shoudl be split for pointer to object and pointer to arithmetic
 * void is used so far, until something better is written. This is quite a important FIXME for one day.
 */
#define ADD_VQ_PTR_REF_VQ_PTR_REF(op, type1) the_instance->add_vq_ptr_ref_vq_ptr_ref<op, type1>();

/*! Return type is pointer, 1st argument is pointer, 2nd argument is int. This is FIXME, shoudl be split for pointer to object and pointer to arithmetic
 * void is used so far, until something better is written. This is quite a important FIXME for one day.
 */
#define ADD_VQ_PTR_NONREF_VQ_PTR_REF_INT(op, type1) the_instance->add_vq_ptr_nonref_vq_ptr_ref_int<op, type1>();
	
/*! Return type is reference, parameter type is pointer. FIXME - void used so far */
#define ADD_REF_PTR(op, type1) the_instance->add_ref_ptr<op, type1>();

/*! Return type is reference, parameter type is pointer to const . FIXME - void used so far */
#define ADD_REF_PTR_CONST(op, type1) the_instance->add_ref_ptr_const<op, type1>();

/*! Return type is pointer, parameter type is pointer. FIXME - void used so far */
#define ADD_PTR_PTR(op) the_instance->add_ptr_ptr<op>();

/*! Return type is type1, argument is type2 */
#define ADD_NONREF_NONREF(op, type1, type2) the_instance->add_nonref_nonref<op, type1, type2>();
/* TODO [13.6/11] to be done */
	
/*! Return type is non-ref and is conversion from , 1st and 2nd arguments are also non-ref */
#define ADD_NONREF_NONREF_NONREF(op, type1, type2, type3) the_instance->add_nonref_nonref_nonref<op, type1, type2, type3>();

/*! Return type is ref, 1st argument is ref and 2nd arguments is non-ref and ret and 1st are the same*/
#define ADD_VQ_REF_VQ_REF_NONREF(op, type1, type2) the_instance->add_vq_ref_vq_ref_nonref<op, type1, type2>();
#define ADD_VQ_REF_VQ_REF_NONREF_REF(op, type1, type2) the_instance->add_vq_ref_vq_ref_nonref_REF<op, type1, type2>();
/* TODO [13.6/19 - 21] to be done*/
	
/*! Return type is non-ref, 1st and 2nd arguments are also non-ref */
#define ADD_NONREF_NONREF_NONREF_NONREF(op, type1, type2, type3, type4) the_instance->add_nonref_nonref_nonref_nonref<op, type1, type2, type3, type4>();

/*! Return type is pointer to type1, 1st arg is pointer, 2nd arg is int */
#define ADD_PTR_PTR_INT(op, type1) the_instance->add_ptr_ptr_int<op, type1>();

/*! Return type is ptr to type1, 1st arg is int, 2nd arg is pointer to type1 */
#define ADD_PTR_INT_PTR(op, type1) the_instance->add_ptr_int_ptr<op, type1>();

/*! Return type is ptrdiff_t , 1st arg and 2nd arg is pointer to type1 */
#define ADD_INT_PTR_PTR(op, type1) the_instance->add_int_ptr_ptr<op, type1>();

/*! Return type is ptrdiff_t , 1st arg and 2nd arg is pointer to type1 */
#define ADD_BOOL_PTR_PTR(op, type1) the_instance->add_bool_ptr_ptr<op, type1>();

/*! Rerturn type is pseudoreference for pointer to type1, 1st arg same as return type, 2nd argument is pointer to type1*/
#define ADD_VQ_PTR_REF_VQ_PTR_REF_PTR(op, type1) the_instance->add_vq_ptr_ref_vq_ptr_ref_ptr<op, type1>();

/*! Return type is pseudoref for ptr to type1, 1st argument is same as return type, 2nd argument is int */
#define ADD_VQ_PTR_REF_VQ_PTR_REF_INT(op, type1) the_instance->add_vq_ptr_ref_vq_ptr_ref_int<op, type1>();

/* and this is the initialization sequence */

#define BUILTIN_DECL_SEQ_INIT() { \
\
		/* [13.6/3] \
		 * For every pair (T, VQ), where T is arithmetic type and VQ is either volatile or empty, there exists \
		 * candidate operator functions of the form\
\
		 	VQ T& operator++(VQ T&);\
			T     operator++(VQ T&, int);\
		*/\
		\
		ADD_VQ_REF_VQ_REF(ss_operator_inc, ss_type_schar);\
		ADD_VQ_REF_VQ_REF(ss_operator_inc, ss_type_sshort);\
		ADD_VQ_REF_VQ_REF(ss_operator_inc, ss_type_sint);\
		ADD_VQ_REF_VQ_REF(ss_operator_inc, ss_type_slong);\
		ADD_VQ_REF_VQ_REF(ss_operator_inc, ss_type_uchar);\
		ADD_VQ_REF_VQ_REF(ss_operator_inc, ss_type_pchar);\
		ADD_VQ_REF_VQ_REF(ss_operator_inc, ss_type_ushort);\
		ADD_VQ_REF_VQ_REF(ss_operator_inc, ss_type_uint);\
		ADD_VQ_REF_VQ_REF(ss_operator_inc, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF(ss_operator_inc, ss_bool);\
		ADD_VQ_REF_VQ_REF(ss_operator_inc, ss_type_float);\
		ADD_VQ_REF_VQ_REF(ss_operator_inc, ss_type_double);\
		ADD_VQ_REF_VQ_REF(ss_operator_inc, ss_type_ldouble);\
\
		ADD_NONREF_VQ_REF_INT(ss_operator_inc, ss_type_schar);\
		ADD_NONREF_VQ_REF_INT(ss_operator_inc, ss_type_sshort);\
		ADD_NONREF_VQ_REF_INT(ss_operator_inc, ss_type_sint);\
		ADD_NONREF_VQ_REF_INT(ss_operator_inc, ss_type_slong);\
		ADD_NONREF_VQ_REF_INT(ss_operator_inc, ss_type_uchar);\
		ADD_NONREF_VQ_REF_INT(ss_operator_inc, ss_type_pchar);\
		ADD_NONREF_VQ_REF_INT(ss_operator_inc, ss_type_ushort);\
		ADD_NONREF_VQ_REF_INT(ss_operator_inc, ss_type_uint);\
		ADD_NONREF_VQ_REF_INT(ss_operator_inc, ss_type_ulong);\
		ADD_NONREF_VQ_REF_INT(ss_operator_inc, ss_bool);\
		ADD_NONREF_VQ_REF_INT(ss_operator_inc, ss_type_float);\
		ADD_NONREF_VQ_REF_INT(ss_operator_inc, ss_type_double);\
		ADD_NONREF_VQ_REF_INT(ss_operator_inc, ss_type_ldouble);\
\
		/* [13.6/4] \
		 * For every pair (T, VQ), where T is arithmetic type OTHER THAN BOOL (wtf?) and VQ is either volatile or empty, there exists \
		 * candidate operator functions of the form\
\
		 	VQ T& operator--(VQ T&);\
			T     operator--(VQ T&, int);\
		*/\
		\
		ADD_VQ_REF_VQ_REF(ss_operator_dec, ss_type_schar);\
		ADD_VQ_REF_VQ_REF(ss_operator_dec, ss_type_sshort);\
		ADD_VQ_REF_VQ_REF(ss_operator_dec, ss_type_sint);\
		ADD_VQ_REF_VQ_REF(ss_operator_dec, ss_type_slong);\
		ADD_VQ_REF_VQ_REF(ss_operator_dec, ss_type_uchar);\
		ADD_VQ_REF_VQ_REF(ss_operator_dec, ss_type_pchar);\
		ADD_VQ_REF_VQ_REF(ss_operator_dec, ss_type_ushort);\
		ADD_VQ_REF_VQ_REF(ss_operator_dec, ss_type_uint);\
		ADD_VQ_REF_VQ_REF(ss_operator_dec, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF(ss_operator_dec, ss_type_float);\
		ADD_VQ_REF_VQ_REF(ss_operator_dec, ss_type_double);\
		ADD_VQ_REF_VQ_REF(ss_operator_dec, ss_type_ldouble);\
	\
		ADD_NONREF_VQ_REF_INT(ss_operator_dec, ss_type_schar);\
		ADD_NONREF_VQ_REF_INT(ss_operator_dec, ss_type_sshort);\
		ADD_NONREF_VQ_REF_INT(ss_operator_dec, ss_type_sint);\
		ADD_NONREF_VQ_REF_INT(ss_operator_dec, ss_type_slong);\
		ADD_NONREF_VQ_REF_INT(ss_operator_dec, ss_type_uchar);\
		ADD_NONREF_VQ_REF_INT(ss_operator_dec, ss_type_pchar);\
		ADD_NONREF_VQ_REF_INT(ss_operator_dec, ss_type_ushort);\
		ADD_NONREF_VQ_REF_INT(ss_operator_dec, ss_type_uint);\
		ADD_NONREF_VQ_REF_INT(ss_operator_dec, ss_type_ulong);\
		ADD_NONREF_VQ_REF_INT(ss_operator_dec, ss_type_float);\
		ADD_NONREF_VQ_REF_INT(ss_operator_dec, ss_type_double);\
		ADD_NONREF_VQ_REF_INT(ss_operator_dec, ss_type_ldouble);\
\
		/* [13.6/5]. See fixme in or.hh \
		 *\
		 * For every pair (T, VQ) where T is cv-qualified or cv-unqualified object type and VQ is either volatile or\
		 * empty, there exist candidate operator functions of the form \
		 \
			T*VQ& operator++(T*VQ&);\
			T*VQ& operator--(T*VQ&);\
			T* operator++(T*VQ&, int);\
			T* operator--(T*VQ&, int);\
		 \
		 */\
\
\
		ADD_VQ_PTR_REF_VQ_PTR_REF(ss_operator_inc, ss_bool);\
		ADD_VQ_PTR_REF_VQ_PTR_REF(ss_operator_inc, ss_type_schar);\
		ADD_VQ_PTR_REF_VQ_PTR_REF(ss_operator_inc, ss_type_sshort);\
		ADD_VQ_PTR_REF_VQ_PTR_REF(ss_operator_inc, ss_type_sint);\
		ADD_VQ_PTR_REF_VQ_PTR_REF(ss_operator_inc, ss_type_slong);\
		ADD_VQ_PTR_REF_VQ_PTR_REF(ss_operator_inc, ss_type_uchar);\
		ADD_VQ_PTR_REF_VQ_PTR_REF(ss_operator_inc, ss_type_pchar);\
		ADD_VQ_PTR_REF_VQ_PTR_REF(ss_operator_inc, ss_type_ushort);\
		ADD_VQ_PTR_REF_VQ_PTR_REF(ss_operator_inc, ss_type_uint);\
		ADD_VQ_PTR_REF_VQ_PTR_REF(ss_operator_inc, ss_type_ulong);\
		ADD_VQ_PTR_REF_VQ_PTR_REF(ss_operator_inc, ss_type_float);\
		ADD_VQ_PTR_REF_VQ_PTR_REF(ss_operator_inc, ss_type_double);\
		ADD_VQ_PTR_REF_VQ_PTR_REF(ss_operator_inc, ss_type_ldouble);\
	\
		ADD_VQ_PTR_NONREF_VQ_PTR_REF_INT(ss_operator_inc, ss_bool);\
		ADD_VQ_PTR_NONREF_VQ_PTR_REF_INT(ss_operator_inc, ss_type_schar);\
		ADD_VQ_PTR_NONREF_VQ_PTR_REF_INT(ss_operator_inc, ss_type_sshort);\
		ADD_VQ_PTR_NONREF_VQ_PTR_REF_INT(ss_operator_inc, ss_type_sint);\
		ADD_VQ_PTR_NONREF_VQ_PTR_REF_INT(ss_operator_inc, ss_type_slong);\
		ADD_VQ_PTR_NONREF_VQ_PTR_REF_INT(ss_operator_inc, ss_type_uchar);\
		ADD_VQ_PTR_NONREF_VQ_PTR_REF_INT(ss_operator_inc, ss_type_pchar);\
		ADD_VQ_PTR_NONREF_VQ_PTR_REF_INT(ss_operator_inc, ss_type_ushort);\
		ADD_VQ_PTR_NONREF_VQ_PTR_REF_INT(ss_operator_inc, ss_type_uint);\
		ADD_VQ_PTR_NONREF_VQ_PTR_REF_INT(ss_operator_inc, ss_type_ulong);\
		ADD_VQ_PTR_NONREF_VQ_PTR_REF_INT(ss_operator_inc, ss_type_float);\
		ADD_VQ_PTR_NONREF_VQ_PTR_REF_INT(ss_operator_inc, ss_type_double);\
		ADD_VQ_PTR_NONREF_VQ_PTR_REF_INT(ss_operator_inc, ss_type_ldouble);\
\
		ADD_VQ_PTR_REF_VQ_PTR_REF(ss_operator_dec, ss_type_schar);\
		ADD_VQ_PTR_REF_VQ_PTR_REF(ss_operator_dec, ss_type_sshort);\
		ADD_VQ_PTR_REF_VQ_PTR_REF(ss_operator_dec, ss_type_sint);\
		ADD_VQ_PTR_REF_VQ_PTR_REF(ss_operator_dec, ss_type_slong);\
		ADD_VQ_PTR_REF_VQ_PTR_REF(ss_operator_dec, ss_type_uchar);\
		ADD_VQ_PTR_REF_VQ_PTR_REF(ss_operator_dec, ss_type_pchar);\
		ADD_VQ_PTR_REF_VQ_PTR_REF(ss_operator_dec, ss_type_ushort);\
		ADD_VQ_PTR_REF_VQ_PTR_REF(ss_operator_dec, ss_type_uint);\
		ADD_VQ_PTR_REF_VQ_PTR_REF(ss_operator_dec, ss_type_ulong);\
		ADD_VQ_PTR_REF_VQ_PTR_REF(ss_operator_dec, ss_type_float);\
		ADD_VQ_PTR_REF_VQ_PTR_REF(ss_operator_dec, ss_type_double);\
		ADD_VQ_PTR_REF_VQ_PTR_REF(ss_operator_dec, ss_type_ldouble);\
	\
		ADD_VQ_PTR_NONREF_VQ_PTR_REF_INT(ss_operator_dec, ss_type_schar);\
		ADD_VQ_PTR_NONREF_VQ_PTR_REF_INT(ss_operator_dec, ss_type_sshort);\
		ADD_VQ_PTR_NONREF_VQ_PTR_REF_INT(ss_operator_dec, ss_type_sint);\
		ADD_VQ_PTR_NONREF_VQ_PTR_REF_INT(ss_operator_dec, ss_type_slong);\
		ADD_VQ_PTR_NONREF_VQ_PTR_REF_INT(ss_operator_dec, ss_type_uchar);\
		ADD_VQ_PTR_NONREF_VQ_PTR_REF_INT(ss_operator_dec, ss_type_pchar);\
		ADD_VQ_PTR_NONREF_VQ_PTR_REF_INT(ss_operator_dec, ss_type_ushort);\
		ADD_VQ_PTR_NONREF_VQ_PTR_REF_INT(ss_operator_dec, ss_type_uint);\
		ADD_VQ_PTR_NONREF_VQ_PTR_REF_INT(ss_operator_dec, ss_type_ulong);\
		ADD_VQ_PTR_NONREF_VQ_PTR_REF_INT(ss_operator_dec, ss_type_float);\
		ADD_VQ_PTR_NONREF_VQ_PTR_REF_INT(ss_operator_dec, ss_type_double);\
		ADD_VQ_PTR_NONREF_VQ_PTR_REF_INT(ss_operator_dec, ss_type_ldouble);\
		\
		/* [13.6/6] and [13.6/7]. Pointer dereference.
		 * 	T& operator*(T*); 
		 */\
\
		ADD_REF_PTR(ss_operator_mul, ss_type_schar);\
		ADD_REF_PTR(ss_operator_mul, ss_type_sshort);\
		ADD_REF_PTR(ss_operator_mul, ss_type_sint);\
		ADD_REF_PTR(ss_operator_mul, ss_type_slong);\
		ADD_REF_PTR(ss_operator_mul, ss_type_uchar);\
		ADD_REF_PTR(ss_operator_mul, ss_type_pchar);\
		ADD_REF_PTR(ss_operator_mul, ss_type_ushort);\
		ADD_REF_PTR(ss_operator_mul, ss_type_uint);\
		ADD_REF_PTR(ss_operator_mul, ss_type_ulong);\
		ADD_REF_PTR(ss_operator_mul, ss_type_float);\
		ADD_REF_PTR(ss_operator_mul, ss_type_double);\
		ADD_REF_PTR(ss_operator_mul, ss_type_ldouble);\
		ADD_REF_PTR_CONST(ss_operator_mul, ss_type_schar);\
		ADD_REF_PTR_CONST(ss_operator_mul, ss_type_sshort);\
		ADD_REF_PTR_CONST(ss_operator_mul, ss_type_sint);\
		ADD_REF_PTR_CONST(ss_operator_mul, ss_type_slong);\
		ADD_REF_PTR_CONST(ss_operator_mul, ss_type_uchar);\
		ADD_REF_PTR_CONST(ss_operator_mul, ss_type_pchar);\
		ADD_REF_PTR_CONST(ss_operator_mul, ss_type_ushort);\
		ADD_REF_PTR_CONST(ss_operator_mul, ss_type_uint);\
		ADD_REF_PTR_CONST(ss_operator_mul, ss_type_ulong);\
		ADD_REF_PTR_CONST(ss_operator_mul, ss_type_float);\
		ADD_REF_PTR_CONST(ss_operator_mul, ss_type_double);\
		ADD_REF_PTR_CONST(ss_operator_mul, ss_type_ldouble);\
		\
		/* [13.6/8] is added to the candidates list later for the current type */\
\
		/* [13.6/9] \
		 * For every promoted arithmetic type T (int, long, float, double), there exists candidate operator functions of the form \
\
		 	T operator+(T);\
			T operator-(T);\
		*/\
\
		ADD_NONREF_NONREF(ss_operator_add, ss_type_sint, ss_type_sint);\
		ADD_NONREF_NONREF(ss_operator_add, ss_type_uint, ss_type_uint);\
		ADD_NONREF_NONREF(ss_operator_add, ss_type_slong, ss_type_slong);\
		ADD_NONREF_NONREF(ss_operator_add, ss_type_ulong, ss_type_ulong);\
		ADD_NONREF_NONREF(ss_operator_add, ss_type_float, ss_type_float);\
		ADD_NONREF_NONREF(ss_operator_add, ss_type_double, ss_type_double);\
		ADD_NONREF_NONREF(ss_operator_add, ss_type_ldouble, ss_type_ldouble);\
\
		ADD_NONREF_NONREF(ss_operator_sub, ss_type_sint, ss_type_sint);\
		ADD_NONREF_NONREF(ss_operator_sub, ss_type_uint, ss_type_uint);\
		ADD_NONREF_NONREF(ss_operator_sub, ss_type_slong, ss_type_slong);\
		ADD_NONREF_NONREF(ss_operator_sub, ss_type_ulong, ss_type_ulong);\
		ADD_NONREF_NONREF(ss_operator_sub, ss_type_float, ss_type_float);\
		ADD_NONREF_NONREF(ss_operator_sub, ss_type_double, ss_type_double);\
		ADD_NONREF_NONREF(ss_operator_sub, ss_type_ldouble, ss_type_ldouble);\
\
		/* [13.6/10] \
		 * For every promoted integral type T (int, long), there exists candidate operator functions of the form \
\
			T operator~(T);\
		*/\
		\
		ADD_NONREF_NONREF(ss_operator_bnot, ss_type_sint, ss_type_sint);\
		ADD_NONREF_NONREF(ss_operator_bnot, ss_type_uint, ss_type_uint);\
		ADD_NONREF_NONREF(ss_operator_bnot, ss_type_slong, ss_type_slong);\
		ADD_NONREF_NONREF(ss_operator_bnot, ss_type_ulong, ss_type_ulong);\
\
		/* [13.6/11] \
		   is TODO\
		 */\
\
		/* [13.6/12]\
	   	 * For every pair of promoted arithmetic types L and R there exist candidate operator functions of the form\
\
			LR operator*(L,R);		 \
			LR operator/(L,R);		 \
			LR operator+(L,R);		 \
			LR operator-(L,R);		 \
\
			bool operator<(L,R);\
			bool operator>(L,R);\
			bool operator<=(L,R);\
			bool operator>=(L,R);\
			bool operator==(L,R);\
			bool operator!=(L,R);\
			\
		*/\
\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_sint, ss_type_sint, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_uint, ss_type_sint, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_slong, ss_type_sint, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_ulong, ss_type_sint, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_sint, ss_type_sint, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_sint, ss_type_sint, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_sint, ss_type_sint, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_uint, ss_type_uint, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_uint, ss_type_uint, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_slong, ss_type_uint, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_ulong, ss_type_uint, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_uint, ss_type_uint, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_uint, ss_type_uint, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_uint, ss_type_uint, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_slong, ss_type_slong, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_slong, ss_type_slong, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_slong, ss_type_slong, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_slong, ss_type_slong, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_slong, ss_type_slong, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_slong, ss_type_slong, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_slong, ss_type_slong, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_ulong, ss_type_ulong, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_ulong, ss_type_ulong, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_ulong, ss_type_ulong, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_ulong, ss_type_ulong, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_ulong, ss_type_ulong, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_ulong, ss_type_ulong, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_ulong, ss_type_ulong, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_sint, ss_type_float, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_uint, ss_type_float, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_slong, ss_type_float, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_ulong, ss_type_float, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_float, ss_type_float, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_double, ss_type_float, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_ldouble, ss_type_float, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_sint, ss_type_double, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_uint, ss_type_double, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_slong, ss_type_double, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_ulong, ss_type_double, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_double, ss_type_double, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_double, ss_type_double, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_ldouble, ss_type_double, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_sint, ss_type_ldouble, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_uint, ss_type_ldouble, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_slong, ss_type_ldouble, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_ulong, ss_type_ldouble, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_ldouble, ss_type_ldouble, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_ldouble, ss_type_ldouble, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_add, ss_type_ldouble, ss_type_ldouble, ss_type_ldouble);\
\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_sint, ss_type_sint, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_uint, ss_type_sint, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_slong, ss_type_sint, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_ulong, ss_type_sint, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_sint, ss_type_sint, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_sint, ss_type_sint, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_sint, ss_type_sint, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_uint, ss_type_uint, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_uint, ss_type_uint, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_slong, ss_type_uint, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_ulong, ss_type_uint, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_uint, ss_type_uint, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_uint, ss_type_uint, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_uint, ss_type_uint, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_slong, ss_type_slong, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_slong, ss_type_slong, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_slong, ss_type_slong, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_slong, ss_type_slong, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_slong, ss_type_slong, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_slong, ss_type_slong, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_slong, ss_type_slong, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_ulong, ss_type_ulong, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_ulong, ss_type_ulong, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_ulong, ss_type_ulong, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_ulong, ss_type_ulong, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_ulong, ss_type_ulong, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_ulong, ss_type_ulong, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_ulong, ss_type_ulong, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_sint, ss_type_float, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_uint, ss_type_float, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_slong, ss_type_float, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_ulong, ss_type_float, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_float, ss_type_float, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_double, ss_type_float, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_ldouble, ss_type_float, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_sint, ss_type_double, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_uint, ss_type_double, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_slong, ss_type_double, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_ulong, ss_type_double, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_double, ss_type_double, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_double, ss_type_double, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_ldouble, ss_type_double, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_sint, ss_type_ldouble, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_uint, ss_type_ldouble, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_slong, ss_type_ldouble, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_ulong, ss_type_ldouble, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_ldouble, ss_type_ldouble, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_ldouble, ss_type_ldouble, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sub, ss_type_ldouble, ss_type_ldouble, ss_type_ldouble);\
\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_sint, ss_type_sint, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_uint, ss_type_sint, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_slong, ss_type_sint, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_ulong, ss_type_sint, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_sint, ss_type_sint, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_sint, ss_type_sint, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_sint, ss_type_sint, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_uint, ss_type_uint, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_uint, ss_type_uint, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_slong, ss_type_uint, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_ulong, ss_type_uint, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_uint, ss_type_uint, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_uint, ss_type_uint, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_uint, ss_type_uint, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_slong, ss_type_slong, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_slong, ss_type_slong, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_slong, ss_type_slong, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_slong, ss_type_slong, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_slong, ss_type_slong, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_slong, ss_type_slong, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_slong, ss_type_slong, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_ulong, ss_type_ulong, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_ulong, ss_type_ulong, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_ulong, ss_type_ulong, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_ulong, ss_type_ulong, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_ulong, ss_type_ulong, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_ulong, ss_type_ulong, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_ulong, ss_type_ulong, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_sint, ss_type_float, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_uint, ss_type_float, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_slong, ss_type_float, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_ulong, ss_type_float, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_float, ss_type_float, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_double, ss_type_float, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_ldouble, ss_type_float, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_sint, ss_type_double, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_uint, ss_type_double, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_slong, ss_type_double, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_ulong, ss_type_double, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_double, ss_type_double, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_double, ss_type_double, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_ldouble, ss_type_double, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_sint, ss_type_ldouble, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_uint, ss_type_ldouble, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_slong, ss_type_ldouble, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_ulong, ss_type_ldouble, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_ldouble, ss_type_ldouble, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_ldouble, ss_type_ldouble, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mul, ss_type_ldouble, ss_type_ldouble, ss_type_ldouble);\
\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_sint, ss_type_sint, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_uint, ss_type_sint, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_slong, ss_type_sint, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_ulong, ss_type_sint, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_sint, ss_type_sint, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_sint, ss_type_sint, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_sint, ss_type_sint, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_uint, ss_type_uint, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_uint, ss_type_uint, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_slong, ss_type_uint, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_ulong, ss_type_uint, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_uint, ss_type_uint, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_uint, ss_type_uint, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_uint, ss_type_uint, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_slong, ss_type_slong, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_slong, ss_type_slong, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_slong, ss_type_slong, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_slong, ss_type_slong, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_slong, ss_type_slong, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_slong, ss_type_slong, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_slong, ss_type_slong, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_ulong, ss_type_ulong, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_ulong, ss_type_ulong, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_ulong, ss_type_ulong, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_ulong, ss_type_ulong, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_ulong, ss_type_ulong, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_ulong, ss_type_ulong, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_ulong, ss_type_ulong, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_sint, ss_type_float, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_uint, ss_type_float, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_slong, ss_type_float, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_ulong, ss_type_float, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_float, ss_type_float, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_double, ss_type_float, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_ldouble, ss_type_float, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_sint, ss_type_double, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_uint, ss_type_double, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_slong, ss_type_double, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_ulong, ss_type_double, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_double, ss_type_double, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_double, ss_type_double, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_ldouble, ss_type_double, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_sint, ss_type_ldouble, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_uint, ss_type_ldouble, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_slong, ss_type_ldouble, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_ulong, ss_type_ldouble, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_ldouble, ss_type_ldouble, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_ldouble, ss_type_ldouble, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_div, ss_type_ldouble, ss_type_ldouble, ss_type_ldouble);\
\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_sint, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_sint, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_sint, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_sint, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_sint, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_sint, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_sint, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_uint, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_uint, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_uint, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_uint, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_uint, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_uint, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_uint, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_slong, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_slong, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_slong, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_slong, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_slong, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_slong, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_slong, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_ulong, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_ulong, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_ulong, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_ulong, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_ulong, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_ulong, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_ulong, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_float, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_float, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_float, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_float, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_float, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_float, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_float, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_double, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_double, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_double, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_double, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_double, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_double, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_double, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_ldouble, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_ldouble, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_ldouble, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_ldouble, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_ldouble, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_ldouble, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbe, ss_bool, ss_type_ldouble, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_sint, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_sint, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_sint, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_sint, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_sint, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_sint, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_sint, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_uint, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_uint, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_uint, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_uint, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_uint, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_uint, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_uint, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_slong, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_slong, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_slong, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_slong, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_slong, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_slong, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_slong, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_ulong, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_ulong, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_ulong, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_ulong, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_ulong, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_ulong, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_ulong, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_float, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_float, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_float, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_float, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_float, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_float, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_float, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_double, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_double, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_double, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_double, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_double, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_double, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_double, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_ldouble, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_ldouble, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_ldouble, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_ldouble, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_ldouble, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_ldouble, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbne, ss_bool, ss_type_ldouble, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_sint, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_sint, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_sint, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_sint, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_sint, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_sint, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_sint, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_uint, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_uint, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_uint, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_uint, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_uint, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_uint, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_uint, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_slong, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_slong, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_slong, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_slong, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_slong, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_slong, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_slong, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_ulong, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_ulong, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_ulong, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_ulong, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_ulong, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_ulong, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_ulong, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_float, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_float, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_float, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_float, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_float, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_float, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_float, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_double, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_double, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_double, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_double, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_double, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_double, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_double, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_ldouble, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_ldouble, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_ldouble, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_ldouble, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_ldouble, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_ldouble, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbng, ss_bool, ss_type_ldouble, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_sint, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_sint, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_sint, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_sint, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_sint, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_sint, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_sint, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_uint, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_uint, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_uint, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_uint, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_uint, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_uint, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_uint, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_slong, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_slong, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_slong, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_slong, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_slong, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_slong, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_slong, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_ulong, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_ulong, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_ulong, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_ulong, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_ulong, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_ulong, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_ulong, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_float, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_float, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_float, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_float, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_float, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_float, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_float, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_double, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_double, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_double, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_double, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_double, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_double, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_double, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_ldouble, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_ldouble, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_ldouble, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_ldouble, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_ldouble, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_ldouble, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbnl, ss_bool, ss_type_ldouble, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_sint, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_sint, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_sint, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_sint, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_sint, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_sint, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_sint, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_uint, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_uint, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_uint, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_uint, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_uint, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_uint, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_uint, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_slong, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_slong, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_slong, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_slong, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_slong, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_slong, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_slong, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_ulong, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_ulong, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_ulong, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_ulong, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_ulong, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_ulong, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_ulong, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_float, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_float, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_float, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_float, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_float, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_float, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_float, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_double, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_double, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_double, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_double, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_double, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_double, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_double, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_ldouble, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_ldouble, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_ldouble, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_ldouble, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_ldouble, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_ldouble, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbg, ss_bool, ss_type_ldouble, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_sint, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_sint, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_sint, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_sint, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_sint, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_sint, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_sint, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_uint, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_uint, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_uint, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_uint, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_uint, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_uint, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_uint, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_slong, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_slong, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_slong, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_slong, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_slong, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_slong, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_slong, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_ulong, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_ulong, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_ulong, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_ulong, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_ulong, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_ulong, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_ulong, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_float, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_float, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_float, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_float, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_float, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_float, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_float, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_double, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_double, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_double, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_double, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_double, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_double, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_double, ss_type_ldouble);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_ldouble, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_ldouble, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_ldouble, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_ldouble, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_ldouble, ss_type_float);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_ldouble, ss_type_double);\
		ADD_NONREF_NONREF_NONREF(ss_operator_sbl, ss_bool, ss_type_ldouble, ss_type_ldouble);\
		/* [13.6/13]
		 * TODO add[]
		 *
		 * 	T* operator+(T*, ptrdiff_t);
		 *	T* operator-(T*, ptrdiff_t);
		 *	T* operator-(T*, ptrdiff_t);
		 */\
		ADD_PTR_PTR_INT(ss_operator_add, ss_bool);\
		ADD_PTR_PTR_INT(ss_operator_add, ss_type_schar);\
		ADD_PTR_PTR_INT(ss_operator_add, ss_type_sshort);\
		ADD_PTR_PTR_INT(ss_operator_add, ss_type_sint);\
		ADD_PTR_PTR_INT(ss_operator_add, ss_type_slong);\
		ADD_PTR_PTR_INT(ss_operator_add, ss_type_uchar);\
		ADD_PTR_PTR_INT(ss_operator_add, ss_type_pchar);\
		ADD_PTR_PTR_INT(ss_operator_add, ss_type_ushort);\
		ADD_PTR_PTR_INT(ss_operator_add, ss_type_uint);\
		ADD_PTR_PTR_INT(ss_operator_add, ss_type_ulong);\
		ADD_PTR_PTR_INT(ss_operator_add, ss_type_float);\
		ADD_PTR_PTR_INT(ss_operator_add, ss_type_double);\
		ADD_PTR_PTR_INT(ss_operator_add, ss_type_ldouble);\
\
		ADD_PTR_PTR_INT(ss_operator_sub, ss_bool);\
		ADD_PTR_PTR_INT(ss_operator_sub, ss_type_schar);\
		ADD_PTR_PTR_INT(ss_operator_sub, ss_type_sshort);\
		ADD_PTR_PTR_INT(ss_operator_sub, ss_type_sint);\
		ADD_PTR_PTR_INT(ss_operator_sub, ss_type_slong);\
		ADD_PTR_PTR_INT(ss_operator_sub, ss_type_uchar);\
		ADD_PTR_PTR_INT(ss_operator_sub, ss_type_pchar);\
		ADD_PTR_PTR_INT(ss_operator_sub, ss_type_ushort);\
		ADD_PTR_PTR_INT(ss_operator_sub, ss_type_uint);\
		ADD_PTR_PTR_INT(ss_operator_sub, ss_type_ulong);\
		ADD_PTR_PTR_INT(ss_operator_sub, ss_type_float);\
		ADD_PTR_PTR_INT(ss_operator_sub, ss_type_double);\
		ADD_PTR_PTR_INT(ss_operator_sub, ss_type_ldouble);\
\
		ADD_PTR_INT_PTR(ss_operator_add, ss_bool);\
		ADD_PTR_INT_PTR(ss_operator_add, ss_type_schar);\
		ADD_PTR_INT_PTR(ss_operator_add, ss_type_sshort);\
		ADD_PTR_INT_PTR(ss_operator_add, ss_type_sint);\
		ADD_PTR_INT_PTR(ss_operator_add, ss_type_slong);\
		ADD_PTR_INT_PTR(ss_operator_add, ss_type_uchar);\
		ADD_PTR_INT_PTR(ss_operator_add, ss_type_pchar);\
		ADD_PTR_INT_PTR(ss_operator_add, ss_type_ushort);\
		ADD_PTR_INT_PTR(ss_operator_add, ss_type_uint);\
		ADD_PTR_INT_PTR(ss_operator_add, ss_type_ulong);\
		ADD_PTR_INT_PTR(ss_operator_add, ss_type_float);\
		ADD_PTR_INT_PTR(ss_operator_add, ss_type_double);\
		ADD_PTR_INT_PTR(ss_operator_add, ss_type_ldouble);\
\
\
		/* [13.6/14]
		 *
		 *	ptrdiff_t operator-(T*, T*);
		 */\
		ADD_INT_PTR_PTR(ss_operator_sub, ss_bool);\
		ADD_INT_PTR_PTR(ss_operator_sub, ss_type_schar);\
		ADD_INT_PTR_PTR(ss_operator_sub, ss_type_sshort);\
		ADD_INT_PTR_PTR(ss_operator_sub, ss_type_sint);\
		ADD_INT_PTR_PTR(ss_operator_sub, ss_type_slong);\
		ADD_INT_PTR_PTR(ss_operator_sub, ss_type_uchar);\
		ADD_INT_PTR_PTR(ss_operator_sub, ss_type_pchar);\
		ADD_INT_PTR_PTR(ss_operator_sub, ss_type_ushort);\
		ADD_INT_PTR_PTR(ss_operator_sub, ss_type_uint);\
		ADD_INT_PTR_PTR(ss_operator_sub, ss_type_ulong);\
		ADD_INT_PTR_PTR(ss_operator_sub, ss_type_float);\
		ADD_INT_PTR_PTR(ss_operator_sub, ss_type_double);\
		ADD_INT_PTR_PTR(ss_operator_sub, ss_type_ldouble);\
\
		/* [13.6/15]
		 *
		 * TODO this is not only for T* but also for enumeration
		 *
		 *	bool operator<(T*,T*);\
		 *	bool operator>(T*,T*);\
		 *	bool operator<=(T*,T*);\
		 *	bool operator>=(T*,T*);\
		 *	bool operator==(T*,T*);\
		 *	bool operator!=(T*,T*);\
		 *
		 */\
\
		ADD_BOOL_PTR_PTR(ss_operator_sbe, ss_bool);\
		ADD_BOOL_PTR_PTR(ss_operator_sbe, ss_type_uchar);\
		ADD_BOOL_PTR_PTR(ss_operator_sbe, ss_type_schar);\
		ADD_BOOL_PTR_PTR(ss_operator_sbe, ss_type_sshort);\
		ADD_BOOL_PTR_PTR(ss_operator_sbe, ss_type_ushort);\
		ADD_BOOL_PTR_PTR(ss_operator_sbe, ss_type_sint);\
		ADD_BOOL_PTR_PTR(ss_operator_sbe, ss_type_uint);\
		ADD_BOOL_PTR_PTR(ss_operator_sbe, ss_type_slong);\
		ADD_BOOL_PTR_PTR(ss_operator_sbe, ss_type_ulong);\
		ADD_BOOL_PTR_PTR(ss_operator_sbe, ss_type_float);\
		ADD_BOOL_PTR_PTR(ss_operator_sbe, ss_type_double);\
		ADD_BOOL_PTR_PTR(ss_operator_sbe, ss_type_ldouble);\
		ADD_BOOL_PTR_PTR(ss_operator_sbne, ss_bool);\
		ADD_BOOL_PTR_PTR(ss_operator_sbne, ss_type_uchar);\
		ADD_BOOL_PTR_PTR(ss_operator_sbne, ss_type_schar);\
		ADD_BOOL_PTR_PTR(ss_operator_sbne, ss_type_sshort);\
		ADD_BOOL_PTR_PTR(ss_operator_sbne, ss_type_ushort);\
		ADD_BOOL_PTR_PTR(ss_operator_sbne, ss_type_sint);\
		ADD_BOOL_PTR_PTR(ss_operator_sbne, ss_type_uint);\
		ADD_BOOL_PTR_PTR(ss_operator_sbne, ss_type_slong);\
		ADD_BOOL_PTR_PTR(ss_operator_sbne, ss_type_ulong);\
		ADD_BOOL_PTR_PTR(ss_operator_sbne, ss_type_float);\
		ADD_BOOL_PTR_PTR(ss_operator_sbne, ss_type_double);\
		ADD_BOOL_PTR_PTR(ss_operator_sbne, ss_type_ldouble);\
		ADD_BOOL_PTR_PTR(ss_operator_sbng, ss_bool);\
		ADD_BOOL_PTR_PTR(ss_operator_sbng, ss_type_uchar);\
		ADD_BOOL_PTR_PTR(ss_operator_sbng, ss_type_schar);\
		ADD_BOOL_PTR_PTR(ss_operator_sbng, ss_type_sshort);\
		ADD_BOOL_PTR_PTR(ss_operator_sbng, ss_type_ushort);\
		ADD_BOOL_PTR_PTR(ss_operator_sbng, ss_type_sint);\
		ADD_BOOL_PTR_PTR(ss_operator_sbng, ss_type_uint);\
		ADD_BOOL_PTR_PTR(ss_operator_sbng, ss_type_slong);\
		ADD_BOOL_PTR_PTR(ss_operator_sbng, ss_type_ulong);\
		ADD_BOOL_PTR_PTR(ss_operator_sbng, ss_type_float);\
		ADD_BOOL_PTR_PTR(ss_operator_sbng, ss_type_double);\
		ADD_BOOL_PTR_PTR(ss_operator_sbng, ss_type_ldouble);\
		ADD_BOOL_PTR_PTR(ss_operator_sbnl, ss_bool);\
		ADD_BOOL_PTR_PTR(ss_operator_sbnl, ss_type_uchar);\
		ADD_BOOL_PTR_PTR(ss_operator_sbnl, ss_type_schar);\
		ADD_BOOL_PTR_PTR(ss_operator_sbnl, ss_type_sshort);\
		ADD_BOOL_PTR_PTR(ss_operator_sbnl, ss_type_ushort);\
		ADD_BOOL_PTR_PTR(ss_operator_sbnl, ss_type_sint);\
		ADD_BOOL_PTR_PTR(ss_operator_sbnl, ss_type_uint);\
		ADD_BOOL_PTR_PTR(ss_operator_sbnl, ss_type_slong);\
		ADD_BOOL_PTR_PTR(ss_operator_sbnl, ss_type_ulong);\
		ADD_BOOL_PTR_PTR(ss_operator_sbnl, ss_type_float);\
		ADD_BOOL_PTR_PTR(ss_operator_sbnl, ss_type_double);\
		ADD_BOOL_PTR_PTR(ss_operator_sbnl, ss_type_ldouble);\
		ADD_BOOL_PTR_PTR(ss_operator_sbg, ss_bool);\
		ADD_BOOL_PTR_PTR(ss_operator_sbg, ss_type_uchar);\
		ADD_BOOL_PTR_PTR(ss_operator_sbg, ss_type_schar);\
		ADD_BOOL_PTR_PTR(ss_operator_sbg, ss_type_sshort);\
		ADD_BOOL_PTR_PTR(ss_operator_sbg, ss_type_ushort);\
		ADD_BOOL_PTR_PTR(ss_operator_sbg, ss_type_sint);\
		ADD_BOOL_PTR_PTR(ss_operator_sbg, ss_type_uint);\
		ADD_BOOL_PTR_PTR(ss_operator_sbg, ss_type_slong);\
		ADD_BOOL_PTR_PTR(ss_operator_sbg, ss_type_ulong);\
		ADD_BOOL_PTR_PTR(ss_operator_sbg, ss_type_float);\
		ADD_BOOL_PTR_PTR(ss_operator_sbg, ss_type_double);\
		ADD_BOOL_PTR_PTR(ss_operator_sbg, ss_type_ldouble);\
		ADD_BOOL_PTR_PTR(ss_operator_sbl, ss_bool);\
		ADD_BOOL_PTR_PTR(ss_operator_sbl, ss_type_uchar);\
		ADD_BOOL_PTR_PTR(ss_operator_sbl, ss_type_schar);\
		ADD_BOOL_PTR_PTR(ss_operator_sbl, ss_type_sshort);\
		ADD_BOOL_PTR_PTR(ss_operator_sbl, ss_type_ushort);\
		ADD_BOOL_PTR_PTR(ss_operator_sbl, ss_type_sint);\
		ADD_BOOL_PTR_PTR(ss_operator_sbl, ss_type_uint);\
		ADD_BOOL_PTR_PTR(ss_operator_sbl, ss_type_slong);\
		ADD_BOOL_PTR_PTR(ss_operator_sbl, ss_type_ulong);\
		ADD_BOOL_PTR_PTR(ss_operator_sbl, ss_type_float);\
		ADD_BOOL_PTR_PTR(ss_operator_sbl, ss_type_double);\
		ADD_BOOL_PTR_PTR(ss_operator_sbl, ss_type_ldouble);\
\
		/* TODO FIXME [13.6/16] */ \
		/* [13.6/17]  \
		 * For every pair of promoted integral types L and R, there exists candidate operator \
		 * functions of the form: \

		 	LR operator&(L, R);\
			LR operator%(L, R);\
			LR operator^(L, R);\
			LR operator|(L, R);\
			L operator<<(L, R);\
			L operator>>(L, R);\
		*/\
\
		ADD_NONREF_NONREF_NONREF(ss_operator_mod, ss_type_sint, ss_type_sint, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mod, ss_type_uint, ss_type_sint, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mod, ss_type_slong, ss_type_sint, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mod, ss_type_ulong, ss_type_sint, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mod, ss_type_uint, ss_type_uint, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mod, ss_type_uint, ss_type_uint, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mod, ss_type_slong, ss_type_uint, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mod, ss_type_ulong, ss_type_uint, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mod, ss_type_slong, ss_type_slong, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mod, ss_type_slong, ss_type_slong, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mod, ss_type_slong, ss_type_slong, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mod, ss_type_ulong, ss_type_slong, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mod, ss_type_ulong, ss_type_ulong, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mod, ss_type_ulong, ss_type_ulong, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mod, ss_type_ulong, ss_type_ulong, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_mod, ss_type_ulong, ss_type_ulong, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_band, ss_type_sint, ss_type_sint, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_band, ss_type_uint, ss_type_sint, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_band, ss_type_slong, ss_type_sint, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_band, ss_type_ulong, ss_type_sint, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_band, ss_type_uint, ss_type_uint, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_band, ss_type_uint, ss_type_uint, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_band, ss_type_slong, ss_type_uint, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_band, ss_type_ulong, ss_type_uint, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_band, ss_type_slong, ss_type_slong, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_band, ss_type_slong, ss_type_slong, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_band, ss_type_slong, ss_type_slong, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_band, ss_type_ulong, ss_type_slong, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_band, ss_type_ulong, ss_type_ulong, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_band, ss_type_ulong, ss_type_ulong, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_band, ss_type_ulong, ss_type_ulong, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_band, ss_type_ulong, ss_type_ulong, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_bxor, ss_type_sint, ss_type_sint, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_bxor, ss_type_uint, ss_type_sint, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_bxor, ss_type_slong, ss_type_sint, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_bxor, ss_type_ulong, ss_type_sint, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_bxor, ss_type_uint, ss_type_uint, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_bxor, ss_type_uint, ss_type_uint, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_bxor, ss_type_slong, ss_type_uint, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_bxor, ss_type_ulong, ss_type_uint, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_bxor, ss_type_slong, ss_type_slong, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_bxor, ss_type_slong, ss_type_slong, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_bxor, ss_type_slong, ss_type_slong, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_bxor, ss_type_ulong, ss_type_slong, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_bxor, ss_type_ulong, ss_type_ulong, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_bxor, ss_type_ulong, ss_type_ulong, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_bxor, ss_type_ulong, ss_type_ulong, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_bxor, ss_type_ulong, ss_type_ulong, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_bor, ss_type_sint, ss_type_sint, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_bor, ss_type_uint, ss_type_sint, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_bor, ss_type_slong, ss_type_sint, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_bor, ss_type_ulong, ss_type_sint, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_bor, ss_type_uint, ss_type_uint, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_bor, ss_type_uint, ss_type_uint, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_bor, ss_type_slong, ss_type_uint, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_bor, ss_type_ulong, ss_type_uint, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_bor, ss_type_slong, ss_type_slong, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_bor, ss_type_slong, ss_type_slong, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_bor, ss_type_slong, ss_type_slong, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_bor, ss_type_ulong, ss_type_slong, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_bor, ss_type_ulong, ss_type_ulong, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_bor, ss_type_ulong, ss_type_ulong, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_bor, ss_type_ulong, ss_type_ulong, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_bor, ss_type_ulong, ss_type_ulong, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_shl, ss_type_sint, ss_type_sint, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_shl, ss_type_uint, ss_type_sint, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_shl, ss_type_slong, ss_type_sint, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_shl, ss_type_ulong, ss_type_sint, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_shl, ss_type_uint, ss_type_uint, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_shl, ss_type_uint, ss_type_uint, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_shl, ss_type_slong, ss_type_uint, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_shl, ss_type_ulong, ss_type_uint, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_shl, ss_type_slong, ss_type_slong, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_shl, ss_type_slong, ss_type_slong, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_shl, ss_type_slong, ss_type_slong, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_shl, ss_type_ulong, ss_type_slong, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_shl, ss_type_ulong, ss_type_ulong, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_shl, ss_type_ulong, ss_type_ulong, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_shl, ss_type_ulong, ss_type_ulong, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_shl, ss_type_ulong, ss_type_ulong, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_shr, ss_type_sint, ss_type_sint, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_shr, ss_type_uint, ss_type_sint, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_shr, ss_type_slong, ss_type_sint, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_shr, ss_type_ulong, ss_type_sint, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_shr, ss_type_uint, ss_type_uint, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_shr, ss_type_uint, ss_type_uint, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_shr, ss_type_slong, ss_type_uint, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_shr, ss_type_ulong, ss_type_uint, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_shr, ss_type_slong, ss_type_slong, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_shr, ss_type_slong, ss_type_slong, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_shr, ss_type_slong, ss_type_slong, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_shr, ss_type_ulong, ss_type_slong, ss_type_ulong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_shr, ss_type_ulong, ss_type_ulong, ss_type_sint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_shr, ss_type_ulong, ss_type_ulong, ss_type_uint);\
		ADD_NONREF_NONREF_NONREF(ss_operator_shr, ss_type_ulong, ss_type_ulong, ss_type_slong);\
		ADD_NONREF_NONREF_NONREF(ss_operator_shr, ss_type_ulong, ss_type_ulong, ss_type_ulong);\
		\
		/* [13.6/18] \
		 * For every triple (L, VQ, R), where L is arithmetic type, VQ is either volatile or empty \
		 * and R is promoted arithmetic type, there exists candidate operator functions of the form \
		 \
		 	VQ L& operator=(VQ L&, R);\
		 	VQ L& operator*=(VQ L&, R);\
		 	VQ L& operator/=(VQ L&, R);\
		 	VQ L& operator+=(VQ L&, R);\
		 	VQ L& operator-=(VQ L&, R);\
		*/\
\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_bool, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_bool, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_bool, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_bool, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_bool, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_bool, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_bool, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_schar, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_schar, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_schar, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_schar, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_schar, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_schar, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_schar, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_uchar, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_uchar, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_uchar, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_uchar, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_uchar, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_uchar, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_uchar, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_pchar, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_pchar, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_pchar, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_pchar, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_pchar, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_pchar, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_pchar, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_sshort, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_sshort, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_sshort, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_sshort, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_sshort, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_sshort, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_sshort, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_ushort, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_ushort, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_ushort, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_ushort, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_ushort, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_ushort, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_ushort, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_sint, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_sint, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_sint, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_sint, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_sint, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_sint, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_sint, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_uint, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_uint, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_uint, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_uint, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_uint, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_uint, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_uint, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_slong, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_slong, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_slong, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_slong, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_slong, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_slong, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_slong, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_ulong, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_ulong, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_ulong, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_ulong, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_ulong, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_ulong, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_ulong, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_float, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_float, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_float, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_float, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_float, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_float, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_float, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_double, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_double, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_double, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_double, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_double, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_double, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_double, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_ldouble, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_ldouble, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_ldouble, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_ldouble, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_ldouble, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_ldouble, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF_REF(ss_operator_assign, ss_type_ldouble, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_bool, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_bool, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_bool, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_bool, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_bool, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_bool, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_bool, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_schar, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_schar, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_schar, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_schar, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_schar, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_schar, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_schar, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_uchar, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_uchar, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_uchar, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_uchar, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_uchar, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_uchar, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_uchar, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_pchar, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_pchar, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_pchar, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_pchar, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_pchar, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_pchar, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_pchar, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_sshort, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_sshort, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_sshort, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_sshort, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_sshort, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_sshort, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_sshort, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_ushort, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_ushort, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_ushort, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_ushort, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_ushort, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_ushort, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_ushort, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_sint, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_sint, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_sint, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_sint, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_sint, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_sint, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_sint, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_uint, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_uint, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_uint, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_uint, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_uint, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_uint, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_uint, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_slong, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_slong, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_slong, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_slong, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_slong, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_slong, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_slong, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_ulong, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_ulong, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_ulong, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_ulong, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_ulong, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_ulong, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_ulong, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_float, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_float, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_float, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_float, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_float, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_float, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_float, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_double, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_double, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_double, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_double, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_double, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_double, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_double, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_ldouble, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_ldouble, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_ldouble, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_ldouble, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_ldouble, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_ldouble, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mul, ss_type_ldouble, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_bool, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_bool, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_bool, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_bool, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_bool, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_bool, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_bool, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_schar, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_schar, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_schar, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_schar, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_schar, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_schar, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_schar, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_uchar, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_uchar, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_uchar, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_uchar, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_uchar, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_uchar, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_uchar, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_pchar, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_pchar, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_pchar, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_pchar, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_pchar, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_pchar, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_pchar, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_sshort, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_sshort, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_sshort, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_sshort, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_sshort, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_sshort, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_sshort, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_ushort, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_ushort, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_ushort, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_ushort, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_ushort, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_ushort, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_ushort, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_sint, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_sint, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_sint, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_sint, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_sint, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_sint, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_sint, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_uint, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_uint, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_uint, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_uint, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_uint, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_uint, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_uint, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_slong, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_slong, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_slong, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_slong, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_slong, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_slong, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_slong, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_ulong, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_ulong, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_ulong, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_ulong, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_ulong, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_ulong, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_ulong, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_float, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_float, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_float, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_float, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_float, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_float, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_float, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_double, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_double, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_double, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_double, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_double, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_double, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_double, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_ldouble, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_ldouble, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_ldouble, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_ldouble, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_ldouble, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_ldouble, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_div, ss_type_ldouble, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_bool, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_bool, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_bool, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_bool, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_bool, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_bool, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_bool, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_schar, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_schar, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_schar, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_schar, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_schar, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_schar, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_schar, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_uchar, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_uchar, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_uchar, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_uchar, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_uchar, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_uchar, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_uchar, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_pchar, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_pchar, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_pchar, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_pchar, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_pchar, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_pchar, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_pchar, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_sshort, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_sshort, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_sshort, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_sshort, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_sshort, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_sshort, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_sshort, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_ushort, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_ushort, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_ushort, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_ushort, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_ushort, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_ushort, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_ushort, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_sint, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_sint, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_sint, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_sint, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_sint, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_sint, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_sint, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_uint, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_uint, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_uint, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_uint, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_uint, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_uint, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_uint, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_slong, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_slong, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_slong, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_slong, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_slong, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_slong, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_slong, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_ulong, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_ulong, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_ulong, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_ulong, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_ulong, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_ulong, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_ulong, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_float, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_float, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_float, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_float, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_float, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_float, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_float, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_double, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_double, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_double, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_double, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_double, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_double, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_double, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_ldouble, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_ldouble, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_ldouble, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_ldouble, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_ldouble, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_ldouble, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_add, ss_type_ldouble, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_bool, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_bool, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_bool, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_bool, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_bool, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_bool, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_bool, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_schar, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_schar, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_schar, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_schar, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_schar, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_schar, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_schar, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_uchar, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_uchar, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_uchar, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_uchar, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_uchar, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_uchar, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_uchar, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_pchar, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_pchar, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_pchar, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_pchar, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_pchar, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_pchar, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_pchar, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_sshort, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_sshort, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_sshort, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_sshort, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_sshort, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_sshort, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_sshort, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_ushort, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_ushort, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_ushort, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_ushort, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_ushort, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_ushort, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_ushort, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_sint, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_sint, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_sint, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_sint, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_sint, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_sint, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_sint, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_uint, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_uint, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_uint, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_uint, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_uint, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_uint, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_uint, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_slong, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_slong, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_slong, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_slong, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_slong, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_slong, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_slong, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_ulong, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_ulong, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_ulong, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_ulong, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_ulong, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_ulong, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_ulong, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_float, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_float, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_float, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_float, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_float, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_float, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_float, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_double, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_double, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_double, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_double, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_double, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_double, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_double, ss_type_ldouble);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_ldouble, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_ldouble, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_ldouble, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_ldouble, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_ldouble, ss_type_float);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_ldouble, ss_type_double);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_sub, ss_type_ldouble, ss_type_ldouble);\
\
		/* [13.6/19] to be handled separately (for every type)
		 *
		 * For every type (TODO - just now only for builtin)
		 *
		 *	T*VQ& operator=(T*VQ&, T*);
		 */\
\
		ADD_VQ_PTR_REF_VQ_PTR_REF_PTR(ss_operator_assign, ss_bool);\
		ADD_VQ_PTR_REF_VQ_PTR_REF_PTR(ss_operator_assign, ss_type_sshort);\
		ADD_VQ_PTR_REF_VQ_PTR_REF_PTR(ss_operator_assign, ss_type_ushort);\
		ADD_VQ_PTR_REF_VQ_PTR_REF_PTR(ss_operator_assign, ss_type_schar);\
		ADD_VQ_PTR_REF_VQ_PTR_REF_PTR(ss_operator_assign, ss_type_uchar);\
		ADD_VQ_PTR_REF_VQ_PTR_REF_PTR(ss_operator_assign, ss_type_pchar);\
		ADD_VQ_PTR_REF_VQ_PTR_REF_PTR(ss_operator_assign, ss_type_sint);\
		ADD_VQ_PTR_REF_VQ_PTR_REF_PTR(ss_operator_assign, ss_type_uint);\
		ADD_VQ_PTR_REF_VQ_PTR_REF_PTR(ss_operator_assign, ss_type_slong);\
		ADD_VQ_PTR_REF_VQ_PTR_REF_PTR(ss_operator_assign, ss_type_ulong);\
		ADD_VQ_PTR_REF_VQ_PTR_REF_PTR(ss_operator_assign, ss_type_float);\
		ADD_VQ_PTR_REF_VQ_PTR_REF_PTR(ss_operator_assign, ss_type_double);\
		ADD_VQ_PTR_REF_VQ_PTR_REF_PTR(ss_operator_assign, ss_type_ldouble);\
\
		/* [13.6/20]
		 *
		 * 	T*VQ& operator+=(T*VQ&, ptrdiff_t);
		 *	T*VQ& operator-=(T*VQ&, ptrdiff_t);
		 */\
\
		ADD_VQ_PTR_REF_VQ_PTR_REF_INT(ss_operator_assign_add, ss_bool);\
		ADD_VQ_PTR_REF_VQ_PTR_REF_INT(ss_operator_assign_add, ss_type_sshort);\
		ADD_VQ_PTR_REF_VQ_PTR_REF_INT(ss_operator_assign_add, ss_type_ushort);\
		ADD_VQ_PTR_REF_VQ_PTR_REF_INT(ss_operator_assign_add, ss_type_schar);\
		ADD_VQ_PTR_REF_VQ_PTR_REF_INT(ss_operator_assign_add, ss_type_uchar);\
		ADD_VQ_PTR_REF_VQ_PTR_REF_INT(ss_operator_assign_add, ss_type_pchar);\
		ADD_VQ_PTR_REF_VQ_PTR_REF_INT(ss_operator_assign_add, ss_type_sint);\
		ADD_VQ_PTR_REF_VQ_PTR_REF_INT(ss_operator_assign_add, ss_type_uint);\
		ADD_VQ_PTR_REF_VQ_PTR_REF_INT(ss_operator_assign_add, ss_type_slong);\
		ADD_VQ_PTR_REF_VQ_PTR_REF_INT(ss_operator_assign_add, ss_type_ulong);\
		ADD_VQ_PTR_REF_VQ_PTR_REF_INT(ss_operator_assign_add, ss_type_float);\
		ADD_VQ_PTR_REF_VQ_PTR_REF_INT(ss_operator_assign_add, ss_type_double);\
		ADD_VQ_PTR_REF_VQ_PTR_REF_INT(ss_operator_assign_add, ss_type_ldouble);\
\
		ADD_VQ_PTR_REF_VQ_PTR_REF_INT(ss_operator_assign_sub, ss_bool);\
		ADD_VQ_PTR_REF_VQ_PTR_REF_INT(ss_operator_assign_sub, ss_type_sshort);\
		ADD_VQ_PTR_REF_VQ_PTR_REF_INT(ss_operator_assign_sub, ss_type_ushort);\
		ADD_VQ_PTR_REF_VQ_PTR_REF_INT(ss_operator_assign_sub, ss_type_schar);\
		ADD_VQ_PTR_REF_VQ_PTR_REF_INT(ss_operator_assign_sub, ss_type_uchar);\
		ADD_VQ_PTR_REF_VQ_PTR_REF_INT(ss_operator_assign_sub, ss_type_pchar);\
		ADD_VQ_PTR_REF_VQ_PTR_REF_INT(ss_operator_assign_sub, ss_type_sint);\
		ADD_VQ_PTR_REF_VQ_PTR_REF_INT(ss_operator_assign_sub, ss_type_uint);\
		ADD_VQ_PTR_REF_VQ_PTR_REF_INT(ss_operator_assign_sub, ss_type_slong);\
		ADD_VQ_PTR_REF_VQ_PTR_REF_INT(ss_operator_assign_sub, ss_type_ulong);\
		ADD_VQ_PTR_REF_VQ_PTR_REF_INT(ss_operator_assign_sub, ss_type_float);\
		ADD_VQ_PTR_REF_VQ_PTR_REF_INT(ss_operator_assign_sub, ss_type_double);\
		ADD_VQ_PTR_REF_VQ_PTR_REF_INT(ss_operator_assign_sub, ss_type_ldouble);\
		/* [13.6/21] to be done later */\
		/* [13.6/22] \
		 * For every triple (L, VQ, R), where L is integral type, VQ is either volatile or empty \
		 * and R is promoted integral type, there exists candidate operator functions of the form \
		 \
		 	VQ L& operator%=(VQ L&, R);\
		 	VQ L& operator<<=(VQ L&, R);\
		 	VQ L& operator>>=(VQ L&, R);\
		 	VQ L& operator&=(VQ L&, R);\
		 	VQ L& operator^=(VQ L&, R);\
		 	VQ L& operator|=(VQ L&, R);\
		*/\
\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mod, ss_bool, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mod, ss_bool, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mod, ss_bool, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mod, ss_bool, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mod, ss_type_schar, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mod, ss_type_schar, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mod, ss_type_schar, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mod, ss_type_schar, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mod, ss_type_uchar, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mod, ss_type_uchar, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mod, ss_type_uchar, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mod, ss_type_uchar, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mod, ss_type_pchar, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mod, ss_type_pchar, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mod, ss_type_pchar, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mod, ss_type_pchar, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mod, ss_type_sshort, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mod, ss_type_sshort, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mod, ss_type_sshort, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mod, ss_type_sshort, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mod, ss_type_ushort, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mod, ss_type_ushort, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mod, ss_type_ushort, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mod, ss_type_ushort, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mod, ss_type_sint, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mod, ss_type_sint, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mod, ss_type_sint, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mod, ss_type_sint, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mod, ss_type_uint, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mod, ss_type_uint, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mod, ss_type_uint, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mod, ss_type_uint, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mod, ss_type_slong, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mod, ss_type_slong, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mod, ss_type_slong, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mod, ss_type_slong, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mod, ss_type_ulong, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mod, ss_type_ulong, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mod, ss_type_ulong, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_mod, ss_type_ulong, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shl, ss_bool, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shl, ss_bool, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shl, ss_bool, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shl, ss_bool, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shl, ss_type_schar, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shl, ss_type_schar, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shl, ss_type_schar, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shl, ss_type_schar, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shl, ss_type_uchar, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shl, ss_type_uchar, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shl, ss_type_uchar, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shl, ss_type_uchar, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shl, ss_type_pchar, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shl, ss_type_pchar, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shl, ss_type_pchar, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shl, ss_type_pchar, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shl, ss_type_sshort, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shl, ss_type_sshort, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shl, ss_type_sshort, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shl, ss_type_sshort, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shl, ss_type_ushort, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shl, ss_type_ushort, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shl, ss_type_ushort, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shl, ss_type_ushort, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shl, ss_type_sint, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shl, ss_type_sint, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shl, ss_type_sint, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shl, ss_type_sint, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shl, ss_type_uint, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shl, ss_type_uint, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shl, ss_type_uint, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shl, ss_type_uint, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shl, ss_type_slong, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shl, ss_type_slong, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shl, ss_type_slong, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shl, ss_type_slong, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shl, ss_type_ulong, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shl, ss_type_ulong, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shl, ss_type_ulong, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shl, ss_type_ulong, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shr, ss_bool, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shr, ss_bool, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shr, ss_bool, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shr, ss_bool, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shr, ss_type_schar, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shr, ss_type_schar, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shr, ss_type_schar, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shr, ss_type_schar, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shr, ss_type_uchar, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shr, ss_type_uchar, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shr, ss_type_uchar, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shr, ss_type_uchar, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shr, ss_type_pchar, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shr, ss_type_pchar, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shr, ss_type_pchar, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shr, ss_type_pchar, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shr, ss_type_sshort, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shr, ss_type_sshort, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shr, ss_type_sshort, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shr, ss_type_sshort, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shr, ss_type_ushort, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shr, ss_type_ushort, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shr, ss_type_ushort, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shr, ss_type_ushort, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shr, ss_type_sint, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shr, ss_type_sint, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shr, ss_type_sint, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shr, ss_type_sint, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shr, ss_type_uint, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shr, ss_type_uint, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shr, ss_type_uint, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shr, ss_type_uint, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shr, ss_type_slong, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shr, ss_type_slong, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shr, ss_type_slong, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shr, ss_type_slong, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shr, ss_type_ulong, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shr, ss_type_ulong, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shr, ss_type_ulong, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_shr, ss_type_ulong, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_band, ss_bool, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_band, ss_bool, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_band, ss_bool, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_band, ss_bool, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_band, ss_type_schar, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_band, ss_type_schar, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_band, ss_type_schar, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_band, ss_type_schar, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_band, ss_type_uchar, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_band, ss_type_uchar, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_band, ss_type_uchar, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_band, ss_type_uchar, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_band, ss_type_pchar, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_band, ss_type_pchar, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_band, ss_type_pchar, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_band, ss_type_pchar, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_band, ss_type_sshort, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_band, ss_type_sshort, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_band, ss_type_sshort, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_band, ss_type_sshort, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_band, ss_type_ushort, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_band, ss_type_ushort, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_band, ss_type_ushort, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_band, ss_type_ushort, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_band, ss_type_sint, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_band, ss_type_sint, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_band, ss_type_sint, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_band, ss_type_sint, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_band, ss_type_uint, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_band, ss_type_uint, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_band, ss_type_uint, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_band, ss_type_uint, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_band, ss_type_slong, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_band, ss_type_slong, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_band, ss_type_slong, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_band, ss_type_slong, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_band, ss_type_ulong, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_band, ss_type_ulong, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_band, ss_type_ulong, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_band, ss_type_ulong, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bxor, ss_bool, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bxor, ss_bool, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bxor, ss_bool, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bxor, ss_bool, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bxor, ss_type_schar, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bxor, ss_type_schar, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bxor, ss_type_schar, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bxor, ss_type_schar, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bxor, ss_type_uchar, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bxor, ss_type_uchar, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bxor, ss_type_uchar, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bxor, ss_type_uchar, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bxor, ss_type_pchar, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bxor, ss_type_pchar, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bxor, ss_type_pchar, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bxor, ss_type_pchar, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bxor, ss_type_sshort, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bxor, ss_type_sshort, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bxor, ss_type_sshort, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bxor, ss_type_sshort, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bxor, ss_type_ushort, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bxor, ss_type_ushort, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bxor, ss_type_ushort, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bxor, ss_type_ushort, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bxor, ss_type_sint, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bxor, ss_type_sint, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bxor, ss_type_sint, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bxor, ss_type_sint, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bxor, ss_type_uint, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bxor, ss_type_uint, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bxor, ss_type_uint, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bxor, ss_type_uint, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bxor, ss_type_slong, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bxor, ss_type_slong, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bxor, ss_type_slong, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bxor, ss_type_slong, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bxor, ss_type_ulong, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bxor, ss_type_ulong, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bxor, ss_type_ulong, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bxor, ss_type_ulong, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bor, ss_bool, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bor, ss_bool, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bor, ss_bool, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bor, ss_bool, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bor, ss_type_schar, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bor, ss_type_schar, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bor, ss_type_schar, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bor, ss_type_schar, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bor, ss_type_uchar, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bor, ss_type_uchar, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bor, ss_type_uchar, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bor, ss_type_uchar, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bor, ss_type_pchar, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bor, ss_type_pchar, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bor, ss_type_pchar, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bor, ss_type_pchar, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bor, ss_type_sshort, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bor, ss_type_sshort, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bor, ss_type_sshort, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bor, ss_type_sshort, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bor, ss_type_ushort, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bor, ss_type_ushort, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bor, ss_type_ushort, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bor, ss_type_ushort, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bor, ss_type_sint, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bor, ss_type_sint, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bor, ss_type_sint, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bor, ss_type_sint, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bor, ss_type_uint, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bor, ss_type_uint, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bor, ss_type_uint, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bor, ss_type_uint, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bor, ss_type_slong, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bor, ss_type_slong, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bor, ss_type_slong, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bor, ss_type_slong, ss_type_ulong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bor, ss_type_ulong, ss_type_sint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bor, ss_type_ulong, ss_type_uint);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bor, ss_type_ulong, ss_type_slong);\
		ADD_VQ_REF_VQ_REF_NONREF(ss_operator_assign_bor, ss_type_ulong, ss_type_ulong);\
		/* [13.6/23] \
		 * There are also \
		 	bool operator!(bool);\
			bool operator&&(bool, bool);\
			bool operator||(bool, bool);\
		*/\
		ADD_NONREF_NONREF(ss_operator_lnot, ss_bool, ss_bool);\
		ADD_NONREF_NONREF_NONREF(ss_operator_land, ss_bool, ss_bool, ss_bool);\
		ADD_NONREF_NONREF_NONREF(ss_operator_lor, ss_bool, ss_bool, ss_bool);\
\
}
			
end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */

