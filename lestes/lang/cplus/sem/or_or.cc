/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
 * This file contains implementation of visitor on ss_decl_name. This is used
 * to determine, if the operator we are currenlty processing is builtin
 * operator, conversion function, or "ordinary" function.
 *
 * Also, this file contains overloaded operator() of or_or_functional, which
 * is used to construct proper ss_expression.
 *
 * \author jikos
 */

#include <lestes/lang/cplus/sem/or_visitor.v.g.hh>
#include <lestes/lang/cplus/sem/ss_decl_name.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>
#include <lestes/lang/cplus/sem/ss_decl_name_visitor.v.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration_visitor.v.g.hh>
#include <lestes/lang/cplus/sem/or_or.g.hh>
#include <lestes/lang/cplus/sem/or_or.m.hh>
#include <lestes/lang/cplus/sem/sa_deconstruct_spse.m.hh>
#include <lestes/lang/cplus/sem/sa_deconstruct_spse.g.hh>
#include <lestes/lang/cplus/sem/sa_statements.g.hh>
#include <lestes/md/types/ss_type_size_evaluator.g.hh>
#include <lestes/std/source_location.hh>
#include <lestes/lang/cplus/sem/or_ics.g.hh>
#include <lestes/lang/cplus/sem/ss_type.g.hh>
#include <lestes/lang/cplus/sem/or_ics_actual_visitors.g.hh>
#include <lestes/std/source_location.hh>
#include <lestes/lang/cplus/sem/ss_literal_info.g.hh>

package(lestes);
package(lang);
package(cplus);
package(sem);

using namespace ::lestes::md::types;

#define IS_VOLATILE(x) (((x) == OR_CV_VOLATILE) || (x) == OR_CV_CONST_VOLATILE)
class ss_function;
/*!
 * This template covers operators which have form of binary op without sideeffect for arithmetics
 * This function takes the arguments to the operator from it's environment (the containing class'
 * fields) and constructs the ss_expression, the type of which is given by the template
 * argument.
 * SS - ss expression corresponding to the particular operator
 */
template <typename SS> void or_builtin_operator_expression_creator::construct_bin_op_nosideeff_arith()
{
	ptr< or_or_functional > l, r;
	ptr< ss_expression > l_expr, r_expr;
	ptr< ::lestes::std::source_location > loc = location_get();
	::lestes::std::list< srp< or_or_functional > >::iterator it = arguments_get()->begin();
	ptr< or_ics_functional > l_func, r_func; 
	::lestes::std::list< srp< ss_type > >::iterator it_p = declaration_get()->type_get().template dncast <ss_function>()->params_get()->begin();
	ptr< ss_expression > sizeof_mul_expr;
	bool was_ptr = false;
	bool multiplied_left = false;
	bool multiplied_right = false;
	
	l = *it;
	it++;
	r = (*it);

	/* find implicit conversion between argument and function parameter types */	

	/* If (one of) the arguments are not pointers, we must convert them both to the type of return value, as this is what ss2pi expects.
	 * This is exactly what the following voodoo does.
	 */
	ptr< or_ics_visitor_cv > v = or_ics_visitor_cv::create();
	
	/* If one of the arguments is pointer, then we are performing pointer arithmetics */
	if( ((l->type_get()->accept_or_ics_base_cv(v) == OR_CV_PTR || 
					(l->type_get()->accept_or_ics_base_cv(v) == OR_CV_PSEUDOREFERENCE && 
					 l->type_get().template dncast<ss_pseudoreference>()->what_get()->accept_or_ics_base_cv(v) == OR_CV_PTR))) ||
	     ((r->type_get()->accept_or_ics_base_cv(v) == OR_CV_PTR || 
					(r->type_get()->accept_or_ics_base_cv(v) == OR_CV_PSEUDOREFERENCE && 
					 r->type_get().template dncast<ss_pseudoreference>()->what_get()->accept_or_ics_base_cv(v) == OR_CV_PTR)))) {

		was_ptr = true;	
		it = arguments_get()->begin();
		l_func = or_find_ics(l, *it_p);
		if (!l_func) {
			report << incompatible_types_conversion << loc;
			exit(1);
		}	

		it_p++;
		r_func = or_find_ics(r, *(it_p));
		if (!r_func) {
			report << incompatible_types_conversion << loc;
			exit(1);
		}	

	} else { 
		/* we have to convert both parameters to the type of return function */
		ptr< or_ics_visitor_cv > v0 = or_ics_visitor_cv::create();
		if(declaration_get()->type_get().template dncast <ss_function>()->returns_get()->accept_or_ics_base_cv(v0) == OR_CV_PSEUDOREFERENCE) {
			l_func = or_find_ics(l, declaration_get()->type_get().template dncast <ss_function>()->returns_get().template dncast<ss_pseudoreference>()->what_get());
			if (!l_func) {
				report << incompatible_types_conversion << loc;
				exit(1);
			}	

		}
		else if (declaration_get()->type_get().template dncast <ss_function>()->returns_get()->accept_or_ics_base_cv(v0) == OR_CV_REFERENCE) {
			l_func = or_find_ics(l, declaration_get()->type_get().template dncast <ss_function>()->returns_get().template dncast<ss_reference>()->what_get());
			if (!l_func) {
				report << incompatible_types_conversion << loc;
				exit(1);
			}	

		}
		
		else { 
			l_func = or_find_ics(l, declaration_get()->type_get().template dncast <ss_function>()->returns_get());
			if (!l_func) {
				report << incompatible_types_conversion << loc;
				exit(1);
			}	

		}

		ptr< or_ics_visitor_cv > v1 = or_ics_visitor_cv::create();
		if(declaration_get()->type_get().template dncast <ss_function>()->returns_get()->accept_or_ics_base_cv(v) == OR_CV_PSEUDOREFERENCE) {
			r_func = or_find_ics(r, declaration_get()->type_get().template dncast <ss_function>()->returns_get().template dncast<ss_pseudoreference>()->what_get());
			if (!r_func) {
				report << incompatible_types_conversion << loc;
				exit(1);
			}	

		}
		else if(declaration_get()->type_get().template dncast <ss_function>()->returns_get()->accept_or_ics_base_cv(v) == OR_CV_REFERENCE) {
			r_func = or_find_ics(r, declaration_get()->type_get().template dncast <ss_function>()->returns_get().template dncast<ss_reference>()->what_get());
			if (!r_func) {
				report << incompatible_types_conversion << loc;
				exit(1);
			}	

		} else {
			r_func = or_find_ics(r, declaration_get()->type_get().template dncast <ss_function>()->returns_get());
			if (!r_func) {
				report << incompatible_types_conversion << loc;
				exit(1);
			}	

		}

	}
	/* this constructs the actual ss_expression for parameters, taking in account conversion sequences needed */
	l_expr = (*l_func)((*l)(psp_get(), nsp_get(), loc, sp_list_get()), psp_get(), nsp_get());
	r_expr = (*r_func)((*r)(psp_get(), nsp_get(), loc, sp_list_get()), psp_get(), nsp_get());

	/* We have to check now if we are performing pointer arithmetics. If so, we need to create another multiplying expression, which
	 * will multiply the 'integral side' of the expression by the sizeof() of pointer's underlying type.
	 *
	 * Even the left one can be integral, as constant -+ ptr is allowed
	 */
	if (was_ptr && (l->type_get()->accept_or_ics_base_cv(v) == OR_CV_SINT)) {
		multiplied_left = true;
	
		if (r->type_get()->accept_or_ics_base_cv(v) == OR_CV_PSEUDOREFERENCE)
			sizeof_mul_expr =  ss_conversion::create(	loc, 
								r->type_get().template dncast<ss_pseudoreference>()->what_get(),
								psp_get(),
								nsp_get(),
								ss_mul::create(	loc, 
									ss_type_sint::instance(), 
									psp_get(), 
									nsp_get(), 
									l_expr, 
									ss_literal::create(
											location, 
											ss_type_sint::instance(), 
											psp_get(), nsp_get(), 
											ss_integral_literal_info::create_from_number(
													ss_type_uint::instance(), 
											/* FIXME this has to neccessairly to be pseudoreference! */
													ss_type_size_evaluator::instance()->size_get(r->type_get().template dncast<ss_pseudoreference>()->what_get().template dncast<ss_pointer>()->what_get())/8))),
								ss_type_sint::instance()
						);
		else
			sizeof_mul_expr =  ss_conversion::create(	loc, 
								r->type_get().template dncast<ss_reference>()->what_get(),
								psp_get(),
								nsp_get(),
								ss_mul::create(	loc, 
									ss_type_sint::instance(), 
									psp_get(), 
									nsp_get(), 
									l_expr, 
									ss_literal::create(
											location, 
											ss_type_sint::instance(), 
											psp_get(), nsp_get(), 
											ss_integral_literal_info::create_from_number(
													ss_type_uint::instance(), 
											/* FIXME this has to neccessairly to be pseudoreference! */
													ss_type_size_evaluator::instance()->size_get(r->type_get().template dncast<ss_pseudoreference>()->what_get().template dncast<ss_pointer>()->what_get())/8))),
								ss_type_sint::instance()
						);
			

	} else if (was_ptr && (r->type_get()->accept_or_ics_base_cv(v) == OR_CV_SINT)) {
		multiplied_right = true;
		if (l->type_get()->accept_or_ics_base_cv(v) == OR_CV_PSEUDOREFERENCE) 
			sizeof_mul_expr = ss_conversion::create(	loc,
								l->type_get().template dncast<ss_pseudoreference>()->what_get(),
								psp_get(),
								nsp_get(),
								ss_mul::create(	loc, 
										ss_type_sint::instance(), 
										psp_get(), 
										nsp_get(), 
										r_expr, 
										ss_literal::create(
											location, 
											ss_type_sint::instance(), 
											psp_get(), nsp_get(), 
											ss_integral_literal_info::create_from_number(
													ss_type_uint::instance(), 
												/* FIXME this has to neccessairly to be pseudoreference! */
													ss_type_size_evaluator::instance()->size_get(l->type_get().template dncast<ss_pseudoreference>()->what_get().template dncast<ss_pointer>()->what_get())/8))),
								ss_type_sint::instance()
						);
		else
			sizeof_mul_expr = ss_conversion::create(	loc,
								l->type_get().template dncast<ss_reference>()->what_get(),
								psp_get(),
								nsp_get(),
								ss_mul::create(	loc, 
										ss_type_sint::instance(), 
										psp_get(), 
										nsp_get(), 
										r_expr, 
										ss_literal::create(
											location, 
											ss_type_sint::instance(), 
											psp_get(), nsp_get(), 
											ss_integral_literal_info::create_from_number(
													ss_type_uint::instance(), 
												/* FIXME this has to neccessairly to be pseudoreference! */
													ss_type_size_evaluator::instance()->size_get(l->type_get().template dncast<ss_pseudoreference>()->what_get().template dncast<ss_pointer>()->what_get())/8))),
								ss_type_sint::instance()
						);
			
	}
	/* If we performed pointer arithmetics, connect the ss_mul operation for sizeof() correctly */
	if (!multiplied_left && !multiplied_right)
		if (l_expr->type_get()->accept_or_ics_base_cv(v) == OR_CV_PTR && r_expr->type_get()->accept_or_ics_base_cv(v) == OR_CV_PTR) 
			/* the ptr - ptr case */
			result_set(ss_conversion::create(loc,
							ss_type_sint::instance(),
							psp_get(),
							nsp_get(),
							SS::create(	loc, 
									l_expr->type_get(), 
									psp_get(), 
									nsp_get(), 
									l_expr, 
									r_expr),
							l_expr->type_get()));
		else
			result_set(SS::create(	loc, 
					declaration_get()->type_get().template dncast<ss_function>()->returns_get(), 
					psp_get(), 
					nsp_get(), 
					l_expr, 
					r_expr));
	else if (multiplied_left)
		result_set(SS::create(	loc, 
				declaration_get()->type_get().template dncast<ss_function>()->returns_get(), 
				psp_get(), 
				nsp_get(), 
				sizeof_mul_expr, 
				r_expr));
	else
		result_set(SS::create(	loc, 
				declaration_get()->type_get().template dncast<ss_function>()->returns_get(), 
				psp_get(), 
				nsp_get(), 
				l_expr, 
				sizeof_mul_expr));

}

/*!
 * This template covers operators which have form of binary op without sideeffect which are non-arithmetic
 * This function takes the arguments to the operator from it's environment (the containing class'
 * fields) and constructs the ss_expression, the type of which is given by the template
 * argument.
 * SS - ss expression corresponding to the particular operator
 */
template <typename SS> void or_builtin_operator_expression_creator::construct_bin_op_nosideeff()
{
	ptr< or_or_functional > l, r;
	ptr< ss_expression > l_expr, r_expr;
	ptr< ::lestes::std::source_location > loc = location_get();
	::lestes::std::list< srp< or_or_functional > >::iterator it = arguments_get()->begin();
	ptr< or_ics_functional > l_func, r_func; 
	::lestes::std::list< srp< ss_type > >::iterator it_p = declaration_get()->type_get().template dncast <ss_function>()->params_get()->begin();

	l = *it;
	it++;
	r = (*it);

	/* find implicit conversion between argument and function parameter types */	

	/* If (one of) the arguments are not pointers, we must convert them both to the type of return value, as this is what ss2pi expects.
	 * This is exactly what the following voodoo does.
	 */
	ptr< or_ics_visitor_cv > v = or_ics_visitor_cv::create();
	
	/* If one of the arguments is pointer, then we are performing pointer arithmetics */
	if( ((l->type_get()->accept_or_ics_base_cv(v) == OR_CV_PTR || 
					(l->type_get()->accept_or_ics_base_cv(v) == OR_CV_PSEUDOREFERENCE && 
					 l->type_get().template dncast<ss_pseudoreference>()->what_get()->accept_or_ics_base_cv(v) == OR_CV_PTR))) ||
	     ((r->type_get()->accept_or_ics_base_cv(v) == OR_CV_PTR || 
					(r->type_get()->accept_or_ics_base_cv(v) == OR_CV_PSEUDOREFERENCE && 
					 r->type_get().template dncast<ss_pseudoreference>()->what_get()->accept_or_ics_base_cv(v) == OR_CV_PTR))))	      	{
		it = arguments_get()->begin();
		l_func = or_find_ics(l, *it_p);
		if (!l_func) {
			report << incompatible_types_conversion << loc;
			exit(1);
		}	

		it_p++;
		r_func = or_find_ics(r, *(it_p));
		if (!r_func) {
			report << incompatible_types_conversion << loc;
			exit(1);
		}	

	} else { 
		ptr< or_ics_visitor_cv > v0 = or_ics_visitor_cv::create();
		if(r->type_get()->accept_or_ics_base_cv(v0) == OR_CV_PSEUDOREFERENCE) {
			l_func = or_find_ics(l, r->type_get().template dncast<ss_pseudoreference>()->what_get());
			if (!l_func) {
				report << incompatible_types_conversion << loc;
				exit(1);
			}	

			r_func = or_find_ics(r, r->type_get().template dncast<ss_pseudoreference>()->what_get());
			if (!r_func) {
				report << incompatible_types_conversion << loc;
				exit(1);
			}	


		}
		else {
			l_func = or_find_ics(l, r->type_get());
			if (!l_func) {
				report << incompatible_types_conversion << loc;
				exit(1);
			}	

			r_func = or_find_ics(r, r->type_get());
			if (!r_func) {
				report << incompatible_types_conversion << loc;
				exit(1);
			}	

		}

	}
	/* this constructs the actual ss_expression for parameters, taking in account conversion sequences needed */
	l_expr = (*l_func)((*l)(psp_get(), nsp_get(), loc, sp_list_get()), psp_get(), nsp_get());
	r_expr = (*r_func)((*r)(psp_get(), nsp_get(), loc, sp_list_get()), psp_get(), nsp_get());
	
	result_set(SS::create(	loc, 
				declaration_get()->type_get().template dncast<ss_function>()->returns_get(), 
				psp_get(), 
				nsp_get(), 
				l_expr, 
				r_expr));

}

/*!
 * This template covers operators which have form of binary op without sideeffect, but there is 
 * need to create new sequence point (so this covers '||' and '&&' operators. The operator 
 * ',' is handled elsewhere, as there is no need to create new ss_expression).
 *
 * This function takes the arguments to the operator from it's environment (the containing class'
 * fields) and constructs the ss_expression, the type of which is given by the template
 * argument.
 * SS - ss expression corresponding to the particular operator
 */
template <typename SS> void or_builtin_operator_expression_creator::construct_bin_op_nosideeff_newsp()
{
	ptr< or_or_functional > l, r;
	ptr< ss_expression > l_expr, r_expr;
	ptr< ::lestes::std::source_location > loc = location_get();
	::lestes::std::list< srp< or_or_functional > >::iterator it = arguments_get()->begin();
	ptr< or_ics_functional > l_func, r_func; 
	::lestes::std::list< srp< ss_type > >::iterator it_p = declaration_get()->type_get().template dncast <ss_function>()->params_get()->begin();

	l = *it;
	it++;
	r = *(it);

	/* find implicit conversion between argument and function parameter types */	
	/* If (one of) the arguments are not pointers, we must convert them both to the type of
	 * return value, as this is what ss2pi expects
	 */
	ptr< or_ics_visitor_cv > v = or_ics_visitor_cv::create();
	if(l->type_get()->accept_or_ics_base_cv(v) != OR_CV_PTR) {

		ptr< or_ics_visitor_cv > v0 = or_ics_visitor_cv::create();
		if(declaration_get()->type_get().template dncast <ss_function>()->returns_get()->accept_or_ics_base_cv(v0) == OR_CV_PSEUDOREFERENCE){
			l_func = or_find_ics(l, declaration_get()->type_get().template dncast <ss_function>()->returns_get().template dncast<ss_pseudoreference>()->what_get());
			if (!l_func) {
				report << incompatible_types_conversion << loc;
				exit(1);
			}	

		}
		else {
			l_func = or_find_ics(l, declaration_get()->type_get().template dncast <ss_function>()->returns_get());
			if (!l_func) {
				report << incompatible_types_conversion << loc;
				exit(1);
			}	

		}

		ptr< or_ics_visitor_cv > v1 = or_ics_visitor_cv::create();
		if(declaration_get()->type_get().template dncast <ss_function>()->returns_get()->accept_or_ics_base_cv(v) == OR_CV_PSEUDOREFERENCE) {
			r_func = or_find_ics(r, declaration_get()->type_get().template dncast <ss_function>()->returns_get().template dncast<ss_pseudoreference>()->what_get());
			if (!r_func) {
				report << incompatible_types_conversion << loc;
				exit(1);
			}	
		}
		else {
			r_func = or_find_ics(r, declaration_get()->type_get().template dncast <ss_function>()->returns_get());
			if (!r_func) {
				report << incompatible_types_conversion << loc;
				exit(1);
			}	
		}
	} else {
		it = arguments_get()->begin();
		l_func = or_find_ics(l, *it_p);
		if (!l_func) {
			report << incompatible_types_conversion << loc;
			exit(1);
		}	

		it_p++;
		r_func = or_find_ics(r, *(it_p));
		if (!r_func) {
			report << incompatible_types_conversion << loc;
			exit(1);
		}	

	}

	/* this constructs the actual ss_expression for parameters, taking in account conversion sequences needed */
	l_expr = (*l_func)((*l)(psp_get(), nsp_get(), loc, sp_list_get()), psp_get(), nsp_get());
	r_expr = (*r_func)((*r)(psp_get(), nsp_get(), loc, sp_list_get()), psp_get(), nsp_get());


	result_set(SS::create(	loc, 
				declaration_get()->type_get().template dncast<ss_function>()->returns_get(), 
				psp_get(), 
				nsp_get(), 
				l_expr, 
				r_expr));

	/* create new sequence point and add to lists properly */
        lint level = MAX(psp_get()->level_get(), nsp_get()->level_get())+1;
        ptr< ss_sp > new_sp = ss_sp::create(loc, psp_get(), nsp_get(), level);
        sp_list_get()->push_back(new_sp);
}

/*!
 * This template covers operators which have form of unary op without sideeffect
 * This function takes the arguments to the operator from it's environment (the containing class'
 * fields) and constructs the ss_expression, the type of which is given by the template
 * argument.
 * SS - ss expression corresponding to the particular operator
 */
template <typename SS> void or_builtin_operator_expression_creator::construct_un_op_nosideeff()
{
	ptr< or_or_functional > l;
	ptr< ss_expression > l_expr;
	ptr< ::lestes::std::source_location > loc = location_get();
	::lestes::std::list< srp< or_or_functional > >::iterator it = arguments_get()->begin();
	ptr< or_ics_functional > l_func; 
	::lestes::std::list< srp< ss_type > >::iterator it_p = declaration_get()->type_get().template dncast <ss_function>()->params_get()->begin();

	l = *it;

	/* find implicit conversion between argument and function parameter types */	
	it = arguments_get()->begin();
	l_func = or_find_ics(l, *it_p);
	if (!l_func) {
		report << incompatible_types_conversion << loc;
		exit(1);
	}	

	
	/* this constructs the actual ss_expression for parameters, taking in account conversion sequences needed */
	l_expr = (*l_func)((*l)(psp_get(), nsp_get(), loc, sp_list_get()), psp_get(), nsp_get());

	result_set(SS::create(	loc, 
				declaration_get()->type_get().template dncast<ss_function>()->returns_get(), 
				psp_get(), 
				nsp_get(), 
				l_expr));
}

/*!
 * This template covers operators which have form of ++ or -- (prefix or postfix)
 *
 * This function takes the arguments to the operator from it's environment (the containing class'
 * fields) and constructs the ss_expression, the type of which is given by the template
 * argument.
 * Also, it inserts the newly created sideeffect to the sideeffect list of the guarding sequence
 * points.
 * SS - ss expression corresponding to the particular operator
 */
template <typename SS> void or_builtin_operator_expression_creator::construct_op_crement()
{
	ptr< ::lestes::std::source_location > loc = location_get();
	ptr< or_or_functional > l, rf;
	ptr < ss_expression > e, l_expr, get, r;
	ptr< ss_assign > p;
	::lestes::std::list< srp< or_or_functional > >::iterator it = arguments_get()->begin();
	::lestes::std::list< srp< or_or_functional > >::size_type num_args = arguments_get()->size();
	ptr< or_ics_visitor_cv > v = or_ics_visitor_cv::create();
	ptr< or_ics_functional > l_func, r_func; 
	::lestes::std::list< srp< ss_type > >::iterator it_p = declaration_get()->type_get().template dncast <ss_function>()->params_get()->begin();
	ptr< ss_expression > literal_one;

	
	l = *it;

	ptr< or_ics_visitor_cv > v1 = or_ics_visitor_cv::create();
	if(l->type_get()->accept_or_ics_base_cv(v1) != OR_CV_PSEUDOREFERENCE && l->type_get()->accept_or_ics_base_cv(v1) != OR_CV_REFERENCE) {
		report << non_lval_crement << loc;
		exit(1);
	} 

	literal_one = ss_literal::create(loc, ss_type_sint::instance(), psp_get(), nsp_get(), ss_integral_literal_info::create_from_number(
				                                            ss_type_sint::instance(), 1));
	
	/* find implicit conversion between argument and function parameter types */
	it = arguments_get()->begin();

	//l_func = or_find_ics(l, *it_p);
	/* The l parameter has to stay as it is for ss2pi to be happy - both types the same */
	l_func = or_ics_functional_for_std_conversion::create(RANK_EXACT, l->type_get());

	/* this constructs the actual ss_expression for parameters, taking in account conversion sequences needed */
	l_expr = (*l_func)((*l)(psp_get(), nsp_get(), loc, sp_list_get()), psp_get(), nsp_get());

	if (num_args == 1) {
		/* argument r, representing literal '1' must be created for the postfix case (for the prefix case, the argument has already 
		 * been created in sa_deconstruct_spse.cc for the purpose of overload resolution
		 */
		ptr< or_ics_visitor_cv > v = or_ics_visitor_cv::create();

		/* If one of the arguments is pointer, then we are performing pointer arithmetics */
		if( ((l->type_get()->accept_or_ics_base_cv(v) == OR_CV_PTR ||
						(l->type_get()->accept_or_ics_base_cv(v) == OR_CV_PSEUDOREFERENCE &&
						 l->type_get().template dncast<ss_pseudoreference>()->what_get()->accept_or_ics_base_cv(v) == OR_CV_PTR)))) {
			ptr< ss_expression > r_;
			/* This ugly code performs literal * literal for sizeof() and converts the result to pointer. backend wants it */
			r_ = ss_literal::create(loc, ss_type_sint::instance(), psp_get(), nsp_get(), 
					ss_integral_literal_info::create_from_number(ss_type_sint::instance(), 
						ss_type_size_evaluator::instance()->size_get(
							l->type_get().template dncast<ss_pseudoreference>()->what_get().template dncast<ss_pointer>()->what_get())/8));
			r = ss_conversion::create(	loc, 
							l->type_get().template dncast<ss_pseudoreference>()->what_get(),
							psp_get(),
							nsp_get(),
							ss_mul::create(	loc, 
									ss_type_sint::instance(), 
									psp_get(), 
									nsp_get(), 
									literal_one,
									r_),
							ss_type_sint::instance()
						);
			
		} else if( ((l->type_get()->accept_or_ics_base_cv(v) == OR_CV_PTR ||
						(l->type_get()->accept_or_ics_base_cv(v) == OR_CV_REFERENCE &&
						 l->type_get().template dncast<ss_reference>()->what_get()->accept_or_ics_base_cv(v) == OR_CV_PTR)))) {
			ptr< ss_expression > r_;
			/* This ugly code performs literal * literal for sizeof() and converts the result to pointer. backend wants it */
	
			r_ = ss_literal::create(loc, ss_type_sint::instance(), psp_get(), nsp_get(), 
					ss_integral_literal_info::create_from_number(ss_type_sint::instance(), 
						ss_type_size_evaluator::instance()->size_get(
							l->type_get().template dncast<ss_reference>()->what_get().template dncast<ss_pointer>()->what_get())/8));
			r = ss_conversion::create(	loc, 
							l->type_get().template dncast<ss_reference>()->what_get(),
							psp_get(),
							nsp_get(),
							ss_mul::create(	loc, 
									ss_type_sint::instance(), 
									psp_get(), 
									nsp_get(), 
									literal_one,
									r_),
							ss_type_sint::instance()
						);
			
	
		} else {
			if(l->type_get()->accept_or_ics_base_cv(v) == OR_CV_PSEUDOREFERENCE)
				r = ss_literal::create(loc, ss_type_sint::instance(), psp_get(), nsp_get(), ss_integral_literal_info::create_from_number(
						l->type_get().template dncast<ss_pseudoreference>()->what_get(), 1));
			else
				r = ss_literal::create(loc, ss_type_sint::instance(), psp_get(), nsp_get(), ss_integral_literal_info::create_from_number(
						l->type_get().template dncast<ss_reference>()->what_get(), 1));
		}
	} else  /* num_args == 2 */
		if( ((l->type_get()->accept_or_ics_base_cv(v) == OR_CV_PTR ||
						(l->type_get()->accept_or_ics_base_cv(v) == OR_CV_PSEUDOREFERENCE &&
						 l->type_get().template dncast<ss_pseudoreference>()->what_get()->accept_or_ics_base_cv(v) == OR_CV_PTR)))) {
			ptr< ss_expression > r_;
			/* This ugly code performs literal * literal for sizeof() and converts the result to pointer. backend wants it */
			r_ = ss_literal::create(loc, ss_type_sint::instance(), psp_get(), nsp_get(), 
					ss_integral_literal_info::create_from_number(ss_type_sint::instance(), 
						ss_type_size_evaluator::instance()->size_get(
							l->type_get().template dncast<ss_pseudoreference>()->what_get().template dncast<ss_pointer>()->what_get())/8));
			r = ss_conversion::create(	loc, 
							l->type_get().template dncast<ss_pseudoreference>()->what_get(),
							psp_get(),
							nsp_get(),
							ss_mul::create(	loc, 
									ss_type_sint::instance(), 
									psp_get(), 
									nsp_get(), 
									literal_one,
									r_),
							ss_type_sint::instance()
						);
		} else	if( ((l->type_get()->accept_or_ics_base_cv(v) == OR_CV_PTR ||
						(l->type_get()->accept_or_ics_base_cv(v) == OR_CV_REFERENCE &&
						 l->type_get().template dncast<ss_reference>()->what_get()->accept_or_ics_base_cv(v) == OR_CV_PTR)))) {
			ptr< ss_expression > r_;
			/* This ugly code performs literal * literal for sizeof() and converts the result to pointer. backend wants it */
			r_ = ss_literal::create(loc, ss_type_sint::instance(), psp_get(), nsp_get(), 
					ss_integral_literal_info::create_from_number(ss_type_sint::instance(), 
						ss_type_size_evaluator::instance()->size_get(
							l->type_get().template dncast<ss_reference>()->what_get().template dncast<ss_pointer>()->what_get())/8));
			r = ss_conversion::create(	loc, 
							l->type_get().template dncast<ss_reference>()->what_get(),
							psp_get(),
							nsp_get(),
							ss_mul::create(	loc, 
									ss_type_sint::instance(), 
									psp_get(), 
									nsp_get(), 
									literal_one,
									r_),
							ss_type_sint::instance()
						);
 
		} else {
			it++;
			rf = (*it);
			it_p++;
			/* both types in assignment have to be the same */
			/* If one of the arguments is pointer, then we are performing pointer arithmetics */
			if(l->type_get()->accept_or_ics_base_cv(v) == OR_CV_PSEUDOREFERENCE &&
				l->type_get().template dncast<ss_pseudoreference>()->what_get()->accept_or_ics_base_cv(v1) != OR_CV_PTR) {
					r_func = or_find_ics(rf, l->type_get().template dncast<ss_pseudoreference>()->what_get());
					if (!r_func) {
						report << incompatible_types_conversion << loc;
						exit(1);
					}	
				
			} else if(l->type_get()->accept_or_ics_base_cv(v) == OR_CV_REFERENCE && 
				l->type_get().template dncast<ss_reference>()->what_get()->accept_or_ics_base_cv(v1) != OR_CV_PTR) {
					r_func = or_find_ics(rf, l->type_get().template dncast<ss_reference>()->what_get());
					if (!r_func) {
						report << incompatible_types_conversion << loc;
						exit(1);
					}	
			} else {
				if (l->type_get()->accept_or_ics_base_cv(v) == OR_CV_PSEUDOREFERENCE)
					r_func = or_ics_functional_for_std_conversion::create(RANK_EXACT, l->type_get().template dncast<ss_pseudoreference>()->what_get().template dncast<ss_pointer>()->what_get());
				else
					r_func = or_ics_functional_for_std_conversion::create(RANK_EXACT, l->type_get().template dncast<ss_reference>()->what_get().template dncast<ss_pointer>()->what_get());
		}
		/* this constructs the actual ss_expression for parameters, taking in account conversion sequences needed */
		r = (*r_func)((*rf)(psp_get(), nsp_get(), loc, sp_list_get()), psp_get(), nsp_get());
	}
	
	/* volatile type should be handled in a different way */
	if(IS_VOLATILE(l->type_get()->accept_or_ics_base_cv(v))) {
		get = ss_vol_get::create(loc, l_expr->type_get(), psp_get(), nsp_get(), l_expr);
		ptr< ss_se > se = ss_se::create(loc, l_expr, psp_get(), nsp_get());

		/* add newly created sideeffect into the lists */
		psp_get()->nse_get()->push_back(se);
		nsp_get()->pse_get()->push_back(se);

	} else {
	        ptr< or_ics_visitor_cv > v = or_ics_visitor_cv::create();
	        if(l->type_get()->accept_or_ics_base_cv(v) != OR_CV_PSEUDOREFERENCE && l->type_get()->accept_or_ics_base_cv(v) != OR_CV_REFERENCE) {
			report << non_lval_crement << loc;
			exit(1);
		}
		if (l->type_get()->accept_or_ics_base_cv(v) == OR_CV_PSEUDOREFERENCE)
			get = ss_get::create(loc, l->type_get().template dncast<ss_pseudoreference>()->what_get(), psp_get(), nsp_get(), l_expr);
		else
			get = ss_get::create(loc, l->type_get().template dncast<ss_reference>()->what_get(), psp_get(), nsp_get(), l_expr);
	}

	/* this creates the actual operation + or - */
	if (num_args == 1) {
		e = SS::create(loc, declaration_get()->type_get().template dncast<ss_function>()->returns_get().template dncast<ss_pseudoreference>()->what_get(), psp_get(), nsp_get(),
			get, r);
	}
	else {
		e = SS::create(loc, declaration_get()->type_get().template dncast<ss_function>()->returns_get(), psp_get(), nsp_get(),
			get, r);
	}
	/* this creates the assign operation with the sideeffect */
	lassert(num_args <= 3);
	if (l->type_get()->accept_or_ics_base_cv(v) == OR_CV_REFERENCE)
		p = ss_assign::create(loc, ss_pseudoreference::instance(l->type_get().template dncast<ss_reference>()->what_get()), psp_get(), nsp_get(), l_expr, e);
	else
		p = ss_assign::create(loc, l->type_get(), psp_get(), nsp_get(), l_expr, e);
		
	ptr< ss_se > __se = ss_se::create(loc, p, psp_get(), nsp_get());

	/* psp/nsps have lists of previous/next sideeffects. this updates them */
	psp_get()->nse_get()->push_back(__se);
	nsp_get()->pse_get()->push_back(__se);

	if (num_args == 1) {
		result_set(p);
	} else {
		result_set(get);
	}
}

/*!
 * This template covers operators which have form of some op + equal
 *
 * This function takes the arguments to the operator from it's environment (the containing class'
 * fields) and constructs the ss_expression, the type of which is given by the template
 * argument.
 * Also, it inserts the newly created sideeffect to the sideeffect list of the guarding sequence
 * points.
 * SS - ss expression corresponding to the particular operator
 */
template <typename SS> void or_builtin_operator_expression_creator::construct_op_equal_sideeff()
{
	ptr< ss_se > se;
	ptr< ::lestes::std::source_location > loc = location_get();
	ptr< or_or_functional > l, r;
	ptr < ss_expression > e, l_expr, r_expr, l_expr_rval, r_expr_rval, r_;
	ptr< ss_assign > p;
	::lestes::std::list< srp< or_or_functional > >::iterator it = arguments_get()->begin();
	ptr< or_ics_functional > l_func, r_func; 
	::lestes::std::list< srp< ss_type > >::iterator it_p = declaration_get()->type_get().template dncast <ss_function>()->params_get()->begin();
	bool is_ptr_arith = false;


	l = *it;
	it++;
	r = *it;

	ptr< or_ics_visitor_cv > v = or_ics_visitor_cv::create();
	if (l->type_get()->accept_or_ics_base_cv(v) != OR_CV_PSEUDOREFERENCE) {
		report << non_lval_assign << loc;
		exit(1);
	}

	
	/* find implicit conversion between argument and function parameter types. The left has to be identity, for ss2pi to be happy (lvalue) */

	if (l->type_get().template dncast<ss_pseudoreference>()->what_get()->accept_or_ics_base_cv(v) == OR_CV_PTR) {

		is_ptr_arith = true;
		l_func = or_ics_functional_for_std_conversion::create(RANK_EXACT, l->type_get());
		if(r->type_get()->accept_or_ics_base_cv(v) == OR_CV_PSEUDOREFERENCE) {
			if(r->type_get().template dncast<ss_pseudoreference>()->what_get()->accept_or_ics_base_cv(v) != OR_CV_SINT) {
				report << assign_incompatible_types << loc;
				exit(1);
			}
			r_func = or_find_ics(r, ss_type_sint::instance());
			if (!r_func) {
				report << incompatible_types_conversion << loc;
				exit(1);
			}	
		}
		else {
			r_func = or_find_ics(r, ss_type_sint::instance());
			if (!r_func) {
				report << incompatible_types_conversion << loc;
				exit(1);
			}	
		}
	} else {
		l_func = or_find_ics(l, *it_p);
		if (!l_func) {
			report << incompatible_types_conversion << loc;
			exit(1);
		}	
		r_func = or_find_ics(r, l->type_get().template dncast<ss_pseudoreference>()->what_get());
		if (!r_func) {
			report << incompatible_types_conversion << loc;
			exit(1);
		}	

	}
	
	it = arguments_get()->begin();
	it_p++;
		
	/* this constructs the actual ss_expression for parameters, taking in account conversion sequences needed */
	l_expr = (*l_func)((*l)(psp_get(), nsp_get(), loc, sp_list_get()), psp_get(), nsp_get());
	r_expr = (*r_func)((*r)(psp_get(), nsp_get(), loc, sp_list_get()), psp_get(), nsp_get());

	/* In the case of pointer arithmetics we have to multiply the constant by the sizeof() of
	 * the underlying type
	 */
	if (is_ptr_arith) {
		r_ = ss_literal::create(loc, ss_type_sint::instance(), psp_get(), nsp_get(), 
				ss_integral_literal_info::create_from_number(ss_type_sint::instance(), 
					ss_type_size_evaluator::instance()->size_get(
						l->type_get().template dncast<ss_pseudoreference>()->what_get().template dncast<ss_pointer>()->what_get())/8));
		r_expr = ss_conversion::create(	loc, 
				l->type_get().template dncast<ss_pseudoreference>()->what_get(),
				psp_get(),
				nsp_get(),
				ss_mul::create(	loc, 
					ss_type_sint::instance(), 
					psp_get(), 
					nsp_get(), 
					r_expr,
					r_),
				ss_type_sint::instance()
				);
	}

	if(l_expr->type_get()->accept_or_ics_base_cv(v) == OR_CV_PSEUDOREFERENCE) {
		l_expr_rval = ss_get::create(loc, l->type_get().template dncast<ss_pseudoreference>()->what_get(), psp_get(), nsp_get(), l_expr); 
	} else {
		l_expr_rval = l_expr;
	}		

	if(r_expr->type_get()->accept_or_ics_base_cv(v) == OR_CV_PSEUDOREFERENCE) {
		r_expr_rval = ss_get::create(loc, r->type_get().template dncast<ss_pseudoreference>()->what_get(), psp_get(), nsp_get(), r_expr); 
	} else {
		r_expr_rval = r_expr;
	}
	
	e = SS::create(	loc, 
			r_expr_rval->type_get(), 
			psp_get(), 
			nsp_get(), 
			l_expr_rval, 
			r_expr_rval);

	p = ss_assign::create(loc, l->type_get(), psp_get(), nsp_get(), l_expr, e);
	se = ss_se::create(loc, p, psp_get(), nsp_get());

	/* psp/nsps have lists of previous/next sideeffects. this updates them */
	psp_get()->nse_get()->push_back(se);
	nsp_get()->pse_get()->push_back(se);

	result_set(p);
}

/*!
 * This handles the '=' operator.
 */
void or_builtin_operator_expression_creator::visit_ss_operator_assign( ptr< ss_operator_assign > )
{
	ptr< ss_se > se;
	ptr< ::lestes::std::source_location > loc = location_get();
	ptr< or_or_functional > l, r, e;
	ptr< ss_expression > l_expr, r_expr;
	ptr< ss_assign > p;
	::lestes::std::list< srp< or_or_functional > >::iterator it = arguments_get()->begin();
	ptr< or_ics_functional > l_func, r_func; 
	::lestes::std::list< srp< ss_type > >::iterator it_p = declaration_get()->type_get().dncast <ss_function>()->params_get()->begin();

	l = *it;
	it++;
	r = *it;

	/* find implicit conversion between argument and function parameter types */
	it = arguments_get()->begin();

	ptr< or_ics_visitor_cv > v = or_ics_visitor_cv::create();
	if (l->type_get()->accept_or_ics_base_cv(v) != OR_CV_PSEUDOREFERENCE && l->type_get()->accept_or_ics_base_cv(v) != OR_CV_REFERENCE) {
		report << non_lval_assign << loc;
		exit(1);
	}

	/* left param should stay the same */
	l_func = or_ics_functional_for_std_conversion::create(RANK_EXACT, l->type_get());
	//l_func = or_find_ics(l, *it_p);
	it_p++;

	/* If the right parameter is not pointer, we must convert it to the type of the whole 
	 * assignment, as ss2pi is expecting all the types in operations the same
	 */
	if(l->type_get()->accept_or_ics_base_cv(v) == OR_CV_PTR) {
		r_func = or_find_ics(r, *it_p);
		if (!r_func) {
			report << incompatible_types_conversion << loc;
			exit(1);
		}
	}
	else {
		ptr< or_ics_visitor_cv > v1 = or_ics_visitor_cv::create();
		if(declaration_get()->type_get().dncast <ss_function>()->returns_get()->accept_or_ics_base_cv(v) == OR_CV_PSEUDOREFERENCE) {
			r_func = or_find_ics(r, declaration_get()->type_get().dncast <ss_function>()->returns_get().dncast<ss_pseudoreference>()->what_get());
			if (!r_func) {
				report << incompatible_types_conversion << loc;
				exit(1);
			}	
		}
		else if(declaration_get()->type_get().dncast <ss_function>()->returns_get()->accept_or_ics_base_cv(v) == OR_CV_REFERENCE) {
			r_func = or_find_ics(r, declaration_get()->type_get().dncast <ss_function>()->returns_get().dncast<ss_reference>()->what_get());
			if (!r_func) {
				report << incompatible_types_conversion << loc;
				exit(1);
			}	
		}
 		else 
		{
			r_func = or_find_ics(r, declaration_get()->type_get().dncast <ss_function>()->returns_get());
			if (!r_func) {
				report << incompatible_types_conversion << loc;
				exit(1);
			}	
		}
	}
		
	/* this constructs the actual ss_expression for parameters, taking in account conversion sequences needed */
	l_expr = (*l_func)((*l)(psp_get(), nsp_get(), loc, sp_list_get()), psp_get(), nsp_get());
	r_expr = (*r_func)((*r)(psp_get(), nsp_get(), loc, sp_list_get()), psp_get(), nsp_get());
	
	p = ss_assign::create(	loc,
				//l->type_get(),
				declaration_get()->type_get().dncast <ss_function>()->returns_get(),
				psp_get(),
				nsp_get(),
				l_expr,
			r_expr);
	se = ss_se::create(loc, p, psp_get(), nsp_get());

	/* psp/nsps have lists of previous/next sideeffects. this updates them */
	psp_get()->nse_get()->push_back(se);
	nsp_get()->pse_get()->push_back(se);

	result_set(p);
}


/*!
 * comma is handled separately in it's own functional.
 */
void or_builtin_operator_expression_creator::visit_ss_operator_comma( ptr< ss_operator_comma > )
{
	/* nop */
}

void or_builtin_operator_expression_creator::visit_ss_dummy_name( ptr< ss_dummy_name> )
{
	lassert2(false, "Dummy name unhandled in or_builtin_operator_expression_creator\n");
}

void or_builtin_operator_expression_creator::visit_ss_operator_function_call( ptr< ss_operator_function_call >)
{
	lassert2(false, "There is no such builtin op\n");
}

void or_builtin_operator_expression_creator::visit_ss_operator_array( ptr< ss_operator_array >)
{
	lassert2(false, "ss_operator_array not yet implemented in or_builtin_operator_expression_creator\n");
}

void or_builtin_operator_expression_creator::visit_ss_operator_access( ptr< ss_operator_access >)
{
	lassert2(false, "ss_operator_access not yet implemented in or_builtin_operator_expression_creator\n");
}
void or_builtin_operator_expression_creator::visit_ss_operator_access_member( ptr< ss_operator_access_member >)
{
	lassert2(false, "ss_operator_access_member not yet implemented in or_builtin_operator_expression_creator\n");
}

void or_builtin_operator_expression_creator::visit_ss_operator_ternary( ptr< ss_operator_ternary >)
{
	lassert2(false, "ss_operator_ternary not yet implemented in or_builtin_operator_expression_creator\n");
}

void or_builtin_operator_expression_creator::visit_ss_ordinary_name( ptr< ss_ordinary_name >)
{
	lassert2(false,"This type of name is not a builtin operator name");
}

void or_builtin_operator_expression_creator::visit_ss_conversion_name( ptr< ss_conversion_name >)
{
	lassert2(false,"This type of name is not a builtin operator name");
}


/* == It has not been decided yet how to handle the following == */
void or_builtin_operator_expression_creator::visit_ss_operator_new( ptr< ss_operator_new >)
{
}

void or_builtin_operator_expression_creator::visit_ss_operator_delete( ptr< ss_operator_delete >)
{
}

void or_builtin_operator_expression_creator::visit_ss_operator_new_array( ptr< ss_operator_new_array >)
{
}

void or_builtin_operator_expression_creator::visit_ss_operator_delete_array( ptr< ss_operator_delete_array >)
{
}
/* == End of not yet decided == */

/* == The following binary operators without sideeffect are just straightforward uses of the template written above == */
void or_builtin_operator_expression_creator::visit_ss_operator_land( ptr< ss_operator_land >)
{
	construct_bin_op_nosideeff_newsp<ss_land>();
}

void or_builtin_operator_expression_creator::visit_ss_operator_lor( ptr< ss_operator_lor >)
{
	construct_bin_op_nosideeff_newsp<ss_lor>();
}

void or_builtin_operator_expression_creator::visit_ss_operator_add( ptr< ss_operator_add >)
{
	::lestes::std::list< srp< or_or_functional > >::size_type num_args = arguments_get()->size();
	ptr< or_or_functional_literal > literal_zero;
	if(num_args == 1) {
	        literal_zero = or_or_functional_literal::create(ss_type_sint::instance(),ss_integral_literal_info::create_from_number(ss_type_sint::instance(), 0));
		arguments_get()->push_front(literal_zero);
	}
	construct_bin_op_nosideeff_arith<ss_add>();
}

void or_builtin_operator_expression_creator::visit_ss_operator_sub( ptr< ss_operator_sub >)
{
	::lestes::std::list< srp< or_or_functional > >::size_type num_args = arguments_get()->size();
	ptr< or_or_functional_literal > literal_zero;
	if(num_args == 1) {
	        literal_zero = or_or_functional_literal::create(ss_type_sint::instance(),ss_integral_literal_info::create_from_number(ss_type_sint::instance(), 0));
		arguments_get()->push_front(literal_zero);
	}
	construct_bin_op_nosideeff_arith<ss_sub>();
}

void or_builtin_operator_expression_creator::visit_ss_operator_mul( ptr< ss_operator_mul >)
{
	::lestes::std::list< srp< or_or_functional > >::size_type num_args = arguments_get()->size();
	if (num_args == 2)
		construct_bin_op_nosideeff_arith<ss_mul>();
	else if (num_args == 1) 
		construct_un_op_nosideeff<ss_dereference>();
	else
		lassert2(false, "Strange, >2 arguments to operator*\n");
}

void or_builtin_operator_expression_creator::visit_ss_operator_div( ptr< ss_operator_div >)
{
	construct_bin_op_nosideeff_arith<ss_div>();
}

void or_builtin_operator_expression_creator::visit_ss_operator_mod( ptr< ss_operator_mod >)
{
	construct_bin_op_nosideeff_arith<ss_mod>();
}

void or_builtin_operator_expression_creator::visit_ss_operator_bxor( ptr< ss_operator_bxor >)
{
	construct_bin_op_nosideeff_arith<ss_bxor>();
}

void or_builtin_operator_expression_creator::visit_ss_operator_band( ptr< ss_operator_band >)
{
	::lestes::std::list< srp< or_or_functional > >::size_type num_args = arguments_get()->size();

	if (num_args == 1)
		construct_un_op_nosideeff<ss_address_of>();
	else
		construct_bin_op_nosideeff_arith<ss_band>();
}

void or_builtin_operator_expression_creator::visit_ss_operator_bor( ptr< ss_operator_bor >)
{
	construct_bin_op_nosideeff_arith<ss_bor>();
}
void or_builtin_operator_expression_creator::visit_ss_operator_shr( ptr< ss_operator_shr >)
{
	construct_bin_op_nosideeff_arith<ss_shr>();
}

void or_builtin_operator_expression_creator::visit_ss_operator_shl( ptr< ss_operator_shl >)
{
	construct_bin_op_nosideeff_arith<ss_shl>();
}

void or_builtin_operator_expression_creator::visit_ss_operator_sbl( ptr< ss_operator_sbl >)
{
	construct_bin_op_nosideeff<ss_sbl>();
}

void or_builtin_operator_expression_creator::visit_ss_operator_sbg( ptr< ss_operator_sbg >)
{
	construct_bin_op_nosideeff<ss_sbg>();
}

void or_builtin_operator_expression_creator::visit_ss_operator_sbng( ptr< ss_operator_sbng >)
{
	construct_bin_op_nosideeff<ss_sbng>();
}

void or_builtin_operator_expression_creator::visit_ss_operator_sbnl( ptr< ss_operator_sbnl >)
{
	construct_bin_op_nosideeff<ss_sbnl>();
}

void or_builtin_operator_expression_creator::visit_ss_operator_sbe( ptr< ss_operator_sbe >)
{
	construct_bin_op_nosideeff<ss_sbe>();
}

void or_builtin_operator_expression_creator::visit_ss_operator_sbne( ptr< ss_operator_sbne >)
{
	construct_bin_op_nosideeff<ss_sbne>();
}
/* == End of binary without sideeffect == */

/* == The following unary opetrators without sideffect are just straightfoward uses of the template above == */
void or_builtin_operator_expression_creator::visit_ss_operator_bnot( ptr< ss_operator_bnot >)
{
	construct_un_op_nosideeff<ss_bnot>();
}

void or_builtin_operator_expression_creator::visit_ss_operator_lnot( ptr< ss_operator_lnot >)
{
	construct_un_op_nosideeff<ss_lnot>();
}

/* == The end of unary operators without sideeffect == */

/* == The following compound binary opetrators with assignment sideffect are just straightfoward uses of the template written above == */
void or_builtin_operator_expression_creator::visit_ss_operator_assign_add( ptr< ss_operator_assign_add >)
{
	construct_op_equal_sideeff<ss_add>();
}

void or_builtin_operator_expression_creator::visit_ss_operator_assign_sub( ptr< ss_operator_assign_sub >)
{
	construct_op_equal_sideeff<ss_sub>();
}

void or_builtin_operator_expression_creator::visit_ss_operator_assign_mul( ptr< ss_operator_assign_mul >)
{
	construct_op_equal_sideeff<ss_mul>();
}

void or_builtin_operator_expression_creator::visit_ss_operator_assign_div( ptr< ss_operator_assign_div >)
{
	construct_op_equal_sideeff<ss_mul>();
}

void or_builtin_operator_expression_creator::visit_ss_operator_assign_mod( ptr< ss_operator_assign_mod >)
{
	construct_op_equal_sideeff<ss_mod>();
}

void or_builtin_operator_expression_creator::visit_ss_operator_assign_bxor( ptr< ss_operator_assign_bxor >)
{
	construct_op_equal_sideeff<ss_bxor>();
}

void or_builtin_operator_expression_creator::visit_ss_operator_assign_band( ptr< ss_operator_assign_band >)
{
	construct_op_equal_sideeff<ss_band>();
}

void or_builtin_operator_expression_creator::visit_ss_operator_assign_bor( ptr< ss_operator_assign_bor >)
{
	construct_op_equal_sideeff<ss_bor>();
}

void or_builtin_operator_expression_creator::visit_ss_operator_assign_shl( ptr< ss_operator_assign_shl >)
{
	construct_op_equal_sideeff<ss_shl>();
}

void or_builtin_operator_expression_creator::visit_ss_operator_assign_shr( ptr< ss_operator_assign_shr >)
{
	construct_op_equal_sideeff<ss_shr>();
}

/* == End of compound binary == */


/* When calling overload resolution for one of prefix/postfix, extra expression representing
 * zero of type int will be created
 *
 * It is necessary to distinguish between prefix and postfix. This is done in the templated 
 * function.
 */
void or_builtin_operator_expression_creator::visit_ss_operator_inc( ptr< ss_operator_inc >)
{
	construct_op_crement<ss_add>();
}

void or_builtin_operator_expression_creator::visit_ss_operator_dec( ptr< ss_operator_dec >)
{
	construct_op_crement<ss_sub>();
}

/* == END OF BUILTIN OPERATOR EXPRESSION CREATOR == */

/* The following cases are not possible, because we know that we have the pointer
 * to builtin or function declaration
 */

void or_declaration_helper::visit_ss_injected_class_declaration( ptr< ss_injected_class_declaration >)
{
	lassert(false);
}

void or_declaration_helper::visit_ss_structure_declaration( ptr< ss_structure_declaration >)
{
	lassert(false);
}

void or_declaration_helper::visit_ss_namespace_definition( ptr< ss_namespace_definition >)
{
	lassert(false);
}

void or_declaration_helper::visit_ss_object_declaration( ptr< ss_object_declaration >)
{
	lassert(false);
}

void or_declaration_helper::visit_ss_bitfield_declaration( ptr< ss_bitfield_declaration >)
{
	lassert(false);
}

void or_declaration_helper::visit_ss_parameter_declaration( ptr< ss_parameter_declaration >)
{
	lassert(false);
}

void or_declaration_helper::visit_ss_enumerator_declaration( ptr< ss_enumerator_declaration >)
{
	lassert(false);
}

void or_declaration_helper::visit_ss_enum_definition( ptr< ss_enum_definition >)
{
	lassert(false);
}

void or_declaration_helper::visit_ss_typedef_definition( ptr< ss_typedef_definition >)
{
	lassert(false);
}

void or_declaration_helper::visit_ss_using_declaration( ptr< ss_using_declaration >)
{
	lassert(false);
}

void or_declaration_helper::visit_ss_compound_stmt_declaration( ptr< ss_compound_stmt_declaration >)
{
	lassert(false);
}
void or_declaration_helper::visit_ss_fake_declaration( ptr< ss_fake_declaration >)
{
	lassert(false);
}

/*!
 * In case of function declaration creation of proper funcall is needed (done by or_funcall_creator)
 * and returning the created funcall via result
 */
void or_declaration_helper::visit_ss_function_declaration( ptr< ss_function_declaration > a )
{
	ptr< or_funcall_creator > crtr = or_funcall_creator::create(a, psp_get(), nsp_get(), arguments_get(), location_get(), sp_list_get() );
	ptr< ss_declaration > decl = a->contained_in_get()->declared_by_get();

	decl->accept_ss_declaration_visitor(crtr);
	result_set(crtr->result_get());
}

/*!
 * In case of function declaration creation of proper funcall is needed (done by or_funcall_creator)
 * and returning the created funcall via result
 *
 * \todo adapt to member function call. FIXME TODO
 */
void or_declaration_helper::visit_ss_method_declaration( ptr< ss_method_declaration > a )
{
	ptr< or_funcall_creator > crtr = or_funcall_creator::create(a, psp_get(), nsp_get(), arguments_get(), location_get(), sp_list_get() );
	ptr< ss_declaration > decl = a->contained_in_get()->declared_by_get();

	decl->accept_ss_declaration_visitor(crtr);
	result_set(crtr->result_get());
}
/*!
 * In case of builtin operator we delegate the creation of appropriate structures to
 * the visitor over the operator name, ie. we delegate the decission to somebody who
 * makes it according to the kind of the operator
 */
void or_declaration_helper::visit_ss_builtin_operator_declaration( ptr< ss_builtin_operator_declaration > a )
{
	ptr< or_builtin_operator_expression_creator > decl_v = or_builtin_operator_expression_creator::create(a, psp_get(), nsp_get(), arguments_get(),
			location_get(), sp_list_get());
	a->name_get()->accept_ss_decl_name_visitor(decl_v);
	result_set(decl_v->result_get());
}


void or_funcall_creator::visit_ss_enumerator_declaration( ptr< ss_enumerator_declaration >)
{
	lassert(false);
}
void or_funcall_creator::visit_ss_using_declaration( ptr< ss_using_declaration >)
{
	lassert(false);
}
void or_funcall_creator::visit_ss_compound_stmt_declaration( ptr< ss_compound_stmt_declaration >)
{
	lassert(false);
}
void or_funcall_creator::visit_ss_fake_declaration( ptr< ss_fake_declaration >)
{
	lassert(false);
}
/*!
 * if the parent scope is namespace, we have to create function call
 */
void or_funcall_creator::visit_ss_namespace_definition( ptr< ss_namespace_definition >)
{
	/* first, we create prefunc and postfunc sequence points. The sequence points are
	 * ordered as following: 
	 *
	 * psp <-> ARGUMENTS <-> prefunc_sp <-> FUNCTION CALL <-> postfunc_sp <-> nsp 
	 */
	lint level = MAX(psp_get()->level_get(), nsp_get()->level_get())+1;
	ptr< ss_sp > prefunc_sp = ss_sp::create(location_get(), psp_get(), NULL, level);
        sp_list_get()->push_back(prefunc_sp);
	ptr< ss_sp > postfunc_sp = ss_sp::create(location_get(), prefunc_sp, nsp_get(), level);
        sp_list_get()->push_back(postfunc_sp);
	prefunc_sp->nsp_set(postfunc_sp);
	ptr< ss_funcall > f;
	
	/* now process the functionals representing the parameters and create ss_expression for every
	 * parameter, with proper sequence points ordering 
	 */

	::lestes::std::list< srp< or_or_functional > > ::iterator it = arguments_get()->begin();
	::lestes::std::list< srp< ss_declaration > >::iterator it_p = declaration_get().dncast<ss_function_declaration>()->parameters_get()->contents_get()->begin();
	ptr< ::lestes::std::list< srp< ss_expression > > > arg_exprs = ::lestes::std::list< srp< ss_expression > >::create();

	/* create list of arguments (or_or_functional::operator() creates the ss_expression) */	
	for (; it != arguments_get()->end() ; it++, it_p++) {
		ptr< or_ics_functional > p_func = or_find_ics((*it), (*it_p)->type_get());
		if (!p_func) {
			report << incompatible_types_conversion << location_get();
			exit(1);
		}	

		arg_exprs->push_back((*p_func)(((**it)(psp, prefunc_sp, location_get(), sp_list_get() )), psp, prefunc_sp));
	}

	ptr< or_ics_visitor_cv > v = or_ics_visitor_cv::create();
	if(declaration_get()->type_get().dncast<ss_function>()->returns_get()->accept_or_ics_base_cv(v) == OR_CV_REFERENCE || 
			declaration_get()->type_get().dncast<ss_function>()->returns_get()->accept_or_ics_base_cv(v) == OR_CV_VOID)

		result_set(f = ss_funcall::create(	location_get(), 
						declaration_get()->type_get().dncast<ss_function>()->returns_get(), 
						prefunc_sp, 
						postfunc_sp, 
						arg_exprs, 
						declaration_get()));
						
	else
		result_set(ss_get::create(location_get(), 
					declaration_get()->type_get().dncast<ss_function>()->returns_get(),  
					postfunc_sp,
					nsp_get(),
					f = ss_funcall::create(	location_get(), 
								ss_pseudoreference::instance(declaration_get()->type_get().dncast<ss_function>()->returns_get()), 
								prefunc_sp, 
								postfunc_sp, 
								arg_exprs, 
								declaration_get()))
					);
	/* Funcall introduces new sideeffect */

	ptr< ss_se > se = ss_se::create(location_get(), f, prefunc_sp, postfunc_sp);
	/* psp/nsps have lists of previous/next sideeffects. this updates them */
	prefunc_sp->nse_get()->push_back(se);
	postfunc_sp->pse_get()->push_back(se);
		
}

void or_funcall_creator::visit_ss_structure_declaration( ptr< ss_structure_declaration >)
{
	// TODO construct mfuncall, vfuncall (declaration and object parameter are passed to the visitor
	// context_get(), declaration_get(), object_expression_get()

	lassert2(false, "mfuncall/vfuncall not yet implemented in or_funcall_creator\n");
}
void or_funcall_creator::visit_ss_object_declaration( ptr< ss_object_declaration >)
{
	lassert2(false,"Object declaration shall not be parent of function declaration");
}

void or_funcall_creator::visit_ss_bitfield_declaration( ptr< ss_bitfield_declaration >)
{
	lassert2(false,"Bitfield declaration shall not be parent of function declaration");
}

void or_funcall_creator::visit_ss_parameter_declaration( ptr< ss_parameter_declaration >)
{
	lassert2(false,"Parameter declaration shall not be parent of function declaration");
}

void or_funcall_creator::visit_ss_injected_class_declaration( ptr< ss_injected_class_declaration >)
{
	lassert2(false,"Injected class declaration shall not be parent of function declaration");
}

void or_funcall_creator::visit_ss_enum_definition( ptr< ss_enum_definition >)
{
	lassert2(false,"Enum declaration shall not be parent of function declaration");
}

void or_funcall_creator::visit_ss_typedef_definition( ptr< ss_typedef_definition >)
{
	lassert2(false,"Typedef declaration shall not be parent of function declaration");
}

void or_funcall_creator::visit_ss_function_declaration( ptr< ss_function_declaration >)
{
	lassert2(false,"Function declaration declaration shall not be parent of function declaration");
}

void or_funcall_creator::visit_ss_method_declaration( ptr< ss_method_declaration >)
{
	lassert2(false,"Method declaration shall not be parent of function declaration");
}

void or_funcall_creator::visit_ss_builtin_operator_declaration( ptr< ss_builtin_operator_declaration >)
{
	lassert2(false,"Builtin op declaration shall not be parent of function declaration");
}


/*!
 * This is overloaded operator () of functional used by overload resolution.
 * It creates corresponding funcall and returns it's expression.
 *
 * \bug context doesn't contain VMT yet, so vfuncall is irrelevant
 *
 * \return pointer to expression, representing the function call
 */
ptr< ss_expression > or_or_functional_concrete::operator()(ptr< ss_sp > psp, ptr< ss_sp > nsp, ptr< ::lestes::std::source_location > location, ptr< ::lestes::std::list< srp< ss_sp > > > sp_list)
{
	ptr< or_declaration_helper > v = or_declaration_helper::create(declaration_get(), psp, nsp, args_get(), location, sp_list);
	declaration_get()->accept_ss_declaration_visitor(v);
	return v->result_get();
}

/*!
 * This is overloaded operator () of functional used by overload resolution.
 * It creates corresponding funcall and returns it's expression.
 *
 * This functional handles the special case of comma operator. This is handled 
 * in a slightly different way (as some other operators, line unary & and ->), as
 * they exist for infinite number of types and their declarations are not filled in
 * the builtin op decl_seq
 *
 * \bug context doesn't contain VMT yet, so vfuncall is irrelevant
 *
 * \return pointer to expression, representing the function call
 */
ptr< ss_expression > or_or_functional_comma::operator()(ptr< ss_sp > psp, ptr< ss_sp > nsp, ptr< ::lestes::std::source_location > location, ptr< ::lestes::std::list< srp< ss_sp > > > sp_list)
{
	ptr< or_or_functional > l, r;
	ptr< ss_expression > l_expr, r_expr;
	ptr< or_ics_functional > l_func, r_func;
	ptr< ::lestes::std::source_location > loc = location;
	::lestes::std::list< srp< or_or_functional > >::iterator it = args_get()->begin();

	l = *it;
	it++;
	r = *it++;

	/* find implicit conversion between argument and function parameter types. Nop here. */

	/* create new sequence point and add to lists properly */
	lint level = MAX(psp->level_get(), nsp->level_get())+1;
	ptr< ss_sp > new_sp = ss_sp::create(loc, psp, nsp, level);
	sp_list->push_back(new_sp);

	/* this constructs the actual ss_expression for parameters, taking in account conversion sequences needed */
	l_expr = (*l)(psp, new_sp, loc, sp_list);
	r_expr = (*r)(new_sp, nsp, loc, sp_list);

        /* get the time of this expression */
	return r_expr;
}

ptr< ss_expression > or_or_functional_addrof::operator()(ptr< ss_sp > psp, ptr< ss_sp > nsp, ptr< ::lestes::std::source_location > location, ptr< ::lestes::std::list< srp< ss_sp > > > sp_list)
{
	ptr< or_or_functional > l;
	ptr< ss_expression > l_expr;
	ptr< or_ics_functional > l_func;
	ptr< ::lestes::std::source_location > loc = location;
	::lestes::std::list< srp< or_or_functional > >::iterator it = args_get()->begin();

	l = *it;

	/* find implicit conversion between argument and function parameter types. Nop here. */

	/* this constructs the actual ss_expression for parameters, taking in account conversion sequences needed */
	l_expr = (*l)(psp, nsp, loc, sp_list);

        /* get the time of this expression */
	return ss_address_of::create(location, type_get(), psp, nsp, l_expr);
}

/*!
 * This is overloaded operator () of functional used by overload resolution.
 * It servers for purpose of creating ss_this_expr expression.
 * It creates corresponding ss_this_expr expression.
 *
 * \return pointer to expression, representing the function call
 */
ptr< ss_expression > or_or_functional_this::operator()(ptr< ss_sp > psp, ptr< ss_sp > nsp, ptr< ::lestes::std::source_location > location, ptr< ::lestes::std::list< srp< ss_sp > > >)
{
	return ss_this_expr::create(location,
			sa_statements::instance()->current_function_get()->type_get().dncast<ss_member_function>()->this_type_get(), psp, nsp);
}

/*!
 * This is overloaded operator () of functional used by overload resolution.
 * It servers for purpose of creating ss_literal expression from ss_literal_info
 * It creates corresponding ss_literal expression.
 *
 * \return pointer to expression, representing the function call
 */
ptr< ss_expression > or_or_functional_literal::operator()(ptr< ss_sp > psp, ptr< ss_sp > nsp, ptr< ::lestes::std::source_location > location, ptr< ::lestes::std::list< srp< ss_sp > > >)
{
	/* TODO as soon as there are other than integral literals, this or_or_functional should 
	 * hold (in some enum) the type of the literal and fill the type field of the newly
	 * created ss_literal accordingly
	 */
	return ss_literal::create(location, type_get(), psp, nsp, literal_get());
}

/*!
 * This functional represents ambiguous conversion and list of viables
 * should be returned. This is not currently possible, as the prototype
 * restricts us to return ptr< ss_expression >
 *
 * The list of viables is needed to provide user with 'candidates are: blah blah'
 * message
 */
ptr< ss_expression > or_or_functional_ambiguous::operator()(ptr< ss_sp > psp, ptr< ss_sp > nsp, ptr< ::lestes::std::source_location > location, ptr< ::lestes::std::list< srp< ss_sp > > >)
{
	/* just to avoid unused parameter warning */
	psp = psp;
	nsp = nsp;
	location = location;
	/* TODO dump the viables to the user */
	report << ambiguous_result << location;
	exit(1);
}

/*!
 * This functional represents no-viables-found functional.
 */
ptr< ss_expression > or_or_functional_noviable::operator()(ptr< ss_sp > psp, ptr< ss_sp > nsp, ptr< ::lestes::std::source_location > location, ptr< ::lestes::std::list< srp< ss_sp > > >)
{
	/* just to avoid unused parameter warning */
	psp = psp;
	nsp = nsp;
	location = location;
	/* TODO dump the viables to the user */
	report << no_viable_found << location;
	exit(1);
}

/*!
 * This functional represents the list of function declarations, from which the 
 * right one should be chosen. The function declarations are obtained from the
 * functional directly using _get() getter, not by calling the operator(), so
 * this would lassert
 */
ptr< ss_expression > or_or_functional_func_decl_set::operator()(ptr< ss_sp > psp, ptr< ss_sp > nsp, ptr< ::lestes::std::source_location > location, ptr< ::lestes::std::list< srp< ss_sp > > > sp_list)
{
	/* just to avoid unused param warning :) */
	psp = psp;
	nsp = nsp;
	location = location;
	sp_list = sp_list;

	lassert2(false, "operator() on or_or_functional_func_decl_set shouldn't be called");
}

/*!
 * This functional holds a single declaration of variable, so it creates varref 
 * for the variable in question.
 *
 * There are special cases to be handled here - for example constructing varref 
 * if the declaration in question is name of a type doesn't make sense - we have
 * to handle function-style-cast somehow instead. This is TODO
 *
 * If the obtained declaration is namespace declaration, boom.
 */
ptr< ss_expression > or_or_functional_decl::operator()(ptr< ss_sp > psp, ptr< ss_sp > nsp, ptr< ::lestes::std::source_location > location, ptr< ::lestes::std::list< srp< ss_sp > > > sp_list)
{
	/* avoid gcc unused var warning */
	sp_list = sp_list;
	
	ptr< ss_decl_to_enum > v = ss_decl_to_enum::create();
	ss_decl_enum e;

	declaration_get()->accept_ss_declaration_visitor(v);
	e = v->result_get();

	if (e == NAMESPACE_DEFINITION || e == USING_DECLARATION) {
		lassert2(false, "The declaration in or_or_functional_decl::operator() is wrong (namespace || using)");
	} 
	else if (e == TYPEDEF_DEFINITION) {
		lassert2(false, "Function style cast is not yet implemented && varref for typename is wrong");
	}
	
	/* return the apropriate varref */
	else { 
		/* if the type is not (pseudo)reference, create pseudoreference. otherwise just var_ref */
		ptr< or_ics_visitor_cv > v = or_ics_visitor_cv::create();
		if(declaration_get()->type_get()->accept_or_ics_base_cv(v) == OR_CV_REFERENCE || declaration_get()->type_get()->accept_or_ics_base_cv(v) == OR_CV_PSEUDOREFERENCE ) 
			return ss_var_ref::create(location, declaration_get()->type_get(), psp, nsp, declaration_get());
		else
			return ss_var_ref::create(location, ss_pseudoreference::instance(declaration_get()->type_get()), psp, nsp, declaration_get());
	}
}


end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);

