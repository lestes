/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Type information for SS.

  Definition of visitor providing type information for certain ss_type classes.
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/sem/ss_type2info.g.hh>
#include <lestes/lang/cplus/sem/ss_type.g.hh>

package(lestes);
package(lang);
package(cplus);
package(sem);

/*
	TODO pt appropriate cast preparation
	ss_const_volatile_object = NULL;
	ss_pointer_object = NULL;
	ss_member_pointer_object = NULL;
	ss_reference_object = NULL;
*/

/*!
  Visits object of any class in ss_struct_base hierarchy.
  Sets type information appropriately.
  \pre obj != NULL;
  \param obj  The object to visit.
*/
void ss_type2info::default_ss_struct_base(ptr<ss_struct_base> obj)
{
	lassert(obj);
	ss_const_object = NULL;
	ss_volatile_object = NULL;
	info = UNKNOWN;
}

/*!
  Visits object of any class in ss_builtin_type hierarchy.
  Sets type information appropriately.
  \pre obj != NULL
  \param obj  The object to visit.
*/
void ss_type2info::default_ss_builtin_type(ptr<ss_builtin_type> obj)
{
	lassert(obj);
	ss_const_object = NULL;
	ss_volatile_object = NULL;
	info = UNKNOWN;
}

/*!
  Visits object of class ss_member_function.
  Sets type information appropriately.
  \pre obj != NULL
  \param obj  The object to visit.
 */
void ss_type2info::visit_ss_member_function(ptr<ss_member_function> obj)
{
	lassert(obj);
	ss_const_object = NULL;
	ss_volatile_object = NULL;
	info = UNKNOWN;
}

/*!
  Visits object of class ss_array.
  Sets type information appropriately.
  \pre obj != NULL
  \param obj  The object to visit.
 */
void ss_type2info::visit_ss_array(ptr<ss_array> obj)
{
	lassert(obj);
	ss_const_object = NULL;
	ss_volatile_object = NULL;
	info = UNKNOWN;
}

/*!
  Visits object of class ss_function.
  Sets type information appropriately.
  \pre obj != NULL
  \param obj  The object to visit.
 */
void ss_type2info::visit_ss_function(ptr<ss_function> obj)
{
	lassert(obj);
	ss_const_object = NULL;
	ss_volatile_object = NULL;
	info = SS_FUNCTION;
}

/*!
  Visits object of class ss_enum.
  Sets type information appropriately.
  \pre obj != NULL
  \param obj  The object to visit.
 */
void ss_type2info::visit_ss_enum(ptr<ss_enum> obj)
{
	lassert(obj);
	ss_const_object = NULL;
	ss_volatile_object = NULL;
	info = UNKNOWN;
}

/*!
  Visits object of class ss_const.
  Sets type information appropriately.
  \pre obj != NULL
  \param obj  The object to visit.
 */
void ss_type2info::visit_ss_const(ptr<ss_const> obj)
{
	lassert(obj);
	ss_const_object = obj;
	ss_volatile_object = NULL;
	info = SS_CONST;
}

/*!
  Visits object of class ss_volatile.
  Sets type information appropriately.
  \pre obj != NULL
  \param obj  The object to visit.
 */
void ss_type2info::visit_ss_volatile(ptr<ss_volatile> obj)
{
	lassert(obj);
	ss_const_object = NULL;
	ss_volatile_object = obj;
	info = SS_VOLATILE;
}

/*!
  Visits object of class ss_const_volatile.
  Sets type information appropriately.
  \pre obj != NULL
  \param obj  The object to visit.
 */
void ss_type2info::visit_ss_const_volatile(ptr<ss_const_volatile> obj)
{
	lassert(obj);
	ss_const_object = NULL;
	ss_volatile_object = NULL;
	info = SS_CONST_VOLATILE;
}

/*!
  Visits object of class ss_pointer.
  Sets type information appropriately.
  \pre obj != NULL
  \param obj  The object to visit.
 */
void ss_type2info::visit_ss_pointer(ptr<ss_pointer> obj)
{
	lassert(obj);
	ss_const_object = NULL;
	ss_volatile_object = NULL;
	info = SS_POINTER;
}

/*!
  Visits object of class ss_member_pointer.
  Sets type information appropriately.
  \pre obj != NULL
  \param obj  The object to visit.
 */
void ss_type2info::visit_ss_member_pointer(ptr<ss_member_pointer> obj)
{
	lassert(obj);
	ss_const_object = NULL;
	ss_volatile_object = NULL;
	info = SS_MEMBER_POINTER;
}

/*!
  Visits object of class ss_reference.
  Sets type information appropriately.
  \pre obj != NULL
  \param obj  The object to visit.
 */
void ss_type2info::visit_ss_reference(ptr<ss_reference> obj)
{
	lassert(obj);
	ss_const_object = NULL;
	ss_volatile_object = NULL;
	info = SS_REFERENCE;
}

/*!
  Visits object of class ss_pseudoreference.
  Sets type information appropriately.
  \param obj  The object to visit.
  \author TMA
 */
void ss_type2info::visit_ss_pseudoreference(ptr<ss_pseudoreference> obj)
{
	lassert(obj);
	ss_const_object = NULL;
	ss_volatile_object = NULL;
	info = SS_PSEUDOREFERENCE;
}

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);
