/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*!
  \brief Parameter declaration type analysis.

  Transformation of parameter declaration to SS type.
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/sem/as_decl.g.hh>
#include <lestes/lang/cplus/sem/ss_type.g.hh>
#include <lestes/lang/cplus/sem/sa_param_declaration_type.g.hh>
#include <lestes/lang/cplus/sem/sa_declaration_specifier_list.g.hh>
#include <lestes/lang/cplus/sem/sa_declaration_specifiers.g.hh>
#include <lestes/lang/cplus/sem/sa_declarator_type.g.hh>
#include <lestes/lang/cplus/sem/ss_type2param_type.g.hh>
#include <lestes/lang/cplus/sem/ss_type_builtin.g.hh>
#include <lestes/lang/cplus/sem/sa_loggers.hh>
#include <lestes/msg/logger.hh>
#include <lestes/msg/logger_util.hh>

package(lestes);
package(lang);
package(cplus);
package(sem);

/*!
  Extracts type from parameter declaration and
  sets it into type field.
  \pre decl != NULL
  \param decl  The parameter declaration to process.
*/
void sa_param_declaration_type::process(ptr<as_param_declaration> decl)
{
	sa_param_declaration_type_logger << "sa_param_declaration_type::process()\n" << msg::eolog;

	lassert(decl);
	
	sa_param_declaration_type_logger << "analysing declaration specifiers\n" << msg::eolog;
	
	// analyse the declaration specifiers
	ptr<sa_declaration_specifiers> sads =
		sa_declaration_specifier_list::create()->process(decl->location_get(),decl->declaration_specifiers_get());

	sa_param_declaration_type_logger << "checking specifiers\n" << msg::eolog;
	// check disallowed declaration specifiers
	if (sads->virtual_flag_get() || sads->explicit_flag_get() || sads->friend_flag_get() ||
		 sads->inline_flag_get() || sads->storage_class_get() != ss_storage_class::ST_NONE) {
		// TODO pt report error: invalid specifier
		sa_param_declaration_type_logger << "invalid specifier flag\n" << msg::eolog;
	}
		 
	sa_param_declaration_type_logger << "analysing the declarator\n" << msg::eolog;
		
	// get the type from the declarator	
	ptr<sa_declarator_type> sadt = sa_declarator_type::create();
	type = sadt->process(sads->type_get(),decl->declarator_get());

	sa_param_declaration_type_logger << "normalizing type\n" << msg::eolog;

	// normalize the type of the parameter
	type = ss_type2param_type::create()->process(type);

	sa_param_declaration_type_logger << "checking void type\n" << msg::eolog;
	
	// check void
	if (type->is_void()) {
		sa_param_declaration_type_logger << "the type is void\n" << msg::eolog;
		// TODO pt report error: parameter type is void
		type = ss_type_sint::instance();
	}
	
	sa_param_declaration_type_logger << "sa_param_declaration_type::process() end\n" << msg::eolog;
}

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);

