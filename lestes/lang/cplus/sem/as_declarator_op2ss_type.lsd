<?xml version="1.0" encoding="iso-8859-1"?>
<!--
	The lestes compiler suite
	Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
	Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
	Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
	Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
	Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
	Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
	Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; version 2 of the License.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	See the full text of the GNU General Public License version 2, and
	the limitations in the file doc/LICENSE.

	By accepting the license the licensee waives any and all claims
	against the copyright holder(s) related in whole or in part to the
	work, its use, and/or the inability to use it.
-->
<lsd xmlns="http://lestes.jikos.cz/schemas/lsd" xmlns:h="http://www.w3.org/TR/REC-html40">

	<file-name>as_declarator_op2ss_type</file-name>

	<dox file="both">
		<bri>Declarator operator visitor.</bri>
		<det>Visitor transforming declarator operator to ss_type.</det>
	</dox>

	<packages>
		<p>lestes</p>
		<p>lang</p>
		<p>cplus</p>
		<p>sem</p>
	</packages>

	<imports>
		<i>lestes/common.hh</i>
		<i>lestes/std/list.hh</i>
		<i>lestes/lang/cplus/sem/as_declarator_op_visitor.v.g.hh</i>
	</imports>

	<implementation-imports>
		<i>lestes/lang/cplus/sem/as_decl.g.hh</i> 
		<i>lestes/lang/cplus/sem/ss_type.g.hh</i>
	</implementation-imports>

	<foreign-class name="ss_type">
		<p>lestes</p>
		<p>lang</p>
		<p>cplus</p>
		<p>sem</p>
	</foreign-class>

	<foreign-class name="as_declarator_op">
		<p>lestes</p>
		<p>lang</p>
		<p>cplus</p>
		<p>sem</p>
	</foreign-class>

	<using-class name="object" packages="::lestes::std::"/>

	<include href="as_declarator_op_visitor.v.lsd" />

	<xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="as_declarator_op_visitor.v.lsd">
		<xi:fallback/>
	</xi:include>

	<class name="as_declarator_op2ss_type" base="as_declarator_op_visitor">
		<dox>
			<bri>Declarator operator visitor.</bri>
			<det>Incrementaly forms ss_type from visited declarator operators.</det>
		</dox>
		<declare-abstract-methods prefix="visit_" skip-defined="yes" />
		<method name="process" type="void">
			<param name="dop" type="as_declarator_op" />
			<dox>
				<bri>Processes the declarator operator.</bri>
			</dox>
		</method>
		<field name="type" type="ss_type" alt="none" init="void">
			<dox>
				<bri>The ananysed type.</bri>
				<det>The analysed SS type formed from the declarator operator.</det>
			</dox>
		</field>
		<!--
		<collection name="parameters" kind="list" type="ss_parameter_declaration" set="none" alt="none" check="" init="NULL">
			<dox>
				<bri>The parameters and also a flag of function declaration.</bri>
			</dox>
		</collection>
		<field name="last_flag" type="bool" set="none" alt="none" init="false">
			<dox>
				<bri>The internal flag for processing the last declarator operator.</bri>
			</dox>
		</field>
		<method name="process" type="void">
			<param name="dop" type="as_declarator_op" />
			<param name="last" type="bool" />
			<dox>
				<bri>Processes the declarator operator.</bri>
			</dox>
		</method>
		-->
	</class>
	
	<!--

	<class name="as_declarator_op2ss_type_param" base="as_declarator_op2ss_type">
		<dox>
			<bri>Declarator operator visitor for parameters.</bri>
			<det>Incrementaly forms ss_type from visited declarator operator.</det>
		</dox>
		<method name="visit_as_declarator_op_func" type="void">
			<dox>
				<bri>Visits the function declarator operator.</bri>
			</dox>
			<param name="dop" type="as_declarator_op_func"/>
		</method>
	</class>

	<class name="as_declarator_op2ss_type_plain" base="as_declarator_op2ss_type">
		<dox>
			<bri>Declarator operator visitor for plain types.</bri>
			<det>Incrementaly forms ss_type from visited declarator operator.</det>
		</dox>
		<method name="visit_as_declarator_op_func" type="void">
			<dox>
				<bri>Visits the function declarator operator.</bri>
			</dox>
			<param name="dop" type="as_declarator_op_func"/>
		</method>
	</class>
	-->

</lsd>
