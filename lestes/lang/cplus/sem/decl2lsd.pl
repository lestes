#! /usr/bin/perl -P

#  A Bison parser, made from idecl2lsd.y
#  by  GNU Bison version 1.25 Perl modifications v1.0


# Identify Bison output.
#define YYBISON 1

sub	ID () { 258; }
sub	COLON () { 259; }
sub	LBR () { 260; }
sub	RBR () { 261; }
sub	ELIPS () { 262; }
sub	MIN () { 263; }
sub	COMMA () { 264; }
sub	BRIEFDOC () { 265; }
sub	DETAILEDDOC () { 266; }
sub	PLUS () { 267; }
sub	LPAR () { 268; }
sub	RPAR () { 269; }
sub	SEMIC () { 270; }

#line 15 "idecl2lsd.y"

#define YYDEBUG 1
# use warnings;
sub yylex;
#define YYDEBUG 1
#define YYPRINT(a,b,c) print a " ", c
#ifndef YYSTYPE
#define YYSTYPE int
#endif
#ifndef __cplusplus
#ifndef __STDC__
#define const
#endif
#endif



#define	YYFINAL		62
#define	YYFLAG		-32768
#define	YYNTBASE	16

#define YYTRANSLATE(x) ((x) <= 270 ? $yytranslate[x] : 29)

@yytranslate = (     0,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     1,     2,     3,     4,     5,
     6,     7,     8,     9,    10,    11,    12,    13,    14,    15
);

#if YYDEBUG != 0
@yyprhs = (     0,
     0,     1,     4,     5,     7,     9,    12,    14,    19,    25,
    30,    36,    42,    43,    45,    47,    51,    55,    60,    66,
    72,    79,    80,    82,    84,    88,    92,    95,    99,   101,
   106,   111
);

@yyrhs = (    -1,
    16,    20,     0,     0,    18,     0,    10,     0,    10,    11,
     0,    11,     0,    17,     3,     4,     3,     0,    17,     8,
     3,     4,     3,     0,    19,     5,    21,     6,     0,    19,
     5,    22,     9,     6,     0,    19,     5,    22,    15,     6,
     0,     0,    22,     0,    28,     0,    22,     9,    28,     0,
    22,    15,    28,     0,    17,     3,     3,    13,     0,    17,
     8,     3,     3,    13,     0,    17,    12,     3,     3,    13,
     0,    17,    12,     8,     3,     3,    13,     0,     0,    25,
     0,    26,     0,    25,     9,    26,     0,     3,     4,     3,
     0,     3,     3,     0,    23,    24,    14,     0,    27,     0,
    17,     3,     4,     3,     0,    17,     3,     7,     3,     0,
    17,     8,     3,     4,     3,     0
);

#endif

#if YYDEBUG != 0

@yyrline = ( 0,
    27,    29,    32,    35,    38,    41,    43,    46,    52,    58,
    61,    63,    66,    68,    70,    72,    73,    76,    82,    87,
    92,    98,   100,   102,   104,   106,   111,   115,   121,   129,
   135,   141
);
#endif


#if YYDEBUG != 0 || defined (YYERROR_VERBOSE)


@yytname = (   '$','error','$undefined.','ID','COLON',
'LBR','RBR','ELIPS','MIN','COMMA','BRIEFDOC','DETAILEDDOC','PLUS','LPAR','RPAR',
'SEMIC','gd','doc_opt','doc','ch','cd','flds','flds2','method_head','params',
'params2','param','method','fld', 'NULL'
);
#endif


@yyr1 = (     0,
    16,    16,    17,    17,    18,    18,    18,    19,    19,    20,
    20,    20,    21,    21,    22,    22,    22,    23,    23,    23,
    23,    24,    24,    25,    25,    26,    26,    27,    28,    28,
    28,    28
);

@yyr2 = (     0,
     0,     2,     0,     1,     1,     2,     1,     4,     5,     4,
     5,     5,     0,     1,     1,     3,     3,     4,     5,     5,
     6,     0,     1,     1,     3,     3,     2,     3,     1,     4,
     4,     5
);

@yydefact = (     1,
     3,     5,     7,     0,     4,     0,     2,     6,     0,     0,
     3,     0,     0,     0,     0,    14,    22,    29,    15,     8,
     0,     0,     0,     0,    10,     3,     3,     0,     0,    23,
    24,     9,     0,     0,     0,     0,     0,     0,    11,    16,
    12,    17,    27,     0,    28,     0,    18,    30,    31,     0,
     0,     0,     0,    26,    25,    19,    32,    20,     0,    21,
     0,     0
);

@yydefgoto = (     1,
    14,     5,     6,     7,    15,    16,    17,    29,    30,    31,
    18,    19
);

@yypact = (-32768,
     0,    -6,-32768,    18,-32768,     3,-32768,-32768,    11,    20,
     6,    29,    31,     1,    30,    -8,    34,-32768,-32768,-32768,
    35,    -1,    36,    19,-32768,     8,    14,    25,    26,    32,
-32768,-32768,    33,    39,    40,    27,    41,    42,-32768,-32768,
-32768,-32768,-32768,    44,-32768,    34,-32768,-32768,-32768,    37,
    45,    38,    46,-32768,-32768,-32768,-32768,-32768,    43,-32768,
    52,-32768
);

@yypgoto = (-32768,
    53,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,     9,
-32768,     7
);


#define	YYLAST		56


@yytable = (    61,
    26,    33,    34,    22,     8,    35,    27,    11,    23,     2,
     3,   -13,    24,    39,    12,     2,     3,     2,     3,    41,
     9,    37,    13,     2,     3,    10,    38,    43,    44,    50,
    51,    20,    40,    42,    21,    25,    28,    32,    36,    45,
    46,    48,    49,    52,    53,    47,    54,    57,    59,    56,
    58,    62,     0,     4,    55,    60
);

@yycheck = (     0,
     9,     3,     4,     3,    11,     7,    15,     5,     8,    10,
    11,     6,    12,     6,     4,    10,    11,    10,    11,     6,
     3,     3,     3,    10,    11,     8,     8,     3,     4,     3,
     4,     3,    26,    27,     4,     6,     3,     3,     3,    14,
     9,     3,     3,     3,     3,    13,     3,     3,     3,    13,
    13,     0,    -1,     1,    46,    13
);
# This is happy joyful -*-perl-*- code!!
#line 3 "/home/tma/usr/share/bison.simple.perl"

#    Skeleton output parser for bison,
#    Copyright (C) 1984, 1989, 1990 Free Software Foundation, Inc.

#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2, or (at your option)
#    any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#    As a special exception, when this file is copied by Bison into a
#    Bison output file, you may use that output file without restriction.
#    This special exception was added by the Free Software Foundation
#    in version 1.24 of Bison.

#    This is the parser code that is written into each bison parser
#    when the %semantic_parser declaration is not specified in the grammar.
#    It was written by Richard Stallman by simplifying the hairy parser
#    used when %semantic_parser is specified.

#    Note: there must be only one dollar sign in this file.
#    It is replaced by the list of actions, each action
#    as one case of the switch.

#define yyerrok		($yyerrstatus = 0)
#define yyclearin	($yychar = YYEMPTY)
#define YYEMPTY		-2
#define YYEOF		0
#define YYACCEPT	return(0)
#define YYABORT 	return(1)
#define YYERROR		{ goto yyerrlab1; }
#    Like YYERROR except do call yyerror.
#    This remains here temporarily to ease the
#    transition to the new meaning of YYERROR, for GCC.
#    Once GCC version 2 has supplanted version 1, this can go.
#define YYFAIL		{ goto yyerrlab; }
#define YYRECOVERING()  (!!yyerrstatus)
#define YYBACKUP(token, value) \
do								\
    if ($yychar == YYEMPTY && $yylen == 1)			\
{ $yychar = (token), $yylval = (value);			\
      $yychar1 = YYTRANSLATE ($yychar);				\
	  YYPOPSTACK;						\
	      goto yybackup;						\
	      }								\
    else								\
{ yyerror("syntax error: cannot back up"); YYERROR; }	\
    while (0)

#define YYTERROR	1
#define YYERRCODE	256

#ifndef YYPURE
#define YYLEX		yylex()
#endif

#ifdef YYPURE
#ifdef YYLSP_NEEDED
#ifdef YYLEX_PARAM
#define YYLEX		yylex(\$yylval, \$yylloc, YYLEX_PARAM)
#else
#define YYLEX		yylex(\$yylval, \$yylloc)
#endif
#else
#ifdef YYLEX_PARAM
#define YYLEX		yylex(\$yylval, YYLEX_PARAM)
#else
#define YYLEX		yylex(\$yylval)
#endif
#endif
#endif

# If nonreentrant, generate the variables here

#ifndef YYPURE

    local($yychar);			#  the lookahead symbol		
local($yylval);			#  the semantic value of the	
#  lookahead symbol		

#ifdef YYLSP_NEEDED
local($yylloc);			#  location data for the lookahead
#  symbol				
#endif

local($yynerrs);		#  number of parse errors so far       
#endif
# not YYPURE

#if YYDEBUG != 0
local($yydebug);		#  nonzero means print parse trace
# Since this is uninitialized, it does not stop multiple parsers
#   from coexisting.
#endif


#line 109 "/home/tma/usr/share/bison.simple.perl"

sub yyparse
{
    my $yystate;
    my $yyn;
    my $yyssp;
    my $yyvsp;
    my $yyerrstatus;    #  number of tokens to shift before error messages enabled
    my $yychar1 = 0;	#  lookahead token as an internal (translated) token number
    
    my @yyssa;	        #  the state stack			
    my @yyvsa;	        #  the semantic value stack		

    my $yyss = \@yyssa; # refer to the stacks thru separate pointers
    my $yyvs = \@yyvsa; #  to allow yyoverflow to reallocate them elsewhere 

#ifdef YYLSP_NEEDED
    my @yylsa;	        #  the location stack			
    my $yyls = \@yylsa;
    my $yylsp;

#define YYPOPSTACK   ($yyvsp--, $yyssp--, $yylsp--)
#else
#define YYPOPSTACK   ($yyvsp--, $yyssp--)
#endif

#ifdef YYPURE
    my $yychar;
    my $yylval;
    my $yynerrs;
#ifdef YYLSP_NEEDED
    my $yylloc;
#endif
#endif

    my $yyval;  		#  the variable used to return		
    #  semantic values from the action	
    #  routines				

    my $yylen;

#if YYDEBUG != 0
    print STDERR "Starting parse\n" if $yydebug;
#endif

    $yystate = 0;
    $yyerrstatus = 0;
    $yynerrs = 0;
    $yychar = YYEMPTY;		# Cause a token to be read.  

    # Initialize stack pointers.
    #  Waste one element of value and location stack
    #  so that they stay on the same level as the state stack.
    #  The wasted elements are never initialized. 

    $yyssp = -1;
    $yyvsp = 0;
#ifdef YYLSP_NEEDED
    $yylsp = 0;
#endif

# Push a new state, which is found in  yystate  . 
# In all cases, when you get here, the value and location stacks
#   have just been pushed. so pushing a state here evens the stacks.
yynewstate:

    $yyss[++$yyssp] = $yystate;

#if YYDEBUG != 0
    print STDERR "Entering state $yystate\n" if $yydebug;
#endif

yybackup:

# Do appropriate processing given the current state.
# Read a lookahead token if we need one and do not already have one.
# yyresume:

    # First try to decide what to do without reference to lookahead token.

    $yyn = $yypact[$yystate];
    if ($yyn == YYFLAG) {
	goto yydefault;
    }

    # Not known => get a lookahead token if do not already have one.

    # yychar is either YYEMPTY or YYEOF
    # or a valid token in external form. 

    if ($yychar == YYEMPTY)
    {
#if YYDEBUG != 0
	print STDERR "Reading a token: " if $yydebug;
#endif
	$yychar = YYLEX;
    }

    # Convert token to internal form (in yychar1) for indexing tables with

    if ($yychar <= 0)		# This means end of input.
    {
	$yychar1 = 0;
	$yychar = YYEOF;		# do not call YYLEX any more

#if YYDEBUG != 0
	printf STDERR "Now at end of input.\n" if $yydebug;
#endif
    }
    else
    {
	$yychar1 = YYTRANSLATE($yychar);

#if YYDEBUG != 0
	if ($yydebug)
	{
	    print STDERR "Next token is $yychar ($yytname[$yychar1]";
	    # Give the individual parser a way to print the precise meaning
	    # of a token, for further debugging info.
#ifdef YYPRINT
	    YYPRINT (STDERR, $yychar, $yylval);
#endif
	    print STDERR ")\n";
	}
#endif
    }

    $yyn += $yychar1;
    if ($yyn < 0 || $yyn > YYLAST || $yycheck[$yyn] != $yychar1) {
	goto yydefault;
    }

    $yyn = $yytable[$yyn];

#    yyn is what to do for this token type in this state.
#      Negative => reduce, -yyn is rule number.
#      Positive => shift, yyn is new state.
#        New state is final state => do not bother to shift,
#        just return success.
#      zero, or most negative number => error.

    if ($yyn < 0) {
	goto yyerrlab if ($yyn == YYFLAG);
	$yyn = -$yyn;
	goto yyreduce;
    } elsif ($yyn == 0) {
	goto yyerrlab;
    }

    if ($yyn == YYFINAL) {
	YYACCEPT;
    }

    # Shift the lookahead token.

#if YYDEBUG != 0
    print STDERR "Shifting token $yychar ($yytname[$yychar1]), " if $yydebug;
#endif

    # Discard the token being shifted unless it is eof.
    if ($yychar != YYEOF) {
	$yychar = YYEMPTY;
    }

    $yyvs[++$yyvsp] = $yylval;
#ifdef YYLSP_NEEDED
    $yyls[++$yylsp] = $yylloc;
#endif

    # count tokens shifted since error; after three, turn off error status.
    $yyerrstatus-- if $yyerrstatus;

    $yystate = $yyn;
    goto yynewstate;

# Do the default action for the current state.
yydefault:

    $yyn = $yydefact[$yystate];
    goto yyerrlab if ($yyn == 0);

# Do a reduction.  yyn is the number of a rule to reduce with.
yyreduce:
    $yylen = $yyr2[$yyn];
    if ($yylen > 0) {
	$yyval = $yyvs[$yyvsp + 1 - $yylen]; # implement default value of the action
    }

#if YYDEBUG != 0
    if ($yydebug)
    {
	my $i;

	print STDERR "Reducing via rule $yyn (line $yyrline[$yyn]), ";

	# Print the symbols being reduced, and their result.
        for ($i = $yyprhs[$yyn]; $yyrhs[$i] > 0; $i++) {
	    print STDERR "$yytname[$yyrhs[$i]] ";
	}
	print STDERR " -> $yytname[$yyr1[$yyn]]\n";
    }
#endif

    
  if (0) {
  } elsif ($yyn == 3) {
#line 34 "idecl2lsd.y"
{ $yyval = ""; ;
}
  } elsif ($yyn == 4) {
#line 36 "idecl2lsd.y"
{ $yyval = $yyvs[$yyvsp+0]; ;
}
  } elsif ($yyn == 5) {
#line 40 "idecl2lsd.y"
{ $yyval = "<1\t<dox>\n<2\t<bri>$yyvs[$yyvsp+0]</bri>\n<1\t</dox>\n"; ;
}
  } elsif ($yyn == 6) {
#line 42 "idecl2lsd.y"
{ $yyval = "<1\t<dox>\n<2\t<bri>$yyvs[$yyvsp+-1]</bri>\n<2\t<det>$yyvs[$yyvsp+0]</det>\n<1\t</dox>\n"; ;
}
  } elsif ($yyn == 7) {
#line 44 "idecl2lsd.y"
{ $yyval = "<1\t<dox>\n<2\t<det>$yyvs[$yyvsp+0]</det>\n<1\t</dox>\n"; ;
}
  } elsif ($yyn == 8) {
#line 48 "idecl2lsd.y"
{
		print "\t<class name=\"$yyvs[$yyvsp+-2]\" base=\"$yyvs[$yyvsp+0]\">\n";
		printdoc2($yyvs[$yyvsp+-3]);
	  ;
}
  } elsif ($yyn == 9) {
#line 53 "idecl2lsd.y"
{
		print "\t<class name=\"$yyvs[$yyvsp+-2]\" base=\"$yyvs[$yyvsp+0]\" abstract=\"yes\">\n";
		printdoc2($yyvs[$yyvsp+-4]);
	  ;
}
  } elsif ($yyn == 10) {
#line 60 "idecl2lsd.y"
{ print "\t</class>\n\n"; ;
}
  } elsif ($yyn == 11) {
#line 62 "idecl2lsd.y"
{ print "\t</class>\n\n"; ;
}
  } elsif ($yyn == 12) {
#line 64 "idecl2lsd.y"
{ print "\t</class>\n\n"; ;
}
  } elsif ($yyn == 18) {
#line 78 "idecl2lsd.y"
{
		print "\t\t<method name=\"$yyvs[$yyvsp+-1]\" type=\"$yyvs[$yyvsp+-2]\">\n";
		printdoc3($yyvs[$yyvsp+-3]);
	  ;
}
  } elsif ($yyn == 19) {
#line 83 "idecl2lsd.y"
{
		print "\t\t<method name=\"$yyvs[$yyvsp+-1]\" type=\"$yyvs[$yyvsp+-2]\" qualifier=\"abstract\" specifier=\"virtual\">\n";
		printdoc3($yyvs[$yyvsp+-4]);
	  ;
}
  } elsif ($yyn == 20) {
#line 88 "idecl2lsd.y"
{
		print "\t\t<method name=\"$yyvs[$yyvsp+-1]\" type=\"$yyvs[$yyvsp+-2]\" specifier=\"static\">\n";
		printdoc3($yyvs[$yyvsp+-4]);
	  ;
}
  } elsif ($yyn == 21) {
#line 93 "idecl2lsd.y"
{
		print "\t\t<method name=\"$yyvs[$yyvsp+-1]\" type=\"$yyvs[$yyvsp+-2]\" specifier=\"virtual\">\n";
		printdoc3($yyvs[$yyvsp+-5]);
	  ;
}
  } elsif ($yyn == 26) {
#line 108 "idecl2lsd.y"
{
		print "\t\t\t<param name=\"$yyvs[$yyvsp+-2]\" type=\"$yyvs[$yyvsp+0]\" />\n";
	  ;
}
  } elsif ($yyn == 27) {
#line 112 "idecl2lsd.y"
{
		print "\t\t\t<param name=\"$yyvs[$yyvsp+0]\" type=\"$yyvs[$yyvsp+-1]\" />\n";
	  ;
}
  } elsif ($yyn == 28) {
#line 117 "idecl2lsd.y"
{
		print "\t\t</method>\n";
	  ;
}
  } elsif ($yyn == 30) {
#line 130 "idecl2lsd.y"
{
		print "\t\t<field name=\"$yyvs[$yyvsp+-2]\" type=\"$yyvs[$yyvsp+0]\">\n";
		printdoc3($yyvs[$yyvsp+-3]);
		print "\t\t</field>\n";
	  ;
}
  } elsif ($yyn == 31) {
#line 136 "idecl2lsd.y"
{
		print "\t\t<collection name=\"$yyvs[$yyvsp+-2]\" type=\"$yyvs[$yyvsp+0]\">\n";
		printdoc3($yyvs[$yyvsp+-3]);
		print "\t\t</collection>\n";
	  ;
}
  } elsif ($yyn == 32) {
#line 142 "idecl2lsd.y"
{
		print "\t\t<field name=\"$yyvs[$yyvsp+-2]\" type=\"$yyvs[$yyvsp+0]\" check=\"\">\n";
		printdoc3($yyvs[$yyvsp+-4]);
		print "\t\t</field>\n";
	  ;
}

}
   # the action file gets copied in in place of this line

#line 314 "/home/tma/usr/share/bison.simple.perl"

	$yyvsp -= $yylen;
    $yyssp -= $yylen;
#ifdef YYLSP_NEEDED
    $yylsp -= $yylen;
#endif

#if YYDEBUG != 0
    print STDERR "state stack now ",join(" ",@yyss[0..$yyssp+1]),"\n" 
	if $yydebug;
#endif

    $yyvs[++$yyvsp] = $yyval;

#ifdef YYLSP_NEEDED
    $yylsp++;
    if ($yylen == 0) {
	$yyls[$yylsp] = $yylloc;
	$yyls[$yylsp]->{text} = '';
    }
    else
    {
	$yyls[$yylsp]->{last_line} = $yyls[$yylsp+$yylen-1]->{last_line};
	$yyls[$yylsp]->{last_column} = $yyls[$yylsp+$yylen-1]->{last_column};
    }
#endif

#      Now "shift" the result of the reduction.
#      Determine what state that goes to,
#      based on the state we popped back to
#      and the rule number reduced by.

    $yyn = $yyr1[$yyn];

    $yystate = $yypgoto[$yyn - YYNTBASE] + $yyss[$yyssp];
    if ($yystate >= 0 && $yystate <= YYLAST && $yycheck[$yystate] == $yyss[$yyssp]) {
	$yystate = $yytable[$yystate];
    } else {
	$yystate = $yydefgoto[$yyn - YYNTBASE];
    }

    goto yynewstate;

yyerrlab:   # here on detecting error

    if (! $yyerrstatus)
	# If not already recovering from an error, report this error.
    {
	++$yynerrs;

#ifdef YYERROR_VERBOSE
	$yyn = $yypact[$yystate];

	if ($yyn > YYFLAG && $yyn < YYLAST)
	{
	    my $size = 0;
	    my $msg;
	    my ($x, $count);

	    $count = 0;
	    # Start X at -yyn if nec to avoid negative indexes in yycheck.
	    for ($x = ($yyn < 0 ? -$yyn : 0);
		 $x < $yytname_size; $x++) {
		if ($yycheck[$x + $yyn] == $x) {
		    $size += length($yytname[$x]) + 15, $count++;
		}
	    }

	    $msg = "parse error";

	    if ($count < 5)
	    {
		$count = 0;
		for ($x = ($yyn < 0 ? -$yyn : 0);
		     $x < $yytname_size; x++) {
		    if ($yycheck[$x + $yyn] == $x)
		    {
			$msg .= $count == 0 ? ", expecting `" : " or `";
			$msg .= $ttyname[$x];
			$msg .= "'";
			$count++;
		    }
		}
	    }
	    yyerror($msg);
	}
	else
#endif
# YYERROR_VERBOSE
	    yyerror("parse error");
    }

    goto yyerrlab1;
yyerrlab1:   # here on error raised explicitly by an action

    if ($yyerrstatus == 3)
    {
	# _if just tried and failed to reuse lookahead token after an error, discard it.

	# return failure if at end of input
	if ($yychar == YYEOF) {
	    YYABORT;
	}

#if YYDEBUG != 0
	if ($yydebug) {
	    print STDERR "Discarding token $yychar ($yytname[$yychar1]).\n";
	}
#endif

	$yychar = YYEMPTY;
    }

    # _Else will try to reuse lookahead token
    #   after shifting the error token.

    $yyerrstatus = 3;		# Each real token shifted decrements this

    goto yyerrhandle;

yyerrdefault:  # current state does not do anything special for the error token.

    1;

yyerrpop:   # pop the current state because it cannot handle the error token

    YYABORT if ($yyssp == 0); 
    $yyvsp--;
    $yystate = $yyss[--$yyssp];
#ifdef YYLSP_NEEDED
    $yylsp--;
#endif

#if YYDEBUG != 0
    print STDERR "state stack now",join(" ",@yyss[0..$yyssp+1]),"\n" 
	if $yydebug;
#endif

yyerrhandle:

    $yyn = $yypact[$yystate];
    goto yyerrdefault if ($yyn == YYFLAG);

    $yyn += YYTERROR;
    if ($yyn < 0 || $yyn > YYLAST || $yycheck[$yyn] != YYTERROR) {
	goto yyerrdefault;
    }

    $yyn = $yytable[$yyn];
    if ($yyn < 0)
    {
	if ($yyn == YYFLAG) {
	    goto yyerrpop;
	}
	$yyn = -$yyn;
	goto yyreduce;
    }
    elsif ($yyn == 0) {
	goto yyerrpop;
    }

    YYACCEPT if ($yyn == YYFINAL);

#if YYDEBUG != 0
    print "Shifting error token, " if $yydebug;
#endif

    $yyvs[++$yyvsp] = $yylval;
#ifdef YYLSP_NEEDED
    $yyls[++$yylsp] = $yylloc;
#endif

    $yystate = $yyn;
    goto yynewstate;
}
#line 149 "idecl2lsd.y"


@tokq = ();

sub yyerror { print @_, "\n"; }

sub yylex
{
	local $_;
	until (@tokq) {
		$_ = <>;
		return YYEOF unless defined $_;
		chomp;
		@tokq = map { (/^(\s*|\/\*)$/o || $_ eq '') ? () : ($_); }
			split /(\s+|[:,;()+]|-|\[|\]|\.\.\.|\/[b*d](?:[^bd*]|[bd*][^\/])*[b*d]\/)/o, $_;
	}
	$yylval = $_ = shift @tokq;
	
	# print "got: $_\n";
	if (/\/[bd]/) {
		$yylval =~ s/\/[bd]\s?//o;
		$yylval =~ s/\s?[b*d]\///o;
		return BRIEFDOC if /\/b/;
		return DETAILEDDOC;
	}
	return LBR if /\[/;
	return RBR if /\]/;
	return COLON if /:/;
	return MIN if /-/;
	return COMMA if /,/;
	return ELIPS if /\.\.\./;
	return LPAR if /\(/;
	return RPAR if /\)/;
	return PLUS if /\+/;
	return SEMIC if /;/;
	return ID;
}

sub printdoc2($)
{
	local $_ = shift;
	s/<1\t/\t\t/go;
	s/<2\t/\t\t\t/go;
	print $_;
}
sub printdoc3($)
{
	local $_ = shift;
	s/<1\t/\t\t\t/go;
	s/<2\t/\t\t\t\t/go;
	print $_;
}

{ # local $yydebug = 1;
yyparse();
}
