/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/lang/cplus/lex/lex_literal.g.hh>
#include <lestes/lang/cplus/lex/cpp_token.hh>
#include <lestes/lang/cplus/lex/token_value.hh>
#include <lestes/lang/cplus/sem/lex_literal_to_ss_literal_info.g.hh>
#include <lestes/lang/cplus/sem/lex_literal_to_ss_literal_info.m.hh>
#include <lestes/lang/cplus/sem/ss_literal_info.g.hh>
#include <lestes/lang/cplus/sem/ss_type.g.hh>
#include <lestes/lang/cplus/sem/ss_type_builtin.g.hh>
#include <lestes/md/types/type_info.g.hh>

package(lestes);
package(lang);
package(cplus);
package(sem);

using namespace ::lestes::md::types;

ptr< ::lestes::lang::cplus::sem::ss_literal_info > lex_literal_to_ss_literal_info::process_token(ptr < ::lestes::lang::cplus::lex::cpp_token > tok)
{
	lassert2(tok, "Cannot convert NULL token to ss_literal_info.");
	return lex_literal_to_ss_literal_info::create(tok)->process(tok->literal_get());
}

void lex_literal_to_ss_literal_info::visit_lex_integral_literal(ptr< lex::lex_integral_literal > ili)
{
	ptr < ss_type > type;
	switch (ili->suffix_get()) {
	case lex::lex_integral_literal::NONE:
	{
		if (ili->base_get() == lex::lex_integral_literal::DECIMAL) {
			// FIFI: signed int, signed long int
			ucn_string val = token->value_get()->content_get();
			if (type_info::instance()->does_value_match_type(val, ss_type_sint::instance())) {
				type = ss_type_sint::instance();
			} else if (type_info::instance()->does_value_match_type(val, ss_type_slong::instance())) {
				type = ss_type_slong::instance();
			} else {
				// FIXME: error: [2.13.1/3]
				report << integer_literal_cannot_be_represented << val << token->location_get();
				type = ss_type_sint::instance();
			}
		} else {
			ucn_string val = token->value_get()->content_get();
			// FIFI: int, unsigned int, long int, unsigned long int
			if (type_info::instance()->does_value_match_type(val, ss_type_sint::instance())) {
				type = ss_type_sint::instance();
			} else if (type_info::instance()->does_value_match_type(val, ss_type_uint::instance())) {
				type = ss_type_uint::instance();
			} else if (type_info::instance()->does_value_match_type(val, ss_type_slong::instance())) {
				type = ss_type_slong::instance();
			} else if (type_info::instance()->does_value_match_type(val, ss_type_ulong::instance())) {
				type = ss_type_ulong::instance();
			} else {
				// FIXME: error: [2.13.1/3]
				report << integer_literal_cannot_be_represented << val << token->location_get();
				type = ss_type_sint::instance();
			}
		}
		break;
	}
	case lex::lex_integral_literal::UNSIGNED:
	{
		// FIFI: unsigned int, unsigned long int
		ucn_string val(token->value_get()->content_get().begin(), token->value_get()->content_get().begin() + ili->end_get());
		if (type_info::instance()->does_value_match_type(val, ss_type_uint::instance())) {
			type = ss_type_uint::instance();
		} else if (type_info::instance()->does_value_match_type(val, ss_type_ulong::instance())) {
			type = ss_type_ulong::instance();
		} else {
			// FIXME: error: [2.13.1/3]
			report << integer_literal_cannot_be_represented << token->value_get()->content_get() << token->location_get();
			type = ss_type_uint::instance();
		}
		break;
	}
	case lex::lex_integral_literal::LONG:
	{
		// FIFI: signed long int, unsigned long int
		ucn_string val(token->value_get()->content_get().begin(), token->value_get()->content_get().begin() + ili->end_get());
		if (type_info::instance()->does_value_match_type(val, ss_type_slong::instance())) {
			type = ss_type_slong::instance();
		} else if (type_info::instance()->does_value_match_type(val, ss_type_ulong::instance())) {
			type = ss_type_ulong::instance();
		} else {
			// FIXME: error: [2.13.1/3]
			report << integer_literal_cannot_be_represented << token->value_get()->content_get() << token->location_get();
			type = ss_type_slong::instance();
		}
		break;
	}
	case lex::lex_integral_literal::UNSIGNED_LONG:
	{
		type = ss_type_ulong::instance();
		// FIXME: check that fits
		ucn_string val(token->value_get()->content_get().begin(), token->value_get()->content_get().begin() + ili->end_get());
		if (!type_info::instance()->does_value_match_type(val, ss_type_ulong::instance())) {
			// FIXME: error: [2.13.1/3]
			report << integer_literal_cannot_be_represented << token->value_get()->content_get() << token->location_get();
		}
		break;
	}
	default:
		lassert2(false,"You should never get here");
	}
	lassert(type);
	// TODO passes also 0x for hexa and suffixes, cut them ? the base can also be a problem
	result_set(ss_integral_literal_info::create(type, token->value_get()->content_get()));
	//lassert2(false, "FIXME: implement me");
}

void lex_literal_to_ss_literal_info::visit_lex_floating_literal( ptr< lex::lex_floating_literal > a )
{
	(void)a;
	lassert2(false, "FIXME: implement me");
}

void lex_literal_to_ss_literal_info::visit_lex_character_literal( ptr< lex::lex_character_literal > cl )
{
	// TODO pt takes only the last character
	ptr<ss_type> type = cl->wide_flag_get() ? ss_type_wchar_t::instance() : ss_type_pchar::instance();

	ucn_string u = token->value_get()->content_get();
	ulint val = character::extract_value(u[u.length() - 1]);
	
	ptr<ss_literal_info> lit = ss_integral_literal_info::create_from_number(type,val);
	result_set(lit);
}

void lex_literal_to_ss_literal_info::visit_lex_string_literal( ptr< lex::lex_string_literal > sl )
{
	//lassert2(false, "FIXME: implement me");

	ptr < ss_type > type;
	if (sl->wide_flag_get()) {
		type = ss_type_wchar_t::instance();
	} else {
		type = ss_type_pchar::instance();
	}

	ucn_string ucs = token->value_get()->content_get();
	ptr < ss_type > arr_type = ss_array::instance(ucs.size()+1, ss_const::instance(type));

	ptr < list < srp < ss_literal_info > > > c = list < srp < ss_literal_info > >::create();
	ptr < ss_compound_literal_info > cli = ss_compound_literal_info::create(arr_type, c);
	
	ucn_string::iterator it = ucs.begin(), end = ucs.end();

	for ( ; it != end ; ++it ) {
		c->push_back(ss_integral_literal_info::create_from_number(type, character::extract_value(*it)));
	}
	c->push_back(ss_integral_literal_info::create_from_number(type, 0));

	result_set(cli);
	
}

/*!
  The routine uses the different lengths of true/false keywords to distinguish
  between them, without the need to consider any encoding interaction.
 */
void lex_literal_to_ss_literal_info::visit_lex_boolean_literal( ptr< lex::lex_boolean_literal >)
{
	//bool value = token->value_get()->content_get() == "true";
	bool value = token->value_get()->content_get().size() == 4;
	result_set(ss_integral_literal_info::create_from_number(ss_bool::instance(), value));
}

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);

