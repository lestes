/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Normalization of SS types in parameters.

  Definition of visitor performing normalization of ss_type in function parameters.
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/sem/ss_type2param_type.g.hh>
#include <lestes/lang/cplus/sem/ss_type.g.hh>

package(lestes);
package(lang);
package(cplus);
package(sem);

/*!
  Visits object of any class in ss_struct_base hierarchy.
  Does no transformation.
  \param obj  The object to visit.
*/
void ss_type2param_type::default_ss_struct_base(ptr<ss_struct_base> obj)
{
	lassert(obj);
	type = obj;
}

/*!
  Visits object of any class in ss_builtin_type hierarchy.
  Does no transformation.
  \pre obj != NULL
  \param obj  The object to visit.
*/
void ss_type2param_type::default_ss_builtin_type(ptr<ss_builtin_type> obj)
{
	lassert(obj);
	type = obj;
}

/*!
  Visits object of class ss_member_function.
  Not allowed.
  \pre obj != NULL
  \param obj  The object to visit.
*/
void ss_type2param_type::visit_ss_member_function(ptr<ss_member_function> obj)
{
	lassert(obj);
	lassert2(false,"ss_member_function shall not be type of parameter");
}

/*!
  Visits object of class ss_array.
  Transforms the type to pointer to the element type.
  \pre obj != NULL
  \param obj  The object to visit.
*/
void ss_type2param_type::visit_ss_array(ptr<ss_array> obj)
{
	lassert(obj);
	type = ss_pointer::instance(obj->type_get());
}

/*!
  Visits object of class ss_function.
  Transforms the type to pointer to the function.
  \pre obj != NULL
  \param obj  The object to visit.
*/
void ss_type2param_type::visit_ss_function(ptr<ss_function> obj)
{
	lassert(obj);
	// turn the pointer into function
	type = ss_pointer::instance(obj);
}

/*!
  Visits object of class ss_enum.
  Does no transformation.
  \pre obj != NULL
  \param obj  The object to visit.
*/
void ss_type2param_type::visit_ss_enum(ptr<ss_enum> obj)
{
	lassert(obj);
	type = obj;
}

/*!
  Visits object of class ss_const.
  Strips the const qualifier.
  \pre obj != NULL
  \param obj  The object to visit.
*/
void ss_type2param_type::visit_ss_const(ptr<ss_const> obj)
{
	lassert(obj);
	// unconst
	type = obj->what_get();
}

/*!
  Visits object of class ss_volatile.
  Strips the volatile qualifier.
  \pre obj != NULL
  \param obj  The object to visit.
*/
void ss_type2param_type::visit_ss_volatile(ptr<ss_volatile> obj)
{
	lassert(obj);
	// unvolatile
	type = obj->what_get();
}

/*!
  Visits object of class ss_const_volatile.
  Strips the const and volatile qualifiers.
  \pre obj != NULL
  \param obj  The object to visit.
*/
void ss_type2param_type::visit_ss_const_volatile(ptr<ss_const_volatile> obj)
{
	lassert(obj);
	// unconst unvolatile
	type = obj->what_get();
}

/*!
  Visits object of class ss_pointer.
  Does no transformation.
  \pre obj != NULL
  \param obj  The object to visit.
*/
void ss_type2param_type::visit_ss_pointer(ptr<ss_pointer> obj)
{
	lassert(obj);
	type = obj;
}

/*!
  Visits object of class ss_member_pointer.
  Does no transformation.
  \pre obj != NULL
  \param obj  The object to visit.
*/
void ss_type2param_type::visit_ss_member_pointer(ptr<ss_member_pointer> obj)
{
	lassert(obj);
	type = obj;
}

/*!
  Visits object of class ss_reference.
  Sets type information appropriately.
  Does no transformation.
  \pre obj != NULL
  \param obj  The object to visit.
*/
void ss_type2param_type::visit_ss_reference(ptr<ss_reference> obj)
{
	lassert(obj);
	type = obj;
}

/*!
  Visits object of class ss_pseudoreference.
  Should not happen.
  \pre obj != NULL
  \param obj  The object to visit.
*/
void ss_type2param_type::visit_ss_pseudoreference(ptr<ss_pseudoreference> obj)
{
	lassert(obj);
	lassert2(false,"pseudoreferences not allowed in parameter type normalizer");
}

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);
