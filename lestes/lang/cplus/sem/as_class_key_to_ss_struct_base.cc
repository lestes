/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/lang/cplus/sem/as_class_key_to_ss_struct_base.g.hh>
#include <lestes/lang/cplus/sem/as_decl.g.hh>
#include <lestes/lang/cplus/sem/sa_class_declaration.m.hh>
#include <lestes/lang/cplus/sem/sa_loggers.hh>
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>
#include <lestes/lang/cplus/sem/ss_misc.g.hh>
#include <lestes/lang/cplus/sem/ss_type.g.hh>
#include <lestes/msg/logger.hh>
#include <lestes/msg/logger_util.hh>

package(lestes);
package(lang);
package(cplus);
package(sem);

void as_class_key_to_ss_struct_base::class_or_struct()
{
	sa_class_type_logger << "creating ss_class\n" << msg::eolog;
	bool is_POD = true;
	::lestes::std::list < srp < ss_base_specifier > >::iterator it = bases->begin();
	for ( ; is_POD && it != bases->end() ; ++it ) {
		is_POD = is_POD && (*it)->base_class_get()->POD_get();
	}
	sa_class_type_logger << "POD:" << is_POD << '\n' << msg::eolog;
	sa_class_type_logger << "ss_class::create\n" << msg::eolog;
	ptr < ss_class > a_class = ss_class::create(
			decl_seq, /* members */
			ss_decl_seq::root_instance()->declared_by_get(), /* declaration */
			is_POD, /* POD */
			ss_declaration_time::infinity(), /* completition time */
			bases /* bases */
			/* descendants */
			);

	struct_base_set(a_class);
}

void as_class_key_to_ss_struct_base::visit_as_class_key_class(ptr< as_class_key_class >)
{
	class_or_struct();
}

void as_class_key_to_ss_struct_base::visit_as_class_key_struct(ptr< as_class_key_struct >)
{
	class_or_struct();
}

void as_class_key_to_ss_struct_base::visit_as_class_key_union(ptr< as_class_key_union >)
{
	sa_class_type_logger << "creating ss_union\n" << msg::eolog;

	if (!bases->empty()) {
		report << union_shall_not_have_bases << bases->front()->location_get();
	}

	sa_class_type_logger << "ss_union::create\n" << msg::eolog;
	ptr < ss_union > a_class = ss_union::create(
			decl_seq, /* members */
			ss_decl_seq::root_instance()->declared_by_get(), /* declaration */
			true, /* POD */
			ss_declaration_time::infinity() /* completition time */
			);

	struct_base_set(a_class);
}

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);

