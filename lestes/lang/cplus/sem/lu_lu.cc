/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/common.hh>
#include <lestes/lang/cplus/sem/lu_filter.g.hh>
#include <lestes/lang/cplus/sem/lu_logger.hh>
#include <lestes/lang/cplus/sem/lu_lu.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>
#include <lestes/lang/cplus/sem/ss_decl_name.g.hh>
#include <lestes/lang/cplus/sem/ss_misc.g.hh>
#include <lestes/lang/cplus/sem/ss_statement.g.hh>
#include <lestes/lang/cplus/sem/ss_type.g.hh>
#include <lestes/lang/cplus/sem/ss_using_target.g.hh>
#include <lestes/msg/logger.hh>
#include <lestes/std/list.hh>
#include <lestes/std/objectize_macros.hh>
#include <lestes/std/pair.hh>
#include <lestes/std/set.hh>

#include <algorithm>
#include <iterator>

package(lestes);
package(lang);
package(cplus);
package(sem);

declare_logger( lookup_log );
initialize_logger( lookup_log, "lookup", lu_logger );

#define LOOKUP_ASSERTIONS

#ifdef LOOKUP_ASSERTIONS
# define lookup_lassert( x )		lassert( x )
# define lookup_lassert2( x, y )	lassert2( x, y )

static ptr<ss_decl_seq> seq_param;

#else
# define lookup_lassert( x )		(void)0
# define lookup_lassert2( x, y )	(void)0
#endif

typedef list< srp<ss_base_specifier> >	bases_type;
end_package(sem);
end_package(cplus);
end_package(lang);
package(std);
specialize_objectize_nodump( ::lestes::lang::cplus::sem::bases_type::const_iterator );
end_package(std);
package(lang);
package(cplus);
package(sem);

/*!
 * Not reentrant!
 * Never returns NULL.
 */
ptr<declaration_set_type> lu_lookup::main( ptr<ss_decl_seq> seq, ptr<lu_params> params )
{
	lassert( !params_get() && !visited_seqs );
	lassert2( params, "Lookup parameters must not be NULL." );

	visited_seqs_set( decl_seq_set_type::create() );
	params_set( params );

	bool search_parents = params->parent_search_get() == lu_params::SEARCH_PARENTS;
	ptr<declaration_set_type> result;
	do {
		bool not_yet_visited = visited_seqs_get()->insert(seq).second;
		if (not_yet_visited)
			result = deep_scan( seq );
		lassert2( result, "The very first decl_seq has been marked as already visited!" );
		if (seq == ss_decl_seq::root_instance())
			break;
		seq = seq->parent_get();
	} while (result->empty() && search_parents);

	params_set( NULL );
	visited_seqs_set( NULL );
	return result;
}

/*!
 * \brief  Computes union of two given sets of declarations.
 */
static ptr<declaration_set_type> unify(
		const ptr<declaration_set_type> & s1,
		const ptr<declaration_set_type> & s2 )
{
	ptr<declaration_set_type> result = declaration_set_type::create();
	::std::set_union( s1->begin(), s1->end(), s2->begin(), s2->end(),
			::std::inserter( *result, result->end() ) );
	return result;
}

/*!
 * Never returns NULL.
 */
ptr<declaration_set_type> lu_lookup::deep_scan( ptr<ss_decl_seq> starting_seq )
{
	typedef list< srp<ss_decl_seq> > queue_type;
	typedef list< srp<ss_using_directive> > directives_type;
	
	ptr<declaration_set_type> result = declaration_set_type::create();

	ptr<queue_type> queue = queue_type::create();
	// starting_seq is already marked as visited in 'main'
	queue->push_back( starting_seq );

	while (!queue->empty()) {
		ptr<ss_decl_seq> seq = queue->front();
		queue->pop_front();
#ifdef LOOKUP_ASSERTIONS
		// assertions are in the visit methods called from lookup_by_decl()
		seq_param = seq;
#endif
		ptr<declaration_set_type> local_result = lookup_by_decl( seq->declared_by_get() );

		result = unify( result, local_result );
		
		switch (params_get()->using_directives_handling_get()) {
		case lu_params::UDIR_IGNORE:
			continue;
		case lu_params::UDIR_FALLBACK:
			if (!local_result->empty())
				continue;
			/* fall-through */
		case lu_params::UDIR_ALWAYS:
			;
			/* fall-through */
		}

		const ptr<directives_type> & directives = seq->using_directives_get();
		for ( directives_type::const_iterator it = directives->begin(); it != directives->end(); ++it ) {
			/*
			 * XXX:
			 * we assume that all using-directives that follow a late one are late too
			 * therefore we can break
			 */
			if (!(*it)->decl_time_get()->is_before( params_get()->time_get() ))
				break;
			const ptr<ss_decl_seq> & used_seq = (*it)->nspace_get()->body_get();
			bool not_yet_visited = visited_seqs_get()->insert(used_seq).second;
			if (not_yet_visited)
				queue->push_back(used_seq);
		}
	}
	return result;
}

ptr<declaration_set_type> lu_lookup::simple_scan( ptr<ss_decl_seq> seq, ptr<lu_params> params )
{
	ptr<declaration_set_type> result = declaration_set_type::create();
	typedef list< srp<ss_declaration> > contents_type;
	const srp<contents_type> & contents = seq->contents_get();
	
	for ( contents_type::reverse_iterator it = contents->rbegin();
			it != contents->rend(); ++it ) {

		ptr<ss_declaration> decl = *it;
		ptr<ss_declaration> real_decl;

		/* skip declarations that are too young */
		if (!decl->decl_time_get()->is_before( params->time_get() ))
			continue;

		lu_params::using_declarations_handling_type udecl_handling =
			params->using_declarations_handling_get();
		if (udecl_handling != lu_params::UDECL_THROUGH) {
			/* handle using declaration, as requested */
			real_decl = ss_using_target::instance()->process(decl);
			if (decl != real_decl) {
				/* it was ss_using_declaration */
				switch (udecl_handling) {
				case lu_params::UDECL_IGNORE:
					continue;
				case lu_params::UDECL_RESOLVE_BEFORE_FILTER:
					decl = real_decl;
					break;
				default:
					/* udecl_handling == UDECL_RESOLVE_AFTER_FILTER */
					break;
				}
			}
		}

		llog(lookup_log) << "calling filter on decl #" << decl->uid_get() << "\n";

		lu_filter::result_type filt = params->filter_get()->filter(decl);
		
		llog(lookup_log) << "got result " << lu_filter::result2lstring(filt) << "\n";

		if (filt == lu_filter::FR_NO)
			continue;
		
		if (udecl_handling == lu_params::UDECL_RESOLVE_AFTER_FILTER) {
			lassert( real_decl );
			decl = real_decl;
		}

		result->insert( decl );

		if (filt == lu_filter::FR_YES)
			break;
	}
	return result;
}

ptr<declaration_set_type> lu_lookup::internal_simple_scan( ptr<ss_decl_seq> seq )
{
	return simple_scan( seq, params_get() );
}

void lu_lookup::default_action( ptr<ss_declaration> )
{
	lassert2( false, "Decl_seq declared by an unknown type of declaration." );
}

void lu_lookup::visit_ss_namespace_definition( ptr<ss_namespace_definition> nsd )
{
	lookup_lassert( nsd->body_get() == seq_param );
	lookup_result_set( internal_simple_scan( nsd->body_get() ) );
}

void lu_lookup::visit_ss_function_declaration( ptr<ss_function_declaration> fd )
{
	lookup_lassert( fd->parameters_get() == seq_param );
	lookup_result_set( internal_simple_scan( fd->body_get()->decl_seq_get() ) );
}

void lu_lookup::visit_ss_compound_stmt_declaration( ptr<ss_compound_stmt_declaration> csd )
{
	lookup_lassert( csd->compound_stmt_get()->decl_seq_get() == seq_param );
	lookup_result_set( internal_simple_scan( csd->compound_stmt_get()->decl_seq_get() ) );
}

void lu_lookup::visit_ss_structure_declaration( ptr<ss_structure_declaration> td )
{
	lookup_result_set( lu_lookup_in_type::instance()->main( td->type_get(), params_get() ) );
}

ptr<declaration_set_type> lu_lookup_in_type::main( ptr<ss_type> type, ptr<lu_params> params )
{
	lassert( !params_get() );
	params_set( checked(params) );

	ptr<declaration_set_type> result = lookup_by_type( type );

	params_set( NULL );

	return result;
}

ptr<declaration_set_type> lu_lookup_in_type::internal_simple_scan( ptr<ss_decl_seq> seq )
{
	return lu_lookup::simple_scan( seq, params_get() );
}

void lu_lookup_in_type::default_action( ptr<ss_type> )
{
	lassert2( false, "Trying to run lookup on unknown ss_type." );
}

void lu_lookup_in_type::visit_ss_class( ptr<ss_class> c )
{
	ptr<declaration_set_type> result;

	result = internal_simple_scan( c->members_get() );
	if (!result->empty())
		return lookup_result_set( result );

	typedef pair<bases_type::const_iterator,bases_type::const_iterator>
						iter_pair_type;
	typedef list< srp<iter_pair_type> >	iter_pair_list_type;
	// this list is used as a stack
	ptr<iter_pair_list_type> st = iter_pair_list_type::create();

	st->push_back( iter_pair_type::create( c->bases_get()->begin(), c->bases_get()->end() ) );
	while (!st->empty()) {
		bases_type::const_iterator & it = st->back()->first;
		const bases_type::const_iterator & end_it = st->back()->second;

		if (it == end_it) {
			st->pop_back();
			continue;
		}

		ptr<ss_class> base_class = (*it)->base_class_get();
		++it;	// changes the value in the pair, as it is a reference

		ptr<declaration_set_type> local_result = internal_simple_scan( base_class->members_get() );
		if (local_result->empty()) {
			// go one level deeper in the lattice
			st->push_back( iter_pair_type::create(
						base_class->bases_get()->begin(),
						base_class->bases_get()->end() ) );
		} else
			result = unify( result, local_result );
	}

	lookup_result_set( result );
}

void lu_lookup_in_type::visit_ss_union( ptr<ss_union> u )
{
	//XXX: maybe something more sophisticated?
	lookup_result_set( internal_simple_scan( u->members_get() ) );
}

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);
