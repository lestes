/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
// needed by ss_statement, oh the horror... headers that are not self-contained
#include <lestes/std/list.hh>
#include <lestes/lang/cplus/sem/ss_expression.g.hh>
#include <lestes/lang/cplus/sem/ss_statement.g.hh>
#include <lestes/lang/cplus/sem/ss_misc.g.hh>
#include <lestes/std/source_location.hh>
#include <lestes/std/file_info.hh>
#include <iostream>

package(lestes);
package(lang);
package(cplus);
package(sem);

static bool hack_used = false;

/*!
 * As we currently do not have protected setters, a setter for the parent field is not provided.
 * However, when constructing the "global" ss_compound_stmt, which is derived from ss_statement,
 * the parent field has to be set to point to the instance itself, which is impossible to achieve
 * using the factory method.
 *
 * Therefore, this hack is provided. It exists solely for the purpose above.
 * Normal code MUST NOT call it, or it will explode :)
 */
void ss_statement::parent_set_once_hack( ptr<ss_compound_stmt> a_parent )
{
	lassert2( !hack_used, "This method is not meant to be called twice." );
	hack_used = true;
	parent = a_parent;
}

/*!
 * When called for the first time, constructs the grandparent of all instances
 * of ss_compound_stmt. This means that you can simply recognize it, just
 * compare your ptr to the return value of this method.
 *
 * Note that the instance is its own parent. Also note that when creating it,
 * root ss_decl_seq must be created too. This is achieved by calling
 * ss_decl_seq::root_instance(), which might in turn call us back. The code is
 * prepared for this. You can choose which of the two methods you call first.
 *
 * \return  Pointer to the instance of root ss_compound_stmt.
 */
ptr<ss_compound_stmt> ss_compound_stmt::root_instance()
{
	if (!the_root_instance) {
		/* this calls us recursively */
		ptr < ss_decl_seq > root_decl_seq = ss_decl_seq::root_instance();

		/* It is somewhat complicated to tell when the control flow
		 * reaches the following statement.
		 *
		 * Firstly, someone from the ``outer world'' might call
		 * ss_decl_seq::root_instance(). Then the root instance object
		 * of the type ss_decl_seq gets constructed and we get called
		 * after the ss_decl_seq root object already exists. Therefore
		 * it is safe to call ss_decl_seq::root_instance() to get that
		 * object, because the function will return immediately with
		 * the allready constucted, albeit incompletelly initialized
		 * object.
		 *
		 * Secondly, someone from the ``outer world'' might call us in
		 * the first place. Then We would initiate the first ever
		 * ss_decl_seq::root_instance() call. The same applies as for
		 * the first case.
		 *
		 * It is perhaps easiest to state the invariants.
		 * \invariant If ss_decl_seq::root_instance() is called first,
		 * then the ss_decl_seq::the_root_instance is created but not
		 * fully initialized before ss_statement::root_instance() is
		 * called for the first time.
		 * \invariant The ss_decl_seq::root_instance() function will
		 * call ss_statement::root_instance() on its first invocation
		 * only.
		 *
		 * Therefore, it can be concluded, that the following statement
		 * will be executed only after the second (and not necessarily
		 * the first) invocation of ss_decl_seq::root_instance() has
		 * finished.
		 *
		 * The only remaining question is in what state will be the
		 * following statement encountered. The
		 * ss_decl_seq::root_instance() will return a partially
		 * initialized ss_decl_seq instance. If the
		 * ss_decl_seq::root_instance() was called in the first place,
		 * the statement will be executed only once.  If, however, the
		 * ss_statement::root_instance() was called in the first place,
		 * then the statement will be encountered twice.  The condition
		 * will evaluate to false in the first encounter, so the
		 * control flow will proceed to the ss_statement root instance
		 * creation.  The condition will evaluate to true on the second
		 * encounter, because the root instance was created after the
		 * first encounter.
		 */
		if (the_root_instance)
			return the_root_instance;

		/* A non-null location is needed.  We can obtain the location
		 * from the ss_decl_seq's root instance */
		ptr < source_location > loc = root_decl_seq->location_get();
		/* create some sequence points */
		ptr < ss_sp > psp = ss_sp::create(loc, NULL, NULL, 0),
			nsp = ss_sp::create(loc, NULL, NULL, 0),
			dsp = ss_sp::create(loc, NULL, NULL, 0);
		/* link them together */
		psp->nsp_set(dsp);
		dsp->nsp_set(nsp);
		dsp->psp_set(psp);
		nsp->psp_set(dsp);
		the_root_instance = new ss_compound_stmt(
				loc,
				list< srp<ss_label> >::create(), NULL, psp, nsp,
				list< srp<ss_sp> >::create(),
				root_decl_seq,
				list< srp<ss_statement> >::create(), dsp,
				NORMAL
				);
		/* insert sequence points to the list */
		the_root_instance->sequence_points_get()->push_back(psp);
		the_root_instance->sequence_points_get()->push_back(nsp);
		the_root_instance->sequence_points_get()->push_back(dsp);
		/* See documentation for the method above... */
		the_root_instance->parent_set_once_hack( the_root_instance );
		/* we do not set decl_seq when calling new above, as the root decl_seq might not
		 * be created yet. it calls us back, so we avoid recursion by setting it here
		 */
		//the_root_instance->decl_seq = ss_decl_seq::root_instance();
	}
	return the_root_instance;
}

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);
