<?xml version="1.0" encoding="UTF-8"?>
<!--
	The lestes compiler suite
	Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
	Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
	Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
	Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
	Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
	Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
	Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; version 2 of the License.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	See the full text of the GNU General Public License version 2, and
	the limitations in the file doc/LICENSE.

	By accepting the license the licensee waives any and all claims
	against the copyright holder(s) related in whole or in part to the
	work, its use, and/or the inability to use it.
-->
<lsd xmlns="http://lestes.jikos.cz/schemas/lsd" xmlns:h="http://www.w3.org/TR/REC-html40">

	<comment>               Prelude for the whole header file       </comment>

	<dox file="both">
		<bri>Intercode structure for project Lestes</bri>
		<det>
			This file describes a set of classes and data types used for intercode layer -ss-.
			It is an output from a XSLT template which generates C++ code.
		</det>
	</dox>

	<file-name>ss_type_builtin</file-name>
	<imports>
		<i>lestes/common.hh</i>
		<i>lestes/lang/cplus/sem/ss_type.g.hh</i>
	</imports>
	<implementation-imports>
		<i>lestes/lang/cplus/sem/ss_type_visitor.v.g.hh</i>
		<i>lestes/lang/cplus/sem/or_visitor.v.g.hh</i>
		<i>lestes/lang/cplus/sem/ss_declaration_visitor.v.g.hh</i>
		<i>lestes/lang/cplus/sem/ss_decl_name_visitor.v.g.hh</i>
		<i>lestes/md/types/tm_data_type_base.g.hh</i>
	</implementation-imports>
	<packages>
		<p>lestes</p>
		<p>lang</p>
		<p>cplus</p>
		<p>sem</p>
	</packages>
	<hh-header/>

	<hh-footer/>

	<cc-header/>
	<cc-footer />


	<comment> 			BUILTIN TYPES				</comment>

	<forward-class name="ss_builtin_type" />

	<include href="ss_type.lsd"/>
	<xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="ss_type.lsd">
		<xi:fallback/>
	</xi:include>
	<using-class name="ss_type" />
	<default-base type="ss_builtin_type" />

	<default-collection kind="list"/>
	<visitor-file href="visitor.lvd"/>    

	<comment>TODO (signed long long int), (unsigned long long int)</comment>

	<class name="ss_bool" base="ss_builtin_type">
		<dox>
			<bri>Base class for built-in bool type</bri>
			<det>Singleton class.</det>
		</dox>
		<field name="the_instance" specifier="static" type="ss_type" get="none" alt="none" set="none">
			<dox>
				<bri>holder for the class' only instance</bri>
			</dox>
		</field>
		<method name="instance" type="ss_type" specifier="static">
			<dox>
				<bri>get the class' only instance</bri>
			</dox>
		</method>
	</class>

	<class name="ss_void" base="ss_builtin_type">
		<dox>
			<bri>Base class for built-in void type</bri>
			<det>Singleton class.</det>
		</dox>
		<method name="is_void" type="bool" specifier="virtual">
			<dox><bri>Is this type void?</bri>
				<det>Used for genarating pi calls.
					Differentiate between call and callv.
					This case thus return true. TODO define
			</det></dox>
		</method>
		<field name="the_instance" specifier="static" type="ss_type" get="none" alt="none" set="none">
			<dox>
				<bri>holder for the class' only instance</bri>
			</dox>
		</field>
		<method name="instance" type="ss_type" specifier="static">
			<dox>
				<bri>get the class' only instance</bri>
			</dox>
		</method>
	</class>


	<class name="ss_integral" abstract="yes" base="ss_builtin_type">
		<dox>
			<bri>Base class for built-in integral types</bri>
		</dox>
	</class>


	<class name="ss_type_int" abstract="yes" base="ss_integral">
		<dox>
			<bri>Base class for built-in integer types</bri>
		</dox>
	</class>


	<class name="ss_type_sint" base="ss_type_int">
		<dox>
			<bri>Base class for built-in signed integer type</bri>
			<det>Singleton class.</det>
		</dox>
		<field name="the_instance" specifier="static" type="ss_type" get="none" alt="none" set="none">
			<dox>
				<bri>holder for the class' only instance</bri>
			</dox>
		</field>
		<method name="instance" type="ss_type" specifier="static">
			<dox>
				<bri>get the class' only instance</bri>
			</dox>
		</method>
	</class>


	<class name="ss_type_uint" base="ss_type_int">
		<dox>
			<bri>Base class for built-in unsigned integer type</bri>
			<det>Singleton class.</det>
		</dox>
		<field name="the_instance" specifier="static" type="ss_type" get="none" alt="none" set="none">
			<dox>
				<bri>holder for the class' only instance</bri>
			</dox>
		</field>
		<method name="instance" type="ss_type" specifier="static">
			<dox>
				<bri>get the class' only instance</bri>
			</dox>
		</method>
	</class>


	<class name="ss_type_long" abstract="yes" base="ss_integral">
		<dox>
			<bri>Base class for built-in long types</bri>
		</dox>
	</class>


	<class name="ss_type_slong" base="ss_type_long">
		<dox>
			<bri>Base class for built-in signed long type</bri>
			<det>Singleton class.</det>
		</dox>
		<field name="the_instance" specifier="static" type="ss_type" get="none" alt="none" set="none">
			<dox>
				<bri>holder for the class' only instance</bri>
			</dox>
		</field>
		<method name="instance" type="ss_type" specifier="static">
			<dox>
				<bri>get the class' only instance</bri>
			</dox>
		</method>
	</class>


	<class name="ss_type_ulong" base="ss_type_long">
		<dox>
			<bri>Base class for built-in unsigned long type</bri>
			<det>Singleton class.</det>
		</dox>
		<field name="the_instance" specifier="static" type="ss_type" get="none" alt="none" set="none">
			<dox>
				<bri>holder for the class' only instance</bri>
			</dox>
		</field>
		<method name="instance" type="ss_type" specifier="static">
			<dox>
				<bri>get the class' only instance</bri>
			</dox>
		</method>
	</class>


	<class name="ss_type_short" abstract="yes" base="ss_integral">
		<dox>
			<bri>Base class for built-in short types</bri>
		</dox>
	</class>


	<class name="ss_type_sshort" base="ss_type_short">
		<dox>
			<bri>Base class for built-in signed short type</bri>
			<det>Singleton class.</det>
		</dox>
		<field name="the_instance" specifier="static" type="ss_type" get="none" alt="none" set="none">
			<dox>
				<bri>holder for the class' only instance</bri>
			</dox>
		</field>
		<method name="instance" type="ss_type" specifier="static">
			<dox>
				<bri>get the class' only instance</bri>
			</dox>
		</method>
	</class>


	<class name="ss_type_ushort" base="ss_type_short">
		<dox>
			<bri>Base class for built-in unsigned short type</bri>
			<det>Singleton class.</det>
		</dox>
		<field name="the_instance" specifier="static" type="ss_type" get="none" alt="none" set="none">
			<dox>
				<bri>holder for the class' only instance</bri>
			</dox>
		</field>
		<method name="instance" type="ss_type" specifier="static">
			<dox>
				<bri>get the class' only instance</bri>
			</dox>
		</method>
	</class>


	<class name="ss_type_char" abstract="yes" base="ss_integral">
		<dox>
			<bri>Base class for built-in char types</bri>
		</dox>
	</class>


	<class name="ss_type_wchar_t" base="ss_type_char">
		<dox>
			<bri>Base class for built-in wchar-t type</bri>
			<det>Singleton class.</det>
		</dox>
		<field name="the_instance" specifier="static" type="ss_type" get="none" alt="none" set="none">
			<dox>
				<bri>holder for the class' only instance</bri>
			</dox>
		</field>
		<method name="instance" type="ss_type" specifier="static">
			<dox>
				<bri>get the class' only instance</bri>
			</dox>
		</method>
	</class>


	<class name="ss_type_schar" base="ss_type_char">
		<dox>
			<bri>Base class for built-in signed char type</bri>
			<det>Singleton class.</det>
		</dox>
		<field name="the_instance" specifier="static" type="ss_type" get="none" alt="none" set="none">
			<dox>
				<bri>holder for the class' only instance</bri>
			</dox>
		</field>
		<method name="instance" type="ss_type" specifier="static">
			<dox>
				<bri>get the class' only instance</bri>
			</dox>
		</method>
	</class>


	<class name="ss_type_uchar" base="ss_type_char">
		<dox>
			<bri>Base class for built-in unsigned char type</bri>
			<det>Singleton class.</det>
		</dox>
		<field name="the_instance" specifier="static" type="ss_type" get="none" alt="none" set="none">
			<dox>
				<bri>holder for the class' only instance</bri>
			</dox>
		</field>
		<method name="instance" type="ss_type" specifier="static">
			<dox>
				<bri>get the class' only instance</bri>
			</dox>
		</method>
	</class>


	<class name="ss_type_pchar" base="ss_type_char">
		<dox>
			<bri>Base class for built-in plain char type</bri>
			<det>Char without signed unsigned modifier. Singleton class.</det>
		</dox>
		<field name="the_instance" specifier="static" type="ss_type" get="none" alt="none" set="none">
			<dox>
				<bri>holder for the class' only instance</bri>
			</dox>
		</field>
		<method name="instance" type="ss_type" specifier="static">
			<dox>
				<bri>get the class' only instance</bri>
			</dox>
		</method>
	</class>


	<class name="ss_floating" abstract="yes" base="ss_builtin_type">
		<dox>
			<bri>Base class for built-in floating types</bri>
		</dox>
	</class>


	<class name="ss_type_float" base="ss_floating">
		<dox>
			<bri>Base class for built-in float type</bri>
			<det>Char without signed unsigned modifier. Singleton class.</det>
		</dox>
		<field name="the_instance" specifier="static" type="ss_type" get="none" alt="none" set="none">
			<dox>
				<bri>holder for the class' only instance</bri>
			</dox>
		</field>
		<method name="instance" type="ss_type" specifier="static">
			<dox>
				<bri>get the class' only instance</bri>
			</dox>
		</method>
	</class>


	<class name="ss_type_double" base="ss_floating">
		<dox>
			<bri>Base class for built-in double type</bri>
			<det>Char without signed unsigned modifier. Singleton class.</det>
		</dox>
		<field name="the_instance" specifier="static" type="ss_type" get="none" alt="none" set="none">
			<dox>
				<bri>holder for the class' only instance</bri>
			</dox>
		</field>
		<method name="instance" type="ss_type" specifier="static">
			<dox>
				<bri>get the class' only instance</bri>
			</dox>
		</method>
	</class>


	<class name="ss_type_ldouble" base="ss_floating">
		<dox>
			<bri>Base class for built-in long double type</bri>
			<det>Char without signed unsigned modifier. Singleton class</det>
		</dox>
		<field name="the_instance" specifier="static" type="ss_type" get="none" alt="none" set="none">
			<dox>
				<bri>holder for the class' only instance</bri>
			</dox>
		</field>
		<method name="instance" type="ss_type" specifier="static">
			<dox>
				<bri>get the class' only instance</bri>
			</dox>
		</method>
	</class>

</lsd>
