/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/lang/cplus/sem/as_decl.g.hh>
#include <lestes/lang/cplus/sem/as_id_to_ss_decl_name.g.hh>
#include <lestes/lang/cplus/sem/ss_decl_name.g.hh>
#include <lestes/lang/cplus/syn/token.hh>

package(lestes);
package(lang);
package(cplus);
package(sem);

void as_id_to_ss_decl_name::visit_as_template_id(ptr< as_template_id > )
{
	lassert2(false, "FIXME: to be implemented");
}

void as_id_to_ss_decl_name::visit_as_identifier(ptr< as_identifier > id)
{
	decl_name_set(ss_ordinary_name::create(id->location_get(), id->token_get()->value_get()));
}

void as_id_to_ss_decl_name::visit_as_destructor_id_token(ptr< as_destructor_id_token > )
{
	lassert2(false, "FIXME: to be implemented");
}

void as_id_to_ss_decl_name::visit_as_destructor_id_template(ptr< as_destructor_id_template > )
{
	lassert2(false, "FIXME: to be implemented");
}

void as_id_to_ss_decl_name::visit_as_constructor_id(ptr< as_constructor_id > )
{
	lassert2(false, "FIXME: to be implemented");
}

void as_id_to_ss_decl_name::visit_as_op_function_id(ptr< as_op_function_id > )
{
	lassert2(false, "FIXME: to be implemented");
}

void as_id_to_ss_decl_name::visit_as_conv_function_id(ptr< as_conv_function_id > )
{
	lassert2(false, "FIXME: to be implemented");
}

void as_id_to_ss_decl_name::visit_as_empty_id(ptr< as_empty_id > id)
{
	decl_name_set(ss_dummy_name::create(id->location_get()));
}

void as_id_to_ss_decl_name::visit_as_global_namespace_fake_id(ptr< as_global_namespace_fake_id > id)
{
	decl_name_set(ss_dummy_name::create(id->location_get()));
	// or: ???
	// ss_decl_seq::root_instance()->declared_by_get()->name_get()
	lassert2(false, "FIXME: if it is OK to use ss_dummy_name for as_global_namespace_fake_id, remove this lassert");
}

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);

