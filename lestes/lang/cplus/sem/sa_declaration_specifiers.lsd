<?xml version="1.0" encoding="iso-8859-1"?>
<!--
	The lestes compiler suite
	Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
	Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
	Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
	Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
	Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
	Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
	Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; version 2 of the License.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	See the full text of the GNU General Public License version 2, and
	the limitations in the file doc/LICENSE.

	By accepting the license the licensee waives any and all claims
	against the copyright holder(s) related in whole or in part to the
	work, its use, and/or the inability to use it.
-->
<lsd xmlns="http://lestes.jikos.cz/schemas/lsd" xmlns:h="http://www.w3.org/TR/REC-html40">

<file-name>sa_declaration_specifiers</file-name>

	<dox file="both">
		<bri>Analysed declaration specifiers.</bri>
		<det>
			Class containing properties of an analysed declaration specifier sequence.
		</det>
	</dox>

	<packages>
		<p>lestes</p>
		<p>lang</p>
		<p>cplus</p>
		<p>sem</p>
	</packages>

	<imports>
		<i>lestes/common.hh</i>
		<i>lestes/std/list.hh</i>
		<i>lestes/lang/cplus/sem/sa_common.g.hh</i>
		<i>lestes/lang/cplus/sem/ss_enums.g.hh</i>
	</imports>

	<implementation-imports>
		<i>lestes/lang/cplus/sem/ss_type.g.hh</i>
	</implementation-imports>

	<foreign-class name="ss_type">
		<p>lestes</p>
		<p>lang</p>
		<p>cplus</p>
		<p>sem</p>
	</foreign-class>

	<using-class name="object" packages="::lestes::std::"/>

	<class name="sa_declaration_specifiers" base="object">
		<dox>
			<bri>Analysed declaration specifiers.</bri>
			<det>Contains ss_type and other properties of analysed declaration specifier sequence.</det>
		</dox>
		<field name="type" type="ss_type" set="none" alt="none" init="void">
			<dox>
				<bri>The cv-qualified ss_type representing the sequence.</bri>
			</dox>
		</field>
		<field name="storage_class" type="ss_storage_class::type" set="none" alt="none" init="void">
			<dox>
				<bri>The storage class (including typedef).</bri>
			</dox>
		</field>
		<field name="explicit_flag" type="bool" set="none" alt="none" init="void">
			<dox>
				<bri>The explicit specifier flag.</bri>
			</dox>
		</field>
		<field name="inline_flag" type="bool" set="none" alt="none" init="void">
			<dox>
				<bri>The inline specifier flag.</bri>
			</dox>
		</field>
		<field name="virtual_flag" type="bool" set="none" alt="none" init="void">
			<dox>
				<bri>The virtual specifier flag.</bri>
			</dox>
		</field>
		<field name="friend_flag" type="bool" set="none" alt="none" init="void">
			<dox>
				<bri>The friend specifier flag.</bri>
			</dox>
		</field>
	</class>

</lsd>
