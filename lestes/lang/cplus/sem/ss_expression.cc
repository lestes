/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file 
\brief Definition of methods of semantic structures (ss_expression classes).
*/

#include <lestes/lang/cplus/sem/ss_expression.g.hh>
#include <lestes/lang/cplus/sem/ss_expr_binary_op.g.hh>
#include <algorithm>

using ::std::max;

package(lestes);
package(lang);
package(cplus);
package(sem);


bool ss_expression :: is_lvalue(){
	return false;
}

bool ss_var_ref_abstract :: is_lvalue(){
	return true;
}

/*!
  Shall be in ss_expr_binary_op.cc
*/
bool ss_assign :: is_lvalue(){
	return true;
}

//FIXME this function shall return result for const expressions
int ss_expression :: get_value(){
return 0;
}

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);

