/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration_to_ss_decl_seq.g.hh>
#include <lestes/lang/cplus/sem/ss_misc.g.hh>
#include <lestes/lang/cplus/sem/ss_statement.g.hh>

package(lestes);
package(lang);
package(cplus);
package(sem);

void ss_declaration_to_ss_decl_seq::visit_ss_namespace_definition(ptr< ::lestes::lang::cplus::sem::ss_namespace_definition > decl)
{
	result_set(decl->body_get());
}

void ss_declaration_to_ss_decl_seq::visit_ss_object_declaration(ptr< ::lestes::lang::cplus::sem::ss_object_declaration > /*decl*/)
{
	result_set(NULL);
}

void ss_declaration_to_ss_decl_seq::visit_ss_bitfield_declaration(ptr< ::lestes::lang::cplus::sem::ss_bitfield_declaration > /*decl*/)
{
	result_set(NULL);
}

void ss_declaration_to_ss_decl_seq::visit_ss_parameter_declaration(ptr< ::lestes::lang::cplus::sem::ss_parameter_declaration > /*decl*/)
{
	result_set(NULL);
}

void ss_declaration_to_ss_decl_seq::visit_ss_structure_declaration(ptr< ::lestes::lang::cplus::sem::ss_structure_declaration > decl)
{
	result_set(decl->body_get());
}

void ss_declaration_to_ss_decl_seq::visit_ss_enum_definition(ptr< ::lestes::lang::cplus::sem::ss_enum_definition > /*decl*/)
{
	result_set(NULL);
}

void ss_declaration_to_ss_decl_seq::visit_ss_typedef_definition(ptr< ::lestes::lang::cplus::sem::ss_typedef_definition > /*decl*/)
{
	result_set(NULL);
}

void ss_declaration_to_ss_decl_seq::visit_ss_function_declaration(ptr< ::lestes::lang::cplus::sem::ss_function_declaration > /*decl*/)
{
	result_set(NULL);
}

void ss_declaration_to_ss_decl_seq::visit_ss_enumerator_declaration(ptr< ::lestes::lang::cplus::sem::ss_enumerator_declaration > /*decl*/)
{
	result_set(NULL);
}

void ss_declaration_to_ss_decl_seq::visit_ss_using_declaration(ptr< ::lestes::lang::cplus::sem::ss_using_declaration > decl)
{
	result_set(process(decl->real_target_get()));
}

void ss_declaration_to_ss_decl_seq::visit_ss_builtin_operator_declaration(ptr< ::lestes::lang::cplus::sem::ss_builtin_operator_declaration > /*decl*/)
{
	result_set(NULL);
}


void ss_declaration_to_ss_decl_seq::visit_ss_compound_stmt_declaration(ptr< ::lestes::lang::cplus::sem::ss_compound_stmt_declaration > decl)
{
	result_set(decl->compound_stmt_get()->decl_seq_get());
}

void ss_declaration_to_ss_decl_seq::visit_ss_method_declaration(ptr< ::lestes::lang::cplus::sem::ss_method_declaration > /*decl*/)
{
	result_set(NULL);
}

void ss_declaration_to_ss_decl_seq::visit_ss_fake_declaration(ptr< ::lestes::lang::cplus::sem::ss_fake_declaration > /*decl*/)
{
	result_set(NULL);
}

void ss_declaration_to_ss_decl_seq::visit_ss_injected_class_declaration(ptr< ::lestes::lang::cplus::sem::ss_injected_class_declaration > decl)
{
	result_set(decl->real_decl_get()->body_get());
}

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);

