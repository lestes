<?xml version="1.0" encoding="iso-8859-1"?>
<!--
	The lestes compiler suite
	Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
	Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
	Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
	Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
	Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
	Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
	Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; version 2 of the License.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	See the full text of the GNU General Public License version 2, and
	the limitations in the file doc/LICENSE.

	By accepting the license the licensee waives any and all claims
	against the copyright holder(s) related in whole or in part to the
	work, its use, and/or the inability to use it.
-->
<lsd xmlns="http://lestes.jikos.cz/schemas/lsd" xmlns:h="http://www.w3.org/TR/REC-html40">

	<file-name>ss_type2info</file-name>

	<dox file="hh">
		<bri>Type information for SS.</bri>
		<det>Declaration of visitor providing type information for certain ss_type classes.</det>
	</dox>

	<dox file="cc">
		<bri>Type information for SS.</bri>
		<det>Definition of visitor providing type information for certain ss_type classes.</det>
	</dox>

	<packages>
		<p>lestes</p>
		<p>lang</p>
		<p>cplus</p>
		<p>sem</p>
	</packages>

	<imports>
		<i>lestes/common.hh</i>
		<i>lestes/std/list.hh</i>
		<i>lestes/lang/cplus/sem/ss_type_visitor.v.g.hh</i>
	</imports>

	<implementation-imports>
		<i>lestes/lang/cplus/sem/ss_type.g.hh</i>
	</implementation-imports>

	<using-class name="object" packages="::lestes::std::"/>

	<forward-class name="ss_const" />
	<forward-class name="ss_volatile" />
	<forward-class name="ss_struct_base" />
	<forward-class name="ss_builtin_type" />
	<forward-class name="ss_type" />

	<include href="ss_type_visitor.v.lsd" />

	<xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="ss_type_visitor.v.lsd">
		<xi:fallback/>
	</xi:include>

	<class name="ss_type2info" base="ss_type2info_base">
		<dox>
			<bri>Visitor providing lightweight type info for certain SS type classes.</bri>
			<det>
				Recognizes certain concrete SS classes, substituting typeinfo mechanism.
				TODO pt Enables cast to the derived type without dynamic_cast.
			</det>
		</dox>

		<enum name="info_type">
			<e n="UNKNOWN" />
			<e n="SS_CONST" />
			<e n="SS_VOLATILE" />
			<e n="SS_CONST_VOLATILE" />
			<e n="SS_POINTER" />
			<e n="SS_MEMBER_POINTER" />
			<e n="SS_REFERENCE" />
			<e n="SS_PSEUDOREFERENCE" />
			<e n="SS_FUNCTION" />
		</enum>
		<declare-abstract-methods prefix="visit_" skip-defined="yes" />
		<visit-return-method name="process" get-from="info" />
		<field name="info" type="info_type" set="none" alt="none" init="UNKNOWN">
			<dox>
				<bri>Type information.</bri>
				<det>The visited object's type information.</det>
			</dox>
		</field>
		<field name="ss_const_object" type="ss_const" set="none" alt="none" init="NULL" check="">
			<dox>
				<bri>Const object.</bri>
				<det>The visited object cast to appropriate type, if instance of class ss_const.</det>
			</dox>
		</field>
		<field name="ss_volatile_object" type="ss_volatile" set="none" alt="none" init="NULL" check="">
			<dox>
				<bri>Volatile object.</bri>
				<det>The visited object cast to appropriate type, if instance of class ss_volatile.</det>
			</dox>
		</field>
		<method name="default_ss_struct_base" type="void">
			<dox>
				<bri>Default visitor method.</bri>
				<det>Visitor method for all SS types in ss_struct_base hierarchy.</det>
			</dox>
			<param name="obj" type="ss_struct_base" />
		</method>
		<method name="default_ss_builtin_type" type="void">
			<dox>
				<bri>Default visitor method.</bri>
				<det>Visitor method for all SS types in ss_builtin_type hierarchy.</det>
			</dox>
			<param name="obj" type="ss_builtin_type" />
		</method>
<!--  TODO pt returning the appropriate casted object
      <field name="ss_const_volatile_object" type="ss_const_volatile" set="none" alt="none" init="NULL" check="">
         <dox>
            <bri>The visited object cast to appropriate type, if instance of class ss_const.</bri>
         </dox>
      </field>
      <field name="ss_pointer_object" type="ss_pointer" set="none" alt="none" init="NULL" check="">
         <dox>
            <bri>The visited object cast to appropriate type, if instance of class ss_pointer.</bri>
         </dox>
      </field>
      <field name="ss_member_pointer_object" type="ss_member_pointer" set="none" alt="none" init="NULL" check="">
         <dox>
            <bri>The visited object cast to appropriate type, if instance of class ss_member_pointer.</bri>
         </dox>
      </field>
      <field name="ss_reference_object" type="ss_reference" set="none" alt="none" init="NULL" check="">
         <dox>
            <bri>The visited object cast to appropriate type, if instance of class ss_reference.</bri>
         </dox>
      </field>
-->
	</class>
</lsd>
