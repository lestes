/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Simple declaration analysis.

  Analysis of simple declaration and its conversion to ss_declaration.
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/sem/sa_simple_declaration.g.hh>
#include <lestes/lang/cplus/sem/sa_simple_declaration.m.hh>
#include <lestes/lang/cplus/sem/declaration_broadcasting.hh>
#include <lestes/lang/cplus/sem/as_decl.g.hh>
#include <lestes/lang/cplus/sem/as_declarator_op2op_func.g.hh>
#include <lestes/lang/cplus/sem/as_id_to_ss_decl_name.g.hh>
#include <lestes/lang/cplus/sem/as_id_to_declaration_set.g.hh>
#include <lestes/lang/cplus/sem/as_other.g.hh>
#include <lestes/lang/cplus/sem/sa_context.g.hh>
#include <lestes/lang/cplus/sem/sa_decl_seq_compound_pair_creator.g.hh>
#include <lestes/lang/cplus/sem/sa_declaration_specifiers.g.hh>
#include <lestes/lang/cplus/sem/sa_declaration_specifier_list.g.hh>
#include <lestes/lang/cplus/sem/sa_declarator_type.g.hh>
#include <lestes/lang/cplus/sem/sa_param_declaration.g.hh>
#include <lestes/lang/cplus/sem/ss_misc.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration2ss_typedef_definition.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration2ss_namespace_definition.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration2ss_structure_declaration.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration2ss_object_declaration.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration2ss_function_declaration.g.hh>
#include <lestes/lang/cplus/sem/ss_statement.g.hh>
#include <lestes/lang/cplus/sem/ss_type.g.hh>
#include <lestes/lang/cplus/sem/ss_decl_name.g.hh>
#include <lestes/lang/cplus/sem/li_func_by_name_in_single_scope.g.hh>
#include <lestes/lang/cplus/sem/li_by_name_in_single_scope.g.hh>
#include <lestes/lang/cplus/sem/li_non_secu_by_name_in_single_scope.g.hh>
#include <lestes/lang/cplus/sem/or_or.g.hh>
#include <lestes/lang/cplus/syn/manager.hh>
#include <lestes/lang/cplus/syn/token.hh>
#include <lestes/lang/cplus/sem/sa_loggers.hh>
#include <lestes/msg/logger.hh>
#include <lestes/msg/logger_util.hh>

#include <algorithm>
#include <iterator>
#include <cstdlib>

package(lestes);
package(lang);
package(cplus);
package(sem);

/*!
  Saves declaration specifiers to the context.
  \pre specifiers != NULL
  \param specifiers The declaration specifiers.
*/
void sa_simple_declaration::save_declaration_specifiers(ptr<as_declaration_specifier_seq> specifiers)
{
	sa_simple_declaration_logger << "sa_simple_declaration::save_declaration_specifiers()\n" << msg::eolog;
	lassert(specifiers);
	
	if (::lestes::lang::cplus::syn::manager::in_disambiguation()) {
		sa_simple_declaration_logger << "nop -- in disambiguation\n" << msg::eolog;
	} else {
		sa_simple_declaration_logger << "saving specifiers\n" << msg::eolog;
		sa_context_manager::instance()->current()->as_get()->declaration_specifier_seq_set(specifiers);
	}

	sa_simple_declaration_logger << "sa_simple_declaration::save_declaration_specifiers() end\n" << msg::eolog;
}

/*!
  Saves declaration specifiers to the context.
  \pre specifiers != NULL
  \param specifiers The declaration specifiers.
*/
void sa_simple_declaration::save_type_specifiers(ptr<as_type_specifier_seq> specifiers)
{
	sa_simple_declaration_logger << "sa_simple_declaration::save_type_specifiers()\n" << msg::eolog;
	lassert(specifiers);
	
	if (::lestes::lang::cplus::syn::manager::in_disambiguation()) {
		sa_simple_declaration_logger << "nop -- in disambiguation\n" << msg::eolog;
	} else {
		sa_simple_declaration_logger << "saving specifiers\n" << msg::eolog;
		ptr<as_declaration_specifier_seq> asdss =
			as_declaration_specifier_seq::create(
					specifiers->location_get(),
					list< srp<as_declaration_specifier> >::create());
		::std::copy(specifiers->l_get()->begin(),specifiers->l_get()->end(),::std::back_inserter(*(asdss->l_get())));
		sa_context_manager::instance()->current()->as_get()->declaration_specifier_seq_set(asdss);
	}

	sa_simple_declaration_logger << "sa_simple_declaration::save_type_specifiers() end\n" << msg::eolog;
}

//! Type of list of as_declarator_op elements.
typedef list< srp<as_declarator_op> > as_declarator_op_list_type;

//! Type of set of ss_declaration elements.
typedef ::lestes::std::set< srp<ss_declaration> > ss_declaration_set_type;

/*!
  Processes declarator and either creates a declaration, or modifies a previously
  established declaration of the same entity.
  \pre declarator != NULL
  \param declarator  The declarator to process.
*/
void sa_simple_declaration::process(ptr<as_declarator> declarator)
{
	sa_simple_declaration_logger << "sa_simple_declaration::process()\n" << msg::eolog;

	if (::lestes::lang::cplus::syn::manager::in_disambiguation()) {
		sa_simple_declaration_logger << "nop -- in disambiguation\n" << msg::eolog;
		sa_simple_declaration_logger << "sa_simple_declaration::process() end\n" << msg::eolog;
		return;
	}
	
	lassert2(declarator->name_get(),"Name not filled in declarator");

	ptr<sa_context> ctx = sa_context_manager::instance()->current();
	
	sa_simple_declaration_logger << "processing declaration specifiers\n" << msg::eolog;

	// process declaration specifiers
	// TODO like this, or processed in sa_get() ???

	// TODO do not create specifiers, just reference them once ?? better  not
	ptr<as_declaration_specifier_seq> adss = ctx->as_get()->declaration_specifier_seq_get();
	ptr<as_declaration_specifier_list_type> adsl = adss->l_get();
	
	ptr<sa_declaration_specifier_list> sdsl = sa_declaration_specifier_list::create();
	ptr<sa_declaration_specifiers> sads = sdsl->process(adss->location_get(),adsl);
	
	if (sads->storage_class_get() == ss_storage_class::ST_TYPEDEF) {
		sa_simple_declaration_logger << "got typedef\n" << msg::eolog;
		// handle typedef
		process_typedef(ctx,sads,declarator);
	} else {
		sa_simple_declaration_logger << "distinguish function from object\n" << msg::eolog;

		ptr<as_declarator_op_list_type> adol = declarator->declarator_ops_get();
	
		ptr<as_declarator_op_func> asdof;
		
		// check whether the first declarator op is func
		if (!adol->empty())
			asdof = as_declarator_op2op_func::instance()->process(adol->front());
		
		if (asdof) {
			sa_simple_declaration_logger << "got function declaration\n" << msg::eolog;
		
			// TODO pt process_function_declaration(ctx,sads,declarator);
			// also get the necessary info for parameter declarations if toplevel is function
			process_function_declaration(ctx,asdof,sads,declarator);

		} else {
			sa_simple_declaration_logger << "got object declaration\n" << msg::eolog;
			
			process_object_declaration(ctx,sads,declarator);
		}
	}
	
	sa_simple_declaration_logger << "sa_simple_declaration::process() end\n" << msg::eolog;
}

/*!
  Processes function declaration and either creates a declaration, or modifies a previously
  established declaration of the same function.
  \pre ctx != NULL
  \pre op_func != NULL
  \pre specifiers != NULL
  \pre declarator != NULL
  
  \param ctx  The context containing the scope.
  \param op_func  The function declarator operator, containing the parameters.
  \param specifiers  The declaration specifiers of the declaration.
  \param declarator  The function declarator to process.
*/
void sa_simple_declaration::process_function_declaration(ptr<sa_context> ctx, ptr<as_declarator_op_func> op_func,
		ptr<sa_declaration_specifiers> specifiers, ptr<as_declarator> declarator)
{
	sa_simple_declaration_logger << "sa_simple_declaration::process_function_declaration()\n" << msg::eolog;

	lassert(ctx);
	lassert(op_func);
	lassert(specifiers);
	lassert(declarator);

	sa_simple_declaration_logger << "checking declaration specifier flags\n" << msg::eolog;

	// TODO pt distinguish whether we are in class scope declaring, defining
	// with implicit inline, or whehter defining method in namespace scope
	// or declaring function in ns/fun scope
	// or defining function in ns scope (parser won't allow defining fun in fun scope)
	// TODO pt check if in class so that we can use v/f/e/i flags
	
	ptr<ss_decl_seq> ctx_scope = ctx->ss_get()->scope_get();
	ptr<ss_declaration> ctx_decl = ctx_scope->declared_by_get();

	ptr<ss_structure_declaration> sssd = ss_declaration2ss_structure_declaration::instance()->process(ctx_decl);
	
	if (sssd) {
		sa_simple_declaration_logger << "declaration is in class\n" << msg::eolog;
		if (specifiers->friend_flag_get()) {
			sa_simple_declaration_logger << "declaration is a friend function\n" << msg::eolog;

			lassert2(false,"Friend functions not implemented.");
			// TODO pt
			//process_friend_function_declaration(ctx,specifiers,declarator);

		} else {
			sa_simple_declaration_logger << "declaration is a method\n" << msg::eolog;

			report << methods_not_implemented << declarator->location_get();
			return;
			// was:
			lassert2(false,"Method declarations not implemented.");
			// TODO pt
			// process_method_declaration(ctx,specifiers,declarator);
		}
	} else {
		sa_simple_declaration_logger << "declaration is out of class\n" << msg::eolog;
		if (specifiers->virtual_flag_get() || specifiers->friend_flag_get() || specifiers->explicit_flag_get() ||
			 specifiers->inline_flag_get()) {
			sa_simple_declaration_logger << "got some invalid flags\n" << msg::eolog;
			// TODO pt distinguish which flag to give more accurate error report
			// error: invalid specifier in function declaration
			report << invalid_specifier_function << declarator->location_get();
		}

		// TODO pt check storage and distinguish extern from normal so that type processor can
		// be called in the proper regime

		// for now, just do the definition
		// TODO how will i know extern f() {} ????

		sa_simple_declaration_logger << "converting the name\n" << msg::eolog;

		ptr<as_name> name = declarator->name_get();
		// get the ss equivalent of the name
		ptr<ss_decl_name> decl_name = as_id_to_ss_decl_name::instance()->process(name->identifier_get());

		/*
		 analyse the RETURN type of the function (that is easy, just omit the first declspec)
		 
		 now pass the return type to the second analyser, also pass the op_func
		 to get the ss_type of the whole and list of the sa_declaration_specifiers
		 
		 the third analyser should take care of all parameter declarations
		 pass op_func and the sa_declaration_specifiers list
		 perhaps including the creation of the special parameter scope, which
		 alas! has to be returned, so perhaps create it outside, hell that makes
		 alot of code in here, perhaps some method would be enough
		 
		 
		 */

#if 0
		sa_simple_declaration_logger << "extracting the ss_type from declarator\n" << msg::eolog;
		
		// extract the function return type
		// TODO pt change for visit-return- as well as sa_declarator_type TODO
		ptr<sa_declarator_return_type> sadrt = sa_declarator_return_type::create();
		ptr<ss_type> return_type = sadrt->process(specifiers->type_get(),declarator);

		// extract the function type and parameter specifiers
		ptr<sa_function_type> saft = sa_function_type::create();
		saft->process(return_type,op_func);
		ptr< list< srp<sa_declaration_specifiers> > > param_specifiers =
			saft->param_declaration_specifiers_get();
		ptr<ss_type> type = saft->type_get();
#endif
		// TODO ordinary type for now, with duplicate ds processing
		// TODO we need ss_function, solve this together
		ptr<ss_function> type =
			(sa_declarator_type::create()->process(specifiers->type_get(),declarator)).dncast<ss_function>();

		ptr<ss_declaration> declaration;

		// TODO lookup the function and check if it exists
		//typedef set< srp<ss_function_declaration> > ss_function_declaration_set_type;
		typedef set< srp<ss_declaration> > ss_declaration_set_type;
		// TODO pt li as singleton
		ptr<ss_declaration_set_type> ssds = li_non_secu_by_name_in_single_scope::create()->process(decl_name,ctx_scope);

		if (!ssds->empty())
			sa_simple_declaration_logger << "found some declarations with the same name\n" << msg::eolog;

		bool error = false;
		
		ptr<ss_function_declaration> found_decl;

		for (ss_declaration_set_type::iterator it = ssds->begin(), end = ssds->end();
				it != end; ++it) {
			ptr<ss_function_declaration> ssfd =
				ss_declaration2ss_function_declaration::instance()->process(*it);
			
			if (ssfd) {
				// TODO pt
				ptr<ss_function> ssft = (ssfd->type_get()).dncast<ss_function>();

				if (ssft->equal_signature(type)) {
					sa_simple_declaration_logger << "found function with the same signature\n" << msg::eolog;

					lassert2(!found_decl,"Two function declarations with the same signature found.");

					if (ssft->returns_get() != type->returns_get()) {
						sa_simple_declaration_logger << "error: the return types are different\n" << msg::eolog;
						report << different_return_types << declarator->location_get();
						report << previous_declaration << ssfd->location_get();
					/*	sa_simple_declaration_logger <<
							"sa_simple_declaration::process_function_declaration() end\n" << msg::eolog;
					 */
						error = true;
						break;
					}
					sa_simple_declaration_logger << "the return types match\n" << msg::eolog;
					found_decl = ssfd;

					// wait for different redeclarations
				}
			} else {
				report << redeclaration_different << declarator->location_get();
				report << previous_declaration << (*it)->location_get();
				error = true;
				break;
			}
		}

		if (error) {
         exit(1);
		} if (found_decl) {
			sa_simple_declaration_logger << "processing the found function declaration\n" << msg::eolog;
         declaration = found_decl;

			// check the defaults set before merging with the current ones
			// how ? create the params again and compare ???

			/*
				
			ptr<source_location> loc = declarator->location_get();
			// create scope for parameters
			ptr<ss_decl_seq> param_scope = 
				sa_decl_seq_compound_pair_creator::instance()->process(loc,
						current_scope,current_scope->compound_stmt_get())->first;

			typedef list< srp<as_param_declaration> > as_param_declaration_list;
			ptr<as_param_declaration_list> aspdl = op_func->parameters_get()->parameters_get();

			sa_simple_declaration_logger << "creating parameter declarations\n" << msg::eolog;
			
			// declare the parameters
			ptr<sa_param_declaration> sapd = sa_param_declaration::create(param_scope);

			for (as_param_declaration_list::iterator it = aspdl->begin(), end = aspdl->end();
					it != end; ++it) {
				sapd->process(*it);
			}

			*/
			
			sa_simple_declaration_logger << "merging parameter declarations\n" << msg::eolog;

			// TODO pt merge

		} else {
			sa_simple_declaration_logger << "adding new function declaration\n" << msg::eolog;


			// TODO pt what about those funny little thingies in op_func
			// like const, volatile for methods, where are these checked?
			// at least that they are not present ^_^

			// TODO this might be different
			ptr<ss_decl_seq> current_scope = ctx->ss_get()->scope_get();
			
			ptr<source_location> loc = declarator->location_get();
			// create scope for parameters
			ptr<ss_decl_seq> param_scope = 
				sa_decl_seq_compound_pair_creator::instance()->process(loc,
						current_scope,current_scope->compound_stmt_get())->first;

			typedef list< srp<as_param_declaration> > as_param_declaration_list;
			ptr<as_param_declaration_list> aspdl = op_func->parameters_get()->parameters_get();

			sa_simple_declaration_logger << "creating parameter declarations\n" << msg::eolog;
			
			// declare the parameters
			ptr<sa_param_declaration> sapd = sa_param_declaration::create(param_scope);

			for (as_param_declaration_list::iterator it = aspdl->begin(), end = aspdl->end();
					it != end; ++it) {
				sapd->process(*it);
			}

#if 0
			for (list< srp< sa_declaration_specifiers > >::iterator it = param_specifiers->begin(),
					end = param_specifiers->end(); it != end; ++it) {
				sapd->process(*it);
			}
#endif
			
			ptr<ss_declaration_time> dt = ss_declaration_time::create(loc->order_get());

			sa_simple_declaration_logger << "creating the function declaration\n" << msg::eolog;

			// declare the function
		   declaration = ss_function_declaration::create(
				loc, // location 
				dt, // visible time
				dt, // declaration time
				decl_name, // declaration name
				current_scope, // contained in
				type, // type
				// TODO pt fill
				ss_linkage::create("C++",ss_linkage::LINKAGE_EXTERNAL), // linkage
				param_scope, // parameters
				// TODO pt body???
				NULL // body
				);
		
			// fix the parameter scope
			param_scope->declared_by_set(declaration);

			// param ds: jeho parent je current scope

			sa_simple_declaration_logger << "adding the declaration\n" << msg::eolog;

			current_scope->contents_get()->push_back(declaration);

		}

      sa_simple_declaration_logger << "announce new declaration\n" << msg::eolog;
      // TMA: make the declaration known to egg
      declaration_finished->broadcast(declaration);
	}
	
	sa_simple_declaration_logger << "sa_simple_declaration::process_function_declaration() end\n" << msg::eolog;
}

void process_friend_function_declaration(ptr<sa_context> ctx, ptr<sa_declaration_specifiers> specifiers,
	ptr<as_declarator> declarator)
{
	lassert(ctx && specifiers && declarator);
	lassert(false);
}

/*!
  Processes object declaration. If the name exists and refers to an object of the same type,
  it is checked, whether a declaration is also a definition and appropriate structures are updated.
  Multiple definition renders the program ill-formed. If the name refers to another entity,
  the program is ill-formed. If the name does not exists, a new declaration is added to the current scope,
  or another enclosing scope according to its storage class and the kind of the current scope.
  \pre ctx != NULL
  \pre specifiers != NULL
  \pre declarator != NULL
  \param ctx  The context of the analyser.
  \param specifiers  The processed declaration specifiers of the object.
  \param declarator  The declarator of the object.
*/
void sa_simple_declaration::process_object_declaration(ptr<sa_context> ctx, ptr<sa_declaration_specifiers> specifiers,
		ptr<as_declarator> declarator)
{
	sa_simple_declaration_logger << "sa_simple_declaration::process_object_declaration()\n" << msg::eolog;

	lassert(ctx);
	lassert(specifiers);
	lassert(declarator);

	sa_simple_declaration_logger << "checking declaration specifier flags\n" << msg::eolog;
	
	if (specifiers->virtual_flag_get() || specifiers->friend_flag_get() || specifiers->explicit_flag_get() ||
		 specifiers->inline_flag_get()) {
		sa_simple_declaration_logger << "got some invalid flags\n" << msg::eolog;
		// TODO pt distinguish which flag to give more accurate error report
		// error: invalid specifier in object declaration
		report << invalid_specifier_object << declarator->location_get();
	}
	
	sa_simple_declaration_logger << "extracting the ss_type from declarator\n" << msg::eolog;
	
	// extract the plain type
	ptr<sa_declarator_type> sdt = sa_declarator_type::create();
	ptr<ss_type> type = sdt->process(specifiers->type_get(),declarator);

	sa_simple_declaration_logger << "converting the name\n" << msg::eolog;

	ptr<as_name> name = declarator->name_get();
	// get the ss equivalent of the name
	ptr<ss_decl_name> decl_name = as_id_to_ss_decl_name::instance()->process(name->identifier_get());

	// TODO pt different
	ptr<ss_decl_seq> current_scope = ctx->ss_get()->scope_get();
	bool ns = ss_declaration2ss_namespace_definition::instance()->process(current_scope->declared_by_get());
	bool cs = ss_declaration2ss_structure_declaration::instance()->process(current_scope->declared_by_get());
   
	ss_storage_class::type storage_class = specifiers->storage_class_get();

	sa_simple_declaration_logger << "looking up the name\n" << msg::eolog;

	ptr<ss_declaration_set_type> decls = li_non_secu_by_name_in_single_scope::create()->process(decl_name,current_scope);
   ptr<ss_declaration> declaration;

	ss_declaration_set_type::iterator it = decls->begin();
	if (it != decls->end()) {
		sa_simple_declaration_logger << "found some declarations\n" << msg::eolog;
	  
	   declaration = *it;

		// TODO pt bugbug
      /*
		sa_simple_declaration_logger << "found " << 
			declaration->name_get().dncast<ss_ordinary_name>()->name_get() << "\n" << msg::eolog;
         */
		
		// check that the name refers to an object
		ptr<ss_object_declaration> ssod = ss_declaration2ss_object_declaration::instance()->process(declaration);

		if (ssod) {
			if (ssod->type_get() != type) {
				sa_simple_declaration_logger << "error: the type does not match\n" << msg::eolog;
				report << conflicting_types << declarator->location_get();
				report << previous_declaration << (*it)->location_get();
			}

			sa_simple_declaration_logger << "analysing found storage specifiers\n" << msg::eolog;
         
         ss_storage_class::type old_storage = ssod->storage_class_get();

			// analyse storage and ban 2xdefinition
			 
         if (cs) {
            report << field_redefinition << declarator->location_get();
            report << previous_declaration << declaration->location_get();
            // TODO pt
            exit(1);
         } if (ns) {
            switch (storage_class) {
               case ss_storage_class::ST_AUTO:
               case ss_storage_class::ST_REGISTER:
               case ss_storage_class::ST_MUTABLE:
                  // report error: invalid storage specifiers
                  report << invalid_storage_specifier << declarator->location_get(); 
                  storage_class = ss_storage_class::ST_NONE;
                  // fall through
               case ss_storage_class::ST_NONE:
                  switch (old_storage) {
                     case ss_storage_class::ST_NONE:
                     case ss_storage_class::ST_STATIC:
                     case ss_storage_class::ST_DEFINED:
                        report << redefinition << declarator->location_get(); 
                        report << previous_declaration << declaration->location_get();
                        storage_class = old_storage;
                        break;
                     case ss_storage_class::ST_EXTERN:
                        storage_class = ss_storage_class::ST_DEFINED;
                        break;
                     default:
                        lassert2(false,"You should never get here");
                  }
                  break;
               case ss_storage_class::ST_STATIC:
                  switch (old_storage) {
                     case ss_storage_class::ST_EXTERN:
                     case ss_storage_class::ST_DEFINED:
                        report << static_and_extern << declarator->location_get(); 
                        storage_class = old_storage;
                        break;
                     case ss_storage_class::ST_NONE:
                     case ss_storage_class::ST_STATIC:
                        report << redefinition << declarator->location_get(); 
                        report << previous_declaration << declaration->location_get();
                        storage_class = old_storage;
                        break;
                     default:
                        lassert2(false,"You should never get here");
                  }
                  break;
               case ss_storage_class::ST_EXTERN:
                  switch (old_storage) {
                     case ss_storage_class::ST_EXTERN:
                     case ss_storage_class::ST_DEFINED:
                        storage_class = old_storage;
                        break;
                     case ss_storage_class::ST_NONE:
                        storage_class = ss_storage_class::ST_DEFINED;
                        break;
                     case ss_storage_class::ST_STATIC:
                        report << static_and_extern << declarator->location_get(); 
                        storage_class = old_storage;
                        break;
                     default:
                        lassert2(false,"You should never get here");
                  }
                  break;
               case ss_storage_class::ST_DEFINED:
                  switch (old_storage) {
                     case ss_storage_class::ST_EXTERN:
                        storage_class = old_storage;
                        break;
                     case ss_storage_class::ST_DEFINED:
                     case ss_storage_class::ST_NONE:
                        report << redefinition << declarator->location_get(); 
                        report << previous_declaration << declaration->location_get();
                        storage_class = old_storage;
                        break;
                     case ss_storage_class::ST_STATIC:
                        report << static_and_extern << declarator->location_get(); 
                        storage_class = old_storage;
                        break;
                     default:
                        lassert2(false,"You should never get here");
                  }
                  break;
               case ss_storage_class::ST_TYPEDEF:
               default:
                  lassert2(false,"You should never get here");
            }
         } else {
            switch (old_storage) {
               case ss_storage_class::ST_NONE:
               case ss_storage_class::ST_AUTO:
               case ss_storage_class::ST_REGISTER:
                  report << redefinition << declarator->location_get(); 
                  report << previous_declaration << declaration->location_get();
                  storage_class = old_storage;
                  break;
               case ss_storage_class::ST_EXTERN:
                  lassert2(false,"Extern in function not supported.");
                  break;
               case ss_storage_class::ST_STATIC:
                  lassert2(false,"Static in function not supported.");
                  break;
               case ss_storage_class::ST_MUTABLE:
                  // report error: invalid storage specifiers
                  report << invalid_storage_specifier << declarator->location_get(); 
                  storage_class = ss_storage_class::ST_NONE;
                  break;
               case ss_storage_class::ST_TYPEDEF:
               default:
                  lassert2(false,"You should never get here");
            }
         }

         // set the new storage class
         ssod->storage_class_set(storage_class);
			
		} else {
			sa_simple_declaration_logger << "error: the found declaration does not declare object\n" << msg::eolog;
			report << redeclaration_different << declarator->location_get();
			report << previous_declaration << (*it)->location_get();
		}

	} else {

		// check the storage class
		
      if (ns) {
         switch (storage_class) {
            case ss_storage_class::ST_NONE:
            case ss_storage_class::ST_STATIC:
            case ss_storage_class::ST_EXTERN:
               break;
            case ss_storage_class::ST_MUTABLE:
            case ss_storage_class::ST_AUTO:
            case ss_storage_class::ST_REGISTER:
               // report error: invalid storage specifiers
               report << invalid_storage_specifier << declarator->location_get(); 
               storage_class = ss_storage_class::ST_NONE;
               break;
            case ss_storage_class::ST_TYPEDEF:
            default:
               lassert2(false,"You should never get here");
         }
      } else if (cs) {
         switch (storage_class) {
            case ss_storage_class::ST_AUTO:
            case ss_storage_class::ST_REGISTER:
            case ss_storage_class::ST_EXTERN:
               // report error: invalid storage specifiers
               report << field_storage_specifier << declarator->location_get(); 
               storage_class = ss_storage_class::ST_NONE;
               break;
            case ss_storage_class::ST_NONE:
            case ss_storage_class::ST_STATIC:
            case ss_storage_class::ST_MUTABLE:
               break;
            case ss_storage_class::ST_TYPEDEF:
            default:
               lassert2(false,"You should never get here");
         }
      } else {
         switch (storage_class) {
            case ss_storage_class::ST_NONE:
            case ss_storage_class::ST_AUTO:
            case ss_storage_class::ST_REGISTER:
               storage_class = ss_storage_class::ST_NONE;
               break;
               case ss_storage_class::ST_EXTERN:
                  lassert2(false,"Extern in function not supported.");
                  break;
               case ss_storage_class::ST_STATIC:
                  lassert2(false,"Static in function not supported.");
                  break;
            case ss_storage_class::ST_MUTABLE:
               // report error: invalid storage specifiers
               report << invalid_storage_specifier << declarator->location_get(); 
               storage_class = ss_storage_class::ST_NONE;
               break;
            case ss_storage_class::ST_TYPEDEF:
            default:
               lassert2(false,"You should never get here");
         }
      }
		
		ptr<source_location> loc = declarator->location_get();
		ptr<ss_declaration_time> dt = ss_declaration_time::create(loc->order_get());

		sa_simple_declaration_logger << "creating the object declaration\n" << msg::eolog;

		// create the object declaration
		declaration = ss_object_declaration::create(
			loc, // location
			dt, // visible since
			dt, // declaration time
			decl_name, // declaration name
			current_scope, // contained in
			type, // ss type
			// TODO pt from context via visitor
			ss_linkage::create("C++",ss_linkage::LINKAGE_NO), // linkage
			ctx->ss_get()->access_specifier_get(),
			storage_class, // storage class
			ss_declaration_time::infinity(), // initialized since
			NULL // initializer
			);

		// TODO pt choose scope to add the declaration to

		sa_simple_declaration_logger << "adding the declaration to current scope\n" << msg::eolog;

		// add the declaration into the current scope
		current_scope->contents_get()->push_back(declaration);

	}

   sa_simple_declaration_logger << "announce new declaration\n" << msg::eolog;
   // TMA: make the declaration known to egg
   declaration_finished->broadcast(declaration);

	sa_simple_declaration_logger << "sa_simple_declaration::process_object_declaration() end\n" << msg::eolog;
}

/*!
  Processes typedef definition. If the name exists and refers to a type, a check is done
  to ensure that the original type and the new type are the same. If the name refers to
  a non-type, the program is ill-formed. If the name does not exists, the newly defined
  type is added to the current scope.
  \pre ctx != NULL
  \pre specifiers != NULL
  \pre declarator != NULL
  \param ctx  The context of the analyser.
  \param specifiers  The processed declaration specifiers of the typedef.
  \param declarator  The declarator of the typedef.
*/
void sa_simple_declaration::process_typedef(ptr<sa_context> ctx, ptr<sa_declaration_specifiers> specifiers,
		ptr<as_declarator> declarator)
{
	sa_simple_declaration_logger << "sa_simple_declaration::process_typedef()\n" << msg::eolog;

	lassert(ctx);
	lassert(specifiers);
	lassert(declarator);

	sa_simple_declaration_logger << "checking declaration specifier flags\n" << msg::eolog;
	
	if (specifiers->virtual_flag_get() || specifiers->friend_flag_get() || specifiers->explicit_flag_get() ||
		 specifiers->inline_flag_get()) {
		sa_simple_declaration_logger << "got some invalid flags\n" << msg::eolog;
		// TODO pt distinguish which flag to give more accurate error report
		// error: invalid specifier in typedef
		report << invalid_specifier_typedef << declarator->location_get();
	}
	
	sa_simple_declaration_logger << "extracting the ss_type from declarator\n" << msg::eolog;
	
	// extract the plain type
	ptr<sa_declarator_type> sdt = sa_declarator_type::create();
	ptr<ss_type> type = sdt->process(specifiers->type_get(),declarator);

	sa_simple_declaration_logger << "converting the name\n" << msg::eolog;

	ptr<as_name> name = declarator->name_get();

	// get the ss equivalent of the name
	ptr<ss_decl_name> decl_name = as_id_to_ss_decl_name::instance()->process(name->identifier_get());
	ptr<ss_decl_seq> current_scope = ctx->ss_get()->scope_get();

	// get the declarations in current scope
	ptr<ss_declaration_set_type> decls = li_by_name_in_single_scope::instance()->process(decl_name,current_scope);

	if (decls->size()) {
		sa_simple_declaration_logger << "found some declarations\n" << msg::eolog;

		// check any of the found, because they must be consistent
		ptr<ss_declaration> found_decl = *(decls->begin());
	
		/* TODO pt remove
	for (ss_declaration_set_type::iterator it = decls->begin(), end = decls->end();
			it != end; ++it) {
		ptr<ss_declaration> one = *it;

		 TODO pt this is wrong?
		if (one->contained_in_get() == current_scope) {
			sa_simple_declaration_logger << "found declaration in current scope\n" << msg::eolog;
		*/

		ptr<ss_typedef_definition> td = ss_declaration2ss_typedef_definition::instance()->process(found_decl);

		if (td) {
			sa_simple_declaration_logger << "the declaration is typedef\n" << msg::eolog;

			if (type == td->type_get()) {
				sa_simple_declaration_logger << "both types are the same\n" << msg::eolog;
			} else {
				sa_simple_declaration_logger << "error: the types are different\n" << msg::eolog;
				// error: name typedef conflict with previous typedef
				report << conflicting_types << declarator->location_get();
				report << previous_declaration << td->location_get();
			}
		} else {
			// error: name redeclared as different kind of symbol
			sa_simple_declaration_logger << "error: the declaration is different from typedef\n" << msg::eolog;
			report << redeclaration_different << declarator->location_get();
			report << previous_declaration << found_decl->location_get();
		}
		
	} else {
	
		ptr<source_location> loc = declarator->location_get();
		ptr<ss_declaration_time> dt = ss_declaration_time::create(loc->order_get());

		sa_simple_declaration_logger << "creating the typedef definition\n" << msg::eolog;

		// create the typedef
		ptr<ss_declaration> type_definition = ss_typedef_definition::create(
			loc, // location
			dt, // visible since
			dt, // declaration time
			decl_name, // declaration name
			current_scope, // contained in
			type, // ss type
			ss_linkage::create("C++",ss_linkage::LINKAGE_NO), // linkage
			ctx->ss_get()->access_specifier_get(), // access specifier
			ss_storage_class::ST_TYPEDEF // storage class
			);

		sa_simple_declaration_logger << "adding the typedef definition to current scope\n" << msg::eolog;

		// add the typedef into the current scope
		current_scope->contents_get()->push_back(type_definition);

		// TODO pt remove
		sa_simple_declaration_logger << "announce new declaration\n" << msg::eolog;
		// TMA: make the declaration known to egg
		declaration_finished->broadcast(type_definition);

	}

	sa_simple_declaration_logger << "sa_simple_declaration::process_typedef() end\n" << msg::eolog;
}


end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);

