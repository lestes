/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__sem___or_ics_hh___included
#define lestes__lang__cplus__sem___or_ics_hh___included

/*! \file
 * Header file for purposes of overload resolution
 * \author jikos
 */
package(lestes);
package(lang);
package(cplus);
package(sem);

typedef enum {
	OR_CV_CONST,
	OR_CV_VOLATILE,
	OR_CV_CONST_VOLATILE,
	OR_CV_MEMBER_PTR,
	OR_CV_PTR,
	OR_CV_BOOL,
	OR_CV_VOID,
	OR_CV_CLASS,
	OR_CV_PSEUDOREFERENCE,
	OR_CV_REFERENCE,
	OR_CV_SINT,
	OR_CV_NONE,
} or_cv_enum;

class ss_expression;
class ss_type;
class or_or_functional;
class or_ics_functional;
class or_ics_functional_for_std_conversion;
class or_ics_functional_for_lval_rval_conversion;
class or_ics_functional_for_user_conversion_by_constructor;
class or_ics_functional_for_user_conversion_by_conversion_function;
class or_ics_functional_for_compound_conversion;

/* is this FIXME? */
#define MAX(A,B) (A)>(B) ? (A) : (B)

ptr< or_ics_functional > or_find_ics(ptr< or_or_functional > source, ptr< ss_type > target);

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */

