/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*
 Memory manager for pi layer
 Declarations are added as they appear in block. 
 After leaving block, declaration is no more available.
*/
class mem_manager {
 public :
 //! add variable declaration
 void add_var(ptr <ss_declaration>);
 //! add function declaration
 void add_f(ptr <ss_declaration>);
 //! add class declaration
 void add_class(ptr <ss_declaration>);
 /*!
  * \brief return operand, where result of expression is stored
  *
  * Get distinguishes between pi_mem_decl and pin_mem_stack
  * dependently on ss-declaration.
  */
 pi_mem get(ptr <ss_declaration>);
}

