/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/common.hh>
#include <lestes/lang/cplus/sem/as_decl.g.hh>
#include <lestes/lang/cplus/sem/as_id_to_ss_decl_name.g.hh>
#include <lestes/lang/cplus/sem/li_by_name_in_single_scope.g.hh>
#include <lestes/lang/cplus/sem/lu_typedef.hh>
#include <lestes/lang/cplus/sem/sa_context.g.hh>
#include <lestes/lang/cplus/sem/sa_loggers.hh>
#include <lestes/lang/cplus/sem/sa_scope_of_as_name.g.hh>
#include <lestes/lang/cplus/sem/sa_usings.g.hh>
#include <lestes/lang/cplus/sem/ss_decl_name.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>
#include <lestes/lang/cplus/sem/ss_misc.g.hh>
#include <lestes/lang/cplus/sem/ss_type.g.hh>
#include <lestes/lang/cplus/sem/ss_using_target.g.hh>
#include <lestes/lang/cplus/syn/manager.hh>
#include <lestes/msg/logger.hh>
#include <lestes/msg/logger_util.hh>

#if 0
#include <lestes/lang/cplus/sem/as_access_specifier_to_ss_access_specifier.g.hh>
#include <lestes/lang/cplus/sem/as_base_specifier_to_ss_base_specifier.g.hh>
#include <lestes/lang/cplus/sem/as_class_key_to_ss_access_specifier.g.hh>
#include <lestes/lang/cplus/sem/as_class_key_to_ss_struct_base.g.hh>
#include <lestes/lang/cplus/sem/as_id_to_declaration_set.g.hh>
#include <lestes/lang/cplus/sem/sa_class_declaration.g.hh>
#include <lestes/lang/cplus/sem/sa_class_declaration.m.hh>
#include <lestes/lang/cplus/sem/sa_context.g.hh>
#include <lestes/lang/cplus/sem/sa_decl_seq_compound_pair_creator.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration_to_ss_decl_seq.g.hh>
#include <lestes/lang/cplus/sem/ss_enums.g.hh>
#include <lestes/lang/cplus/sem/ss_statement.g.hh>
#include <lestes/lang/cplus/sem/ss_type2info.g.hh>
#include <lestes/lang/cplus/syn/token.hh>
#endif


/*! \file
  \brief Handling of using declarations and directives.
  
  \author TMA
 */

package(lestes);
package(lang);
package(cplus);
package(sem);

/*!
  \invariant !syn::manager::in_disambiguation()
 */
void sa_usings::process_declaration(ptr < ::lestes::lang::cplus::sem::as_using_declaration > ud)
{
	lassert(!syn::manager::in_disambiguation());

	ptr < source_location > location = ud->location_get();
	ptr < ss_declaration_time > dt = ss_declaration_time::create(location->order_get());

	ptr < as_name > name = ud->name_get();
	lassert(name->was_qualified_get());

	ptr < ss_decl_name > decl_name = as_id_to_ss_decl_name::instance()->process(name->identifier_get());
	// FIXME: get scope from qualification
	ptr < ss_decl_seq > containing = sa_scope_of_as_name::instance()->extract(name);

	// FIXME: lookup in scope
	ptr < declaration_set_type > target_declarations = li_by_name_in_single_scope::instance()->process_preserve_usings(decl_name, containing);

	if ( target_declarations->empty() ) {
		lassert3(false, "declaration of used name not found",location);
	}

	for (declaration_set_type::iterator it = target_declarations->begin() ; it != target_declarations->end() ; ++it) {
		ptr < ss_declaration > target_decl = *it;

		// FIXME: a check shall be done whether the fund declaration
		// does not clash with an existing declaration in the current
		// scope.
		
		// FIXME: lookup in scope
		ptr < ss_declaration > real_decl = ss_using_target::instance()->process(target_decl);

		// FIXME: foreach looked up:
		sa_class_declaration_logger << "using\n" << msg::eolog;
		ptr < ss_using_declaration > using_decl = ss_using_declaration::create(
				location, // location
				dt, // visible
				dt, // decl time
				decl_name, //name
				sa_context_manager::instance()->current()->ss_get()->scope_get(), // contained in
				real_decl->type_get(), // type
				// FIXME: correct?
				ss_linkage::create("C++", ss_linkage::LINKAGE_EXTERNAL), // linkage
				//real_decl->linkage_get(), // linkage
				// FIXME: correct?
				sa_context_manager::instance()->current()->ss_get()->access_specifier_get(), // access specifier
				//real_decl->access_specifier_get(), // access specifier
				ss_storage_class::ST_NONE, // storage class
				target_decl, // using's target declaration
				real_decl // real declaration
				);
		// add to decl_seq:
		sa_context_manager::instance()->current()->ss_get()->scope_get()->contents_get()->push_back(using_decl);
	}
}

/*!
  \invariant !syn::manager::in_disambiguation()
 */
void sa_usings::process_directive(ptr < ::lestes::lang::cplus::sem::as_using_directive > ud)
{
	lassert2(!syn::manager::in_disambiguation(), "invariant");
	lassert3(false,"using directives not implemented", ud->location_get());
}

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);

