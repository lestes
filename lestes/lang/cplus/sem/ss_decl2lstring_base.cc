/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*!
	\file
	\brief Visistors returning lstring value for ss_declaration.
	\author jaz
*/

#include <lestes/lang/cplus/sem/ss_declaration.g.hh>
#include <lestes/lang/cplus/sem/ss_decl_name.g.hh>
#include <lestes/lang/cplus/sem/ss_misc.g.hh>
#include <lestes/lang/cplus/sem/ss_type.g.hh>
#include <lestes/lang/cplus/sem/ss_decl2lstring_base.g.hh>
#include <lestes/lang/cplus/sem/ss_decl_name2lstring_base.g.hh>
#include <lestes/lang/cplus/sem/ss_type2lstring_base.g.hh>
#include <lestes/md/tasm/tm_asm.mdg.hh>
#include <sstream>

package(lestes);
package(lang);
package(cplus);
package(sem);

using ::lestes::md::tasm::tm_asm;
using ::lestes::lang::cplus::sem::ss_declaration;

typedef list<srp<ss_declaration> > decl_list_type;

ptr< ss_decl2mangled_name > ss_decl2mangled_name::instance(){
	if ( !singleton_instance ) {
		singleton_instance = ss_decl2mangled_name::create();
	}
	
	singleton_instance->init();
		
	return singleton_instance;
}

void ss_decl2mangled_name::init() {
    level_set(0);
	substitutions->clear();
}

/*!
	\brief Gets substitution for a mangled string.
	
	If a substitution for mangled name exists then function returns it.
	Else function creates new substitution, adds it to substitutions map and returns
	original mangled name.
	
	\param mangled_name The mangled name.
	\return The substitution.
*/
lstring ss_decl2mangled_name::get_substitution(lstring mangled_name) {
	map<lstring,lstring>::iterator existing_subst = substitutions->find(mangled_name);
				
	if ( existing_subst==substitutions->end()) {
		//Add new substitution.
		::std::ostringstream oss_subst;
		oss_subst << "S";
					
		if ( substitutions->size() >0 ) {
			oss_subst << substitutions->size()-1;
		}
					 
		oss_subst << "_";
					
		(*substitutions)[mangled_name] = oss_subst.str(); 
					
		return mangled_name;
	} else {
		return existing_subst->second;
	}
	
	lassert(false);
	return lstring();
}

void ss_decl2mangled_name::visit_ss_namespace_definition(ptr< ::lestes::lang::cplus::sem::ss_namespace_definition > decl) {
	lassert(decl);
	
	level++;	
	
	::std::ostringstream oss;
	
	if ( decl->contained_in_get() != ss_decl_seq::root_instance() ){
		//Is nested -> prepend encoded namespaces
		oss <<  process(decl->contained_in_get()->parent_get()->declared_by_get());
	}
	
	//Get mangled namespace's name.
	lstring name = decl->name_get()->accept_ss_decl_name2lstring_base(ss_decl_name2mangled_name::instance());
	
	//Create mangled symbol.
	oss << name.length() << name;
	
	return result_set(oss.str());
}

void ss_decl2mangled_name::visit_ss_object_declaration(ptr< ::lestes::lang::cplus::sem::ss_object_declaration > decl) {
	lassert(decl);
	
	level++;	
	
	int my_level = level;
	
	::std::ostringstream oss;

	//Get mangled name.	
	lstring name = decl->name_get()->accept_ss_decl_name2lstring_base(ss_decl_name2mangled_name::instance());

	//mangle namespace
	lstring enclosing = lstring();
	if ( decl->contained_in_get() != ss_decl_seq::root_instance()) {
		::std::ostringstream oss1;
		//Is nested? -> encode namespaces
		oss1 << process(decl->contained_in_get()->declared_by_get());
		oss1 << name.length();
		
		enclosing = oss1.str();
	}
	
	if ( my_level==1 ) {
		//This is top level object.
		if ( !enclosing.empty() ) {
			oss << "_ZN" << enclosing << name.length() << name << "E";
		} else {
			oss << name;
		}
	} else {
		//This is enclosing object declaration for another one. 
		if ( !enclosing.empty() ) {
			oss << enclosing << name.length() << name;
		} else {
			oss << name.length() << name;
		}
	}
	
	return result_set(oss.str());
}

void ss_decl2mangled_name::visit_ss_bitfield_declaration(ptr< ::lestes::lang::cplus::sem::ss_bitfield_declaration > decl) {
	lassert(decl);
	
	level++;	
	
	//A bitfield can be declared in class/struct only. It can't be static. -> No mangling needed.
	lassert(false);
	return result_set(lstring());
}

void ss_decl2mangled_name::visit_ss_parameter_declaration(ptr< ::lestes::lang::cplus::sem::ss_parameter_declaration > decl) {
	lassert(decl);
	
	level++;	
	
	return result_set(ss_type2mangled_id::instance()->process(decl->type_get()));
}

void ss_decl2mangled_name::visit_ss_structure_declaration(ptr< ::lestes::lang::cplus::sem::ss_structure_declaration > decl) {
	lassert(decl);
	
	level++;	
	
	//TODO
	lassert(false);
	return result_set(lstring());
}

void ss_decl2mangled_name::visit_ss_enum_definition(ptr< ::lestes::lang::cplus::sem::ss_enum_definition > decl) {
	lassert(decl);
	
	level++;	
	
	//TODO
	lassert(false);
	return result_set(lstring());
}

void ss_decl2mangled_name::visit_ss_typedef_definition(ptr< ::lestes::lang::cplus::sem::ss_typedef_definition > decl) {
	lassert(decl);
	
	level++;	
	
	//TODO
	lassert(false);
	return result_set(lstring());
}

void ss_decl2mangled_name::visit_ss_function_declaration(ptr< ::lestes::lang::cplus::sem::ss_function_declaration > decl) {
	lassert(decl);
	
	level++;	

	int my_level = level;
		
	::std::ostringstream oss;
		
	if ( decl->is_entry_point() ) {		
		return result_set(tm_asm::ent_main_name_get());
	} else {	
	
		
		lstring enclosing = lstring();
	
		//mangle namespace
		if ( decl->contained_in_get() != ss_decl_seq::root_instance()) {
			::std::ostringstream oss1;
			//Is nested? -> encode namespaces
			oss1 << process(decl->contained_in_get()->declared_by_get());
		
			enclosing = oss1.str();
		}
	
		lstring name = decl->name_get()->accept_ss_decl_name2lstring_base(ss_decl_name2mangled_name::instance());
		
		::std::ostringstream oss2;
		//mangle parameters
		if ( !decl->parameters_get() ) {
			//No parameters.
			oss2 << "v";
		} else {
			//Encode parameters
			ptr<decl_list_type> params = decl->parameters_get()->contents_get();
			lassert(params);
			
			decl_list_type::iterator it;
			for(it=params->begin(); it!=params->end(); ++it) {
				lstring mangled_parameter = process(*it);
				
				if ( mangled_parameter.length()>1 ) {
					//Substitute builtin datatypes only.
					oss2 << get_substitution(mangled_parameter);
				} else {
					oss2 << mangled_parameter;
				}
			}
		}
		
		lstring parameters = oss2.str();
		
		if ( my_level==1 ) {
			//This is top level declaration.
			if ( !enclosing.empty() ) {
				oss << "_ZN" << enclosing << name.length() << name << "E" << parameters;
			} else {
				oss << "_Z" << name.length() << name << parameters;
			}
		} else {
			//This is enclosing function declaration for another declaration.
			if ( !enclosing.empty() ) {
				oss << enclosing << name.length() << name << parameters;
			} else {
				oss << name.length() << name << parameters;
			}
		}

		return result_set(oss.str());
	}
	lassert(false);
	return result_set(NULL);
}

void ss_decl2mangled_name::visit_ss_enumerator_declaration(ptr< ::lestes::lang::cplus::sem::ss_enumerator_declaration > decl) {
	lassert(decl);
	
	level++;	
	
	//TODO
	lassert(false);
	return result_set(lstring());
}

void ss_decl2mangled_name::visit_ss_using_declaration(ptr< ::lestes::lang::cplus::sem::ss_using_declaration > decl) {
	lassert(decl);
	
	level++;	
	
	//TODO
	lassert(false);
	return result_set(lstring());
}

void ss_decl2mangled_name::visit_ss_builtin_operator_declaration(ptr< ::lestes::lang::cplus::sem::ss_builtin_operator_declaration > decl) {
	lassert(decl);
	
	level++;	
	
	//TODO
	lassert(false);
	return result_set(lstring());
}


void ss_decl2mangled_name::visit_ss_compound_stmt_declaration(ptr< ::lestes::lang::cplus::sem::ss_compound_stmt_declaration > decl) {
	lassert(decl);
	
	level++;	
	
	lassert(false);
	//TODO
	return result_set(lstring());
}

void ss_decl2mangled_name::visit_ss_method_declaration(ptr< ::lestes::lang::cplus::sem::ss_method_declaration > decl) {
	lassert(decl);
	
	level++;	
	
	lassert(false);
	//TODO
	return result_set(lstring());
}

void ss_decl2mangled_name::visit_ss_fake_declaration(ptr< ::lestes::lang::cplus::sem::ss_fake_declaration > decl){
	lassert(decl);
	
	level++;	
	
	lassert(false);
	//TODO
	return result_set(lstring());
}

void ss_decl2mangled_name::visit_ss_injected_class_declaration(ptr< ::lestes::lang::cplus::sem::ss_injected_class_declaration > decl){
	lassert(decl);
	
	level++;	
	
	lassert(false);
	//TODO
	return result_set(lstring());
}

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);

