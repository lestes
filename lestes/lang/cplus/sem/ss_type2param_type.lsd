<?xml version="1.0" encoding="iso-8859-1"?>
<!--
	The lestes compiler suite
	Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
	Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
	Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
	Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
	Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
	Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
	Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; version 2 of the License.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	See the full text of the GNU General Public License version 2, and
	the limitations in the file doc/LICENSE.

	By accepting the license the licensee waives any and all claims
	against the copyright holder(s) related in whole or in part to the
	work, its use, and/or the inability to use it.
-->
<lsd xmlns="http://lestes.jikos.cz/schemas/lsd" xmlns:h="http://www.w3.org/TR/REC-html40">

	<file-name>ss_type2param_type</file-name>

	<dox file="both">
		<bri>Normalization of parameter types.</bri>
		<det>Visitor performing normalization of SS types in parameters.</det>
	</dox>

	<packages>
		<p>lestes</p>
		<p>lang</p>
		<p>cplus</p>
		<p>sem</p>
	</packages>

	<imports>
		<i>lestes/common.hh</i>
		<i>lestes/std/list.hh</i>
		<i>lestes/lang/cplus/sem/ss_type_visitor.v.g.hh</i>
	</imports>
	
	<implementation-imports>
		<i>lestes/lang/cplus/sem/ss_type.g.hh</i>
	</implementation-imports>

	<using-class name="object" packages="::lestes::std::"/>
	
	<foreign-class name="ss_type">
		<p>lestes</p>
		<p>lang</p>
		<p>cplus</p>
		<p>sem</p>
	</foreign-class>
	<foreign-class name="ss_struct_base">
		<p>lestes</p>
		<p>lang</p>
		<p>cplus</p>
		<p>sem</p>
	</foreign-class>
	<foreign-class name="ss_builtin_type">
		<p>lestes</p>
		<p>lang</p>
		<p>cplus</p>
		<p>sem</p>
	</foreign-class>
	<foreign-class name="ss_const">
		<p>lestes</p>
		<p>lang</p>
		<p>cplus</p>
		<p>sem</p>
	</foreign-class>
	<foreign-class name="ss_volatile">
		<p>lestes</p>
		<p>lang</p>
		<p>cplus</p>
		<p>sem</p>
	</foreign-class>

	<include href="ss_type_visitor.v.lsd" />

	<xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="ss_type_visitor.v.lsd">
		<xi:fallback/>
	</xi:include>

	<class name="ss_type2param_type" base="ss_type2param_type_base">
		<dox>
			<bri>Normalizes types of parameters.</bri>
			<det>
				Performs normalization of SS types in function parameters.
				Function types are transformed into pointers to function.
				Arrays of T are transformed into pointers to T.
			</det>
		</dox>
		<declare-abstract-methods prefix="visit_" skip-defined="yes" />
		<visit-return-method name="process" get-from="type" />
		<method name="default_ss_struct_base" type="void">
			<dox>
				<bri>Visitor method for all SS types in ss_struct_base hierarchy.</bri>
			</dox>
			<param name="obj" type="ss_struct_base" />
		</method>
		<method name="default_ss_builtin_type" type="void">
			<dox>
				<bri>Visitor method for all SS types in ss_builtin_type hierarchy.</bri>
			</dox>
			<param name="obj" type="ss_builtin_type" />
		</method>
		<field name="type" type="ss_type" set="none" alt="none" check="" init="NULL">
			<dox>
				<bri>The normalized type.</bri>
				<det>The processed type normalized for usage in function parameter.</det>
			</dox>
		</field>
	</class>
</lsd>
