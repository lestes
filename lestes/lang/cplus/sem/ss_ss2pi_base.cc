/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <algorithm>
#include <iostream>
#include <lestes/std/list.hh>
#include <lestes/std/pair.hh>
#include <lestes/std/reflect.hh>
#include <lestes/msg/logger.hh>
#include <lestes/msg/logger_util.hh>

#include <lestes/backend_v2/interface/backend_data_builder.g.hh>
#include <lestes/md/mem/mem_alloc_manager.g.hh>
#include <lestes/md/types/copy_constructor_call_generator.g.hh>
#include <lestes/md/functions/function_parameter_accessor.g.hh>
#include <lestes/md/literals/literal_loader.g.hh>
#include <lestes/md/literals/literal_info.g.hh>
#include <lestes/md/mem/memory_allocator_bases.g.hh>
#include <lestes/md/types/tm_data_type_base.g.hh>
#include <lestes/md/registers/tm_register_base.g.hh>
#include <lestes/md/types/ss_type2tm_type_convertor.g.hh>
#include <lestes/backend_v2/intercode/pi_mem_factory.g.hh>

#include <lestes/lang/cplus/sem/ss_type.g.hh>
#include <lestes/lang/cplus/sem/ss_misc.g.hh>
#include <lestes/lang/cplus/sem/ss_expr_binary_op.g.hh>
#include <lestes/lang/cplus/sem/ss_expr_unary_op.g.hh>
#include <lestes/lang/cplus/sem/ss_expr_funcall.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>
#include <lestes/lang/cplus/sem/ss_type_builtin.g.hh>

#include <lestes/lang/cplus/sem/ss_finders.g.hh>
#include <lestes/lang/cplus/sem/ss_ss2pi_base.g.hh>
#include <lestes/lang/cplus/sem/ss_ss2pi_typedefs.hh>
#include <lestes/lang/cplus/sem/ss_literal_info.g.hh>
#include <lestes/lang/cplus/sem/ss_type2info.g.hh>
#include <lestes/lang/cplus/sem/ss_ss2pi_typedefs.hh>
#include <lestes/lang/cplus/sem/sem.hh>


package(lestes);
package(lang);
package(cplus);
package(sem);

using ::std::cerr;
using ::std::max;
using namespace ::lestes::std;
using namespace ::lestes::msg;
using namespace ::lestes::backend_v2::intercode;
using namespace ::lestes::backend_v2::interface;
using namespace ::lestes::backend_v2::structs;
using namespace ::lestes::md::types;
using namespace ::lestes::md::mem;
using namespace ::lestes::md::registers;
using namespace ::lestes::md::literals;
using namespace ::lestes::md::functions;


////////////////////////////////////		Loggers & dumps 		//////////////////////////////


declare_logger(dtl);	//destructor tables control
declare_logger(fll);	//floating control
declare_logger(spl);	//sequence points control
declare_logger(gnl);	//general ss2pi logger

initialize_logger( dtl, "destr_tab", sem_logger );
initialize_logger( fll, "flow", sem_logger );
initialize_logger( spl, "SP", sem_logger );
initialize_logger( gnl, "ss2pi", sem_logger );

//#define log_flow_indent "  "
#define log_flow_indent "  "
int log_count = 0;
int log_indent(ptr< ::lestes::msg::logger > flow,int ind){	//indentation for control_flow logs
	for (int i=0; i<ind; i++){
		flow << log_flow_indent;
	}
	return ind;
}

//#define plog(x) llog(x)
//#define plog(x) llog_plain(x)
#define plog

//#define entry(a) plog(fll) << "	in  " << x->uid_get() << " (" << x->reflection_get()->back()->name_get() << ")\n" << eolog
#define entry(a) log_indent(fll,log_count++), plog(fll) <<"       in  " << x->uid_get() << " (" << a << ")\n" << eolog
#define outry(a) log_indent(fll,--log_count), plog(fll) <<"       out " << x->uid_get() << " (" << a << ")\n" << eolog

/*!
  Dumps objects hierarchy starting from dumpable x.
*/
void dump( ptr< object >x){
plog(gnl)<<"..dumping..\n";
	::std::ofstream f("dump.xml");
	::lestes::std::dumper::dump(f,x);
	f.close();
}

/*!
  Dumps objects hierarchy starting from dumpable x.
*/
void dumb( ptr< object >x){
plog(gnl)<<"..dumbing..\n";
	::std::ofstream f("dumb.xml");
	::lestes::std::dumper::dump(f,x);
	f.close();
}

/*!
  Dumps objects hierarchy starting from dumpable x.
  Usage a=func(b,dung(c),d);
*/
template <typename X> 
ptr<X> dung( ptr< X >x){
plog(gnl)<<"..dumbing..\n";
	::std::ofstream f("dumb.xml");
	::lestes::std::dumper::dump(f,x);
	f.close();
	return x;
}


/*!
  Print out top_level sequence points. For debuging purposes.
*/
void summary_seq_points(ptr< ss_sp > x){
 {
	while (x->psp_get()) x=x->psp_get();
	ptr< ss_sp > tmp, tmp2;
	
	for (tmp=x; tmp->nsp_get(); tmp=tmp->nsp_get()){
		plog(spl)<<tmp->uid_get()<<"->";
	}
	plog(spl)<<tmp->uid_get()<<"\n";
	
	for (tmp2=tmp; tmp2->psp_get(); tmp2=tmp2->psp_get()){
		plog(spl)<<tmp2->uid_get()<<"->";
	}
	plog(spl)<<tmp2->uid_get()<<"\n";
 }
 {
	ptr< pi_pi >X = ss_sp2pi_sp(x);
	ptr< pi_pi > tmp, tmp2;
	
	for (tmp=X; tmp->nsp_get(); tmp=tmp->nsp_get()){
		plog(spl)<<tmp->uid_get()<<"->";
	}
	plog(spl)<<tmp->uid_get()<<"\n";
	
	for (tmp2=tmp; tmp2->psp_get(); tmp2=tmp2->psp_get()){
		plog(spl)<<tmp2->uid_get()<<"->";
	}
	plog(spl)<<tmp2->uid_get()<<"\n"<<eolog;
 }
}


///////////////////////////////			Common functions	///////////////////////////////////


/*!
  Main entry point for ss2pi transformation.
*/
void ss2pi_start(ptr< ss_translation_unit > x){entry("start");

	//	dumb(x);	//To mantain constant numbers for structures in dumps
	      
	ptr< variable_map_type > namespace_variable_map = variable_map_type::create();	//namespace-scope map
	
	ptr< ss_stmt2pi > stmt_visitor = 			//new visitor - new destructor list has to be created
		ss_stmt2pi::create( NULL, NULL, namespace_variable_map );
	
	x->global_scope_get()->compound_stmt_get()->accept_ss_statement_visitor(stmt_visitor);
	
outry("start");
}

/*!
  Converts returned result from expression visitor for requested type.
  TODO: reformulate dncast via visitors on pi_operand structures
*/
ptr< pi_mem >pi_operand2pi_mem(ptr< pi_operand > x){
	
	ptr<pi_mem>p = x.dncast<pi_mem>();
	return p;
}

/*!
  Converts returned result from expression visitor for requested type.
  TODO: reformulate dncast via visitors on pi_operand structures
*/
ptr< pi_mem_ptr_deref >pi_operand2pi_mem_pointer(ptr< pi_operand > x){
	
	ptr<pi_mem_ptr_deref>p = x.dncast<pi_mem_ptr_deref>();
	return p;
}

/*!
  Converts returned result from expression visitor for requested type.
  TODO: reformulate dncast via visitors on pi_operand structures
*/
ptr< pi_preg >pi_operand2pi_preg(ptr< pi_operand > x){

	ptr<pi_preg>p = x.dncast<pi_preg>();
	return p;
}


/*!
  Computation of origin to get an apropriate pi_mem for pi_mem_factory.
  Premise for right construction: 1 side effect dont interfere(value change, volatile access)
  	with more than one entity.
	
  Arguments:
	xsp points to sequence potin wherefrom we start our search (usually psp of current expression).
	
  Used variables:
 	psp points to start of fullexpression, where wee look for previous side effects
*/
ptr< pi_pi > ss_expr2pi::compute_origin(ptr< pi_mem_factory > x, ptr< ss_sp > xsp){

	for ( ptr< ss_sp > i=xsp; i!=psp ; i=i->psp_get()){			//or all seq.points only inside fullexpression
	
		list <srp <ss_se> >::iterator ise = i->pse_get()->begin();	//search for sideeffect, which interfere with given pi_mem_factory
		for ( ; ise!=i->pse_get()->end(); ise++){
			ptr< ss_expr2destination > dest_visitor = ss_expr2destination::create();
			ptr< ss_expression > dest = dest_visitor->get_destination((*ise)->from_get());
			
			if (dest) {						// if side effect with destination
			
				ptr< pi_mem > accessed;		//memory placeholder accessed by side effect
				
				expression_results_map_type::iterator expr_result = expr2operand_map->find(dest);	//get result of evaluation
				
				if ( expr_result == expr2operand_map->end() ) 		//this expression has not been evaluated yet
					accessed = this->returns_mem(dest);			//evaluate it now
				  else
					 accessed = pi_operand2pi_mem( (*expr_result).second ); 
					 
				if (accessed->factory_get() == x){		//we found side effect, where we accessed given x
					return accessed->origin_get();
				}//if acc
			}//if dest
		}//for se
	}//for psp
	
	return ss_sp2pi_sp(psp); // When no side effect inside fullexpression found, return sequence point on the begining of fullexpression
}

/*!
  Returning value from transforming expression visitor.
*/
ptr< ::lestes::backend_v2::intercode::pi_preg > ss_expr2pi::returns_preg(const ptr < ss_expression > & o){
	ptr <pi_operand> res=this->returns(o);
	return pi_operand2pi_preg(res);
}

/*!
  Returning value from transforming expression visitor.
*/
ptr< ::lestes::backend_v2::intercode::pi_mem > ss_expr2pi::returns_mem(const ptr < ss_expression > & o){
	ptr <pi_operand> res=this->returns(o);
	return pi_operand2pi_mem(res);
}

/*!
  Returning value from transforming expression visitor.
*/
ptr< ::lestes::backend_v2::intercode::pi_mem_ptr_deref > ss_expr2pi::returns_mem_pointer(const ptr < ss_expression > & o){
	ptr <pi_operand> res=this->returns(o);
	return pi_operand2pi_mem_pointer(res);
}
	

/*
  Function for recursive building of pi_sp from ss_sp.
  The whole transformation of sequence points is lazy in the sense, that we transform only
  sequence points, which are available through psp and nsp.
  Stop mark for sequence points in ss as well as in pi layer is NULL.
  TODO: reformulate via maps, without ss_sp.pi_sp field.
*/
ptr< pi_sp > ss_sp2pi_sp(ptr < ss_sp > sp){
	
	if (sp->pi_spe_get()) return sp->pi_spe_get();				//already constructed
	
	ptr< pi_sp > empty = NULL;
	
	ptr<pi_sp> tmp = pi_sp::create (NULL,NULL,sp->level_get());	//Dont mix ordering of commands below !
	
	sp->pi_spe_set(tmp);
	
	if (sp->psp_get()) tmp->psp_set(ss_sp2pi_sp(sp->psp_get()));		//recursion
	if (sp->nsp_get()) tmp->nsp_set(ss_sp2pi_sp(sp->nsp_get()));
	
	plog(spl) << "	SP: ss " <<sp->uid_get() <<" -> pi " << tmp->uid_get() << "\n" << eolog;
	
	ptr<backend_data_builder> initial_structure_builder = backend_data_builder::instance();
	initial_structure_builder->add_sp(tmp);		//backend shall know about constructed sequnce point

	return tmp;
}

/////////////////////////		Expression visitors		/////////////////////////////////


/*!
   Check whether given expression was already transformed.
   In such a case result is set to according result of evaluated subexpression
   and returned TRUE.
*/
bool ss_expr2pi::evaluated(ptr< ss_expression > x){
	
	if ( expr2operand_map->find(x)!=expr2operand_map->end() ) {
		result = (*expr2operand_map)[x]; 
		outry("out - already evaluated");
		return true;
	 }
	return false;
}

/*!
  Assignment transformation.
  It distinguishes between store and store via pointer dependently on returned result from subexpression.
*/
void ss_expr2pi::visit_ss_assign(ptr< ::lestes::lang::cplus::sem::ss_assign > x ) {entry("=");
	
		
	if (evaluated(x)) { returned_pointer = (*expr2operand_map)[x]->kind_get()==pi_operand::MEM_PTR_DEREF; return; }
	
	ptr< pi_pi > res;	
	ptr< pi_preg > rt = this->returns_preg(x->right_get());	//rvalue which to store

	if (is_returned_pointer(x->left_get())){	//real evaluation of left operand
							//store via pointer
		ptr< pi_mem_ptr_deref > lft = this->returns_mem_pointer(x->left_get());		//pointer to be dereferenced
		ptr< pi_mem_ptr_deref > new_lft = lft->factory_get()->get_mem(NULL).dncast<pi_mem_ptr_deref>();//pozn tohle ma byt v tmp.dest
		
		if (!lft || !rt) lassert2(false,"ss_assign operands incorrect.");
		
		lint level = max ( 						//get level for current pseudoinstruction
				max( x->psp_get()->level_get(), x->nsp_get()->level_get() ),
				rt->origin_get()->level_get() 
//				max( lft->origin_get()->level_get(), rt->origin_get()->level_get() )
			);
	
		//JAZ
		level = 1 + max(level,(lint)new_lft->factory_get().dncast<pi_mf_ptr_deref>()->addr_get()->origin_get()->level_get());
		
		ptr< pi_stp > tmp = pi_stp::create(					//store equivalent to assignemnt
				ss_sp2pi_sp(x->psp_get()),
				ss_sp2pi_sp(x->nsp_get()),
				level,
				new_lft,
				//((lft->factory_get()).dncast<pi_mf_ptr_deref>())->addr_get(),
				rt,
				ss_type2tm_type_convertor::instance()->convert(		///pointer on destination type
								ss_pointer::instance(x->type_get().dncast<ss_pseudoreference>()->what_get())
								),
				rt->type_get()
				
			); 
		
		new_lft->origin_set(tmp);
		res=tmp;
		(*expr2operand_map)[x]=result=new_lft;//pozn1
		returned_pointer = true; //jaz
	 }
	   else			//store to usual lvalue
	 {
		ptr< pi_mem > lft = this->returns_mem(x->left_get());		//lvalue where to store
	
		if (!lft || !rt) lassert2(false,"ss_assign operands incorrect.");
		
		lint level = 1+max ( 						//get level for current pseudoinstruction
				max( x->psp_get()->level_get(), x->nsp_get()->level_get() ),
				rt->origin_get()->level_get() 
			);
	
		ptr< pi_st > tmp = pi_st::create(					//store equivalent to assignemnt
				ss_sp2pi_sp(x->psp_get()),
				ss_sp2pi_sp(x->nsp_get()),
				level,
				lft,			//this is only fake, new pi_mem for lft will be here
				rt,
				ss_type2tm_type_convertor::instance()->convert(x->type_get())
			);
		ptr< pi_mem > new_lft = lft->factory_get()->get_mem(tmp);
		tmp->destination_set(new_lft);
		
		res=tmp;
		(*expr2operand_map)[x]=result=new_lft;
	 }//else
	 
	backend_data_builder::instance()->add_pi(res);
	backend_data_builder::instance()->add_rail();
	

outry("=");
}

/*!
  Load proper pi_mem_factory for a given declaration.
  In case of & references, which are treated as pointers, 
  pointer is read and flag for dereference is set.
  For referenced variables origin has to be computed, to determine
  correct data flow (e.g. in  a=1+a, where a is used as 
  both a load's source and a store's target).
*/
void ss_expr2pi::visit_ss_var_ref(ptr< ::lestes::lang::cplus::sem::ss_var_ref > x) {entry("VR");

	bool to_be_dereferenced=ss_type2info::create()->process(x->type_get()) == ss_type2info::SS_REFERENCE;
	if  (to_be_dereferenced) { plog(gnl)<<x->uid_get()<<" pointer to be dereferenced\n"; }
	
	if (evaluated(x)) { returned_pointer=to_be_dereferenced;  return; }
	
	ptr< pi_mem_factory > var = caller->mem_variable_get(x->var_decl_get());
	lassert2(var,"Variable reference not found in variable map.");

        if (to_be_dereferenced) {				//references shall be treated as pointers

		ptr< pi_mem > lval  = var->get_mem(compute_origin(var,x->psp_get()));
		lassert(lval->kind_get()!=pi_operand::MEM_PTR_DEREF);
		
		ptr< pi_preg > rval = pi_preg::create(NULL,ss_type2tm_type_convertor::instance()->convert(x->type_get()));
	
		ulint level = max(x->psp_get()->level_get(),x->nsp_get()->level_get());
	
		ptr< pi_ld > tmp = pi_ld::create (
				ss_sp2pi_sp(x->psp_get()), ss_sp2pi_sp(x->nsp_get()),
				1+max(level,lval->origin_get()->level_get()),
				rval,
				lval,
				rval->type_get()
			);
			
		rval->origin_set(tmp);
		
		ptr<backend_data_builder> initial_structure_builder = backend_data_builder::instance();
		initial_structure_builder->add_pi(tmp);	
		initial_structure_builder->add_rail();


		ptr< pi_mf_ptr_deref > pointer_factory = pi_mf_ptr_deref::create(		//dereferenced memory placeholder
					NULL,
					ss_type2tm_type_convertor::instance()->convert(x->type_get()),
					rval
				);

		returned_pointer=true;		//dereference have to be checked on higher level 
		
		(*expr2operand_map)[x] = result =  pointer_factory->get_mem(rval->origin_get());

	 } //ss_reference
	 
		else	//standard var_ref
		 (*expr2operand_map)[x] = result = var->get_mem(compute_origin(var,x->psp_get())); //get pi_mem for variable x
	
outry("VR");
}

/*!
  lvalue->rvalue conversion or indirect load via pointer 
*/
void ss_expr2pi::visit_ss_get(ptr< ::lestes::lang::cplus::sem::ss_get > x) {entry("get");
	
	if (evaluated(x)) return;
	ptr< pi_pi > res;
	
	//ptr< pi_operand > tmp_lval  = this->returns(x->expr_get());
	bool to_be_dereferenced=ss_type2info::create()->process(x->type_get()) == ss_type2info::SS_REFERENCE;
	
	if (is_returned_pointer(x->expr_get()) || to_be_dereferenced  /*tmp_lval->kind_get()==pi_operand::MEM_PTR_DEREF */ )		//real evaluation run here
	 {		//we got pointer for dereference, so load will be via pointer
	 
	 	ptr < pi_mem_ptr_deref > lval = this->returns_mem_pointer(x->expr_get());	//pointer to be dereferenced
		//ptr < pi_mem_ptr_deref > lval = tmp_lval.dncast<pi_mem_ptr_deref>();
		
		ptr< pi_preg > rval = pi_preg::create(NULL,ss_type2tm_type_convertor::instance()->convert(x->type_get()));
		
		ulint level = max(x->psp_get()->level_get(),x->nsp_get()->level_get());

		ptr< pi_ldp > tmp = pi_ldp::create (
				ss_sp2pi_sp(x->psp_get()), ss_sp2pi_sp(x->nsp_get()),
				1+max(level,lval->origin_get()->level_get()),
				rval,
				//((lval->factory_get()).dncast<pi_mf_ptr_deref>())->addr_get(),
				lval,
				rval->type_get(),
				ss_type2tm_type_convertor::instance()->convert(		///pointer on source type
								ss_pointer::instance(x->type_get())
								)
			);

		rval->origin_set(tmp);
		res=tmp;
		(*expr2operand_map)[x] = result = rval;
		
	 } else
	 {		//standard rvalue_lvalue conversion
		ptr< pi_operand > lval = this->returns(x->expr_get()); 
		
		lassert(lval->kind_get()!=pi_operand::MEM_PTR_DEREF);
		
		ptr< pi_preg > rval = pi_preg::create(NULL,ss_type2tm_type_convertor::instance()->convert(x->type_get()));
	
		ulint level = max(x->psp_get()->level_get(),x->nsp_get()->level_get());
	
		ptr< pi_ld > tmp = pi_ld::create (
				ss_sp2pi_sp(x->psp_get()), ss_sp2pi_sp(x->nsp_get()),
				1+max(level,lval->origin_get()->level_get()),
				rval,
				lval,
				rval->type_get()
			);
		
		/*
		if ( tmp->uid_get()==1638 ) {
			lassert(false);
		}
		*/	

		rval->origin_set(tmp);
		res=tmp;
		(*expr2operand_map)[x] = result = rval;
	}
	
	ptr<backend_data_builder> initial_structure_builder = backend_data_builder::instance();
	initial_structure_builder->add_pi(res);	
	initial_structure_builder->add_rail();

outry("get");
}

/*!
  Transform given expression and check whether pointer for dereference is returned.
  Used by (vol)get and assign for identification of dereferenced pointer in operand,which cause load/store via returned pointer.
  This can happen via dereferience of ss_pointer or via var_ref of variable with ss_reference type. 
  */
bool ss_expr2pi::is_returned_pointer(ptr< ss_expression > x){
	returned_pointer=false;
	this->returns(x);
	if (returned_pointer) {
		returned_pointer=false;
		plog(gnl) << x->uid_get() << " returned pointer\n";
		return true;
		}
	return false;
}

void ss_expr2pi::visit_ss_dereference(ptr< ::lestes::lang::cplus::sem::ss_dereference > x) {entry("deref");

//	plog(gnl) << x->uid_get() << " returned pointer\n";
	
	if (evaluated(x)) {returned_pointer=true; return;}
		
	ptr< pi_preg > val = this->returns_preg( x->expr_get() );
	
	ptr< pi_mf_ptr_deref > pointer_factory = pi_mf_ptr_deref::create(		//dereferenced memory placeholder
					NULL,
					ss_type2tm_type_convertor::instance()->convert(x->type_get()),
					val
				);

	(*expr2operand_map)[x] = result =  pointer_factory->get_mem(val->origin_get());
	returned_pointer=true;		//dereference have to be checked on higher level 

outry("deref");
}


void ss_expr2pi::visit_ss_literal(ptr< ::lestes::lang::cplus::sem::ss_literal > x) {entry("lit");
	
	if (evaluated(x)) return;
	ptr < ss_linfo2pi_linfo > cnv = ss_linfo2pi_linfo::create();
	ptr< literal_info_base > pinfo = cnv->get_pinfo(x->value_get());
	
	
	ptr < pi_lit > lit = pi_lit::create(NULL,pinfo->type_get(),pinfo); 	//origin of literal is always NULL
	ptr< pi_preg > dest= pi_preg::create(NULL,pinfo->type_get());
	
	ptr<backend_data_builder> builder = backend_data_builder::instance();
	builder->add_pis( literal_loader::instance()->
				generate_load(dest, lit, ss_sp2pi_sp(x->psp_get()), ss_sp2pi_sp(x->nsp_get())));
	builder->add_rail();	

	(*expr2operand_map)[x]=result=dest;
	
outry("lit");	
}

#define pi_operand_void NULL

void ss_expr2pi::visit_ss_funcall(ptr< ::lestes::lang::cplus::sem::ss_funcall > x) {entry("funcall");

	if (evaluated(x)) return;
	
	ptr<mem_alloc_manager> allocator = mem_alloc_manager::instance();
	
	ptr< ss_destructor_tables > dts = ss_destructor_tables::instance();
	ptr<backend_data_builder> builder = backend_data_builder::instance();
	
	ptr<pi_mem_factory> rv;		//return value
	ptr<pi_abstract_function_call> fcall;

	if (x->type_get()->is_void()) {
	 	
		rv=pi_operand_void;
		fcall = pi_callv::create (
			ss_sp2pi_sp(x->psp_get()), ss_sp2pi_sp(x->nsp_get()),
			0,				//level - will be filled later
			//jaz list_mem_type::create(),				//funcall args - will be filled later
			list<srp<pi_operand> >::create(),
			NULL,				//this; this will be filled only through access operators
			x->function_get()		//declaration
			);				
		(*expr2operand_map)[x]=result=pi_operand_void;		//checked for return void funcall();
		}
		
	else 				//non void return
		{
		
		
		ptr<tm_data_type_base> trvt = ss_type2tm_type_convertor::instance()->convert(x->type_get());
		ptr< pi_call > tmp = pi_call::create (					//tmp needed for access to fields
			ss_sp2pi_sp(x->psp_get()), ss_sp2pi_sp(x->nsp_get()),
			0,				//level - will be filled later
			list<srp<pi_operand> >::create(),
			//list_mem_type::create(),				//funcall args - will be filled later
			NULL,				//this; this will be filled only through acces operators
			trvt,		//type  
			NULL,				//rv
			x->function_get() 		//declaration
			);
			
		/* jaz	
		rv = allocator->allocate_fcall_par_ret_val(
					caller->current_function_get(),		//caller
					tmp					//calee
				);
		*/
		
		if ( trvt->return_reg_get() ) {
			//type is returned in register
			rv = pi_mf_preg::create(NULL,trvt, pi_preg::create(tmp,trvt));	
		} else {
			//type is returned in memory 
			rv = pi_mf_stack::create(NULL,trvt, 0);	
		}
				
		tmp->rv_set(rv->get_mem(tmp));
		dts->destructor_add_to_destr(temporaries,x->type_get(),rv); 
				
		fcall=tmp;
		(*expr2operand_map)[x]=result=tmp->rv_get();
		}//else

	//TODO ellipsis
	ulint level=max(fcall->psp_get()->level_get(),fcall->nsp_get()->level_get());		// max level from seqeuence points and argumets 
	
	/*		Parameters		*/
	//ptr < list_mem_type > fc_args = fcall->args_get();		//args for pi_call
	ptr< list<srp<pi_operand> > > fc_args = fcall->args_get();		//args for pi_call
	lint i=1;						//index of function parameter
	ptr < pi_operand > eval_arg;				//temporary for evaluation of the argument
	list < srp<ss_expression> >::iterator iss=x->args_get()->begin();	//argument of funcall in ss

	
   	for ( ; iss != x->args_get()->end() ;  i++, iss++){		//setting args for pi_call
		
		eval_arg = this->returns(*iss);				//transform argument expression into pi; evaluates just load of arg
		fc_args->push_back(eval_arg);
		
		if ( eval_arg->origin_get() ) {
			level = max(eval_arg->origin_get()->level_get(),level);
		}
		
		//jaz - convert a pi_operand to pi_mem operand
		/*
		ptr<pi_mem> eval_arg_mem;
		
		switch ( eval_arg->kind_get() ) {
			case pi_operand::PREG: {
				ptr<pi_mf_preg> mf = pi_mf_preg::create(eval_arg->origin_get(), eval_arg->type_get(), eval_arg.dncast<pi_preg>());
				eval_arg_mem = mf->get_mem(eval_arg->origin_get()); 
			} break;
			
			case pi_operand::LIT: {
				ptr<pi_mf_lit> mf = pi_mf_lit::create(NULL, eval_arg->type_get(), eval_arg.dncast<pi_lit>());
				eval_arg_mem = mf->get_mem(eval_arg->origin_get()); 
			} break;
			
			default:
				eval_arg_mem = eval_arg.dncast<pi_mem>();
			
		}
		
		fc_args->push_back(eval_arg_mem);
		*/
		
		/* jaz
		ptr< pi_mem_factory > arg_alloc = allocator->allocate_fcall_par(caller->current_function_get(),fcall,(*iss)->type_get(), i);
		fc_args->push_back(arg_alloc->get_mem(ss_sp2pi_sp(psp)));
			//temporary for argument on stack; origin on psp of the whole expression
	
		ptr < list < srp<pi_pi> > > 
			pis = 	copy_constructor_call_generator::instance()->			//store arg to stack for the function
					generate(	fc_args->back(),
							eval_arg,
							ss_sp2pi_sp((*iss)->psp_get()),
							ss_sp2pi_sp((*iss)->nsp_get())
						);		
		builder->add_pis(pis);
		
		
		list < srp<pi_pi> >::iterator ipi=pis->begin();			//we have to keep level on maximum of all the instruction levels used
										//to store argument	
		for ( ; ipi != pis->end() ; ipi++ ) level = max ((*ipi) -> level_get(),level);
		
		dts->destructor_add_to_destr(temporaries,(*iss)->type_get(),fc_args->back()->factory_get());
		*/
	}

	fcall->args_set(fc_args);
	fcall->level_set(level+1);
	
	builder->add_pi(fcall);	
	builder->add_rail();	

outry("funcall");
}

/*template<typename Ss, typename Pi>
void binary_op_visit_template(ptr< ss_expr2pi > This, ptr< Ss > x){entry("X");
	
	if (This->evaluated(x)) return;

	ptr< pi_preg > rt = This->returns_preg(x->right_get());	//evaluate operands
	ptr< pi_preg > lft= This->returns_preg(x->left_get());	
	ptr< pi_preg > dest= pi_preg::create(NULL,ss_type2tm_type_convertor::instance()->convert(x->type_get()));
	
	if (!lft || !rt) lassert2(false,"operands incorrect.");

	lint level = 1+max ( 							//get level for current pseudoinstruction
			max( x->psp_get()->level_get(), x->nsp_get()->level_get() ),
			max( lft->origin_get()->level_get(), rt->origin_get()->level_get() )
		);
	
	ptr< pi_add > tmp = pi_add::create(					//store equivalent to assignemnt
				ss_sp2pi_sp(x->psp_get()),
				ss_sp2pi_sp(x->nsp_get()),
				level,
				lft,
				rt,
				dest,
				dest->type_get()
				);

	dest->origin_set(tmp);
		
	backend_data_builder::instance()->add_pi(tmp);
	backend_data_builder::instance()->add_rail();
	
	(*This->expr2operand_map)[x]=This->result=lft;


outry("X");
}//template
*/

/*!
  Generic visitor for ss -> pi transformation of binary operators
  Level for pseudoinstructions: maximum(psp,nsp,lft,rt) + 1
*/

#define BIND_EQUAL_SS_PI(OP,second_type...) \
void ss_expr2pi::visit_ss_##OP(ptr<ss_##OP> x){entry(#OP);\
\
	if (evaluated(x)) return;\
\
	ptr< pi_preg > rt = returns_preg(x->right_get());	\
	ptr< pi_preg > lft= returns_preg(x->left_get());	\
	ptr< pi_preg > dest= pi_preg::create(NULL,ss_type2tm_type_convertor::instance()->convert(x->type_get()));\
\
	if (!lft || !rt) lassert2(false,"operands incorrect.");\
\
	lint level = 1+max ( 								\
			max( x->psp_get()->level_get(), x->nsp_get()->level_get() ),	\
			max( lft->origin_get()->level_get(), rt->origin_get()->level_get() )\
		);\
\
	ptr< pi_##OP > tmp = pi_##OP::create(			\
				ss_sp2pi_sp(x->psp_get()),	\
				ss_sp2pi_sp(x->nsp_get()),	\
				level,				\
				lft,				\
				rt,				\
				dest,				\
				dest->type_get(),		\
				## second_type			\
				);				\
\
	dest->origin_set(tmp);\
\
	backend_data_builder::instance()->add_pi(tmp);\
	backend_data_builder::instance()->add_rail();\
\
	(*expr2operand_map)[x]=result=dest;\
\
outry(#OP);\
}


#define second_type_arg (rt->type_get())

BIND_EQUAL_SS_PI(add);
BIND_EQUAL_SS_PI(sub);
BIND_EQUAL_SS_PI(mul);
BIND_EQUAL_SS_PI(div);	//TODO there are more possibilities
BIND_EQUAL_SS_PI(mod);
BIND_EQUAL_SS_PI(shr);
BIND_EQUAL_SS_PI(shl);
//TODO shr -> sar for signed
//TODO shl -> sal for signed


BIND_EQUAL_SS_PI(band);
BIND_EQUAL_SS_PI(bor);
BIND_EQUAL_SS_PI(bxor);

//TODO arr_acc

BIND_EQUAL_SS_PI(sbg,second_type_arg);
BIND_EQUAL_SS_PI(sbl,second_type_arg);
BIND_EQUAL_SS_PI(sbe,second_type_arg);
BIND_EQUAL_SS_PI(sbng,second_type_arg);
BIND_EQUAL_SS_PI(sbnl,second_type_arg);
BIND_EQUAL_SS_PI(sbne,second_type_arg);


/*!
  Generic visitor for ss -> pi transformation of unary operators
  Level for pseudoinstructions: maximum(psp,nsp,lft,rt) + 1
*/
#define BIND_UNARY_EQUAL_SS_PI(OP) \
void ss_expr2pi::visit_ss_##OP(ptr<ss_##OP> x){entry(#OP);\
\
	if (evaluated(x)) return;\
\
	ptr< pi_preg > src = returns_preg(x->expr_get());	\
	ptr< pi_preg > dest= pi_preg::create(NULL,ss_type2tm_type_convertor::instance()->convert(x->type_get()));\
\
	lassert2(src,"operands incorrect.");\
\
	lint level = 1+max ( 								\
			max( x->psp_get()->level_get(), x->nsp_get()->level_get() ),	\
			src->origin_get()->level_get()\
		);\
\
	ptr< pi_##OP > tmp = pi_##OP::create(			\
				ss_sp2pi_sp(x->psp_get()),	\
				ss_sp2pi_sp(x->nsp_get()),	\
				level,				\
				src,				\
				dest,				\
				dest->type_get()		\
				);				\
\
	dest->origin_set(tmp);\
\
	backend_data_builder::instance()->add_pi(tmp);\
	backend_data_builder::instance()->add_rail();\
\
	(*expr2operand_map)[x]=result=dest;\
\
outry(#OP);\
}

BIND_UNARY_EQUAL_SS_PI(gat);
BIND_UNARY_EQUAL_SS_PI(neg);
BIND_UNARY_EQUAL_SS_PI(lnot);
BIND_UNARY_EQUAL_SS_PI(bnot);


/*!
  lvalue->rvalue conversion or indirect load via pointer on volatile variable 
*/
void ss_expr2pi::visit_ss_vol_get(ptr< ::lestes::lang::cplus::sem::ss_vol_get > x) {entry("vol_get");
	
	if (evaluated(x)) return;
	ptr< pi_pi > res;
	
	if (is_returned_pointer(x->expr_get()))		//real evaluation run here
	 {		//we got pointer for dereference, so load will be via pointer
	 
	 	ptr< pi_mem_ptr_deref > mem = this->returns_mem_pointer(x->expr_get());
		
	 	//ptr < pi_preg > lval = this->returns_preg(x->expr_get());	//pointer to be dereferenced
		ptr< pi_preg > rval = pi_preg::create(NULL,ss_type2tm_type_convertor::instance()->convert(x->type_get()));
		
		//JAZ
		ptr<pi_sp> psp = ss_sp2pi_sp(x->psp_get());
		ptr<pi_sp> nsp = ss_sp2pi_sp(x->nsp_get());
		
		//ptr<pi_mf_ptr_deref> mf = pi_mf_ptr_deref::create(NULL, rval->type_get(),lval);
		//ptr<pi_mem_ptr_deref> mem = mf->get_mem(compute_origin(mf,x->psp_get())).dncast<pi_mem_ptr_deref>();
		
		//ulint level = max(x->psp_get()->level_get(),x->nsp_get()->level_get());
		ulint level = max(psp->level_get(),nsp->level_get());

		ptr< pi_ldpv > tmp = pi_ldpv::create (
				psp, nsp,
				1+max(level,mem->origin_get()->level_get()),
				rval,
				mem,
				rval->type_get(),
				ss_type2tm_type_convertor::instance()->convert(		///pointer on source type
								ss_pointer::instance(x->type_get().dncast<ss_pseudoreference>()->what_get())
								)
			);
		
		rval->origin_set(tmp);
		res=tmp;
		(*expr2operand_map)[x] = result = rval;
		
	 } else
	 {		//standard rvalue_lvalue conversion
	 
		ptr< pi_mem > lval  = this->returns_mem(x->expr_get());
		lassert(lval->kind_get()!=pi_operand::MEM_PTR_DEREF);
		
		ptr< pi_preg > rval = pi_preg::create(NULL,ss_type2tm_type_convertor::instance()->convert(x->type_get()));
	
		ulint level = max(x->psp_get()->level_get(),x->nsp_get()->level_get());
	
		ptr< pi_ldv > tmp = pi_ldv::create (
				ss_sp2pi_sp(x->psp_get()), ss_sp2pi_sp(x->nsp_get()),
				1+max(level,lval->origin_get()->level_get()),
				rval,
				lval,
				rval->type_get()
			);
		
		rval->origin_set(tmp);
		res=tmp;
		(*expr2operand_map)[x] = result = rval;
	}
	
	ptr<backend_data_builder> initial_structure_builder = backend_data_builder::instance();
	initial_structure_builder->add_pi(res);	
	initial_structure_builder->add_rail();

outry("vol_get");
}

/*!
  This time, templates should be instantiated.
*/
void ss_expr2pi::visit_ss_var_ref_by_qname(ptr< ::lestes::lang::cplus::sem::ss_var_ref_by_qname > ) {
	lassert2(false,"Reference to unistantiated template.");
}

/*!
  Up to now, there is nothing to be done for array->pointer conversion.
*/
void ss_expr2pi::visit_ss_array_to_pointer(ptr< ::lestes::lang::cplus::sem::ss_array_to_pointer > x){entry("array->pointer");

	if (evaluated(x)) return;
	
	(*expr2operand_map)[x] = result = this->returns(x->expr_get()) ;
	
outry("array->pointer");	
}

/*!
  Conversion between types.
  Note, that frontend products conversions, where source type is equal destination
  type. These cases must be properly treated.
*/
void ss_expr2pi::visit_ss_conversion(ptr< ::lestes::lang::cplus::sem::ss_conversion > x) {entry("convert");
	if (evaluated(x)) return;
	
	if (x->type_get()==x->src_type_get()) { 	//conversion is meaningful only on different types
		ptr< pi_operand > src = this->returns(x->expr_get());
		(*expr2operand_map)[x]=result=src; 
		outry("convert(identity)");
		return; 
	 } 
	
	ptr < pi_preg > src = this->returns_preg(x->expr_get());	//pointer to be dereferenced
	ptr< pi_preg > dest = pi_preg::create(NULL,ss_type2tm_type_convertor::instance()->convert(x->type_get()));

	lint level = 1+max ( src->origin_get()->level_get(),
			max( x->psp_get()->level_get(), x->nsp_get()->level_get() ));
			
	ptr< pi_cvt > tmp = pi_cvt::create(
				ss_sp2pi_sp(x->psp_get()),
				ss_sp2pi_sp(x->nsp_get()),
				level,
				src,
				dest,
				ss_type2tm_type_convertor::instance()->convert(x->type_get()),
				ss_type2tm_type_convertor::instance()->convert(x->src_type_get())
		);
	
	dest->origin_set(tmp);

	backend_data_builder::instance()->add_pi(tmp);
	backend_data_builder::instance()->add_rail();

	(*expr2operand_map)[x]=result=dest;

outry("convert");
}

/*!
  Load address of a given object (&x).
  Only lvalue can be the argument of this operator.
*/
void ss_expr2pi::visit_ss_address_of(ptr< ::lestes::lang::cplus::sem::ss_address_of > x) {entry("reference");

	if (evaluated(x)) return;
	
	ptr < pi_mem > src = this->returns_mem(x->expr_get());		//address to be loaded
	ptr< pi_preg > dest = pi_preg::create(NULL,ss_type2tm_type_convertor::instance()->convert(x->type_get()));

	lint level = 1+max ( src->origin_get()->level_get(),
			max( x->psp_get()->level_get(), x->nsp_get()->level_get() ));
	
	ptr< pi_lda > tmp = pi_lda::create(
				ss_sp2pi_sp(x->psp_get()),
				ss_sp2pi_sp(x->nsp_get()),
				level,
				dest,
				src,
				dest->type_get(),
				src->type_get()
		);
	
	dest->origin_set(tmp);

	backend_data_builder::instance()->add_pi(tmp);
	backend_data_builder::instance()->add_rail();

	(*expr2operand_map)[x]=result=dest;


outry("reference");
}

/*!
  Bind reference. It can occurs in initialization, function parameter passing and return value store.
  Its input value shall be lvalue - in case of dereference on pointer, only pointer shall be returned,
  in case of variable reference, its address shall be loaded.
*/
void ss_expr2pi::visit_ss_bind_reference(ptr< ::lestes::lang::cplus::sem::ss_bind_reference > x) {entry("bind_ref");
	
	if (evaluated(x)) return;
	
	if (is_returned_pointer(x->expr_get()))		//real evaluation run here
	 {		//we got pointer dereference, so we simply return it
	 
	 	ptr < pi_preg > lval = this->returns_preg(x->expr_get());	//pointer
		(*expr2operand_map)[x] = result = lval;
		
	 } else
	 {		//var_ref address to load
	 
		ptr< pi_mem > var  = this->returns_mem(x->expr_get());
		ptr< pi_preg > addr = pi_preg::create(NULL,ss_type2tm_type_convertor::instance()->convert(x->type_get()));
	
		ulint level = max(x->psp_get()->level_get(),x->nsp_get()->level_get());

		ptr< pi_lda > tmp = pi_lda::create (
				ss_sp2pi_sp(x->psp_get()), ss_sp2pi_sp(x->nsp_get()),
				1+max(level,var->origin_get()->level_get()),
				addr,
				var,
				addr->type_get(),
				var->type_get()
			);

		addr->origin_set(tmp);
		(*expr2operand_map)[x] = result = addr;
		
		ptr<backend_data_builder> initial_structure_builder = backend_data_builder::instance();
		initial_structure_builder->add_pi(tmp);	
		initial_structure_builder->add_rail();

	} //else
	
outry("bind_ref");
}


//These visitors shall be implemented in the future

void ss_expr2pi::visit_ss_land(ptr< ::lestes::lang::cplus::sem::ss_land > ) {
lassert2(false,"Visitor of && expression not implemented in ss2pi\n");
//TODO
}

void ss_expr2pi::visit_ss_lor(ptr< ::lestes::lang::cplus::sem::ss_lor > ) {
lassert2(false,"Visitor of || expression not implemented in ss2pi\n");
//TODO
}

void ss_expr2pi::visit_ss_arr_acc(ptr< ::lestes::lang::cplus::sem::ss_arr_acc > ) {
lassert2(false,"Visitor of array access expression not implemented in ss2pi\n");
//TODO
}

void ss_expr2pi::visit_ss_pfuncall(ptr< ::lestes::lang::cplus::sem::ss_pfuncall > ) {
lassert2(false,"Visitor of pfuncall expression not implemented in ss2pi\n");
//TODO
}

void ss_expr2pi::visit_ss_ifuncall(ptr< ::lestes::lang::cplus::sem::ss_ifuncall > ) {
lassert2(false,"Visitor of ifuncall expression not implemented in ss2pi\n");
//TODO
}

void ss_expr2pi::visit_ss_mfuncall(ptr< ::lestes::lang::cplus::sem::ss_mfuncall > ) {
lassert2(false,"Visitor of mfuncall expression not implemented in ss2pi\n");
//TODO
}

void ss_expr2pi::visit_ss_vfuncall(ptr< ::lestes::lang::cplus::sem::ss_vfuncall > ) {
lassert2(false,"Visitor of vfuncall expression not implemented in ss2pi\n");
//TODO
}

void ss_expr2pi::visit_ss_bind_to_temporary(ptr< ::lestes::lang::cplus::sem::ss_bind_to_temporary > ) {
lassert2(false,"Visitor of bind-to-temporary expression not implemented in ss2pi\n");
//TODO
}

void ss_expr2pi::visit_ss_this_expr(ptr< ::lestes::lang::cplus::sem::ss_this_expr > ) {
lassert2(false,"Visitor of 'this' expression not implemented in ss2pi\n");
//TODO
}

void ss_expr2pi::visit_ss_ternary(ptr< ::lestes::lang::cplus::sem::ss_ternary > ) {
lassert2(false,"Visitor of ternary expression not implemented in ss2pi\n");
//TODO
}

void ss_expr2pi::visit_ss_throw(ptr< ::lestes::lang::cplus::sem::ss_throw > ) {
lassert2(false,"Visitor of throe expression not implemented in ss2pi\n");
//TODO
}

void ss_expr2pi::visit_ss_member_ptr(ptr< ::lestes::lang::cplus::sem::ss_member_ptr > ) {
lassert2(false,"Visitor of member pointer expression not implemented in ss2pi\n");
//TODO
}

void ss_expr2pi::visit_ss_ptr_member_ptr(ptr< ::lestes::lang::cplus::sem::ss_ptr_member_ptr > ) {
lassert2(false,"Visitor of current expression not implemented in ss2pi\n");
//TODO
}

void ss_expr2pi::visit_ss_access(ptr< ::lestes::lang::cplus::sem::ss_access > ) {
lassert2(false,"Visitor of accessing operator not implemented in ss2pi\n");
//TODO
}

void ss_expr2pi::visit_ss_ptr_access(ptr< ::lestes::lang::cplus::sem::ss_ptr_access > ) {
lassert2(false,"Visitor of pointer accessing operator expression not implemented in ss2pi\n");
//TODO
}


/////////////////////		Destination visitors		///////////////////////////

/*!
  We should not ask for another expressions.
*/
void ss_expr2destination::default_destination(ptr< ::lestes::lang::cplus::sem::ss_expression > ){
	lassert2(false,"Trying to get destination from non-side-effect expression.");
}

/*!
  There is no destination for funcall.
*/
void ss_expr2destination::visit_ss_funcall(ptr< ::lestes::lang::cplus::sem::ss_funcall > ){
	destination=NULL;	
}

/*!
  Destination for assignment is left operand.
*/
void ss_expr2destination::visit_ss_assign(ptr< ::lestes::lang::cplus::sem::ss_assign > x){
	destination=x->left_get();
}

/*!
  Destination for volatile access is accessed variable itself.
*/
void ss_expr2destination::visit_ss_vol_get(ptr< ::lestes::lang::cplus::sem::ss_vol_get > x){
	destination=x->expr_get();
}



/////////////////////////		Statement Visitors		/////////////////////////////////

/*!
  Entering new compound statement
*/
void ss_stmt2pi::visit_ss_compound_stmt(ptr< ss_compound_stmt > x){entry("compound");

	ptr< ss_stmt2pi > stmt_visitor = 			//new visitor
		ss_stmt2pi::create( this->current_function_get(), local_variable_map, namespace_variable_map );
	
	ptr< ss_destructor_tables > dts = ss_destructor_tables::instance();	//new destructor table for variables on compound
	dts->open_compound_stmt(x);
	
	list < srp<ss_statement> >::iterator is=x->statements_get()->begin();
	
	for ( ; is != x->statements_get()->end() ; is++ ){
		(*is) -> accept_ss_statement_visitor(stmt_visitor);
	}
	
	if (current_function)			//TODO global destruction folows ABI
	dts->back_roll_from_current_compound_stmt();
	dts->close_compound_stmt();

outry("compound");	
}

void ss_stmt2pi::visit_ss_decl_stmt(ptr< ss_decl_stmt > x){ 

	ptr< ss_decl2pi > decl_visitor = ss_decl2pi::create(this,x);
	x->decl_get()->accept_ss_declaration_visitor(decl_visitor);
	
	//initializations are done inside declaration
}


/*!
  Common entry point for expression to pi tranformation, since there are more ways
  how to get into ss_expression - through expression statement initializer or conditions in other statements.
  
  Parameters: 		
  			stmt- decl/expr statement from wherewe are calling start_ss_expr2pi	
  			stmt_caller - statement visitor, which is calling this routine
  Used variables:
  				psp - previous sequence point for the whole expression needed for
	  				for origin lookup.
				nsp - needed for building of temporaries destruction

*/
#define start_ss_expr2pi(stmt,stmt_caller) \
	ptr< expression_results_map_type > expr2operand = expression_results_map_type::create();\
	ptr< ss_destructor_table > temporaries = ss_destructor_table::create(NULL);\
	plog(dtl) << "New temporaries table " << temporaries->uid_get()<<"\n" << eolog;\
	ptr< ss_expr2pi > expr_visitor = ss_expr2pi::create(stmt_caller,stmt->psp_get(),temporaries,expr2operand);\
\
	list < srp<ss_sp> > ::iterator i = stmt->sequence_points_get()->begin();\
	for ( ; i != stmt->sequence_points_get()->end(); i++) {	\
\
		list < srp<ss_se> >::iterator ii = (*i)->nse_get()->begin();	\
		for ( ; ii != (*i)->nse_get()->end(); ii++){			\
			expr_visitor->returns((*ii)->from_get());		\
		} 	\
	}	\
	
										

void ss_stmt2pi::visit_ss_return(ptr< ss_return > x){entry("return");
	
	start_ss_expr2pi(x,this);
	
	ptr< pi_operand > res ;
	if (x->result_get()) res = expr_visitor->returns(x->result_get());	//sideeffect evaluation is not enough, we need to know result of the whole expression
		else 
			res=NULL;	//no return expression
			
	ptr< backend_data_builder > builder = backend_data_builder::instance();
	
	ptr< pi_sp > first = ss_sp2pi_sp(x->nsp_get());				//psp<->first :expresion
	ptr< pi_sp > last  = ss_sp2pi_sp(x->nsp_get()->nsp_get());	
	ptr< pi_sp > temp   = pi_sp::create(first,last,first->level_get());//first(med)<->destr_compound :temporaries deletion
	first->nsp_set(temp);last->psp_set(temp);				//destr_compound<->temp :compound destructors
	builder->add_sp(temp);							//temp<->last :pi_leave
	plog(spl) << "	SP: pi_sp for temp deletion : " <<temp->uid_get() <<
			" inserted between " <<first->uid_get()<<" "<<last->uid_get()<<"\n" <<eolog;		
	
	ptr< pi_sp > med   = first;					//first<->med :retval store
	if (res) {			//non_void result has to be stored into ret_value
		med   = pi_sp::create(first,temp,first->level_get());
		builder->add_sp(med);
		
		first->nsp_set(med); temp->psp_set(med);
		
		plog(spl) << "	SP: pi_sp for retval : " <<med->uid_get() << 
				" inserted between " <<first->uid_get()<<" "<<temp->uid_get()<<"\n" <<eolog;		
		builder->add_pis(
			copy_constructor_call_generator::instance()->		//copy ret value between first and med
			  generate(
				(function_parameter_accessor::instance(current_function)->get_ret_val())->
						get_mem(ss_sp2pi_sp(x->psp_get())),
				res,
				first,
				med)
			);
	}	
	
	ptr< pi_sp > destr_compound = temporaries->back_roll_table(med,temp);	//Temporaries destructors will be made between statement sequence points
	ss_destructor_tables::instance()->back_roll_from_function(destr_compound,temp);	//destructors for local variables in the function

	ptr< pi_leave > tmp = pi_leave::create(	temp,last,last->level_get()+1,current_function );
	builder->add_pi(tmp);
	builder->add_rail();
	
outry("return");
}

/*!
  Expression statement. Most expressions are reached through this statement.
*/
void ss_stmt2pi::visit_ss_expr_stmt(ptr< ss_expr_stmt > x){ 	// start of expression tranformation

	start_ss_expr2pi(x,this);		//common starting sequence for expressioon transformation
	
	temporaries->back_roll_table(ss_sp2pi_sp(x->nsp_get()),ss_sp2pi_sp(x->nsp_get()->nsp_get()));	//Temporaries destructors will be made between statement sequence points

}

/*!
  If statement. Optimalizations could be done through pi_bX pseudoinstructions.
*/
void ss_stmt2pi::visit_ss_if_stmt(ptr< ss_if_stmt > x){entry("if"); 
		
	start_ss_expr2pi(x,this);
	ptr< pi_preg > res = expr_visitor->returns_preg(x->condition_get());	
		//sideeffect evaluation is not enough, we need to know result of the whole expression
		//for optimalization on branch type, here should be the special expression visitor
	
	lint level = 1+max ( res->origin_get()->level_get(),
			max( x->psp_get()->level_get(), x->nsp_get()->level_get() ));
	
	ptr< pi_bf > tmp = pi_bf::create(			//if false jump to else branch
				ss_sp2pi_sp(x->psp_get()),
				ss_sp2pi_sp(x->nsp_get()),
				level,
				res,
				ss_sp2pi_sp(x->celse_get()->psp_get())	//there is always else compound statement
	);
	
	ptr< pi_ba > tmp2 = pi_ba::create(			//skip else branch
				ss_sp2pi_sp(x->cthen_get()->nsp_get()),
				ss_sp2pi_sp(x->celse_get()->psp_get()),
				1+max( 	x->cthen_get()->nsp_get()->level_get(), 
					x->celse_get()->psp_get()->level_get() 
				     ),
				ss_sp2pi_sp(x->parent_get()->destructor_sp_get())
	);
	
	
	backend_data_builder::instance()->add_pi(tmp);
	backend_data_builder::instance()->add_pi(tmp2);
	backend_data_builder::instance()->add_rail();
	
	temporaries->back_roll_table(ss_sp2pi_sp(x->nsp_get()),ss_sp2pi_sp(x->nsp_get()->nsp_get()));	//Temporaries destructors will be made between statement sequence points

outry("if");
}

/*!
  While statement. 
  break and continue jumps are not implemented yet.
*/
void ss_stmt2pi::visit_ss_while(ptr< ss_while > x){entry("while");
		
	start_ss_expr2pi(x,this);
	ptr< pi_preg > res = expr_visitor->returns_preg(x->condition_get());	
		//sideeffect evaluation is not enough, we need to know result of the whole expression
		//for optimalization on branch type, here should be the special expression visitor
	
	lint level = 1+max ( res->origin_get()->level_get(),
			max( x->psp_get()->level_get(), x->nsp_get()->level_get() ));
	
	ptr< pi_bf > tmp = pi_bf::create(			//jump is always to else branch
				ss_sp2pi_sp(x->psp_get()),
				ss_sp2pi_sp(x->nsp_get()),
				level,
				res,
				ss_sp2pi_sp(x->parent_get()->destructor_sp_get())	//there is always else compound statement
	);
	
	ptr< pi_ba > tmp2 = pi_ba::create(			//continue new loop
				ss_sp2pi_sp(x->body_get()->nsp_get()),
				ss_sp2pi_sp(x->parent_get()->destructor_sp_get()),
				1+max( 	x->body_get()->nsp_get()->level_get(), 
					x->parent_get()->destructor_sp_get()->level_get() 
				     ),
				ss_sp2pi_sp(x->psp_get())
	);
	
	
	backend_data_builder::instance()->add_pi(tmp);
	backend_data_builder::instance()->add_pi(tmp2);
	backend_data_builder::instance()->add_rail();
	
	temporaries->back_roll_table(ss_sp2pi_sp(x->nsp_get()),ss_sp2pi_sp(x->nsp_get()->nsp_get()));	//Temporaries destructors will be made between statement sequence points

outry("while");
}


//following visitors should be implemented later

void ss_stmt2pi::visit_ss_try(ptr< ss_try > ){ 
lassert2(false,"Visitor of try statement is not implemented now in ss2pi.\n");
//TODO
}

void ss_stmt2pi::visit_ss_break(ptr< ss_break > ){ 
lassert2(false,"Visitor of break statement is not implemented now in ss2pi.\n");
//TODO
}

void ss_stmt2pi::visit_ss_goto(ptr< ss_goto > ){ 
lassert2(false,"Visitor of goto statement is not implemented now in ss2pi.\n");
//TODO
}

void ss_stmt2pi::visit_ss_continue(ptr< ss_continue > ){ 
lassert2(false,"Visitor of continue statement is not implemented now in ss2pi.\n");
//TODO
}

void ss_stmt2pi::visit_ss_catch(ptr< ss_catch > ){ 
lassert2(false,"Visitor of catch statement is not implemented now in ss2pi.\n");
//TODO
}

void ss_stmt2pi::visit_ss_switch_stmt(ptr< ss_switch_stmt > ){ 
lassert2(false,"Visitor of switch statement is not implemented now in ss2pi.\n");
//TODO
}

void ss_stmt2pi::visit_ss_do(ptr< ss_do > ){ 
lassert2(false,"Visitor of 'do' statement is not implemented now in ss2pi.\n");
//TODO
}

void ss_stmt2pi::visit_ss_for(ptr< ss_for > ){ 
lassert2(false,"Visitor of for statement is not implemented now in ss2pi.\n");
//TODO
}

void ss_stmt2pi::mem_variable_set(ptr< ss_declaration > x, ptr< pi_mem_factory > y){
	
	if(current_function)  		//variable on local scope
		(*local_variable_map)[x]=y;
	 else
	 	(*namespace_variable_map)[x]=y;
}


/*!
  This code presumes, there si no ss_fake_declaration on namespace scope.
*/
ptr< pi_mem_factory > ss_stmt2pi::mem_variable_get(ptr< ss_declaration > x){
//dumb(x);
	 	//try to find on namespace scope firstly

	variable_map_type::iterator it = (*namespace_variable_map).find(x);
	if ( it != (*namespace_variable_map).end())
		return (*it).second;
	
		//try to find on a local scope
	
	it = (*local_variable_map).find(x);
	if ( it != (*local_variable_map).end() )
		return (*it).second;
		
	 else { 	//not yet searched funcall parameter or alias declaration
	 	ptr< pi_mem_factory > res = ss_decl2mem::create(this)->get_real_decl_mem(x);
		if (res) (*local_variable_map)[x]=res;
		return res;
	      } //else 
		
	return NULL;		//doesnt exists
}



//////////////////////////    	DECLARATIONS		/////////////////////////////////


void ss_decl2pi::visit_ss_function_declaration(ptr< ss_function_declaration > x){entry("fundecl");	//we will transform current function
	
	if (!x->body_get()){ 				//only function declaration
		outry("fundecl - no body");
		return;
	}
	
	ptr < variable_map_type > vm = variable_map_type::create();	//for each function there is map of variables and their correspond memory placeholders
	
	ptr< ss_stmt2pi > stmt_visitor = ss_stmt2pi::create(x,vm,caller->namespace_variable_map_get());		//new visitor - new destructor list has to be created
	
	ptr<backend_data_builder> initial_structure_builder = backend_data_builder::instance();
	initial_structure_builder->add_function_start(x);		//backend shall know about processing new function
	
	x->body_get()->accept_ss_statement_visitor(stmt_visitor);		//no destructors needed 
									//- served by nested compound statement
									// special case of main function is governed by backend itself
	initial_structure_builder->add_function_end();

//	summary_seq_points( x->body_get()->psp_get() );		//debug dump for top level sequence points
	
outry("fundecl");	
}

/*!
  In case of object declaration new variable shall be created.
  Only basic initializations are implemented here and many TODOs have to be done,
  after frontend implement necessary routines.
*/
void ss_decl2pi::visit_ss_object_declaration(ptr< ss_object_declaration > x){entry("objdecl");

	//TODO 
	//	* static & local extern - they have to be allocated in global scope
	//		and also their destructor 
	//	* constructors and destructors lookup have to be done later
	//	* initializations by constant expressions (int i=3+5;)
	//	* binding references
	// 	* int A=f(x) has to be handled especially on namespace scope as described in ABI.

	ptr<pi_mem_factory> var_mem;
		
	ptr<mem_alloc_manager> allocator = mem_alloc_manager::instance();
	
	if (caller->current_function_get()) {		//local scope	//&TODO local extern allocate as global var
		
		var_mem = allocator->allocate_local_var(caller->current_function_get(),x);
		
		if (statement->initializer_kind_get() == ss_decl_stmt::IK_COPY_INITIALIZATION){		//int i=f(x);
			entry("local copy init");
	
			ptr< ss_expression > expr_temp = statement->args_get()->front(); 
			start_ss_expr2pi(statement,caller);
			ptr< pi_preg > res = expr_visitor->returns_preg(expr_temp);	//sideeffect evaluation is not enough, we need to know result of the whole expression
		
			ptr< pi_mem > dest = var_mem->get_mem(ss_sp2pi_sp(statement->psp_get()));
		
			lassert2(res,"initialization operand incorrect.");
		
			lint level = 1+max ( 						//get level for current pseudoinstruction
				max( statement->psp_get()->level_get(), statement->nsp_get()->level_get() ),
				res->origin_get()->level_get()
				);
		
			ptr< pi_st > tmp = pi_st::create(					//store equivalent to assignemnt
				ss_sp2pi_sp(statement->psp_get()),
				ss_sp2pi_sp(statement->nsp_get()),
				level,
				dest,
				res,
				ss_type2tm_type_convertor::instance()->convert(x->type_get())
				);
		
			backend_data_builder::instance()->add_pi(tmp);
			temporaries->back_roll_table(ss_sp2pi_sp(statement->nsp_get()),ss_sp2pi_sp(statement->nsp_get()->nsp_get()));	//Temporaries destructors will be made between statement sequence points

		 	outry("local copy init");
		 } //copy init

	
	 } //local scope
	 else			//namespace scope
	 	{	
			//zero intialization is automatical 
			
			var_mem = allocator->allocate_global_var(x);
		
			if (statement->initializer_kind_get() == ss_decl_stmt::IK_COPY_INITIALIZATION){		//int i=3; 
		 		entry("global copy init");
		
		
		// this hack is to be removed, when initialization by constant expressions and function is implemented
				ptr< ss_expression > expr_init = statement->args_get()->front(); 
				if(expr_init && !(dynamic_cast <ss_literal *>(expr_init.operator->()))) {
					lassert2(false,"This kind of initialization on namespace scope is not implemented now.");
				}
	
	
				ptr < ss_linfo2pi_linfo > cnv = ss_linfo2pi_linfo::create();
		
				ptr< ss_expression > expr_temp = statement->args_get()->front(); 
				ptr< ss_literal > lit_temp = expr_temp.dncast<ss_literal>();
				ptr< literal_info_base > pinfo = cnv->get_pinfo(lit_temp->value_get()); 
	
				allocator->init_global_var(var_mem,pinfo);
		 		outry("global copy init");
			}
		} //local scope	

	
	if (statement->initializer_kind_get() == ss_decl_stmt::IK_DIRECT_INITIALIZATION){	//int i(3);
		lassert2(false,"Direct initializations are not implemented now");
	 }

	if (statement->initializer_kind_get() == ss_decl_stmt::IK_DEFAULT_INITIALIZATION){	//int i;	
		//TODO for classes constructor shall be calleda
	 }
	 
	caller->mem_variable_set(x,var_mem);			//new memory allocated for variable //TODO local extern
	ss_destructor_tables::instance()->destructor_add(x->type_get(),var_mem);

outry("objdecl");
}

/*!
  VMT for classes should be done here, when classes will be implemented.
*/
void ss_decl2pi::visit_ss_structure_declaration(ptr< ss_structure_declaration > ){
	lassert2(false,"Class support is not implemented now in ss2pi.");
}


void ss_decl2pi::default_action(ptr< ss_declaration > ){		//other declaration are not important
	return;
}



/////////////////////////////////////		Real declaration lookup 		///////////////////

/*!
  Function parameter cant be obtained from ss_decl_statement as normal variables.
*/
void ss_decl2mem::visit_ss_parameter_declaration(ptr< ss_parameter_declaration > x){entry("lookup param decl");
	  
	  result = function_parameter_accessor::instance(caller->current_function_get())->get_parameter(x->position_get());
	  
outry("lookup param decl");
}

/*!
  Fake declarations are ignored, real declaration is important.
*/
void ss_decl2mem::visit_ss_fake_declaration(ptr< ss_fake_declaration > x){entry("lookup fake decl");

	this->get_real_decl_mem(x->real_decl_get());		//no result needed, it remains the same
	
outry("lookup fake decl");
}

/*!
  Usually variable overriden by fake in case of "if" statement. 
  pi_mem_factory will be already in map.
*/
void ss_decl2mem::visit_ss_object_declaration(ptr< ss_object_declaration > x){entry("lookup object decl");

	result = caller->mem_variable_get(x);

outry("lookup object decl");
}


void ss_decl2mem::default_action(ptr< ss_declaration >){
	result=NULL;
}

///////////////////////////		TYPE  ->  Destructor	////////////////////////////////


void ss_type2destructor::default_destructor(ptr< ::lestes::lang::cplus::sem::ss_type > ){
	destructor = NULL;	//builtin types have no destructors
}

void ss_type2destructor::visit_ss_class(ptr< ::lestes::lang::cplus::sem::ss_class > x){
	destructor = ss_destructor_finder::instance()->find_default_destructor (x);
}

/*!
  Unions also have constructors and destructors [9.5/1]
*/
void ss_type2destructor::visit_ss_union(ptr< ::lestes::lang::cplus::sem::ss_union > ){
//TODO
}

/////////////////////////		Destructor Tables		////////////////////////////////



/*!
  Adds destructor to destructor table.
  It expect appropriate destructor to memmory placeholder (namely NULL for builtin types).
*/
void ss_destructor_table::destructor_add(ptr< ss_function_declaration > x , ptr< ::lestes::backend_v2::intercode::pi_mem_factory > y ){
	plog(dtl) << "Table " << this->uid_get()<< " : added "<< y->uid_get() << "\n";
	destructors->push_back(destructor_entry_type::create(x,y));
}

/*!
  Basic interface to get handle for destructor tables of the current translatioon unit.
  Internal fields of destructor tables are static. They shall be initialized by open_compound_stmt method.
*/
ptr< ss_destructor_tables > ss_destructor_tables::instance(){
	if ( !singleton_instance_get() ) {
		singleton_instance_set(ss_destructor_tables_stack::create());
	}
	return singleton_instance_get();
}

/*!
  Open new destructor table for newly entered compound statement and insert it into destructor_tables structure.
*/
ptr< ss_destructor_table > ss_destructor_tables_stack::open_compound_stmt(ptr< ss_compound_stmt > x){
	ptr< ss_destructor_table > tmp = ss_destructor_table::create(x);
	plog(dtl) << "Opening Scope Table " << tmp->uid_get()<<"\n" << eolog;
	
	tables->push_back(tmp);
	current = tmp;
	return tmp;
}

/*!
  Close destructor table of compound statement and return table for now.
*/
ptr< ss_destructor_table > ss_destructor_tables_stack::close_compound_stmt(){

	lassert2(!tables->empty(),"There is no destructor table left to be removed.");
	plog(dtl) << "Closing Scope Table " << tables->back()->uid_get()<<"\n" << eolog;

	tables->pop_back();
	current = tables->back();
	if (tables->empty()) return NULL;		//occurs in a case of closing global compound statement
	return tables->back();
}

/*!
  Find and record destructor for ss type, which destructs appropriate memory placeholder.
  Such a record will be added to a given destructor table.
*/
void ss_destructor_tables_stack::destructor_add_to_destr(ptr< ss_destructor_table >destr, ptr< ss_type > type , ptr< ::lestes::backend_v2::intercode::pi_mem_factory > var ){

	destr -> destructor_add ( ss_type2destructor::create()->find_destructor(type) , var );
}

/*!
  Find and record destructor for ss type, which destructs appropriate memory placeholder.
  Such a record will be added to current destructor table.
*/
void ss_destructor_tables_stack::destructor_add(ptr< ss_type > type , ptr< ::lestes::backend_v2::intercode::pi_mem_factory > var ){

	destructor_add_to_destr (current, type , var );
}

/*!
  Implements back rolling of a given destructor table (i.e. usually compound statement).
  Accepts pseudoinstruction sequence points, between which pastes destructor calls
  and dealocation routines. psp and nsp of a given sequence points will change.
  
  Returns last sequence point, which has nsp set to nsp argument of function. 
  (i.e. there is no instruction between returned sp and given psp)
  So than can be recursive continuation in other compound statements.
*/
ptr< ::lestes::backend_v2::intercode::pi_sp > ss_destructor_table::back_roll_table(ptr< ::lestes::backend_v2::intercode::pi_sp > psp, ptr< ::lestes::backend_v2::intercode::pi_sp > nsp){
plog(dtl) << "table back roll " << this->uid_get()<<" [ psp: "<<psp->uid_get()<< " nsp: " <<nsp->uid_get()<<"]\n" <<eolog;; 

 	ptr< pi_sp > last_sp = psp;
	list < srp < destructor_entry_type > >::reverse_iterator i = this->destructors_get()->rbegin();
	for(; i != this->destructors_get()->rend() ; i++ ){
		
		ptr< pi_sp > new_sp = pi_sp::create (last_sp,nsp,nsp->level_get());
		last_sp->nsp_set(new_sp);
		backend_data_builder::instance()->add_sp(new_sp);

		if ((*i)->first){			//exists destructor function
			//TODO	when classes are implemented, destructor shall be called here
		}//if
		
		plog(dtl) << "Table " << this->uid_get() << " : destruct " << (*i)->second->uid_get() <<"\n";
		plog(spl) << "	SP: destr pi_sp added : " <<new_sp->uid_get() << "\n" <<eolog;
	
		mem_alloc_manager::instance()->deallocate((*i)->second,last_sp,new_sp);
		last_sp=new_sp;
	}//for
	
	last_sp->nsp_set(nsp);
	nsp->psp_set(last_sp);
	return last_sp;
}

/*!
  Normal end of compound statement.
  back_roll_table shall be called. Also appropriate sequence points have to be tranformed into 
  pseudoinstruction sequence points. (this transformation has to be done before new nested pi-sp
  will be added.
*/
void ss_destructor_tables_stack::back_roll_from_current_compound_stmt(){
plog(dtl) << "From current compound";
	current->back_roll_table(
			ss_sp2pi_sp(current->compound_stmt_get()->destructor_sp_get()),
			ss_sp2pi_sp(current->compound_stmt_get()->nsp_get())
			);
}

/*!
  Abnormal end of compound statement; psp,nsp shall be taken from current return statement.
  This function requires that the whole destructor_tables structure begin with compound statement
  representing global namespace.
*/
void ss_destructor_tables_stack::back_roll_from_function(ptr< pi_sp > psp, ptr< pi_sp > nsp){
plog(dtl) << "From function ";

	back_roll_to_compound_stmt(ss_compound_stmt::root_instance(),false,psp,nsp);
}

/*!
  Abnormal end of compound statement; psp,nsp shall be taken from current statement(e.g. break, continue, goto).
  Builds destructors for all the variablefrom current compound statement to compound statement given(inclusive
  if inclusive set to true).  
*/
void ss_destructor_tables_stack::back_roll_to_compound_stmt(ptr< ss_compound_stmt > stmt, bool inclusive, ptr< pi_sp > psp, ptr< pi_sp > nsp){
plog(dtl) << "To compound";	
	lassert2(!tables->empty(),"Trying destructor back_roll in empty destructor tables.");

	ptr< pi_sp > last_sp = psp;
	ptr< pi_sp > end_sp = nsp;
	
	list < srp < ss_destructor_table > >::reverse_iterator i = tables->rbegin();
	do {
		if (!inclusive) if(stmt == (*i)->compound_stmt_get() ) break;	//if exclusive do not back_roll the last record
		last_sp=(*i)->back_roll_table(last_sp,end_sp);
	} while((*i++)->compound_stmt_get() != stmt);		//while in function/while

}
	

/*!
  Visitor for converting ss integral literals into pi literals.
  Structure for ss and pi are similar, thus simple conversion is possible.
*/
void ss_linfo2pi_linfo::visit_ss_integral_literal_info(ptr< ss_integral_literal_info > x){
	
	pinfo = li_simple::create (
			ss_type2tm_type_convertor::instance()->convert(x->type_get()),
			x->data_get());	
}

/*!
  Visitor for converting ss floating literals into pi literals.
  Up to now, backend doesnt provide floating point literal info,
  thus no conversion is possible.
*/
void ss_linfo2pi_linfo::visit_ss_floating_literal_info(ptr< ss_floating_literal_info >){
	lassert2(false, "Floating point literal conversion to backend structures is not implemented.");
}


/*!
  Visitor for converting ss compound literals into pi literals.
  Structure for ss and pi are similar, thus simple conversion is possible.
*/
void ss_linfo2pi_linfo::visit_ss_compound_literal_info(ptr < ss_compound_literal_info > x){

	ptr< ss_linfo2pi_linfo > cnv = ss_linfo2pi_linfo::create();

	ptr< list< srp< literal_info_base > > > atoms = list< srp< literal_info_base > >::create();  
	
	list < srp < ss_literal_info > >::iterator i = x->constituents_get()->begin();
	for(; i != x->constituents_get()->end() ; i++ )		
		atoms->push_back(cnv->get_pinfo(*i));

	pinfo = li_compound::create (
			ss_type2tm_type_convertor::instance()->convert(x->type_get()),
			atoms
		);
		
}

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);
