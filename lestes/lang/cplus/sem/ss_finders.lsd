<?xml version="1.0" encoding="UTF-8"?>
<!--
	The lestes compiler suite
	Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
	Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
	Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
	Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
	Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
	Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
	Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; version 2 of the License.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	See the full text of the GNU General Public License version 2, and
	the limitations in the file doc/LICENSE.

	By accepting the license the licensee waives any and all claims
	against the copyright holder(s) related in whole or in part to the
	work, its use, and/or the inability to use it.
-->
<lsd xmlns="http://lestes.jikos.cz/schemas/lsd" xmlns:h="http://www.w3.org/TR/REC-html40">

	<file-name>ss_finders</file-name>
	
	<packages>
		<p>lestes</p>
		<p>lang</p>
		<p>cplus</p>
		<p>sem</p>
	</packages>
	
	<imports>
		<i>lestes/common.hh</i>
		<i>lestes/std/set.hh</i>
	</imports>
	
	<implementation-imports>
		<i>lestes/lang/cplus/sem/ss_declaration.g.hh</i>
	</implementation-imports>
	
	<hh-header></hh-header>
	
	<hh-footer></hh-footer>
	
	<cc-header></cc-header>
	
	<cc-footer></cc-footer>
    
	<xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="visitor.v.lsd">
		<xi:fallback/>
	</xi:include>

	<foreign-class name="object">
		<p>lestes</p>
		<p>std</p>
	</foreign-class>

	<forward-class name="ss_function_declaration" />
	<forward-class name="ss_struct_base" />
    
	<class name="ss_constructor_finder" base="object" abstract="yes">
		<dox><bri>Finds a proper constructor (set) for a given class.</bri></dox>
		<field name="singleton_instance" type="ss_constructor_finder" specifier="static" init="NULL" />
		<method name="instance" type="ss_constructor_finder" specifier="static"/>
		<method name="find_copy_constructor" type="ss_function_declaration" specifier="virtual" qualifier="abstract">
			<param type="ss_struct_base"/>
		</method>
		<method name="find_default_constructor" type="ss_function_declaration" specifier="virtual" qualifier="abstract">
			<param type="ss_struct_base"/>
		</method>
		<method name="find_converting_constructors" 
			type="set &lt; ptr &lt; ss_function_declaration &gt; &gt;" specifier="virtual" qualifier="abstract">
			<param type="ss_struct_base"/>
		</method>
		<method name="find_constructors" 
			type="set &lt; ptr &lt; ss_function_declaration &gt; &gt;" specifier="virtual" qualifier="abstract">
			<param type="ss_struct_base"/>
		</method>
	</class>

	<class name="ss_constructor_find" base="ss_constructor_finder">
		<declare-abstract-methods />
	</class>
	
	<class name="ss_destructor_finder" base="object" abstract="yes">
		<dox><bri>Finds a proper destructor for a given class.</bri></dox>
		<field name="singleton_instance" type="ss_destructor_finder" specifier="static" init="NULL" />
		<method name="instance" type="ss_destructor_finder" specifier="static"/>
		<method name="find_default_destructor" type="ss_function_declaration" specifier="virtual" qualifier="abstract">
			<param type="ss_struct_base"/>
		</method>
	</class>

	<class name="ss_destructor_find" base="ss_destructor_finder">
		<declare-abstract-methods />
	</class>



</lsd>
