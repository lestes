/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/lang/cplus/lex/file_system.hh>
#include <lestes/lang/cplus/lex/preprocessor.hh>
#include <lestes/lang/cplus/sem/sa_statements.g.hh>
#include <lestes/lang/cplus/sem/ss_misc.g.hh>
#include <lestes/lang/cplus/sem/ss_ss2pi_base.hh>
#include <lestes/lang/cplus/syn/hinter.hh>
#include <lestes/lang/cplus/syn/manager.hh>
#include <lestes/lang/cplus/syn/parse_result.hh>
#include <lestes/lang/cplus/syn/parser.hh>
#include <lestes/lang/cplus/syn/prefixer.hh>
#include <lestes/msg/logger.hh>
#include <lestes/package.hh>
#include <lestes/std/data_types.hh>
#include <lestes/std/dumper.hh>
#include <lestes/backend/backend.g.hh>
#include <vector>
#include <iostream>
#include <fstream>
#include <cstdlib>


package(lestes);
package(lang);
package(cplus);
package(syn);


using ::lestes::lang::cplus::sem::ss_decl_seq;
using ::lestes::lang::cplus::sem::ss2pi_start;
using ::lestes::lang::cplus::sem::ss_translation_unit;
using ::lestes::lang::cplus::sem::sa_context_manager;


int do_it( bool flat_dump, lstring filename, lstring ss_filename, bool hinter_on )
{
	/* lex init */
	ptr<prefixer::preprocessor> pp =
		prefixer::preprocessor::create(
			::lestes::lang::cplus::lex::file_system::create(), "" );

	/* syn init */
	prefixer::init( prefixer::PREFIX_ON, pp );
	manager::init();
	hinter::init( hinter_on ? hinter::HINTER_CHECK : hinter::HINTER_OFF );
	parser::init();
	::lestes::lang::cplus::sem::sa_statements::instance();

//F//	//FIXME: this should be done elsewhere
//F//	hinter::forced_scope_set( ss_decl_seq::root_instance() );
//F//	// end

	ptr<parse_result_type> parse_result = parser::parse();
	bool success = parse_result->success_get();
	::std::cout << (success ? "succeeded" : "failed") << ::std::endl;

	ptr< ::lestes::intercode::as_base > as_result = parse_result->as_result_get();
	if (filename != "" && as_result) {
		::std::ofstream f(filename.c_str());
		if (flat_dump)
			::lestes::std::dumper::dump( f, as_result );
		else
			::lestes::std::readable_dumper::dump( f, as_result );
		f.close();
	}

	if (ss_filename != "") {
		::std::ofstream f(ss_filename.c_str());
		if (flat_dump)
			::lestes::std::dumper::dump(f, sem::ss_decl_seq::root_instance());
		else
			::lestes::std::readable_dumper::dump(f, sem::ss_decl_seq::root_instance());
		f.close();
	}

	ss2pi_start(ss_translation_unit::create(ss_decl_seq::root_instance()));
	lestes::backend::backend::create(::std::cout)->main();
	return success ? 0 : 1;
}

end_package(syn);
end_package(cplus);
end_package(lang);
end_package(lestes);

using ::std::vector;
using ::std::cout;
using ::std::endl;
using ::lestes::std::lstring;

struct usage_and_exit {
};

void operator << ( ::std::ostream & os, const usage_and_exit & )
{
	os << "\n\n"
		"Usage: parser_test [-h] [-l llc_file] [-s skel_file] [-o output_file] [-f filename] [dump_type]\n"
		"\n"
		"Logger configuration is read from llc_file, if specified.\n"
		"Logger configuration skeleton is dumped to skel_file, if specified.\n"
		"AS structures will be dumped into file specified by the -o option.\n"
		"  When the option is missing, they will not be dumped at all.\n"
		"SS structures will be dumped into file specified by the -f option.\n"
		"  When the option is missing, they will not be dumped at all.\n"
		"dump_type is either 'flat' or 'deep', the default is 'deep'.\n"
		"Hinter is turned on by the -h option.\n"
		<< endl;
	return exit(1);
}

int main( int argc, char * argv[] )
{
	vector<lstring> args( argv, argv+argc );
	vector<lstring>::iterator it = args.begin();
	++it;
	lstring filename = "";
	lstring ss_filename = "";
	bool log_finish = false;
	bool o_seen = false;
	bool f_seen = false;
	bool l_seen = false;
	bool h_seen = false;
	while (it != args.end() && (*it)[0] == '-') {
		// option found
		if (*it == "-o") {
			if (o_seen)
				cout << "Error: -o option used multiple times." << usage_and_exit();
			o_seen = true;
			++it;
			if (it == args.end())
				cout << "Argument to -o option is missing." << usage_and_exit();
			filename = *it++;
		} else if (*it == "-f") {
			if (f_seen)
				cout << "Error: -f option used multiple times." << usage_and_exit();
			f_seen = true;
			++it;
			if (it == args.end())
				cout << "Argument to -f option is missing." << usage_and_exit();
			ss_filename = *it++;
		} else if (*it == "-l") {
			if (l_seen)
				cout << "Error: -l option used multiple times." << usage_and_exit();
			l_seen = true;
			++it;
			if (it == args.end())
				cout << "Argument to -l option is missing." << usage_and_exit();
			log_finish = ::lestes::msg::logger::init(*it++);
			if (!log_finish)
				cout << "Error, loggers will be off." << endl;
		} else if (*it == "-s") {
			// can be used multiple times
			++it;
			if (it == args.end())
				cout << "Argument to -s option is missing." << usage_and_exit();
			::std::ofstream of( it->c_str() );
			++it;
			::lestes::msg::logger::dump_skeleton(of);
			of.close();
		} else if (*it == "-h") {
			++it;
			h_seen = true;
		} else
			cout << "Unknown option '" << *it << "'." << usage_and_exit();
	}
	bool flat_dump = false;
	if (it != args.end()) {
		if (*it == "flat")
			flat_dump = true;
		else if (*it != "deep")
			cout << "Unknown dump type '" << *it << "'." << usage_and_exit();
		++it;
		if (it != args.end())
			cout << "Too many arguments." << usage_and_exit();
	}
	int result = ::lestes::lang::cplus::syn::do_it( flat_dump, filename, ss_filename, h_seen );
	if (log_finish)
		::lestes::msg::logger::finish();
	return result;
}
