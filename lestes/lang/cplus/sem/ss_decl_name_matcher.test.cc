/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/lang/cplus/sem/ss_decl_name.g.hh>
#include <lestes/lang/cplus/sem/ss_decl_name_matcher.g.hh>
#include <lestes/lang/cplus/sem/ss_type_builtin.g.hh>
#include <lestes/std/source_location.hh>

package(lestes);
package(lang);
package(cplus);
package(sem);

int run_test()
{
	ptr<source_location> l = source_location::create(file_info::create("",NULL),1,1);
	ptr<source_location> ll = source_location::create(file_info::create("",NULL),2,3);

	ptr<ss_decl_name> nvoid1 = ss_conversion_name::create( l, ss_void::instance() );
	ptr<ss_decl_name> nvoid2 = ss_conversion_name::create( ll, ss_void::instance() );
	ptr<ss_decl_name> nsint = ss_conversion_name::create( ll, ss_type_sint::instance() );

	ptr<ss_decl_name> dummy = ss_dummy_name::create( ll );

	ptr<ss_decl_name> ord1 = ss_ordinary_name::create( l, "name" );
	ptr<ss_decl_name> ord2 = ss_ordinary_name::create( ll, "name" );
	ptr<ss_decl_name> ordx = ss_ordinary_name::create( ll, "x" );

	ptr<ss_decl_name> nopbe1 = ss_operator_sbe::create( l );
	ptr<ss_decl_name> nopbe2 = ss_operator_sbe::create( ll );
	ptr<ss_decl_name> nopbg = ss_operator_sbg::create( ll );

	lassert( nvoid1->matches(nvoid2) );
	lassert( nvoid2->matches(nvoid1) );
	lassert( !nvoid1->matches(nsint) );

	lassert( !dummy->matches(dummy) );

	lassert( ord1->matches(ord2) );
	lassert( ord2->matches(ord1) );
	lassert( ordx->matches(ordx) );
	lassert( !ord1->matches(ordx) );

	lassert( nopbe1->matches(nopbe2) );
	lassert( nopbe2->matches(nopbe1) );
	lassert( !nopbe2->matches(nopbg) );

	lassert( !dummy->matches(nopbg) );
	lassert( !ord1->matches(nsint) );
	lassert( !nopbe1->matches(nvoid1) );

	return 0;
}

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);

int main()
{
	return ::lestes::lang::cplus::sem::run_test();
}
