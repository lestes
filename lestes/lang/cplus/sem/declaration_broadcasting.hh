/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__sem__declaration_broadcasting_hh__included
#define lestes__lang__cplus__sem__declaration_broadcasting_hh__included

#include <lestes/common.hh>
#include <lestes/std/broadcaster.hh>
#include <lestes/std/broadcast_listener.hh>
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>
package(lestes);
package(lang);
package(cplus);
package(sem);

typedef broadcast_listener < ss_declaration > declaration_broadcast_listener;
typedef broadcaster < ss_declaration > declaration_broadcaster;
extern ptr < declaration_broadcaster > declaration_finished;
// egu: todle: do: jinyho: fajlu: a: iplantovat:     class egg_declaration_listener : public declaration_broadcast_listener { void run(ptr < ss_declaration > decl) { /* zavolat visitora, pokud funkce, vyplnit si do tabulek */ } }; /* atakdale atakpodobne */

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif // lestes__lang__cplus__sem__declaration_broadcasting_hh__included
