/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/lang/cplus/sem/as_decl.g.hh>
#include <lestes/lang/cplus/sem/as_id_to_declaration_set.g.hh>
#include <lestes/lang/cplus/sem/as_id_to_ss_decl_name.g.hh>
#include <lestes/lang/cplus/sem/li_by_name_in_single_scope.g.hh>
#include <lestes/lang/cplus/sem/lu_typedef.hh>
#include <lestes/lang/cplus/sem/sa_context.g.hh>
#include <lestes/lang/cplus/sem/sa_decl_seq_compound_pair_creator.g.hh>
#include <lestes/lang/cplus/sem/sa_namespace.g.hh>
#include <lestes/lang/cplus/sem/sa_namespace.m.hh>
#include <lestes/lang/cplus/sem/ss_decl_name.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>
#include <lestes/lang/cplus/sem/ss_enums.g.hh>
#include <lestes/lang/cplus/sem/ss_misc.g.hh>
#include <lestes/lang/cplus/sem/ss_statement.g.hh>
#include <lestes/lang/cplus/sem/ss_type_builtin.g.hh>
#include <lestes/std/source_location.hh>

package(lestes);
package(lang);
package(cplus);
package(sem);

void sa_namespace_definition::process_named( ptr<as_name> name )
{
	ptr<source_location> loc = name->location_get();
	ptr<ss_declaration_time> time = ss_declaration_time::create( loc->order_get() );
	ptr<ss_decl_seq> curr_scope =
		sa_context_manager::instance()->current()->ss_get()->scope_get();
	ptr<ss_decl_name> decl_name =
		as_id_to_ss_decl_name::instance()->process( name->identifier_get() );

	/*
	 * we MUST NOT rely on the lookup performed by the hinter,
	 * as that processes using directives (which we want to ignore)
	 */
	ptr<declaration_set_type> decls =
		li_by_name_in_single_scope::instance()->process( decl_name, curr_scope );

	bool is_new = true;
	if (!decls->empty()) {
		if (error_check( *decls->begin() )) {
			/* something with the same name is already declared and it is not a namespace...
			 * scream, but create the namespace anyway; with a dummy name, that is ;-)
			 */
			report << name_already_declared << name->identifier_get() << loc;
			for ( declaration_set_type::iterator it = decls->begin(); it != decls->end(); ++it )
				report << declared_here << (*it)->location_get();
			decl_name = ss_dummy_name::create( loc );
		} else {
			lassert( decls->size() == 1 );
			/* there is only one declaration with that name in current scope AND
			 * it is a namespace definition, therefore it must be the one to extend
			 */
			is_new = false;
		}
	}
	if (is_new) {
		ptr<ss_decl_seq> new_scope =
			sa_decl_seq_compound_pair_creator::instance()->process(
				loc, curr_scope, curr_scope->compound_stmt_get() )->first;
		ptr<ss_declaration> decl = ss_namespace_definition::create(
				loc, time, time, decl_name, curr_scope, ss_void::instance(),
				ss_linkage::create( "C++", ss_linkage::LINKAGE_EXTERNAL ), new_scope );
		curr_scope->contents_get()->push_back( decl );
		new_scope->declared_by_set( decl );

		sa_context_manager::instance()->push(
			sa_context::create(
				sa_as_context::create( name,
					as_access_specifier_public::create(loc) ),
				sa_ss_context::create( new_scope,
					ss_access_specifier::ACCESS_PUBLIC ),
				sa_sa_context::create()
			)
		);
	} else {
		ptr<ss_namespace_definition> nsd = decls->begin()->dncast<ss_namespace_definition>();

		sa_context_manager::instance()->push(
			sa_context::create(
				sa_as_context::create( name,
					as_access_specifier_public::create(loc) ),
				sa_ss_context::create( nsd->body_get(),
					ss_access_specifier::ACCESS_PUBLIC ),
				sa_sa_context::create()
			)
		);
	}
}

void sa_namespace_definition::process_unnamed( ptr<source_location> loc )
{
	report << unnamed_not_yet << loc;
	ptr<sa_context> ctx = sa_context_manager::instance()->current();
	/* for now, just copy the context */
	sa_context_manager::instance()->push(
		sa_context::create(
			sa_as_context::create( ctx->as_get()->scope_get(),
				as_access_specifier_public::create(loc) ),
			sa_ss_context::create( ctx->ss_get()->scope_get(),
				ss_access_specifier::ACCESS_PUBLIC ),
			sa_sa_context::create()
		)
	);
}

void sa_namespace_definition::process_end()
{
	sa_context_manager::instance()->pop();
}

void sa_namespace_definition::default_action( ptr<ss_declaration> )
{
	result_set( true );
}

void sa_namespace_definition::visit_ss_namespace_definition( ptr<ss_namespace_definition> )
{
	result_set( false );
}

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);
