/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
 * This file contains implementation of overload resolution, according to 
 * chapter 13, especially 13.3.
 *
 * \author jikos
 */
#include <lestes/lang/cplus/sem/or_ics.g.hh>
#include <lestes/lang/cplus/sem/or_or.g.hh>
#include <lestes/lang/cplus/sem/as_decl.g.hh>
#include <lestes/lang/cplus/sem/or.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>
#include <lestes/lang/cplus/sem/ss_statement.g.hh>
#include <lestes/lang/cplus/sem/ss_decl_name.g.hh>
#include <lestes/lang/cplus/sem/sa_decl_seq_compound_pair_creator.g.hh>
#include <lestes/lang/cplus/sem/ss_misc.g.hh>
#include <lestes/lang/cplus/sem/ss_type.g.hh>
#include <lestes/lang/cplus/sem/ss_type_builtin.g.hh>
#include <lestes/lang/cplus/sem/as_id_to_declaration_set.g.hh>
#include <lestes/lang/cplus/sem/li_func_by_name.g.hh>
#include <lestes/lang/cplus/sem/li_func_by_name_in_single_scope.g.hh>
#include <lestes/lang/cplus/sem/sa_context.g.hh>
#include <lestes/lang/cplus/sem/or.hh>
#include <lestes/lang/cplus/sem/or_logger.hh>
#include <lestes/msg/logger.hh>
#include <lestes/std/dumper.hh>

package(lestes);
package(lang);
package(cplus);
package(sem);

declare_logger( or_log );
initialize_logger( or_log, "overload", or_logger );

typedef ::lestes::std::set< srp< ss_function_declaration > > func_decl_set;
typedef ::lestes::std::list< srp< ss_declaration > > decl_list;

ptr< or_ics_functional > or_find_ics(ptr< or_or_functional > source, ptr< ss_type > target);
bool is_not_worse_conv_seq(ptr< or_ics_functional > r1, ptr< or_or_functional > e1,ptr< or_ics_functional > r2, ptr< or_or_functional > e2);
bool is_better_conv_seq (ptr< or_ics_functional > r1, ptr< or_or_functional > e1, ptr< or_ics_functional > r2, ptr< or_or_functional > e2);


/*!
 * This function tests if there exists implicit conversion sequence from expressions,
 * representing arguments in function call, to parameters of the candidate function.
 * 
 * \bug This functions doesn't deal with elipsis functions yet, as thery are not handled by
 * sem so far
 *
 * \return true if the conversion exists, false otherwise
 */
bool exists_ics_conversion (ptr< ss_function_declaration > candidate, ptr< exprlist > exprs)
{
	ptr< decl_list > params = candidate->parameters_get()->contents_get();
	decl_list::iterator it = params->begin();
	::lestes::std::list< srp< or_or_functional > >::iterator it_expr = exprs->begin();

	for(; it_expr != exprs->end() && it != params->end(); ++it, ++it_expr) {
		if(!or_find_ics(*it_expr, (*it)->type_get()))
			return false;
	}

	/* TODO
	for(; it_exprs != exprs->end(); ++it_expr) {
		if(!find_ics_ellipsis(*it_expr))
			return false;
	}
	*/
	return true;
}

/*!
 * This function filters out those candidates from candidates list, for which the 
 * implicit conversion sequence from expressions coming from call, to function
 * parameters, doesn't exist
 *
 * \return filtered list of candidates
 */
ptr< func_decl_set > filter_ics_existence(ptr< func_decl_set > candidates, ptr< exprlist > exprs)
{
	ptr< func_decl_set > new_cands = func_decl_set::create();

	func_decl_set::iterator it = candidates->begin(), it_end = candidates->end();

	for (it = candidates->begin(); it != it_end; ++it) {
		if (exists_ics_conversion(*it, exprs)) 
			new_cands->insert(*it);
		
	}
	return new_cands;
}

/*!
 * This function filters out those candidates from candidates list, which do not have
 * proper count of arguments.
 *
 * \bug This functions doesn't deal with elipsis functions yet, as thery are not handled by
 * sem so far
 *
 * \return filtered list of candidates 
 */
ptr< func_decl_set > filter_num_args(ptr< func_decl_set > candidates, decl_list::size_type num_args)
{
	ptr< func_decl_set > filtered = func_decl_set::create();

	func_decl_set::iterator it = candidates->begin();
	
	for(; it != candidates->end(); ++it) {
		if((*it)->parameters_get()->contents_get()->size() == num_args) {
			filtered->insert(*it);
		} else if ((*it)->parameters_get()->contents_get()->size() > num_args) {
			/* FIXME for ps - has_default() ?? 
			if((it->parameters_get()->begin()+num_args)->has_default())
				filtered->insert(*it);
			*/
		}
		/* FIXME - don't know how to get ellipsis yet 
		else if(it->ellipsis_get()) {
			filtered->insert(*it);
		}
		*/
	}
	return filtered;
}

/*!
 * This function returns the viable functions for overload resolution - those functions
 * are those having correct number of arguments && those for which's parameters implicit
 * conversion sequence from expressions to function parameters exists.
 *
 * \return set of viable functions
 */
ptr< func_decl_set > select_viable(ptr< func_decl_set > candidates, ptr< exprlist > exprs)
{
	llog(or_log) << "Filtering according to num of arguments\n";

	ptr< func_decl_set > enough_arg_funcs = filter_num_args(candidates, exprs->size());

	llog(or_log) << "Selected " << enough_arg_funcs->size() << " enough-arg funcs\n";

	return filter_ics_existence(enough_arg_funcs, exprs);
}

/*!
 * This function tests if function f is better (in terms of overload resolution)
 * than function g, when considering parameters in expression list exprlist.
 *
 * \bug tests for templates not done, as templates are not yet supported. tests for user
 *      conversions are not done, as user conversions are not yet supported.
 *
 * \return true if f is better conversion than g, otherwise return false
 */
bool is_better_function(ptr< ss_function_declaration > f, ptr< ss_function_declaration > g, ptr< exprlist > exprs)
{
	//decl_list::size_type num_args = exprs->size();

	ptr< decl_list > params_f = f->parameters_get()->contents_get();
	decl_list::iterator it_f_param = params_f->begin();

	ptr< decl_list > params_g = g->parameters_get()->contents_get();
	decl_list::iterator it_g_param = params_g->begin();

	exprlist::iterator it_exprs = exprs->begin();
	
	
	for(; it_f_param != params_f->end() && it_g_param != params_g->end() && it_exprs != exprs->end(); 
			++it_f_param, ++it_g_param, ++it_exprs) {
		ptr< or_ics_functional > f_func, g_func;

		f_func = or_find_ics((*it_exprs), (*it_f_param)->type_get());
		g_func = or_find_ics((*it_exprs), (*it_g_param)->type_get());

		if(!is_not_worse_conv_seq(f_func, (*it_exprs), g_func, (*it_exprs))) {
			return false;
		}
	}
	for(it_f_param = params_f->begin(), it_g_param = params_g->begin(), it_exprs = exprs->begin(); 
			it_f_param != params_f->end() && it_g_param != params_g->end() && it_exprs != exprs->end(); 
			++it_f_param, ++it_g_param, ++it_exprs) {
		ptr< or_ics_functional > f_func, g_func;

		f_func = or_find_ics((*it_exprs), (*it_f_param)->type_get());
		g_func = or_find_ics((*it_exprs), (*it_g_param)->type_get());
		
		if(is_better_conv_seq(f_func, (*it_exprs), g_func, (*it_exprs))){
			return true;
		}
	}
	/* TODO 
	 * - f is not template && G is a template specialization instance 
	 *   && g are templates && f is a better template specialization than g (see 14.5.5.2) 
	 * - context is inicialization by user conversion  && SCS from return_type(f) -> dest_type is better conv seq 
	 *   than SCS from return_type(g) -> dest_type
	 */

	return false;
}

/*!
 * This function selects the best function from set of viable functions. 
 * 
 * \return The functional, which represents the corresponding funcall, for the
 *         selected function.
 */
ptr< or_or_functional > best_selection(ptr< func_decl_set > viables, ptr< exprlist > exprs)
{
	func_decl_set::iterator it = viables->begin();
	ptr< ss_function_declaration > best = *it;
	bool was_better = true;

	if (viables->size() == 0) {
		/* there are no candidates */
		return or_or_functional_noviable::create(ss_void::instance());
	}
	for(++it; it != viables->end(); ++it) {
		if(is_better_function(best, *it, exprs)) {
			llog(or_logger) << "1: true\n";
			was_better = true;
		} else if (is_better_function(*it, best, exprs)) {
			llog(or_logger) << "2: true\n";
			was_better = true;
			best = *it; 
		} else {
			llog(or_logger) << "3: false\n";
			was_better = false;
		}
	}

	it = viables->begin();
	for(++it; it != viables->end(); ++it) {
		if(!((*it)->equals(best)) && !is_better_function(*it, best, exprs) && !is_better_function(best, *it, exprs))
			return or_or_functional_ambiguous::create(best->type_get().dncast<ss_function>()->returns_get(), viables);
	}

	return or_or_functional_concrete::create(best->type_get().dncast<ss_function>()->returns_get(), exprs, best);
}

/* 
 * This function performs lookup for the given operator name. This first means to perform
 * usual lookup in the current scope, and then also add declarations of builtin operators,
 * as per [13.6]
 */
ptr< func_decl_set > collect_candidates(ptr< ss_operator > func_name, ptr< ss_decl_seq > ctxt)
{
	ptr< li_func_by_name > lookup = li_func_by_name::create();
	ptr< li_func_by_name_in_single_scope > lookup_builtin = li_func_by_name_in_single_scope::create();
	ptr< func_decl_set > res_lu, res_builtin;
	func_decl_set::iterator it;

	/* lookup the name in the current scope */
	llog(or_log) << "Looking it up in the current scope\n";

	res_lu = lookup->process(func_name, ctxt);

	if (res_lu->empty()) {
		llog(or_log) << "Found nothing\n";
	}
	/* lookup the name in the builtin op scope */
	llog(or_log) << "Looking it up in the builtin op scope\n";
	res_builtin = lookup_builtin->process(func_name, or_builtin_operator_declaration_creator::instance()->builtin_op_decl_seq_get());

	/* unite the result of the lookup and the result of lookup in builtin decl seq */
	if (!res_builtin->empty()) {
		llog(or_log) << "Found something, pushing into return list\n";
		
		it = res_builtin->begin();
		for(; it != res_builtin->end(); it++){
			res_lu->insert(*it);
		}
	}
	
	return res_lu; 
}

/*!
 * This is the main function performing overload resolution algorithm. It
 * first collects candidates (performs lookup for function name), selects
 * viable functions from these candidates, and then returns best of the
 * viable functions found
 *
 * \bug Koenig lookup not ready yet
 *
 * \return functional representing the best selected overloaded function
 */

ptr< or_or_functional > overload_resolution (ptr< exprlist > exprs, ptr< ss_operator > func_name)
{
	/* search func_name in current context & associated namespaces (exprs) */
	/* Rudo will search according to decl_seq and decl_name and returns 
	 * collection ss_declarations (will have to be dncasted to ss_function_delcaration
	 */

	ptr< ss_decl_seq > ctxt = sa_context_manager::instance()->current()->ss_get()->scope_get();

	llog(or_log) << "Before collect candidates\n";

	ptr< func_decl_set > candidates = collect_candidates(func_name, ctxt);

	llog(or_log) << "Done, selected " << candidates->size() <<  " candidates\n";

	ptr< func_decl_set > viable_candidates = select_viable(candidates, exprs);

	llog(or_log) << "After viable candidates selection, selected " << viable_candidates->size() << " viable\n";
	
	/* if no viable functions were found, look if it is not the case of 'there exists builtin
	 * declaration for every type', which is ',' and some others. See [13.6] and [13.3] for more
	 * details.
	 *
	 * In such case, add the declaration of given ss_operator with current parameters to the decl_seq. 
	 * This is immediate TODO, just after a few tests.
	 */
	if (viable_candidates->size() == 0) {
		return NULL;
	}
	return best_selection(viable_candidates, exprs);
}

/*!
 * This is the main function performing overload resolution algorithm. It
 * differs from the previous one in the sense of how it is collecting
 * candidates - this one receives candidates from "upstream" and then 
 * just (will one day) performs Koenig lookup, but the set of candidates
 * is given and not obtained using lookup.
 *
 * This is used in the case of sa_deconstruct_spse::visit_as_expression_function_call,
 * for distinguisging the function prefix. See the apropriate comment in there
 *
 * \bug Koenig lookup not ready yet
 *
 * \return functional representing the best selected overloaded function
 */

ptr< or_or_functional > overload_resolution (ptr< exprlist > exprs, ptr< func_decl_set > candidates)
{
	ptr< func_decl_set > viable_candidates = select_viable(candidates, exprs);

	return best_selection(viable_candidates, exprs);
}

/* == here the main "high-level" overload resolution implementation ends == */

/* following is the implementation of visitor on ss_decl_name, which is used for purposes of creating 
 * the builtin operator declarations, as specified in [13.6]
 */

/*! 
 * this creates the singletonized instance of the creator and also fills the newly created decl_seq with
 *  declarations for builtin operators
 */
ptr< or_builtin_operator_declaration_creator > or_builtin_operator_declaration_creator::instance() 
{
	if (!the_instance) {
		/* before we create the actual ss_builtin_operator declaration, we need to prepare decl_seq, which
		 * will contain declarations of parameters and also the operator declarations themselves
		 * parameter types are stored in the func_type, however this is still needed (for overload resolution,
		 * which is examining these declarations).
		 */

		/* create pair of decl seq and corresponding compound stmt. This is for the operator declaration. */
		ptr< ss_decl_seq > builtin_op_decl_seq = sa_decl_seq_compound_pair_creator::instance()->process(
							source_location::zero(),
							ss_decl_seq::root_instance(),
							ss_compound_stmt::root_instance()
						)->first;
		builtin_op_decl_seq->dump_barrier_set(true);
		/* 2.1. create fake declaration and make the newly created ss_decl_seq to be declared
		 * by this declaration. This fake declaration is contained in root decl seq
		 */ 
		ptr<ss_declaration> gl_decl = ss_namespace_definition::create(source_location::zero(), ss_declaration_time::infinity(), ss_declaration_time::create(0),
						ss_dummy_name::create(source_location::zero()), ss_decl_seq::root_instance(), ss_void::instance(), 
						ss_linkage::create("C++", ss_linkage::LINKAGE_NO), builtin_op_decl_seq );
		builtin_op_decl_seq->declared_by_set( gl_decl );
		ss_decl_seq::root_instance()->contents_get()->push_back( gl_decl );

		the_instance = new or_builtin_operator_declaration_creator(builtin_op_decl_seq);

		/* Some of the thing described in [13.6] and [13] generally have to be added later. This means those, which
		 * take place 'for every type', so just adding them later to the list of candidates, with current types,
		 * is OK, when no else viable functions are found.
		 */

		BUILTIN_DECL_SEQ_INIT();

		/* we do not want this decl_seq to be incorporated in the dump. It's 15 mb of nothing :) */

	}

	return the_instance;
}

/*! 
 * This template performs creation of builtin operator declaration. The template argument OP specifies the 
 * ss_decl_name of the operator for which the declaration must be created. The function parameter types 
 * contains list of types, which should the arguments have. Note, that this is different from the types
 * obtained from arguments_get->begin() (which is or_or_functional) - this represents the actual parameters
 * (and their types) in the actual call (and for which the conversion is being searched for during overload
 * resolution)
 */
void or_builtin_operator_declaration_creator::construct_builtin_op(ptr< ss_decl_name > op, ptr< ::lestes::std::list< srp< ss_type > > > types, ptr< ss_type > return_type)
{
	typedef ::lestes::std::list< srp< ss_type > > typelist;
	typedef ::lestes::std::set< srp< ss_struct_base > > structlist;
	::lestes::std::list< srp< ss_type > >::iterator it_d = types->begin(); /* _d for declaration */
	ptr< typelist > paramlist = types;
	ptr< structlist > friend_of = structlist::create();
	ptr< ss_decl_seq > parameter_decls;
	
	/* the actual ss_function creation */
	ptr< ss_type > func_type = ss_function::instance(return_type, paramlist, false);
	

	/* 1.2 create pair of decl seq and corresponding compound stmt. This is for the operator parameters. */
	ptr< ss_decl_seq > builtin_op_params_decl_seq = sa_decl_seq_compound_pair_creator::instance()->process(
							source_location::zero(),
							ss_decl_seq::root_instance(),
							ss_compound_stmt::root_instance()
						)->first;


	/* 3. create dummy ss_decl_name for the parameter */
	ptr< ss_dummy_name > param_name = ss_dummy_name::create(source_location::zero());

	lint pos = 1;
	for(; it_d != types->end(); it_d++, pos++) {
		/* 4. once we have decl_seq and corresponding (empty) statement, creation of declarations for parameters must be done */
		ptr< ss_parameter_declaration > p_decl = ss_parameter_declaration::create	(
								source_location::zero(), 				/* location */
								ss_declaration_time::create(source_location::zero()->order_get()), /* visible since */
								ss_declaration_time::create(source_location::zero()->order_get()), /* declaration time */
								param_name,					/* ss_decl_name */
								builtin_op_params_decl_seq,			/* contained-in declseq */
								*it_d,						/* ss_type */
								ss_linkage::create("C++", ss_linkage::LINKAGE_EXTERNAL), /* linkage */
								ss_declaration_time::infinity(),		/* initialized since */
								NULL,						/* initializer */
								pos						/* position */
							);
		builtin_op_params_decl_seq->contents_get()->push_back(p_decl);
	}

	ptr< ss_builtin_operator_declaration > op_decl = ss_builtin_operator_declaration::create	(
							source_location::zero(), 				/* location */
							ss_declaration_time::create(source_location::zero()->order_get()), /* visible since */
							ss_declaration_time::create(source_location::zero()->order_get()), /* declaration time */
							op,						/* ss_decl_name */
							builtin_op_decl_seq,/* contained-in declseq */
							func_type,					/* ss_type (ss_function) */
							ss_linkage::create("C++", ss_linkage::LINKAGE_EXTERNAL), /* linkage */
							builtin_op_params_decl_seq,			/* ss_decl_seq(??) parameters */
							NULL						/* body. check="" */
						);
	/* the decl_seq for parameters is declared by the builtin op declaration */
	builtin_op_params_decl_seq->declared_by_set(op_decl);
	/* add the declaration of the op to the builtin op decl seq */
	builtin_op_decl_seq->contents_get()->push_back(op_decl);
}

/*!
 * or_buitlin_operator_declaration_creator is visitor only because of a historic reasons, should be removed
 */ 
void or_builtin_operator_declaration_creator::visit_ss_operator_assign( ptr< ss_operator_assign >)
{	
}

void or_builtin_operator_declaration_creator::visit_ss_operator_comma( ptr< ss_operator_comma > )
{
}

void or_builtin_operator_declaration_creator::visit_ss_dummy_name( ptr< ss_dummy_name> )
{
}

void or_builtin_operator_declaration_creator::visit_ss_operator_function_call( ptr< ss_operator_function_call >)
{
}

void or_builtin_operator_declaration_creator::visit_ss_operator_array( ptr< ss_operator_array >)
{
}

void or_builtin_operator_declaration_creator::visit_ss_operator_access( ptr< ss_operator_access >)
{
}
void or_builtin_operator_declaration_creator::visit_ss_operator_access_member( ptr< ss_operator_access_member >)
{
}

void or_builtin_operator_declaration_creator::visit_ss_operator_land( ptr< ss_operator_land >)
{
}

void or_builtin_operator_declaration_creator::visit_ss_operator_lor( ptr< ss_operator_lor >)
{
}

void or_builtin_operator_declaration_creator::visit_ss_operator_ternary( ptr< ss_operator_ternary >)
{
}

void or_builtin_operator_declaration_creator::visit_ss_ordinary_name( ptr< ss_ordinary_name >)
{
}

void or_builtin_operator_declaration_creator::visit_ss_conversion_name( ptr< ss_conversion_name >)
{
}
void or_builtin_operator_declaration_creator::visit_ss_operator_new( ptr< ss_operator_new >)
{
}

void or_builtin_operator_declaration_creator::visit_ss_operator_delete( ptr< ss_operator_delete >)
{
}

void or_builtin_operator_declaration_creator::visit_ss_operator_new_array( ptr< ss_operator_new_array >)
{
}

void or_builtin_operator_declaration_creator::visit_ss_operator_delete_array( ptr< ss_operator_delete_array >)
{
}
void or_builtin_operator_declaration_creator::visit_ss_operator_add( ptr< ss_operator_add >)
{
}

void or_builtin_operator_declaration_creator::visit_ss_operator_sub( ptr< ss_operator_sub >)
{
}

void or_builtin_operator_declaration_creator::visit_ss_operator_mul( ptr< ss_operator_mul >)
{
}

void or_builtin_operator_declaration_creator::visit_ss_operator_div( ptr< ss_operator_div >)
{
}

void or_builtin_operator_declaration_creator::visit_ss_operator_mod( ptr< ss_operator_mod >)
{
}

void or_builtin_operator_declaration_creator::visit_ss_operator_bxor( ptr< ss_operator_bxor >)
{
}

void or_builtin_operator_declaration_creator::visit_ss_operator_band( ptr< ss_operator_band >)
{
}

void or_builtin_operator_declaration_creator::visit_ss_operator_bor( ptr< ss_operator_bor >)
{
}
void or_builtin_operator_declaration_creator::visit_ss_operator_shr( ptr< ss_operator_shr >)
{
}

void or_builtin_operator_declaration_creator::visit_ss_operator_shl( ptr< ss_operator_shl >)
{
}

void or_builtin_operator_declaration_creator::visit_ss_operator_sbl( ptr< ss_operator_sbl >)
{
}

void or_builtin_operator_declaration_creator::visit_ss_operator_sbg( ptr< ss_operator_sbg >)
{
}

void or_builtin_operator_declaration_creator::visit_ss_operator_sbng( ptr< ss_operator_sbng >)
{
}

void or_builtin_operator_declaration_creator::visit_ss_operator_sbnl( ptr< ss_operator_sbnl >)
{
}

void or_builtin_operator_declaration_creator::visit_ss_operator_sbe( ptr< ss_operator_sbe >)
{
}

void or_builtin_operator_declaration_creator::visit_ss_operator_sbne( ptr< ss_operator_sbne >)
{
}
void or_builtin_operator_declaration_creator::visit_ss_operator_bnot( ptr< ss_operator_bnot >)
{
}

void or_builtin_operator_declaration_creator::visit_ss_operator_lnot( ptr< ss_operator_lnot >)
{
}
void or_builtin_operator_declaration_creator::visit_ss_operator_assign_add( ptr< ss_operator_assign_add >)
{
}

void or_builtin_operator_declaration_creator::visit_ss_operator_assign_sub( ptr< ss_operator_assign_sub >)
{
}

void or_builtin_operator_declaration_creator::visit_ss_operator_assign_mul( ptr< ss_operator_assign_mul >)
{
}

void or_builtin_operator_declaration_creator::visit_ss_operator_assign_div( ptr< ss_operator_assign_div >)
{
}

void or_builtin_operator_declaration_creator::visit_ss_operator_assign_mod( ptr< ss_operator_assign_mod >)
{
}

void or_builtin_operator_declaration_creator::visit_ss_operator_assign_bxor( ptr< ss_operator_assign_bxor >)
{
}

void or_builtin_operator_declaration_creator::visit_ss_operator_assign_band( ptr< ss_operator_assign_band >)
{
}

void or_builtin_operator_declaration_creator::visit_ss_operator_assign_bor( ptr< ss_operator_assign_bor >)
{
}

void or_builtin_operator_declaration_creator::visit_ss_operator_assign_shl( ptr< ss_operator_assign_shl >)
{
}

void or_builtin_operator_declaration_creator::visit_ss_operator_assign_shr( ptr< ss_operator_assign_shr >)
{
}
void or_builtin_operator_declaration_creator::visit_ss_operator_inc( ptr< ss_operator_inc >)
{
}

void or_builtin_operator_declaration_creator::visit_ss_operator_dec( ptr< ss_operator_dec >)
{
}

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);

