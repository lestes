/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/lang/cplus/sem/ss_decl_name.g.hh>
#include <lestes/lang/cplus/sem/ss_decl_name_matcher.g.hh>
#include <lestes/lang/cplus/sem/ss_type.g.hh>

package(lestes);
package(lang);
package(cplus);
package(sem);

bool ss_decl_name::matches( ptr<ss_decl_name> other )
{
	return ss_decl_name_matcher::match( this, other );
}

bool ss_ordinary_name::matches_same( ptr<ss_ordinary_name> o )
{
	// XXX: is this the corret comparison?
	return name_get() == o->name_get();
}

bool ss_conversion_name::matches_same( ptr<ss_conversion_name> o )
{
	// XXX: is this the corret comparison?
	return type_get() == o->type_get();
}

/*!
 * This method always returns true. It is used for all derived classes. For
 * their instances to match, it is sufficient for the actual type to match.
 * Therefore we can implement it here, in their base class.
 *
 * \return  true
 */
bool ss_operator::matches_same( ptr<ss_operator> )
{
	return true;
}

/*!
 * Dummy names never match, aways returns false.
 *
 * \return false
 */
bool ss_dummy_name::matches_same( ptr<ss_dummy_name> )
{
	return false;
}

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);
