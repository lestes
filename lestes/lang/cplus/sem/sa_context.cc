/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/common.hh>
#include <lestes/std/source_location.hh>
#include <lestes/lang/cplus/sem/sa_context.g.hh>
#include <lestes/lang/cplus/sem/ss_misc.g.hh>
#include <lestes/lang/cplus/sem/as_decl.g.hh>

/*! \file
  \author TMA
 */

package(lestes);
package(lang);
package(cplus);
package(sem);

/*!
  Returns the only instance of the SA context manager.
  \return  The manager instance.
*/
ptr<sa_context_manager> sa_context_manager::instance(void)
{
	if (!singleton) {
		singleton = sa_context_manager_concrete::instance();
	}
	return singleton;
}

/*!
  Returns the only instance of the concrete SA context manager.
  \return  The manager instance.
*/
ptr<sa_context_manager_concrete> sa_context_manager_concrete::instance(void)
{
	if (!singleton) {
		ptr< ::lestes::std::stack< srp<sa_context> > > st =
			::lestes::std::stack< srp<sa_context> >::create();
		ptr<ss_decl_seq> decl_seq = ss_decl_seq::root_instance();
		ptr<source_location> loc = decl_seq->location_get();
		ptr<as_name> name = as_name::create(loc,NULL,as_global_namespace_fake_id::create(loc),false);
		ptr<sa_as_context> asctx = sa_as_context::create(name,as_access_specifier_public::create(loc));
		ptr<sa_ss_context> ssctx = sa_ss_context::create(decl_seq,ss_access_specifier::ACCESS_PUBLIC);
		ptr<sa_sa_context> sactx = sa_sa_context::create();
		ptr<sa_context> ctx = sa_context::create(asctx,ssctx,sactx);
		st->push(ctx);
		singleton = sa_context_manager_concrete::create(st);
	}
	return singleton;
}

/*!
  Returns current context.
  \pre Context stack is nonempty.
  \return  The current context.
*/
ptr<sa_context> sa_context_manager_concrete::current(void)
{
	lassert(!contexts->empty());
	return checked(contexts->top());
}

void sa_context_manager_concrete::push(ptr< sa_context > ctx)
{
	lassert(ctx);
	contexts->push(ctx);
}

void sa_context_manager_concrete::pop()
{
	contexts->pop();
}

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);
/* vim: set ft=lestes : */
