/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/lang/cplus/sem/ss_misc.g.hh>
#include <lestes/lang/cplus/sem/ss_statement.g.hh>
#include <lestes/lang/cplus/sem/ss_type_builtin.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>
#include <lestes/lang/cplus/sem/ss_decl_name.g.hh>
#include <lestes/lang/cplus/sem/ss_decl_name_matcher.g.hh>
#include <lestes/std/source_location.hh>

package(lestes);
package(lang);
package(cplus);
package(sem);

/*!
  Tells whether this declaration time is before other declaration time.
  \pre other != NULL
  \param other  The declaration time to compare to.
  \return true  If this declaration is earlier.
*/
bool ss_declaration_time::is_before(ptr<ss_declaration_time> other)
{
	lassert( other );
	return time < other->time;
}

/*!
  The infinity value shall be larger than any regular value.
 */
ptr < ss_declaration_time > ss_declaration_time::infinity()
{
	if (!infinity_instance)
		infinity_instance = ss_declaration_time::create(static_cast < ulint > (-1));
	return infinity_instance;
}

/*!
 * When called for the first time, constructs the grandparent of all instances
 * of ss_decl_seq. This means that you can simply recognize it, just compare
 * your ptr to the return value of this method.
 *
 * Note that the instance is its own parent. Also note that when creating it,
 * root ss_compound_stmt must be created too. This is achieved by calling
 * ss_compound_stmt::root_instance(), which might in turn call us back. The
 * code is prepared for this. You can choose which of the two methods you call
 * first.
 *
 * \return  Pointer to the instance of root ss_decl_seq.
 */
ptr<ss_decl_seq> ss_decl_seq::root_instance()
{
	if (!the_root_instance) {
		/* FIXME: non-null location needed */
		ptr < source_location > loc = source_location::create(file_info::create("",NULL),1,1);
		the_root_instance = new ss_decl_seq(loc,
				list< srp<ss_declaration> >::create(),
				NULL, NULL, list< srp<ss_using_directive> >::create(), NULL );
		/* there is no setter for the parent field... */
		the_root_instance->parent = the_root_instance;
		/* FIXME: non-null location needed _twice_ */
		/* FIXME: non-null ss_declaration_time needed */
		/* FIXME: non-null ss_linkage needed */
		ptr<ss_declaration> gl_decl = ss_namespace_definition::create(
				loc, ss_declaration_time::infinity(), ss_declaration_time::create(0),
				ss_dummy_name::create(loc), the_root_instance,
				ss_void::instance(), ss_linkage::create("C++", ss_linkage::LINKAGE_NO),
				the_root_instance );
		the_root_instance->declared_by_set( gl_decl );
		the_root_instance->contents_get()->push_back( gl_decl );
		/* calls this method again; luckily, we have the_root_instance already set up ;-) */
		the_root_instance->compound_stmt = ss_compound_stmt::root_instance();
	}
	return the_root_instance;
}

/*!
  Check whether the current function is entry point of the translation unit.
  
  This function has to be maintained by semantic part, because backend
  should be independet on current language, which defines entry point(main)[].
					      
*/
bool ss_function_declaration::is_entry_point()
{
	if (contained_in_get() != ss_decl_seq::root_instance()) 		//has to be on global scope
		return false;
	ptr<ss_decl_name> main_name = ss_ordinary_name::create( name_get()->location_get(), "main" );
	if (main_name->matches(name_get())) 					//has to be main
		return true;

	return false;
}


end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);
