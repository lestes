/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Declaration specifier sequence analyser.

  Definition of classes performing declaration specifier analysis.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/sem/sa_declaration_specifier_list.g.hh>
#include <lestes/lang/cplus/sem/sa_declaration_specifier_list.m.hh>
#include <lestes/lang/cplus/sem/as_decl.g.hh>
#include <lestes/lang/cplus/sem/sa_declaration_specifiers.g.hh>
#include <lestes/lang/cplus/sem/as_declaration_specifier2properties.g.hh>
#include <lestes/lang/cplus/sem/ss_type_builtin.g.hh>
#include <lestes/lang/cplus/sem/ss_type.g.hh>

package(lestes);
package(lang);
package(cplus);
package(sem);

/*!
  Processes the declaration specifier sequence within the given context.
  Fills all properties of the sequence into the fields.
  \pre lst != NULL
  \param lst  The list of specifiers to process.
  
*/
ptr<sa_declaration_specifiers> sa_declaration_specifier_list::process(ptr<source_location> loc, ptr<as_declaration_specifier_list_type> lst)
{
	// create the appropriate visitor 
	ptr<as_declaration_specifier2properties> v = as_declaration_specifier2properties::create();
	
	// walk through all specifiers
	for (as_declaration_specifier_list_type::iterator it = lst->begin(), end = lst->end();
			it != end; ++it) {
		// classify individual specifiers
		v->process(*it);
	}

	ptr<ss_type> type;

	// gather nonexclusive specifiers and form the ss_type
	switch (v->type_specifier_get()) {
		case as_declaration_specifier2properties::TS_NONE:
			// implicitly int
			if (!v->short_flag_get() && !v->long_flag_get() &&
				 !v->signed_flag_get() && !v->unsigned_flag_get()) {
				// TODO pt report error: type defaults to int
				report << declaration_without_type << loc;
			}
			// fall through
		case as_declaration_specifier2properties::TS_INT:
			if (v->unsigned_flag_get()) {
				if (v->short_flag_get()) {
					type = ss_type_ushort::instance();
				} else if (v->long_flag_get()) {
					type = ss_type_ulong::instance();
				} else {
					type = ss_type_uint::instance();
				}
			} else {
				// int type is implicitly signed, also for bitfields
				if (v->short_flag_get()) {
					type = ss_type_sshort::instance();
				} else if (v->long_flag_get()) {
					type = ss_type_slong::instance();
				} else {
					type = ss_type_sint::instance();
				}
			}
			break;
		case as_declaration_specifier2properties::TS_CHAR:
			if (v->short_flag_get() || v->long_flag_get()) {
				// report error: sh/lo invalid for char
				report << invalid_char_specifier << loc;
			}
			if (v->signed_flag_get()) {
				type = ss_type_schar::instance();
			} else if (v->unsigned_flag_get()) {
				type = ss_type_uchar::instance();
			} else {
				type = ss_type_pchar::instance();
			}
			break;
		case as_declaration_specifier2properties::TS_DOUBLE:
			if (v->signed_flag_get() || v->unsigned_flag_get() || v->short_flag_get()) {
				// report error: s/u/sh invalid for double
				report << invalid_double_specifier << loc;
			}
			if (v->long_flag_get()) {
				type = ss_type_ldouble::instance();
			} else {
				type = ss_type_double::instance();
			}
			break;
		case as_declaration_specifier2properties::TS_OTHER:
			if (v->signed_flag_get() || v->unsigned_flag_get() || v->short_flag_get() || v->long_flag_get()) {
				// pt report error: s/u/sh/lo invalid for the specifier
			}
			type = v->other_type_get();
			break;
		default:
			lassert(false);
			break;
	}

	// add cv-qualifier to the type
	if (v->const_flag_get()) type = ss_const::instance(type);
	if (v->volatile_flag_get()) type = ss_volatile::instance(type);

	// return the result
	return sa_declaration_specifiers::create(type,v->storage_class_get(), v->explicit_flag_get(),
		v->inline_flag_get(), v->virtual_flag_get(), v->friend_flag_get());
}

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);
/* vim: set ft=lestes : */
