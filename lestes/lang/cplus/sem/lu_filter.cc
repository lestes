/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/lang/cplus/sem/lu_filter.g.hh>
#include <lestes/lang/cplus/sem/ss_decl_name.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>
#include <lestes/lang/cplus/sem/ss_misc.g.hh>

package(lestes);
package(lang);
package(cplus);
package(sem);

lstring lu_filter::result2lstring( result_type result )
{
	switch (result) {
	case FR_NO:
		return "FR_NO";
	case FR_YES:
		return "FR_YES";
	case FR_YES_CONT:
		return "FR_YES_CONT";
	}
	lassert(false);
}

void lu_yes_cont_filter::default_action( ptr<ss_declaration> )
{
	result_set( FR_YES_CONT );
}

void lu_yes_filter::default_action( ptr<ss_declaration> )
{
	result_set( FR_YES );
}

void lu_name_filter::default_action( ptr<ss_declaration> d )
{
	if (d->name_get()->matches( name_get() ))
		result_set( FR_YES_CONT );
	else
		result_set( FR_NO );
}

void lu_visible_filter::default_action( ptr<ss_declaration> d )
{
	// FIXME: visibility is ignored for now; remove following line when times are implemented
	return result_set( FR_YES_CONT );

	if (d->visible_since_get()->is_before( visible_before_get() ))
		result_set( FR_YES_CONT );
	else
		result_set( FR_NO );
}

void lu_multi_filter::default_action( ptr<ss_declaration> d )
{
	result_type partial = FR_YES_CONT;
	for ( list< srp<lu_filter> >::const_iterator it = filter_list_get()->begin();
			partial != FR_NO && it != filter_list_get()->end(); ++it ) {

		result_type local = (*it)->filter(d);
		// quit with FR_NO after first FR_NO
		// after first FR_YES, downgrade from FR_YES_CONT to FR_YES
		if (local != FR_YES_CONT)
			partial = local;
	}
	result_set(partial);
}

void lu_multi_filter::add_filter( ptr<lu_filter> f )
{
	filter_list_get()->push_back(f);
}

void lu_struct_filter::default_action( ptr<ss_declaration> )
{
	result_set( FR_NO );
}

void lu_struct_filter::visit_ss_structure_declaration( ptr< ss_structure_declaration > )
{
	result_set( FR_YES_CONT );
}

void lu_namespace_filter::default_action( ptr<ss_declaration> )
{
	result_set( FR_NO );
}

void lu_namespace_filter::visit_ss_namespace_definition( ptr< ss_namespace_definition > )
{
	result_set( FR_YES );
}

void lu_elaborated_filter::default_action( ptr< ss_declaration > )
{
	result_set( FR_NO );
}

void lu_elaborated_filter::visit_ss_injected_class_declaration( ptr< ss_injected_class_declaration > )
{
	result_set( FR_YES );
	// FIXME: yes, but ill-formed
}

void lu_elaborated_filter::visit_ss_enum_definition( ptr< ss_enum_definition > )
{
	result_set( FR_YES );
}

void lu_elaborated_filter::visit_ss_typedef_definition( ptr< ss_typedef_definition > )
{
	result_set( FR_YES );
	// FIXME: yes, but ill-formed
}

void lu_elaborated_filter::visit_ss_structure_declaration( ptr< ss_structure_declaration > )
{
	result_set( FR_YES );
}

void lu_elaborated_filter::visit_ss_using_declaration( ptr< ss_using_declaration > )
{
	lassert( false );
}

void lu_func_cont_filter::default_action( ptr< ss_declaration > )
{
	result_set( FR_YES );
}

void lu_func_cont_filter::visit_ss_function_declaration( ptr< ss_function_declaration > )
{
	result_set( FR_YES_CONT );
}

void lu_func_cont_filter::visit_ss_method_declaration( ptr< ss_method_declaration > )
{
	result_set( FR_YES_CONT );
}

void lu_func_cont_filter::visit_ss_builtin_operator_declaration( ptr< ss_builtin_operator_declaration > )
{
	result_set( FR_YES_CONT );
}

void lu_qual_filter::default_action( ptr< ss_declaration > )
{
	result_set( FR_YES_CONT );
}

void lu_qual_filter::visit_ss_object_declaration( ptr< ss_object_declaration > )
{
	result_set( FR_NO );
}

void lu_qual_filter::visit_ss_bitfield_declaration( ptr< ss_bitfield_declaration > )
{
	result_set( FR_NO );
}

void lu_qual_filter::visit_ss_parameter_declaration( ptr< ss_parameter_declaration > )
{
	result_set( FR_NO );
}

void lu_qual_filter::visit_ss_fake_declaration( ptr< ss_fake_declaration > )
{
	result_set( FR_NO );
}

void lu_qual_filter::visit_ss_enumerator_declaration( ptr< ss_enumerator_declaration > )
{
	result_set( FR_NO );
}

void lu_qual_filter::visit_ss_function_declaration( ptr< ss_function_declaration > )
{
	result_set( FR_NO );
}

void lu_qual_filter::visit_ss_method_declaration( ptr< ss_method_declaration > )
{
	result_set( FR_NO );
}

void lu_qual_filter::visit_ss_builtin_operator_declaration( ptr< ss_builtin_operator_declaration > )
{
	result_set( FR_NO );
}

/*!
  Default action for non-secu declarations.
  Sets result to reject.
*/
void lu_secu_filter::default_action(ptr<ss_declaration>)
{
	result_set(FR_NO);
}

/*!
  Visits injected class declaration.
  Sets result to accept.
*/
void lu_secu_filter::visit_ss_injected_class_declaration(ptr<ss_injected_class_declaration>)
{
	result_set(FR_YES_CONT);
}

/*!
  Visits enumeration definition.
  Sets result to accept.
*/
void lu_secu_filter::visit_ss_enum_definition(ptr<ss_enum_definition>)
{
	result_set(FR_YES_CONT);
}

/*!
  Visits structure declaration.
  Sets result to accept.
*/
void lu_secu_filter::visit_ss_structure_declaration(ptr<ss_structure_declaration>)
{
	result_set(FR_YES_CONT);
}

/*!
  Inverts the result of applying the contained filter on the declaration.
  \param declaration  The declaration to filter.
*/
void lu_inversion_filter::default_action(ptr<ss_declaration> declaration)
{
	result_type r = inverted->filter(declaration);
	if (r == FR_NO)
		result_set(FR_YES);
	else
		result_set(FR_NO);
}

#if 0
void lu_filter::visit_ss_namespace_definition( ptr< ss_namespace_definition > )
{
	lassert( false );
}

void lu_filter::visit_ss_object_declaration( ptr< ss_object_declaration > )
{
	lassert( false );
}

void lu_filter::visit_ss_bitfield_declaration( ptr< ss_bitfield_declaration > )
{
	lassert( false );
}

void lu_filter::visit_ss_parameter_declaration( ptr< ss_parameter_declaration > )
{
	lassert( false );
}

void lu_filter::visit_ss_fake_declaration( ptr< ss_fake_declaration > )
{
	lassert( false );
}

void lu_filter::visit_ss_injected_class_declaration( ptr< ss_injected_class_declaration > )
{
	lassert( false );
}

void lu_filter::visit_ss_enum_definition( ptr< ss_enum_definition > )
{
	lassert( false );
}

void lu_filter::visit_ss_typedef_definition( ptr< ss_typedef_definition > )
{
	lassert( false );
}

void lu_filter::visit_ss_enumerator_declaration( ptr< ss_enumerator_declaration > )
{
	lassert( false );
}

void lu_filter::visit_ss_structure_declaration( ptr< ss_structure_declaration > )
{
	lassert( false );
}

void lu_filter::visit_ss_function_declaration( ptr< ss_function_declaration > )
{
	lassert( false );
}

void lu_filter::visit_ss_method_declaration( ptr< ss_method_declaration > )
{
	lassert( false );
}

void lu_filter::visit_ss_using_declaration( ptr< ss_using_declaration > )
{
	lassert( false );
}

void lu_filter::visit_ss_compound_stmt_declaration( ptr< ss_compound_stmt_declaration > )
{
	lassert( false );
}

void lu_filter::visit_ss_builtin_operator_declaration( ptr< ss_builtin_operator_declaration > )
{
	lassert( false );
}

#endif

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);

