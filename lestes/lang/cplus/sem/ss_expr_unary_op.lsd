<?xml version="1.0" encoding="UTF-8"?>
<!--
	The lestes compiler suite
	Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
	Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
	Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
	Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
	Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
	Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
	Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; version 2 of the License.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	See the full text of the GNU General Public License version 2, and
	the limitations in the file doc/LICENSE.

	By accepting the license the licensee waives any and all claims
	against the copyright holder(s) related in whole or in part to the
	work, its use, and/or the inability to use it.
-->
<lsd xmlns="http://lestes.jikos.cz/schemas/lsd" xmlns:h="http://www.w3.org/TR/REC-html40">

<comment>               Prelude for the whole header file       </comment>

    <dox file="both">
        <bri>Intercode structure for project Lestes</bri>
        <det>
            This file describes a set of classes and data types used for intercode layer -ss-.
            It is an output from a XSLT template which generates C++ code.
        </det>
    </dox>

        <file-name>ss_expr_unary_op</file-name>
        <imports>
		<i>lestes/common.hh</i>
                <i>lestes/lang/cplus/sem/ss_expression.g.hh</i>
        </imports>
	<implementation-imports>
		<i>lestes/lang/cplus/sem/visitor.v.g.hh</i>
		<i>lestes/lang/cplus/sem/ss_type.g.hh</i>
		<i>lestes/lang/cplus/sem/ss_misc.g.hh</i>
	</implementation-imports>
        <packages>
                <p>lestes</p>
                <p>lang</p>
                <p>cplus</p>
                <p>sem</p>
        </packages>
        <hh-header/>

        <hh-footer/>

        <cc-header/>
        <cc-footer />


<comment>			Unary expressions, operators		</comment>

<include href="ss_expression.lsd"/>
	<xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="ss_expression.lsd">
		<xi:fallback/>
	</xi:include>
<visitor-file href="visitor.lvd"/>

<forward-class name="ss_se" />
<forward-class name="ss_expression" />
<forward-class name="ss_type" />


    <default-base type="ss_unary_expr"/>

    <default-collection kind="list"/>

    <class name="ss_unary_expr" abstract="yes" base="ss_expression">
	<dox>
	    <bri>Base class for function call expressions</bri>
	    <det></det>
	</dox>
	<field name="expr" type="ss_expression"/>
    </class>


    <class name="ss_dereference" base="ss_unary_expr">
	<dox>
	    <bri>Class for dereference operator (*)</bri>
	    <det></det>
	</dox>
        <method name="is_lvalue" type="bool" specifier="virtual">
	    <dox><bri>return true</bri></dox>
        </method>
    </class>


    <class name="ss_conversion" base="ss_unary_expr">
	<dox>
	    <bri>Class for conversion operator </bri>
	    <det></det>
	</dox>
        <field name="src_type" type="ss_type"/>
    </class>

    <class name="ss_array_to_pointer" base="ss_unary_expr">
	<dox>
	    <bri>Class for conversion operator for the array to pointer case</bri>
	    <det>Detailed doxygen documentation</det>
	</dox>
        <field name="src_type" type="ss_type"/>
    </class>

    <class name="ss_bind_reference" base="ss_unary_expr">
	<dox>
	    <bri>Class for reference binding  </bri>
	    <det>	This is conversion operator used to represent reference binding
			from lvalue to reference.</det>
	</dox>
        <field name="src_type" type="ss_type"/>
    </class>


    <class name="ss_bind_to_temporary" base="ss_unary_expr">
	<dox>
	    <bri>Class for pruposes of reference binding </bri>
	    <det>	This is conversion operator used to represent reference binding
			from rvalue to const reference.</det>
	</dox>
        <field name="src_type" type="ss_type"/>
    </class>


    <class name="ss_get" base="ss_unary_expr">
	<dox>
	    <bri>Class representing lvalue to rvalue conversion</bri>
	    <det></det>
	</dox>
    </class>


    <class name="ss_vol_get" base="ss_get">
	<dox>
	    <bri>Class representing lvalue to rvalue conversion (from volatile variable object)</bri>
	    <det></det>
	</dox>
    </class>


    <class name="ss_gat" base="ss_unary_expr">
	<dox>
	    <bri>Class for unary operator plus/gation (+)</bri>
	    <det></det>
	</dox>
    </class>


    <class name="ss_neg" base="ss_unary_expr">
	<dox>
	    <bri>Class for unary operator minus/negation (-)</bri>
	    <det></det>
	</dox>
    </class>


    <class name="ss_lnot" base="ss_unary_expr">
	<dox>
	    <bri> Class for unary operator of logical negation (!)</bri>
	    <det></det>
	</dox>
    </class>


    <class name="ss_bnot" base="ss_unary_expr">
	<dox>
	    <bri> Class for unary operator of binary negation/complement (~)</bri>
	    <det></det>
	</dox>
    </class>


    <class name="ss_address_of" base="ss_unary_expr">
	<dox>
	    <bri> Class for reference operator (&amp;)</bri>
	    <det>Dont mistake with var_ref. Here any lvalue can be referenced.
		Also dont mistake with ss_reference, which is type (eg. difference
		between fundecl(paramdecl &amp;a) X funcall(&amp;argdecl) ).
	    </det>
	</dox>
        <method name="is_lvalue" type="bool" specifier="virtual">
	    <dox><bri>return true</bri></dox>
        </method>
    </class>


</lsd>
