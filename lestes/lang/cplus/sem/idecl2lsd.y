#
# The lestes compiler suite
# Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
# Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
# Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
# Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
# Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
# Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
# Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# See the full text of the GNU General Public License version 2, and
# the limitations in the file doc/LICENSE.
#
# By accepting the license the licensee waives any and all claims
# against the copyright holder(s) related in whole or in part to the
# work, its use, and/or the inability to use it.
#

%token	ID
%token	COLON
%token	LBR
%token	RBR
%token	ELIPS
%token	MIN
%token	COMMA
%token	BRIEFDOC
%token	DETAILEDDOC
%token	PLUS
%token	LPAR
%token	RPAR
%token	SEMIC

%{
#define YYDEBUG 1
# use warnings;
sub yylex;
#define YYDEBUG 1
#define YYPRINT(a,b,c) print a " ", c
%}

%start gd

%%

gd:
	  /* empty */
	| gd cd
	;

doc_opt:
	  /* empty */
	  { $$ = ""; }
	| doc
	  { $$ = $1; }
	;
doc:
	  BRIEFDOC
	  { $$ = "<1\t<dox>\n<2\t<bri>$1</bri>\n<1\t</dox>\n"; }
	| BRIEFDOC DETAILEDDOC
	  { $$ = "<1\t<dox>\n<2\t<bri>$1</bri>\n<2\t<det>$2</det>\n<1\t</dox>\n"; }
	| DETAILEDDOC
	  { $$ = "<1\t<dox>\n<2\t<det>$1</det>\n<1\t</dox>\n"; }
	;
ch:
	  doc_opt ID COLON ID
	  {
		print "\t<class name=\"$2\" base=\"$4\">\n";
		printdoc2($1);
	  }
	| doc_opt MIN ID COLON ID
	  {
		print "\t<class name=\"$3\" base=\"$5\" abstract=\"yes\">\n";
		printdoc2($1);
	  }
	;
cd:
	  ch LBR flds RBR
	  { print "\t</class>\n\n"; }
	| ch LBR flds2 COMMA RBR
	  { print "\t</class>\n\n"; }
	| ch LBR flds2 SEMIC RBR
	  { print "\t</class>\n\n"; }
	;
flds:
	  /* empty */
	| flds2
	;
flds2:
	  fld
	| flds2 COMMA fld
	| flds2 SEMIC fld
	;

method_head:
	  doc_opt ID ID LPAR
	  {
		print "\t\t<method name=\"$3\" type=\"$2\">\n";
		printdoc3($1);
	  }
	| doc_opt MIN ID ID LPAR
	  {
		print "\t\t<method name=\"$4\" type=\"$3\" qualifier=\"abstract\" specifier=\"virtual\">\n";
		printdoc3($1);
	  }
	| doc_opt PLUS ID ID LPAR
	  {
		print "\t\t<method name=\"$4\" type=\"$3\" specifier=\"static\">\n";
		printdoc3($1);
	  }
	| doc_opt PLUS MIN ID ID LPAR
	  {
		print "\t\t<method name=\"$5\" type=\"$4\" specifier=\"virtual\">\n";
		printdoc3($1);
	  }
	;
params:
	  /* empty */
	| params2
	;
params2:
	  param
	| params2 COMMA param
	;
param:
	  ID COLON ID
	  {
		print "\t\t\t<param name=\"$1\" type=\"$3\" />\n";
	  }
	| ID ID
	  {
		print "\t\t\t<param name=\"$2\" type=\"$1\" />\n";
	  }
method:
	  method_head params RPAR
	  {
		print "\t\t</method>\n";
	  }
	;
fld:
	  method
	/*| ID COLON ID
	  { print "\t\t<field name=\"$1\" type=\"$3\" />\n"; }
	| ID ELIPS ID
	  { print "\t\t<collection name=\"$1\" type=\"$3\" />\n"; }
	| MIN ID COLON ID
	  { print "\t\t<field name=\"$2\" type=\"$4\" check=\"\" />\n"; } */
	| doc_opt ID COLON ID
	  {
		print "\t\t<field name=\"$2\" type=\"$4\">\n";
		printdoc3($1);
		print "\t\t</field>\n";
	  }
	| doc_opt ID ELIPS ID
	  {
		print "\t\t<collection name=\"$2\" type=\"$4\">\n";
		printdoc3($1);
		print "\t\t</collection>\n";
	  }
	| doc_opt MIN ID COLON ID
	  {
		print "\t\t<field name=\"$3\" type=\"$5\" check=\"\">\n";
		printdoc3($1);
		print "\t\t</field>\n";
	  }
	;

%%

@tokq = ();

sub yyerror { print @_, "\n"; }

sub yylex
{
	local $_;
	until (@tokq) {
		$_ = <>;
		return YYEOF unless defined $_;
		chomp;
		@tokq = map { (/^(\s*|\/\*)$/o || $_ eq '') ? () : ($_); }
			split /(\s+|[:,;()+]|-|\[|\]|\.\.\.|\/[b*d](?:[^bd*]|[bd*][^\/])*[b*d]\/)/o, $_;
	}
	$yylval = $_ = shift @tokq;
	
	# print "got: $_\n";
	if (/\/[bd]/) {
		$yylval =~ s/\/[bd]\s?//o;
		$yylval =~ s/\s?[b*d]\///o;
		return BRIEFDOC if /\/b/;
		return DETAILEDDOC;
	}
	return LBR if /\[/;
	return RBR if /\]/;
	return COLON if /:/;
	return MIN if /-/;
	return COMMA if /,/;
	return ELIPS if /\.\.\./;
	return LPAR if /\(/;
	return RPAR if /\)/;
	return PLUS if /\+/;
	return SEMIC if /;/;
	return ID;
}

sub printdoc2($)
{
	local $_ = shift;
	s/<1\t/\t\t/go;
	s/<2\t/\t\t\t/go;
	print $_;
}
sub printdoc3($)
{
	local $_ = shift;
	s/<1\t/\t\t\t/go;
	s/<2\t/\t\t\t\t/go;
	print $_;
}

{ # local $yydebug = 1;
yyparse();
}
