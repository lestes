/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Declaration specifier visitor.

  Definition of visitor transforming declaration specifier into SS type and properties.
  \author pt
*/

#include <lestes/common.hh>
#include <lestes/lang/cplus/sem/as_declaration_specifier2properties.g.hh>
#include <lestes/lang/cplus/sem/as_decl.g.hh>
#include <lestes/lang/cplus/sem/ss_type_builtin.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>
#include <lestes/lang/cplus/sem/ss_type.g.hh>
#include <lestes/lang/cplus/sem/as_id_to_declaration_set.g.hh>
#include <lestes/lang/cplus/sem/lu_typedef.hh>

package(lestes);
package(lang);
package(cplus);
package(sem);

/*!
  Processes the declaration specifier.
  \pre specifier != NULL
  \param specifier  The specifier to process.
*/
void as_declaration_specifier2properties::process(ptr<as_declaration_specifier> specifier)
{
	lassert(specifier);
	specifier->accept_as_declaration_specifier_visitor(this);
}

/*!
  Visits instance of as_function_specifier_inline.
  \param specifier  The instance to visit.
*/
void as_declaration_specifier2properties::visit_as_function_specifier_inline(
	ptr< ::lestes::lang::cplus::sem::as_function_specifier_inline > specifier)
{
	lassert(specifier); 
	if (inline_flag) {
		// TODO pt report error: duplicate inline
	}
	inline_flag = true;
}

/*!
  Visits instance of as_function_specifier_virtual.
  \param specifier  The instance to visit.
*/
void as_declaration_specifier2properties::visit_as_function_specifier_virtual(
	ptr< ::lestes::lang::cplus::sem::as_function_specifier_virtual > specifier)
{
	lassert(specifier); 
	if (virtual_flag) {
		// TODO pt report error: duplicat virtual
	}
	virtual_flag = true;
}

/*!
  Visits instance of as_function_specifier_explicit.
  \param specifier  The instance to visit.
*/
void as_declaration_specifier2properties::visit_as_function_specifier_explicit(
	ptr< ::lestes::lang::cplus::sem::as_function_specifier_explicit > specifier)
{
	lassert(specifier); 
	if (explicit_flag) {
		// TODO pt report error: duplicate explicit
	}
	explicit_flag = true;
}

/*!
  Visits instance of as_friend_specifier.
  \param specifier  The instance to visit.
*/
void as_declaration_specifier2properties::visit_as_friend_specifier(
	ptr< ::lestes::lang::cplus::sem::as_friend_specifier > specifier)
{
	lassert(specifier); 
	if (friend_flag) {
		// TODO pt report error: duplicate friend
	}
	friend_flag = true;
}

/*!
  Visits instance of as_typedef_specifier.
  \param specifier  The instance to visit.
*/
void as_declaration_specifier2properties::visit_as_typedef_specifier(
	ptr< ::lestes::lang::cplus::sem::as_typedef_specifier > specifier)
{
	lassert(specifier); 
	if (storage_class == ss_storage_class::ST_NONE) {
		storage_class = ss_storage_class::ST_TYPEDEF;
	} else {
		// TODO pt report errors
	}
}

/*!
  Visits instance of as_storage_class_specifier_auto.
  \param specifier  The instance to visit.
*/
void as_declaration_specifier2properties::visit_as_storage_class_specifier_auto(
	ptr< ::lestes::lang::cplus::sem::as_storage_class_specifier_auto > specifier)
{
	lassert(specifier); 
	if (storage_class == ss_storage_class::ST_NONE) {
		storage_class = ss_storage_class::ST_AUTO;
	} else {
		// TODO pt report error
	}
}

/*!
  Visits instance of as_storage_class_specifier_register.
  \param specifier  The instance to visit.
*/
void as_declaration_specifier2properties::visit_as_storage_class_specifier_register(
	ptr< ::lestes::lang::cplus::sem::as_storage_class_specifier_register > specifier)
{
	lassert(specifier); 
	if (storage_class == ss_storage_class::ST_NONE) {
		storage_class = ss_storage_class::ST_REGISTER;
	} else {
		// TODO pt report error
	}
}

/*!
  Visits instance of as_storage_class_specifier_static.
  \param specifier  The instance to visit.
*/
void as_declaration_specifier2properties::visit_as_storage_class_specifier_static(
	ptr< ::lestes::lang::cplus::sem::as_storage_class_specifier_static > specifier)
{
	lassert(specifier); 
	if (storage_class == ss_storage_class::ST_NONE) {
		storage_class = ss_storage_class::ST_STATIC;
	} else {
		// TODO pt report error
	}
}

/*!
  Visits instance of as_storage_class_specifier_extern.
  \param specifier  The instance to visit.
*/
void as_declaration_specifier2properties::visit_as_storage_class_specifier_extern(
	ptr< ::lestes::lang::cplus::sem::as_storage_class_specifier_extern > specifier)
{
	lassert(specifier); 
	if (storage_class == ss_storage_class::ST_NONE) {
		storage_class = ss_storage_class::ST_EXTERN;
	} else {
		// TODO pt report error
	}
}

/*!
  Visits instance of as_storage_class_specifier_mutable.
  \param specifier  The instance to visit.
*/
void as_declaration_specifier2properties::visit_as_storage_class_specifier_mutable(
	ptr< ::lestes::lang::cplus::sem::as_storage_class_specifier_mutable > specifier)
{
	lassert(specifier); 
	if (storage_class == ss_storage_class::ST_NONE) {
		storage_class = ss_storage_class::ST_MUTABLE;
	} else {
		// TODO pt report error
	}
}

/*!
  Visits instance of as_enumeration_specifier.
  \param specifier  The instance to visit.
*/
void as_declaration_specifier2properties::visit_as_enumeration_specifier(
	ptr< ::lestes::lang::cplus::sem::as_enumeration_specifier > specifier)
{
	lassert(specifier); 
	if (type_specifier == TS_NONE) {
		// TODO pt lookup
		//other_type = lookup(specifier->name_get());
		other_type = ss_type_wchar_t::instance(); // temporarily wchar
		type_specifier = TS_OTHER;
	} else {
		// TODO pt report error: multiple types in declaration 
	}
}

/*!
  Visits instance of as_char_simple_type_specifier.
  \param specifier  The instance to visit.
*/
void as_declaration_specifier2properties::visit_as_char_simple_type_specifier(
	ptr< ::lestes::lang::cplus::sem::as_char_simple_type_specifier > specifier)
{
	lassert(specifier); 
	if (type_specifier == TS_NONE) {
		type_specifier = TS_CHAR;
	} else {
		// TODO pt report error: multiple types
	}
}

/*!
  Visits instance of as_wchar_t_simple_type_specifier.
  \param specifier  The instance to visit.
*/
void as_declaration_specifier2properties::visit_as_wchar_t_simple_type_specifier(
	ptr< ::lestes::lang::cplus::sem::as_wchar_t_simple_type_specifier > specifier)
{
	lassert(specifier); 
	if (type_specifier == TS_NONE) {
		type_specifier = TS_OTHER;
		other_type = ss_type_wchar_t::instance();
	} else {
		// TODO pt report error: multiple types
	}
}

/*!
  Visits instance of as_bool_simple_type_specifier.
  \param specifier  The instance to visit.
*/
void as_declaration_specifier2properties::visit_as_bool_simple_type_specifier(
	ptr< ::lestes::lang::cplus::sem::as_bool_simple_type_specifier > specifier)
{
	lassert(specifier); 
	if (type_specifier == TS_NONE) {
		type_specifier = TS_OTHER;
		other_type = ss_bool::instance();
	} else {
		// TODO pt report error: multiple types
	}
}

/*!
  Visits instance of as_int_simple_type_specifier.
  \param specifier  The instance to visit.
*/
void as_declaration_specifier2properties::visit_as_int_simple_type_specifier(
	ptr< ::lestes::lang::cplus::sem::as_int_simple_type_specifier > specifier)
{
	lassert(specifier); 
	if (type_specifier == TS_NONE) {
		type_specifier = TS_INT;
	} else {
		// TODO pt report error: multiple types
	}
}

/*!
  Visits instance of as_float_simple_type_specifier.
  \param specifier  The instance to visit.
*/
void as_declaration_specifier2properties::visit_as_float_simple_type_specifier(
	ptr< ::lestes::lang::cplus::sem::as_float_simple_type_specifier > specifier)
{
	lassert(specifier); 
	if (type_specifier == TS_NONE) {
		type_specifier = TS_OTHER;
		other_type = ss_type_float::instance();
	} else {
		// TODO pt report error: multiple types
	}
}

/*!
  Visits instance of as_double_simple_type_specifier.
  \param specifier  The instance to visit.
*/
void as_declaration_specifier2properties::visit_as_double_simple_type_specifier(
	ptr< ::lestes::lang::cplus::sem::as_double_simple_type_specifier > specifier)
{
	lassert(specifier); 
	if (type_specifier == TS_NONE) {
		type_specifier = TS_DOUBLE;
	} else {
		// TODO pt report error: multiple types
	}
}

/*!
  Visits instance of as_void_simple_type_specifier.
  \param specifier  The instance to visit.
*/
void as_declaration_specifier2properties::visit_as_void_simple_type_specifier(
	ptr< ::lestes::lang::cplus::sem::as_void_simple_type_specifier > specifier)
{
	lassert(specifier); 
	if (type_specifier == TS_NONE) {
		type_specifier = TS_OTHER;
		other_type = ss_void::instance();
	} else {
		// TODO pt report error: multiple types
	}
}

/*!
  Visits instance of as_short_simple_type_specifier.
  \param specifier  The instance to visit.
*/
void as_declaration_specifier2properties::visit_as_short_simple_type_specifier(
	ptr< ::lestes::lang::cplus::sem::as_short_simple_type_specifier > specifier)
{
	lassert(specifier); 
	if (short_flag) {
		// TODO pt report error
	} else if (long_flag) {
		// TODO pt report error: short and long
	} else {
		short_flag = true;
	}
}

/*!
  Visits instance of as_long_simple_type_specifier.
  \param specifier  The instance to visit.
*/
void as_declaration_specifier2properties::visit_as_long_simple_type_specifier(
	ptr< ::lestes::lang::cplus::sem::as_long_simple_type_specifier > specifier)
{
	lassert(specifier); 
	if (long_flag) {
		// TODO pt report error
	} else if (short_flag) {
		// TODO pt report error: short and long
	} else {
		long_flag = true;
	}
}

/*!
  Visits instance of as_signed_simple_type_specifier.
  \param specifier  The instance to visit.
*/
void as_declaration_specifier2properties::visit_as_signed_simple_type_specifier(
	ptr< ::lestes::lang::cplus::sem::as_signed_simple_type_specifier > specifier)
{
	lassert(specifier); 
	if (signed_flag) {
		// TODO pt report error
	} else if (unsigned_flag) {
		// TODO pt report error: signed and unsigned together
	} else {
		signed_flag = true;
	}
}

/*!
  Visits instance of as_unsigned_simple_type_specifier.
  \param specifier  The instance to visit.
*/
void as_declaration_specifier2properties::visit_as_unsigned_simple_type_specifier(
	ptr< ::lestes::lang::cplus::sem::as_unsigned_simple_type_specifier > specifier)
{
	lassert(specifier); 
	if (unsigned_flag) {
		// TODO pt report error
	} else if (signed_flag) {
		// TODO pt report error: signed and unsigned together
	} else {
		unsigned_flag = true;
	}
}

/*!
  Visits instance of as_cv_qualifier_const.
  \param specifier  The instance to visit.
*/
void as_declaration_specifier2properties::visit_as_cv_qualifier_const(
	ptr< ::lestes::lang::cplus::sem::as_cv_qualifier_const > specifier)
{
	lassert(specifier); 
	if (const_flag) {
		// TODO pt report error
	}
	const_flag = true;
}

/*!
  Visits instance of as_cv_qualifier_volatile.
  \param specifier  The instance to visit.
*/
void as_declaration_specifier2properties::visit_as_cv_qualifier_volatile(
	ptr< ::lestes::lang::cplus::sem::as_cv_qualifier_volatile > specifier)
{
	lassert(specifier); 
	if (volatile_flag) {
		// TODO pt report error
	}
	volatile_flag = true;
}

/*!
  Visits instance of as_cv_qualifier_restrict.
  \param specifier  The instance to visit.
*/
void as_declaration_specifier2properties::visit_as_cv_qualifier_restrict(
	ptr< ::lestes::lang::cplus::sem::as_cv_qualifier_restrict > specifier)
{
	lassert2(false,"restrict is not C++");
	lassert(specifier); 
}

/*!
  Visits instance of as_named_simple_type_specifier.
  \param specifier  The instance to visit.
*/
void as_declaration_specifier2properties::visit_as_named_simple_type_specifier(
	ptr< ::lestes::lang::cplus::sem::as_named_simple_type_specifier > specifier)
{
	lassert(specifier); 
	if (type_specifier == TS_NONE) {
	   ptr<declaration_set_type> decls =
		   as_id_to_declaration_set::instance()->process(specifier->name_get()->identifier_get() );
	   lassert(decls && decls->size() == 1);

		other_type = (*decls->begin())->type_get();
      
      type_specifier = TS_OTHER;
	} else {
		// TODO pt report error: multiple types in declaration 
	} 
}

/*!
  Visits instance of as_elaborated_type_specifier_typename.
  \param specifier  The instance to visit.
*/
void as_declaration_specifier2properties::visit_as_elaborated_type_specifier_typename(
	ptr< ::lestes::lang::cplus::sem::as_elaborated_type_specifier_typename > specifier)
{
	lassert2(false,"typename is unsupported");
	lassert(specifier); 
}

/*!
  Visits instance of as_elaborated_type_specifier_class_key.
  \param specifier  The instance to visit.
*/
void as_declaration_specifier2properties::visit_as_elaborated_type_specifier_class_key(
	ptr< ::lestes::lang::cplus::sem::as_elaborated_type_specifier_class_key > specifier)
{
	lassert(specifier); 
	if (type_specifier == TS_NONE) {
	   ptr<declaration_set_type> decls =
		   as_id_to_declaration_set::instance()->process(specifier->name_get()->identifier_get() );
		// TODO pt add the invisible forward for the class, if the name was not found
	   lassert(decls && decls->size() == 1);

		other_type = (*decls->begin())->type_get();
      
      type_specifier = TS_OTHER;
	} else {
		// TODO pt report error: multiple types in declaration 
	} 
}

/*!
  Visits instance of as_elaborated_type_specifier_enum.
  \param specifier  The instance to visit.
*/
void as_declaration_specifier2properties::visit_as_elaborated_type_specifier_enum(
	ptr< ::lestes::lang::cplus::sem::as_elaborated_type_specifier_enum > specifier)
{
	lassert(specifier); 
	if (type_specifier == TS_NONE) {
	   ptr<declaration_set_type> decls =
		   as_id_to_declaration_set::instance()->process(specifier->name_get()->identifier_get() );
		// TODO pt add the invisible forward for the enum if the name was not found
	   lassert(decls && decls->size() == 1);

		other_type = (*decls->begin())->type_get();
      
      type_specifier = TS_OTHER;
	} else {
		// TODO pt report error: multiple types in declaration 
	} 
}

/*!
  Visits instance of as_class_specifier.
  \param specifier  The instance to visit.
*/
void as_declaration_specifier2properties::visit_as_class_specifier(
	ptr< ::lestes::lang::cplus::sem::as_class_specifier > specifier)
{
	lassert(specifier); 
	if (type_specifier == TS_NONE) {
		// TODO pt lookup
		//other_type = lookup(specifier->name_get(),specifier->key_get()); 
		// TODO pt rename wchar_t to wchar
		other_type = ss_type_wchar_t::instance(); // temporarily wchar
      type_specifier = TS_OTHER;
	} else {
		// TODO pt report error: multiple types in declaration 
	} 
}

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);

