/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*!
	\file
	\brief ss_type identification.
	\author jaz
*/

#include <lestes/lang/cplus/sem/ss_type2id.g.hh>
#include <lestes/lang/cplus/sem/ss_type.g.hh>

package(lestes);
package(lang);
package(cplus);
package(sem);


ptr< ss_type2id > ss_type2id::instance()
{
	if ( !singleton_instance_get()) {
		singleton_instance_set(ss_type2id::create());
	}	
	
	return singleton_instance_get();
}


void ss_type2id::visit_ss_const(ptr< ::lestes::lang::cplus::sem::ss_const >) {
	ret_val = CONST;
}

void ss_type2id::visit_ss_volatile(ptr< ::lestes::lang::cplus::sem::ss_volatile >){
	ret_val = VOLATILE;
}

void ss_type2id::visit_ss_const_volatile(ptr< ::lestes::lang::cplus::sem::ss_const_volatile >){
	ret_val = CONST_VOLATILE;
}

void ss_type2id::visit_ss_reference(ptr< ::lestes::lang::cplus::sem::ss_reference >){
	ret_val = REFERENCE;
}

void ss_type2id::visit_ss_pseudoreference(ptr< ::lestes::lang::cplus::sem::ss_pseudoreference >){
	ret_val = PSEUDOREFERENCE;
}

void ss_type2id::visit_ss_pointer(ptr< ::lestes::lang::cplus::sem::ss_pointer >){
	ret_val = POINTER;
}

void ss_type2id::visit_ss_enum(ptr< ::lestes::lang::cplus::sem::ss_enum >){
	ret_val = ENUM;
}

void ss_type2id::visit_ss_array(ptr< ::lestes::lang::cplus::sem::ss_array >){
	ret_val = ARRAY;
}

void ss_type2id::visit_ss_member_pointer(ptr< ::lestes::lang::cplus::sem::ss_member_pointer >){
	ret_val = POINTER;
}

void ss_type2id::visit_ss_function(ptr< ::lestes::lang::cplus::sem::ss_function >){
	ret_val = FUNCTION;
}

void ss_type2id::visit_ss_member_function(ptr< ::lestes::lang::cplus::sem::ss_member_function >){
	ret_val = MEMBER_FUNCTION;
}

void ss_type2id::visit_ss_class(ptr< ::lestes::lang::cplus::sem::ss_class >){
	ret_val = CLASS;
}

void ss_type2id::visit_ss_union(ptr< ::lestes::lang::cplus::sem::ss_union >){
	ret_val = UNION;
}

void ss_type2id::visit_ss_bool(ptr< ::lestes::lang::cplus::sem::ss_bool >){
	ret_val = BOOL;
}

void ss_type2id::visit_ss_void(ptr< ::lestes::lang::cplus::sem::ss_void >){
	ret_val = VOID;
}

void ss_type2id::visit_ss_type_sint(ptr< ::lestes::lang::cplus::sem::ss_type_sint >){
	ret_val = SINT;
}

void ss_type2id::visit_ss_type_uint(ptr< ::lestes::lang::cplus::sem::ss_type_uint >) {
	ret_val = UINT;
}

void ss_type2id::visit_ss_type_slong(ptr< ::lestes::lang::cplus::sem::ss_type_slong >) {
	ret_val = SLONG;
}

void ss_type2id::visit_ss_type_ulong(ptr< ::lestes::lang::cplus::sem::ss_type_ulong >){
	ret_val = ULONG;
}

void ss_type2id::visit_ss_type_sshort(ptr< ::lestes::lang::cplus::sem::ss_type_sshort >){
	ret_val = SSHORT;
}

void ss_type2id::visit_ss_type_ushort(ptr< ::lestes::lang::cplus::sem::ss_type_ushort >){
	ret_val = USHORT;
}

void ss_type2id::visit_ss_type_wchar_t(ptr< ::lestes::lang::cplus::sem::ss_type_wchar_t >){
	ret_val = WCHAR_T;
}

void ss_type2id::visit_ss_type_schar(ptr< ::lestes::lang::cplus::sem::ss_type_schar >){
	ret_val = SCHAR;
}

void ss_type2id::visit_ss_type_uchar(ptr< ::lestes::lang::cplus::sem::ss_type_uchar >){
	ret_val = UCHAR;
}

void ss_type2id::visit_ss_type_pchar(ptr< ::lestes::lang::cplus::sem::ss_type_pchar >){
	ret_val = PCHAR;
}

void ss_type2id::visit_ss_type_float(ptr< ::lestes::lang::cplus::sem::ss_type_float >){
	ret_val = FLOAT;
}

void ss_type2id::visit_ss_type_double(ptr< ::lestes::lang::cplus::sem::ss_type_double >){
	ret_val = DOUBLE;
}

void ss_type2id::visit_ss_type_ldouble(ptr< ::lestes::lang::cplus::sem::ss_type_ldouble >){
	ret_val = LDOUBLE;
}


end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);

