<?xml version="1.0" encoding="UTF-8"?>
<!--
	The lestes compiler suite
	Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
	Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
	Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
	Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
	Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
	Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
	Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; version 2 of the License.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	See the full text of the GNU General Public License version 2, and
	the limitations in the file doc/LICENSE.

	By accepting the license the licensee waives any and all claims
	against the copyright holder(s) related in whole or in part to the
	work, its use, and/or the inability to use it.
-->
<lsd xmlns="http://lestes.jikos.cz/schemas/lsd" xmlns:h="http://www.w3.org/TR/REC-html40">

	<comment>               Prelude for the whole header file       </comment>

	<dox file="both">
		<bri>Intercode structure for project Lestes</bri>
		<det>
			This file describes a set of classes and data types used for intercode layer -ss-.
			It is an output from a XSLT template which generates C++ code.
		</det>
	</dox>

	<file-name>ss_type</file-name>
	<imports>
		<i>lestes/common.hh</i>
		<i>lestes/std/pair_comparator.hh</i>
		<i>lestes/std/map.hh</i>
		<i>lestes/std/list.hh</i>
		<i>lestes/std/set.hh</i>
		<i>lestes/intercode/intercode.g.hh</i>
		<i>lestes/lang/cplus/sem/or_ics.hh</i>	<!-- for or_cv_enum -->
	</imports>
	<implementation-imports>
		<i>lestes/lang/cplus/sem/as_decl.g.hh</i>
		<i>lestes/lang/cplus/sem/ss_declaration.g.hh</i>
		<i>lestes/lang/cplus/sem/ss_type_visitor.v.g.hh</i>
		<i>lestes/lang/cplus/sem/or_visitor.v.g.hh</i>
		<i>lestes/lang/cplus/sem/ss_declaration_visitor.v.g.hh</i>
		<i>lestes/lang/cplus/sem/ss_decl_name_visitor.v.g.hh</i>
		<i>lestes/lang/cplus/sem/ss_misc.g.hh</i>
		<i>lestes/md/types/tm_data_type_base.g.hh</i>
		<i>lestes/lang/cplus/lex/location.hh</i>
		<i>lestes/lang/cplus/sem/ss_expression.g.hh</i>
		<i>lestes/lang/cplus/sem/ss_type.hh</i>
	</implementation-imports>
	<packages>
		<p>lestes</p>
		<p>lang</p>
		<p>cplus</p>
		<p>sem</p>
	</packages>
	<hh-header>
		class ss_function_comparator;
	</hh-header>

	<hh-footer />

	<cc-header />
	<cc-footer />


	<comment> 			TYPES				</comment>

	<forward-class name="ss_declaration" />
	<forward-class name="ss_decl_seq" />
	<forward-class name="ss_base_specifier" />
	<forward-class name="ss_declaration_time" />
	<forward-class name="as_name" />
	<forward-class name="or_ics_functional" />
	
	<foreign-class name="tm_data_type_base">
		<p>lestes</p>
		<p>md</p>
		<p>types</p>
	</foreign-class>

	<using-class name="ss_base" packages="::lestes::intercode::" />

	<default-base type="ss_type" />

	<default-collection kind="list" />
	<visitor-file href="visitor.lvd" />


	<class name="ss_type" abstract="yes" base="ss_base">
		<dox>
			<bri>Base class for intercode types</bri>
			<det></det>
		</dox>
		<method name="is_void" type="bool" specifier="virtual">
			<dox><bri>Is this type void?</bri>
				<det>Used for genarating pi calls. 
					Differentiate between call and callv. 
					Generic case returns false;
			</det></dox>
		</method>			      
		<method name="is_volatile" type="bool" specifier="virtual">
			<dox><bri>Is this type volatile?</bri>
				<det>
					Used for generating side effects. 
					Generic case returns false;
				</det>
			</dox>
		</method>
		<visitor name="or_ics_base" type="ptr&lt; ::lestes::std::list&lt; srp&lt; or_ics_functional &gt; &gt; &gt;">
			<dox><bri>Visitor for purposes of overload resolution algorithm, for visiting types</bri></dox>
		</visitor>
		<visitor name="or_ics_base_cv" type="or_cv_enum">
			<dox><bri>Visitor for checking cv-qualification of the type</bri></dox>
		</visitor>
		<visitor name="ss_type2tm_type_gen_base" type="tm_data_type_base">
			<dox><bri>Visitor for converting types into pseudoinstructions types</bri></dox>
		</visitor>
		<visitor name="ss_type_visitor" type="void" />
	</class>

	<class name="ss_builtin_type" abstract="yes"  base="ss_type">
		<dox>
			<bri>Class for built-in types</bri>
			<det></det>
		</dox>
	</class>


	<class name="ss_const"  base="ss_type">
		<dox>
			<bri>Class for const types</bri>
			<det>Singleton class.</det>
		</dox>
		<field name="what" type="ss_type" />
		<method name="is_void" type="bool" specifier="virtual">
			<dox><bri>Is this type void?</bri>
				<det>Used for genarating pi calls. 
					Differentiate between call and callv. 
					Generic case returns false;
			</det></dox>
		</method>			      
		<collection name="the_instances" specifier="static" type="ss_type" get="none" alt="none" set="none"
			key="ss_type" kind="map" init="">
			<dox>
				<bri>holder for the class instances</bri>
			</dox>
		</collection>
		<method name="instance" type="ss_type" specifier="static">
			<dox>
				<bri>get the instance representing appropriately qualified argument type</bri>
			</dox>
			<param name="type" type="ss_type">
				<dox>
					<bri>the type refered to</bri>
				</dox>
			</param>
		</method>
	</class>


	<class name="ss_volatile"  base="ss_type">
		<dox>
			<bri>Class for volatile types</bri>
			<det>Singleton class.</det>
		</dox>
		<field name="what" type="ss_type" />
		<method name="is_void" type="bool" specifier="virtual">
			<dox><bri>Is this type void?</bri>
				<det>Used for genarating pi calls. 
					Differentiate between call and callv. 
					Generic case returns false;
			</det></dox>
		</method>			      
		<method name="is_volatile" type="bool" specifier="virtual">
			<dox><bri>Is this type volatile?</bri>
				<det>Used for generating side effects. 
					Returns true.
			</det></dox>
		</method>
		<collection name="the_instances" specifier="static" type="ss_type" get="none" alt="none" set="none"
			key="ss_type" kind="map" init="">
			<dox>
				<bri>holder for the class instances</bri>
			</dox>
		</collection>
		<method name="instance" type="ss_type" specifier="static">
			<dox>
				<bri>get the instance representing appropriately qualified argument type</bri>
			</dox>
			<param name="type" type="ss_type">
				<dox>
					<bri>the type refered to</bri>
				</dox>
			</param>
		</method>
	</class>


	<class name="ss_const_volatile"  base="ss_type">
		<dox>
			<bri>Class for volatile types</bri>
			<det>Singleton class</det>
		</dox>
		<field name="what" type="ss_type" />
		<method name="is_void" type="bool" specifier="virtual">
			<dox><bri>Is this type void?</bri>
				<det>Used for genarating pi calls. 
					Differentiate between call and callv. 
					Generic case returns false;
			</det></dox>
		</method>			      
		<method name="is_volatile" type="bool" specifier="virtual">
			<dox><bri>Is this type volatile?</bri>
				<det>Used for generating side effects. 
					Returns true.
			</det></dox>
		</method>
	</class>


	<class name="ss_referential"  abstract="yes" base="ss_type">
		<dox>
			<bri>Class for references</bri>
			<det>Type representing lvalues.</det>
		</dox>
		<field name="what" type="ss_type" />
		<method name="is_void" type="bool" specifier="virtual">
			<dox><bri>Is this type void?</bri>
				<det>Used for genarating pi calls. 
					Differentiate between call and callv. 
					Generic case returns false;
			</det></dox>
		</method>			      
		<method name="is_volatile" type="bool" specifier="virtual">
			<dox><bri>Is this type volatile?</bri>
				<det>Used for generating side effects. 
					Returns true.
			</det></dox>
		</method>

	</class>


	<class name="ss_reference"  base="ss_referential">
		<dox>
			<bri>Class for referenced types via &amp;</bri>
			<det>Compare with ss_address_of. Singleton class.</det>
		</dox>
		<collection name="the_instances" specifier="static" type="ss_type" get="none" alt="none" set="none"
			key="ss_type" kind="map" init="">
			<dox>
				<bri>holder for the class instances</bri>
			</dox>
		</collection>
		<method name="instance" type="ss_type" specifier="static">
			<dox>
				<bri>get the instance representing reference to the argument type</bri>
			</dox>
			<param name="type" type="ss_type">
				<dox>
					<bri>the type refered to</bri>
				</dox>
			</param>
		</method>
	</class>


	<class name="ss_pseudoreference"  base="ss_referential">
		<dox>
			<bri>Class for pseudo-referenced types</bri>
			<det>Represents usual variable.</det>
		</dox>
		<collection name="the_instances" specifier="static" type="ss_type" get="none" alt="none" set="none"
			key="ss_type" kind="map" init="">
			<dox>
				<bri>holder for the class instances</bri>
			</dox>
		</collection>
		<method name="instance" type="ss_type" specifier="static">
			<dox>
				<bri>get the instance representing reference to the argument type</bri>
			</dox>
			<param name="type" type="ss_type">
				<dox>
					<bri>the type refered to</bri>
				</dox>
			</param>
		</method>
	</class>


	<class name="ss_pointer"  base="ss_type">
		<dox>
			<bri>Class for pointered types</bri>
			<det>Singleton class.</det>
		</dox>
		<field name="what" type="ss_type" />
		<collection name="the_instances" specifier="static" type="ss_type" get="none" alt="none" set="none"
			key="ss_type" kind="map" init="">
			<dox>
				<bri>holder for the class instances</bri>
			</dox>
		</collection>
		<method name="instance" type="ss_type" specifier="static">
			<dox>
				<bri>get the instance representing pointer to the argument type</bri>
			</dox>
			<param name="type" type="ss_type">
				<dox>
					<bri>the pointee</bri>
				</dox>
			</param>
		</method>
	</class>


	<class name="ss_enum"  base="ss_type">
		<dox>
			<bri>Class for enumeration type</bri>
			<det></det>
		</dox>
		<collection name="enumerators" type="ss_declaration" kind="list" />
	</class>


	<class name="ss_array"  base="ss_type">
		<dox>
			<bri>Class for array type</bri>
			<det></det>
		</dox>
		<field name="bound" type="t_size" />
		<field name="type" type="ss_type" />
		<collection name="the_instances" specifier="static" type="ss_type" get="none" alt="none" set="none"
			key="srp &lt; ::lestes::std::pair &lt; t_size, srp &lt; ss_type > > >" kind="map"
			comparator="::lestes::std::pair_comparator &lt; srp &lt; ::lestes::std::pair &lt; t_size, srp &lt; ss_type > > > >"
			init="">
			<dox>
				<bri>holder for the class instances</bri>
			</dox>
		</collection>
		<method name="instance" type="ss_type" specifier="static">
			<dox>
				<bri>get the instance representing array of the argument type</bri>
			</dox>
			<param name="size" type="t_size">
				<dox>
					<bri>the dimension</bri>
				</dox>
			</param>
			<param name="type" type="ss_type">
				<dox>
					<bri>the element type</bri>
				</dox>
			</param>
		</method>
	</class>


	<class name="ss_member_pointer"  base="ss_type">
		<dox>
			<bri>Class for member pointer (8.3.3)</bri>
			<det></det>
		</dox>
		<field name="base" type="ss_type" />
		<field name="item" type="ss_type" />
		<collection name="the_instances" specifier="static" type="ss_type" get="none" alt="none" set="none"
			key="srp &lt; ::lestes::std::pair &lt; srp &lt; ss_type >, srp &lt; ss_type > > >" kind="map"
			comparator="::lestes::std::pair_comparator &lt; srp &lt; ::lestes::std::pair &lt; srp &lt; ss_type >, srp &lt; ss_type > > > >"
			init="">
			<dox>
				<bri>holder for the class instances</bri>
			</dox>
		</collection>
		<method name="instance" type="ss_type" specifier="static">
			<dox>
				<bri>get the instance representing array of the argument type</bri>
			</dox>
			<param name="base_type" type="ss_type">
				<dox>
					<bri>the type of class into which the pointer points</bri>
				</dox>
			</param>
			<param name="element_type" type="ss_type">
				<dox>
					<bri>the element type to which the pointer points</bri>
				</dox>
			</param>
		</method>
	</class>


	<class name="ss_function"  base="ss_type">
		<dox>
			<bri>Class for function type.</bri>
			<det>Represents namespace function.</det>
		</dox>
		<collection name="instances" specifier="static" type="ss_function" get="none" alt="none" set="none"
			kind="set"
			comparator="ss_function_comparator"
			init="">
			<dox>
				<bri>The singleton instances of the class.</bri>
			</dox>
		</collection>
		<field name="returns" type="ss_type">
			<dox>
				<bri>The return type of the function.</bri>
			</dox>
		</field>
		<collection name="params" type="ss_type" kind="list">
			<dox>
				<bri>The types of parameters of the function.</bri>
			</dox>
		</collection>
		<comment>	?terminologie</comment>
		<field name="ellipsis" type="bool" >
			<dox>
				<bri>Whether current function has an ellipsis(...) in function parameters.</bri>
				<det>Rationale for field contra individual class: There is no C++ type, which corresponds to ellipsis.</det>
			</dox>
		</field>
		<method name="instance" type="ss_type" specifier="static">
			<dox>
				<bri>Returns the instance representing specified function.</bri>
			</dox>
			<param name="return_type" type="ss_type" />
			<param name="param_types" type="ptr &lt; ::lestes::std::list &lt; srp&lt;ss_type&gt; &gt; &gt;" />
			<param name="ellipsis" type="bool" />
		</method>
		<method name="less_than" type="bool">
			<dox>
				<bri>Compares to other ss_function for inequality.</bri>
			</dox>
			<param name="other" type="ss_function" />
		</method>
		<method name="equal_signature" type="bool">
			<dox>
				<bri>Compares to other ss_function signature.</bri>
			</dox>
			<param name="other" type="ss_function" />
		</method>
	</class>

	<class name="ss_member_function"  base="ss_function">
		<dox>
			<bri>Class for member function type</bri>
			<det></det>
		</dox>
		<field name="this_type" type="ss_type" >
			<dox>
				<bri> The type of "this" as it will be seen in the function.</bri>
				<det> 
					In a function declared const in class "a" (e.g. int a::f(int) const;)
					This field will represent the type "a const *".
				</det>
			</dox>
		</field>
	</class>

	<class name="ss_struct_base"  abstract="yes" base="ss_type">
		<dox>
			<bri>Base class for structurated objects</bri>
		</dox>
		<field name="members" type="ss_decl_seq"/>
		<field name="decl" type="ss_declaration">
			<dox><bri>Points to declaration of this class</bri>
				<det>Declaration is needed because of access to enclosing scopes.</det></dox>
		</field>
		<field name="POD" type="bool">
			<dox><bri>Is this class POD strcuture ?</bri></dox>
		</field>    
		<field name="completion_time" type="ss_declaration_time">
			<dox>
				<bri> Structures can have forward declaration </bri>
				<det> Before structure is completed, this field is set to NULL. </det>
			</dox>
		</field>
	</class>


	<class name="ss_class"  base="ss_struct_base">
		<dox>
			<bri>Class for struct-class types</bri>
			<det>Distinction of struct-class: default visibility is resolved before. TODO struct x class POD status</det>
		</dox>
		<collection name="bases" type="ss_base_specifier" kind="list">
			<dox>
				<det>Bases of the class are in the list, because ABI-structure-layout has to now order.</det>
			</dox>
		</collection>
		<collection name="descendants" type="ss_class" kind="set" init="">
			<dox><bri>Direct descendant classes of current class.</bri>
				<det>Rationale: Overload resolution needs to know descendants. 
					(Conversions of member pointers into member pointer of derived class.[4.11/2])
				</det>
			</dox>
		</collection>
		<method name="is_ancestor" type="bool" >
			<dox><bri>Check whether is this class ancestor of parameter.</bri>
				<det>Used in overload resolution.	    
			</det></dox>
			<param name="descendant" type="ss_class"/>
		</method>	     
	</class>


	<class name="ss_union"  base="ss_struct_base">
		<dox>
			<bri>Class for union type</bri>
			<det></det>
		</dox>
	</class>


	<class name="ss_typename_type"  abstract="yes" base="ss_type">
		<dox>
			<bri>Class for template typename types</bri>
			<det>
				This type is prepared for template typename types such as:

				template&lt;typename X&gt; class A {
				typename X::T a(typename X::F);
				};

				and since the templates are not now implemented, processing with this
				type will cause lassert.
			</det>
		</dox>
		<field name="contained_in" type="ss_decl_seq"/>
		<field name="decl_time" type="ss_declaration_time"/>
		<field name="qualified_name" type="as_name">
			<dox>
				<bri>This field represents qualified name.</bri>
				<det>
					Since we dont know the type before template initialization happens, there is
					no obvious standard ss_type representing this name. To convert
					current as type to ss type, we will wait for instantiation.
				</det>
			</dox>
		</field>
	</class>


</lsd>
