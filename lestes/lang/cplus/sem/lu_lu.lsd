<?xml version="1.0" encoding="iso-8859-1"?>
<!--
	The lestes compiler suite
	Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
	Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
	Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
	Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
	Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
	Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
	Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; version 2 of the License.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	See the full text of the GNU General Public License version 2, and
	the limitations in the file doc/LICENSE.

	By accepting the license the licensee waives any and all claims
	against the copyright holder(s) related in whole or in part to the
	work, its use, and/or the inability to use it.
-->
<lsd xmlns="http://lestes.jikos.cz/schemas/lsd" xmlns:h="http://www.w3.org/TR/REC-html40">

	<file-name>lu_lu</file-name>

	<packages>
		<p>lestes</p>
		<p>lang</p>
		<p>cplus</p>
		<p>sem</p>
	</packages>

	<include href="ss_declaration_visitor.v.lsd" />
	<include href="ss_type_visitor.v.lsd" />	<!-- change to ss_type_visitor when it is separated from the big file -->

	<imports>
		<i>lestes/common.hh</i>
		<i>lestes/lang/cplus/sem/lu_typedef.hh</i>
		<i>lestes/lang/cplus/sem/ss_declaration_visitor.v.g.hh</i>
		<i>lestes/lang/cplus/sem/ss_type_visitor.v.g.hh</i>
		<i>lestes/std/set.hh</i>
	</imports>

	<implementation-imports>
		<i>lestes/lang/cplus/sem/lu_filter.g.hh</i>
		<i>lestes/lang/cplus/sem/ss_declaration.g.hh</i>
		<i>lestes/lang/cplus/sem/ss_misc.g.hh</i>
		<i>lestes/lang/cplus/sem/ss_type.g.hh</i>
	</implementation-imports>

	<foreign-class name="object"><p>lestes</p><p>std</p></foreign-class>

	<forward-class name="ss_decl_seq" />
	<forward-class name="ss_declaration_time" />
	<forward-class name="ss_type" />
	<forward-class name="lu_filter" />

	<using-class name="declaration_set_type" />
	<using-class name="decl_seq_set_type" />

	<class name="lu_params" base="object">
		<enum name="using_directives_handling_type">
			<e n="UDIR_IGNORE">
				<dox>
					<bri>Never consider using directives.</bri>
					<det>
						Used in argument-dependent lookup, for example.
					</det>
				</dox>
			</e>
			<e n="UDIR_ALWAYS">
				<dox>
					<bri>Consider using directives even when looked up identifier was found.</bri>
					<det>
						Used in unqualified lookup, for example.
					</det>
				</dox>
			</e>
			<e n="UDIR_FALLBACK">
				<dox>
					<bri>Consider using directives only when looked up identifier was not found.</bri>
					<det>
						Used in qualified lookup, for example.
					</det>
				</dox>
			</e>
		</enum>

		<enum name="using_declarations_handling_type">
			<e n="UDECL_IGNORE">
				<dox>
					<bri>Do not process instances of ss_using_declaration at all.</bri>
					<det>
						Instances of ss_using_declaration are immediately thrown away, they do not reach the filter.
					</det>
				</dox>
			</e>
			<e n="UDECL_THROUGH">
				<dox>
					<bri>Process instances of ss_using_declaration as other kinds of declarations.</bri>
					<det>
						Instances of ss_using_declaration are not resolved but processed verbatim.
					</det>
				</dox>
			</e>
			<e n="UDECL_RESOLVE_BEFORE_FILTER">
				<dox>
					<bri>Resolve instances of ss_using_declaration and process their targets before calling the filter.</bri>
					<det>
						Before calling filter on a declaration, it is resolved using ss_using_target.
					</det>
				</dox>
			</e>
			<e n="UDECL_RESOLVE_AFTER_FILTER">
				<dox>
					<bri>Resolve instances of ss_using_declaration and process their targets after calling the filter.</bri>
					<det>
						After a declaration passes the filter, it is resolved using ss_using_target.
					</det>
				</dox>
			</e>
		</enum>

		<enum name="parent_search_type">
			<e n="SEARCH_PARENTS">
				<dox>
					<bri>Search parents of ss_decl_seq in question.</bri>
					<det>
						Parents (including transitive ones) of given ss_decl_seq are searched until a declaration is found.
					</det>
				</dox>
			</e>
			<e n="SKIP_PARENTS">
				<dox>
					<bri>Do not search parents at all.</bri>
					<det>
						Only specified ss_decl_seq is searched. When matching declaration was not found, the search stops.
					</det>
				</dox>
			</e>
		</enum>

		<field name="using_directives_handling" type="using_directives_handling_type" />
		<field name="using_declarations_handling" type="using_declarations_handling_type" />
		<field name="parent_search" type="parent_search_type" />
		<field name="time" type="ss_declaration_time" />
		<field name="filter" type="lu_filter" />
	</class>
	
	<class name="lu_lookup" base="ss_declaration_default_visitor" create="none">
		<singleton />
		<declare-abstract-methods exact="default_action" />

		<declare-abstract-methods exact="visit_ss_namespace_definition" />
		<declare-abstract-methods exact="visit_ss_function_declaration" />
		<declare-abstract-methods exact="visit_ss_parameters_declaration" />
		<declare-abstract-methods exact="visit_ss_compound_stmt_declaration" />
		<declare-abstract-methods exact="visit_ss_structure_declaration" />

		<field name="params" type="lu_params" init="NULL" check="" get="protected" set="protected" alt="protected">
			<dox><bri>Used internally by the visitor; holds the parameters of the lookup.</bri></dox>
		</field>

		<field name="visited_seqs" type="decl_seq_set_type" init="NULL" check="" get="protected" set="protected" alt="protected">
			<dox><bri>Used internally by the visitor; holds decl_seqs already visited during current invocation of the 'main' method.</bri></dox>
		</field>

		<method name="main" type="declaration_set_type">
			<param type="ss_decl_seq" name="seq" />
			<param type="lu_params" name="params" />
		</method>

		<method specifier="static" name="simple_scan" type="declaration_set_type">
			<dox><bri>Used internally, but can be used from outside as well.</bri></dox>
			<param type="ss_decl_seq" name="seq" />
			<param type="lu_params" name="params" />
		</method>

		<method name="internal_simple_scan" type="declaration_set_type">
			<dox><bri>Used internally, just a wrapper around simple_scan() that adds params to the argument list.</bri></dox>
			<param type="ss_decl_seq" name="seq" />
		</method>

		<method name="deep_scan" type="declaration_set_type">
			<dox><bri>Used internally; scans given decl_seq and its using-directives using params.</bri></dox>
			<param type="ss_decl_seq" name="starting_seq" />
		</method>

		<field name="lookup_result" type="declaration_set_type" init="NULL" check="" get="protected" set="protected" alt="protected" />
		<visit-return-method name="lookup_by_decl" get-from="lookup_result" />
	</class>

	<class name="lu_lookup_in_type" base="ss_type_default_visitor" create="none">
		<singleton />
		<declare-abstract-methods exact="default_action" />

		<declare-abstract-methods exact="visit_ss_class" />
		<declare-abstract-methods exact="visit_ss_union" />

		<method name="main" type="declaration_set_type">
			<param type="ss_type" name="type" />
			<param type="lu_params" name="params" />
		</method>

		<method name="internal_simple_scan" type="declaration_set_type">
			<dox><bri>Used internally, just a wrapper around lu_lookup::simple_scan() that adds params to the argument list.</bri></dox>
			<param type="ss_decl_seq" name="seq" />
		</method>

		<field name="params" type="lu_params" init="NULL" check="" get="protected" set="protected" alt="protected">
			<dox><bri>Used internally by the visitor; holds the parameters of the lookup.</bri></dox>
		</field>

		<field name="lookup_result" type="declaration_set_type" init="NULL" check="" get="protected" set="protected" alt="protected" />
		<visit-return-method name="lookup_by_type" get-from="lookup_result" />
	</class>
</lsd>
