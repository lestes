<?xml version="1.0" encoding="UTF-8"?>
<!--
	The lestes compiler suite
	Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
	Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
	Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
	Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
	Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
	Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
	Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; version 2 of the License.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	See the full text of the GNU General Public License version 2, and
	the limitations in the file doc/LICENSE.

	By accepting the license the licensee waives any and all claims
	against the copyright holder(s) related in whole or in part to the
	work, its use, and/or the inability to use it.
-->
<lsd xmlns="http://lestes.jikos.cz/schemas/lsd" xmlns:h="http://www.w3.org/TR/REC-html40">

<comment>               Prelude for the whole header file       </comment>

    <dox file="both">
        <bri>Intercode structure for project Lestes</bri>
        <det>
            This file describes a set of classes and data types used for intercode layer -ss-.
            It is an output from a XSLT template which generates C++ code.
        </det>
    </dox>

	<file-name>ss_expr_funcall</file-name>
	<imports>
		<i>lestes/common.hh</i>
		<i>lestes/lang/cplus/sem/ss_expression.g.hh</i>
	</imports>
        <implementation-imports>
		<i>lestes/lang/cplus/sem/visitor.v.g.hh</i>
		<i>lestes/lang/cplus/sem/ss_declaration.g.hh</i>
		<i>lestes/lang/cplus/sem/ss_type.g.hh</i>
		<i>lestes/lang/cplus/sem/ss_misc.g.hh</i>
        </implementation-imports>
	<packages>
		<p>lestes</p>
		<p>lang</p>
		<p>cplus</p>
		<p>sem</p>
	</packages>
	<hh-header />
	
	<hh-footer />

	<cc-header />
	<cc-footer />


<comment>				Function calls	 			</comment>

<include href="ss_expression.lsd" />
	<xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="ss_expression.lsd">
		<xi:fallback/>
	</xi:include>
<include href="ss_misc.lsd" />
	<xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="ss_misc.lsd">
		<xi:fallback/>
	</xi:include>
<visitor-file href="visitor.lvd" />

<forward-class name="ss_function_declaration" />
<forward-class name="ss_expression" />


    <default-base type="ss_funcall_abstr" />

    <default-collection kind="list" />

    <class name="ss_funcall_abstr" abstract="yes" base="ss_expression">
	<dox>
	    <bri>Base class for function call expressions</bri>
	    <det></det>
	</dox>
	<collection name="args" type="ss_expression" kind="list" />
    </class>


    <class name="ss_pfuncall" base="ss_funcall_abstr">
	<dox>
	    <bri>Class for member pointer call</bri>
	    <det></det>
	</dox>
	<field name="fun_ptr" type="ss_expression" />
	<field name="object" type="ss_expression" />
    </class>


    <class name="ss_funcall" base="ss_funcall_abstr">
	<dox>
	    <bri>Class for ordinary function call</bri>
	    <det></det>
	</dox>
	<field name="function" type="ss_function_declaration" />
    </class>


    <class name="ss_ifuncall" base="ss_funcall_abstr">
	<dox>
	    <bri>Class for indirect ordinary global function call</bri>
	    <det></det>
	</dox>
	<field name="fun_ptr" type="ss_expression" />
    </class>


    <class name="ss_mfuncall" base="ss_funcall_abstr">
	<dox>
	    <bri>Class for method call</bri>
	    <det></det>
	</dox>
	<field name="function" type="ss_function_declaration" />
	<field name="object" type="ss_expression" />
<comment>	TODO ? pointer insufficies, temporaries causa</comment>
    </class>

    
    <class name="ss_vfuncall" base="ss_funcall_abstr">
	<dox>
	    <bri>Class for virtual method call</bri>
	    <det></det>
	</dox>
	<field name="function" type="ss_function_declaration" />
	<field name="object_ptr" type="ss_expression" >
	 <dox><bri>Object, where to find correct VMT table.</bri></dox>
	</field>
    </class>

</lsd>
