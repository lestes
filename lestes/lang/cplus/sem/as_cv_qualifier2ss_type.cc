/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief CV qualification.

  Definition of visitor adding CV qualifiers to ss_type.
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/sem/as_decl.g.hh>
#include <lestes/lang/cplus/sem/ss_type.g.hh>
#include <lestes/lang/cplus/sem/as_cv_qualifier2ss_type.g.hh>

package(lestes);
package(lang);
package(cplus);
package(sem);

/*!
  Adds the qualifier to the current type.
  \pre qualifier != NULL
  \param qualifier  The qualifier to add.
*/
void as_cv_qualifier2ss_type::process(ptr<as_cv_qualifier> qualifier)
{
	lassert(qualifier);
	qualifier->accept_as_cv_qualifier_visitor(this);
}

/*!
  Adds const qualifier to the current type.
  \pre qualifier != NULL
  \param qualifier  The const qualifier.
*/
void as_cv_qualifier2ss_type::visit_as_cv_qualifier_const(
	ptr<as_cv_qualifier_const> qualifier)
{
	lassert(qualifier);
	type = ss_const::instance(type);
}

/*!
  Adds volatile qualifier to the current type.
  \pre qualifier != NULL
  \param qualifier  The volatile qualifier.
*/
void as_cv_qualifier2ss_type::visit_as_cv_qualifier_volatile(
	ptr<as_cv_qualifier_volatile> qualifier)
{
	lassert(qualifier);
	type = ss_volatile::instance(type);
}

/*!
  Visits restrict qualifier.
  \pre qualifier != NULL
  \param qualifier  The restrict qualifier.
*/
void as_cv_qualifier2ss_type::visit_as_cv_qualifier_restrict(
	ptr<as_cv_qualifier_restrict> qualifier)
{
	lassert(qualifier);
	lassert2(false,"restrict is not C++");
}

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);
