/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief CV qualifiers remover.
		
  Visitor removing CV qualifiers from SS types.
*/

#include <lestes/common.hh>
#include <lestes/lang/cplus/sem/ss_type.g.hh>
#include <lestes/lang/cplus/sem/ss_type2cv_unqualified.g.hh>

package(lestes);
package(lang);
package(cplus);
package(sem);

/*!
  Performs the default operation, that is identity.
  \pre type != NULL
  \param type  The type to unqualify.
*/
void ss_type2cv_unqualified::default_action(ptr<ss_type> type)
{
	lassert(type);
	unqualified = type;
}

/*!
  Visits object of type ss_const.
  Removes the const qualifier from the type.
  \pre type != NULL
  \param type  The type to unqualify.
*/
void ss_type2cv_unqualified::visit_ss_const(ptr<ss_const> type)
{
	lassert(type);
	unqualified = type->what_get();
}

/*!
  Visits object of type ss_volatile.
  Removes the volatile qualifier from the type.
  \pre type != NULL
  \param type  The type to unqualify.
*/
void ss_type2cv_unqualified::visit_ss_volatile(ptr<ss_volatile> type)
{
	lassert(type);
	unqualified = type->what_get();
}

/*!
  Visits object of type ss_const_volatile.
  Removes the const and volatile qualifier from the type.
  \pre type != NULL
  \param type  The type to unqualify.
*/
void ss_type2cv_unqualified::visit_ss_const_volatile(ptr<ss_const_volatile> type)
{
	lassert(type);
	unqualified = type->what_get();
}

end_package(sem);
end_package(cplus);
end_package(lestes);
end_package(lang);

