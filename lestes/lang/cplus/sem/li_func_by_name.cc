/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/lang/cplus/sem/lu_filter.g.hh>
#include <lestes/lang/cplus/sem/li_func_by_name.g.hh>
#include <lestes/lang/cplus/sem/lu_lu.g.hh>
#include <lestes/lang/cplus/sem/ss_decl_name.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>
#include <lestes/lang/cplus/sem/ss_misc.g.hh>
#include <lestes/std/source_location.hh>
#include <lestes/std/set.hh>
#include <lestes/msg/logger.hh>
#include <lestes/msg/logger_util.hh>

/*! \file
  \brief Lookup for function decl in a given scope and parents
  \todo UDIR_, UDECL_ to be adjusted
  \author jikos
 */

package(lestes);
package(lang);
package(cplus);
package(sem);

/*!
 */
ptr < ::lestes::std::set< srp< ss_function_declaration > > > li_func_by_name::process(ptr < ss_decl_name > name, ptr < ss_decl_seq > scope)
{
	ptr < lu_name_filter > nf = lu_name_filter::create(name);
	ptr < lu_func_cont_filter > ff = lu_func_cont_filter::instance();
	ptr < lu_multi_filter > mf = lu_multi_filter::create();
	mf->add_filter(ff);
	mf->add_filter(nf);
	ptr < lu_params > params = lu_params::create(lu_params::UDIR_IGNORE, lu_params::UDECL_IGNORE, lu_params::SEARCH_PARENTS,
			ss_declaration_time::create(name->location_get()->order_get()), mf);
	ptr< ::lestes::std::set< srp< ss_declaration > > > res = lu_lookup::instance()->main(scope, params);
	ptr< ::lestes::std::set< srp< ss_function_declaration > > > res_f = ::lestes::std::set< srp< ss_function_declaration > >::create();
	::lestes::std::set< srp< ss_declaration > >::iterator it;
	
	for(it = res->begin(); it != res->end(); it++) {
		res_f->insert((*it).dncast<ss_function_declaration>());
	}
	return res_f;
}

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);

