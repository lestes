/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/lang/cplus/sem/as_decl.g.hh>
#include <lestes/lang/cplus/sem/as_access_specifier_to_ss_access_specifier.g.hh>

package(lestes);
package(lang);
package(cplus);
package(sem);

void as_access_specifier_to_ss_access_specifier::visit_as_access_specifier_public(ptr< as_access_specifier_public >)
{
	access_specifier_set(ss_access_specifier::ACCESS_PUBLIC);
	return;
}

void as_access_specifier_to_ss_access_specifier::visit_as_access_specifier_protected(ptr< as_access_specifier_protected >)
{
	access_specifier_set(ss_access_specifier::ACCESS_PROTECTED);
	return;
}

void as_access_specifier_to_ss_access_specifier::visit_as_access_specifier_private(ptr< as_access_specifier_private >)
{
	access_specifier_set(ss_access_specifier::ACCESS_PRIVATE);
	return;
}

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);

