/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Unit test.
  
  Unit test for class sa_declarator.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/std/file_info.hh>
#include <lestes/std/source_location.hh>
#include <lestes/lang/cplus/sem/sa_decl_spec.g.hh>
#include <lestes/lang/cplus/sem/sa_declarator.g.hh>
#include <lestes/lang/cplus/sem/as_decl.g.hh>
#include <lestes/lang/cplus/sem/as_other.g.hh>
#include <lestes/lang/cplus/sem/ss_type.g.hh>
#include <lestes/lang/cplus/sem/ss_type_builtin.g.hh>
#include <lestes/lang/cplus/sem/ss_statement.g.hh>
#include <lestes/lang/cplus/sem/ss_misc.g.hh>

package(lestes);
package(lang);
package(cplus);
package(sem);

using namespace ::std;

/*!
  \brief Tests sa_declarator class.

  Performs testing of sa_declarator class.
*/
void sa_declarator_test(void)
{
	ptr<file_info> fi = file_info::create("abc",NULL);
	ptr<source_location> loc = source_location::create(fi,1,1);

#if 0
	ptr<ss_sp> psp = ss_sp::create(
		loc,
		NULL, // a__ss_sp__psp,
		NULL, //  a__ss_sp__nsp,
		::lestes::std::list< srp< ss_se > >::create(),  //  a__ss_sp__pse,
		::lestes::std::list< srp< ss_se > >::create(),  //  a__ss_sp__nse,
		NULL, // ptr< ::lestes::backend::intercode::pi_sp >   a__ss_sp__pi_spe,
		0    //ulint    a__ss_sp__level
		);
		
	ptr<ss_sp> nsp = ss_sp::create(
		loc,
		NULL, // a__ss_sp__psp,
		NULL, //  a__ss_sp__nsp,
		::lestes::std::list< srp< ss_se > >::create(),  //  a__ss_sp__pse,
		::lestes::std::list< srp< ss_se > >::create(),  //  a__ss_sp__nse,
		NULL, // ptr< ::lestes::backend::intercode::pi_sp >   a__ss_sp__pi_spe,
		0    //ulint    a__ss_sp__level
		);
		
	ptr<ss_compound_stmt> containing_compound =
		ss_compound_stmt::create(
			loc, // ptr< ::lestes::std::object >      a__ss_base_with_location__location,
			::lestes::std::list< srp< ss_label > >::create(), //   a__ss_statement__labels,
			NULL, // a__ss_statement__parent,
			psp, // a__ss_statement__psp,
			nsp, // a__ss_statement__nsp,
			::lestes::std::list< srp< ss_sp > >::create(), // a__ss_statement__sequence_points,
			NULL,     // ptr< ss_decl_seq >,  a__ss_compound_stmt__decl,
			::lestes::std::list< srp< ss_statement > >::create() // statements
			);
	
	ptr<ss_decl_seq> containing_scope = ss_decl_seq::create(
			loc,
			::lestes::std::list< srp< ss_declaration > >::create(), // a__ss_decl_seq__declarations
			NULL, //ptr< ss_decl_seq > a__ss_decl_seq__parent
			containing_compound, //ptr< ss_compound_stmt > a__ss_decl_seq__compound_stmt
			::lestes::std::list< srp< ss_using_directive > >::create(), // using directives
			NULL //   ptr< ss_declaration >       a__ss_decl_seq__declared_by         
			);
#endif

	ptr<ss_compound_stmt> containing_compound = ss_compound_stmt::root_instance();
	ptr<ss_decl_seq> containing_scope = ss_decl_seq::root_instance();
	
	typedef sa_declarator_context::as_decl_spec_list_type ds_list_type;
	
	ptr<ds_list_type> dsl = ds_list_type::create();
	ptr<sa_decl_spec_context> sadsc =
		sa_decl_spec_context::create(declaration_context::CTX_NAMESPACE);
	ptr<sa_decl_spec> sads = sa_decl_spec::create(sadsc);
	dsl->push_back(as_int_simple_type_specifier::create(loc));
	sads->process(dsl);

	typedef ::lestes::std::list< srp<as_cv_qualifier> > as_cv_qualifier_list;
	ptr<as_cv_qualifier_list> ascvql = as_cv_qualifier_list::create();
	ascvql->push_back(as_cv_qualifier_const::create(loc));
	
	ptr<as_ptr_op_star> aspos = as_ptr_op_star::create(loc,ascvql);
	
	typedef ::lestes::std::list< srp<as_declarator_op> > as_declarator_op_list;
	ptr<as_declarator_op_list> asdol = as_declarator_op_list::create();
	asdol->push_back(aspos);
	
	ptr<as_name> asn = as_name::create(
			loc,
			as_name::create(loc, NULL,as_global_namespace_fake_id::create(loc),false),
			as_global_namespace_fake_id::create(loc),
			true);
	
	ptr<as_declarator> asd = as_declarator::create(
			loc,
			asn,
			asdol
			);

	ptr<sa_declarator_context> sadc =
		sa_declarator_context::create(
				declaration_context::CTX_NAMESPACE,
				containing_scope,
				sads);
	ptr<sa_declarator> sad = sa_declarator::create(sadc);
	sad->process(asd);

}

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);

/*!
  \brief Main function.

  Runs the unit test in different namespace.
*/
int main(void)
{
	::lestes::lang::cplus::sem::sa_declarator_test();
	return 0;
}
/* vim: set ft=lestes : */
