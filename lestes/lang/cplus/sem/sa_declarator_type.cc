/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Declarator type analyser.

  Declarator structural analysis and conversion to ss_type.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/sem/as_declarator_op2ss_type.g.hh>
#include <lestes/lang/cplus/sem/sa_declarator_type.g.hh>
#include <lestes/lang/cplus/sem/as_decl.g.hh>
#include <lestes/lang/cplus/sem/ss_type.g.hh>
#include <lestes/lang/cplus/sem/sa_loggers.hh>
#include <lestes/msg/logger.hh>
#include <lestes/msg/logger_util.hh>
#if 0
#include <lestes/std/source_location.hh>
#include <lestes/lang/cplus/sem/sa_declarator.g.hh>
#include <lestes/lang/cplus/sem/sa_decl_spec.g.hh>
#include <lestes/lang/cplus/sem/as_other.g.hh>
#include <lestes/lang/cplus/sem/ss_type_builtin.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>
#include <lestes/lang/cplus/sem/ss_misc.g.hh>
#include <lestes/lang/cplus/sem/ss_decl_name.g.hh>
#include <lestes/lang/cplus/sem/ss_expression.g.hh>
#include <lestes/lang/cplus/sem/ss_statement.g.hh>

#include <iostream>
#endif

package(lestes);
package(lang);
package(cplus);
package(sem);

// TODO pt remove
#if 0
/*!
  Returns as_declarator_op visitor.
  \return A visitor appropriate for the context.
*/
ptr<as_declarator_op2ss_type> sa_declarator_type_context::create_as_declarator_op2ss_type(void)
{
	return as_declarator_op2ss_type::create(decl_specs->type_get());
}
#endif

typedef list< srp< as_declarator_op > > as_declarator_op_list_type;

/*!
  Returns the type of the declarator.
  \pre initial != NULL
  \pre declarator != NULL
  \param start_type  The initial type.
  \param declarator  The the declarator to process.
  \return The type of the declarator.
*/
ptr<ss_type> sa_declarator_type::process(ptr<ss_type> start_type, ptr<as_declarator> declarator)
{
	sa_declarator_type_logger << "sa_declarator_type::process()\n" << msg::eolog;
	
	lassert(start_type);
	lassert(declarator);
	
	// create the appropriate visitor 
	ptr<as_declarator_op2ss_type> v = as_declarator_op2ss_type::create(start_type);

	ptr<as_declarator_op_list_type> lst = declarator->declarator_ops_get();
	
	sa_declarator_type_logger << "analysing declarator ops\n" << msg::eolog;
	
	// walk through all declarator ops in reverse direction
	for (as_declarator_op_list_type::reverse_iterator rit = lst->rbegin(), rend = lst->rend();
			rit != rend; ++rit) {
		// classify individual declarator ops
		v->process(*rit);
	}
	
	sa_declarator_type_logger << "sa_declarator_type::process() end\n" << msg::eolog;

	return v->type_get();
}

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);
/* vim: set ft=lestes : */
