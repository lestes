/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Unit test.
  
  Unit test for class sa_decl_spec.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/std/file_info.hh>
#include <lestes/std/source_location.hh>
#include <lestes/lang/cplus/sem/sa_decl_spec.g.hh>
#include <lestes/lang/cplus/sem/as_decl.g.hh>
#include <lestes/lang/cplus/sem/as_other.g.hh>
#include <lestes/lang/cplus/sem/ss_type.g.hh>
#include <lestes/lang/cplus/sem/ss_enums.g.hh>
#include <lestes/lang/cplus/sem/ss_type_builtin.g.hh>

package(lestes);
package(lang);
package(cplus);
package(sem);

using namespace ::std;

#define lassert_no_specifiers(x) \
	lassert(sads->explicit_flag_get() == false); \
	lassert(sads->inline_flag_get() == false); \
	lassert(sads->friend_flag_get() == false); \
	lassert(sads->virtual_flag_get() == false); \
	lassert(sads->storage_get() == ss_storage_class::ST_NONE);

/*!
  \brief Tests sa_decl_spec class.

  Performs testing of sa_decl_spec class.
*/
void sa_decl_spec_test(void)
{
	ptr<file_info> fi = file_info::create("abc",NULL);
	ptr<source_location> loc = source_location::create(fi,1,1);
	typedef sa_decl_spec::as_decl_spec_list_type ds_list_type;
	
	ptr<ds_list_type> dsl = ds_list_type::create();
	ptr<sa_decl_spec_context> ctx =
		sa_decl_spec_context::create(declaration_context::CTX_NAMESPACE);
	ptr<sa_decl_spec> sads = sa_decl_spec::create(ctx);
		
	dsl->push_back(as_int_simple_type_specifier::create(loc));
	sads->process(dsl);
	lassert(sads->type_get() == ss_type_sint::instance());
	lassert_no_specifiers(sads);
	dsl->clear();

	dsl->push_back(as_unsigned_simple_type_specifier::create(loc));
	sads->process(dsl);
	lassert(sads->type_get() == ss_type_uint::instance());
	lassert_no_specifiers(sads);
	dsl->clear();

	dsl->push_back(as_short_simple_type_specifier::create(loc));
	sads->process(dsl);
	lassert(sads->type_get() == ss_type_sshort::instance());
	lassert_no_specifiers(sads);
	dsl->clear();

	dsl->push_back(as_int_simple_type_specifier::create(loc));
	dsl->push_back(as_long_simple_type_specifier::create(loc));
	dsl->push_back(as_unsigned_simple_type_specifier::create(loc));
	sads->process(dsl);
	lassert(sads->type_get() == ss_type_ulong::instance());
	lassert_no_specifiers(sads);
	dsl->clear();
	
	dsl->push_back(as_char_simple_type_specifier::create(loc));
	dsl->push_back(as_unsigned_simple_type_specifier::create(loc));
	sads->process(dsl);
	lassert(sads->type_get() == ss_type_uchar::instance());
	lassert_no_specifiers(sads);
	dsl->clear();
	
	dsl->push_back(as_char_simple_type_specifier::create(loc));
	dsl->push_back(as_signed_simple_type_specifier::create(loc));
	sads->process(dsl);
	lassert(sads->type_get() == ss_type_schar::instance());
	lassert_no_specifiers(sads);
	dsl->clear();
	
	dsl->push_back(as_char_simple_type_specifier::create(loc));
	sads->process(dsl);
	lassert(sads->type_get() == ss_type_pchar::instance());
	lassert_no_specifiers(sads);
	dsl->clear();

	dsl->push_back(as_int_simple_type_specifier::create(loc));
	dsl->push_back(as_cv_qualifier_const::create(loc));
	sads->process(dsl);
	lassert(sads->type_get() == ss_const::instance(ss_type_sint::instance()));
	lassert_no_specifiers(sads);
	dsl->clear();

	lassert(ss_const::instance(ss_volatile::instance(ss_type_sint::instance())) ==
		ss_const::instance(ss_volatile::instance(ss_type_sint::instance())));
		
	lassert(ss_const::instance(ss_volatile::instance(ss_type_sint::instance())) ==
		ss_volatile::instance(ss_const::instance(ss_type_sint::instance())));
		
	dsl->push_back(as_int_simple_type_specifier::create(loc));
	dsl->push_back(as_cv_qualifier_const::create(loc));
	dsl->push_back(as_cv_qualifier_volatile::create(loc));
	sads->process(dsl);
	lassert(sads->type_get() ==
		ss_const::instance(ss_volatile::instance(ss_type_sint::instance())));
	lassert_no_specifiers(sads);
	dsl->clear();

}

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);

/*!
  \brief Main function.

  Runs the unit test in different namespace.
*/
int main(void)
{
	::lestes::lang::cplus::sem::sa_decl_spec_test();
	return 0;
}
/* vim: set ft=lestes : */
