/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Void specifier recognition.

  Visitor recognizing void simple type declaration specifier.
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/sem/as_declaration_specifier2is_void.g.hh>
#include <lestes/lang/cplus/sem/as_decl.g.hh>
#include <lestes/lang/cplus/sem/ss_type.g.hh>
#include <lestes/lang/cplus/sem/sa_loggers.hh>
#include <lestes/msg/logger.hh>
#include <lestes/msg/logger_util.hh>

package(lestes);
package(lang);
package(cplus);
package(sem);

/*!
  Visits specifier of type as_void_simple_type_specifier.
  Sets void flag to true.
  \pre specifier != NULL
  \param specifier  The void specifier.
*/
void as_declaration_specifier2is_void::visit_as_void_simple_type_specifier(ptr<as_void_simple_type_specifier> specifier)
{
	lassert(specifier);
	void_flag = true;
}

/*!
  Handles specifiers other than void.
  Sets the void flag to false.

  \pre specifier != NULL
  \param specifier  The declaration specifier to visit.
*/ 
void as_declaration_specifier2is_void::default_action(ptr<as_declaration_specifier> specifier)
{
	lassert(specifier);
	void_flag = false;
}

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);
