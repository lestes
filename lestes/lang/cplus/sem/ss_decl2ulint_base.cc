/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*!
	\file
	\brief Visitors returning ulint value for ss_declaration.
	\author jaz
*/

#include <lestes/lang/cplus/sem/ss_decl2ulint_base.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>

package(lestes);
package(lang);
package(cplus);
package(sem);

ptr< ss_decl2id > ss_decl2id::instance(){
	if ( !singleton_instance ) {
		singleton_instance = ss_decl2id::create();
	}
	return singleton_instance;
}

ulint ss_decl2id::visit_ss_namespace_definition(ptr< ::lestes::lang::cplus::sem::ss_namespace_definition >) {
	return NAMESPACE;
}

ulint ss_decl2id::visit_ss_object_declaration(ptr< ::lestes::lang::cplus::sem::ss_object_declaration >) {
	return OBJECT;
}

ulint ss_decl2id::visit_ss_bitfield_declaration(ptr< ::lestes::lang::cplus::sem::ss_bitfield_declaration >) {
	return BITFIELD;
}

ulint ss_decl2id::visit_ss_parameter_declaration(ptr< ::lestes::lang::cplus::sem::ss_parameter_declaration >) {
	return PARAMETER;
}

ulint ss_decl2id::visit_ss_structure_declaration(ptr< ::lestes::lang::cplus::sem::ss_structure_declaration >) {
	return TYPE;
}

ulint ss_decl2id::visit_ss_enum_definition(ptr< ::lestes::lang::cplus::sem::ss_enum_definition >) {
	return ENUM;
}

ulint ss_decl2id::visit_ss_typedef_definition(ptr< ::lestes::lang::cplus::sem::ss_typedef_definition >) {
	return TYPEDEF;
}

ulint ss_decl2id::visit_ss_function_declaration(ptr< ::lestes::lang::cplus::sem::ss_function_declaration >) {
	return FUNCTION;
}

ulint ss_decl2id::visit_ss_enumerator_declaration(ptr< ::lestes::lang::cplus::sem::ss_enumerator_declaration >) {
	return ENUMERATOR;
}


ulint ss_decl2id::visit_ss_using_declaration(ptr< ::lestes::lang::cplus::sem::ss_using_declaration >) {
	return USING;
}

ulint ss_decl2id::visit_ss_builtin_operator_declaration(ptr< ::lestes::lang::cplus::sem::ss_builtin_operator_declaration >) {
	return BUILTIN_OPERATOR;
}


ulint ss_decl2id::visit_ss_compound_stmt_declaration(ptr< ::lestes::lang::cplus::sem::ss_compound_stmt_declaration >){
	return COMPOUND_STATEMENT;
}


ulint ss_decl2id::visit_ss_method_declaration(ptr< ::lestes::lang::cplus::sem::ss_method_declaration >){
	return METHOD;
}

ulint ss_decl2id::visit_ss_fake_declaration(ptr< ::lestes::lang::cplus::sem::ss_fake_declaration >){
	lassert(false);
	return 0;
}

ulint ss_decl2id::visit_ss_injected_class_declaration(ptr< ::lestes::lang::cplus::sem::ss_injected_class_declaration >){
	lassert(false);
	return 0;
}




end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);

