/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/lang/cplus/lex/cpp_token.hh>
#include <lestes/lang/cplus/sem/as_access_specifier_to_ss_access_specifier.g.hh>
#include <lestes/lang/cplus/sem/as_base_specifier_to_ss_base_specifier.g.hh>
#include <lestes/lang/cplus/sem/as_decl.g.hh>
#include <lestes/lang/cplus/sem/as_id_to_declaration_set.g.hh>
#include <lestes/lang/cplus/sem/sa_loggers.hh>
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>
#include <lestes/lang/cplus/sem/ss_misc.g.hh>
#include <lestes/lang/cplus/sem/ss_type.g.hh>
#include <lestes/msg/logger.hh>
#include <lestes/msg/logger_util.hh>
//#include <lestes/lang/cplus/syn/token.hh>


package(lestes);
package(lang);
package(cplus);
package(sem);

/*!
  \todo TMA after lookup: look up the class
 */
ptr < ss_base_specifier > as_base_specifier_to_ss_base_specifier::process(ptr < as_base_specifier > bs)
{
	bool is_virtual_base = bs->virtual_base_get();
	ss_access_specifier::type access_specifier;

	sa_class_base_logger << "base\n" << msg::eolog;

	if (bs->access_specifier_get()) {
		sa_class_base_logger << "access specifier given\n" << msg::eolog;
		access_specifier = as_access_specifier_to_ss_access_specifier::create()->process(bs->access_specifier_get());
	} else {
		sa_class_base_logger << "access specifier default\n" << msg::eolog;
		access_specifier = default_access_specifier_get();
	}

	sa_class_base_logger << "base type FIXME\n" << msg::eolog;
	/* FIXME: lookup here */
	ptr < as_name > name = bs->class_name_get();
	lassert(name);
	ptr < as_identifier > id = name->identifier_get().dncast<as_identifier>();
	lassert(id);

	ptr < set < srp < ss_declaration > > > decls = as_id_to_declaration_set::instance()->process(id);
	if (decls->empty()) {
		lassert2(false, "FIXME report");
		//report << identifier_shall_have_a_visible_declaration << name->location_get();
	}
	if (decls->size() != 1) {
		lassert2(false, "FIXME report");
		//report << identifier_shall_name_a_unique_class << name->location_get();
	}
	ptr < ss_structure_declaration > decl = decls->begin()->dncast<ss_structure_declaration>();
	ptr < ss_class > a_class = decl->type_get().dncast<ss_class>();
	if (! a_class->completion_time_get()->is_before(ss_declaration_time::infinity())) {
		lassert2(false, "FIXME report");
		//report << incomplete_class_shall_not_be_base << name->location_get();
	}

	sa_class_base_logger << "ss_base_specifier::create\n" << msg::eolog;
	return ss_base_specifier::create(bs->location_get(), access_specifier, a_class, is_virtual_base);
}

ptr< ::lestes::std::list < srp < ss_base_specifier > > > as_base_specifier_to_ss_base_specifier::process_list(ptr< ::lestes::std::list < srp < as_base_specifier > > > l)
{
	::lestes::std::list < srp < as_base_specifier > >::iterator it = l->begin();
	ptr< ::lestes::std::list < srp < ss_base_specifier > > > r = ::lestes::std::list < srp < ss_base_specifier > >::create();

	for (; it != l->end(); ++it) {
		r->push_back(process(*it));
	}
	return r;
}

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);

