/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Empty parameter declaration test.

  Checking whether a parameter declaration is a special case equivalent to empty list.
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/sem/sa_param_declaration_empty.g.hh>
#include <lestes/lang/cplus/sem/as_declaration_specifier2is_void.g.hh>
#include <lestes/lang/cplus/sem/as_decl.g.hh>
#include <lestes/lang/cplus/sem/sa_loggers.hh>
#include <lestes/msg/logger.hh>
#include <lestes/msg/logger_util.hh>

package(lestes);
package(lang);
package(cplus);
package(sem);

/*!
  Tests whether a parameter declaration contains only void declaration specifier
  and nothing else, no declarator specifiers and no name, so that it can be considered
  as a (void) parameter, designating empty paramter list.
  Does not report any error, only checks the conditions. Errors are reported in
  the parameter declaration itself.

  \pre decl != NULL
  \param decl  The parameter declaration to test.
  \return true  If the declaration designates empty parameter list.
*/
bool sa_param_declaration_empty::process(ptr<as_param_declaration> decl)
{
	typedef list< srp<as_declaration_specifier> > as_declaration_specifier_list;

	sa_param_declaration_empty_logger << "sa_param_declaration::process()\n" << msg::eolog;

	lassert(decl);

	bool correct = false;
	
	do {
		sa_param_declaration_empty_logger << "checking declaration specifiers\n" << msg::eolog;
		
		// analyse the declaration specifiers
		ptr<as_declaration_specifier_list> adsl = decl->declaration_specifiers_get();
		// TODO pt BUGBUG must test the actual type, perhaps typedefed!!!!

		// must be only specifier
		if (adsl->size() != 1) break;

		sa_param_declaration_empty_logger << "has single specifier\n" << msg::eolog;
		
		// must be void
		if (!as_declaration_specifier2is_void::create()->process(adsl->front())) break;
		
		sa_param_declaration_empty_logger << "the specifier is void\n" << msg::eolog;
		
		sa_param_declaration_empty_logger << "checking the declarator\n" << msg::eolog;

		// analyse declarator
		ptr<as_declarator> asd = decl->declarator_get();
		// must have no ops
		if (asd->declarator_ops_get()->size() != 0) break;
		
		sa_param_declaration_empty_logger << "has no declarator ops\n" << msg::eolog;
		
		// must have no name
		if (asd->name_get()) break;

		sa_param_declaration_empty_logger << "has no name\n" << msg::eolog;
		
		// all checks passed
		correct = true;

		sa_param_declaration_empty_logger << "all checks passed\n" << msg::eolog;
	} while (false);

	sa_param_declaration_empty_logger << "sa_param_declaration::process() end\n" << msg::eolog;

	return correct;
}

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);

