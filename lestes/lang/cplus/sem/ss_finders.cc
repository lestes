/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include "ss_finders.g.hh"

package(lestes);
package(lang);
package(cplus);
package(sem);


ptr <ss_constructor_finder> ss_constructor_finder::instance(){
	if ( !singleton_instance_get() ) {
		singleton_instance_set(ss_constructor_find::create());
	}
	return singleton_instance_get();
}


ptr <ss_destructor_finder> ss_destructor_finder::instance(){
	if ( !singleton_instance_get() ) {
		singleton_instance_set(ss_destructor_find::create());
	}
	return singleton_instance_get();

}
 

ptr< ss_function_declaration > ss_constructor_find:: find_copy_constructor ( ptr < ss_struct_base > ){
	lassert2(false,"Class support is not implemented in ss2pi now.");
//FIXME
}

ptr< ss_function_declaration > ss_constructor_find::find_default_constructor ( ptr < ss_struct_base > ){
	lassert2(false,"Class support is not implemented in ss2pi now.");
//FIXME
}

/*!
  Find all constructors for a given class
*/
set < ptr< ss_function_declaration > >   ss_constructor_find::find_constructors ( ptr < ss_struct_base > ){
	lassert2(false,"Class support is not implemented in ss2pi now.");
//FIXME
}

/*!
  Find conversion constructors, i.e. constructors having single parameter of another type, see [12.3.1]
*/
set < ptr< ss_function_declaration > >   ss_constructor_find::find_converting_constructors ( ptr < ss_struct_base > ){
	lassert2(false,"Class support is not implemented in ss2pi now.");
//FIXME
}

ptr < ss_function_declaration >  ss_destructor_find::find_default_destructor ( ptr < ss_struct_base > ){
	lassert2(false,"Class support is not implemented in ss2pi now.");
//FIXME
}

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);
