/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Semantic structures definition.
  
  Definition of methods of semantic structures (ss_type classes).
*/
#include <lestes/lang/cplus/sem/ss_type.hh>
#include <lestes/lang/cplus/sem/ss_misc.g.hh>
#include <lestes/lang/cplus/sem/ss_type.g.hh>
#include <lestes/lang/cplus/sem/ss_type_builtin.g.hh>
#include <lestes/lang/cplus/sem/ss_type2info.g.hh>
#include <lestes/std/lassert.hh>

package(lestes);
package(lang);
package(cplus);
package(sem);

/*!
  Returns void flag for the general type.
  \return The void flag, that is, false.
*/
bool ss_type::is_void()
{
	return false;
}


/*!
  Returns void flag for this type.
  \return The void flag, that is, true.
*/
bool ss_void::is_void()
{
	return true;
}

/*!
  Returns void flag for this type.
  \return The void flag of the underlying type.
*/
bool ss_volatile::is_void()
{
	return what->is_void();
}

/*!
  Returns void flag for this type.
  \return The void flag of the underlying type.
*/
bool ss_const::is_void()
{
	return what->is_void();
}

/*!
  Returns void flag for this type.
  \return The void flag of the underlying type.
*/
bool ss_const_volatile::is_void()
{
	return what->is_void();
}

/*!
  Returns volatile flag for general type.
  \return The volatile flag, that is, false.
*/
bool ss_type::is_volatile()
{
	return false;
}

/*!
  Returns volatile flag for this type.
  \return The volatile flag, that is, true.
*/
bool ss_volatile::is_volatile()
{
	return true;
}

/*!
  Returns volatile flag for this type.
  \return The volatile flag, that is, true.
*/
bool ss_const_volatile::is_volatile()
{
	return true;
}

/*!
  Returns volatile flag for this type.
  \return The volatile flag, which depends on referenced type.
*/
bool ss_referential::is_volatile()
{
	return what->is_volatile();
}

/*!
  Returns void flag for this type.
  \return The void flag, which depends on referenced type.
*/
bool ss_referential::is_void()
{
	return what->is_void();
}

/*!
  Returns constized version of a given type.
  The constization is an idempotent operation.
  Constization and volatilization is commutative.
  \pre x != NULL
  \param x  The type to make const.
  \return  The constized type.
*/
ptr <ss_type> ss_const::instance(ptr<ss_type> x)
{
	the_instances->dump_barrier_set(true); // XXX: needed just once :-I

	lassert(x);
	srp<ss_type> &result = (*the_instances)[x];

	if (result)
		return result;

	ptr<ss_type2info> ssti = ss_type2info::create();

	switch (ssti->process(x)) {
		case ss_type2info::SS_CONST:
		case ss_type2info::SS_CONST_VOLATILE:
			return result = x;
		case ss_type2info::SS_VOLATILE:
			// this is the only place to create const_volatile
			return result = ss_const_volatile::create(ssti->ss_volatile_object_get()->what_get());
		default:
			break;
	}
	
	return result = ss_const::create(x);
}

/*!
  Returns volatilized version of a given type.
  The volatilization is an idempotent operation.
  Constization and volatilization is commutative.
  \pre x != NULL
  \param x  The type to make volatile.
  \return  The volatilized type.
*/
ptr<ss_type> ss_volatile::instance(ptr<ss_type> x)
{
	the_instances->dump_barrier_set(true); // XXX: needed just once :-I

	lassert(x);
	srp <ss_type> &result = (*the_instances)[x];

	if (result) 
		return result;

	ptr<ss_type2info> ssti = ss_type2info::create();

	switch (ssti->process(x)) {
	case ss_type2info::SS_VOLATILE:
	case ss_type2info::SS_CONST_VOLATILE:
		return result = x;
	case ss_type2info::SS_CONST:
		// create ss_const_volatile only via instance of ss_const
		return result = ss_const::instance(ss_volatile::instance(ssti->ss_const_object_get()->what_get()));
	default:
		break;
	}

	return result = ss_volatile::create(x);
}

/*!
  Returns type representing pointer to a given type.
  \pre x != NULL
  \param x  The type to make pointer to.
  \return  The pointer type to the given type.  
 */
ptr<ss_type> ss_pointer::instance(ptr<ss_type> x)
{
	the_instances->dump_barrier_set(true); // XXX: needed just once :-I

	lassert(x);
	srp < ss_type > &result = (*the_instances)[x];

	if (result) 
		return result;

	return result = ss_pointer::create(x);
}

/*!
  Returns type representing reference to a given type.
  \pre x != NULL
  \param x  The type to make reference to.
  \return  The  to the given type.  
*/
ptr<ss_type> ss_reference::instance(ptr<ss_type > x)
{
	the_instances->dump_barrier_set(true); // XXX: needed just once :-I

	lassert(x);

	ptr<ss_type2info> ssti = ss_type2info::create();
	x->accept_ss_type_visitor(ssti);

	switch (ssti->info_get()) {
	case ss_type2info::SS_PSEUDOREFERENCE:
		lassert2(false, "Reference to pseudoreference shall not be made.");
		break;
	case ss_type2info::SS_REFERENCE:
		lassert2( 0, "FIXME: (ERRREPORT): Creating reference to a reference type" );
		return NULL;
	default:
		break;
	}

	srp<ss_type> &result = (*the_instances)[x];

	if (result)
		return result;

	return result = ss_reference::create(x);
}

/*!
  Returns type representing pseudoreference to a given type.
  \pre x != NULL
  \param x  The type to make pseudoreference to.
  \return  The  to the given type.  
*/
ptr<ss_type> ss_pseudoreference::instance(ptr < ss_type > x)
{
	the_instances->dump_barrier_set(true); // XXX: needed just once :-I

	lassert(x);

	ptr<ss_type2info> ssti = ss_type2info::create();

	switch (ssti->process(x)) {
	case ss_type2info::SS_REFERENCE:
	case ss_type2info::SS_PSEUDOREFERENCE:
		return x;
	default:
		break;
	}

	srp<ss_type> &result = (*the_instances)[x];

	if (result)
		return result;

	return result = ss_pseudoreference::create(x);
}

/*!
 * Check is this class is ancestor of its parameter
 */
bool ss_class::is_ancestor(ptr< ss_class > descendant)
{
	typedef ::lestes::std::list< srp< ss_class > > classlist;
	ptr< classlist > lst = classlist::create();
	::lestes::std::list< srp< ss_class > >::iterator it_cl;
	::lestes::std::list< srp< ss_base_specifier > >::iterator it;

	/* push 1st level of bases into list */
	for(it = descendant->bases_get()->begin(); it!= descendant->bases_get()->end(); it++) {
		lst->push_back((*it)->base_class_get());
		/* have we found ourselves? */
		if ((*it)->base_class_get() == this) 
			return true;
	}
		
	/* for every class in the list, push into list all bases */
	for(it_cl = lst->begin(); it_cl != lst->end(); it_cl++){
		for(it = (*it_cl)->bases_get()->begin(); it != (*it_cl)->bases_get()->end(); it_cl++) {
			lst->push_back((*it)->base_class_get());
			/* have we found ourselves? */
			if ((*it)->base_class_get() == this)
				return true;
		}
	}
	return false;
	
}

/*!
  Returns type representing array of a given type.
  \pre type != NULL
  \param size  The size of the array.
  \param type  The base type for the array.
  \return  The array of the given type.  
*/
ptr<ss_type> ss_array::instance(t_size size, ptr<ss_type> type)
{
	the_instances->dump_barrier_set(true); // XXX: needed just once :-I

	lassert(type);

	ptr < pair < t_size, srp < ss_type > > > twin = pair < t_size, srp < ss_type > >::create(size,type);
	srp < ss_type > & result = (*the_instances)[twin];
	
	if (result)
		return result;
	
	return result = ss_array::create(size, type);
}

/*!
  Returns type representing member pointer to a given type in a given base.
  \pre base_type != NULL
  \pre element_type != NULL
  \param base_type  The base type for the member pointer.
  \param element_type  The type of the element pointed to.
  \return  The type of member pointer of the given properties.  
*/
ptr<ss_type> ss_member_pointer::instance(ptr<ss_type> base_type, ptr<ss_type> element_type)
{
	the_instances->dump_barrier_set(true); // XXX: needed just once :-I

	lassert(base_type);
	lassert(element_type);

	ptr < pair < srp < ss_type >, srp < ss_type > > > twin = 
		pair < srp < ss_type >, srp < ss_type > >::create(base_type, element_type);
	srp < ss_type > & result = (*the_instances)[twin];

	if (result)
		return result;
	
	return result = ss_member_pointer::create(base_type, element_type);
}

/*!
  Compares to other ss_function in the defined ordering. Used for container comparator.
  The ordering considers (in this order) the first of the following to compare less:
  the ellipsis flag, the return value, and the parameter sequence.
  For the purpose of comparison, the value of the pointer determines the ordering for
  the objects representing data types, as these are all singletonized SS types and thus
  impose consistent ordering. The parameters are compared lexicographically in the usual manner,
  stopping at the first inequality. If there is none, the shorter sequence compares less.
  \pre other != NULL
  \param other  The other instance to compare to.
  \return true  If this instance is less than the other in the defined ordering.
*/
bool ss_function::less_than(ptr<ss_function> other)
{
	lassert(other);
	
	if (ellipsis_get() != other->ellipsis_get())
		return ellipsis_get() < other->ellipsis_get();

	if (returns_get() != other->returns_get())
		return returns_get() < other->returns_get();

	typedef ::lestes::std::list< srp<ss_type> > ss_type_list;
	ptr<ss_type_list> p = params_get();
	ptr<ss_type_list> q = other->params_get();

	ss_type_list::iterator pi = p->begin(), qi = q->begin(), pe = p->end(), qe = q->end();
	for ( ; pi != pe && qi != qe; ++pi, ++qi) {
		if (*pi != *qi)
			return *pi < *qi;
	}

	// less when the other parameter list is longer
	return pi == pe && qi != qe;
}

/*!
  Compares signatures ss_function in the defined ordering.
  Both functions have the same signature, if both have or have no ellipsis,
  if the parameter type sequences have the same length and
  the corresponding parameter types are the same.
  \pre other != NULL
  \param other  The other instance to compare to.
  \return true  If this instance has the same signature.
*/
bool ss_function::equal_signature(ptr<ss_function> other)
{
	lassert(other);

	if (ellipsis_get() != other->ellipsis_get())
		return false;

	typedef ::lestes::std::list< srp<ss_type> > ss_type_list;
	ptr<ss_type_list> p = params_get();
	ptr<ss_type_list> q = other->params_get();

	if (p->size() != q->size()) return false;

	for (ss_type_list::iterator pi = p->begin(), qi = q->begin(), pe = p->end(), qe = q->end();
		pi != pe && qi != qe; ++pi, ++qi) {
		if (*pi != *qi) return false;
	}

	return true;
}


/*
  Returns type representing function returning given type with given arguments.
  \pre returns != NULL
  \pre params != NULL
  \param return_type  The type of the return value.
  \param param_types  The types of the parameters.
  \param ellipsis  The ellipsis flag.
  \return  The type of function with the given properties.
*/
ptr<ss_type> ss_function::instance(ptr<ss_type> return_type, ptr< list< srp<ss_type> > > param_types, bool ellipsis)
{
	instances->dump_barrier_set(true); // XXX: needed just once :-I

	lassert(return_type);
	lassert(param_types);

	ptr<ss_function> fun = ss_function::create(return_type,param_types,ellipsis);

	::std::pair< set< srp<ss_function> >::iterator, bool> result = instances->insert(fun);

	// return the original value, duplicate is not inserted
	return *(result.first);
}

/*!
  Compares two ss_function object with less_than method.
  \pre left != NULL
  \pre right != NULL
  \param left  The left argument for the comparison.
  \param right  The right argument for the comparison.
  \return  The result of call of left->less_than(right).
*/
bool ss_function_comparator::operator()(const ptr<ss_function> &left, const ptr<ss_function> &right) const
{
	lassert(left && right);
	return left->less_than(right);
}

/*!
  Creates the comparator.
*/
ss_function_comparator::ss_function_comparator(void)
{
}

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);

