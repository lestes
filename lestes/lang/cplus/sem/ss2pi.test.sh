#!/bin/sh
#
# The lestes compiler suite
# Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
# Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
# Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
# Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
# Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
# Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
# Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# See the full text of the GNU General Public License version 2, and
# the limitations in the file doc/LICENSE.
#
# By accepting the license the licensee waives any and all claims
# against the copyright holder(s) related in whole or in part to the
# work, its use, and/or the inability to use it.
#
if [ -e ss2pi.test.llc ]; then
	./ss2pi.test -h -l ss2pi.test.llc -f ss2pi.test.xml flat < ss2pi.test_cc.cc
	./ss2pi.test -h < ss2pi.test_cc.cc 2>/dev/null|grep -v succeeded > ss2pi.test.out.asm
else
	./ss2pi.test -h -f ss2pi.test.xml flat < ss2pi.test_cc.cc |grep -v succeeded >ss2pi.test.out.asm
fi

nasm -f elf ss2pi.test.out.asm 
g++ -o ss2pi.test.out ss2pi.test.out.o  ../../../../lib/lio.o
./ss2pi.test.out
echo $?
