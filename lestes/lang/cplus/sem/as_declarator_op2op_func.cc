/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Function declarator op recognition.

  Visitor recognizing function declarator operator.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/sem/as_declarator_op2op_func.g.hh>
#include <lestes/lang/cplus/sem/as_decl.g.hh>

package(lestes);
package(lang);
package(cplus);
package(sem);

/*!
  Visits the function declarator operator.
  Sets the function field to the parameter.
  \pre dop != NULL
  \dop  The declarator operator to visit.
*/
void as_declarator_op2op_func::visit_as_declarator_op_func(ptr<as_declarator_op_func> dop)
{
	lassert(dop);
	function = dop;
}

/*!
  Visits declarator ops other than function.
  Resets the function field.

  \pre dop != NULL
  \param dop  The declarator operator to visit.
*/
void as_declarator_op2op_func::default_action(ptr<as_declarator_op> dop)
{
	lassert(dop);
	function = NULL;
}

end_package(sem);
end_package(cplus);
end_package(lang);
end_package(lestes);

/* vim: set ft=lestes : */
