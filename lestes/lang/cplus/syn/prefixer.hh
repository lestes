/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__syn__prefixer_hh__included
#define lestes__lang__cplus__syn__prefixer_hh__included

#include <lestes/common.hh>
#include <lestes/lang/cplus/syn/token.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);
class preprocessor;
end_package(lex);
package(syn);

class prefixer {
public:
	typedef ::lestes::lang::cplus::lex::preprocessor preprocessor;

	//! handles user hints (prefixes): they are stored in the user_hint field of the returned token
	static ptr<bison_token> yylex();

	enum mode_type {
		PREFIX_OFF,
		PREFIX_ON
	};

	static void init( mode_type, ptr<preprocessor> );
	static mode_type mode_get();
protected:
	//! used internally to actually call the lex scanner
	static ptr<bison_token> internal_yylex();
	//! given ucn_string, returns corresponding hint; when the string is not a valid prefix, HINT_NONE is returned
	static bison_token::hint_type prefix2hint( const ucn_string & );
private:
	//! Do no allow instantiating this class.
	prefixer();
	//! Hide copy-constructor.
	prefixer( const prefixer & );
	static mode_type mode;
	static ptr<preprocessor> pp;
};

end_package(syn);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif	// lestes__lang__cplus__syn__prefixer_hh__included
