/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/lang/cplus/sem/as_decl.g.hh>
#include <lestes/lang/cplus/sem/lu_filter.g.hh>
#include <lestes/lang/cplus/sem/lu_lu.g.hh>
#include <lestes/lang/cplus/sem/sa_context.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration_visitor.v.g.hh>
#include <lestes/lang/cplus/sem/ss_decl_name.g.hh>
#include <lestes/lang/cplus/sem/ss_misc.g.hh>
#include <lestes/lang/cplus/sem/ss_type.g.hh>
#include <lestes/lang/cplus/syn/errors.m.hh>
#include <lestes/lang/cplus/syn/hinter_actions.g.hh>
#include <lestes/lang/cplus/syn/hinter.g.hh>
#include <lestes/lang/cplus/syn/hinter.hh>
#include <lestes/lang/cplus/syn/manager.hh>
#include <lestes/lang/cplus/syn/syn.hh>
#include <lestes/lang/cplus/syn/token.hh>
#include <lestes/msg/logger.hh>
#include <lestes/std/list.hh>

package(lestes);
package(lang);
package(cplus);
package(syn);

declare_logger( hl );
initialize_logger( hl, "hinter", syn_logger );

hinter::mode_type hinter::mode = HINTER_NORMAL;
::std::map<bison_token::hint_type,bison_token::yytokentype> hinter::id_hint2tok_type_map;

using ::lestes::lang::cplus::sem::declaration_set_type;
using ::lestes::lang::cplus::sem::lu_elaborated_filter;
using ::lestes::lang::cplus::sem::lu_filter;
using ::lestes::lang::cplus::sem::lu_func_cont_filter;
using ::lestes::lang::cplus::sem::lu_lookup;
using ::lestes::lang::cplus::sem::lu_multi_filter;
using ::lestes::lang::cplus::sem::lu_name_filter;
using ::lestes::lang::cplus::sem::lu_namespace_filter;
using ::lestes::lang::cplus::sem::lu_params;
using ::lestes::lang::cplus::sem::lu_qual_filter;
using ::lestes::lang::cplus::sem::lu_visible_filter;
using ::lestes::lang::cplus::sem::sa_context_manager;
using ::lestes::lang::cplus::sem::ss_decl_name;
using ::lestes::lang::cplus::sem::ss_decl_seq;
using ::lestes::lang::cplus::sem::ss_declaration;
using ::lestes::lang::cplus::sem::ss_declaration_time;
using ::lestes::lang::cplus::sem::ss_dummy_name;
using ::lestes::lang::cplus::sem::ss_ordinary_name;

class one_hinter : public object {
public:
	class state_type : public object {
	private:
//F//		srp<ss_decl_seq> forced_scope;
		srp<ss_decl_seq> qual_scope;
		srp<elab_spec_key> elab_spec;
		bool namespace_mode;
	protected:
		state_type()
//F//			: forced_scope(NULL),
			: qual_scope(NULL), elab_spec(NULL),
			namespace_mode(false)
		{}
		//! copy-constructor
		state_type( const ptr<state_type> & other )
//F//			: forced_scope( other->forced_scope ),
			: qual_scope( other->qual_scope ),
			elab_spec( other->elab_spec ),
			namespace_mode( other->namespace_mode )
		{}
		void gc_mark()
		{
//F//			forced_scope.gc_mark();
			qual_scope.gc_mark();
			elab_spec.gc_mark();
			object::gc_mark();
		}
	public:
		static ptr<state_type> create()
		{
			return new state_type();
		}
		//! copy-constructor-like factory method
		static ptr<state_type> create( const ptr<state_type> & other )
		{
			return new state_type( other );
		}

//F//		void forced_scope_set( const ptr<ss_decl_seq> & s )
//F//		{
//F//			forced_scope = s;
//F//		}
		void qual_scope_set( const ptr<ss_decl_seq> & s )
		{
			qual_scope = s;
		}
		void elab_spec_set( const ptr<elab_spec_key> & a_elab_spec )
		{
			elab_spec = a_elab_spec;
		}
		void namespace_mode_set( bool b )
		{
			namespace_mode = b;
		}
		
//F//		ptr<ss_decl_seq> forced_scope_get() const
//F//		{
//F//			return checked(forced_scope);
//F//		}
		ptr<ss_decl_seq> qual_scope_get() const
		{
			return qual_scope;
		}
		ptr<elab_spec_key> elab_spec_get() const
		{
			return elab_spec;
		}
		bool namespace_mode_get() const
		{
			return namespace_mode;
		}
	};
private:
	srp< list< srp<state_type> > > state_stack;
protected:
	one_hinter() : state_stack( list< srp<state_type> >::create() )
	{
		state_stack->push_back( state_type::create() );
	}
	void gc_mark()
	{
		state_stack.gc_mark();
		object::gc_mark();
	}
public:
	static ptr<one_hinter> create()
	{
		return new one_hinter();
	}

	//! Copies current state and pushed in onto the stack (which makes it current).
	void push_state()
	{
		state_stack->push_back( state_type::create( state_stack->back() ) );
	}
	void pop_state()
	{
		lassert( !state_stack->empty() );
		state_stack->pop_back();
		lassert( !state_stack->empty() );
	}

//F//	void forced_scope_set( const ptr<ss_decl_seq> & s )
//F//	{
//F//		lassert( !state_stack->empty() );
//F//		state_stack->back()->forced_scope_set( s );
//F//	}
	void qual_scope_set( const ptr<ss_decl_seq> & s )
	{
		lassert( !state_stack->empty() );
		state_stack->back()->qual_scope_set( s );
	}
	void elab_spec_set( const ptr<elab_spec_key> & es )
	{
		lassert( !state_stack->empty() );
		state_stack->back()->elab_spec_set( es );
	}
	void namespace_mode_set( bool b )
	{
		lassert( !state_stack->empty() );
		state_stack->back()->namespace_mode_set( b );
	}

//F//	ptr<ss_decl_seq> forced_scope_get() const
//F//	{
//F//		lassert( !state_stack->empty() );
//F//		return state_stack->back()->forced_scope_get();
//F//	}
	ptr<ss_decl_seq> qual_scope_get() const
	{
		lassert( !state_stack->empty() );
		return state_stack->back()->qual_scope_get();
	}
	ptr<elab_spec_key> elab_spec_get() const
	{
		lassert( !state_stack->empty() );
		return state_stack->back()->elab_spec_get();
	}
	bool namespace_mode_get() const
	{
		lassert( !state_stack->empty() );
		return state_stack->back()->namespace_mode_get();
	}
};

typedef list< srp<one_hinter> > hinter_stack_type;
static ptr<hinter_stack_type> hinter_stack;

void spawn_hinter_action::run()
{
	hinter_stack->push_back( one_hinter::create() );
}

void close_hinter_action::run()
{
	lassert( !hinter_stack->empty() );
	hinter_stack->pop_back();
	lassert( !hinter_stack->empty() );
}

void push_state_action::run()
{
	lassert( !hinter_stack->empty() );
	hinter_stack->back()->push_state();
}

void pop_state_action::run()
{
	lassert( !hinter_stack->empty() );
	hinter_stack->back()->pop_state();
}

/*!
 * \pre Manager must be already initialized, as this method installs hooks into it.
 */
void hinter::init( mode_type a_mode )
{
	lassert2( id_hint2tok_type_map.empty(),
			"Trying to initialize hinter for the second time." );

	mode = a_mode;

	hinter_stack = hinter_stack_type::create();
	hinter_stack->push_back( one_hinter::create() );

	id_hint2tok_type_map[ bison_token::HINT_TEMPL_NONTYPE ]
		= bison_token::_TEMPL_NONTYPE;
	id_hint2tok_type_map[ bison_token::HINT_TEMPL_TYPE ]
		= bison_token::_TEMPL_TYPE;
	id_hint2tok_type_map[ bison_token::HINT_NONTYPE ]
		= bison_token::_NONTYPE;
	id_hint2tok_type_map[ bison_token::HINT_CLASS ]
		= bison_token::_CLASS_NAME_ID;
	id_hint2tok_type_map[ bison_token::HINT_ENUM ]
		= bison_token::_ENUM_NAME_ID;
	id_hint2tok_type_map[ bison_token::HINT_TYPEDEF ]
		= bison_token::_TYPEDEF_NAME_ID;
	id_hint2tok_type_map[ bison_token::HINT_NAMESPACE ]
		= bison_token::_NMSPC_NAME;
	id_hint2tok_type_map[ bison_token::HINT_UNKNOWN ]
		= bison_token::_UNKNOWN_ID;

	manager::spawn_event->attach( spawn_hinter_action::instance() );
	manager::close_event->attach( close_hinter_action::instance() );

	manager::start_event->attach( push_state_action::instance() );
	manager::commit_event->attach( pop_state_action::instance() );
	manager::rollback_event->attach( pop_state_action::instance() );
}

hinter::mode_type hinter::mode_get()
{
	return mode;
}

ptr<bison_token> hinter::use_user_hint( const ptr<bison_token> & t )
{
	lassert( t->user_hint_get() != bison_token::HINT_NONE );
	int result_type = id_hint2tok_type_map[ t->user_hint_get() ];
	lassert2( result_type != 0,
			"Either got invalid hint, or the map is corrupted." );

	return bison_id_token::create( result_type, t->wrapped_token_get(),
			bison_id_token::BY_USER,
			declaration_set_type::create() );
}

static ptr<declaration_set_type> unqualified( const ptr<ss_decl_name> & name, const ptr<ss_decl_seq> & seq )
{
	static ptr<lu_params> params;
	static ptr<lu_name_filter> name_filter;
	static ptr<lu_visible_filter> visible_filter;
	if (!params) {
		ptr<lu_multi_filter> big_filter = lu_multi_filter::create();
		params = lu_params::create( lu_params::UDIR_ALWAYS,
				lu_params::UDECL_RESOLVE_BEFORE_FILTER,
				lu_params::SEARCH_PARENTS,
				ss_declaration_time::infinity(), big_filter );
		name_filter = lu_name_filter::create( name );
		big_filter->add_filter( name_filter );
		big_filter->add_filter( lu_func_cont_filter::instance() );
		visible_filter = lu_visible_filter::create( ss_declaration_time::infinity() );
		big_filter->add_filter( visible_filter );
	}
	// the location -> time translation should be made easier...
	ptr<ss_declaration_time> time = ss_declaration_time::create( name->location_get()->order_get() );
	params->time_set( time );
	visible_filter->visible_before_set( time );
	name_filter->name_set( name );
	return lu_lookup::instance()->main( seq, params );
}

static ptr<declaration_set_type> unqualified_elab( const ptr<ss_decl_name> & name, const ptr<ss_decl_seq> & seq )
{
	static ptr<lu_params> params;
	static ptr<lu_name_filter> name_filter;
	static ptr<lu_visible_filter> visible_filter;
	if (!params) {
		ptr<lu_multi_filter> big_filter = lu_multi_filter::create();
		params = lu_params::create( lu_params::UDIR_ALWAYS,
				lu_params::UDECL_RESOLVE_BEFORE_FILTER,
				lu_params::SEARCH_PARENTS,
				ss_declaration_time::infinity(), big_filter );
		name_filter = lu_name_filter::create( name );
		big_filter->add_filter( name_filter );
		big_filter->add_filter( lu_elaborated_filter::instance() );
		visible_filter = lu_visible_filter::create( ss_declaration_time::infinity() );
		big_filter->add_filter( visible_filter );
	}
	// the location -> time translation should be made easier...
	ptr<ss_declaration_time> time = ss_declaration_time::create( name->location_get()->order_get() );
	params->time_set( time );
	visible_filter->visible_before_set( time );
	name_filter->name_set( name );
	return lu_lookup::instance()->main( seq, params );
}

static ptr<declaration_set_type> look_first( const ptr<ss_decl_name> & name, const ptr<ss_decl_seq> & seq )
{
	static ptr<lu_params> params;
	static ptr<lu_name_filter> name_filter;
	static ptr<lu_visible_filter> visible_filter;
	if (!params) {
		ptr<lu_multi_filter> big_filter = lu_multi_filter::create();
		// XXX: really UDIR_ALWAYS ???
		params = lu_params::create( lu_params::UDIR_ALWAYS,
				lu_params::UDECL_RESOLVE_BEFORE_FILTER,
				lu_params::SEARCH_PARENTS,
				ss_declaration_time::infinity(), big_filter );
		name_filter = lu_name_filter::create( name );
		big_filter->add_filter( name_filter );
		big_filter->add_filter( lu_qual_filter::instance() );
		visible_filter = lu_visible_filter::create( ss_declaration_time::infinity() );
		big_filter->add_filter( visible_filter );
	}
	// the location -> time translation should be made easier...
	ptr<ss_declaration_time> time = ss_declaration_time::create( name->location_get()->order_get() );
	params->time_set( time );
	visible_filter->visible_before_set( time );
	name_filter->name_set( name );
	return lu_lookup::instance()->main( seq, params );
}

static ptr<declaration_set_type> look_last( const ptr<ss_decl_name> & name, const ptr<ss_decl_seq> & seq )
{
	static ptr<lu_params> params;
	static ptr<lu_name_filter> name_filter;
	static ptr<lu_visible_filter> visible_filter;
	if (!params) {
		ptr<lu_multi_filter> big_filter = lu_multi_filter::create();
		params = lu_params::create( lu_params::UDIR_FALLBACK,
				lu_params::UDECL_RESOLVE_BEFORE_FILTER,
				lu_params::SKIP_PARENTS,
				ss_declaration_time::infinity(), big_filter );
		name_filter = lu_name_filter::create( name );
		big_filter->add_filter( name_filter );
		big_filter->add_filter( lu_func_cont_filter::instance() );
		visible_filter = lu_visible_filter::create( ss_declaration_time::infinity() );
		big_filter->add_filter( visible_filter );
	}
	// the location -> time translation should be made easier...
	ptr<ss_declaration_time> time = ss_declaration_time::create( name->location_get()->order_get() );
	params->time_set( time );
	visible_filter->visible_before_set( time );
	name_filter->name_set( name );
	return lu_lookup::instance()->main( seq, params );
}

static ptr<declaration_set_type> look_last_elab( const ptr<ss_decl_name> & name, const ptr<ss_decl_seq> & seq )
{
	static ptr<lu_params> params;
	static ptr<lu_name_filter> name_filter;
	static ptr<lu_visible_filter> visible_filter;
	if (!params) {
		ptr<lu_multi_filter> big_filter = lu_multi_filter::create();
		params = lu_params::create( lu_params::UDIR_FALLBACK,
				lu_params::UDECL_RESOLVE_BEFORE_FILTER,
				lu_params::SKIP_PARENTS,
				ss_declaration_time::infinity(), big_filter );
		name_filter = lu_name_filter::create( name );
		big_filter->add_filter( name_filter );
		big_filter->add_filter( lu_elaborated_filter::instance() );
		visible_filter = lu_visible_filter::create( ss_declaration_time::infinity() );
		// we _want_ to see invisible classes in elaborated type specifiers
		// big_filter->add_filter( visible_filter );
	}
	// the location -> time translation should be made easier...
	ptr<ss_declaration_time> time = ss_declaration_time::create( name->location_get()->order_get() );
	params->time_set( time );
	visible_filter->visible_before_set( time );
	name_filter->name_set( name );
	return lu_lookup::instance()->main( seq, params );
}

static ptr<declaration_set_type> middle( const ptr<ss_decl_name> & name, const ptr<ss_decl_seq> & seq )
{
	static ptr<lu_params> params;
	static ptr<lu_name_filter> name_filter;
	static ptr<lu_visible_filter> visible_filter;
	if (!params) {
		ptr<lu_multi_filter> big_filter = lu_multi_filter::create();
		params = lu_params::create( lu_params::UDIR_FALLBACK,
				lu_params::UDECL_RESOLVE_BEFORE_FILTER,
				lu_params::SKIP_PARENTS,
				ss_declaration_time::infinity(), big_filter );
		name_filter = lu_name_filter::create( name );
		big_filter->add_filter( name_filter );
		big_filter->add_filter( lu_qual_filter::instance() );
		visible_filter = lu_visible_filter::create( ss_declaration_time::infinity() );
		big_filter->add_filter( visible_filter );
	}
	// the location -> time translation should be made easier...
	ptr<ss_declaration_time> time = ss_declaration_time::create( name->location_get()->order_get() );
	params->time_set( time );
	visible_filter->visible_before_set( time );
	name_filter->name_set( name );
	return lu_lookup::instance()->main( seq, params );
}

static ptr<declaration_set_type> nmspace( const ptr<ss_decl_name> & name, const ptr<ss_decl_seq> & seq )
{
	static ptr<lu_params> params;
	static ptr<lu_name_filter> name_filter;
	if (!params) {
		ptr<lu_multi_filter> big_filter = lu_multi_filter::create();
		params = lu_params::create( lu_params::UDIR_IGNORE,
				lu_params::UDECL_RESOLVE_BEFORE_FILTER,
				lu_params::SKIP_PARENTS,
				ss_declaration_time::infinity(), big_filter );
		name_filter = lu_name_filter::create( name );
		big_filter->add_filter( name_filter );
// deliberately commented out:
//		big_filter->add_filter( lu_namespace_filter::instance() );
	}
	// the location -> time translation should be made easier...
	ptr<ss_declaration_time> time = ss_declaration_time::create( name->location_get()->order_get() );
	params->time_set( time );
	name_filter->name_set( name );
	return lu_lookup::instance()->main( seq, params );
}

ptr<bison_token> hinter::compute_hint( const ptr<bison_token> & t,
		const ptr<bison_token> & lookahead )
{
	lassert( t && lookahead );

	ptr<ss_decl_seq> qual_scope = qual_scope_get();
	ptr<elab_spec_key> elab_spec = elab_spec_get();

	bool first = !qual_scope;	// first -- there was no qualification
	bool last = lookahead->type_get() != bison_token::TOK_COLON_COLON;
	bool ns_mode = namespace_mode_get();

	ptr<ss_ordinary_name> name = ss_ordinary_name::create( t->location_get(), t->value_get() );

	ptr<ss_decl_seq> seq;
	if (qual_scope)
		seq = qual_scope;
	else
		seq = sa_context_manager::instance()->current()->ss_get()->scope_get();
		// was:
		//seq = forced_scope_get();

	// first && last -> unqualified identifier
	// first && !last -> first part of qualification
	// !first && last -> last part in qualified name
	// !first && !last -> middle part in qualification

	// elab_spec is considered only when last == true

	llog(hl) << "first: " << first << " last: " << last << " elab_spec: " << bool(elab_spec) << "\n";

	ptr<declaration_set_type> lookup_result;

	if (ns_mode && !qual_scope)
		lookup_result = nmspace( name, seq );
	else if (ns_mode && qual_scope)
		lassert2( false, "Not implemented yet." );
	else if (first && last && !elab_spec)
		lookup_result = unqualified( name, seq );
	else if (first && last && elab_spec)
		lookup_result = unqualified_elab( name, seq );
	else if (first && !last)
		lookup_result = look_first( name, seq );
	else if (!first && last && !elab_spec)
		lookup_result = look_last( name, seq );
	else if (!first && last && elab_spec)
		lookup_result = look_last_elab( name, seq );
	else if (!first && !last)
		lookup_result = middle( name, seq );
	lassert(lookup_result);

	llog(hl) << "lookup_result->size(): " << lookup_result->size() << "\n";

	int result_token_type = hint_from_declaration_set::instance()->process( lookup_result );

	// [3.4.3/1] class or namespacename must be found before "non-global" :: operator
	if (!last && result_token_type != bison_token::_CLASS_NAME_ID
			&& result_token_type != bison_token::_NMSPC_NAME) {
		if (result_token_type == bison_token::_UNKNOWN_ID)
			report << unknown_before_coloncolon << t->location_get();
		else
			report << wrong_before_coloncolon << t->location_get();
	}

	/* when we got typedef name *not* prefixed by class-key/enum, see if it is a class name */
	if (result_token_type == bison_token::_TYPEDEF_NAME_ID && !elab_spec) {
		lookup_result = typedef_class_resolver::instance()->process( lookup_result );
		result_token_type =
			hint_from_declaration_set::instance()->process( lookup_result );
	}

	namespace_mode_set(false);
	qual_scope_set(NULL);
	if (last)
		elab_spec_set(NULL);

	ptr<bison_token> result = bison_id_token::create( result_token_type,
		t->wrapped_token_get(), bison_id_token::BY_HINTER, lookup_result );

	llog(hl) << "computed " << result << "\n";

	return result;
}

ptr<bison_token> hinter::hint( const ptr<bison_token> & t, const ptr<bison_token> & lookahead )
{
	lassert( t && lookahead );

	llog(hl) << "hinter::hint( " << t << " ; la: " << lookahead << " ) called\n";

	lassert2( t->type_get() == bison_token::TOK_IDENT,
			"Hinter called on non-identifier token." );

	bool is_user_hinted = t->user_hint_get() != bison_token::HINT_NONE;

	ptr<bison_token> result;

	switch (mode) {
	case HINTER_OFF:
		lassert2( is_user_hinted, "Got unhinted identifier." );
		result = use_user_hint(t);
		break;
	case HINTER_USER:
		if (is_user_hinted)
			result = use_user_hint(t);
		else
			result = compute_hint( t, lookahead );
		break;
	case HINTER_CHECK:
		result = compute_hint( t, lookahead );
		if (is_user_hinted) {
			ptr<bison_token> by_user = use_user_hint(t);
			lassert2( by_user->type_get() == result->type_get(),
				"User and computed hint do not agree." );
		}
		break;
	case HINTER_NORMAL:
		result = compute_hint( t, lookahead );
		break;
	}

	return checked(result);
}

//F//ptr<ss_decl_seq> hinter::forced_scope_get()
//F//{
//F//	lassert( !hinter_stack->empty() );
//F//	return hinter_stack->back()->forced_scope_get();
//F//}

ptr<ss_decl_seq> hinter::qual_scope_get()
{
	lassert( !hinter_stack->empty() );
	return hinter_stack->back()->qual_scope_get();
}

ptr<elab_spec_key> hinter::elab_spec_get()
{
	lassert( !hinter_stack->empty() );
	return hinter_stack->back()->elab_spec_get();
}

bool hinter::namespace_mode_get()
{
	lassert( !hinter_stack->empty() );
	return hinter_stack->back()->namespace_mode_get();
}

//F//void hinter::forced_scope_set( const ptr<ss_decl_seq> & ds )
//F//{
//F//	lassert( !hinter_stack->empty() );
//F//	hinter_stack->back()->forced_scope_set(ds);
//F//}

void hinter::qual_scope_set( const ptr<ss_decl_seq> & ds )
{
	lassert( !hinter_stack->empty() );
	hinter_stack->back()->qual_scope_set(ds);
}

void hinter::elab_spec_set( const ptr<elab_spec_key> & es )
{
	lassert( !hinter_stack->empty() );
	hinter_stack->back()->elab_spec_set(es);
}

void hinter::namespace_mode_set( bool b )
{
	lassert( !hinter_stack->empty() );
	hinter_stack->back()->namespace_mode_set( b );
}

void hinter::qual_scope_set_root()
{
	qual_scope_set( ss_decl_seq::root_instance() );
}

using ::lestes::lang::cplus::sem::ss_namespace_definition;
using ::lestes::lang::cplus::sem::ss_object_declaration;
using ::lestes::lang::cplus::sem::ss_bitfield_declaration;
using ::lestes::lang::cplus::sem::ss_parameter_declaration;
using ::lestes::lang::cplus::sem::ss_fake_declaration;
using ::lestes::lang::cplus::sem::ss_injected_class_declaration;
using ::lestes::lang::cplus::sem::ss_enum_definition;
using ::lestes::lang::cplus::sem::ss_typedef_definition;
using ::lestes::lang::cplus::sem::ss_enumerator_declaration;
using ::lestes::lang::cplus::sem::ss_structure_declaration;
using ::lestes::lang::cplus::sem::ss_function_declaration;
using ::lestes::lang::cplus::sem::ss_method_declaration;
using ::lestes::lang::cplus::sem::ss_using_declaration;
using ::lestes::lang::cplus::sem::ss_compound_stmt_declaration;
using ::lestes::lang::cplus::sem::ss_builtin_operator_declaration;

void hint_from_declaration_set::visit_ss_namespace_definition( ptr< ss_namespace_definition > )
{
	// this type of declaration can only be alone in the set
	lassert( result_get() == bison_token::_UNKNOWN_ID );
	result_set( bison_token::_NMSPC_NAME );
}

void hint_from_declaration_set::visit_ss_object_declaration( ptr< ss_object_declaration > )
{
	// this type of declaration can only be alone in the set
	lassert( result_get() == bison_token::_UNKNOWN_ID );
	result_set( bison_token::_NONTYPE );
}

void hint_from_declaration_set::visit_ss_bitfield_declaration( ptr< ss_bitfield_declaration > )
{
	// this type of declaration can only be alone in the set
	lassert( result_get() == bison_token::_UNKNOWN_ID );
	result_set( bison_token::_NONTYPE );
}

void hint_from_declaration_set::visit_ss_parameter_declaration( ptr< ss_parameter_declaration > )
{
	// this type of declaration can only be alone in the set
	lassert( result_get() == bison_token::_UNKNOWN_ID );
	result_set( bison_token::_NONTYPE );
}

void hint_from_declaration_set::visit_ss_fake_declaration( ptr< ss_fake_declaration > )
{
	// this type of declaration can only be alone in the set
	lassert( result_get() == bison_token::_UNKNOWN_ID );
	result_set( bison_token::_NONTYPE );
}

void hint_from_declaration_set::visit_ss_injected_class_declaration( ptr< ss_injected_class_declaration > )
{
	// FIXME:
	// FIXME: is this correct with respect to A::A::x being forbidden?
	// this type of declaration can only be alone in the set
	lassert( result_get() == bison_token::_UNKNOWN_ID );
	result_set( bison_token::_CLASS_NAME_ID );
}

void hint_from_declaration_set::visit_ss_enum_definition( ptr< ss_enum_definition > )
{
	// this type of declaration can only be alone in the set
	lassert( result_get() == bison_token::_UNKNOWN_ID );
	result_set( bison_token::_ENUM_NAME_ID );
}

void hint_from_declaration_set::visit_ss_typedef_definition( ptr< ss_typedef_definition > )
{
	// this type of declaration can only be alone in the set
	lassert( result_get() == bison_token::_UNKNOWN_ID );
	result_set( bison_token::_TYPEDEF_NAME_ID );
}

void hint_from_declaration_set::visit_ss_enumerator_declaration( ptr< ss_enumerator_declaration > )
{
	// this type of declaration can only be alone in the set
	lassert( result_get() == bison_token::_UNKNOWN_ID );
	result_set( bison_token::_NONTYPE );
}

void hint_from_declaration_set::visit_ss_structure_declaration( ptr< ss_structure_declaration > )
{
	// this type of declaration can only be alone in the set
	lassert( result_get() == bison_token::_UNKNOWN_ID );
	result_set( bison_token::_CLASS_NAME_ID );
}

void hint_from_declaration_set::visit_ss_function_declaration( ptr< ss_function_declaration > )
{
	// allow multiple functions in the set; they cannot be mixed with other types, however
	lassert( result_get() == bison_token::_UNKNOWN_ID || was_function_get() );
	was_function_set( true );
	result_set( bison_token::_NONTYPE );
}

void hint_from_declaration_set::visit_ss_method_declaration( ptr< ss_method_declaration > )
{
	// allow multiple functions in the set; they cannot be mixed with other types, however
	lassert( result_get() == bison_token::_UNKNOWN_ID || was_function_get() );
	was_function_set( true );
	result_set( bison_token::_NONTYPE );
}

void hint_from_declaration_set::visit_ss_using_declaration( ptr< ss_using_declaration > )
{
	// FIXME: is this possible?!
	lassert( false );
}

void hint_from_declaration_set::visit_ss_compound_stmt_declaration( ptr< ss_compound_stmt_declaration > )
{
	lassert( false );
}

void hint_from_declaration_set::visit_ss_builtin_operator_declaration( ptr< ss_builtin_operator_declaration > )
{
	lassert( false );
}

int hint_from_declaration_set::process( ptr<declaration_set_type> ds )
{
	result_set( bison_token::_UNKNOWN_ID );
	was_function_set( false );
	/* visit each of the declarations... */
	for ( declaration_set_type::const_iterator it = ds->begin(); it != ds->end(); ++it )
		(*it)->accept_ss_declaration_visitor( ptr<ss_declaration_visitor>(this) );
	return result_get();
}

using ::lestes::lang::cplus::sem::ss_type;
using ::lestes::lang::cplus::sem::ss_struct_base;
using ::lestes::lang::cplus::sem::ss_class;
using ::lestes::lang::cplus::sem::ss_union;

ptr<declaration_set_type> typedef_class_resolver::process( ptr<declaration_set_type> ds )
{
	lassert2( ds->size() == 1, "Typedef must have only one delcaration." );

	/*
	 * if it is a struct type, sets decl to non-NULL value
	 * otherwise sets it to NULL
	 * */
	(*ds->begin())->type_get()->accept_ss_type_visitor( this );

	ptr<declaration_set_type> result;
	if (decl_get()) {
		result = declaration_set_type::create();
		result->insert( decl_get() );
	} else
		result = ds;

	return result;
}

void typedef_class_resolver::default_action( ptr<ss_type> )
{
	decl_set( NULL );
}

void typedef_class_resolver::visit_ss_struct_base( ptr<ss_struct_base> sb )
{
	decl_set( sb->decl_get() );
}

void typedef_class_resolver::visit_ss_class( ptr<ss_class> c )
{
	visit_ss_struct_base(c);
}

void typedef_class_resolver::visit_ss_union( ptr<ss_union> u )
{
	visit_ss_struct_base(u);
}

end_package(syn);
end_package(cplus);
end_package(lang);
end_package(lestes);
