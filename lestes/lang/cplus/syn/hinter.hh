/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__syn__hinter_hh__included
#define lestes__lang__cplus__syn__hinter_hh__included

#include <lestes/common.hh>
#include <lestes/lang/cplus/syn/token.hh>

#include <map>

package(lestes);
package(lang);
package(cplus);
package(sem);
class ss_decl_seq;
end_package(sem);
package(syn);

// completed in hinter.g.hh
class elab_spec_key;

class hinter {
public:

	enum mode_type {
		HINTER_NORMAL,	// hinter works full-time, ignores user hints
		HINTER_CHECK,	// compares its hints with user's ones (if available)
				//   it is fatal when they don't agree
		HINTER_USER,    // trusts user hints, only hints itself when no user hint is available
		HINTER_OFF	// hinter off; every identifier must be user-hinted
	};

	// possibly returns a new token
	// the hinted token has its origin saved in the flags
	static ptr<bison_token> hint( const ptr<bison_token> & t,
			const ptr<bison_token> & lookahead );

	//! Initializes the hinter; manager must be already initialized when calling this function.
	static void init( mode_type );
	static mode_type mode_get();

//	static void forced_scope_set( const ptr< ::lestes::lang::cplus::sem::ss_decl_seq > & );
	static void qual_scope_set( const ptr< ::lestes::lang::cplus::sem::ss_decl_seq > & );
	static void elab_spec_set( const ptr<elab_spec_key> & );

	static void namespace_mode_set( bool );

	static void qual_scope_set_root();
protected:
	//! maps identifier type hints to token types; note that not all hints are covered!
	static ::std::map<bison_token::hint_type,bison_token::yytokentype> id_hint2tok_type_map;

//	static ptr< ::lestes::lang::cplus::sem::ss_decl_seq > forced_scope_get();
	static ptr< ::lestes::lang::cplus::sem::ss_decl_seq > qual_scope_get();
	static ptr<elab_spec_key> elab_spec_get();

	static bool namespace_mode_get();

	static ptr<bison_token> use_user_hint( const ptr<bison_token> & t );
	static ptr<bison_token> compute_hint( const ptr<bison_token> & t,
					const ptr<bison_token> & lookahead );
private:
	//! Do not allow instantiating this class.
	hinter();
	//! Hide copy-constructor.
	hinter( const hinter & );

	static mode_type mode;
};

end_package(syn);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif	// lestes__lang__cplus__syn__hinter_hh__included
