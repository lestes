/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>
#include <lestes/lang/cplus/syn/parser.hh>
#include <lestes/lang/cplus/syn/token.hh>
#include <lestes/std/list.hh>
#include <lestes/std/pair.hh>
#include <lestes/std/reflect.hh>

#include <ostream>

package(lestes);
package(lang);
package(cplus);
package(syn);

static lstring hint2str( bison_token::hint_type hint )
{
	lstring str = "BUG!";
	switch (hint) {
	case bison_token::HINT_NONE:
		str = "HINT_NONE";
		break;

	case bison_token::HINT_TEMPL_NONTYPE:
		str = "HINT_TEMPL_NONTYPE";
		break;
	case bison_token::HINT_TEMPL_TYPE:
		str = "HINT_TEMPL_TYPE";
		break;
	case bison_token::HINT_NONTYPE:
		str = "HINT_NONTYPE";
		break;
	case bison_token::HINT_CLASS:
		str = "HINT_CLASS";
		break;
	case bison_token::HINT_ENUM:
		str = "HINT_ENUM";
		break;
	case bison_token::HINT_TYPEDEF:
		str = "HINT_TYPEDEF";
		break;
	case bison_token::HINT_NAMESPACE:
		str = "HINT_NAMESPACE";
		break;
	case bison_token::HINT_UNKNOWN:
		str = "HINT_UNKNOWN";
		break;

	case bison_token::HINT_CTOR:
		str = "HINT_CTOR";
		break;
	case bison_token::HINT_NO_CTOR:
		str = "HINT_NO_CTOR";
		break;

	case bison_token::HINT_BITFIELD:
		str = "HINT_BITFIELD";
		break;
	case bison_token::HINT_NO_BITFIELD:
		str = "HINT_NO_BITFIELD";
		break;
	}
	return str;
}

static lstring hinted_by2str( bison_id_token::hinted_by_type hb )
{
	lstring str = "BUG!";
	switch (hb) {
	case bison_id_token::BY_HINTER:
		str = "BY_HINTER";
		break;
//	case bison_id_token::BY_BOTH:
//		str = "BY_BOTH";
//		break;
	case bison_id_token::BY_USER:
		str = "BY_USER";
		break;
	}
	return str;
}

::std::ostream & operator << ( ::std::ostream & os, const ptr<bison_token> & bt )
{
	bt->print( os );
	return os;
}

void bison_token::gc_mark()
{
	wrapped_token.gc_mark();
	as_base::gc_mark();
}

ptr< object::reflection_list > bison_token::reflection = reflection;

ptr< object::reflection_list > bison_token::reflection_get() const
{
	if (!reflection) {
		typedef class_reflection::field_metadata md;
		typedef class_reflection::field_metadata_list mdlist;
		ptr<mdlist> mdl = mdlist::create();
		mdl->push_back( md::create( "type", "lstring" ) );
		mdl->push_back( md::create( "wrapped_token", "cpp_token" ) );
		mdl->push_back( md::create( "user_hint", "lstring" ) );
		// append our one to a copy of the base class's list
		reflection = reflection_list::create( as_base::reflection_get() );
		reflection->push_back( class_reflection::create( "bison_token", mdl ) );
	}
	return reflection;
}

ptr< object::field_list_list > bison_token::field_values_get() const
{
	ptr<field_list_list> result = as_base::field_values_get();
	result->push_back( value_list::create() );
	result->back()->push_back( objectize<lstring>::create( parser::token_type2name(type) ) );
	result->push_back( value_list::create() );
	result->back()->push_back( wrapped_token );
	result->push_back( value_list::create() );
	result->back()->push_back( objectize<lstring>::create( hint2str(user_hint) ) );
	return result;
}

void bison_token::print( ::std::ostream & os ) const
{
	os << "bison_token " << parser::token_type2name(type);
	if (type == TOK_IDENT || type == TOK_LITERAL)
		os << " (\"" << value_get() << "\")";
	if (user_hint != HINT_NONE)
		os << ", user " << hint2str(user_hint);
}

ptr< object::reflection_list > bison_madeup_token::reflection = reflection;

ptr< object::reflection_list > bison_madeup_token::reflection_get() const
{
	if (!reflection) {
		typedef class_reflection::field_metadata md;
		typedef class_reflection::field_metadata_list mdlist;
		ptr<mdlist> mdl = mdlist::create();
		// append our one to a copy of the base class's list
		reflection = reflection_list::create( bison_token::reflection_get() );
		reflection->push_back( class_reflection::create( "bison_madeup_token", mdl ) );
	}
	return reflection;
}

ptr< object::field_list_list > bison_madeup_token::field_values_get() const
{
	return bison_token::field_values_get();
}

void bison_madeup_token::print( ::std::ostream & os ) const
{
	os << "bison_madeup_token " << parser::token_type2name(type_get());
	lassert2( user_hint_get() == HINT_NONE, "Made up tokens cannot have hints!" );
}

void bison_id_token::gc_mark()
{
	found_decls.gc_mark();
	bison_token::gc_mark();
}

ptr< ::lestes::lang::cplus::sem::declaration_set_type > bison_id_token::found_decls_get() const
{
	return found_decls;
}

ptr< object::reflection_list > bison_id_token::reflection = reflection;

ptr< object::reflection_list > bison_id_token::reflection_get() const
{
	if (!reflection) {
		typedef class_reflection::field_metadata md;
		typedef class_reflection::field_metadata_list mdlist;
		ptr<mdlist> mdl = mdlist::create();
		mdl->push_back( md::create( "hinted_by", "lstring" ) );
		// append our one to a copy of the base class's list
		reflection = reflection_list::create( bison_token::reflection_get() );
		reflection->push_back( class_reflection::create( "bison_id_token", mdl ) );
	}
	return reflection;
}

ptr< object::field_list_list > bison_id_token::field_values_get() const
{
	ptr<field_list_list> result = bison_token::field_values_get();
	result->push_back( value_list::create() );
	result->back()->push_back( objectize<lstring>::create( hinted_by2str(hinted_by) ) );
	return result;
}

void bison_id_token::print( ::std::ostream & os ) const
{
	os << "bison_id_token " << parser::token_type2name(type_get()) <<
		" (\"" << value_get() << "\"),"
		" hinted " << hinted_by2str(hinted_by);
	lassert2( user_hint_get() == HINT_NONE, "Id tokens must not have hints!" );
}

void bison_pack_token::gc_mark()
{
	pack.gc_mark();
	end_location.gc_mark();
#if 0 || BISON_PACK_TOKEN_ERRORS_WANTED
	errors.gc_mark();
#endif
	bison_madeup_token::gc_mark();
}

ptr< object::reflection_list > bison_pack_token::reflection = reflection;

ptr< object::reflection_list > bison_pack_token::reflection_get() const
{
	if (!reflection) {
		typedef class_reflection::field_metadata md;
		typedef class_reflection::field_metadata_list mdlist;
		ptr<mdlist> mdl = mdlist::create();
		mdl->push_back( md::create( "pack", "list&lt; srp&lt; bison_token &gt; &gt;" ) );
		mdl->push_back( md::create( "end_location", "source_location" ) );
#if 0 || BISON_PACK_TOKEN_ERRORS_WANTED
		mdl->push_back( md::create( "errors", "list&lt; srp&lt; bison_token &gt; &gt;" ) );
#endif
		// append our one to a copy of the base class's list
		reflection = reflection_list::create( bison_madeup_token::reflection_get() );
		reflection->push_back( class_reflection::create( "bison_pack_token", mdl ) );
	}
	return reflection;
}

ptr< object::field_list_list > bison_pack_token::field_values_get() const
{
	ptr<field_list_list> result = bison_madeup_token::field_values_get();
	result->push_back( value_list::create() );
	result->back()->push_back( pack );
	result->push_back( value_list::create() );
	result->back()->push_back( end_location );
#if 0 || BISON_PACK_TOKEN_ERRORS_WANTED
	result->push_back( value_list::create() );
	result->back()->push_back( errors );
#endif
	return result;
}

void bison_pack_token::print( ::std::ostream & os ) const
{
	os << "bison_pack_token " << parser::token_type2name(type_get()) <<
		", size " << pack->size();
	lassert2( user_hint_get() == HINT_NONE, "Pack tokens must not have hints!" );
}

end_package(syn);
end_package(cplus);
end_package(lang);
end_package(lestes);
