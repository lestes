%{
/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */

#define YYSTYPE	semval

#define clear_lookahead()			\
	do {					\
		lassert( yychar != YYEMPTY );	\
		yychar = YYEMPTY;		\
		manager::back_up();		\
	} while (false)			

#define try_begin( _txt ) do {			\
		llog_xml_open(pl,"disambiguation") << "\nkind: " << _txt << "\n";

#define try_end_fail()									\
			llog_xml_close(pl,"disambiguation") << " all tries failed\n";	\
			if (manager::in_disambiguation())				\
				YYABORT;	/* returns from yyparse() */		\
			report << disa_failed << loc(manager::peek());			\
			/* commit ANY pad token, or NO_PAD_TOKEN_BUG() may kick in */	\
			manager::start( TOK_START_TO_BE_DISCARDED );			\
			manager::commit( TOK_PAD_TO_BE_DISCARDED );			\
			manager::failing_set_true();					\
		} while (false)

#define try_end_skip() llog_xml_close(pl,"disambiguation") << "\n";	\
		} while (false)

// DO NOT use this one in *any* construct (if, for, ...)
// (or enclose it in braces :)
#define try_and_commit( start_token_value, commit_token_value, result_var )	\
	manager::start( start_token_value );					\
	ptr<parse_result_type> result_var = parser::parse();			\
	if (result_var->success_get()) {					\
		manager::commit( commit_token_value );				\
		llog_xml_close(pl,"disambiguation") << "\n";			\
		break;								\
	}									\
	manager::rollback();

static bool is_not_pad( int i )
{
	return (i < _PAD_FIRST || i > _PAD_LAST);
}

static bool is_pack( int i )
{
	return (i > _PACK_FIRST && i < _PACK_LAST);
}

static void disambiguate_ctor_and_bitfield_class_scope( ptr<as_declaration_specifier_seq> const );
static void disambiguate_ctor_nmspc_scope( ptr<as_declaration_specifier_seq> const );

#define loc(x)					((x)->location_get())

#define UNWTOK(x)				((x)->wrapped_token_get())

#define EMPTY_LIST( type )			::lestes::std::list< srp< type > >::create()
#define EMPTY_WRAPPER( wrapper_type, l, type )	wrapper_type::create( (l), EMPTY_LIST(type) )
// "create empty wrapper with one item" ;-)
#define NEW_WRAPPER_W1( dest, wrapper_type, l, type, item )				\
			do {								\
				(dest) = EMPTY_WRAPPER( wrapper_type, (l), type );	\
				WRAPPER_APPEND( (dest), (item) );			\
			} while(false)

#define LIST_APPEND( list, item )		(list)->push_back(item)
#define LIST_PREPEND( list, item )		(list)->push_front(item)
#define LIST_APPEND_LIST( append_to, list )	(append_to)->splice( (append_to)->end(), *(list) )
#define LIST_PREPEND_LIST( prepend_to, list )	(prepend_to)->splice( (prepend_to)->begin(), *(list) )

#define UNWRAP(w)				(w)->l_get()
#define WRAPPER_APPEND( wrapper, item )		LIST_APPEND( UNWRAP(wrapper), (item) )
#define WRAPPER_PREPEND( wrapper, item )	LIST_PREPEND( UNWRAP(wrapper), (item) )
#define WRAPPER_APPEND_WRAPPER( append_to, w )	LIST_APPEND_LIST( UNWRAP(append_to), UNWRAP(w) )

/*!
  \brief Accessory macro for obtaining current as_name context.

  \invariant The value of this macro shall have the as_name::was_qualified set to false.
*/
#define CURR_CTX(l)	(sa_context_manager::instance()->current()->as_get()->scope_get())
#define GLOB_CTX(l)	as_name::create( (l), NULL, as_global_namespace_fake_id::create(l), true )

#define DUMMY_STMT	as_dummy_statement::create(DUMMY_LOC)
#define DUMMY_LOC	source_location::zero()
#define DUMMY_DCLTOR	as_dummy_declarator::create( DUMMY_LOC, NULL, EMPTY_LIST(as_declarator_op) )

#define EMPTY_EXPR(l)	as_empty_expression::create( (l) )
#define EMPTY_DCLTOR(l)	as_declarator::create( (l), NULL, EMPTY_LIST(as_declarator_op) )
#define EMPTY_TYPE_ID(l,dl)	as_type_id::create( (l), EMPTY_LIST(as_type_specifier), EMPTY_DCLTOR(dl) )

#define EXPR2STMT(x)	as_expression_statement::create( loc(x), (x) )
#define TOK2ID(x)	as_identifier::create( loc(x), (x) )

#define EMPTY_ID(l)	as_empty_id::create( (l) )

#define CURR_ACCESS_SPEC() (sa_context_manager::instance()->current()->as_get()->access_specifier_get())

#define NO_PAD_TOKEN_BUG(l)	lassert3( false, "Disambiguation did not commit a PAD token.", (l) )

#define sel(type)	select< type >()

%}

%pure-parser
%parse-param { ptr<as_base> & as_result }
%token-table
%error-verbose

%nonassoc _LOWEST_PREC

%token <sel(bison_token)> TOK_LITERAL

// keywords
%token <sel(bison_token)> TOK_ASM
%token <sel(bison_token)> TOK_AUTO
%token <sel(bison_token)> TOK_BOOL
%token <sel(bison_token)> TOK_BREAK
%token <sel(bison_token)> TOK_CASE
%token <sel(bison_token)> TOK_CATCH
%token <sel(bison_token)> TOK_CHAR
%token <sel(bison_token)> TOK_CLASS
%token <sel(bison_token)> TOK_CONST
%token <sel(bison_token)> TOK_CONST_CAST
%token <sel(bison_token)> TOK_CONTINUE
%token <sel(bison_token)> TOK_DEFAULT
%token <sel(bison_token)> TOK_DELETE
%token <sel(bison_token)> TOK_DO
%token <sel(bison_token)> TOK_DOUBLE
%token <sel(bison_token)> TOK_DYNAMIC_CAST
%token <sel(bison_token)> TOK_ELSE
%token <sel(bison_token)> TOK_ENUM
%token <sel(bison_token)> TOK_EXPLICIT
%token <sel(bison_token)> TOK_EXPORT
%token <sel(bison_token)> TOK_EXTERN
%token <sel(bison_token)> TOK_FLOAT
%token <sel(bison_token)> TOK_FOR
%token <sel(bison_token)> TOK_FRIEND
%token <sel(bison_token)> TOK_GOTO
%token <sel(bison_token)> TOK_IF
%token <sel(bison_token)> TOK_INLINE
%token <sel(bison_token)> TOK_INT
%token <sel(bison_token)> TOK_LONG
%token <sel(bison_token)> TOK_MUTABLE
%token <sel(bison_token)> TOK_NAMESPACE
%token <sel(bison_token)> TOK_NEW
%token <sel(bison_token)> TOK_OPERATOR
%token <sel(bison_token)> TOK_PRIVATE
%token <sel(bison_token)> TOK_PROTECTED
%token <sel(bison_token)> TOK_PUBLIC
%token <sel(bison_token)> TOK_REGISTER
%token <sel(bison_token)> TOK_REINTERPRET_CAST
%token <sel(bison_token)> TOK_RETURN
%token <sel(bison_token)> TOK_SHORT
%token <sel(bison_token)> TOK_SIGNED
%token <sel(bison_token)> TOK_SIZEOF
%token <sel(bison_token)> TOK_STATIC
%token <sel(bison_token)> TOK_STATIC_CAST
%token <sel(bison_token)> TOK_STRUCT
%token <sel(bison_token)> TOK_SWITCH
%token <sel(bison_token)> TOK_TEMPLATE
%token <sel(bison_token)> TOK_THIS
%token <sel(bison_token)> TOK_THROW
%token <sel(bison_token)> TOK_TRY
%token <sel(bison_token)> TOK_TYPEDEF
%token <sel(bison_token)> TOK_TYPEID
%token <sel(bison_token)> TOK_TYPENAME
%token <sel(bison_token)> TOK_UNION
%token <sel(bison_token)> TOK_UNSIGNED
%token <sel(bison_token)> TOK_USING
%token <sel(bison_token)> TOK_VIRTUAL
%token <sel(bison_token)> TOK_VOID
%token <sel(bison_token)> TOK_VOLATILE
%token <sel(bison_token)> TOK_WCHAR_T
%token <sel(bison_token)> TOK_WHILE

// not used in the grammar, but needed for the lexer
%token <BOOM> TOK_IDENT

/* _TYPEDEF_NAME_ID is used for typedefs that do *not* "point" to classes, eg. typedef int int_type; */
%nonassoc <sel(bison_id_token)> _TEMPL_NONTYPE _TEMPL_TYPE _NONTYPE _TEMPL_FORCED _CLASS_NAME_ID _ENUM_NAME_ID _TYPEDEF_NAME_ID
%nonassoc <sel(bison_id_token)> _UNKNOWN_ID

%nonassoc <sel(bison_id_token)> _NMSPC_NAME

/* disambiguation tokens */
%token _PAD_FIRST

%token TOK_PAD_EXPR_STMT TOK_PAD_BLOCK_DECL TOK_PAD_SIMPLE_DECL TOK_PAD_LABEL
%token TOK_PAD_COND_EXPR TOK_PAD_COND_DECL
%token TOK_PAD_FOR_COND_EXPR TOK_PAD_FOR_COND_DECL
%token TOK_PAD_SIZEOF_EXPR TOK_PAD_SIZEOF_TYPE_ID
%token TOK_PAD_TYPE_ID_RPAR_CAST_EXPR
%token TOK_PAD_EXPR_RPAR TOK_PAD_TYPE_ID_RPAR
%token TOK_PAD_LPAR_EXPR_LIST_RPAR TOK_PAD_EXPR_LIST_RPAR
%token TOK_PAD_TEMPL_ARG_EXPR TOK_PAD_TEMPL_ARG_TYPE_ID
%token TOK_PAD_PARAM_DECL_CLAUSE TOK_PAD_PARAM_DCLTOR
%token TOK_PAD_TEMPL_PARAM_TYPE TOK_PAD_TEMPL_PARAM_NONTYPE

%token TOK_PAD_CTOR TOK_PAD_UNNAMED_BITFIELD

%token TOK_PAD_MEMBER_FUNC TOK_PAD_MEMBER_OBJECT

%token TOK_PAD_TEMPL_OP TOK_PAD_OP_NOT_TEMPL

%token TOK_PAD_TO_BE_DISCARDED // used when commit()ing pack token

%token _PAD_LAST

/* starting tokens */
%token _START_FIRST

%token TOK_START_PROGRAM

%token TOK_START_EXPR_STMT TOK_START_BLOCK_DECL TOK_START_SIMPLE_DECL
%token TOK_START_LABEL TOK_START_COND_EXPR TOK_START_COND_DECL
%token TOK_START_FOR_COND_EXPR TOK_START_FOR_COND_DECL
%token TOK_START_SIZEOF_EXPR TOK_START_SIZEOF_TYPE_ID
%token TOK_START_EXPR_RPAR TOK_START_TYPE_ID_RPAR
%token TOK_START_TYPE_ID_RPAR_CAST_EXPR
%token TOK_START_LPAR_EXPR_LIST_RPAR TOK_START_EXPR_LIST_RPAR
%token TOK_START_TEMPL_ARG_EXPR TOK_START_TEMPL_ARG_TYPE_ID
%token TOK_START_PARAM_DECL_CLAUSE TOK_START_PARAM_DCLTOR
%token TOK_START_TEMPL_PARAM_TYPE TOK_START_TEMPL_PARAM_NONTYPE

%token TOK_START_CTOR

%token TOK_START_TO_BE_DISCARDED // used when commit()ing right after start()ing

%token _START_LAST

/* pack tokens; used as starting tokens too */
%token _PACK_FIRST

%token <sel(bison_pack_token)> TOK_PACK_BODY

%token <sel(bison_pack_token)> TOK_PACK_ERROR_COND
%token <sel(bison_pack_token)> TOK_PACK_ERROR_FOR_COND
%token <sel(bison_pack_token)> TOK_PACK_ERROR_STMT

%token _PACK_LAST

%nonassoc <sel(bison_token)> TOK_COLON TOK_EQ TOK_LEFT_PAR TOK_RIGHT_PAR TOK_SEMICOLON TOK_LEFT_BRACE TOK_RIGHT_BRACE TOK_COMMA TOK_QMARK TOK_VBAR TOK_HAT TOK_AMP TOK_LT TOK_GT TOK_PLUS TOK_MINUS TOK_STAR TOK_SLASH TOK_PERCENT TOK_TILDE TOK_EXCLAMATION TOK_LEFT_BRACKET TOK_RIGHT_BRACKET TOK_DOT
%nonassoc <sel(bison_token)> TOK_PLUS_EQ TOK_MINUS_EQ TOK_STAR_EQ TOK_SLASH_EQ TOK_PERCENT_EQ TOK_HAT_EQ TOK_AMP_EQ TOK_VBAR_EQ TOK_GT_GT_EQ TOK_LT_LT_EQ TOK_PLUS_PLUS TOK_MINUS_MINUS TOK_LT_LT TOK_GT_GT TOK_VBAR_VBAR TOK_AMP_AMP TOK_EQ_EQ TOK_EXCLAMATION_EQ TOK_LT_EQ TOK_GT_EQ TOK_DOT_STAR TOK_MINUS_GT_STAR TOK_MINUS_GT TOK_DOT_DOT_DOT

/* TOK_COLON_COLON is here to have higher precedence than all the ID tokens and TOK_LT and ... */
%nonassoc <sel(bison_token)> TOK_COLON_COLON	// "::"

%nonassoc _HIGHEST_PREC

%start start

/* the following have to be declared to be available to flex, but they never reach us */
%token <sel(bison_token)> TOK_EOF 0

%token <sel(bison_token)> TOK_ERROR
%token <sel(bison_token)> TOK_TEXT

%type<sel(as_declaration_seq)>		program _decl_seq_in_nmspc_scope_opt _decl_seq_in_nmspc_scope
%type<sel(as_no_token)>			_no_token _next_location
%type<sel(as_no_token)>			_lookahead_assert_empty _lookahead_assert_non_empty
%type<sel(as_try_block_statement)>	try_block
%type<sel(as_function_definition)>	func_try_block _func_defn_tail _ctor_defn_tail _func_defn_dtc
%type<sel(as_function_definition)>	_func_defn_in_class_tail
%type<sel(as_function_definition)>	_func_defn_in_class_scope _func_defn_in_nmspc_scope
%type<sel(as_handler)>			handler
%type<sel(as_exception_declaration)>	excptn_decl
%type<sel(as_exception_specification)>	excptn_spec
%type<sel(as_type_id)>			type_id new_type_id
%type<sel(as_template_param)>		templ_param
%type<sel(as_template_type_param)>	type_param
%type<sel(as_access_specifier)>		access_spec
%type<sel(as_access_spec_opt_bearer)>	access_spec_opt
%type<sel(as_function_specifier)>	func_spec
%type<sel(as_class_key)>		class_key _class_key_real
%type<sel(as_name)>			nested_name_spec coloncolon_opt
%type<sel(as_name)>			_nested_nmspc_name _nested_nmspc_spec
%type<sel(as_storage_class_specifier)>	storage_class_spec
%type<sel(as_cv_qualifier)>		cv_qualifier
%type<sel(as_cv_qualifier_seq)>		cv_qualifier_seq
%type<sel(as_expression)>		expr expr_opt _for_expr
%type<sel(as_condition)>		condition condition_opt
%type<sel(as_if_statement)>		if_head
%type<sel(as_compound_statement)>	compound_stmt
%type<sel(as_statement)>		stmt stmt_else_can_follow stmt_else_cant_follow _non_if_stmt
%type<sel(as_statement)>		_non_if_stmt_real
%type<sel(as_labeled_statement)>	label _label_head
%type<sel(as_iteration_statement)>	_for_head _for_head_real _while_head
%type<sel(as_switch_statement)>		_switch_head
%type<sel(as_identifier)>		_anyid _tts1 _enum_or_typedef_name_id _class_forward_id
%type<sel(as_mem_initializer)>		mem_inizer
%type<sel(as_mem_initializer_list)>	mem_inizer_list mem_inizer_list_opt
%type<sel(as_enumerator_definition)>	enumerator_defn
%type<sel(as_using_declaration)>	using_decl _access_decl
%type<sel(as_expression)>		assign_expr cond_expr log_or_expr log_and_expr
%type<sel(as_expression)>		or_expr xor_expr and_expr eq_expr rel_expr shift_expr
%type<sel(as_expression)>		add_expr mul_expr pm_expr cast_expr un_expr new_expr
%type<sel(as_expression)>		delete_expr postfix_expr
%type<sel(as_expression)>		_assign_expr_no_gt _cond_expr_no_gt _log_or_expr_no_gt _log_and_expr_no_gt
%type<sel(as_expression)>		_or_expr_no_gt _xor_expr_no_gt _and_expr_no_gt _eq_expr_no_gt
%type<sel(as_expression)>		_rel_expr_no_gt
%type<sel(as_constant_expression)>	const_expr const_expr_opt
%type<sel(as_name)>			id_expr unqualified_id _id_expr_nontype _unqualified_id_nontype
%type<sel(as_name)>			_id_expr_dtc _id_expr_dtc_in_opt_par
%type<sel(as_name)>			_dtc_unqualified_id _ctor_unqualified_id
%type<sel(as_name)>			conv_func_id op_func_id _using_id
%type<sel(as_name)>			_member_access_suffix pseudo_destr_name _pdn1
%type<sel(as_name)>			_simple_type_spec_exclusive_name
%type<sel(as_op_function_id)>		_op_func_id_orig
%type<sel(as_initializer_clause)>	inizer_clause
%type<sel(as_init_declarator)>		init_dcltor
%type<sel(as_init_declarator_list)>	init_dcltor_list
%type<sel(as_simple_declaration)>	_simple_decl_in_func_scope _simple_decl_in_nmspc_scope
%type<sel(as_declarator_op)>		ptr_op _func_suffix_clean _ctor_func_suffix _bracket_suffix
%type<sel(as_declarator_op)>		_func_suffix_clean_no_scope_change
%type<sel(as_declarator)>		dcltor direct_dcltor _dcltor_action
%type<sel(as_declarator)>		_inner_dcltor _inner_direct_dcltor
%type<sel(as_declarator)>		_in_class_dcltor_action
%type<sel(as_declarator)>		_non_bitfield_member_dcltor_inizer_action
%type<sel(as_declarator)>		_param_dcltor _param_direct_dcltor
%type<sel(as_declarator)>		abst_dcltor direct_abst_dcltor
%type<sel(as_declarator)>		new_dcltor direct_new_dcltor
%type<sel(as_param_declaration)>	param_decl _param_decl_no_gt
%type<sel(as_param_decl_clause)>	param_decl_clause
%type<sel(as_statement_seq)>		stmt_seq stmt_seq_opt
%type<sel(as_initializer_list)>		inizer_list
%type<sel(as_handler_seq)>		handler_seq _func_handler_seq
%type<sel(as_type_id_list)>		type_id_list
%type<sel(as_template_argument_list)>	templ_arg_list _templ_args
%type<sel(as_template_argument)>	templ_arg
%type<sel(as_template_id)>		_tts2
%type<sel(as_type_specifier)>		_typename_type_spec
%type<sel(as_builtin_simple_type_specifier)> _simple_type_spec_non_exclusive
%type<sel(as_named_simple_type_specifier)> _ctor_type
%type<sel(as_simple_type_specifier)>	_simple_type_spec_exclusive
%type<sel(as_type_specifier)>		_type_spec_not_declaring_class_enum_exclusive
%type<sel(as_type_specifier)>		_type_spec_declaring_class_enum
%type<sel(as_anyid_opt_bearer)>		_anyid_opt
%type<sel(as_excptn_spec_opt_bearer)>	excptn_spec_opt
%type<sel(as_enumerator_list)>		enumerator_list enumerator_list_opt
%type<sel(as_member_declaration)>	member_decl
%type<sel(as_declarator_op_seq)>	_ptr_op_seq
%type<sel(as_declaration_specifier)>	_decl_spec_exclusive _decl_spec_non_exclusive _decl_spec_other
%type<sel(as_declaration_specifier_seq)> _decl_spec_seq_in_func_scope _decl_spec_seq_in_param_scope
%type<sel(as_declaration_specifier_seq)>  _decl_spec_seq_exclusive _decl_spec_seq_non_exclusive
%type<sel(as_declaration_specifier_seq)> _decl_spec_seq_other _decl_spec_seq_orig
%type<sel(as_declaration_specifier_seq)> _decl_spec_seq_in_class_scope _decl_spec_seq_in_nmspc_scope
%type<sel(as_member_declarator_list)>	member_dcltor_list _next_member_dcltor_list_cdtc
%type<sel(as_member_declarator_list)>	_cdtc_member_dcltor_list_starting_with_ctor_no_par
%type<sel(as_member_declarator_list)>	_cdtc_member_dcltor_list_not_starting_with_ctor_no_par
%type<sel(as_param_declaration_list)>	param_decl_list
%type<sel(as_virtual_opt_bearer)>	virtual_opt
%type<sel(as_type_specifier_seq)>	_type_spec_seq_not_declaring_class_enum
%type<sel(as_type_specifier_seq)>	_type_spec_seq_not_declaring_class_enum_exclusive
%type<sel(as_type_specifier_seq)>	_type_spec_seq_not_declaring_class_enum_non_exclusive
%type<sel(as_block_declaration)>	_block_decl_in_func_scope _block_decl_in_nmspc_scope
%type<sel(as_expression_throw)>		throw_expr
%type<sel(as_expression_list)>		expr_list _expr_list_opt_in_pars
%type<sel(bison_token)>			op _enum
%type<sel(as_declaration)>		_decl_in_nmspc_scope
%type<sel(as_namespace_definition)>	nmspc_defn _nmspc_defn_head
%type<sel(as_template_param_list)>	templ_param_list
%type<sel(as_member_specification)>	member_spfn member_spfn_opt
%type<sel(as_declarator)>		_dtc_dcltor _dtc_dcltor_action
%type<sel(as_base_specifier)>		base_spec
%type<sel(as_base_specifier_list)>	base_spec_list
%type<sel(as_ordinary_member_declarator)> _non_bitfield_member_dcltor _dtc_member_dcltor
%type<sel(as_ordinary_member_declarator)> _ctor_first_member_dcltor_in_par
%type<sel(as_ordinary_member_declarator)> _ctor_first_member_dcltor_no_par _ctor_next_member_dcltor
%type<sel(as_ordinary_member_declarator)> _cdtc_next_member_dcltor _cdtc_first_member_dcltor
%type<sel(as_bitfield_member_declarator)> _first_bitfield_member_dcltor _next_bitfield_member_dcltor
%type<sel(as_bitfield_member_declarator)> _named_bitfield_member_dcltor
%type<sel(as_class_specifier)>		_class_specifier_head _class_specifier_action
%type<sel(bison_pack_token)>		_packed_compound_stmt
%type<sel(as_constructor_id)>		_ctor_name_in_opt_par _ctor_name_in_no_par _ctor_name_in_par
%type<sel(as_member_declaration_ordinary)> _cdtc_member_decl
%type<sel(bison_token)>			_if_action _while_action _for_action _enter_compound_action


%%

_lookahead_assert_empty:
	  _next_location
	  {
		lassert3( yychar == YYEMPTY, "Lookahead is supposed to be empty.", loc($1) );
		$$ = $1;
	  }
	;

_lookahead_assert_non_empty:
	  _next_location
	  {
		lassert3( yychar != YYEMPTY, "Lookahead is not supposed to be empty.", loc($1) );
		$$ = $1;
	  }
	;

_hinter_to_root_scope:
	  _lookahead_assert_empty
	  { hinter::qual_scope_set_root(); }
	;

_hinter_pdc_scope:
	  _lookahead_assert_empty
	  /* XXX action */
	;

_hinter_pdc_scope_end:
	  _lookahead_assert_empty
	  /* XXX action */
	;

_hinter_templ_id_follows:
	  _lookahead_assert_empty
	  /* XXX action */
	;

_hinter_nmspc_name_follows:
	  _lookahead_assert_empty
	  { hinter::namespace_mode_set(true); }
	;

/* meant tu be used *only* after namespace keyword *not* followed by an identifier (an unnamed namespace) */
_hinter_nmspc_name_clear:
	  _lookahead_assert_non_empty
	  { hinter::namespace_mode_set(false); }
	;

// only used when qualification has to be explicitly cleared
//  * after a non-identifier (eg. operator+)
//  * in member pointer rules (ptr_op)
_hinter_clear_qualification:
	  /* empty */
	  {
		// for simplicity, there is one nonterminal for both cases:
		//   with and without the lookahead token read
		if (yychar != YYEMPTY)
			clear_lookahead();
		hinter::qual_scope_set( NULL );
	  }
	;

_hinter_descend_pseudo_destr:
	  _lookahead_assert_empty
	  /* XXX action */
	  /* actually, we probably will not want to move from the scope we are in [3.4.3/5] */
	;


_disambiguate_expr_stmt_vs_block_decl_vs_label:
	  _lookahead_assert_empty
	  {
	  	int next_val = manager::peek()->type_get();
		switch (next_val) {
			case TOK_BREAK:
			case TOK_CATCH:
			case TOK_CONTINUE:
			case TOK_DO:
			case TOK_ELSE:
			case TOK_FOR:
			case TOK_GOTO:
			case TOK_IF:
			case TOK_RETURN:
			case TOK_SWITCH:
			case TOK_TRY:
			case TOK_WHILE:
			case TOK_SEMICOLON:
			case TOK_EOF:
			case TOK_LEFT_BRACE:
			case TOK_RIGHT_BRACE:
				/* no ambiguity here */
				break;
			case TOK_PAD_LABEL:
			case TOK_PAD_BLOCK_DECL:
			case TOK_PAD_EXPR_STMT:
				/* ambiguity already resolved */
				break;
			default:
				/* pad tokens that are allowed in this context are handled above */
				lassert2( is_not_pad(next_val), "Illegal pad token." );

				try_begin( "expr_stmt_vs_block_decl_vs_label" );
				try_and_commit( TOK_START_LABEL, TOK_PAD_LABEL, r1 );
				try_and_commit( TOK_START_BLOCK_DECL, TOK_PAD_BLOCK_DECL, r2 );
				try_and_commit( TOK_START_EXPR_STMT, TOK_PAD_EXPR_STMT, r3 );
				/* all tries failed, report the errors */
				ptr<bison_pack_token> err_pack = manager::pack( TOK_PACK_ERROR_STMT, NULL );
				syntax_error::disa_stmt( err_pack, loc(r2->last_token_get()),
						loc(r3->last_token_get()) );
				try_end_skip();
		}
	  }
	;

_disambiguate_expr_stmt_vs_simple_decl:
	  _lookahead_assert_empty
	  {
		int next_val = manager::peek()->type_get();
		if (is_not_pad(next_val) && next_val != TOK_SEMICOLON ) {
			try_begin( "expr_stmt_vs_simple_decl" );
			/* [6.8/1] */
			try_and_commit( TOK_START_SIMPLE_DECL, TOK_PAD_SIMPLE_DECL, r1 );
			try_and_commit( TOK_START_EXPR_STMT, TOK_PAD_EXPR_STMT, r2 );
			try_end_fail();
		}
	  }
	;

_disambiguate_cast_expr_vs_expr_in_pars:
	  _lookahead_assert_empty
	  {
		if (is_not_pad(manager::peek()->type_get())) {
			try_begin( "cast_expr_vs_expr_in_pars" );
			/*
			 * [8.2/2] and examples therein
			 * "(T())+4" is a funny example.
			 * hint: [5.3.1/6], compare with "((T()))+4"
			 */
			try_and_commit( TOK_START_TYPE_ID_RPAR_CAST_EXPR,
						TOK_PAD_TYPE_ID_RPAR_CAST_EXPR, r1 );
			try_and_commit( TOK_START_EXPR_RPAR, TOK_PAD_EXPR_RPAR, r2 );
			try_end_fail();
		}
	  }
	;

_disambiguate_expr_vs_type_id:
	  _lookahead_assert_empty
	  {
		if (is_not_pad(manager::peek()->type_get())) {
			try_begin( "expr_vs_type_id" );
			/* [8.2/2] and examples therein */
			try_and_commit( TOK_START_TYPE_ID_RPAR, TOK_PAD_TYPE_ID_RPAR, r1 );
			try_and_commit( TOK_START_EXPR_RPAR, TOK_PAD_EXPR_RPAR, r2 );
			try_end_fail();
		}
	  }
	;

_disambiguate_condition:
	  _lookahead_assert_empty
	  {
		ptr<source_location> prev_loc = loc(manager::prev_yylex());
		int next_val = manager::peek()->type_get();
		if (is_not_pad(next_val)) {
			try_begin( "condition" );
			// [6.4/5]
			try_and_commit( TOK_START_COND_DECL, TOK_PAD_COND_DECL, r1 );
			try_and_commit( TOK_START_COND_EXPR, TOK_PAD_COND_EXPR, r2 );
			/* error... */
			ptr<bison_pack_token> err_pack =
				manager::pack( TOK_PACK_ERROR_COND, prev_loc );
			syntax_error::condition( err_pack, loc(r1->last_token_get()),
					loc(r2->last_token_get()) );
			try_end_skip();
		}
	  }
	;

_disambiguate_for_condition_opt:
	  _lookahead_assert_empty
	  {
		ptr<source_location> prev_loc = loc(manager::prev_yylex());
		int next_val = manager::peek()->type_get();
		if (is_not_pad(next_val) && next_val != TOK_SEMICOLON ) {
			try_begin( "condition" );
			try_and_commit( TOK_START_FOR_COND_DECL, TOK_PAD_FOR_COND_DECL, r1 );
			try_and_commit( TOK_START_FOR_COND_EXPR, TOK_PAD_FOR_COND_EXPR, r2 );
			/* error... */
			ptr<bison_pack_token> err_pack =
				manager::pack( TOK_PACK_ERROR_FOR_COND, prev_loc );
			syntax_error::condition( err_pack, loc(r1->last_token_get()),
					loc(r2->last_token_get()) );
			try_end_skip();
		}
	  }
	;

_disambiguate_sizeof:
	  _lookahead_assert_empty
	  {
		if (is_not_pad(manager::peek()->type_get())) {
			try_begin( "sizeof" );
			/* [8.2/2] and examples therein */
			try_and_commit( TOK_START_SIZEOF_TYPE_ID, TOK_PAD_SIZEOF_TYPE_ID, r2 );
			try_and_commit( TOK_START_SIZEOF_EXPR, TOK_PAD_SIZEOF_EXPR, r1 );
			try_end_fail();
		}
	  }
	;

_disambiguate_expr_list_vs_type_id:
	  _lookahead_assert_empty
	  {
		if (is_not_pad(manager::peek()->type_get())) {
			try_begin( "expr_list_vs_type_id" );
			/* [8.2/2] and examples therein */
			try_and_commit( TOK_START_TYPE_ID_RPAR, TOK_PAD_TYPE_ID_RPAR, r1 );
			try_and_commit( TOK_START_EXPR_LIST_RPAR, TOK_PAD_EXPR_LIST_RPAR, r2 );
			try_end_fail();
		}
	  }
	;

_disambiguate_templ_arg:
	  _lookahead_assert_empty
	  {
		int next_val = manager::peek()->type_get();
		if (is_not_pad(next_val) && next_val != TOK_GT ) {
			try_begin( "templ_arg" );
			/* [8.2/2] and examples therein */
			try_and_commit( TOK_START_TEMPL_ARG_TYPE_ID, TOK_PAD_TEMPL_ARG_TYPE_ID, r1 );
			try_and_commit( TOK_START_TEMPL_ARG_EXPR, TOK_PAD_TEMPL_ARG_EXPR, r2 );
			try_end_fail();
		}
	  }
	;

_disambiguate_templ_param:
	  _lookahead_assert_empty
	  {
		int next_val = manager::peek()->type_get();
		// when it is explicit specialization, do nothing
		if (is_not_pad(next_val) && next_val != TOK_GT) {
			try_begin( "templ_param" );
			// [14.1/3]
			try_and_commit( TOK_START_TEMPL_PARAM_TYPE, TOK_PAD_TEMPL_PARAM_TYPE, r1 );
			try_and_commit( TOK_START_TEMPL_PARAM_NONTYPE, TOK_PAD_TEMPL_PARAM_NONTYPE, r2 );
			try_end_fail();
		}
	  }
	;

/*
 * we are not sure about the lookahead in this case:
 *   after "int f()" it must be read, looking for const/volatile/throw;
 *   while after "int i[5]", or "int f()throw()" it doesn't have to be
 * see direct_dcltor, where this rule is used
 */
_disambiguate_param_decl_clause_vs_expr_list:
	  /* empty */
	{
		if (yychar != YYEMPTY)
			clear_lookahead();
		int next_val = manager::peek()->type_get();
		if (next_val == TOK_LEFT_PAR) {
			try_begin( "pdc_vs_expr_list" );
			// [8.2/1]
			try_and_commit( TOK_START_PARAM_DECL_CLAUSE, TOK_PAD_PARAM_DECL_CLAUSE, r1 );
			try_and_commit( TOK_START_LPAR_EXPR_LIST_RPAR, TOK_PAD_LPAR_EXPR_LIST_RPAR, r2 );
			try_end_fail();
		} else {
			/* if it is a pad, it must be one of those two */
			lassert( is_not_pad(next_val) ||
				next_val == TOK_PAD_PARAM_DECL_CLAUSE ||
				next_val == TOK_PAD_LPAR_EXPR_LIST_RPAR );
		}
	}
	;

_disambiguate_param_decl_clause_vs_param_dcltor:
	  _lookahead_assert_non_empty {
	// either the right pad, or '(' must be in the lookahead
	if (yychar == TOK_LEFT_PAR) {
		clear_lookahead();
		try_begin( "pdc_vs_param_dcltor" );
		// [8.2/7]
		try_and_commit( TOK_START_PARAM_DECL_CLAUSE, TOK_PAD_PARAM_DECL_CLAUSE, r1 );
		try_and_commit( TOK_START_PARAM_DCLTOR, TOK_PAD_PARAM_DCLTOR, r2 );
		try_end_fail();
	} else
		lassert( yychar == TOK_PAD_PARAM_DECL_CLAUSE || yychar == TOK_PAD_PARAM_DCLTOR );
	}
	;

/* used when location of next token is needed, but the token itself is part of another rule */
_next_location:
	  /* empty */
	  {
		if (yychar == YYEMPTY)
			$$ = as_no_token::create( loc(manager::peek()) );
		else {
			/* we could use yylval and cast it, but this is cleaner probably */
			$$ = as_no_token::create( loc(manager::prev_yylex()) );
		}
	  }
	;

/*
 * same as _next_location, but with non-empty lookahead assertion
 * used in rules when something can be optionally omitted, but a location is needed
 */
_no_token:
	  _lookahead_assert_non_empty
	  { $$ = $1; }
	;

/*

How I translate terms and suffixes from the ISO C++ norm.

Norm		My notation

opt		_opt
declaration	decl
definition	defn
specifier	spec
function	func
template	templ
namespace	nmspc
declarator	dcltor
operator	op
conversion	conv
parameter	param
argument	arg
destructor	destr
placement	pcmt
initializer	inizer
explicit	expl
specification	spfn
directive	drctve
exception	excptn

 */

start:
	  /* empty */
	  {
		lassert2( false, "The parser should always see at least one token." );
		/* an ugly hack to supress the warning; see parser.cc too */
		goto yyerrlab1; // we never get here
	  }
	| TOK_START_PROGRAM program { as_result = $2; }
	| disambiguation { YYACCEPT; }// this is needed as TOK_EOF does not follow
	| packs
	;

disambiguation:
	  TOK_START_PARAM_DECL_CLAUSE TOK_LEFT_PAR param_decl_clause TOK_RIGHT_PAR
	| TOK_START_PARAM_DCLTOR TOK_LEFT_PAR _param_dcltor TOK_RIGHT_PAR
	| TOK_START_LPAR_EXPR_LIST_RPAR TOK_LEFT_PAR expr_list TOK_RIGHT_PAR 

	| TOK_START_SIMPLE_DECL _simple_decl_in_func_scope
	| TOK_START_BLOCK_DECL _block_decl_in_func_scope 
	| TOK_START_EXPR_STMT expr TOK_SEMICOLON 
	| TOK_START_LABEL label 

	| TOK_START_EXPR_RPAR expr TOK_RIGHT_PAR 
	| TOK_START_TYPE_ID_RPAR type_id TOK_RIGHT_PAR 
	| TOK_START_EXPR_LIST_RPAR expr_list TOK_RIGHT_PAR 
	| TOK_START_TYPE_ID_RPAR_CAST_EXPR type_id TOK_RIGHT_PAR cast_expr

	| TOK_START_COND_EXPR expr TOK_RIGHT_PAR
	| TOK_START_COND_DECL _type_spec_seq_not_declaring_class_enum dcltor TOK_EQ assign_expr TOK_RIGHT_PAR

	| TOK_START_FOR_COND_EXPR expr TOK_SEMICOLON
	| TOK_START_FOR_COND_DECL _type_spec_seq_not_declaring_class_enum dcltor TOK_EQ assign_expr TOK_SEMICOLON

	| TOK_START_SIZEOF_EXPR un_expr 
	| TOK_START_SIZEOF_TYPE_ID TOK_LEFT_PAR type_id TOK_RIGHT_PAR 

	| TOK_START_TEMPL_ARG_EXPR _assign_expr_no_gt 
	| TOK_START_TEMPL_ARG_TYPE_ID type_id 

	// make sure that we read the whole parameter
	| TOK_START_TEMPL_PARAM_TYPE type_param TOK_COMMA
	| TOK_START_TEMPL_PARAM_TYPE type_param TOK_GT
	| TOK_START_TEMPL_PARAM_NONTYPE _param_decl_no_gt

	| TOK_START_CTOR TOK_LEFT_PAR param_decl_clause TOK_RIGHT_PAR TOK_THROW		// excptn_spec_opt
	| TOK_START_CTOR TOK_LEFT_PAR param_decl_clause TOK_RIGHT_PAR cv_qualifier	// cv_qualifier_seq
	| TOK_START_CTOR TOK_LEFT_PAR param_decl_clause TOK_RIGHT_PAR TOK_COLON		// mem_inizer_list_opt
	| TOK_START_CTOR TOK_LEFT_PAR param_decl_clause TOK_RIGHT_PAR TOK_LEFT_BRACE	// compound_stmt
	| TOK_START_CTOR TOK_LEFT_PAR param_decl_clause TOK_RIGHT_PAR TOK_TRY		// "ctor_try_block"
	| TOK_START_CTOR TOK_LEFT_PAR param_decl_clause TOK_RIGHT_PAR TOK_COMMA		// next member declarator; applicable only in class scope
	| TOK_START_CTOR TOK_LEFT_PAR param_decl_clause TOK_RIGHT_PAR TOK_SEMICOLON
	;

packs:
	  TOK_PACK_BODY stmt_seq_opt { as_result = $2; }
	;

program:
	  _decl_seq_in_nmspc_scope_opt
	  { $$ = $1; }
	;

label:
	  _anyid TOK_COLON
	  { $$ = as_identifier_labeled_statement::create( loc($1), DUMMY_STMT, $1 ); }
	| TOK_CASE const_expr TOK_COLON
	  { $$ = as_case_labeled_statement::create( loc($1), DUMMY_STMT, $2 ); }
	| TOK_DEFAULT TOK_COLON
	  { $$ = as_default_labeled_statement::create( loc($1), DUMMY_STMT ); }
	;

/* this can be only used when the TOK_COLON_COLON means that the "root" namespace is to be searched */
coloncolon_opt:
	  _no_token
	  { $$ = CURR_CTX( loc($1) ); }
	| TOK_COLON_COLON _hinter_to_root_scope
	  { $$ = GLOB_CTX( loc($1) ); }
	;

/* only used in for statement */
condition_opt:
	  _disambiguate_for_condition_opt _no_token	// semicolon follows
	  { $$ = as_empty_condition::create( loc($2) ); }
	| _disambiguate_for_condition_opt TOK_PAD_FOR_COND_EXPR expr
	  { $$ = as_condition_expression::create( loc($3), $3 ); }
	| _disambiguate_for_condition_opt TOK_PAD_FOR_COND_DECL _type_spec_seq_not_declaring_class_enum _dcltor_action TOK_EQ assign_expr
	  {
		$$ = as_condition_declaration::create( loc($3), UNWRAP($3), $4, $6 );
		sa_statements::instance()->condition_decl(loc($3), $6);
	  }
	| _disambiguate_for_condition_opt TOK_PACK_ERROR_FOR_COND
	  {
		// create empty expression instead of the invalid one
		$$ = as_empty_condition::create( loc($2) );
	  }
	;

condition:
	  _disambiguate_condition TOK_PAD_COND_EXPR expr
	  { $$ = as_condition_expression::create( loc($3), $3 ); }
	| _disambiguate_condition TOK_PAD_COND_DECL _type_spec_seq_not_declaring_class_enum _dcltor_action TOK_EQ assign_expr
	  {
		$$ = as_condition_declaration::create( loc($3), UNWRAP($3), $4, $6 );
		sa_statements::instance()->condition_decl(loc($3), $6);
	  }
	| _disambiguate_condition TOK_PACK_ERROR_COND
	  {
		// create empty expression instead of the invalid one
		$$ = as_empty_condition::create( loc($2) );
	  }
	;

if_head:
	  _if_action TOK_LEFT_PAR condition TOK_RIGHT_PAR _disambiguate_expr_stmt_vs_block_decl_vs_label
	  {
		$$ = as_if_statement::create( loc($1), $3, DUMMY_STMT, DUMMY_STMT );
		sa_statements::instance()->if_head(loc($1), $3);
	  }
	;

_if_action:
	  TOK_IF
	  {
		$$ = $1;
		sa_statements::instance()->enter_scope(loc($1));
	  }

// the following constructs (_*_head) suffer from dangling else problem

_label_head:
	  TOK_PAD_LABEL label _disambiguate_expr_stmt_vs_block_decl_vs_label
	  { $$ = $2; }
	;

expr_opt:
	  _no_token
	  { $$ = EMPTY_EXPR( loc($1) ); }
	| expr
	  { $$ = $1; }
	;

_for_head:
	  _for_head_real _disambiguate_expr_stmt_vs_block_decl_vs_label
	  { $$ = $1; }
	;

_for_head_real:
	  _for_action TOK_LEFT_PAR _disambiguate_expr_stmt_vs_simple_decl TOK_SEMICOLON _for_inner_action condition_opt TOK_SEMICOLON expr_opt TOK_RIGHT_PAR
	  { // for ( ; cond; expr)
		$$ = as_for_statement::create( loc($1), DUMMY_STMT, as_for_init_statement_expression::create(loc($4),EXPR2STMT(EMPTY_EXPR(loc($4)))), $6, $8 );
		sa_statements::instance()->for_head(loc($1), $6, $8);
	  }
	| _for_action TOK_LEFT_PAR _disambiguate_expr_stmt_vs_simple_decl TOK_PAD_EXPR_STMT _for_expr TOK_SEMICOLON _for_inner_action condition_opt TOK_SEMICOLON expr_opt TOK_RIGHT_PAR
	  { // for ( expr; cond; expr)
		$$ = as_for_statement::create( loc($1), DUMMY_STMT, as_for_init_statement_expression::create(loc($5),EXPR2STMT($5)), $8, $10 );
		sa_statements::instance()->for_head(loc($1), $8, $10);
	  }
	| _for_action TOK_LEFT_PAR _disambiguate_expr_stmt_vs_simple_decl TOK_PAD_SIMPLE_DECL _simple_decl_in_func_scope _for_inner_action condition_opt TOK_SEMICOLON expr_opt TOK_RIGHT_PAR
	  { // for( decl; cond; expr)
		$$ = as_for_statement::create( loc($1), DUMMY_STMT, as_for_init_statement_declaration::create(loc($5),$5), $7, $9 );
		sa_statements::instance()->for_head(loc($1), $7, $9);
	  }
	;

_for_expr:
	  expr
	  {
		$$ = $1;
		sa_statements::instance()->expression_stmt($1);
	  }
	;

_for_inner_action:
	  _lookahead_assert_empty
	  {
		sa_statements::instance()->for_inner_action(loc($1));
	  }
	;

_for_action:
	  TOK_FOR
	  {
		$$ = $1;
		sa_statements::instance()->enter_scope(loc($1));
	  }
	;

_while_head:
	  _while_action TOK_LEFT_PAR condition TOK_RIGHT_PAR _disambiguate_expr_stmt_vs_block_decl_vs_label
	  {
		$$ = as_while_statement::create( loc($1), DUMMY_STMT, $3 );
		sa_statements::instance()->while_head( loc($1), $3 );
	  }
	;

_while_action:
	  TOK_WHILE
	  {
		$$ = $1;
		sa_statements::instance()->enter_scope(loc($1));
	  }
	;

_switch_head:
	  TOK_SWITCH TOK_LEFT_PAR condition TOK_RIGHT_PAR _disambiguate_expr_stmt_vs_block_decl_vs_label
	  { $$ = as_switch_statement::create( loc($1), $3, DUMMY_STMT ); }
	;

stmt_seq_opt:
	  _disambiguate_expr_stmt_vs_block_decl_vs_label _no_token
	  { $$ = EMPTY_WRAPPER( as_statement_seq, loc($2), as_statement ); }
	| _disambiguate_expr_stmt_vs_block_decl_vs_label stmt_seq
	  { $$ = $2; }
	;

stmt_seq:
	  stmt
	  { NEW_WRAPPER_W1( $$, as_statement_seq, loc($1), as_statement, $1 ); }
	| stmt_seq stmt
	  { $$ = $1; WRAPPER_APPEND( $$, $2 ); }
	;

_non_if_stmt:
	  _non_if_stmt_real _disambiguate_expr_stmt_vs_block_decl_vs_label
	  { $$ = $1; }
	  /*
	   * try_block is special as it runs the disambiguation after each of the handlers,
	   * before reading the token that signals end/continuation of the handler sequence
	   */
	| try_block
	  { $$ = $1; }
	;

// these constructs are put in the same nonterminal as they can't end with if/else
_non_if_stmt_real:
	  TOK_SEMICOLON
	  { $$ = EXPR2STMT( EMPTY_EXPR(loc($1)) ); }
	| TOK_PAD_EXPR_STMT expr TOK_SEMICOLON
	  {
		ptr<as_expression_statement> as = as_expression_statement::create( loc($2), $2 );
		sa_statements::instance()->expression_stmt(as->expression_get());
		$$ = as;
	  }
	| TOK_BREAK TOK_SEMICOLON
	  { $$ = as_break_statement::create( loc($1) ); }
	| TOK_CONTINUE TOK_SEMICOLON
	  { $$ = as_continue_statement::create( loc($1) ); }
	| TOK_RETURN expr_opt TOK_SEMICOLON
	  {
		ptr<as_return_statement> as = as_return_statement::create( loc($1), $2 );
		sa_statements::instance()->return_stmt(as);
		$$ = as;
	  }
	| TOK_GOTO _anyid TOK_SEMICOLON
	  { $$ = as_goto_statement::create( loc($1), $2 ); }
	| compound_stmt
	  { $$ = $1; }
	| TOK_DO stmt TOK_WHILE TOK_LEFT_PAR expr TOK_RIGHT_PAR TOK_SEMICOLON
	  { $$ = as_do_statement::create( loc($1), $2, $5 ); }
	| TOK_PAD_BLOCK_DECL _block_decl_in_func_scope
	  { $$ = as_declaration_statement::create( loc($2), $2 ); }
	| TOK_PACK_ERROR_STMT
	  {
		/* create empty expression statement instead of the errorneous input */
		$$ = EXPR2STMT( EMPTY_EXPR(loc($1)) );
	  }
	;

compound_stmt:
	  _enter_compound_action stmt_seq_opt TOK_RIGHT_BRACE
	  {
		$$ = as_compound_statement::create( loc($1), UNWRAP($2) );
		sa_statements::instance()->leave_scope();
	  }
	;

_enter_compound_action:
	  TOK_LEFT_BRACE
	  {
		$$ = $1;
		sa_statements::instance()->enter_scope(loc($1));
	  }
	;

stmt:
	  stmt_else_cant_follow
	  { $$ = $1; }
	| stmt_else_can_follow
	  { $$ = $1; }
	;

stmt_else_can_follow:
	  _label_head stmt_else_can_follow
	  { $1->statement_set( $2 ); $$ = $1; }
	| _for_head stmt_else_can_follow
	  {
		$1->controlled_statement_set( $2 );
		$$ = $1;
		sa_statements::instance()->leave_scope(); // leave body compound statement
		sa_statements::instance()->leave_scope(); // leave 'for' inner encapsulating compound
		sa_statements::instance()->leave_scope(); // leave 'for' outer encapsulating compound
	  }
	| _while_head stmt_else_can_follow
	  {
		$1->controlled_statement_set( $2 );
		$$ = $1;
		sa_statements::instance()->leave_scope(); // leave body compound statement
		sa_statements::instance()->leave_scope(); // leave 'while' encapsulating compound
	  }
	| _switch_head stmt_else_can_follow
	  { $1->controlled_statement_set( $2 ); $$ = $1; }
	| _non_if_stmt
	  { $$ = $1; }
	| if_head stmt_else_can_follow _else_action stmt_else_can_follow
	  {
		$1->then_statement_set( $2 );
		$1->else_statement_set( $4 );
		$$ = $1;
		sa_statements::instance()->leave_scope(); // leave 'else' compound statement
		sa_statements::instance()->leave_scope(); // leave 'if' encapsulating compoud
	  }
	;

stmt_else_cant_follow:
	  _label_head stmt_else_cant_follow
	  { $1->statement_set( $2 ); $$ = $1; }
	| _for_head stmt_else_cant_follow
	  {
		$1->controlled_statement_set( $2 );
		$$ = $1;
		sa_statements::instance()->leave_scope(); // leave body compound statement
		sa_statements::instance()->leave_scope(); // leave 'for' inner encapsulating compound
		sa_statements::instance()->leave_scope(); // leave 'for' outer encapsulating compound
	  }
	| _while_head stmt_else_cant_follow
	  {
		$1->controlled_statement_set( $2 );
		$$ = $1;
		sa_statements::instance()->leave_scope(); // leave body compound statement
		sa_statements::instance()->leave_scope(); // leave 'while' encapsulating compound
	  }
	| _switch_head stmt_else_cant_follow
	  { $1->controlled_statement_set( $2 ); $$ = $1; }
	| if_head stmt
	  {
		$1->then_statement_set( $2 );
		$1->else_statement_set( NULL );
		$$ = $1;
		sa_statements::instance()->leave_scope(); // leave 'then' compound statement
		sa_statements::instance()->leave_scope(); // leave 'if' encapsulating compound
	  }
	| if_head stmt_else_can_follow _else_action stmt_else_cant_follow
	  {
		$1->then_statement_set( $2 );
		$1->else_statement_set( $4 );
		$$ = $1;
		sa_statements::instance()->leave_scope(); // leave 'else' compound statement
		sa_statements::instance()->leave_scope(); // leave 'if' encapsulating compound
	  }
	;

_else_action:
	  TOK_ELSE _disambiguate_expr_stmt_vs_block_decl_vs_label
	  {
		sa_statements::instance()->leave_scope(); // leave 'then' compound statement
		sa_statements::instance()->enter_else(); // enter 'else' compound statement
	  }
	;

// 5.18 Comma operator
expr:
	  assign_expr
	  { $$ = $1; }
	| expr TOK_COMMA assign_expr
	  { $$ = as_expression_comma::create( loc($2), $1, $3 ); }
	;

// 5.19 Constant expressions
const_expr:
	  cond_expr
	  { $$ = as_constant_expression::create( loc($1), $1 ); }
	;

// 5.17 Assignment operators
assign_expr:
	  cond_expr
	  { $$ = $1; }
	| log_or_expr TOK_EQ assign_expr
	  { $$ = as_expression_equal::create( loc($2), $1, $3 ); }
	| log_or_expr TOK_PLUS_EQ assign_expr
	  { $$ = as_expression_plus_equal::create( loc($2), $1, $3 ); }
	| log_or_expr TOK_MINUS_EQ assign_expr
	  { $$ = as_expression_minus_equal::create( loc($2), $1, $3 ); }
	| log_or_expr TOK_STAR_EQ assign_expr
	  { $$ = as_expression_star_equal::create( loc($2), $1, $3 ); }
	| log_or_expr TOK_SLASH_EQ assign_expr
	  { $$ = as_expression_slash_equal::create( loc($2), $1, $3 ); }
	| log_or_expr TOK_PERCENT_EQ assign_expr
	  { $$ = as_expression_percent_equal::create( loc($2), $1, $3 ); }
	| log_or_expr TOK_HAT_EQ assign_expr
	  { $$ = as_expression_hat_equal::create( loc($2), $1, $3 ); }
	| log_or_expr TOK_AMP_EQ assign_expr
	  { $$ = as_expression_amp_equal::create( loc($2), $1, $3 ); }
	| log_or_expr TOK_VBAR_EQ assign_expr
	  { $$ = as_expression_vbar_equal::create( loc($2), $1, $3 ); }
	| log_or_expr TOK_GT_GT_EQ assign_expr
	  { $$ = as_expression_greater_greater_equal::create( loc($2), $1, $3 ); }
	| log_or_expr TOK_LT_LT_EQ assign_expr
	  { $$ = as_expression_less_less_equal::create( loc($2), $1, $3 ); }
	| throw_expr
	  { $$ = $1; }
	;

// 5.16 Conditional operator
cond_expr:
	  log_or_expr
	  { $$ = $1; }
	| log_or_expr TOK_QMARK expr TOK_COLON assign_expr
	  { $$ = as_expression_qmark::create( loc($2), $1, $3, $5 ); }
	;

// 5.15 Logical OR operator
log_or_expr:
	  log_and_expr
	  { $$ = $1; }
	| log_or_expr TOK_VBAR_VBAR log_and_expr
	  { $$ = as_expression_vbar_vbar::create( loc($2), $1, $3 ); }
	;

// 5.14 Logical AND operator
log_and_expr:
	  or_expr
	  { $$ = $1; }
	| log_and_expr TOK_AMP_AMP or_expr
	  { $$ = as_expression_amp_amp::create( loc($2), $1, $3 ); }
	;

// 5.13 Bitwise inclusive OR operator
or_expr:
	  xor_expr
	  { $$ = $1; }
	| or_expr TOK_VBAR xor_expr
	  { $$ = as_expression_vbar::create( loc($2), $1, $3 ); }
	;

// 5.12 Bitwise exclusive OR operator
xor_expr:
	  and_expr
	  { $$ = $1; }
	| xor_expr TOK_HAT and_expr
	  { $$ = as_expression_hat::create( loc($2), $1, $3 ); }
	;

// 5.11 Bitwise AND operator
and_expr:
	  eq_expr
	  { $$ = $1; }
	| and_expr TOK_AMP eq_expr
	  { $$ = as_expression_amp::create( loc($2), $1, $3 ); }
	;

// 5.10 Equality operators
eq_expr:
	  rel_expr
	  { $$ = $1; }
	| eq_expr TOK_EQ_EQ rel_expr
	  { $$ = as_expression_equal_equal::create( loc($2), $1, $3 ); }
	| eq_expr TOK_EXCLAMATION_EQ rel_expr
	  { $$ = as_expression_exclam_equal::create( loc($2), $1, $3 ); }
	;

// 5.9 Relational operators
rel_expr:
	  shift_expr
	  { $$ = $1; }
	| rel_expr TOK_LT shift_expr
	  { $$ = as_expression_less::create( loc($2), $1, $3 ); }
	| rel_expr TOK_GT shift_expr
	  { $$ = as_expression_greater::create( loc($2), $1, $3 ); }
	| rel_expr TOK_LT_EQ shift_expr
	  { $$ = as_expression_less_equal::create( loc($2), $1, $3 ); }
	| rel_expr TOK_GT_EQ shift_expr
	  { $$ = as_expression_greater_equal::create( loc($2), $1, $3 ); }
	;

// 5.8 Shift operators
shift_expr:
	  add_expr
	  { $$ = $1; }
	| shift_expr TOK_LT_LT add_expr
	  { $$ = as_expression_less_less::create( loc($2), $1, $3 ); }
	| shift_expr TOK_GT_GT add_expr
	  { $$ = as_expression_greater_greater::create( loc($2), $1, $3 ); }
	;

// 5.7 Additive operators
add_expr:
	  mul_expr
	  { $$ = $1; }
	| add_expr TOK_PLUS mul_expr
	  { $$ = as_expression_plus::create( loc($2), $1, $3 ); }
	| add_expr TOK_MINUS mul_expr
	  { $$ = as_expression_minus::create( loc($2), $1, $3 ); }
	;

// 5.6 Multiplicative operators
mul_expr:
	  pm_expr
	  { $$ = $1; }
	| mul_expr TOK_STAR pm_expr
	  { $$ = as_expression_star::create( loc($2), $1, $3 ); }
	| mul_expr TOK_SLASH pm_expr
	  { $$ = as_expression_slash::create( loc($2), $1, $3 ); }
	| mul_expr TOK_PERCENT pm_expr
	  { $$ = as_expression_percent::create( loc($2), $1, $3 ); }
	;

// 5.5 Pointer-to-member operators
pm_expr:
	  cast_expr
	  { $$ = $1; }
	| pm_expr TOK_DOT_STAR cast_expr
	  { $$ = as_expression_dot_star::create( loc($2), $1, $3 ); }
	| pm_expr TOK_MINUS_GT_STAR cast_expr
	  { $$ = as_expression_minus_greater_star::create( loc($2), $1, $3 ); }
	;

// 5.4 Explicit type conversion (cast notation)
cast_expr:
	  un_expr
	  { $$ = $1; }
	| TOK_LEFT_PAR _disambiguate_cast_expr_vs_expr_in_pars TOK_PAD_TYPE_ID_RPAR_CAST_EXPR type_id TOK_RIGHT_PAR cast_expr
	  { $$ = as_expression_old_style_cast::create( loc($1), $4, $6 ); }
	;

// 5.3 Unary expressions
un_expr:
	  postfix_expr
	  { $$ = $1; }
	| TOK_PLUS_PLUS cast_expr
	  { $$ = as_expression_plus_plus_pre::create( loc($1), $2 ); }
	| TOK_MINUS_MINUS cast_expr
	  { $$ = as_expression_minus_minus_pre::create( loc($1), $2 ); }
	| TOK_STAR cast_expr
	  { $$ = as_expression_unary_star::create( loc($1), $2 ); }
	| TOK_AMP cast_expr
	  { $$ = as_expression_unary_amp::create( loc($1), $2 ); }
	| TOK_PLUS cast_expr
	  { $$ = as_expression_unary_plus::create( loc($1), $2 ); }
	| TOK_MINUS cast_expr
	  { $$ = as_expression_unary_minus::create( loc($1), $2 ); }
	| TOK_EXCLAMATION cast_expr
	  { $$ = as_expression_exclam::create( loc($1), $2 ); }
	| TOK_TILDE cast_expr
	  { $$ = as_expression_tilde::create( loc($1), $2 ); }
	| TOK_SIZEOF _disambiguate_sizeof TOK_PAD_SIZEOF_EXPR un_expr
	  { $$ = as_expression_sizeof_expr::create( loc($1), $4 ); }
	| TOK_SIZEOF _disambiguate_sizeof TOK_PAD_SIZEOF_TYPE_ID TOK_LEFT_PAR type_id TOK_RIGHT_PAR
	  { $$ = as_expression_sizeof_type::create( loc($1), $5 ); }
	| new_expr
	  { $$ = $1; }
	| delete_expr
	  { $$ = $1; }
	;

new_expr:
	// with new_type_id (is it a as_type_id? FIXME)
	  coloncolon_opt TOK_NEW new_type_id
	  { $$ = as_expression_new::create( loc($2), $1, NULL, $3, NULL ); }
	| coloncolon_opt TOK_NEW TOK_LEFT_PAR _disambiguate_expr_list_vs_type_id TOK_PAD_EXPR_LIST_RPAR expr_list TOK_RIGHT_PAR new_type_id
	  { $$ = as_expression_new::create( loc($2), $1, UNWRAP($6), $8, NULL ); }
	| coloncolon_opt TOK_NEW new_type_id _expr_list_opt_in_pars
	  { $$ = as_expression_new::create( loc($2), $1, NULL, $3, as_initializer_expression_list::create(loc($4),UNWRAP($4)) ); }
	| coloncolon_opt TOK_NEW TOK_LEFT_PAR _disambiguate_expr_list_vs_type_id TOK_PAD_EXPR_LIST_RPAR expr_list TOK_RIGHT_PAR new_type_id _expr_list_opt_in_pars
	  { $$ = as_expression_new::create( loc($2), $1, UNWRAP($6), $8, as_initializer_expression_list::create(loc($9),UNWRAP($9)) ); }
	// with type_id in parentheses
	| coloncolon_opt TOK_NEW TOK_LEFT_PAR _disambiguate_expr_list_vs_type_id TOK_PAD_TYPE_ID_RPAR type_id TOK_RIGHT_PAR
	  { $$ = as_expression_new::create( loc($2), $1, NULL, $6, NULL ); }
	| coloncolon_opt TOK_NEW TOK_LEFT_PAR _disambiguate_expr_list_vs_type_id TOK_PAD_EXPR_LIST_RPAR expr_list TOK_RIGHT_PAR TOK_LEFT_PAR type_id TOK_RIGHT_PAR
	  { $$ = as_expression_new::create( loc($2), $1, UNWRAP($6), $9, NULL ); }
	| coloncolon_opt TOK_NEW TOK_LEFT_PAR _disambiguate_expr_list_vs_type_id TOK_PAD_TYPE_ID_RPAR type_id TOK_RIGHT_PAR _expr_list_opt_in_pars
	  { $$ = as_expression_new::create( loc($2), $1, NULL, $6, as_initializer_expression_list::create(loc($8),UNWRAP($8)) ); }
	| coloncolon_opt TOK_NEW TOK_LEFT_PAR _disambiguate_expr_list_vs_type_id TOK_PAD_EXPR_LIST_RPAR expr_list TOK_RIGHT_PAR TOK_LEFT_PAR type_id TOK_RIGHT_PAR _expr_list_opt_in_pars
	  { $$ = as_expression_new::create( loc($2), $1, UNWRAP($6), $9, as_initializer_expression_list::create(loc($11),UNWRAP($11)) ); }
	;

new_type_id:
	  _type_spec_seq_not_declaring_class_enum _next_location		%prec _LOWEST_PREC
	  { $$ = as_type_id::create( loc($1), UNWRAP($1), EMPTY_DCLTOR(loc($2)) ); }
	| _type_spec_seq_not_declaring_class_enum _next_location new_dcltor
	  { $$ = as_type_id::create( loc($1), UNWRAP($1), $3 ); }
	;

new_dcltor:
	  _ptr_op_seq			%prec _LOWEST_PREC
	  { $$ = as_declarator::create( loc($1), NULL, UNWRAP($1) ); }
	| _ptr_op_seq direct_new_dcltor
	  { $$ = $2; LIST_APPEND_LIST( $$->declarator_ops_get(), UNWRAP($1) ); } 
	| direct_new_dcltor
	  { $$ = $1; }
	;

direct_new_dcltor:
	  TOK_LEFT_BRACKET expr TOK_RIGHT_BRACKET
	  { $$ = as_declarator::create( loc($1), NULL, EMPTY_LIST(as_declarator_op) );
	    LIST_APPEND( $$->declarator_ops_get(), as_declarator_op_non_constant_array::create(loc($1),$2) ); }
	| direct_new_dcltor TOK_LEFT_BRACKET const_expr TOK_RIGHT_BRACKET
	  { $$ = $1; LIST_APPEND( $$->declarator_ops_get(), as_declarator_op_array::create(loc($2),$3) ); }
	;

delete_expr:
	  coloncolon_opt TOK_DELETE cast_expr
	  { $$ = as_expression_delete::create( loc($2), $3, $1 ); }
	| coloncolon_opt TOK_DELETE TOK_LEFT_BRACKET TOK_RIGHT_BRACKET cast_expr
	  { $$ = as_expression_delete_array::create( loc($2), $5, $1 ); }
	;

_member_access_suffix:
	  id_expr
	  { $$ = $1; }
	| _id_expr_dtc
	  { $$ = $1 }
	| TOK_TEMPLATE _hinter_templ_id_follows id_expr
	  { $$ = $3; } //XXX: isn't this one covered by the first rule in this group?
	| TOK_TEMPLATE _hinter_templ_id_follows _id_expr_dtc
	  { $$ = $3; }
	;

// 5.2 Postfix expressions

postfix_expr:
	/* following four are from primary_expr */
	  TOK_LITERAL
	  { $$ = as_literal::create( loc($1), as_literal::UNKNOWN, UNWTOK($1) ); }
	| TOK_THIS
	  { $$ = as_this_expression::create( loc($1) ); }
	| TOK_LEFT_PAR _disambiguate_cast_expr_vs_expr_in_pars TOK_PAD_EXPR_RPAR expr TOK_RIGHT_PAR
	  { $$ = $4; }
	| _id_expr_nontype
	  { $$ = as_name_expression::create( loc($1), $1 ); }

	| postfix_expr TOK_LEFT_BRACKET expr TOK_RIGHT_BRACKET
	  { $$ = as_expression_brackets::create( loc($2), $1, $3 ); }
	| postfix_expr _expr_list_opt_in_pars
	  { $$ = as_expression_function_call::create( loc($2), $1, UNWRAP($2) ); }

	  /* explicit type conversion (functional notation): */
	| _simple_type_spec_non_exclusive _expr_list_opt_in_pars
	  { ptr<as_type_id> tid = EMPTY_TYPE_ID( loc($1), loc($2) );
	    tid->type_specifiers_get()->push_back($1);
	    $$ = as_expression_functional_style_cast::create( loc($1), tid, UNWRAP($2) ); }
	| _simple_type_spec_exclusive _expr_list_opt_in_pars
	  { ptr<as_type_id> tid = EMPTY_TYPE_ID( loc($1), loc($2) );
	    tid->type_specifiers_get()->push_back($1);
	    $$ = as_expression_functional_style_cast::create( loc($1), tid, UNWRAP($2) ); }
	| _typename_type_spec _expr_list_opt_in_pars
	  { ptr<as_type_id> tid = EMPTY_TYPE_ID( loc($1), loc($2) );
	    tid->type_specifiers_get()->push_back($1);
	    $$ = as_expression_functional_style_cast::create( loc($1), tid, UNWRAP($2) ); }

	| postfix_expr TOK_DOT _member_access_suffix
	  { $$ = as_expression_member_access_dot::create( loc($2), $1, $3 ); }
	| postfix_expr TOK_MINUS_GT _member_access_suffix
	  { $$ = as_expression_member_access_arrow::create( loc($2), $1, $3 ); }

	| postfix_expr TOK_DOT pseudo_destr_name
	  { $$ = as_expression_pseudo_destruct_dot::create( loc($1), $1, $3 ); }
	| postfix_expr TOK_MINUS_GT pseudo_destr_name
	  { $$ = as_expression_pseudo_destruct_arrow::create( loc($1), $1, $3 ); }

	| postfix_expr TOK_PLUS_PLUS
	  { $$ = as_expression_plus_plus_post::create( loc($2), $1 ); }
	| postfix_expr TOK_MINUS_MINUS
	  { $$ = as_expression_minus_minus_post::create( loc($2), $1 ); }

	| TOK_DYNAMIC_CAST TOK_LT type_id TOK_GT TOK_LEFT_PAR expr TOK_RIGHT_PAR
	  { $$ = as_expression_dynamic_cast::create( loc($1), $3, $6 ); }
	| TOK_STATIC_CAST TOK_LT type_id TOK_GT TOK_LEFT_PAR expr TOK_RIGHT_PAR
	  { $$ = as_expression_static_cast::create( loc($1), $3, $6 ); }
	| TOK_REINTERPRET_CAST TOK_LT type_id TOK_GT TOK_LEFT_PAR expr TOK_RIGHT_PAR
	  { $$ = as_expression_reinterpret_cast::create( loc($1), $3, $6 ); }
	| TOK_CONST_CAST TOK_LT type_id TOK_GT TOK_LEFT_PAR expr TOK_RIGHT_PAR
	  { $$ = as_expression_const_cast::create( loc($1), $3, $6 ); }

	| TOK_TYPEID TOK_LEFT_PAR _disambiguate_expr_vs_type_id TOK_PAD_EXPR_RPAR expr TOK_RIGHT_PAR
	  { $$ = as_expression_typeid_expr::create( loc($1), $5 ); }
	| TOK_TYPEID TOK_LEFT_PAR _disambiguate_expr_vs_type_id TOK_PAD_TYPE_ID_RPAR type_id TOK_RIGHT_PAR
	  { $$ = as_expression_typeid_type::create( loc($1), $5 ); }

//99	| TOK_LEFT_PAR _disambiguate_expr_vs_type_id TOK_PAD_TYPE_ID_RPAR type_id TOK_RIGHT_PAR TOK_LEFT_BRACE  inizer_list comma_opt  TOK_RIGHT_BRACE //TMA
//99	  { warn("C99"); }
	;

_typename_type_spec:
	  TOK_TYPENAME coloncolon_opt nested_name_spec _tts1
	  { $$ = as_elaborated_type_specifier_typename::create( loc($1), as_name::create(loc($2),$3,$4,true) ); }
	| TOK_TYPENAME coloncolon_opt nested_name_spec _tts2
	  { $$ = as_elaborated_type_specifier_typename::create( loc($1), as_name::create(loc($2),$3,$4,true) ); }
	| TOK_TYPENAME coloncolon_opt nested_name_spec TOK_TEMPLATE _hinter_templ_id_follows _tts2
	  { $$ = as_elaborated_type_specifier_typename::create( loc($1), as_name::create(loc($2),$3,$6,true) ); }
	;

_tts1:
	  _UNKNOWN_ID
	  { $$ = TOK2ID($1); }
	| _NONTYPE
	  { $$ = TOK2ID($1); }
	| _CLASS_NAME_ID
	  { $$ = TOK2ID($1); }
	| _ENUM_NAME_ID
	  { $$ = TOK2ID($1); }
	| _TYPEDEF_NAME_ID
	  { $$ = TOK2ID($1); }
	;

_tts2:
	  _TEMPL_TYPE _templ_args
	  { $$ = as_template_id::create( loc($1), $1, UNWRAP($2) ); }
	| _TEMPL_NONTYPE _templ_args
	  { $$ = as_template_id::create( loc($1), $1, UNWRAP($2) ); }
	| _TEMPL_FORCED _templ_args
	  { $$ = as_template_id::create( loc($1), $1, UNWRAP($2) ); }
	;

_expr_list_opt_in_pars:
	  TOK_LEFT_PAR TOK_RIGHT_PAR
	  { $$ = EMPTY_WRAPPER( as_expression_list, loc($1), as_expression ); }
	| TOK_LEFT_PAR expr_list TOK_RIGHT_PAR
	  { $$ = $2; }
	;

expr_list:
	  assign_expr
	  { NEW_WRAPPER_W1( $$, as_expression_list, loc($1), as_expression, $1 ); }
	| expr_list TOK_COMMA assign_expr
	  { $$ = $1; WRAPPER_APPEND( $$, $3 ); }
	;

_enum_or_typedef_name_id:
	  _ENUM_NAME_ID
	  { $$ = TOK2ID($1); }
	| _TYPEDEF_NAME_ID
	  { $$ = TOK2ID($1); }
	;

/* type-name is a non-class type-name, see [5.2.4/1] */
pseudo_destr_name:
	  _pdn1 TOK_TILDE _enum_or_typedef_name_id
	  { $$ = $1;
	    // make_sure_these_types_are_equal( $$, as_name::create( loc($3), $$->qualification_get(), $3, $$->was_qualified_get() ) );
	    // otherwise the program is ill-formed
	  }

	| coloncolon_opt TOK_TILDE _enum_or_typedef_name_id
	  { $$ = as_name::create( loc($1), $1, $3, $1->was_qualified_get() ); }
	| coloncolon_opt nested_name_spec TOK_TILDE _enum_or_typedef_name_id
	  { $$ = as_name::create( loc($1), $2, $4, true ); }
	;

_pdn1:
	  coloncolon_opt _enum_or_typedef_name_id TOK_COLON_COLON _hinter_descend_pseudo_destr
	  { $$ = as_name::create( loc($1), $1, $2, $1->was_qualified_get() ); }
	| coloncolon_opt nested_name_spec _enum_or_typedef_name_id TOK_COLON_COLON _hinter_descend_pseudo_destr
	  { $$ = as_name::create( loc($1), $2, $3, true ); }
	;

id_expr:
          coloncolon_opt unqualified_id
	  { $2->was_qualified_set( $1->was_qualified_get() ); $$ = $2; }
	| coloncolon_opt nested_name_spec unqualified_id
	  { $3->was_qualified_set(true); $$ = $3; }
	| coloncolon_opt nested_name_spec TOK_TEMPLATE _hinter_templ_id_follows unqualified_id
	  { $5->was_qualified_set(true); $$ = $5; }
	;

unqualified_id:
	  _CLASS_NAME_ID
	  { $$ = as_name::create( loc($1), CURR_CTX(loc($1)), TOK2ID($1) ); }
	| _ENUM_NAME_ID
	  { $$ = as_name::create( loc($1), CURR_CTX(loc($1)), TOK2ID($1) ); }
	| _TYPEDEF_NAME_ID
	  { $$ = as_name::create( loc($1), CURR_CTX(loc($1)), TOK2ID($1) ); }
/* these are handled separately
	| TOK_TILDE _CLASS_NAME_ID
	  { $$ = as_name::create( loc($1), CURR_CTX(loc($1)), destructor_id_token(loc($1),TOK2ID($2)) ); }
	| TOK_TILDE _TEMPL_TYPE _templ_args
	  { $$ = as_name::create( loc($1), CURR_CTX(loc($1)), as_destructor_id_template(loc($1),as_template_id::create(loc($2),$2,UNWRAP($3))) ); }
	| conv_func_id
	  { $$ = $1; }
 */
	| _TEMPL_TYPE _templ_args
	  { $$ = as_name::create( loc($1), CURR_CTX(loc($1)), as_template_id::create(loc($1),$1,UNWRAP($2)) ); }
	| _TEMPL_TYPE
	  { $$ = as_name::create( loc($1), CURR_CTX(loc($1)), as_template_id::create(loc($1),$1,NULL) ); }
	| _UNKNOWN_ID
	  { $$ = as_name::create( loc($1), CURR_CTX(loc($1)), TOK2ID($1) ); }
	| _NONTYPE
	  { $$ = as_name::create( loc($1), CURR_CTX(loc($1)), TOK2ID($1) ); }
	| _NMSPC_NAME
	  { $$ = as_name::create( loc($1), CURR_CTX(loc($1)), TOK2ID($1) ); }
	| op_func_id
	  { $$ = $1; }
	| _TEMPL_NONTYPE _templ_args
	  { $$ = as_name::create( loc($1), CURR_CTX(loc($1)), as_template_id::create(loc($1),$1,UNWRAP($2)) ); }
	| _TEMPL_NONTYPE
	  { $$ = as_name::create( loc($1), CURR_CTX(loc($1)), as_template_id::create(loc($1),$1,NULL) ); }
	| _TEMPL_FORCED _templ_args
	  { $$ = as_name::create( loc($1), CURR_CTX(loc($1)), as_template_id::create(loc($1),$1,UNWRAP($2)) ); }
	;

_id_expr_nontype:
	  coloncolon_opt _unqualified_id_nontype
	  { $2->was_qualified_set( $1->was_qualified_get() ); $$ = $2; }
	| coloncolon_opt nested_name_spec _unqualified_id_nontype
	  { $3->was_qualified_set(true); $$ = $3; }
	| coloncolon_opt nested_name_spec TOK_TEMPLATE _hinter_templ_id_follows _unqualified_id_nontype
	  { $5->was_qualified_set(true); $$ = $5; }
	;

_unqualified_id_nontype:
	  _NONTYPE
	  { $$ = as_name::create( loc($1), CURR_CTX(loc($1)), TOK2ID($1) ); }
	| _UNKNOWN_ID
	  {
		/* ill-formed; error is reported in semantic analysis */
		$$ = as_name::create( loc($1), CURR_CTX(loc($1)), TOK2ID($1) );
	  }
	  /* note that we DO allow conv_func_id here: */
	| conv_func_id
	  { $$ = $1; }
	| op_func_id
	  { $$ = $1; }
	| _TEMPL_NONTYPE _templ_args
	  { $$ = as_name::create( loc($1), CURR_CTX(loc($1)), as_template_id::create(loc($1),$1,UNWRAP($2)) ); }
	| _TEMPL_NONTYPE
	  { $$ = as_name::create( loc($1), CURR_CTX(loc($1)), as_template_id::create(loc($1),$1,NULL) ); }
	| _TEMPL_FORCED _templ_args
	  { $$ = as_name::create( loc($1), CURR_CTX(loc($1)), as_template_id::create(loc($1),$1,UNWRAP($2)) ); }
	;

/* this one is the other way round in the norm */
nested_name_spec:
	  _CLASS_NAME_ID TOK_COLON_COLON
	  {
	  	$$ = as_name::create( loc($1), CURR_CTX(loc($1)), TOK2ID($1) );
		hinter::qual_scope_set( $1->found_decls_get()->begin()->dncast<ss_structure_declaration>()->body_get() );
	  }
	| _TEMPL_TYPE _templ_args TOK_COLON_COLON
	  {
		$$ = as_name::create( loc($1), CURR_CTX(loc($1)), as_template_id::create(loc($1),$1,UNWRAP($2)) );
	  }
	| _TEMPL_FORCED _templ_args TOK_COLON_COLON
	  {
		$$ = as_name::create( loc($1), CURR_CTX(loc($1)), as_template_id::create(loc($1),$1,UNWRAP($2)) );
	  }
	| _NMSPC_NAME TOK_COLON_COLON
	  {
		$$ = as_name::create( loc($1), CURR_CTX(loc($1)), TOK2ID($1) );
		hinter::qual_scope_set( $1->found_decls_get()->begin()->dncast<ss_namespace_definition>()->body_get() );
	  }

	| nested_name_spec _CLASS_NAME_ID TOK_COLON_COLON
	  {
		$$ = as_name::create( loc($1), $1, TOK2ID($2), true );
		hinter::qual_scope_set( $2->found_decls_get()->begin()->dncast<ss_structure_declaration>()->body_get() );
	  }
	| nested_name_spec _TEMPL_TYPE _templ_args TOK_COLON_COLON
	  {
		$$ = as_name::create( loc($1), $1, as_template_id::create(loc($2),$2,UNWRAP($3)), true );
	  }
	| nested_name_spec _NMSPC_NAME TOK_COLON_COLON
	  {
		$$ = as_name::create( loc($1), $1, TOK2ID($2), true );
		hinter::qual_scope_set( $2->found_decls_get()->begin()->dncast<ss_namespace_definition>()->body_get() );
	  }
	| nested_name_spec TOK_TEMPLATE _hinter_templ_id_follows _TEMPL_FORCED _templ_args TOK_COLON_COLON
	  {
		$$ = as_name::create( loc($1), $1, as_template_id::create(loc($4),$4,UNWRAP($5)), true );
	  }
	;

op_func_id:
	  _op_func_id_orig TOK_PAD_OP_NOT_TEMPL
	  { $$ = as_name::create( loc($1), CURR_CTX(loc($1)), $1 ); }
	/* only in the new norm: */
	| _op_func_id_orig TOK_PAD_TEMPL_OP _templ_args
	  { $1->arguments_set( UNWRAP($3) );
	    $$ = as_name::create( loc($1), CURR_CTX(loc($1)), $1 ); }

	/* dummy rules to catch conflicts */
	| _op_func_id_orig		%prec _LOWEST_PREC
	  {
		/* look-ahead was surely read, see the following rule :) */
		NO_PAD_TOKEN_BUG( loc(manager::prev_yylex()) );
	  }
	| _op_func_id_orig TOK_LT
	  { NO_PAD_TOKEN_BUG( loc($2) ); }
	;

_op_func_id_orig:
	  TOK_OPERATOR op _hinter_clear_qualification
	  { $$ = as_op_function_id::create( loc($1), UNWTOK($2), NULL );
	    /* we cannot be sure about the lookahead token here, because
	     * when parsing operator new, we have to look ahead for a bracket;
	     * when parsing operator +, we do not
	     */
	    if (yychar != YYEMPTY)
		clear_lookahead();
	    int next_val = manager::peek()->type_get();
	    manager::start( TOK_START_TO_BE_DISCARDED );
	    /* TODO: inspect the operator whether it is templated, or not. change 'false' in the condition below then */
	    if (false && next_val == TOK_LT)
		manager::commit( TOK_PAD_TEMPL_OP );
	    else
		manager::commit( TOK_PAD_OP_NOT_TEMPL );
	  }
	;

op:
	  TOK_NEW			%prec _LOWEST_PREC
	| TOK_DELETE			%prec _LOWEST_PREC
	| TOK_NEW TOK_LEFT_BRACKET TOK_RIGHT_BRACKET
	| TOK_DELETE TOK_LEFT_BRACKET TOK_RIGHT_BRACKET
	| TOK_PLUS
	| TOK_MINUS
	| TOK_STAR
	| TOK_SLASH
	| TOK_PERCENT
	| TOK_HAT
	| TOK_AMP
	| TOK_VBAR
	| TOK_TILDE
	| TOK_EXCLAMATION
	| TOK_EQ
	| TOK_PLUS_EQ
	| TOK_MINUS_EQ
	| TOK_STAR_EQ
	| TOK_SLASH_EQ
	| TOK_PERCENT_EQ
	| TOK_HAT_EQ
	| TOK_AMP_EQ
	| TOK_VBAR_EQ
	| TOK_GT_GT_EQ
	| TOK_LT_LT_EQ
	| TOK_LT
	| TOK_GT
	| TOK_LT_EQ
	| TOK_GT_EQ
	| TOK_LT_LT
	| TOK_GT_GT
	| TOK_EQ_EQ
	| TOK_EXCLAMATION_EQ
	| TOK_AMP_AMP
	| TOK_VBAR_VBAR
	| TOK_PLUS_PLUS
	| TOK_MINUS_MINUS
	| TOK_COMMA
	| TOK_MINUS_GT_STAR
	| TOK_MINUS_GT
	| TOK_LEFT_PAR TOK_RIGHT_PAR
	| TOK_LEFT_BRACKET TOK_RIGHT_BRACKET
	;

conv_func_id:
	  TOK_OPERATOR _hinter_clear_qualification _type_spec_seq_not_declaring_class_enum 			%prec _LOWEST_PREC
	  { $$ = as_name::create( loc($1), CURR_CTX(loc($3)), as_conv_function_id::create( loc($1), UNWRAP($3), EMPTY_LIST(as_declarator_op) ) ); }
	| TOK_OPERATOR _hinter_clear_qualification _type_spec_seq_not_declaring_class_enum _ptr_op_seq		%prec _LOWEST_PREC
	  { $$ = as_name::create( loc($1), CURR_CTX(loc($3)), as_conv_function_id::create( loc($1), UNWRAP($3), UNWRAP($4) ) ); }
	;

mem_inizer_list_opt:
	  _no_token
	  { $$ = EMPTY_WRAPPER( as_mem_initializer_list, loc($1), as_mem_initializer ); }
	| TOK_COLON mem_inizer_list
	  { $$ = $2; }
	;

mem_inizer_list:
	  mem_inizer
	  { NEW_WRAPPER_W1( $$, as_mem_initializer_list, loc($1), as_mem_initializer, $1 ); }
	| mem_inizer_list TOK_COMMA mem_inizer
	  { $$ = $1; WRAPPER_APPEND( $$, $3 ); }
	;

/* [12.6.2/2] */
mem_inizer:
	  _NONTYPE _expr_list_opt_in_pars
	  { $$ = as_mem_initializer::create( loc($1), as_name::create(loc($1),CURR_CTX(loc($1)),TOK2ID($1)), UNWRAP($2) ); }
	| _TYPEDEF_NAME_ID _expr_list_opt_in_pars
	  { $$ = as_mem_initializer::create( loc($1), as_name::create(loc($1),CURR_CTX(loc($1)),TOK2ID($1)), UNWRAP($2) ); }
	| coloncolon_opt _CLASS_NAME_ID _expr_list_opt_in_pars
	  { $$ = as_mem_initializer::create( loc($1), as_name::create(loc($1),$1,TOK2ID($2),$1->was_qualified_get()), UNWRAP($3) ); }
	| coloncolon_opt _TEMPL_TYPE _templ_args _expr_list_opt_in_pars
	  { $$ = as_mem_initializer::create( loc($1), as_name::create(loc($1),$1,as_template_id::create(loc($2),$2,UNWRAP($3)),$1->was_qualified_get()), UNWRAP($4) ); }
	| coloncolon_opt nested_name_spec _CLASS_NAME_ID _expr_list_opt_in_pars
	  { $$ = as_mem_initializer::create( loc($1), as_name::create(loc($1),$2,TOK2ID($3),true), UNWRAP($4) ); }
	| coloncolon_opt nested_name_spec _TEMPL_TYPE _templ_args _expr_list_opt_in_pars
	  { $$ = as_mem_initializer::create( loc($1), as_name::create(loc($1),$2,as_template_id::create(loc($3),$3,UNWRAP($4)),true), UNWRAP($5) ); }
	;

// 7 Declarations

_decl_seq_in_nmspc_scope_opt:
	  _no_token
	  { $$ = EMPTY_WRAPPER( as_declaration_seq, loc($1), as_declaration ); }
	| _decl_seq_in_nmspc_scope
	  { $$ = $1; }
	;

_decl_seq_in_nmspc_scope:
	  _decl_in_nmspc_scope
	  { NEW_WRAPPER_W1( $$, as_declaration_seq, loc($1), as_declaration, $1 ); }
	| _decl_seq_in_nmspc_scope _decl_in_nmspc_scope
	  { $$ = $1; WRAPPER_APPEND( $$, $2 ); }
	;

/*
 * this is needed to find out whether the decl_spec_seq could be a ctor-like one
 * example: "A::A" could, "A::B" could not; it is up to the called function to decide
 */
_decl_spec_seq_in_nmspc_scope:
	  _decl_spec_seq_orig _lookahead_assert_non_empty
	  {
		clear_lookahead();
		disambiguate_ctor_nmspc_scope( $1 );
		$$ = $1;
		sa_parser_action_logger << "decl spec seq in namespace scope\n" << msg::eolog;
		sa_simple_declaration::instance()->save_declaration_specifiers($1);
	  }
	;

_class_forward_id:
	  /* maybe not entirely true, XXX */
	  _anyid
	  { $$ = $1; }
	;

_decl_in_nmspc_scope:
	  _block_decl_in_nmspc_scope
	  { $$ = $1; }
	| _func_defn_in_nmspc_scope
	  { $$ = $1; }
	| nmspc_defn
	  { $$ = $1; }

	/* template declaration */
	| TOK_TEMPLATE TOK_LT templ_param_list TOK_GT _decl_in_nmspc_scope
	  { $$ = as_template_declaration::create( loc($1), NULL, UNWRAP($3), $5 ); }
	| TOK_EXPORT TOK_TEMPLATE TOK_LT templ_param_list TOK_GT _decl_in_nmspc_scope
	  { $$ = as_template_declaration::create( loc($2), UNWTOK($1), UNWRAP($4), $6 ); }

	/* explicit instantiation */
	| TOK_TEMPLATE _decl_in_nmspc_scope
	  { $$ = as_explicit_instantiation::create( loc($1), $2 ); }
	
	/*
	 * explicit specialization
	 * we do the disambiguation to eliminate reading the look-ahead token
	 * the same disambiguation is used deep inside templ_param_list
	 */
	| TOK_TEMPLATE TOK_LT _disambiguate_templ_param TOK_GT _decl_in_nmspc_scope
	  { $$ = as_explicit_specialization::create( loc($1), $5 ); }
	
	/* linkage specification */
	| TOK_EXTERN TOK_LITERAL TOK_LEFT_BRACE _decl_seq_in_nmspc_scope_opt TOK_RIGHT_BRACE
	  { $$ = as_linkage_specification::create( loc($1), UNWTOK($2), UNWRAP($4) ); }
	| TOK_EXTERN TOK_LITERAL _decl_in_nmspc_scope
	  { ptr<as_linkage_specification> ls = as_linkage_specification::create( loc($1), UNWTOK($2), EMPTY_LIST(as_declaration) );
	    LIST_APPEND( ls->declarations_get(), $3 );
	    $$ = ls; }
	;

_block_decl_in_nmspc_scope:
	  TOK_SEMICOLON
	  { $$ = as_simple_declaration::create( loc($1), EMPTY_LIST(as_declaration_specifier), EMPTY_LIST(as_init_declarator) ); }
	| _simple_decl_in_nmspc_scope
	  { $$ = $1; }

	| TOK_ASM TOK_LEFT_PAR TOK_LITERAL TOK_RIGHT_PAR TOK_SEMICOLON
	  { $$ = as_asm_declaration::create( loc($1), UNWTOK($3) ); }

	/*
	 * namespace alias definition
	 * See [7.3.2/3] for an example of well-formed code with a known id after TOK_NAMESPACE.
	 */
	| TOK_NAMESPACE _anyid TOK_EQ _nested_nmspc_name TOK_SEMICOLON
	  {
		$$ = as_namespace_alias_declaration::create( loc($2), $2, $4 );
	  }

	| using_decl
	  {
		$$ = $1;
		sa_usings::instance()->process_declaration($1);
	  }
	/* using directive */
	| TOK_USING TOK_NAMESPACE _nested_nmspc_name TOK_SEMICOLON
	  { $$ = as_using_directive::create( loc($1), $3 ); }

	| class_key _class_forward_id TOK_SEMICOLON
	  {
		ptr<as_class_specifier> cs;
		cs = as_class_specifier::create(
			loc($1),
			$1,
			as_name::create(
				loc($2),
				CURR_CTX(loc($2)),
				$2 ),
			EMPTY_LIST(as_base_specifier),
			EMPTY_LIST(as_member_declaration));
		$$ = as_class_forward_declaration::create( loc($1), $1, $2 );
		sa_class_declaration::create()->process_forward_declaration(cs);
	  }
	;

_block_decl_in_func_scope:
	  _simple_decl_in_func_scope
	  { $$ = $1; }
	/* asm definition */
	| TOK_ASM TOK_LEFT_PAR TOK_LITERAL TOK_RIGHT_PAR TOK_SEMICOLON
	  { $$ = as_asm_declaration::create( loc($1), UNWTOK($3) ); }

	/*
	 * namespace alias definition -- here??? XXX
	 * See [7.3.2/3] for an example of well-formed code with a known id after TOK_NAMESPACE.
	 */
	| TOK_NAMESPACE _anyid TOK_EQ _nested_nmspc_name TOK_SEMICOLON
	  {
		$$ = as_namespace_alias_declaration::create( loc($2), $2, $4 );
	  }

	| using_decl
	  {
		$$ = $1;
		sa_usings::instance()->process_declaration($1);
	  }
	/* using directive */
	| TOK_USING TOK_NAMESPACE _nested_nmspc_name TOK_SEMICOLON
	  { $$ = as_using_directive::create( loc($1), $3 ); }

	| class_key _class_forward_id TOK_SEMICOLON
	  { $$ = as_class_forward_declaration::create( loc($1), $1, $2 ); }
	;

/* qualified-namespace-specifier (including the optional ::), see [7.3.2/2], [3.4.6] */
_nested_nmspc_name:
	  _hinter_nmspc_name_follows coloncolon_opt _NMSPC_NAME
	  { $$ = as_name::create( loc($2), $2, TOK2ID($3), $2->was_qualified_get() ); }
	| _hinter_nmspc_name_follows coloncolon_opt _nested_nmspc_spec _NMSPC_NAME
	  { $$ = as_name::create( loc($2), $3, TOK2ID($4), true ); }
	;

_nested_nmspc_spec:
	  _NMSPC_NAME TOK_COLON_COLON _hinter_nmspc_name_follows
	  {
		$$ = as_name::create( loc($1), CURR_CTX(loc($1)), TOK2ID($1) );
		hinter::qual_scope_set( $1->found_decls_get()->begin()->dncast<ss_namespace_definition>()->body_get() );
	  }
	| _nested_nmspc_spec _NMSPC_NAME TOK_COLON_COLON _hinter_nmspc_name_follows
	  {
		$$ = as_name::create( loc($1), $1, TOK2ID($2), true );
		hinter::qual_scope_set( $2->found_decls_get()->begin()->dncast<ss_namespace_definition>()->body_get() );
	  }
	;

_simple_decl_in_nmspc_scope:
	  _decl_spec_seq_in_nmspc_scope init_dcltor_list TOK_SEMICOLON
	  { $$ = as_simple_declaration::create( loc($1), UNWRAP($1), UNWRAP($2) ); }
	  /* init_declarator_list is omitted here */
	| _type_spec_declaring_class_enum TOK_SEMICOLON
	  { ptr<as_simple_declaration> sd = as_simple_declaration::create( loc($1), EMPTY_LIST(as_declaration_specifier), EMPTY_LIST(as_init_declarator) );
	    LIST_APPEND( sd->declaration_specifiers_get(), $1 );
	    $$ = sd; }
	;

_simple_decl_in_func_scope:
	  _decl_spec_seq_in_func_scope init_dcltor_list TOK_SEMICOLON
	  { $$ = as_simple_declaration::create( loc($1), UNWRAP($1), UNWRAP($2) ); }
	| _type_spec_declaring_class_enum TOK_SEMICOLON
	  { ptr<as_simple_declaration> sd = as_simple_declaration::create( loc($1), EMPTY_LIST(as_declaration_specifier), EMPTY_LIST(as_init_declarator) );
	    LIST_APPEND( sd->declaration_specifiers_get(), $1 );
	    $$ = sd; }
	;

/*
 * [7/7] init_dcltor_list without decl_spec can only be:
 *   constructors, destructors and type conversions
 */

_next_member_dcltor_list_cdtc:
	  _cdtc_next_member_dcltor
	  { NEW_WRAPPER_W1( $$, as_member_declarator_list, loc($1), as_member_declarator, $1 ); }
	| _next_member_dcltor_list_cdtc TOK_COMMA _cdtc_next_member_dcltor
	  { $$ = $1; WRAPPER_APPEND( $$, $3 ); }
	;

// 7.1 Specifiers

/* at namespace and class scope, these behave differently a bit */
_decl_spec_seq_in_func_scope:
	  _decl_spec_seq_orig
	  {
		$$ = $1;
		sa_parser_action_logger << "decl spec seq in function scope\n" << msg::eolog;
		sa_simple_declaration::instance()->save_declaration_specifiers($1);
	  }
	;

_decl_spec_seq_in_param_scope:
	  _decl_spec_seq_orig
	  {
		$$ = $1;
	  }
	;

/* at most/least one type_spec is allowed in decl_spec_seq [7.1.5/1,2] */
_decl_spec_seq_orig:
	  _decl_spec_seq_exclusive
	  { $$ = $1; }
	| _decl_spec_seq_non_exclusive
	  { $$ = $1; }
	;

/*
 * exclusive type specifier is one that cannot be combined with another one
 * non-exclusive ones can be combined to form another type-specifier [7.1.5/1]
 * 
 * examples:
 *   "class a", "b", "typename c" are exclusive
 *   "int", "unsigned", "long" are non-exclusive (you can form "unsigned int")
 *
 * note that deciding which of the non-exclusive combinations are valid
 *   is up to the semantic level
 */

/*
 * declaration specifier sequence containing an exclusive specifier
 * it may also contain "other" specifiers (eg. storage class) before and/or
 *   after the exclusive one
 */
_decl_spec_seq_exclusive:
	  _decl_spec_exclusive
	  { NEW_WRAPPER_W1( $$, as_declaration_specifier_seq, loc($1), as_declaration_specifier, $1 ); }
	
	| _decl_spec_seq_other _decl_spec_exclusive
	  { $$ = $1; WRAPPER_APPEND( $$, $2 ); }

	| _decl_spec_seq_exclusive _decl_spec_other
	  { $$ = $1; WRAPPER_APPEND( $$, $2 ); }
	;

/*
 * declaration specifier sequence containing non-exclusive and other specifiers
 * note that these are intermixed; semantic level should care
 */
_decl_spec_seq_non_exclusive:
	  _decl_spec_non_exclusive
	  { NEW_WRAPPER_W1( $$, as_declaration_specifier_seq, loc($1), as_declaration_specifier, $1 ); }
	| _decl_spec_seq_other _decl_spec_non_exclusive
	  { $$ = $1; WRAPPER_APPEND( $$, $2 ); }

	| _decl_spec_seq_non_exclusive _decl_spec_non_exclusive
	  { $$ = $1; WRAPPER_APPEND( $$, $2 ); }
	| _decl_spec_seq_non_exclusive _decl_spec_other
	  { $$ = $1; WRAPPER_APPEND( $$, $2 ); }
	;

_decl_spec_seq_other:
	  _decl_spec_other
	  { NEW_WRAPPER_W1( $$, as_declaration_specifier_seq, loc($1), as_declaration_specifier, $1 ); }
	| _decl_spec_seq_other _decl_spec_other
	  { $$ = $1; WRAPPER_APPEND( $$, $2 ); }
	;

_decl_spec_exclusive:
	  _type_spec_not_declaring_class_enum_exclusive
	  { $$ = $1; }
	| _type_spec_declaring_class_enum
	  { $$ = $1; }
	;

_decl_spec_non_exclusive:
	  _simple_type_spec_non_exclusive
	  { $$ = $1; }
	;

_decl_spec_other:
	  storage_class_spec
	  { $$ = $1; }
	| cv_qualifier
	  { $$ = $1; }
	| func_spec
	  { $$ = $1; }
	| TOK_FRIEND
	  { $$ = as_friend_specifier::create( loc($1) ); }
	| TOK_TYPEDEF
	  { $$ = as_typedef_specifier::create( loc($1) ); }
	;

// 7.1.1 Storage class specifiers

storage_class_spec:
	  TOK_AUTO
	  { $$ = as_storage_class_specifier_auto::create( loc($1) ); }
	| TOK_REGISTER
	  { $$ = as_storage_class_specifier_register::create( loc($1) ); }
	| TOK_STATIC
	  { $$ = as_storage_class_specifier_static::create( loc($1) ); }
	| TOK_EXTERN
	  { $$ = as_storage_class_specifier_extern::create( loc($1) ); }
	| TOK_MUTABLE
	  { $$ = as_storage_class_specifier_mutable::create( loc($1) ); }
	;

// 7.1,2 Function specifiers

func_spec:
	  TOK_INLINE
	  { $$ = as_function_specifier_inline::create( loc($1) ); }
	| TOK_VIRTUAL
	  { $$ = as_function_specifier_virtual::create( loc($1) ); }
	| TOK_EXPLICIT
	  { $$ = as_function_specifier_explicit::create( loc($1) ); }
	;

_enum:
	  TOK_ENUM
	  { $$ = $1; hinter::elab_spec_set( elab_spec_enum::create(loc($1)) ); }
	;

// 7.1.5 Type specifiers

/*
 * torso of type-specifier [7.1.5/1]
 * elaborated type-specifier is emerged here
 * part of class-head is also emerged here
 * cv_qualifier is pushed upwards
 *
 * "not declaring" means that it lacks class- and enum-specifiers
 */
_type_spec_not_declaring_class_enum_exclusive:
	  _simple_type_spec_exclusive
	  { $$ = $1; }
	/* elaborated_type_spec begin */
	| class_key TOK_COLON_COLON _hinter_to_root_scope _CLASS_NAME_ID
	  { $$ = as_elaborated_type_specifier_class_key::create( loc($1), as_name::create(loc($2),GLOB_CTX(loc($2)),TOK2ID($4),true), $1 ); }
	| class_key TOK_COLON_COLON _hinter_to_root_scope nested_name_spec _CLASS_NAME_ID
	  { $$ = as_elaborated_type_specifier_class_key::create( loc($1), as_name::create(loc($2),$4,TOK2ID($5),true), $1 ); }
	| _enum _ENUM_NAME_ID
	  { $$ = as_elaborated_type_specifier_enum::create( loc($1), as_name::create(loc($2),CURR_CTX(loc($2)),TOK2ID($2)) ); }
	| _enum nested_name_spec _ENUM_NAME_ID
	  { $$ = as_elaborated_type_specifier_enum::create( loc($1), as_name::create(loc($2),$2,TOK2ID($3),true) ); }
	| _enum TOK_COLON_COLON _hinter_to_root_scope _ENUM_NAME_ID
	  { $$ = as_elaborated_type_specifier_enum::create( loc($1), as_name::create(loc($2),GLOB_CTX(loc($2)),TOK2ID($4),true) ); }
	| _enum TOK_COLON_COLON _hinter_to_root_scope nested_name_spec _ENUM_NAME_ID
	  { $$ = as_elaborated_type_specifier_enum::create( loc($1), as_name::create(loc($2),$4,TOK2ID($5),true) ); }
	| _typename_type_spec
	  { $$ = $1; }
	/* end */
	/* class-head portion */
	| class_key _CLASS_NAME_ID
	  { $$ = as_elaborated_type_specifier_class_key::create( loc($1), as_name::create(loc($2),CURR_CTX(loc($2)),TOK2ID($2)), $1 ); }
	| class_key nested_name_spec _CLASS_NAME_ID
	  { $$ = as_elaborated_type_specifier_class_key::create( loc($1), as_name::create(loc($2),$2,TOK2ID($3),true), $1 ); }
	;

_anyid_opt:
	  _no_token
	  { $$ = as_anyid_opt_bearer::create( loc($1), NULL ); }
	| _anyid
	  { $$ = as_anyid_opt_bearer::create( loc($1), $1 ); }
	;

_anyid:
	  _UNKNOWN_ID
	  { $$ = TOK2ID($1); }
	| _NONTYPE
	  { $$ = TOK2ID($1); }
	| _CLASS_NAME_ID
	  { $$ = TOK2ID($1); }
	| _ENUM_NAME_ID
	  { $$ = TOK2ID($1); }
	| _TYPEDEF_NAME_ID
	  { $$ = TOK2ID($1); }
	| _TEMPL_NONTYPE
	  { $$ = TOK2ID($1); }
	| _TEMPL_TYPE
	  { $$ = TOK2ID($1); }
	| _NMSPC_NAME
	  { $$ = TOK2ID($1); }
	;

/*
 * When in disambiguation, behaves just like _pack_body.
 * When parsing for real (ie. not in disambiguation), *unpacks* the previously packed tokens.
 *
 * Used to skip parsing class/enum bodies when doing disambiguation.
 *
 * Rationale: During disambiguation declaration tables are not being filled, so
 * doing lookup in the bodies would not be possible. Current approach also
 * speeds up the parsing: the bodies are parsed only once - for real.
 *
 * Note: As a speed-up, we could (elsewhere) immediately commit to declaration
 * when we see enum or class keyword. This would eliminate the need to pack and
 * unpack here.
 */
_pack_body_in_disambiguation:
	  _lookahead_assert_empty
	  {
		int next_type = manager::peek()->type_get();
		if (manager::in_disambiguation()) {
			lassert2( !is_pack(next_type),
				"Trying to pack a pack, forgot to unpack when rolling back?" );
	  		manager::pack( TOK_PACK_BODY, loc(manager::prev_yylex()) );
		} else if (is_pack(next_type)) {
			// parsing for real, have to unpack the body
			// another possibility _would_ be to spawn a manager on the pack
			lassert2( next_type == TOK_PACK_BODY, "Different pack type found!" );
			manager::unpack();
		} else {
			// parsing the body without previous disambiguation (which would imply packing). good.
		}
	  }
	;

_type_spec_declaring_class_enum:
	  _class_specifier_action TOK_LEFT_BRACE _pack_body_in_disambiguation TOK_PACK_BODY TOK_RIGHT_BRACE
	  {
		/* always in disambiguation here */
		$$ = $1;
		/* the actual value is irrelevant */
		sa_parser_action_logger
			<< "after _class_specifier_action: disambiguating\n"
			<< ::lestes::msg::eolog;
	  }
	| _class_specifier_action TOK_LEFT_BRACE _pack_body_in_disambiguation member_spfn_opt TOK_RIGHT_BRACE
	  {
		/* never in disambiguation here */
		ptr<as_class_specifier> cs = $1;
		LIST_APPEND_LIST( cs->member_declarations_get(), UNWRAP($4) );
		$$ = cs;
		sa_parser_action_logger
			<< "after _class_specifier_action: parsing\n"
			<< ::lestes::msg::eolog;
		sa_class_declaration::create()->process_end(loc($5));
	  }

	| _enum _anyid_opt TOK_LEFT_BRACE _pack_body_in_disambiguation TOK_PACK_BODY TOK_RIGHT_BRACE
	  // value set to $$ is irrelevant, but has to be of correct type
	  { $$ = as_enumeration_specifier::create( loc($1), NULL, EMPTY_LIST(as_enumerator_definition) ); }
	| _enum _anyid_opt TOK_LEFT_BRACE _pack_body_in_disambiguation enumerator_list_opt TOK_RIGHT_BRACE
	  { $$ = as_enumeration_specifier::create( loc($1), $2->value_get() ? as_name::create(loc($2),CURR_CTX(loc($2)),$2->value_get()) : ptr<as_name>(NULL), UNWRAP($5) ); }
	;

_class_specifier_action:
	  _class_specifier_head
	  { $$ = $1; sa_class_declaration::create()->process($1); }
	;

_class_specifier_head:
	  class_key _no_token
	  { $$ = as_class_specifier::create( loc($1), $1, as_name::create( loc($2), CURR_CTX(loc($2)), EMPTY_ID(loc($2)) ), EMPTY_LIST(as_base_specifier), EMPTY_LIST(as_member_declaration) ); }
	| class_key TOK_COLON base_spec_list
	  { $$ = as_class_specifier::create( loc($1), $1, as_name::create( loc($2), CURR_CTX(loc($2)), EMPTY_ID(loc($2)) ), UNWRAP($3), EMPTY_LIST(as_member_declaration) ); }

	| class_key _anyid
	  { $$ = as_class_specifier::create( loc($1), $1, as_name::create( loc($2), CURR_CTX(loc($2)), $2 ), EMPTY_LIST(as_base_specifier), EMPTY_LIST(as_member_declaration) ); }
	| class_key nested_name_spec _anyid
	  { $$ = as_class_specifier::create( loc($1), $1, as_name::create( loc($2), $2, $3, true ), EMPTY_LIST(as_base_specifier), EMPTY_LIST(as_member_declaration) ); }
	| class_key _anyid TOK_COLON base_spec_list
	  { $$ = as_class_specifier::create( loc($1), $1, as_name::create( loc($2), CURR_CTX(loc($2)), $2 ), UNWRAP($4), EMPTY_LIST(as_member_declaration) ); }
	| class_key nested_name_spec _anyid TOK_COLON base_spec_list
	  { $$ = as_class_specifier::create( loc($1), $1, as_name::create( loc($2), $2, $3, true ), UNWRAP($5), EMPTY_LIST(as_member_declaration) ); }

	| class_key _TEMPL_TYPE _templ_args
	  { $$ = as_class_specifier::create( loc($1), $1, as_name::create( loc($2), CURR_CTX(loc($2)), as_template_id::create(loc($2),$2,UNWRAP($3)) ), EMPTY_LIST(as_base_specifier), EMPTY_LIST(as_member_declaration) ); }
	| class_key _TEMPL_TYPE _templ_args TOK_COLON base_spec_list
	  { $$ = as_class_specifier::create( loc($1), $1, as_name::create( loc($2), CURR_CTX(loc($2)), as_template_id::create(loc($2),$2,UNWRAP($3)) ), UNWRAP($5), EMPTY_LIST(as_member_declaration) ); }
	| class_key nested_name_spec _TEMPL_TYPE _templ_args
	  { $$ = as_class_specifier::create( loc($1), $1, as_name::create( loc($2), $2, as_template_id::create(loc($3),$3,UNWRAP($4)), true ), EMPTY_LIST(as_base_specifier), EMPTY_LIST(as_member_declaration) ); }
	| class_key nested_name_spec _TEMPL_TYPE _templ_args TOK_COLON base_spec_list
	  { $$ = as_class_specifier::create( loc($1), $1, as_name::create( loc($2), $2, as_template_id::create(loc($3),$3,UNWRAP($4)), true ), UNWRAP($6), EMPTY_LIST(as_member_declaration) ); }

/* these are probably impossible, it would be a specialization of function template(?):
	| class_key _TEMPL_NONTYPE _templ_args
	  { $$ = as_class_specifier::create( loc($1), $1, as_name::create( loc($2), CURR_CTX(loc($2)), as_template_id::create(loc($2),$2,UNWRAP($3)) ), EMPTY_LIST(as_base_specifier), EMPTY_LIST(as_member_declaration) ); }
	| class_key _TEMPL_NONTYPE _templ_args TOK_COLON base_spec_list
	  { $$ = as_class_specifier::create( loc($1), $1, as_name::create( loc($2), CURR_CTX(loc($2)), as_template_id::create(loc($2),$2,UNWRAP($3)) ), UNWRAP($5), EMPTY_LIST(as_member_declaration) ); }
	| class_key nested_name_spec _TEMPL_NONTYPE _templ_args
	  { $$ = as_class_specifier::create( loc($1), $1, as_name::create( loc($2), $2, as_template_id::create(loc($3),$3,UNWRAP($4)), true ), EMPTY_LIST(as_base_specifier), EMPTY_LIST(as_member_declaration) ); }
	| class_key nested_name_spec _TEMPL_NONTYPE _templ_args TOK_COLON base_spec_list
	  { $$ = as_class_specifier::create( loc($1), $1, as_name::create( loc($2), $2, as_template_id::create(loc($3),$3,UNWRAP($4)), true ), UNWRAP($6), EMPTY_LIST(as_member_declaration) ); }
 */
	;

// 7.1.5.1 The cv-qualifiers

cv_qualifier:
	  TOK_CONST
	  { $$ = as_cv_qualifier_const::create( loc($1) ); }
	| TOK_VOLATILE
	  { $$ = as_cv_qualifier_volatile::create( loc($1) ); }
//99	| TOK_RESTRICT
//99	  { warn("C99"); }
	;

cv_qualifier_seq:
	  cv_qualifier
	  { NEW_WRAPPER_W1( $$, as_cv_qualifier_seq, loc($1), as_cv_qualifier, $1 ); }
	| cv_qualifier_seq cv_qualifier
	  { $$ = $1; WRAPPER_APPEND( $$, $2 ); }
	;

// 7.1.5.2 Simple type specifiers

_simple_type_spec_exclusive:
	  _simple_type_spec_exclusive_name
	  { $$ = as_named_simple_type_specifier::create( loc($1), $1 ); }
	| TOK_VOID
	  { $$ = as_void_simple_type_specifier::create( loc($1) ); }
	;

_simple_type_spec_exclusive_name:
	  coloncolon_opt _CLASS_NAME_ID
	  { $$ = as_name::create( loc($1), $1, TOK2ID($2), $1->was_qualified_get() ); }
	| coloncolon_opt _TEMPL_TYPE _templ_args
	  { $$ = as_name::create( loc($1), $1, as_template_id::create(loc($2),$2,UNWRAP($3)), $1->was_qualified_get() ); }
	| coloncolon_opt _ENUM_NAME_ID
	  { $$ = as_name::create( loc($1), $1, TOK2ID($2), $1->was_qualified_get() ); }
	| coloncolon_opt _TYPEDEF_NAME_ID
	  { $$ = as_name::create( loc($1), $1, TOK2ID($2), $1->was_qualified_get() ); }
	| coloncolon_opt nested_name_spec _CLASS_NAME_ID
	  { $$ = as_name::create( loc($1), $2, TOK2ID($3), true ); }
	| coloncolon_opt nested_name_spec _TEMPL_TYPE _templ_args
	  { $$ = as_name::create( loc($1), $2, as_template_id::create( loc($3), $3, UNWRAP($4) ), true ); }
	| coloncolon_opt nested_name_spec _ENUM_NAME_ID
	  { $$ = as_name::create( loc($1), $2, TOK2ID($3), true ); }
	| coloncolon_opt nested_name_spec _TYPEDEF_NAME_ID
	  { $$ = as_name::create( loc($1), $2, TOK2ID($3), true ); }
	| coloncolon_opt nested_name_spec TOK_TEMPLATE _hinter_templ_id_follows _TEMPL_FORCED _templ_args
	  { $$ = as_name::create( loc($1), $2, as_template_id::create( loc($5), $5, UNWRAP($6) ), true ); }
	;

_simple_type_spec_non_exclusive:
	  TOK_CHAR
	  { $$ = as_char_simple_type_specifier::create( loc($1) ); }
	| TOK_WCHAR_T
	  { $$ = as_wchar_t_simple_type_specifier::create( loc($1) ); }
	| TOK_BOOL
	  { $$ = as_bool_simple_type_specifier::create( loc($1) ); }
	| TOK_SHORT
	  { $$ = as_short_simple_type_specifier::create( loc($1) ); }
	| TOK_INT
	  { $$ = as_int_simple_type_specifier::create( loc($1) ); }
	| TOK_LONG
	  { $$ = as_long_simple_type_specifier::create( loc($1) ); }
	| TOK_SIGNED
	  { $$ = as_signed_simple_type_specifier::create( loc($1) ); }
	| TOK_UNSIGNED
	  { $$ = as_unsigned_simple_type_specifier::create( loc($1) ); }
	| TOK_FLOAT
	  { $$ = as_float_simple_type_specifier::create( loc($1) ); }
	| TOK_DOUBLE
	  { $$ = as_double_simple_type_specifier::create( loc($1) ); }
	;

enumerator_list_opt:
	  _no_token
	  { $$ = EMPTY_WRAPPER( as_enumerator_list, loc($1), as_enumerator_definition ); }
	| enumerator_list
	  { $$ = $1; }
//99	| enumerator_list TOK_COMMA
//99	  { $$ = $1; warn("C99"); }
	;

enumerator_list:
	  enumerator_defn
	  { NEW_WRAPPER_W1( $$, as_enumerator_list, loc($1), as_enumerator_definition, $1 ); }
	| enumerator_list TOK_COMMA enumerator_defn
	  { $$ = $1; WRAPPER_APPEND( $$, $3 ); }
	;

enumerator_defn:
	  _anyid
	  { $$ = as_enumerator_definition::create( loc($1), $1, as_constant_expression::create( loc($1), EMPTY_EXPR(loc($1)) ) ); }
	| _anyid TOK_EQ const_expr
	  { $$ = as_enumerator_definition::create( loc($1), $1, $3 ); }
	;

// flattened
nmspc_defn:
	  _nmspc_defn_head TOK_LEFT_BRACE _decl_seq_in_nmspc_scope_opt TOK_RIGHT_BRACE
	  {
	  	LIST_APPEND_LIST( $1->declarations_get(), UNWRAP($3) );
		$$ = $1;
		sa_namespace_definition::instance()->process_end();
	  }
	;

_nmspc_defn_head:
	  TOK_NAMESPACE _anyid
	  {
		ptr<as_namespace_definition> nd = as_namespace_definition::create( loc($1), as_name::create(loc($2),CURR_CTX(loc($1)),$2), EMPTY_LIST(as_declaration) );
		sa_namespace_definition::instance()->process_named( nd->name_get() );
		$$ = nd;
	  }
	| TOK_NAMESPACE _no_token _hinter_nmspc_name_clear
	  {
		$$ = as_namespace_definition::create( loc($1), as_name::create(loc($1),CURR_CTX(loc($1)),EMPTY_ID(loc($2))), EMPTY_LIST(as_declaration) );
		sa_namespace_definition::instance()->process_unnamed( loc($1) );
	  }
	;

_using_id:
	  unqualified_id
	  { $$ = $1; }
	| conv_func_id
	  { $$ = $1; }
	;

using_decl:
	  TOK_USING coloncolon_opt _using_id TOK_SEMICOLON
	  { $3->was_qualified_set( $2->was_qualified_get() ); $$ = as_using_declaration::create( loc($1), $3 ); }
	| TOK_USING coloncolon_opt nested_name_spec _using_id TOK_SEMICOLON
	  { $4->was_qualified_set(true); $$ = as_using_declaration::create( loc($1), $4 ); }
	| TOK_USING TOK_TYPENAME coloncolon_opt nested_name_spec _using_id TOK_SEMICOLON
	  { $5->was_qualified_set(true); $$ = as_using_declaration_with_typename::create( loc($1), $5 ); }
	;

// Classes

class_key:
	  _class_key_real
	  { $$ = $1; hinter::elab_spec_set( elab_spec_class_key::create(loc($1),$1) ); }
	;

_class_key_real:
	  TOK_CLASS
	  { $$ = as_class_key_class::create( loc($1) ); }
	| TOK_STRUCT
	  { $$ = as_class_key_struct::create( loc($1) ); }
	| TOK_UNION
	  { $$ = as_class_key_union::create( loc($1) ); }
	;

member_spfn_opt:
	  _no_token
	  { $$ = EMPTY_WRAPPER( as_member_specification, loc($1), as_member_declaration ); }
	| member_spfn
	  { $$ = $1; }
	;
	
member_spfn:
	  member_decl
	  { NEW_WRAPPER_W1( $$, as_member_specification, loc($1), as_member_declaration, $1 ); }
	| access_spec TOK_COLON
	  { $$ = EMPTY_WRAPPER( as_member_specification, loc($1), as_member_declaration );
	    /* XXX change CURR_ACCESS_SPEC */ }
	| member_spfn member_decl
	  { $$ = $1; WRAPPER_APPEND( $$, $2 ); }
	| member_spfn access_spec TOK_COLON
	  { $$ = $1; /* access_spec at the end of member_spfn is ignored */ }
	;

/*
 * for the grammar to remain reasonably simple, the parser does not know
 *   whether TOK_COLON denotes a bitfield or a ctor-initializer
 * "when you don't know, disambiguate" ;-)
 */
_decl_spec_seq_in_class_scope:
	  _decl_spec_seq_orig _lookahead_assert_non_empty
	  {
		clear_lookahead();
		disambiguate_ctor_and_bitfield_class_scope( $1 );
		$$ = $1;
		sa_parser_action_logger << "decl spec seq in class scope\n" << msg::eolog;
		sa_simple_declaration::instance()->save_declaration_specifiers($1);
	  }
	;

_ctor_first_member_dcltor_in_par:
	  /* covers at least one pair of parentheses around the ctor name */
	  _ctor_name_in_par _func_suffix_clean
	  { ptr<as_declarator> ctor_dcltor = as_declarator::create( loc($1), as_name::create(loc($2),CURR_CTX(loc($1)),$1), EMPTY_LIST(as_declarator_op) );
	    LIST_APPEND( ctor_dcltor->declarator_ops_get(), $2 );
	    $$ = as_ordinary_member_declarator::create( loc(ctor_dcltor), ctor_dcltor );
	  }
	  /* covers exactly one pair of parentheses around the whole ctor declarator */
	| TOK_LEFT_PAR _ctor_name_in_no_par _func_suffix_clean TOK_RIGHT_PAR
	  { ptr<as_declarator> ctor_dcltor = as_declarator::create( loc($2), as_name::create(loc($3),CURR_CTX(loc($2)),$2), EMPTY_LIST(as_declarator_op) );
	    LIST_APPEND( ctor_dcltor->declarator_ops_get(), $3 );
	    $$ = as_ordinary_member_declarator::create( loc(ctor_dcltor), ctor_dcltor );
	  }
	  /* adds parenthesis around the rules above (the whole ctor declarator) */
	| TOK_LEFT_PAR _ctor_first_member_dcltor_in_par TOK_RIGHT_PAR
	  { $$ = $2; }
	;

_ctor_next_member_dcltor:
	  _ctor_name_in_opt_par _func_suffix_clean
	  { ptr<as_declarator> ctor_dcltor = as_declarator::create( loc($1), as_name::create(loc($1),CURR_CTX(loc($1)),$1), EMPTY_LIST(as_declarator_op) );
	    LIST_APPEND( ctor_dcltor->declarator_ops_get(), $2 );
	    $$ = as_ordinary_member_declarator::create( loc(ctor_dcltor), ctor_dcltor );
	  }
	| TOK_LEFT_PAR _ctor_next_member_dcltor TOK_RIGHT_PAR
	  { $$ = $2; }
	;

_ctor_name_in_par:
	  TOK_LEFT_PAR _ctor_name_in_opt_par TOK_RIGHT_PAR
	  { $$ = $2; }
	;

_ctor_name_in_opt_par:
	  _ctor_name_in_no_par
	  { $$ = $1; }
	| TOK_LEFT_PAR _ctor_name_in_opt_par TOK_RIGHT_PAR
	  { $$ = $2; }
	;

_ctor_name_in_no_par:
	  _ctor_type
	  { $$ = as_constructor_id::create( loc($1), EMPTY_LIST(as_declaration_specifier) );
	    LIST_APPEND( $$->declaration_specifiers_get(), $1 ); }
	;

_ctor_type:
	  coloncolon_opt _ctor_unqualified_id
	  { $2->was_qualified_set( $1->was_qualified_get() );
	    $$ = as_named_simple_type_specifier::create( loc($1), $2 ); }
	| coloncolon_opt nested_name_spec _ctor_unqualified_id
	  { $3->was_qualified_set(true);
	    $$ = as_named_simple_type_specifier::create( loc($1), $3 ); }
	;

_ctor_unqualified_id:
	  _CLASS_NAME_ID
	  { $$ = as_name::create( loc($1), CURR_CTX(loc($1)), TOK2ID($1) ); }
	| _TEMPL_TYPE _templ_args
	  { $$ = as_name::create( loc($1), CURR_CTX(loc($1)), as_template_id::create(loc($1),$1,UNWRAP($2)) ) ; }
	;

member_decl:
	  TOK_SEMICOLON
	  { $$ = as_member_declaration_ordinary::create( loc($1), CURR_ACCESS_SPEC(), EMPTY_LIST(as_declaration_specifier), EMPTY_LIST(as_member_declarator) ); }

	/* constructors, destructors, and type conversion operators */
	| _cdtc_member_decl TOK_SEMICOLON
	  { $$ = $1; }

	| _decl_spec_seq_in_class_scope member_dcltor_list TOK_SEMICOLON
	  { $$ = as_member_declaration_ordinary::create( loc($1), CURR_ACCESS_SPEC(), UNWRAP($1), UNWRAP($2) ); }

	/*
	 * in the following group member_dcltor_list is omitted [9.2/7]
	 * otherwise, _type_spec_declaring_class_enum is covered by _decl_spec_seq_in_class_scope
	 */
	| _type_spec_declaring_class_enum TOK_SEMICOLON
	  { ptr<as_member_declaration_ordinary> mdo = as_member_declaration_ordinary::create( loc($1), CURR_ACCESS_SPEC(), EMPTY_LIST(as_declaration_specifier), EMPTY_LIST(as_member_declarator) );
	    LIST_APPEND( mdo->declaration_specifiers_get(), $1 );
	    $$ = mdo; }

	| _func_defn_in_class_scope
	  { $$ = as_member_function_definition::create( loc($1), CURR_ACCESS_SPEC(), $1 ); }

	| _access_decl
	  { $$ = as_member_using_declaration::create( loc($1), CURR_ACCESS_SPEC(), $1 ); }

	| using_decl
	  { $$ = as_member_using_declaration::create( loc($1), CURR_ACCESS_SPEC(), $1 ); }

	| TOK_TEMPLATE TOK_LT templ_param_list TOK_GT member_decl
	  { $$ = as_member_template_declaration::create( loc($1), CURR_ACCESS_SPEC(), NULL, UNWRAP($3), $5 ); }
	| TOK_EXPORT TOK_TEMPLATE TOK_LT templ_param_list TOK_GT member_decl
	  { $$ = as_member_template_declaration::create( loc($2), CURR_ACCESS_SPEC(), UNWTOK($1), UNWRAP($4), $6 ); }

	| class_key _class_forward_id TOK_SEMICOLON
	  { $$ = as_class_forward_member_declaration::create( loc($1), CURR_ACCESS_SPEC(), $1, $2 ); }
	;

// [11.3]
_access_decl:
	  coloncolon_opt nested_name_spec unqualified_id TOK_SEMICOLON
	  { $3->was_qualified_set(true); $$ = as_access_declaration::create( loc($1), $3 ); }
	| coloncolon_opt nested_name_spec TOK_TEMPLATE _hinter_templ_id_follows unqualified_id TOK_SEMICOLON
	  { $5->was_qualified_set(true); $$ = as_access_declaration::create( loc($1), $5 ); }
	;

_cdtc_member_decl:
	  _cdtc_member_dcltor_list_starting_with_ctor_no_par
	  { $$ = as_member_declaration_ordinary::create( loc($1), CURR_ACCESS_SPEC(), EMPTY_LIST(as_declaration_specifier), UNWRAP($1) ); }
	| _cdtc_member_dcltor_list_not_starting_with_ctor_no_par
	  { $$ = as_member_declaration_ordinary::create( loc($1), CURR_ACCESS_SPEC(), EMPTY_LIST(as_declaration_specifier), UNWRAP($1) ); }
	| _decl_spec_seq_other _cdtc_member_dcltor_list_not_starting_with_ctor_no_par
	  { $$ = as_member_declaration_ordinary::create( loc($1), CURR_ACCESS_SPEC(), UNWRAP($1), UNWRAP($2) ); }
	;

_cdtc_member_dcltor_list_starting_with_ctor_no_par:
	  _ctor_first_member_dcltor_no_par
	  { NEW_WRAPPER_W1( $$, as_member_declarator_list, loc($1), as_member_declarator, $1 ); }
	| _ctor_first_member_dcltor_no_par TOK_COMMA _next_member_dcltor_list_cdtc
	  { $$ = $3; WRAPPER_PREPEND( $$, $1 ); }
	;

_ctor_first_member_dcltor_no_par:
	  _decl_spec_seq_in_class_scope _ctor_func_suffix
	  /* FIXME:
	   * instead of unwrapping all declaration specifiers from $1,
	   *   the "constructor class name" [12.1/1] should be _moved_
	   *   into the as_constructor_id created herein
	   * that will make this case consistent with the "parenthesized" ones
	   * the difference can be ilustrated by the following example:
	   *   class c {
	   *     inline c explicit (); // here, the name is "hidden" in the decl_spec_seq
	   *                           //   among "inline" and "explicit"
	   *                           // this is the case we cover by this particular rule
	   *     inline (c)();         // "explicit" not allowed after the parenthesized name
	   *   };
	   *
	   * _func_defn_in_nmspc_scope needs to be fixed in the same way
	   */
	  { ptr<as_declarator> ctor_dcltor = as_declarator::create( loc($2), as_name::create(loc($2),CURR_CTX(loc($2)),as_constructor_id::create(loc($2),UNWRAP($1))), EMPTY_LIST(as_declarator_op) );
	    LIST_APPEND( ctor_dcltor->declarator_ops_get(), $2 );
	    $$ = as_ordinary_member_declarator::create( loc(ctor_dcltor), ctor_dcltor );
	  }
	;

_cdtc_member_dcltor_list_not_starting_with_ctor_no_par:
	  _cdtc_first_member_dcltor
	  { NEW_WRAPPER_W1( $$, as_member_declarator_list, loc($1), as_member_declarator, $1 ); }
	| _cdtc_first_member_dcltor TOK_COMMA _next_member_dcltor_list_cdtc
	  { $$ = $3; WRAPPER_PREPEND( $$, $1 ); }
	;

_cdtc_first_member_dcltor:
	  _ctor_first_member_dcltor_in_par
	  { $$ = $1; }
	| _dtc_member_dcltor
	  { $$ = $1; }
	;

member_dcltor_list:
	  _first_bitfield_member_dcltor
	  { NEW_WRAPPER_W1( $$, as_member_declarator_list, loc($1), as_member_declarator, $1 ); }
	| _non_bitfield_member_dcltor
	  { NEW_WRAPPER_W1( $$, as_member_declarator_list, loc($1), as_member_declarator, $1 ); }
	| member_dcltor_list TOK_COMMA _next_bitfield_member_dcltor
	  { $$ = $1; WRAPPER_APPEND( $$, $3 ); }
	| member_dcltor_list TOK_COMMA _non_bitfield_member_dcltor
	  { $$ = $1; WRAPPER_APPEND( $$, $3 ); }
	;

/*
 * the first bitfield declarator is padded if and only if it is anonymous
 * this is because disambiguation is run after the decl_spec_seq
 * the second and further ones ("next", below), including anonymous, are not padded
 *
 * also see _decl_spec_seq_in_class_scope
 */
_first_bitfield_member_dcltor:
	  TOK_PAD_UNNAMED_BITFIELD TOK_COLON const_expr
	  { $$ = as_bitfield_member_declarator::create( loc($2), NULL, $3 ); }
	| _named_bitfield_member_dcltor
	  { $$ = $1; }
	;

_next_bitfield_member_dcltor:
	  TOK_COLON const_expr
	  { $$ = as_bitfield_member_declarator::create( loc($1), NULL, $2 ); }
	| _named_bitfield_member_dcltor
	  { $$ = $1; }
	;

_named_bitfield_member_dcltor:
	  /* coloncolon_opt is incorrect here, but without it, it conflicts with ptr_op in dcltor */
	  coloncolon_opt unqualified_id TOK_COLON const_expr
	  {
		$$ = as_bitfield_member_declarator::create( loc($2), $2, $4 );
		// we only parse class bodies for real
		lassert( !manager::in_disambiguation() );
		// if the coloncolon _was_ there, report an error, but continue
		if ($1->was_qualified_get())
			report << bad_global_qual << loc($1);
	  }
	;

_non_bitfield_member_dcltor:
	  _in_class_dcltor_action
	  { $$ = as_ordinary_member_declarator::create( loc($1), $1 ); }
	| _non_bitfield_member_dcltor_inizer_action TOK_PAD_MEMBER_OBJECT const_expr
	  { $$ = as_initialized_member_declarator::create( loc($1), $1, $3 ); }
	  /*
	   * pure specifier:
	   * TOK_LITERAL must be '0' (not any other kind of zero or expression!)
	   */
	| _non_bitfield_member_dcltor_inizer_action TOK_PAD_MEMBER_FUNC TOK_LITERAL
	  { $$ = as_pure_member_declarator::create( loc($1), $1, UNWTOK($3) ); }
	;

// are we holding declarator of a function or of an object?
_non_bitfield_member_dcltor_inizer_action:
	  _in_class_dcltor_action TOK_EQ _lookahead_assert_empty {
		ptr< list< srp<as_declarator_op> > > ops = $1->declarator_ops_get();
		manager::start( TOK_START_TO_BE_DISCARDED );
		// look at the first declarator operator (if any) to decide
		if ( !ops->empty() &&
				as_declarator_op2op_func::instance()->process(ops->front()) )
			manager::commit( TOK_PAD_MEMBER_FUNC );
		else
			manager::commit( TOK_PAD_MEMBER_OBJECT );

		// the stored value is the same either way
		$$ = $1;
	  }
	;

_in_class_dcltor_action:
	  /* in-class declarator does not have conflicts; we can reuse inner_dcltor */
	  _inner_dcltor
	  {
		$$ = $1;
		sa_parser_action_logger << "processing in class declarator\n" << msg::eolog;
		sa_simple_declaration::instance()->process($1);
	  }
	;

base_spec_list:
	  base_spec
	  { NEW_WRAPPER_W1( $$, as_base_specifier_list, loc($1), as_base_specifier, $1 ); }
	| base_spec_list TOK_COMMA base_spec
	  { $$ = $1; WRAPPER_APPEND( $1, $3 ); }
	;

base_spec:
	  coloncolon_opt _CLASS_NAME_ID
	  { $$ = as_base_specifier::create( loc($1), NULL, NULL, as_name::create(loc($1),$1,TOK2ID($2),$1->was_qualified_get()) ); }
	| coloncolon_opt _TEMPL_TYPE _templ_args
	  { $$ = as_base_specifier::create( loc($1), NULL, NULL, as_name::create( loc($1), $1, as_template_id::create(loc($2),$2,UNWRAP($3)), $1->was_qualified_get() ) ); }
	| coloncolon_opt nested_name_spec _CLASS_NAME_ID
	  { $$ = as_base_specifier::create( loc($1), NULL, NULL, as_name::create(loc($1),$2,TOK2ID($3),true) ); }
	| coloncolon_opt nested_name_spec _TEMPL_TYPE _templ_args
	  { $$ = as_base_specifier::create( loc($1), NULL, NULL, as_name::create( loc($1), $1, as_template_id::create(loc($3),$3,UNWRAP($4)), true ) ); }
	| TOK_VIRTUAL access_spec_opt coloncolon_opt _CLASS_NAME_ID
	  { $$ = as_base_specifier::create( loc($1), UNWTOK($1), $2->value_get(), as_name::create(loc($3),$3,TOK2ID($4),$3->was_qualified_get()) ); }
	| TOK_VIRTUAL access_spec_opt coloncolon_opt _TEMPL_TYPE _templ_args
	  { $$ = as_base_specifier::create( loc($1), UNWTOK($1), $2->value_get(), as_name::create( loc($3), $3, as_template_id::create(loc($4),$4,UNWRAP($5)), $3->was_qualified_get() ) ); }
	| TOK_VIRTUAL access_spec_opt coloncolon_opt nested_name_spec _CLASS_NAME_ID
	  { $$ = as_base_specifier::create( loc($1), UNWTOK($1), $2->value_get(), as_name::create(loc($4),$4,TOK2ID($5),true) ); }
	| TOK_VIRTUAL access_spec_opt coloncolon_opt nested_name_spec _TEMPL_TYPE _templ_args
	  { $$ = as_base_specifier::create( loc($1), UNWTOK($1), $2->value_get(), as_name::create( loc($4), $4, as_template_id::create(loc($5),$5,UNWRAP($6)), true ) ); }
	| access_spec virtual_opt coloncolon_opt _CLASS_NAME_ID
	  { $$ = as_base_specifier::create( loc($1), $2->value_get(), $1, as_name::create(loc($3),$3,TOK2ID($4),$3->was_qualified_get()) ); }
	| access_spec virtual_opt coloncolon_opt _TEMPL_TYPE _templ_args
	  { $$ = as_base_specifier::create( loc($1), $2->value_get(), $1, as_name::create( loc($3), $3, as_template_id::create(loc($4),$4,UNWRAP($5)), $3->was_qualified_get() ) ); }
	| access_spec virtual_opt coloncolon_opt nested_name_spec _CLASS_NAME_ID
	  { $$ = as_base_specifier::create( loc($1), $2->value_get(), $1, as_name::create(loc($4),$4,TOK2ID($5),true) ); }
	| access_spec virtual_opt coloncolon_opt nested_name_spec _TEMPL_TYPE _templ_args
	  { $$ = as_base_specifier::create( loc($1), $2->value_get(), $1, as_name::create( loc($4), $4, as_template_id::create(loc($5),$5,UNWRAP($6)), true ) ); }
	;

virtual_opt:
	  _no_token
	  { $$ = as_virtual_opt_bearer::create( loc($1), NULL ); }
	| TOK_VIRTUAL
	  { $$ = as_virtual_opt_bearer::create( loc($1), UNWTOK($1) ); }
	;

access_spec:
	  TOK_PRIVATE
	  { $$ = as_access_specifier_private::create( loc($1) ); }
	| TOK_PROTECTED
	  { $$ = as_access_specifier_protected::create( loc($1) ); }
	| TOK_PUBLIC
	  { $$ = as_access_specifier_public::create( loc($1) ); }
	;

access_spec_opt:
	  _no_token
	  { $$ = as_access_spec_opt_bearer::create( loc($1), NULL ); }
	| access_spec
	  { $$ = as_access_spec_opt_bearer::create( loc($1), $1 ); }
	;

// Templates

templ_param_list:
	  templ_param
	  { NEW_WRAPPER_W1( $$, as_template_param_list, loc($1), as_template_param, $1 ); }
	| templ_param_list TOK_COMMA templ_param
	  { $$ = $1; WRAPPER_APPEND( $$, $3 ); }
	;

templ_param:
	  _disambiguate_templ_param TOK_PAD_TEMPL_PARAM_TYPE type_param
	  { $$ = $3; }
	| _disambiguate_templ_param TOK_PAD_TEMPL_PARAM_NONTYPE _param_decl_no_gt
	  { $$ = as_template_nontype_param::create( loc($3), $3 ); }
	;

type_param:
	  TOK_CLASS _anyid_opt
	  { $$ = as_template_type_param_type::create( loc($1), $2->value_get(), NULL ); }
	| TOK_CLASS _anyid_opt TOK_EQ type_id
	  { $$ = as_template_type_param_type::create( loc($1), $2->value_get(), $4 ); }
	| TOK_TYPENAME _anyid_opt
	  { $$ = as_template_type_param_type::create( loc($1), $2->value_get(), NULL ); }
	| TOK_TYPENAME _anyid_opt TOK_EQ type_id
	  { $$ = as_template_type_param_type::create( loc($1), $2->value_get(), $4 ); }
	| TOK_TEMPLATE TOK_LT templ_param_list TOK_GT TOK_CLASS _anyid_opt
	  { $$ = as_template_type_param_template::create( loc($1), UNWRAP($3), $6->value_get(), NULL ); }
	| TOK_TEMPLATE TOK_LT templ_param_list TOK_GT TOK_CLASS _anyid_opt TOK_EQ id_expr
	  { $$ = as_template_type_param_template::create( loc($1), UNWRAP($3), $6->value_get(), $8 ); }
	;

_templ_args:
	  /* the disambiguation is needed to prevent the look-ahead from being read */
	  TOK_LT _disambiguate_templ_arg TOK_GT
	  { $$ = EMPTY_WRAPPER( as_template_argument_list, loc($3), as_template_argument ); }
	| TOK_LT templ_arg_list TOK_GT
	  { $$ = $2; }
	;

templ_arg_list:
	  templ_arg
	  { NEW_WRAPPER_W1( $$, as_template_argument_list, loc($1), as_template_argument, $1 ); }
	| templ_arg_list TOK_COMMA templ_arg
	  { $$ = $1; WRAPPER_APPEND( $$, $3 ); }
	;

templ_arg:
	  _disambiguate_templ_arg TOK_PAD_TEMPL_ARG_EXPR _assign_expr_no_gt
	  { $$ = as_non_type_template_argument::create( loc($3), $3 ); }
	| _disambiguate_templ_arg TOK_PAD_TEMPL_ARG_TYPE_ID type_id
	  { $$ = as_type_template_argument::create( loc($3), $3 ); }
	  /* not needed, covered by type_id; at least in sensible cases (ie. no dtor names etc.) */
	//| _id_expr_type
	;

// 8 Declarators

init_dcltor_list:
	  init_dcltor
	  { NEW_WRAPPER_W1( $$, as_init_declarator_list, loc($1), as_init_declarator, $1 ); }
	| init_dcltor_list TOK_COMMA init_dcltor
	  { $$ = $1; WRAPPER_APPEND( $$, $3 ); }
	;

/*
init_dcltor_list_opt:
	  _no_token
	  { $$ = EMPTY_WRAPPER( as_init_declarator_list, loc($1), as_init_declarator ); }
	| init_dcltor_list
	  { $$ = $1; }
	;
 */

init_dcltor:
	  _dcltor_action
	  {
		$$ = as_init_declarator::create( loc($1), $1, NULL );
		sa_statements::instance()->declarator_simple(loc($1));
	  }
	| _dcltor_action TOK_EQ inizer_clause
	  {
		$$ = as_init_declarator::create( loc($1), $1, $3 );
		sa_statements::instance()->declarator_init(loc($1), $3);
	  }
	  /* the pad token is already there... */
	| _dcltor_action TOK_PAD_LPAR_EXPR_LIST_RPAR TOK_LEFT_PAR expr_list TOK_RIGHT_PAR
	  {
		$$ = as_init_declarator::create( loc($1), $1, as_initializer_expression_list::create(loc($4),UNWRAP($4)) );
		sa_statements::instance()->declarator_ctor(loc($1), $4);
	  }
	;

_dcltor_action:
	  dcltor
	  {
		$$ = $1;
		sa_parser_action_logger << "processing declarator\n" << msg::eolog;
		sa_simple_declaration::instance()->process($1);
	  }
	;

_cdtc_next_member_dcltor:
	  _dtc_member_dcltor
	  { $$ = $1; }
	| _ctor_next_member_dcltor
	  { $$ = $1; }
	;

/* dtc stands for "destructor or type conversion" */
_dtc_member_dcltor:
	  _dtc_dcltor_action
	  { $$ = as_ordinary_member_declarator::create( loc($1), $1 ); }
	  /* TOK_LITERAL must be '0' (not any other kind of zero or expression!) */
	| _dtc_dcltor_action TOK_EQ TOK_LITERAL
	  { $$ = as_pure_member_declarator::create( loc($1), $1, UNWTOK($3) ); }
	;

_dtc_dcltor_action:
	  _dtc_dcltor
	  { $$ = $1; /* XXX action */ }
	;

_id_expr_dtc_in_opt_par:
	  _id_expr_dtc
	  { $$ = $1; }
	| TOK_LEFT_PAR _id_expr_dtc_in_opt_par TOK_RIGHT_PAR
	  { $$ = $2; }
	;

_dtc_dcltor:
	  _id_expr_dtc_in_opt_par _func_suffix_clean
	  { $$ = as_declarator::create( loc($1), $1, EMPTY_LIST(as_declarator_op) );
	    LIST_APPEND( $$->declarator_ops_get(), $2 ); }
	| TOK_LEFT_PAR _dtc_dcltor TOK_RIGHT_PAR
	  { $$ = $2; }
	;

_ctor_func_suffix:
	  TOK_PAD_CTOR _func_suffix_clean
	  { $$ = $2; }
	;

_id_expr_dtc:
	  coloncolon_opt _dtc_unqualified_id
	  { $2->was_qualified_set( $1->was_qualified_get() ); $$ = $2; }
	| coloncolon_opt nested_name_spec _dtc_unqualified_id
	  { $3->was_qualified_set(true); $$ = $3; }
	;

_dtc_unqualified_id:
	  TOK_TILDE _CLASS_NAME_ID
	  { $$ = as_name::create( loc($1), CURR_CTX(loc($1)), as_destructor_id_token::create(loc($1),$2) ); }
	| TOK_TILDE _TEMPL_TYPE _templ_args
	  { $$ = as_name::create( loc($1), CURR_CTX(loc($1)), as_destructor_id_template::create(loc($1),as_template_id::create(loc($2),$2,UNWRAP($3))) ) ; }
	| conv_func_id
	  { $$ = $1; }
	;

dcltor:
	  direct_dcltor
	  { $$ = $1; }
	| _ptr_op_seq direct_dcltor
	  { $$ = $2; LIST_APPEND_LIST( $$->declarator_ops_get(), UNWRAP($1) ); } 
	;

/*
 * the "danger" of ambiguity between param_decl_clause and expr_list exists
 *   only at the top level of the declarator
 * the disambiguation is run before every reduction of direct_dcltor (i.e. only
 *   at the top level)
 */
direct_dcltor:
	  id_expr _disambiguate_param_decl_clause_vs_expr_list
	  { $$ = as_declarator::create( loc($1), $1, EMPTY_LIST(as_declarator_op) ); }
	  /*
	   * make sure the disambiguation rule (nested in the direct_dcltor)
	   * "see" the left parenthesis
	   */
	| direct_dcltor TOK_LEFT_PAR
	  { NO_PAD_TOKEN_BUG( loc($2) ); }
	| direct_dcltor TOK_PAD_PARAM_DECL_CLAUSE _func_suffix_clean _disambiguate_param_decl_clause_vs_expr_list
	  { $$ = $1; LIST_APPEND( $$->declarator_ops_get(), $3 ); }
	| direct_dcltor _bracket_suffix _disambiguate_param_decl_clause_vs_expr_list
	  { $$ = $1; LIST_APPEND( $$->declarator_ops_get(), $2 ); }
	| TOK_LEFT_PAR _inner_dcltor TOK_RIGHT_PAR _disambiguate_param_decl_clause_vs_expr_list
	  { $$ = $2; }
	;

_inner_dcltor:
	  _inner_direct_dcltor
	  { $$ = $1; }
	| _ptr_op_seq _inner_direct_dcltor
	  { $$ = $2; LIST_APPEND_LIST( $$->declarator_ops_get(), UNWRAP($1) ); } 
	;
	  
_inner_direct_dcltor:
	  id_expr
	  { $$ = as_declarator::create( loc($1), $1, EMPTY_LIST(as_declarator_op) ); }
	| _inner_direct_dcltor _func_suffix_clean
	  { $$ = $1; LIST_APPEND( $$->declarator_ops_get(), $2 ); }
	| _inner_direct_dcltor _bracket_suffix
	  { $$ = $1; LIST_APPEND( $$->declarator_ops_get(), $2 ); }
	| TOK_LEFT_PAR _inner_dcltor TOK_RIGHT_PAR
	  { $$ = $2; }
	;

_bracket_suffix:
	  TOK_LEFT_BRACKET const_expr_opt TOK_RIGHT_BRACKET
	  { $$ = as_declarator_op_array::create( loc($1), $2 ); }
//99	| __C99_bracket
	;

/*99
__C99_bracket:
	  __C99_bracket1 { error("C99"); }
	;

__C99_bracket1:
	  TOK_LEFT_BRACKET cv_qualifier_seq assign_expr TOK_RIGHT_BRACKET
	  //available in non-abstract_declarator only:
	| TOK_LEFT_BRACKET cv_qualifier_seq TOK_RIGHT_BRACKET
	  // next two conflict with ordinary brackets:
	//| TOK_LEFT_BRACKET assign_expr TOK_RIGHT_BRACKET
	//| TOK_LEFT_BRACKET TOK_RIGHT_BRACKET
	  //available in non-abstract_declarator only:
	| TOK_LEFT_BRACKET TOK_STATIC cv_qualifier_seq assign_expr TOK_RIGHT_BRACKET
	  //available in non-abstract_declarator only:
	| TOK_LEFT_BRACKET TOK_STATIC assign_expr TOK_RIGHT_BRACKET
	  //available in non-abstract_declarator only:
	| TOK_LEFT_BRACKET cv_qualifier_seq TOK_STATIC assign_expr TOK_RIGHT_BRACKET
	  //available in non-abstract_declarator only:
	| TOK_LEFT_BRACKET cv_qualifier_seq TOK_STAR TOK_RIGHT_BRACKET
	| TOK_LEFT_BRACKET TOK_STAR TOK_RIGHT_BRACKET
	;
 */

/* the collection in semantic value is in reverse order! */
_ptr_op_seq:
	  ptr_op
	  { NEW_WRAPPER_W1( $$, as_declarator_op_seq, loc($1), as_declarator_op, $1 ); }
	| _ptr_op_seq ptr_op
	  { $$ = $1; WRAPPER_PREPEND( $$, $2 ); }
	;

ptr_op:
	  TOK_STAR
	  { $$ = as_ptr_op_star::create( loc($1), EMPTY_LIST(as_cv_qualifier) ); }
	| TOK_STAR cv_qualifier_seq
	  { $$ = as_ptr_op_star::create( loc($1), UNWRAP($2) ); }
	| TOK_AMP
	  { $$ = as_ptr_op_amp::create( loc($1) ); }

	| coloncolon_opt nested_name_spec TOK_STAR _hinter_clear_qualification
	  { $$ = as_ptr_op_member_ptr::create( loc($1), $1, EMPTY_LIST(as_cv_qualifier) ); }
	| coloncolon_opt nested_name_spec TOK_STAR cv_qualifier_seq _hinter_clear_qualification
	  { $$ = as_ptr_op_member_ptr::create( loc($1), $1, UNWRAP($4) ); }
	;

// 8.1 Type names

type_id:
	  _type_spec_seq_not_declaring_class_enum _next_location
	  { $$ = as_type_id::create( loc($1), UNWRAP($1), EMPTY_DCLTOR(loc($2)) ); }
	| _type_spec_seq_not_declaring_class_enum _next_location abst_dcltor
	  { $$ = as_type_id::create( loc($1), UNWRAP($1), $3 ); }
	;

_type_spec_seq_not_declaring_class_enum:
	  _type_spec_seq_not_declaring_class_enum_exclusive
	  {
		$$ = $1;
		sa_parser_action_logger << "type spec seq exclusive\n" << msg::eolog;
		sa_simple_declaration::instance()->save_type_specifiers($1);
	  }
	| _type_spec_seq_not_declaring_class_enum_non_exclusive
	  {
		$$ = $1;
		sa_parser_action_logger << "decl spec seq non exclusive\n" << msg::eolog;
		sa_simple_declaration::instance()->save_type_specifiers($1);
	  }
	;

_type_spec_seq_not_declaring_class_enum_exclusive:
	  _type_spec_not_declaring_class_enum_exclusive
	  { NEW_WRAPPER_W1( $$, as_type_specifier_seq, loc($1), as_type_specifier, $1 ); }
	| cv_qualifier_seq _type_spec_not_declaring_class_enum_exclusive
	  { $$ = EMPTY_WRAPPER( as_type_specifier_seq, loc($1), as_type_specifier );
	    // we have to copy, because the lists carry differently typed items
	    // luckily, they are convertible (as_cv_qualifier is derived from as_type_specifier)
	    ::std::copy( UNWRAP($1)->begin(), UNWRAP($1)->end(), ::std::back_inserter(*(UNWRAP($$))) );
	    WRAPPER_APPEND( $$, $2 ); }
	// type specifier sequence below
	| _type_spec_seq_not_declaring_class_enum_exclusive cv_qualifier
	  { $$ = $1; WRAPPER_APPEND( $$, $2 ); }
	;

_type_spec_seq_not_declaring_class_enum_non_exclusive:
	  _simple_type_spec_non_exclusive
	  { NEW_WRAPPER_W1( $$, as_type_specifier_seq, loc($1), as_type_specifier, $1 ); }
	| cv_qualifier_seq _simple_type_spec_non_exclusive
	  { $$ = EMPTY_WRAPPER( as_type_specifier_seq, loc($1), as_type_specifier );
	    // we have to copy, because the lists carry differently typed items
	    // luckily, they are convertible (as_cv_qualifier is derived from as_type_specifier)
	    ::std::copy( UNWRAP($1)->begin(), UNWRAP($1)->end(), ::std::back_inserter(*(UNWRAP($$))) );
	    WRAPPER_APPEND( $$, $2 ); }
	// type specifier sequences below
	| _type_spec_seq_not_declaring_class_enum_non_exclusive cv_qualifier
	  { $$ = $1; WRAPPER_APPEND( $$, $2 ); }
	| _type_spec_seq_not_declaring_class_enum_non_exclusive _simple_type_spec_non_exclusive
	  { $$ = $1; WRAPPER_APPEND( $$, $2 ); }
	;

/*
 * no ambiguities in abst_dcltor in contexts where it *cannot* appear instead of dcltor
 *
 * as we do not have to change scopes when dealing with param_decl_clause, no
 * disambiguation is required (we use _func_suffix_clean_no_scope_change)
 */
abst_dcltor:
	  direct_abst_dcltor
	  { $$ = $1; }
	| _ptr_op_seq
	  { $$ = as_declarator::create( loc($1), NULL, UNWRAP($1) ); }
	| _ptr_op_seq direct_abst_dcltor
	  { $$ = $2; LIST_APPEND_LIST( $$->declarator_ops_get(), UNWRAP($1) ); } 
	;

direct_abst_dcltor:
	  TOK_LEFT_PAR abst_dcltor TOK_RIGHT_PAR
	  { $$ = $2; }

	| _bracket_suffix
	  { $$ = as_declarator::create( loc($1), NULL, EMPTY_LIST(as_declarator_op) );
	    LIST_APPEND( $$->declarator_ops_get(), $1 ); }
	| direct_abst_dcltor _bracket_suffix
	  { $$ = $1; LIST_APPEND( $$->declarator_ops_get(), $2 ); }

	| _func_suffix_clean_no_scope_change
	  { $$ = as_declarator::create( loc($1), NULL, EMPTY_LIST(as_declarator_op) );
	    LIST_APPEND( $$->declarator_ops_get(), $1 ); }
	| direct_abst_dcltor _func_suffix_clean_no_scope_change
	  { $$ = $1; LIST_APPEND( $$->declarator_ops_get(), $2 ); }
	;

const_expr_opt:
	  _no_token
	  { $$ = as_constant_expression::create( loc($1), EMPTY_EXPR(loc($1)) ); }
	| const_expr
	  { $$ = $1; }
	;

// 8.3.5 Functions

param_decl_clause:
	  _no_token
	  { $$ = as_param_decl_clause::create( loc($1), EMPTY_LIST(as_param_declaration), NULL ); }
	| TOK_DOT_DOT_DOT
	  { $$ = as_param_decl_clause::create( loc($1), EMPTY_LIST(as_param_declaration), UNWTOK($1) ); }
	| param_decl_list
	  { $$ = as_param_decl_clause::create( loc($1), UNWRAP($1), NULL ); }
	| param_decl_list TOK_DOT_DOT_DOT
	  { $$ = as_param_decl_clause::create( loc($1), UNWRAP($1), UNWTOK($2) ); }
	| param_decl_list TOK_COMMA TOK_DOT_DOT_DOT
	  { $$ = as_param_decl_clause::create( loc($1), UNWRAP($1), UNWTOK($3) ); }
	;

param_decl_list:
	  param_decl
	  { NEW_WRAPPER_W1( $$, as_param_declaration_list, loc($1), as_param_declaration, $1 ); }
	| param_decl_list TOK_COMMA param_decl
	  { $$ = $1; WRAPPER_APPEND( $$, $3 ); }
	;

param_decl:
	  _decl_spec_seq_in_param_scope _param_dcltor
	  { $$ = as_param_declaration::create( loc($1), UNWRAP($1), $2, EMPTY_EXPR(loc($2)) ); }
	| _decl_spec_seq_in_param_scope _param_dcltor TOK_EQ assign_expr
	  { $$ = as_param_declaration::create( loc($1), UNWRAP($1), $2, $4 ); }
	| _decl_spec_seq_in_param_scope
	  { $$ = as_param_declaration::create( loc($1), UNWRAP($1), DUMMY_DCLTOR, EMPTY_EXPR(loc($1)) ); }
	| _decl_spec_seq_in_param_scope TOK_EQ assign_expr
	  { $$ = as_param_declaration::create( loc($1), UNWRAP($1), DUMMY_DCLTOR, $3 ); }
	;

_param_decl_no_gt:
	  _decl_spec_seq_in_param_scope _param_dcltor
	  { $$ = as_param_declaration::create( loc($1), UNWRAP($1), $2, EMPTY_EXPR(loc($2)) ); }
	| _decl_spec_seq_in_param_scope _param_dcltor TOK_EQ _assign_expr_no_gt
	  { $$ = as_param_declaration::create( loc($1), UNWRAP($1), $2, $4 ); }
	| _decl_spec_seq_in_param_scope
	  { $$ = as_param_declaration::create( loc($1), UNWRAP($1), DUMMY_DCLTOR, EMPTY_EXPR(loc($1)) ); }
	| _decl_spec_seq_in_param_scope TOK_EQ _assign_expr_no_gt
	  { $$ = as_param_declaration::create( loc($1), UNWRAP($1), DUMMY_DCLTOR, $3 ); }
	;
	
/* [8.3/1] */
_param_dcltor:
	  _param_direct_dcltor
	  { $$ = $1; }
	| _ptr_op_seq
	  { $$ = as_declarator::create( loc($1), NULL, UNWRAP($1) ); }
	| _ptr_op_seq _param_direct_dcltor
	  { $$ = $2; LIST_APPEND_LIST( $$->declarator_ops_get(), UNWRAP($1) ); }
	;

_param_direct_dcltor:
	  _disambiguate_param_decl_clause_vs_param_dcltor TOK_LEFT_PAR
	  { NO_PAD_TOKEN_BUG( loc($2) ); }

	  /* coloncolon_opt is incorrect here, but without it, it conflicts with ptr_op in _param_dcltor */
	| coloncolon_opt unqualified_id
	  {
		$$ = as_declarator::create( loc($2), $2, EMPTY_LIST(as_declarator_op) );
		// when the coloncolon _was_ there...
		if ($1->was_qualified_get()) {
			// do not accept this alternative when disambiguating
			// when not disambiguating, report an error, but continue
			if (manager::in_disambiguation()) {
				llog(pl) << "qualified declarator-id, syntax error\n";
				YYABORT;	// returns from yyparse()
			}
			report << bad_global_qual << loc($1);
		}
	  }

	| _disambiguate_param_decl_clause_vs_param_dcltor TOK_PAD_PARAM_DECL_CLAUSE _func_suffix_clean_no_scope_change
	  { $$ = as_declarator::create( loc($3), NULL, EMPTY_LIST(as_declarator_op) );
	    LIST_APPEND( $$->declarator_ops_get(), $3 ); }
	| _param_direct_dcltor _func_suffix_clean_no_scope_change
	  { $$ = $1; LIST_APPEND( $$->declarator_ops_get(), $2 ); }

	| _bracket_suffix
	  { $$ = as_declarator::create( loc($1), NULL, EMPTY_LIST(as_declarator_op) );
	    LIST_APPEND( $$->declarator_ops_get(), $1 ); }
	| _param_direct_dcltor _bracket_suffix
	  { $$ = $1; LIST_APPEND( $$->declarator_ops_get(), $2 ); }

	| _disambiguate_param_decl_clause_vs_param_dcltor TOK_PAD_PARAM_DCLTOR TOK_LEFT_PAR _param_dcltor TOK_RIGHT_PAR
	  { $$ = $4; }
	;

/* "clean" here means that no disambiguation is needed; at least on the top-level ;-) */
_func_suffix_clean:
	  TOK_LEFT_PAR _hinter_pdc_scope param_decl_clause TOK_RIGHT_PAR _hinter_pdc_scope_end excptn_spec_opt
	  { $$ = as_declarator_op_func::create( loc($1), $3, EMPTY_LIST(as_cv_qualifier), $6->value_get() ); }
	| TOK_LEFT_PAR _hinter_pdc_scope param_decl_clause TOK_RIGHT_PAR _hinter_pdc_scope_end cv_qualifier_seq excptn_spec_opt
	  { $$ = as_declarator_op_func::create( loc($1), $3, UNWRAP($6), $7->value_get() ); }
	;

/*
 * func_suffix inside another param_decl_clause does not need to change scopes
 * same goes for param_decl_clause in an abstract declarator
 */
_func_suffix_clean_no_scope_change:
	  TOK_LEFT_PAR param_decl_clause TOK_RIGHT_PAR excptn_spec_opt
	  { $$ = as_declarator_op_func::create( loc($1), $2, EMPTY_LIST(as_cv_qualifier), $4->value_get() ); }
	| TOK_LEFT_PAR param_decl_clause TOK_RIGHT_PAR cv_qualifier_seq excptn_spec_opt
	  { $$ = as_declarator_op_func::create( loc($1), $2, UNWRAP($4), $5->value_get() ); }
	;

_func_defn_dtc:
	  _dtc_dcltor _packed_compound_stmt
	  { $$ = as_function_definition::create( loc($1), EMPTY_LIST(as_declaration_specifier), $1, EMPTY_LIST(as_mem_initializer), $2, EMPTY_LIST(as_handler) ); }
	| _dtc_dcltor func_try_block
	  { $$ = $2; $$->declarator_set($1); }
	;

_func_defn_in_nmspc_scope:
	  _decl_spec_seq_in_nmspc_scope _func_defn_tail
	  { $$ = $2; LIST_APPEND_LIST( $$->declaration_specifiers_get(), UNWRAP($1) ); }
	| _decl_spec_seq_in_nmspc_scope _ctor_func_suffix _ctor_defn_tail
	  // FIXME: see comments in _ctor_first_member_dcltor_no_par
	  { ptr<as_declarator> ctor_dcltor = as_declarator::create( loc($2), as_name::create(loc($2),CURR_CTX(loc($2)),as_constructor_id::create(loc($2),UNWRAP($1))), EMPTY_LIST(as_declarator_op) );
	    LIST_APPEND( ctor_dcltor->declarator_ops_get(), $2 );
	    $3->declarator_set( ctor_dcltor );
	    $$ = $3; }
	| _func_defn_dtc
	  { $$ = $1; }
	/* allow virtual/inline/other specifiers */
	| _decl_spec_seq_other _func_defn_dtc
	  { LIST_PREPEND_LIST( $2->declaration_specifiers_get(), UNWRAP($1) );
	    $$ = $2; }
	;

_func_defn_in_class_scope:
	  _decl_spec_seq_in_class_scope _func_defn_in_class_tail
	  { $$ = $2; LIST_APPEND_LIST( $$->declaration_specifiers_get(), UNWRAP($1) ); }
	| _decl_spec_seq_in_class_scope _ctor_func_suffix _ctor_defn_tail
	  { ptr<as_declarator> ctor_dcltor = as_declarator::create( loc($2), as_name::create(loc($2),CURR_CTX(loc($2)),as_constructor_id::create(loc($2),UNWRAP($1))), EMPTY_LIST(as_declarator_op) );
	    LIST_APPEND( ctor_dcltor->declarator_ops_get(), $2 );
	    $3->declarator_set( ctor_dcltor );
	    $$ = $3; }
	| _func_defn_dtc
	  { $$ = $1; }
	/* allow virtual/inline/other specifiers */
	| _decl_spec_seq_other _func_defn_dtc
	  { LIST_PREPEND_LIST( $2->declaration_specifiers_get(), UNWRAP($1) );
	    $$ = $2; }
	;

_pack_body:
	  _lookahead_assert_empty
	  {
	  	int next_val = manager::peek()->type_get();
		if (is_pack(next_val))
			lassert2( next_val == TOK_PACK_BODY, "Pack already done, but the type differs!" );
		else
	  		manager::pack( TOK_PACK_BODY, loc(manager::prev_yylex()) );
	  }
	;

_packed_compound_stmt:
	  TOK_LEFT_BRACE _pack_body TOK_PACK_BODY TOK_RIGHT_BRACE
	  { $$ = $3; $$->location_set( loc($1) ); }
	;

_ctor_defn_tail:
	  mem_inizer_list_opt _packed_compound_stmt
	  { $$ = as_function_definition::create( loc($1), EMPTY_LIST(as_declaration_specifier), DUMMY_DCLTOR, UNWRAP($1), $2, EMPTY_LIST(as_handler) ); }
	| TOK_TRY mem_inizer_list_opt _packed_compound_stmt _func_handler_seq
	  { $$ = as_function_definition::create( loc($1), EMPTY_LIST(as_declaration_specifier), DUMMY_DCLTOR, UNWRAP($2), $3, UNWRAP($4) ); }
	;

_func_defn_tail:
	  _dcltor_action _packed_compound_stmt
	  {
		ptr<as_function_definition> as_func = as_function_definition::create( loc($1), EMPTY_LIST(as_declaration_specifier), $1, EMPTY_LIST(as_mem_initializer), $2, EMPTY_LIST(as_handler) );
		$$ = as_func;

		// FIXME: [rudo] this is here just for testing purposes
		sa_statements::instance()->enter_function(loc($1),as_func);
		manager::spawn($2);
//F//		hinter::forced_scope_set( ss_decl_seq::root_instance() );
		ptr<parse_result_type> parse_result = parser::parse();
		bool ok = parse_result->success_get();
		manager::close();
		if (ok) {
			// store the parsed tree (for dumping, mainly)
			as_func->parsed_body_set( parse_result->as_result_get() );
		}
		sa_statements::instance()->leave_function();
	  }

	| _dcltor_action func_try_block
	  { $$ = $2; $$->declarator_set($1); }
	;

_func_defn_in_class_tail:
	  _in_class_dcltor_action _packed_compound_stmt
	  { $$ = as_function_definition::create( loc($1), EMPTY_LIST(as_declaration_specifier), $1, EMPTY_LIST(as_mem_initializer), $2, EMPTY_LIST(as_handler) ); }
	| _in_class_dcltor_action func_try_block
	  { $$ = $2; $$->declarator_set($1); }
	;

inizer_clause:
	  assign_expr
	  { $$ = as_initializer_clause_expression::create( loc($1), $1 ); }
	| TOK_LEFT_BRACE inizer_list comma_opt TOK_RIGHT_BRACE
	  { $$ = as_initializer_clause_braced::create( loc($1), UNWRAP($2) ); }
	| TOK_LEFT_BRACE TOK_RIGHT_BRACE
	  { $$ = as_initializer_clause_braced::create( loc($1), EMPTY_LIST(as_initializer_clause) ); }
	;

comma_opt:
	  /* empty */
	| TOK_COMMA
	;

inizer_list:
	  inizer_clause
	  { NEW_WRAPPER_W1( $$, as_initializer_list, loc($1), as_initializer_clause, $1 ); }
	| inizer_list TOK_COMMA inizer_clause
	  { $$ = $1; WRAPPER_APPEND( $$, $3 ); }
//99	| __C99_inizer_clause
//99	| inizer_list TOK_COMMA __C99_inizer_clause
	;

/*99
__C99_inizer_clause:
	  __C99_designation inizer_clause
	  { error("C99"); }
	;
__C99_designation:
	  __C99_designator_seq TOK_EQ
	;
__C99_designator_seq:
	  __C99_designator
	| __C99_designator_seq __C99_designator
	;
__C99_designator:
	  TOK_DOT _anyid
	| TOK_LEFT_BRACKET const_expr TOK_RIGHT_BRACKET
 	;
 */

// Exception handling

try_block:
	  TOK_TRY compound_stmt handler_seq
	  { $$ = as_try_block_statement::create( loc($1), $2, UNWRAP($3) ); }
	;

func_try_block:
	  TOK_TRY _packed_compound_stmt _func_handler_seq
	  { $$ = as_function_definition::create( loc($1), EMPTY_LIST(as_declaration_specifier), DUMMY_DCLTOR, EMPTY_LIST(as_mem_initializer), $2, UNWRAP($3) ); }
	;

handler_seq:
	  handler _disambiguate_expr_stmt_vs_block_decl_vs_label
	  { NEW_WRAPPER_W1( $$, as_handler_seq, loc($1), as_handler, $1 ); }
	| handler_seq handler _disambiguate_expr_stmt_vs_block_decl_vs_label
	  { $$ = $1; WRAPPER_APPEND( $$, $2 ); }
	;

handler:
	  TOK_CATCH TOK_LEFT_PAR excptn_decl TOK_RIGHT_PAR compound_stmt
	  { $$ = as_handler::create( loc($1), $3, $5 ); }
	;

/* sequence of handlers in function-try-block; no disambiguation needed */
_func_handler_seq:
	  handler
	  { NEW_WRAPPER_W1( $$, as_handler_seq, loc($1), as_handler, $1 ); }
	| _func_handler_seq handler
	  { $$ = $1; WRAPPER_APPEND( $$, $2 ); }
	;

excptn_decl:
	  _type_spec_seq_not_declaring_class_enum _param_dcltor
	  { $$ = as_exception_declaration_single_type_with_variable::create( loc($1), UNWRAP($1), $2 ); }
	| _type_spec_seq_not_declaring_class_enum
	  { $$ = as_exception_declaration_single_type::create( loc($1), UNWRAP($1) ); }
	// has to be last handler
	| TOK_DOT_DOT_DOT
	  { $$ = as_exception_declaration_any_type::create( loc($1) ); }
	;

throw_expr:
	  TOK_THROW assign_expr
	  { $$ = as_expression_throw::create( loc($1), $2 ); }
	| TOK_THROW
	  { $$ = as_expression_throw::create( loc($1), EMPTY_EXPR(loc($1)) ); }
	;

excptn_spec_opt:
	  _no_token
	  { $$ = as_excptn_spec_opt_bearer::create( loc($1), NULL ); }
	| excptn_spec
	  { $$ = as_excptn_spec_opt_bearer::create( loc($1), $1 ); }
	;

excptn_spec:
	  TOK_THROW TOK_LEFT_PAR TOK_RIGHT_PAR
	  { $$ = as_exception_specification::create( loc($1), EMPTY_LIST(as_type_id) ); }
	| TOK_THROW TOK_LEFT_PAR type_id_list TOK_RIGHT_PAR
	  { $$ = as_exception_specification::create( loc($1), UNWRAP($3) ); }
	;

type_id_list:
	  type_id
	  { NEW_WRAPPER_W1( $$, as_type_id_list, loc($1), as_type_id, $1 ); }
	| type_id_list TOK_COMMA type_id
	  { $$ = $1; WRAPPER_APPEND( $$, $3 ); }
	;

_assign_expr_no_gt:
	  _cond_expr_no_gt
	  { $$ = $1; }
	| _log_or_expr_no_gt TOK_EQ _assign_expr_no_gt
	  { $$ = as_expression_equal::create( loc($2), $1, $3 ); }
	| _log_or_expr_no_gt TOK_PLUS_EQ _assign_expr_no_gt
	  { $$ = as_expression_plus_equal::create( loc($2), $1, $3 ); }
	| _log_or_expr_no_gt TOK_MINUS_EQ _assign_expr_no_gt
	  { $$ = as_expression_minus_equal::create( loc($2), $1, $3 ); }
	| _log_or_expr_no_gt TOK_STAR_EQ _assign_expr_no_gt
	  { $$ = as_expression_star_equal::create( loc($2), $1, $3 ); }
	| _log_or_expr_no_gt TOK_SLASH_EQ _assign_expr_no_gt
	  { $$ = as_expression_slash_equal::create( loc($2), $1, $3 ); }
	| _log_or_expr_no_gt TOK_PERCENT_EQ _assign_expr_no_gt
	  { $$ = as_expression_percent_equal::create( loc($2), $1, $3 ); }
	| _log_or_expr_no_gt TOK_HAT_EQ _assign_expr_no_gt
	  { $$ = as_expression_hat_equal::create( loc($2), $1, $3 ); }
	| _log_or_expr_no_gt TOK_AMP_EQ _assign_expr_no_gt
	  { $$ = as_expression_amp_equal::create( loc($2), $1, $3 ); }
	| _log_or_expr_no_gt TOK_VBAR_EQ _assign_expr_no_gt
	  { $$ = as_expression_vbar_equal::create( loc($2), $1, $3 ); }
	| _log_or_expr_no_gt TOK_GT_GT_EQ _assign_expr_no_gt
	  { $$ = as_expression_greater_greater_equal::create( loc($2), $1, $3 ); }
	| _log_or_expr_no_gt TOK_LT_LT_EQ _assign_expr_no_gt
	  { $$ = as_expression_less_less_equal::create( loc($2), $1, $3 ); }
	// not possible
	//| throw_expr
	;

// 5.16 Conditional operator
_cond_expr_no_gt:
	  _log_or_expr_no_gt
	  { $$ = $1; }
	  /* CHECK: what do they mean by `nested' in [14.1/15], [14.2/3]?
	   * in the expr here nested?
	   */
	| _log_or_expr_no_gt TOK_QMARK expr TOK_COLON _assign_expr_no_gt
	  { $$ = as_expression_qmark::create( loc($2), $1, $3, $5 ); }
	;

// 5.15 Logical OR operator
_log_or_expr_no_gt:
	  _log_and_expr_no_gt
	  { $$ = $1; }
	| _log_or_expr_no_gt TOK_VBAR_VBAR _log_and_expr_no_gt
	  { $$ = as_expression_vbar_vbar::create( loc($2), $1, $3 ); }
	;

// 5.14 Logical AND operator
_log_and_expr_no_gt:
	  _or_expr_no_gt
	  { $$ = $1; }
	| _log_and_expr_no_gt TOK_AMP_AMP _or_expr_no_gt
	  { $$ = as_expression_amp_amp::create( loc($2), $1, $3 ); }
	;

// 5.13 Bitwise inclusive OR operator
_or_expr_no_gt:
	  _xor_expr_no_gt
	  { $$ = $1; }
	| _or_expr_no_gt TOK_VBAR _xor_expr_no_gt
	  { $$ = as_expression_vbar::create( loc($2), $1, $3 ); }
	;

// 5.12 Bitwise exclusive OR operator
_xor_expr_no_gt:
	  _and_expr_no_gt
	  { $$ = $1; }
	| _xor_expr_no_gt TOK_HAT _and_expr_no_gt
	  { $$ = as_expression_hat::create( loc($2), $1, $3 ); }
	;

// 5.11 Bitwise AND operator
_and_expr_no_gt:
	  _eq_expr_no_gt
	  { $$ = $1; }
	| _and_expr_no_gt TOK_AMP _eq_expr_no_gt
	  { $$ = as_expression_amp::create( loc($2), $1, $3 ); }
	;

// 5.10 Equality operators
_eq_expr_no_gt:
	  _rel_expr_no_gt
	  { $$ = $1; }
	| _eq_expr_no_gt TOK_EQ_EQ _rel_expr_no_gt
	  { $$ = as_expression_equal_equal::create( loc($2), $1, $3 ); }
	| _eq_expr_no_gt TOK_EXCLAMATION_EQ _rel_expr_no_gt
	  { $$ = as_expression_exclam_equal::create( loc($2), $1, $3 ); }
	;

// 5.9 Relational operators
_rel_expr_no_gt:
	  shift_expr
	  { $$ = $1; }
	| _rel_expr_no_gt TOK_LT shift_expr
	  { $$ = as_expression_less::create( loc($2), $1, $3 ); }
	// this is the "nasty" one ;-)
	// CHECK that shift_expr cannot contain a TOK_LT
	//| rel_expr TOK_GT shift_expr
	| _rel_expr_no_gt TOK_LT_EQ shift_expr
	  { $$ = as_expression_less_equal::create( loc($2), $1, $3 ); }
	| _rel_expr_no_gt TOK_GT_EQ shift_expr
	  { $$ = as_expression_greater_equal::create( loc($2), $1, $3 ); }
	;

%%

static void try_ctor( ptr<as_declaration_specifier_seq> const type, ptr<bison_token> const next )
{
	(void)type;
	// XXX in real world, we will ask about the passed type
	bison_token::hint_type hint = next->user_hint_get();
	if (hint == bison_token::HINT_NONE) {
		llog(pl) << "unhinted left parenthesis, asuming no ctor\n";
	} else if (hint == bison_token::HINT_CTOR) {
		try_begin( "ctor" );
		try_and_commit( TOK_START_CTOR, TOK_PAD_CTOR, r );
		try_end_skip();
	} else {
		lassert( hint == bison_token::HINT_NO_CTOR );
	}
}

static void disambiguate_ctor_and_bitfield_class_scope( ptr<as_declaration_specifier_seq> const type )
{
	ptr<bison_token> next = manager::peek();
	bison_token::hint_type hint = next->user_hint_get();
	switch (next->type_get()) {
	case TOK_LEFT_PAR:
		// XXX	in real world, we will ask about the passed type
		//	from it, we might be able to say when the declaration *is not* a ctor
		//	in some cases, we will still have to run the disambiguation
		try_ctor( type, next );
		break;
	case TOK_COLON:
		// XXX in real world, we will ask about the passed type
		if (hint == bison_token::HINT_NONE) {
			llog(pl) << "unhinted colon, asuming no bitfield\n";
		} else if (hint == bison_token::HINT_BITFIELD) {
			manager::start( TOK_START_TO_BE_DISCARDED );
			manager::commit( TOK_PAD_UNNAMED_BITFIELD );
		} else {
			lassert( hint == bison_token::HINT_NO_BITFIELD );
		}
		break;
	}
}

static void disambiguate_ctor_nmspc_scope( ptr<as_declaration_specifier_seq> const type )
{
	ptr<bison_token> next = manager::peek();
	// XXX	in real world, we will ask about the passed type
	//	from it, we might be able to say when the declaration *is not* a ctor
	//	in some cases, we will still have to run the disambiguation
	if (next->type_get() == TOK_LEFT_PAR)
		try_ctor( type, next );
}
