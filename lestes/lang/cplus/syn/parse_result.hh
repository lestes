/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__syn__parse_result_type_hh__included
#define lestes__lang__cplus__syn__parse_result_type_hh__included

/*! \file
 * \brief Class for holding result of a parser run: success and the final token.
 *
 * For error reporting, we want to know the token (its location, actually) on
 * which the parser failed. Parser's parse() method returns an instance of
 * parse_result_type class which contains both the bool value (success) and the
 * last token read by the parser.
 *
 * \author Rudo
 */

#include <lestes/common.hh>
#include <lestes/intercode/intercode.g.hh>

package(lestes);
package(lang);
package(cplus);
package(syn);

class bison_token;

/*!
 * \brief Class that contains data about a finished parser run.
 *
 * At the moment, it holds
 * - a boolean success flag
 * - token that was lat read by the parser
 * - as tree of the parsed input
 *
 * The AS tree is here primarily for debugging purposes. It has broad type
 * (as_base) and is only set when successfully parsing for real. This means
 * that failed runs and even successful disambiguation runs store NULL here.
 *
 */
class parse_result_type : public ::lestes::std::object {
public:
	bool success_get() const;
	ptr<bison_token> last_token_get() const;
	ptr< ::lestes::intercode::as_base > as_result_get() const;
	/*!
	 * \brief Creates an instance of the class.
	 *
	 * a_as_result is not const reference, as we allow literal NULL to
	 * be passed in. (Nobody is doing that at the moment, though.)
	 */
	static ptr<parse_result_type> create( bool a_success,
			const ptr<bison_token> & a_last_token,
			ptr< ::lestes::intercode::as_base > a_as_result );
protected:
	parse_result_type( bool a_success, const ptr<bison_token> & a_last_token,
			ptr< ::lestes::intercode::as_base > a_as_result );
	virtual void gc_mark();
private:
	const bool success;
	const srp<bison_token> last_token;
	const srp< ::lestes::intercode::as_base > as_result;
};

end_package(syn);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif	// lestes__lang__cplus__syn__parse_result_type_hh__included
