/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__syn__token_hh__included
#define lestes__lang__cplus__syn__token_hh__included

#include <lestes/common.hh>
#include <lestes/intercode/intercode.g.hh>
#include <lestes/lang/cplus/lex/cpp_token.hh>
#include <lestes/lang/cplus/lex/token_value.hh>
#include <lestes/lang/cplus/sem/lu_typedef.hh>
#include <lestes/std/list.hh>
#include <lestes/std/set.hh>
#include <lestes/std/source_location.hh>

#include <iosfwd>

package(lestes);
package(lang);
package(cplus);
package(syn);

class bison_token : public ::lestes::intercode::as_base {
public:
	// enum yytokentype:
#include <lestes/lang/cplus/syn/cpp_token_type_enum.s.hh> 
	typedef ::lestes::lang::cplus::lex::cpp_token wrapped_token_type;
	enum hint_type {
		HINT_NONE,

		HINT_TEMPL_NONTYPE,
		HINT_TEMPL_TYPE,
		HINT_NONTYPE,
		HINT_CLASS,
		HINT_ENUM,
		HINT_TYPEDEF,
		HINT_NAMESPACE,
		HINT_UNKNOWN,
		
		HINT_CTOR,
		HINT_NO_CTOR,

		HINT_BITFIELD,
		HINT_NO_BITFIELD,
	};
private:
	//! type of the token; this is what bison cares about
	const int type;
protected:
	//! pointer to token read from lex
	/*!
	 * Must not be NULL when the real type is bison_token.
	 * In other words, derived classes are allowed to set it to NULL.
	 * All the methods that use the value are virtual and the implementations in
	 * this class (bison_token, that is) always check the value.
	 */
	const srp<wrapped_token_type> wrapped_token;
private:
	hint_type user_hint;
protected:
	bison_token( const ptr<source_location> & a_location, int a_type,
			const ptr<wrapped_token_type> & a_token_to_wrap ) :
		as_base(a_location), type(a_type),
		wrapped_token(a_token_to_wrap), user_hint(HINT_NONE)
	{}
	void gc_mark();
public:
	static ptr<bison_token> create( ptr<wrapped_token_type> a_token_to_wrap )
	{
		// we check the value here, not in the constructor
		// this allows derived classes to set wrapped_token to NULL
		return new bison_token( a_token_to_wrap->location_get(),
				a_token_to_wrap->type_get(),
				checked(a_token_to_wrap) );
	}
	int type_get() const
	{
		return type;
	}
	virtual ucn_string value_get() const
	{
		return checked(wrapped_token)->value_get()->content_get();
	}
	virtual ptr<wrapped_token_type> wrapped_token_get() const
	{
		return checked(wrapped_token);
	}
	void user_hint_set( hint_type hint )
	{
		lassert2( hint != HINT_NONE, "Trying to set user hint to NONE." );
		lassert2( user_hint == HINT_NONE, "User hint already set." );
		user_hint = hint;
	}
	hint_type user_hint_get() const
	{
		return user_hint;
	}
	virtual void print( ::std::ostream & ) const;
/* the dump part: */
private:
	static ptr<reflection_list> reflection;
public:
	virtual ptr<reflection_list> reflection_get() const;
	virtual ptr<field_list_list> field_values_get() const;
};

class bison_madeup_token : public bison_token {
protected:
	bison_madeup_token( const ptr<source_location> & a_location, int a_type ) :
		bison_token( a_location, a_type, NULL )
	{}
public:
	static ptr<bison_madeup_token> create( const ptr<source_location> & a_location, int a_type )
	{
		return new bison_madeup_token( a_location, a_type );
	}
	virtual ucn_string value_get() const
	{
		lassert2( false, "value_get() called on a made up token." );
	}
	virtual ptr<wrapped_token_type> wrapped_token_get() const
	{
		lassert2( false, "wrapped_token_get() called on a made up token." );
	}
	virtual void print( ::std::ostream & ) const;
/* the dump part: */
private:
	static ptr<reflection_list> reflection;
public:
	virtual ptr<reflection_list> reflection_get() const;
	virtual ptr<field_list_list> field_values_get() const;
};

class bison_id_token : public bison_token {
public:
	enum hinted_by_type {
		BY_HINTER,
//		BY_BOTH,
		BY_USER,
	};
	ptr< ::lestes::lang::cplus::sem::declaration_set_type > found_decls_get() const;
private:
	const hinted_by_type hinted_by;
	srp< ::lestes::lang::cplus::sem::declaration_set_type > found_decls;
protected:
	bison_id_token( const ptr<source_location> & a_location, int a_value,
			const ptr<wrapped_token_type> & a_token_to_wrap,
			hinted_by_type a_hinted_by,
			const ptr< ::lestes::lang::cplus::sem::declaration_set_type > & a_found_decls )
		: bison_token( a_location, a_value, a_token_to_wrap ),
		hinted_by(a_hinted_by), found_decls(a_found_decls)
	{}
	void gc_mark();
public:
	static ptr<bison_id_token> create( int a_type,
			const ptr<wrapped_token_type> & a_token_to_wrap,
			hinted_by_type a_hinted_by,
			const ptr< ::lestes::lang::cplus::sem::declaration_set_type > & a_found_decls )
	{
		return new bison_id_token( a_token_to_wrap->location_get(),
				a_type, a_token_to_wrap, a_hinted_by,
				a_found_decls );
	}
	virtual void print( ::std::ostream & ) const;
/* the dump part: */
private:
	static ptr<reflection_list> reflection;
public:
	virtual ptr<reflection_list> reflection_get() const;
	virtual ptr<field_list_list> field_values_get() const;
};

class bison_pack_token : public bison_madeup_token {
private:
	typedef list< srp<bison_token> > token_list_type;
	const srp<token_list_type> pack;
	const srp<source_location> end_location;
#if 0 || BISON_PACK_TOKEN_ERRORS_WANTED
	//! when this pack_token is used in error recovery, errors contains tokens at which sytax error(s) occured
	const srp<token_list_type> errors;
#endif
protected:
	bison_pack_token( const ptr<source_location> & a_location, int a_type,
			ptr<token_list_type> a_pack,
			const ptr<source_location> & a_end_location
#if 0 || BISON_PACK_TOKEN_ERRORS_WANTED
			,ptr<token_list_type> a_errors
#endif
			) :
		bison_madeup_token( a_location, a_type ),
		pack(a_pack), end_location(a_end_location)
#if 0 || BISON_PACK_TOKEN_ERRORS_WANTED
		,errors(a_errors)
#endif
	{}
	void gc_mark();
public:
	static ptr<bison_pack_token> create( const ptr<source_location> & a_location,
			int a_type, const ptr<source_location> a_end_location )
	{
		lassert( a_type > bison_token::_PACK_FIRST && a_type < bison_token::_PACK_LAST );
		return new bison_pack_token( a_location, a_type,
				token_list_type::create(), a_end_location
#if 0 || BISON_PACK_TOKEN_ERRORS_WANTED
				,token_list_type::create()
#endif
				);
	}
	ptr<token_list_type> pack_get() const
	{
		return pack;
	}
	ptr<source_location> end_location_get() const
	{
		return end_location;
	}
#if 0 || BISON_PACK_TOKEN_ERRORS_WANTED
	ptr<token_list_type> errors_get() const
	{
		return errors;
	}
#endif
	virtual void print( ::std::ostream & ) const;
/* the dump part: */
private:
	static ptr<reflection_list> reflection;
public:
	virtual ptr<reflection_list> reflection_get() const;
	virtual ptr<field_list_list> field_values_get() const;
};

::std::ostream & operator << ( ::std::ostream &, const ptr<bison_token> & );

end_package(syn);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif	// lestes__lang__cplus__syn__token_hh__included
