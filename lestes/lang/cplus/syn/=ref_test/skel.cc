/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <iostream>
#include <lestes/common.hh>

using namespace ::lestes;

// note that gcc takes care not to produce ">>", even then type below ends with a ">"
#define body( type )								\
	static ptr<type> create( int i ) { return ptr<type>( new type(i) ); }	\
	virtual void print() { ::std::cout << #type " " << i << ::std::endl; }

#define ctor( type, base )	type( int a_i ) : base(a_i) {}

struct r : object { int i; r(int a_i) : i(a_i) {} body(r); };
struct t1 : public r { ctor(t1,r); body(t1); };
struct t11 : public t1 { ctor(t11,t1); body(t11); };
struct t2 : public r { ctor(t2,r); body(t2); };

template<typename T>
class semref {
	template<typename Y>
	friend class semref;
private:
	ptr<r> & ref;
public:
	explicit semref<T>( ptr<r> & a_ref ) : ref(a_ref)
	{}
	void operator= ( const ptr<T> & p ) const
	{
		ref = p;
	}
	void operator= ( const semref<T> & a ) const
	{
		// run-time check if `a' really references ptr<T> or class derived from it
		a.ref.template dncast<T>();
		ref = a.ref;
	}
	template<typename Y> void operator= ( const semref<Y> & a ) const
	{
		// run-time check if `a' really references ptr<Y> or class derived from it
		// compile-time enforcement that Y is derived from T
		ptr<T> tmp = a.ref.template dncast<Y>();
		ref = tmp;
	}
	ptr<T> operator-> () const
	{
		return operator ptr<T>();
	}
	operator ptr<T> () const
	{
		// T below is intentional
		return ref.template dncast<T>();
	}
	// the following one not only makes ptr<T> from semval<T>, but also ptr<Y> when Y is base class of T
	template<typename Y>
	operator ptr<Y> () const
	{
		// T below is intentional
		return ref.template dncast<T>();
	}
	operator srp<T> () const
	{
		// T below is intentional
		return srp<T>(ref.template dncast<T>());
	}
	// the following one not only makes ptr<T> from semval<T>, but also ptr<Y> when Y is base class of T
	template<typename Y>
	operator srp<Y> () const
	{
		// T below is intentional
		return srp<Y>(ref.template dncast<T>());
	}
};

class semval
{
private:
	ptr<r> pp;
public:
	explicit semval() : pp() {}
	template<typename T> semref<T> select()
	{
		return semref<T>(pp);
	}
};

#define l( x ) do ::std::cerr << #x << ::std::endl; while (false)

// emulates bison's $<type>$ expansion
#define bis( type ) select<type>()

typedef semval YYSTYPE;

struct dummy {};

YYSTYPE v, v1, v11, v2;

int main()
{
	v.bis(r) = r::create(0);
	v1.bis(t1) = t1::create(1);
	v11.bis(t1) = t11::create(11);
	v2.bis(t2) = t2::create(2);

	// this one should get #defined
	test_line;

	return 2;
}
