/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*
 * Example demonstrating as->ss expressions transformation and overload resolution, specifically
 * for the reference binding.
 *
 * Two main things are demonstrated here - that reference binding works for parameters and in return
 * statements, and also that overload resolution works correctly with respect to references.
 *
 * \author jikos
 */

int a;
int *p;

/* This function is not chosen when calling f(a), because direct reference binding int -> int& is
 * better.
 *
 * ! If one changes the parameter from long to int, the compilation will correctly fail, because
 * ! it would not be possible for overload resolution to succeed - both direct reference binding 
 * ! and int->int are exact conversions, so the overload resolution is ambiguous
 * This is demonstrated in deconstruct_test_reference_fail.cc
 */
int f(long z)
{
	return a;
}

/* Thing to be noted here is that also for the return parameter the reference binding is done, 
 * because int is returned from fucntion having type of int&
 */
int & f(int &z) 
{
	return a;
}

int main(int argc, char *argv[])
{ 
	f(a);
	p = &a;
}

