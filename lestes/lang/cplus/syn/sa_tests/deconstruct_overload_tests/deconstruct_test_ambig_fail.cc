/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*
 * Example demonstrating as->ss expressions transformation and overload resolution.
 * 
 * This is a little modification of decontruct_test.cc example. This example is intended
 * to fail, as the overload resolution is ambiguous here - conversions from g (signed int)
 * to either signed long or unsigned long are ambiguous.
 *
 * \author jikos
 */
int a,b,c,d,e,g;
long l;

long f(signed long z) 
{
	a = b+c;
	return a;
}

long f(unsigned long z) 
{
	return l;
}

int main(int argc, char *argv[])
{ 
	d = e + f(g);
	return d;
}

