#! /bin/sh
#
# The lestes compiler suite
# Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
# Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
# Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
# Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
# Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
# Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
# Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# See the full text of the GNU General Public License version 2, and
# the limitations in the file doc/LICENSE.
#
# By accepting the license the licensee waives any and all claims
# against the copyright holder(s) related in whole or in part to the
# work, its use, and/or the inability to use it.
#

echo ""
echo ""
echo " The following test should fail, it is correct behavior."
echo ""
echo "PARSER deconstruct_test_ptr_crement_fail.cc"
if [ -f deconstruct.llc ]; then
	../../parser.test -h -l deconstruct.llc -f ss_deconstruct_test_ptr_crement_fail.xml -o as_deconstruct_test_ptr_crement_fail.xml flat < deconstruct_test_ptr_crement_fail.cc
else
	../../parser.test -h -f ss_deconstruct_test_ptr_crement_fail.xml -o as_deconstruct_test_ptr_crement_fail.xml flat < deconstruct_test_ptr_crement_fail.cc
fi
if ( ! [ $? -eq 0 ]; ) then
	exit 0
else
	exit 1
fi	
