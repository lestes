/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*
 * Example demonstrating as->ss expressions transformation and overload resolution, specifically
 * for the postfix and prefix ++ and -- operators for integers and pointers.
 *
 * From the resulting dump, there can be seen what is the difference between postfix and prefix 
 * increment (can be interchanged for decrement). The main things to notice are
 * 
 * 	- the result of prefix operation is lvalue, and therefore can be used 
 *	  further in expression expecting lvalue (e.g. another increment/decrement).
 *	- the result of the prefix operation (as seen by 'upper' expressions) is the
 * 	  assignment operation itself. 
 *	- the result of the postfix operation (as seen by 'upper' expressions) is the
 *	  + (or - respectively) operation.
 *
 * \author jikos
 */

int *ptr;
int i;

int main()
{ 
	++(++ptr);
	(++ptr)++;

	++(++(++i));
	(++i)++;
}

