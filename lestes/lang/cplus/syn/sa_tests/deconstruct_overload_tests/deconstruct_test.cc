/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*
 * Example demonstrating as->ss expressions transformation and overload resolution.
 * 
 * There are two functions f() to demonstrate that the proper one (with the 'cheapest'
 * conversion) will be chosen - in our case it is the one taking int parameter.
 *
 * The functions of type 'long' are returning type int to demonstrate that the integral
 * conversions work.
 *
 * Functions operator+ and operator= for the operators used in this source are chosen
 * by overload resolution process from the declarations described in [13.6].
 *
 * Just before leaving the main() function, some pointer arithmetics is performed.
 * More on this in special pointer tests in this directory.
 *
 * \author jikos
 */
int a,b,c,d,e,g,i;
long l;
int *p1, *p2;
bool t;

/* This function demonstrates that it is possible to return lvalue from function
 * returning reference, and in dumps it can be seen that proper reference binding
 * has been made.
 *
 * This also demonstrates, that when this function is called with lvalue argument,
 * that reference binding for argument is also performed
 */
int & ref_func(int &ref_par)
{
	return a;
}

long f(int z) 
{
	z = b+c;
	/* to test that the return value will be converted from int to long */
	return z;
}

long f(double z) 
{
	return l;
}

int main(int argc, char *argv[])
{ 
	/* to test sequence points, sideeffects, overload resolution and function call */
	d = e + f(g);

	/* to test pointer arithmetics, assignment and lvalueness of prefix ++/-- */
	p1 = p1++;
	p1 = p1+1;
	p1 += 12;
	++p2;
	p1 = p2;
	p2 = ++(++p1);

	a < b;
	/* pointer arithmetics and comparsions with bool result */
	i = p1 - p2;
	l = p1 - p2;
	t = p1 > p2;
	t = p1 >= p2;
	t = p1 <= p2;

	/* In dumps one can see that reference binding is done, because parameter type is
	 * int and the function takes reference
	 */
	ref_func(a);
	
	/* This demonstrates that address_of is processed correctly */
	p1 = &a;
	
	return ++d;
}

