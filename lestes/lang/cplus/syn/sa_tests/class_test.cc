/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
//==> lestes/lang/cplus/syn/tests/ctortest.cc <==
class _hint_unknown X;
class _hint_unknown Y;
class _hint_unknown A {
	int _hint_bitfield :3, _hint_unknown a:3, :3, :0;
	int _hint_unknown b:3, :3, _hint_unknown c:3;
/* hmm, these do not produce warnings...
	class _hint_class warn1 : public _hint_class irrelevant {};
	class _hint_class quiet1 _hint_no_bitfield : public _hint_class irrelevant {};
*/
	// int :3; //would cause an error (correctly :)
	_hint_class A _hint_ctor ();
	(_hint_class A (int));
	_hint_class A _hint_ctor (),
	(_hint_class X) (int),
	~ _hint_class Y() = 0,
	(operator int)();

	// ill-formed, but should pass the parser:
	//operator void( int ), (operator char)() = 0;
	//(operator void( int )), (_hint_class X(long double));
		
	//int ( _hint_unknown warn2 ) ( int );
	//int _hint_no_ctor ( _hint_unknown quiet2 ) ( unsigned char const );
};

_hint_class A :: _hint_class A _hint_unknown y; // ill-formed [3.4.3.1/1a]
_hint_class A :: _hint_class A _hint_ctor (_hint_unknown z);

//==> lestes/lang/cplus/syn/tests/packtest.cc <==
int _hint_unknown f()
{
	class {};
	class _hint_unknown UNKN {
	//class {
		int _hint_unknown g()
		{
			int _hint_unknown i;
		}
	};
}

//==> lestes/lang/cplus/syn/tests/test1.cc <==
namespace _hint_unknown n
{
	;
	class _hint_unknown xx;
	int _hint_unknown bla;
	int _hint_unknown x;
	int _hint_class xx = _hint_nontype bla;
	int _hint_unknown ff(), _hint_unknown gg();
}

int & _hint_unknown yy =
_hint_namespace n::
_hint_nontype x;

int _hint_unknown f1();

void _hint_unknown f13213( _hint_class A * _hint_unknown o23 )
{
	if (4)
	{
		class _hint_unknown T;
		class _hint_unknown c;
		//_hint_class T( _hint_class c);
		//_hint_templ_nontype f( _hint_nontype c );
	}
	else
		;
}

//==> lestes/lang/cplus/syn/tests/test2.cc <==
class _hint_unknown B : public _hint_class A {};
class _hint_unknown BB {};
_hint_class A _hint_unknown ff(int(_hint_unknown k));
_hint_class B _hint_unknown fg((int(_hint_unknown l)));

void _hint_unknown func() {
	_hint_class T (_hint_nontype f);
_hint_unknown bla:
	_hint_class U (_hint_nontype g) + 4;

	int _hint_unknown a = 3, _hint_unknown x();
	int ( _hint_unknown b = 3 ), _hint_unknown y();
}

int _hint_unknown i = 0;

//==> lestes/lang/cplus/syn/tests/test3.cc <==
void _hint_unknown a() {
	int (_hint_unknown blaa(int(_hint_unknown g) = 1));
	int (_hint_unknown blae(int(_hint_unknown g) = 1)) + 1;
}

//==> lestes/lang/cplus/syn/tests/test4.cc <==
//void _hint_unknown f( int * );

template <>
	void _hint_unknown f();

typedef _hint_class A _hint_unknown D;

template <class _hint_class A>
	void _hint_unknown f();
	
template <class _hint_class B _hint_unknown b>
	void _hint_unknown g();

class _hint_unknown works;

class _hint_unknown C {
	class _hint_unknown X;
};

