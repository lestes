/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__syn__manager_hh__included
#define lestes__lang__cplus__syn__manager_hh__included

#include <lestes/common.hh>
#include <lestes/std/action.g.hh>
#include <lestes/std/event.hh>
#include <lestes/std/list.hh>

package(lestes);
package(std);
class source_location;
end_package(std);
package(lang);
package(cplus);
package(lex);
class preprocessor;
end_package(lex);
package(syn);

class bison_token;
class bison_pack_token;

class manager {
public:
	typedef ::lestes::lang::cplus::lex::preprocessor preprocessor;
	static void init();

	static void spawn( ptr<bison_pack_token> );
	static void close();

	static ptr<bison_token> yylex();
	//! Returns value identical to the result of previous yylex() invocation.
	static ptr<bison_token> prev_yylex();
	//! Same as yylex(), but does not move to the next token.
	static ptr<bison_token> peek();
	static void back_up();

	static void start( int );
	static void commit( int );
	static void rollback();

	//! Packs the stream to form a pack token with given type and returns it.
	static ptr<bison_pack_token> pack( int, const ptr<source_location> & );
	//! Unpacks current token; current token is the one returned by peek(), it has to be a pack.
	static void unpack();

	static bool in_disambiguation();

	static void install_undo_action( ptr<action> );

	static bool failing_get();
	static void failing_set_true();

	static ptr<event> spawn_event;
	static ptr<event> close_event;
	static ptr<event> start_event;
	static ptr<event> commit_event;
	static ptr<event> rollback_event;

private:
	//! Do no allow instantiating this class.
	manager();
	//! Hide copy-constructor.
	manager( const manager & );
};

end_package(syn);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif	// lestes__lang__cplus__syn__manager_hh__included
