/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/lang/cplus/syn/parse_result.hh>
#include <lestes/lang/cplus/syn/token.hh>

package(lestes);
package(lang);
package(cplus);
package(syn);

using namespace ::lestes::intercode;

bool parse_result_type::success_get() const
{
	return success;
}

ptr<bison_token> parse_result_type::last_token_get() const
{
	return last_token;
}

ptr<as_base> parse_result_type::as_result_get() const
{
	return as_result;
}

ptr<parse_result_type> parse_result_type::create( bool a_success,
		const ptr<bison_token> & a_last_token,
		ptr<as_base> a_as_result )
{
	return new parse_result_type( a_success, a_last_token, a_as_result );
}

parse_result_type::parse_result_type( bool a_success,
		const ptr<bison_token> & a_last_token,
		ptr<as_base> a_as_result )
	: success(a_success), last_token(a_last_token), as_result(a_as_result)
{}

void parse_result_type::gc_mark()
{
	last_token.gc_mark();
	object::gc_mark();
}

end_package(syn);
end_package(cplus);
end_package(lang);
end_package(lestes);
