/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
class c {};
typedef c b;

int f()
{
	class i {};
	int a = 1;
	for (int _hint_class i = 3; bool _hint_class b = _hint_nontype i < 10; i++, _hint_nontype b++ ) {
		a++;
	}
	_hint_class i j;	// we do not see 'i' from the for statement
	if (int x = 3) {
		a++;
	} else
		a--;
	int _hint_unknown x;	// we do not see 'x' from the if statement
	while (int y = a)
		a--;
	int _hint_unknown y;	// we do not see 'y' from the while statement
	return a;
}
