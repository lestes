/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/common.hh>
#include <lestes/std/source_location.hh>

package(lestes);
package(lang);
package(cplus);
package(syn);

class bison_pack_token;

class syntax_error {
public:
	//! Report that statement disambiguation failed.
	static void disa_stmt( ptr<bison_pack_token> error_pack,
		ptr<source_location> suberr_loc1, ptr<source_location> suberr_loc2 );
	//! Reports error in condition.
	static void condition( ptr<bison_pack_token> error_pack,
		ptr<source_location> suberr_loc1, ptr<source_location> suberr_loc2 );
private:
	//! Hide default constructor.
	syntax_error();
	//! Hide copy-constructor.
	syntax_error( const syntax_error & );
};

end_package(syn);
end_package(cplus);
end_package(lang);
end_package(lestes);;
