/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/common.hh>
#include <lestes/intercode/intercode.g.hh>
#include <lestes/lang/cplus/sem/as_decl.g.hh>
#include <lestes/lang/cplus/sem/as_declarator_op2op_func.g.hh>
#include <lestes/lang/cplus/sem/as_expr.g.hh>
#include <lestes/lang/cplus/sem/as_other.g.hh>
#include <lestes/lang/cplus/sem/as_statements.g.hh>
#include <lestes/lang/cplus/sem/sa_class_declaration.g.hh>
#include <lestes/lang/cplus/sem/sa_context.g.hh>
#include <lestes/lang/cplus/sem/sa_loggers.hh>
#include <lestes/lang/cplus/sem/sa_namespace.g.hh>
#include <lestes/lang/cplus/sem/sa_simple_declaration.g.hh>
#include <lestes/lang/cplus/sem/sa_statements.g.hh>
#include <lestes/lang/cplus/sem/sa_usings.g.hh>
#include <lestes/lang/cplus/sem/ss_misc.g.hh>
#include <lestes/lang/cplus/syn/errors.hh>
#include <lestes/lang/cplus/syn/errors.m.hh>
#include <lestes/lang/cplus/syn/hinter.g.hh>
#include <lestes/lang/cplus/syn/hinter.hh>
#include <lestes/lang/cplus/syn/manager.hh>
#include <lestes/lang/cplus/syn/parse_result.hh>
#include <lestes/lang/cplus/syn/parser.hh>
#include <lestes/lang/cplus/syn/semval.hh>
#include <lestes/lang/cplus/syn/syn.hh>
#include <lestes/lang/cplus/syn/token.hh>
#include <lestes/msg/logger.hh>
#include <lestes/msg/logger_util.hh>
#include <lestes/std/dumper.hh>
#include <lestes/std/source_location.hh>

#include <iostream>
#include <iterator>
#include <algorithm>
#include <fstream>

package(lestes);
package(lang);
package(cplus);
package(syn);

declare_logger( pl );
initialize_logger( pl, "parser", syn_logger );

// ugly, but we do not want to fully qualify as_* types all the time
using namespace ::lestes::intercode;
using namespace ::lestes::lang::cplus::sem;

static int yylex( semval *sv )
{
	ptr<bison_token> t = manager::yylex();
	llog(pl) << "yylex: " << t << "\n";
	sv->select<bison_token>() = t;
	return t->type_get();
}

// interface mandated by bison
static void yyerror( ptr<as_base> &, const char * str )
{
	llog(pl) << "yyerror: " << str << "\n";
	// do not report errors when trying to resolve an ambiguity
	// nor when we are expecting more errors to follow (we are failing)
	if (!manager::in_disambiguation() && !manager::failing_get()) {
		ptr<bison_token> prev = manager::prev_yylex();
		// supress confusing message about TOK_EOF -- it might be end
		//   of a function body just as well
		if (prev->type_get() == bison_token::TOK_EOF)
			report << bad_end << prev->location_get();
		else
			report << parse_error << lstring(str) << prev->location_get();
	}
}

void parser::init()
{
	// there used to be something here :-)
}

// do not include enum yytokentype in the result; it is included in token.hh
#define YYTOKENTYPE

/* hack to allow compilation under some versions of g++ that do not allow __attribute__ after a label */
#define __attribute__( x )

#include <lestes/lang/cplus/syn/parser.tab.cc>

#undef __attribute__

ptr<parse_result_type> parser::parse()
{
	ptr<as_base> as_result = NULL;
	llog_xml_open(pl,"parse") << "\n";
	bool success = (yyparse(as_result) == 0);
	llog_xml_close(pl,"parse") << "\n";
	return parse_result_type::create( success, manager::prev_yylex(), as_result );
}

const char * parser::token_type2name( int type )
{
	int tok_type = YYTRANSLATE( type );
	lassert( tok_type != YYUNDEFTOK );
	return yytname[tok_type];
}

end_package(syn);
end_package(cplus);
end_package(lang);
end_package(lestes);
