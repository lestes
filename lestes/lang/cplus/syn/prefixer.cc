/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/lang/cplus/lex/preprocessor.hh>
#include <lestes/lang/cplus/syn/prefixer.hh>
#include <lestes/lang/cplus/syn/syn.hh>
#include <lestes/lang/cplus/syn/token.hh>
#include <lestes/msg/logger.hh>

#include <iostream>

package(lestes);
package(lang);
package(cplus);
package(syn);

declare_logger( prl );
initialize_logger( prl, "prefixer", syn_logger );

prefixer::mode_type prefixer::mode;

ptr<prefixer::preprocessor> prefixer::pp;

void prefixer::init( mode_type a_mode, ptr<preprocessor> a_pp )
{
	mode = a_mode;
	pp = a_pp;
}

prefixer::mode_type prefixer::mode_get()
{
	return mode;
}

ptr<bison_token> prefixer::internal_yylex()
{
	return bison_token::create( pp->read() );
}

// this cannot be a standard map<ucn_string,hint_type>, as ucn_string does not support operator <
static struct prefix2hint_map_item {
	const ucn_string value;
	const bison_token::hint_type hint;
} prefix2hint_map[] = {
	{ "_hint_templ_nontype",bison_token::HINT_TEMPL_NONTYPE },
	{ "_hint_templ_type",	bison_token::HINT_TEMPL_TYPE },
	{ "_hint_nontype",	bison_token::HINT_NONTYPE },
	{ "_hint_class",	bison_token::HINT_CLASS },
	{ "_hint_enum",		bison_token::HINT_ENUM },
	{ "_hint_typedef",	bison_token::HINT_TYPEDEF },
	{ "_hint_namespace",	bison_token::HINT_NAMESPACE },
	{ "_hint_unknown",	bison_token::HINT_UNKNOWN },
/* these are special hints; they hint the disambiguation, not the hinter */
	{ "_hint_ctor",		bison_token::HINT_CTOR },
	{ "_hint_no_ctor",	bison_token::HINT_NO_CTOR },
	{ "_hint_bitfield",	bison_token::HINT_BITFIELD },
	{ "_hint_no_bitfield",	bison_token::HINT_NO_BITFIELD },
/* terminating item */
	{ "",			bison_token::HINT_NONE }
};

bison_token::hint_type prefixer::prefix2hint( const ucn_string & str )
{
	const struct prefix2hint_map_item * it = prefix2hint_map;
	// try to match the strings, stop at the end of the array where the string is empty
	while (!(it->value.empty()) && str != it->value)
		++it;
	return it->hint;
}

ptr<bison_token> prefixer::yylex()
{
	ptr<bison_token> tok1 = internal_yylex();
	// when we are off, do not bother examining the token
	if (mode == PREFIX_OFF)
		return tok1;
	bison_token::hint_type hint;
	ptr<bison_token> tok2;
	do {
		// the prefixes are always identifiers
		if (tok1->type_get() != bison_token::TOK_IDENT)
			return tok1;
		hint = prefix2hint( tok1->value_get() );
		if (hint == bison_token::HINT_NONE)
			return tok1;
		tok2 = internal_yylex();
		// if the following token is not an identifier
		//   (or left parenthesis or colon, where appropriate),
		//   ignore the hint completely, warn, and try again
		// also ignore two or more hints in a row
		if (hint == bison_token::HINT_CTOR ||
				hint == bison_token::HINT_NO_CTOR) {
			if (tok2->type_get() == bison_token::TOK_LEFT_PAR)
				break;
		} else if (hint == bison_token::HINT_BITFIELD ||
				hint == bison_token::HINT_NO_BITFIELD) {
			if (tok2->type_get() == bison_token::TOK_COLON)
				break;
		} else if (tok2->type_get() == bison_token::TOK_IDENT) {
			if (prefix2hint(tok2->value_get()) == bison_token::HINT_NONE)
				break;
			else
				;	// hint before another hint
		}
		llog(prl) << "user-specified hint (\"" << tok1->value_get() <<
			"\") irrelevant, discarding\n";
		tok1 = tok2;
	} while (true);
	// now we have the user hint (in hint) and the hinted token (in tok2)
	tok2->user_hint_set(hint);
	return tok2;
}

end_package(syn);
end_package(cplus);
end_package(lang);
end_package(lestes);
