/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/lang/cplus/syn/errors.hh>
#include <lestes/lang/cplus/syn/errors.m.hh>
#include <lestes/lang/cplus/syn/token.hh>

package(lestes);
package(lang);
package(cplus);
package(syn);

void syntax_error::disa_stmt( ptr<bison_pack_token> error_pack,
		ptr<source_location> suberr_loc1, ptr<source_location> suberr_loc2 )
{
#if OLD_ERROR_WANTED
	ptr<source_location> at_loc = error_pack->location_get();
	report << disa_failed << at_loc;
	if (suberr_loc1->order_get() != at_loc->order_get())
		report << sub_error << suberr_loc1;
	if (suberr_loc2->order_get() != at_loc->order_get() &&
			suberr_loc2->order_get() != suberr_loc1->order_get())
		report << sub_error << suberr_loc2;
#else
	report << disa_failed << suberr_loc1;
	if (suberr_loc2->order_get() != suberr_loc1->order_get())
		report << or_here << suberr_loc2;
#endif
	report << cont_here << error_pack->end_location_get();
}

void syntax_error::condition( ptr<bison_pack_token> error_pack,
		ptr<source_location> suberr_loc1, ptr<source_location> suberr_loc2 )
{
	/* if the pack is empty, the condition "body" was empty, report it
	 * note that in for statement -- where empty conditions are allowed -- we handle
	 *   this case without running disambiguation, which means that this method
	 *   is not called then
	 */
	if (error_pack->pack_get()->empty())
		report << empty_condition << error_pack->location_get();
	else {
		report << invalid_cond << suberr_loc1;
		if (suberr_loc2->order_get() != suberr_loc1->order_get())
			report << or_here << suberr_loc2;
	}
}

end_package(syn);
end_package(cplus);
end_package(lang);
end_package(lestes);
