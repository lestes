/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__syn__semval_hh__included
#define lestes__lang__cplus__syn__semval_hh__included

#include <lestes/common.hh>
#include <lestes/intercode/intercode.g.hh>

package(lestes);
package(lang);
package(cplus);
package(syn);

// base type; all referenced pointer have to be to a class derived from it
typedef ::lestes::intercode::as_base semref_type;

template<typename T>
class semref {
	template<typename Y>
	friend class semref;
private:
	ptr<semref_type> & ref;
public:
	explicit semref<T>( ptr<semref_type> & a_ref ) : ref(a_ref)
	{}
	void operator= ( const ptr<T> & p ) const
	{
		ref = p;
	}
	void operator= ( const semref<T> & a ) const
	{
		// run-time check if `a' really references ptr<T> or class derived from it
		ref = a.ref.template dncast<T>();
		lassert2( ref, "Pointer assigned to a semref should not be NULL." );
	}
	template<typename Y>
	void operator= ( const semref<Y> & a ) const
	{
		// run-time check if `a' really references ptr<Y> or class derived from it
		// compile-time enforcement that Y is derived from T
		ptr<T> tmp = a.ref.template dncast<Y>();
		ref = tmp;
		lassert2( ref, "Pointer assigned to a semref should not be NULL." );
	}
	ptr<T> operator-> () const
	{
		// TODO pt workaround for 3.2.3
		return ref.template dncast<T>();
		// equivalent to the original:
		//return operator ptr<T>();
	}
#if 0
operator ptr<T> () const
{
	return ref.template dncast<T>();
}
operator srp<T> () const
{
	return srp<T>(ref.template dncast<T>());
}
#endif
	// the following one not only makes ptr<T> from semval<T>, but also ptr<Y> when Y is base class of T
	template<typename Y>
	operator ptr<Y> () const
	{
		// T below is intentional
		return ref.template dncast<T>();
	}
	// the following one not only makes ptr<T> from semval<T>, but also ptr<Y> when Y is base class of T
	template<typename Y>
	operator srp<Y> () const
	{
		// T below is intentional
		return srp<Y>(ref.template dncast<T>());
	}
};

class semval {
private:
	ptr<semref_type> pp;
public:
	explicit semval() : pp() {}
	template<typename T> semref<T> select()
	{
		return semref<T>(pp);
	}
};

end_package(syn);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif	// lestes__lang__cplus__syn__semval_hh__included
