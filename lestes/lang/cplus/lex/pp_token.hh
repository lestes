/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___pp_token_hh___included
#define lestes__lang__cplus__lex___pp_token_hh___included

/*! \file
  \brief Preprocessor token.

  Declaration of pp_token class representing preprocessor token.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/std/ucn_string.hh>
#include <lestes/lang/cplus/lex/basic_token.hh>

package(lestes);

package(std);
// forward declaration to avoid cycle
class source_location;
end_package(std);

package(lang);
package(cplus);
package(lex);

// forward declaration to avoid cycle
class taboo_macros;
class token_value;

/*!
  Preprocessing tokens list definition.
  Each entry is of the form
  DEF_PP_TOKEN(symbolic name, token spelling, alternative spelling, flags).
  The entries act as function-like macro calls, that are redefined to select one of the items.
*/
#define PP_TOKENS_LIST \
DEF_PP_TOKEN(FILE_END,"","",FLG_NONE)\
DEF_PP_TOKEN(TERMINATOR,"","",FLG_NONE)\
DEF_PP_TOKEN(LEFT_BRACKET,"[","<:",FLG_VARIANT)\
DEF_PP_TOKEN(RIGHT_BRACKET,"]",":>",FLG_VARIANT)\
DEF_PP_TOKEN(LEFT_BRACE,"{","<%",FLG_VARIANT)\
DEF_PP_TOKEN(RIGHT_BRACE,"}","%>",FLG_VARIANT)\
DEF_PP_TOKEN(LEFT_PAR,"(","",FLG_NONE)\
DEF_PP_TOKEN(RIGHT_PAR,")","",FLG_NONE)\
DEF_PP_TOKEN(SEMICOLON,";","",FLG_NONE)\
DEF_PP_TOKEN(COLON,":","",FLG_NONE)\
DEF_PP_TOKEN(QMARK,"?","",FLG_NONE)\
DEF_PP_TOKEN(DOT,".","",FLG_NONE)\
DEF_PP_TOKEN(PLUS,"+","",FLG_NONE)\
DEF_PP_TOKEN(STAR,"*","",FLG_NONE)\
DEF_PP_TOKEN(PERCENT,"%","",FLG_NONE)\
DEF_PP_TOKEN(SLASH,"/","",FLG_NONE)\
DEF_PP_TOKEN(HAT,"^","xor",FLG_VARIANT)\
DEF_PP_TOKEN(AMP,"&","bitand",FLG_VARIANT)\
DEF_PP_TOKEN(VBAR,"|","bitor",FLG_VARIANT)\
DEF_PP_TOKEN(TILDE,"~","compl",FLG_VARIANT)\
DEF_PP_TOKEN(EMARK,"!","not",FLG_VARIANT)\
DEF_PP_TOKEN(EQ,"=","",FLG_NONE)\
DEF_PP_TOKEN(LT,"<","",FLG_NONE)\
DEF_PP_TOKEN(GT,">","",FLG_NONE)\
DEF_PP_TOKEN(COMMA,",","",FLG_NONE)\
DEF_PP_TOKEN(MINUS,"-","",FLG_NONE)\
DEF_PP_TOKEN(DOT_DOT_DOT,"...","",FLG_NONE)\
DEF_PP_TOKEN(COLON_COLON,"::","",FLG_NONE)\
DEF_PP_TOKEN(DOT_STAR,".*","",FLG_NONE)\
DEF_PP_TOKEN(PLUS_EQ,"+=","",FLG_NONE)\
DEF_PP_TOKEN(MINUS_EQ,"-=","",FLG_NONE)\
DEF_PP_TOKEN(STAR_EQ,"*=","",FLG_NONE)\
DEF_PP_TOKEN(SLASH_EQ,"/=","",FLG_NONE)\
DEF_PP_TOKEN(PERCENT_EQ,"%=","",FLG_NONE)\
DEF_PP_TOKEN(HAT_EQ,"^=","xor_eq",FLG_VARIANT)\
DEF_PP_TOKEN(AMP_EQ,"&=","and_eq",FLG_VARIANT)\
DEF_PP_TOKEN(VBAR_EQ,"|=","or_eq",FLG_VARIANT)\
DEF_PP_TOKEN(LT_LT,"<<","",FLG_NONE)\
DEF_PP_TOKEN(GT_GT,">>","",FLG_NONE)\
DEF_PP_TOKEN(LT_LT_EQ,"<<=","",FLG_NONE)\
DEF_PP_TOKEN(GT_GT_EQ,">>=","",FLG_NONE)\
DEF_PP_TOKEN(EQ_EQ,"==","",FLG_NONE)\
DEF_PP_TOKEN(EMARK_EQ,"!=","not_eq",FLG_VARIANT)\
DEF_PP_TOKEN(LT_EQ,"<=","",FLG_NONE)\
DEF_PP_TOKEN(GT_EQ,">=","",FLG_NONE)\
DEF_PP_TOKEN(AMP_AMP,"&&","and",FLG_VARIANT)\
DEF_PP_TOKEN(VBAR_VBAR,"||","or",FLG_VARIANT)\
DEF_PP_TOKEN(PLUS_PLUS,"++","",FLG_NONE)\
DEF_PP_TOKEN(MINUS_MINUS,"--","",FLG_NONE)\
DEF_PP_TOKEN(MINUS_GT_STAR,"->*","",FLG_NONE)\
DEF_PP_TOKEN(MINUS_GT,"->","",FLG_NONE)\
DEF_PP_TOKEN(ASM,"asm","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(AUTO,"auto","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(BOOL,"bool","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(BREAK,"break","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(CASE,"case","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(CATCH,"catch","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(CHAR,"char","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(CLASS,"class","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(CONST,"const","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(CONST_CAST,"const_cast","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(CONTINUE,"continue","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(DEFAULT,"default","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(DELETE,"delete","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(DO,"do","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(DOUBLE,"double","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(DYNAMIC_CAST,"dynamic_cast","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(ELSE,"else","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(ENUM,"enum","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(EXPLICIT,"explicit","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(EXPORT,"export","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(EXTERN,"extern","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(FLOAT,"float","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(FOR,"for","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(FRIEND,"friend","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(GOTO,"goto","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(IF,"if","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(INLINE,"inline","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(INT,"int","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(LONG,"long","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(MUTABLE,"mutable","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(NAMESPACE,"namespace","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(NEW,"new","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(OPERATOR,"operator","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(PRIVATE,"private","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(PROTECTED,"protected","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(PUBLIC,"public","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(REGISTER,"register","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(REINTERPRET_CAST,"reinterpret_cast","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(RETURN,"return","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(SHORT,"short","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(SIGNED,"signed","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(SIZEOF,"sizeof","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(STATIC,"static","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(STATIC_CAST,"static_cast","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(STRUCT,"struct","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(SWITCH,"switch","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(TEMPLATE,"template","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(THIS,"this","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(THROW,"throw","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(TRY,"try","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(TYPEDEF,"typedef","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(TYPEID,"typeid","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(TYPENAME,"typename","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(UNION,"union","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(UNSIGNED,"unsigned","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(USING,"using","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(VIRTUAL,"virtual","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(VOID,"void","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(VOLATILE,"volatile","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(WCHAR,"wchar_t","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(WHILE,"while","",FLG_NAME | FLG_INTERNAL)\
DEF_PP_TOKEN(BOOL_LIT,"","",FLG_NAME | FLG_EXTERNAL)\
DEF_PP_TOKEN(CHAR_LIT,"","",FLG_EXTERNAL)\
DEF_PP_TOKEN(WCHAR_LIT,"","",FLG_EXTERNAL)\
DEF_PP_TOKEN(STRING_LIT,"","",FLG_EXTERNAL)\
DEF_PP_TOKEN(WSTRING_LIT,"","",FLG_EXTERNAL)\
DEF_PP_TOKEN(NUMBER_LIT,"","",FLG_EXTERNAL)\
DEF_PP_TOKEN(IDENT,"","",FLG_NAME | FLG_EXTERNAL)\
DEF_PP_TOKEN(HASH,"#","%:",FLG_VARIANT)\
DEF_PP_TOKEN(HASH_HASH,"##","%:%:",FLG_VARIANT)\
DEF_PP_TOKEN(BLANK," ","",FLG_NONE)\
DEF_PP_TOKEN(LINE_END,"\n","",FLG_NONE)\
DEF_PP_TOKEN(OTHER,"","",FLG_EXTERNAL)\
DEF_PP_TOKEN(INCLUDE,"include","",FLG_NAME)\
DEF_PP_TOKEN(DEFINE,"define","",FLG_NAME)\
DEF_PP_TOKEN(UNDEF,"undef","",FLG_NAME)\
DEF_PP_TOKEN(LINE,"line","",FLG_NAME)\
DEF_PP_TOKEN(PRAGMA,"pragma","",FLG_NAME)\
DEF_PP_TOKEN(ERROR,"error","",FLG_NAME)\
DEF_PP_TOKEN(DEFINED,"defined","",FLG_NAME)\
DEF_PP_TOKEN(IFDEF,"ifdef","",FLG_NAME)\
DEF_PP_TOKEN(IFNDEF,"ifndef","",FLG_NAME)\
DEF_PP_TOKEN(ELIF,"elif","",FLG_NAME)\
DEF_PP_TOKEN(ENDIF,"endif","",FLG_NAME)\
DEF_PP_TOKEN(INCL_HCHAR,"","",FLG_EXTERNAL)\
DEF_PP_TOKEN(INCL_QCHAR,"","",FLG_EXTERNAL)\
DEF_PP_TOKEN(INCL_SIG,"","",FLG_NONE)\
DEF_PP_TOKEN(LAST,"","",FLG_NONE)

//! Defines basic preprocessor token.
typedef basic_token<lc_host_uint_least16,ptr<source_location>, ptr<token_value> > basic_pp_token;

/*!
  \brief Preprocessor token.

  Represents preprocessor token. Each token has type and location, literals and
  names also have value. Token type properties are represented by internal flags.
*/
class pp_token:
	public basic_pp_token {
public:
	//! Token can be macro name.
	bool is_name(void) const;
	//! Token has alternative spelling.
	bool is_alternative(void) const;
	//! Token has value.
	bool is_valued(void) const;
	//! Tests equality.
	bool equals(const ptr<pp_token> &other) const;
	//! Tests congruence.
	bool congruent(const ptr<pp_token> &other) const;
	//! Returns description.
	lstring description_get(void) const;
	//! Returns token spelling.
	ucn_string spelling_get(void) const;
	//! Returns taboo macros.
	ptr<taboo_macros> taboo_get(void) const;
	//! Returns copy of token with different location.
	ptr<pp_token> clone(const location_type &a_location) const;
	//! Returns copy of token with different taboo macros.
	ptr<pp_token> clone(const ptr<taboo_macros> &a_taboo) const;
	//! Returns new token, initializes with location and type.
	static ptr<pp_token> create(const location_type &a_location, const type_type &a_type);
	//! Returns new alternative token, initializes with location and type.
	static ptr<pp_token> create_alternative(const location_type &a_location, const type_type &a_type);
	//! Returns new token, initializes with location, type and value.
	static ptr<pp_token> create(const location_type &a_location, const type_type &a_type,
		const value_type &a_value);
	//! Returns the terminator token.
	static ptr<pp_token> terminator(void);

#define DEF_PP_TOKEN(x,y,z,w) TOK_##x, 
	//! Token type constants.
	enum token_types {
		PP_TOKENS_LIST
		TOK_AVOID_WARNING
	};
#undef DEF_PP_TOKEN
	
	enum { 
		//! Number of token types.
		TOKEN_TYPE_COUNT = TOK_AVOID_WARNING
	};

protected:
	//! Creates new token, initializes with appropriate values. 
	pp_token(const location_type &a_location, const type_type &a_type,
				const value_type &a_value, bool a_alternative,
				const ptr<taboo_macros> &a_taboo);
	//! Marks the object.
	virtual void gc_mark(void);
private:
	//! Type of flags.
	typedef lc_host_uint_least16 flags_type;
	//! Token flags.
	enum pp_token_flags {
		//! Empty flags.
		FLG_NONE = 0x0,
		//! Token has internally initialized value.
		FLG_INTERNAL = 0x1,
		//! Token has externally initialized value.
		FLG_EXTERNAL = 0x2,
		//! Token is a name.
		FLG_NAME = 0x4,
		//! Token has two variants of spelling.
		FLG_VARIANT = 0x08
	};
	//! Token has alternative spelling.
	bool alternative;
	//! Taboo macros for token.
	srp<taboo_macros> taboo;
	//! Hides copy constructor.
	pp_token(const pp_token &);   
	//! Hides assignment operator.
	pp_token &operator=(const pp_token &);
	//! Initializes static structures. 
	static void initialize(void);
	//! Initialization flag.
	static bool initialized;
	//! Descriptions of tokens.
	static lstring the_description[TOKEN_TYPE_COUNT];
	//! Spelling of tokens.
	static ucn_string the_spelling[2][TOKEN_TYPE_COUNT];
	//! Values of tokens.
	static ptr<token_value> the_value[TOKEN_TYPE_COUNT];
	//! Flags of tokens.
	static flags_type the_flags[TOKEN_TYPE_COUNT];
	//! The terminator token instance.
	static ptr<pp_token> terminator_instance;
};

//! Type of token in pp_token.
typedef pp_token::type_type pp_token_type;

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
