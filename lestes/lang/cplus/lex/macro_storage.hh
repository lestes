/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___macro_storage_hh___included
#define lestes__lang__cplus__lex___macro_storage_hh___included

/*! \file
  \brief Storage of defined macros.
  
  Declaration of macro_storage class representing defined macros.
  \author pt
*/

#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/macro.hh>
#include <lestes/std/map.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

// forward declaration to avoid cycle
class token_value;

/*!
  \brief Storage of defined macros.
  
  Represents storage of defined macros.
  Handles definition, removal and lookup.
*/
class macro_storage: public ::lestes::std::object {
public:
	//! Defines new macro.
	void define(const ptr<macro> &a_macro);
	//! Undefines macro.
	void undef(const ptr<pp_token> &tok);
	//! Tests whether a macro is defined.
	bool defined(const ptr<pp_token> &tok);
	//! Searches for a macro of given name.
	ptr<macro> lookup(const ptr<token_value> &a_name);
	//! Discards user defined macros.
	void clear(void);
	//! Returns new storage.
	static ptr<macro_storage> create(void);
protected:
	//! Constructs empty object.
	macro_storage(void);
	//! Marks the object.
	virtual void gc_mark(void);
private:
	//! Type of storage for macros.
	typedef map< srp<token_value>, srp<macro> > storage_type;
	//! Storage for macros.
	srp<storage_type> storage;
};

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);
#endif
/* vim: set ft=lestes : */
