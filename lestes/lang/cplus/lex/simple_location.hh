/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___simple_location_hh___included
#define lestes__lang__cplus__lex___simple_location_hh___included

/*! \file
  \brief Simple token location.
  
  Declaration of simple_location class representing simple token location.
  \author pt
*/
#include <lestes/common.hh>
package(lestes);
package(lang);
package(cplus);
package(lex);

/*!
  \brief Simple token location.

  Contains line number and column number.
  The object is inmutable to help keeping invariants.
*/
class simple_location: public ::lestes::std::object {
public:
	//! Returns line in file.
	ulint line_get(void) const;
	//! Returns column on line.
	ulint column_get(void) const;
	//! Tests equality.
	bool equals(const ptr<simple_location> &rhs) const;
	//! Returns new object, initializes with position.
	static ptr<simple_location> create(ulint a_line, ulint a_column);
protected:
	//! Creates new object, initializes with postion.
	simple_location(ulint a_line, ulint a_column);
private:
	//! Line in file.
	ulint line;
	//! Column inside the line.
	ulint column;
	//! Hides copy constructor.
	simple_location(const simple_location &);
	//! Hides assigment operator.
	simple_location &operator=(const simple_location &);
};

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */

