/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___line_numbers_hh___included
#define lestes__lang__cplus__lex___line_numbers_hh___included

/*! \file
  \brief Token numbering filter.

  Declaration of line_numbers class performing token numbering.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/ucn_filter.hh>
package(lestes);
package(lang);
package(cplus);
package(lex);

/*!
  \brief Token numbering filter.

  Performs assignment of relative line numbers and columns to tokens.
*/
class line_numbers: public ucn_filter {
public:
	//! Common type for counting tokens.
	typedef ulint count_type;
	//! Reads next token from input.
	ptr<ucn_token> read(void);
	//! Returns new instance, initialize with first row and column.
	static ptr<line_numbers> create(void);
protected:
	//! Creates the object, initializes with first row and column.
	line_numbers(void);
private:
	//! Line of the coming token.
	count_type line;
	//! Column of the coming token.
	count_type column;
	//! Hides copy constructor.
	line_numbers(const line_numbers &);
	//! Hides assignment operator.
	line_numbers &operator=(const line_numbers &);
};

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
