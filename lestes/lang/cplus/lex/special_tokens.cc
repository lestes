/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Token type assignment.
  
  Definition of special_tokens class assigning types to tokens.
  \author pt
*/

#include <lestes/common.hh>
#include <lestes/std/character.hh>
#include <lestes/lang/cplus/lex/special_tokens.hh>
#include <lestes/lang/cplus/lex/ucn_token.hh>
#include <lestes/lang/cplus/lex/ucn_filter.hh>
package(lestes);
package(lang);
package(cplus);
package(lex);

using namespace ::std;

/*!
  Creates the object.
  \post errors == 0
*/
special_tokens::special_tokens(void):
	state(START),
	errors(0)
{
}

/*!
  Returns next token, assigns token type.
  Saves the EOF token to return it again.
  Tries to stop the error flood from encoder by sending EOF.
  \return The token with assigned type.
*/  
ptr<ucn_token> special_tokens::read(void)
{
	if (state == END)
		return saved;

	ptr<ucn_token> t = input_read();
	ucn_token_type utt = t->type_get();
	ucn u = t->value_get();

	switch (utt) {
		case ucn_token::TOK_NOT_EOF:
			if (character::is_basic(u)) {
				t = t->clone_type(ucn_token::TOK_BASIC);
			} else if (character::is_translated(u)) {
				t = t->clone_type(ucn_token::TOK_TRANSLATED);
			} else {
				// TODO pt report error: character value out of range
				t = ucn_token::create_error(::lestes::msg::message::create(0xdead,"TODO char out of range",::lestes::msg::message::FLG_ERROR));
			}
			break;
		case ucn_token::TOK_ERROR:
			++errors;
			if (errors < ERRORS_LIMIT)
				break;

			// prepare fake EOF
			saved = ucn_token::create(ucn_token::TOK_EOF,0,t->location_get());
			// TODO pt report error: wrong encoding in source, stopping
			t = ucn_token::create_error(
					::lestes::msg::message::create(0xbeef,"TODO flood",::lestes::msg::message::FLG_ERROR));
			state = END;
			break;
		case ucn_token::TOK_EOF:
			saved = t;
			state = END;
			break;
		default:
			lassert2(false,"You should never get here");
	}

	return t;
}

/*!
  Marks the object.
*/
void special_tokens::gc_mark(void)
{
	ucn_filter::gc_mark();
}

/*!
  Returns new instance.
  \post state == BEGIN
  \return New instance of the class.
*/
ptr<special_tokens> special_tokens::create(void)
{
	return new special_tokens();
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

/* vim: set ft=lestes : */
