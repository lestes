/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Unit test.
  
  Unit test for class encoder_factory.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/equality.hh>
#include <lestes/lang/cplus/lex/encoder.hh>
#include <lestes/lang/cplus/lex/encoder_factory.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

using namespace ::std;

/*!
  \brief Test encoder.

  Simple encoder to test the encoder_factory.
  Returns specific token according to the template parameter.
  \param Discriminator  The value to 
*/
template <ulint Discriminator>
class encoder_test: public encoder {
public:
	//! Reads next token.
	ptr<ucn_token> read(void);
	//! Returns new instance.
	static ptr< encoder_test<Discriminator> > create(void);
protected:
	//! Creates the object.
	encoder_test(void);
private:
	//! Hides copy constructor.
	encoder_test(const encoder_test<Discriminator> &);
	//! Hides assignment operator.
	encoder_test<Discriminator> &operator=(const encoder_test<Discriminator> &);
};

/*!
  Creates the encoder.
*/
template <ulint Discriminator>
encoder_test<Discriminator>::encoder_test<Discriminator>(void)
{
}

/*!
  Returns token to distinguish instances.
  The token value corresponds to Discriminator.
  \return  The token with value set to the discriminator.
*/
template <ulint Discriminator>
ptr<ucn_token> encoder_test<Discriminator>::read(void)
{
	return ucn_token::create(ucn_token::TOK_NOT_EOF,Discriminator);
}

/*!
  Returns new instance of first type.
  \return  The new instance.
*/
template <ulint Discriminator>
ptr< encoder_test<Discriminator> > encoder_test<Discriminator>::create(void)
{
	return new encoder_test<Discriminator>();
}

//! Helper typedef for test encoder.
typedef encoder_test<1> enc1;
//! Helper typedef for test encoder.
typedef encoder_test<2> enc2;
//! Helper typedef for test encoder.
typedef encoder_test<3> enc3;

/*!
  \brief Tests encoder_factory class.

  Performs testing of encoder_factory class.
*/
void encoder_factory_test(void)
{
	ptr<encoder_factory> ef = encoder_factory::instance();
	
	// instance exists
	lassert(ef);

	// the factory is empty
	lassert(is_equal(ef->summon("first"),ptr<encoder>(NULL)));
	lassert(is_equal(ef->summon("second"),ptr<encoder>(NULL)));
	lassert(is_equal(ef->summon("third"),ptr<encoder>(NULL)));
	
	// inserted first
	ef->insert("first",encoder_factory::encoder_create_adaptor<enc1>);
	lassert(!is_equal(ef->summon("first"),ptr<encoder>(NULL)));
	lassert(is_equal(ef->summon("second"),ptr<encoder>(NULL)));
	lassert(is_equal(ef->summon("third"),ptr<encoder>(NULL)));
	lassert(is_equal(ef->summon("first")->read()->value_get(),1U));
	
	// inserted second
	ef->insert("second",&encoder_factory::encoder_create_adaptor<enc2>);
	lassert(!is_equal(ef->summon("first"),ptr<encoder>(NULL)));
	lassert(!is_equal(ef->summon("second"),ptr<encoder>(NULL)));
	lassert(is_equal(ef->summon("third"),ptr<encoder>(NULL)));
	lassert(is_equal(ef->summon("first")->read()->value_get(),1U));
	lassert(is_equal(ef->summon("second")->read()->value_get(),2U));
	
	// inserted third
	ef->insert("third",&encoder_factory::encoder_create_adaptor<enc3>);
	lassert(!is_equal(ef->summon("first"),ptr<encoder>(NULL)));
	lassert(!is_equal(ef->summon("second"),ptr<encoder>(NULL)));
	lassert(!is_equal(ef->summon("third"),ptr<encoder>(NULL)));
	lassert(is_equal(ef->summon("first")->read()->value_get(),1U));
	lassert(is_equal(ef->summon("second")->read()->value_get(),2U));
	lassert(is_equal(ef->summon("third")->read()->value_get(),3U));
	
	// removed first
	ef->remove("first");
	lassert(is_equal(ef->summon("first"),ptr<encoder>(NULL)));
	lassert(!is_equal(ef->summon("second"),ptr<encoder>(NULL)));
	lassert(!is_equal(ef->summon("third"),ptr<encoder>(NULL)));
	lassert(is_equal(ef->summon("second")->read()->value_get(),2U));
	lassert(is_equal(ef->summon("third")->read()->value_get(),3U));
	
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

/*!
  \brief Main function.

  Runs the unit test in different namespace.
*/
int main(void)
{
	::lestes::lang::cplus::lex::encoder_factory_test();
	return 0;
}
/* vim: set ft=lestes : */
