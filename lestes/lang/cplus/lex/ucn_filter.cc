/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Token filter.

  Definition of ucn_filter class representing token filter.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/ucn_filter.hh>
package(lestes);
package(lang);
package(cplus);
package(lex);

/*!
  Creates new filter with no input.
  \post input == NULL
*/
ucn_filter::ucn_filter(void):
	input(NULL)
{
}

/*!
  Sets input into this filter.
  \param a_input The new input.
*/
void ucn_filter::input_set(const ptr<ucn_filter> &a_input)
{
	input = a_input;
}

/*!
  Marks the object.
*/
void ucn_filter::gc_mark(void)
{
	input.gc_mark();
	::lestes::std::object::gc_mark();
}

/*!
  Returns current input into the filter.
  \return The current input.
*/
ptr<ucn_filter> ucn_filter::input_get(void) const
{
	return input;
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);
/* vim: set ft=lestes : */
