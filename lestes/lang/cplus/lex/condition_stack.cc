/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Conditional directives stack.

  Declaration of conditions class representing conditonal directives stack.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/condition_stack.hh>
#include <lestes/lang/cplus/lex/condition_stack.m.hh>
#include <lestes/lang/cplus/lex/condition.hh>
#include <lestes/std/source_location.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

/*!
  Creates empty condition stack.
  \post active == true
*/
condition_stack::condition_stack(void):
	active(true),
	cstack(cstack_type::create())
{
	cstack->push_back(condition::create_empty());
}
	
/*!
  Tests if the output is active.
  \return true  If tokens shall be output for the current condition.
*/
bool condition_stack::active_get(void) const
{
	return active;
}

/*!
  Processes conditional directive, issues any error messages.
  \pre a_location != NULL
  \param a_dir  The directive to process 
  \param value  The value of the condition for if-like directives.
  \param a_location  The location of the directive.
  \return true  If the directive is correct in the current context.
*/
bool condition_stack::process(directive_type a_dir, bool value, const ptr<source_location> &a_location)
{
	lassert(a_location);

	condition::type_type cond = condition::COND_EMPTY;
	ptr<condition> curr = cstack->back();
	condition::type_type curr_type = curr->type_get();
	bool curr_waiting = true;
	
	switch (a_dir) {
		case DIR_IFNDEF:
			cond = condition::COND_IFNDEF;
			break;
		case DIR_IF:
			cond = condition::COND_IF;
			break;
		case DIR_IFDEF:
			cond = condition::COND_IFDEF;
			break;
		case DIR_ELIF:
			switch (curr_type) {
				case condition::COND_EMPTY:
					report << elif_without_if << a_location;
					return false;
				case condition::COND_ELSE:
					report << elif_after_else << a_location;
					report << conditional_starts_here << curr->location_get();
					return false;
				default:
					break;
			}
			curr_waiting = curr->waiting_get();
			cond = condition::COND_ELIF;
			break;
		case DIR_ELSE:
			switch (curr_type) {
				case condition::COND_EMPTY:
					report << else_without_if << a_location;
					return false;
				case condition::COND_ELSE:
					report << else_after_else << a_location;
					report << conditional_starts_here << curr->location_get();
					return false;
				default:
					break;
			}
			// implicitly fulfilled condition
			value = true;
			curr_waiting = curr->waiting_get();
			cond = condition::COND_ELSE;
			break;
		case DIR_ENDIF:
			if (curr_type == condition::COND_EMPTY) {
				report << endif_without_if << a_location;
				return false;
			}
			break;
		case DIR_EOF:
			if (curr_type != condition::COND_EMPTY) {
				report << unterminated_conditional << curr->name_get() << curr->location_get();
				return false;
			}
			break;
		default:
			lassert2(false,"You should never get here");
			break;
	}
	
	// active = curr_active && value
	switch (a_dir) {
		case DIR_ELSE:
		case DIR_ELIF:
			{
				// remove old condition on the same level
				cstack->pop_back();
				bool curr_active = curr->active_get();

				// new waiting = curr_active && curr_waiting && !value
				// keep the original location
				cstack->push_back(
						condition::create(cond,curr_active && curr_waiting && !value,curr_active,curr->location_get()));
				active = curr_active && curr_waiting && value;
			}
			break;
		case DIR_IFNDEF:
		case DIR_IF:
		case DIR_IFDEF:
			// new waiting = active && curr_waiting && !value
			cstack->push_back(condition::create(cond,active && curr_waiting && !value,active,a_location));
			active = active && curr_waiting && value;
			break;
		case DIR_ENDIF:
			// restore active value
			active = curr->active_get();
			// remove the old condition
			cstack->pop_back();
			break;
		case DIR_EOF:
			// do nothing
			break;
		default:
			lassert2(false,"You should never get here");
			break;
	}

	return true;
}

/*!
  Returns depth of the condition stack.
  \return The number of nested conditions.
*/
ulint condition_stack::depth(void) const
{
	// subtract the padding
	return cstack->size() - 1;
}

/*!
  Marks the object.
*/
void condition_stack::gc_mark(void)
{
	cstack.gc_mark();
	::lestes::std::object::gc_mark();
}

/*!
  Returns empty condition stack.
  \return An empty condition stack.
*/
ptr<condition_stack> condition_stack::create(void)
{
	return new condition_stack();
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

/* vim: set ft=lestes : */
