/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___basic_token_hh___included
#define lestes__lang__cplus__lex___basic_token_hh___included

/*! \file
  \brief Token template.

  Definition of basic_token class template.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/equality.hh>
#include <lestes/pointer_helpers.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

/*!
  \brief Token template.

  General token template representing an entity with specific occurence within file.
  \param Type  Type of the kind of the token.
  \param Location  Type of the location of the token.
  \param Value Type of the value of the token.
*/
template <typename Type, typename Location, typename Value>
class basic_token: public ::lestes::std::object {
public:
	//! Type of token type.
	typedef typename convert<Type>::to_ptr type_type;
	//! Type of token value.
	typedef typename convert<Value>::to_ptr value_type;
	//! Type of token location.
	typedef typename convert<Location>::to_ptr location_type;
	//! Returns type of token.
	Type type_get(void) const;
	//! Returns value of token.
	Value value_get(void) const;
	//! Returns location of token.
	Location location_get(void) const;
	//! Tests equality.
	bool equals(const ptr< basic_token<Type,Location,Value> > &rhs) const;
protected:
	//! Initializes with location, token type and value.
	basic_token(const Location &a_location, const Type &a_type, const Value &a_value);
	//! Marks the object.
	virtual void gc_mark(void);
private:
	//! Type of token.
	typename convert<Type>::to_srp type;
	//! Token value itself.
	typename convert<Value>::to_srp value;
	//! Where the token is located.
	typename convert<Location>::to_srp location;
	//! Hides copy constructor.
	basic_token(const basic_token<Type,Location,Value> &copy);
	//! Hides assignment operator.
	basic_token<Type,Location,Value> &operator=
		(const basic_token<Type,Location,Value> &rhs);
};

/*!
  Creates initialized object.
  \param a_location  The location of the token.
  \param a_type  The type of the token.
  \param a_value  The value of the token.
*/
template <typename Type, typename Location, typename Value>
basic_token<Type,Location,Value>::basic_token(const Location &a_location,
		const Type &a_type, const Value &a_value):
	type(a_type),
	value(a_value),
	location(a_location)
{
}

/*!
  Returns token type.
  \return  The token type.
*/
template <typename Type, typename Location, typename Value>
inline Type basic_token<Type,Location,Value>::type_get(void) const
{
	return type;
}

/*!
  Returns token value.
  \return  The token value.
*/
template <typename Type, typename Location, typename Value>
inline Value basic_token<Type,Location,Value>::value_get(void) const
{
	return value;
}

/*!
  Returns token location.
  \return  The token location.
*/
template <typename Type, typename Location, typename Value>
inline Location basic_token<Type,Location,Value>::location_get(void) const
{
	return location;
}

/*!
  Marks the object.
*/
template <typename Type, typename Location, typename Value>
inline void basic_token<Type,Location,Value>::gc_mark(void)
{
	gc_mark_srp(type);
	gc_mark_srp(location);
	gc_mark_srp(value);
	
	::lestes::std::object::gc_mark();
}

/*!
  Tests equality of tokens. Checks equality of type, value and location.
  \param rhs  The token to compare with.
  \return true  If both tokens are is_equal.
*/
template <typename Type, typename Location, typename Value>
bool basic_token<Type,Location,Value>::equals(
		const ptr< basic_token<Type,Location,Value> > &rhs) const
{
	return
		is_equal(type,rhs->type_get()) &&
		is_equal(value,rhs->value_get()) &&
		is_equal(location,rhs->location_get());
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);
#endif
/* vim: set ft=lestes : */
