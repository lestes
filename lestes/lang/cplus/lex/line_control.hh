/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___line_control_hh___included
#define lestes__lang__cplus__lex___line_control_hh___included

/*! \file
  \brief Line number control.

  Declaration of class line_control responsible for creating full location information
  from physical location and effects of #line directives.
  \author pt
*/
#include <lestes/common.hh>

package(lestes);

package(std);
// forward declaration to avoid cycle
class file_info;
class source_location;
end_package(std);

package(lang);
package(cplus);
package(lex);

// forward declaration to avoid cycle
class simple_location;

/*!
  \brief Line number control.
  
  Performs translating physical location to full featured source location information.
  Handles line number and file name changes as of #line directive.
*/
class line_control : public ::lestes::std::object {
public:
	//! Translates physical location to source location.
	ptr<source_location> translate_location(const ptr<simple_location> &physical) const;
	//! Changes line numbering.
	void change_line(const ptr<source_location> &before, ulint line_number);
	//! Changes file name.
	void change_file(const lstring &file_name);
	//! Returns new instance, initializes with file information.
	static ptr<line_control> create(const ptr<file_info> &a_info);
protected:
	//! Creates the object.
	line_control(const ptr<file_info> &a_info);
	//! Marks the object.
	virtual void gc_mark(void);
private:
	//! The file information for locations.
	srp<file_info> file;
	//! The delta between physical and logical line number.
	ulint delta;
	//! Hides copy constructor.
	line_control(const line_control &);
	//! Hides assignment operator.
	line_control &operator=(const line_control &);
};

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
