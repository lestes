/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Storage of defined macros.
  
  Definition of macro_storage class representing defined macros.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/equality.hh>
#include <lestes/lang/cplus/lex/macro_storage.hh>
#include <lestes/lang/cplus/lex/macro_storage.m.hh>
#include <lestes/lang/cplus/lex/macro.hh>
#include <lestes/lang/cplus/lex/pp_token.hh>
#include <lestes/lang/cplus/lex/token_value.hh>
#include <lestes/std/map.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

using namespace ::std;

/*!
  Constructs empty object.
*/
macro_storage::macro_storage(void):
	storage(storage_type::create())
{
	// TODO pt add predefined
}

/*!
  Defines new macro, checks for redefinition.
  \pre a_macro != NULL
  \param a_macro  The macro to be defined.
*/
void macro_storage::define(const ptr<macro> &a_macro)
{
	lassert(a_macro);
	
	ptr<token_value> name = a_macro->name_get();
	
	storage_type::iterator it = storage->find(name);
	if (it == storage->end()) {
		storage->insert(make_pair(name,a_macro));
		return;
	}
	
	ptr<macro> orig = (*it).second;
	if (orig->predefined_get()) {
		// redefinition of predefined internal macro
		report << predefined_macro_redefined << name->content_get() << a_macro->location_get();
		return;
	}
	
	// redefinition is equivalent, do nothing
	if (is_equal(a_macro,orig)) return;

	// macro redefinition is different
	report << macro_redefinition_different << name->content_get() << a_macro->location_get();
	// this is the location of the previous definition
	report << previous_definition << orig->location_get();
}


/*!
  Undefines macro, checks whether was defined.
  \pre tok != NULL
  \param tok  The token with name of macro to undefine.
*/
void macro_storage::undef(const ptr<pp_token> &tok)
{
	lassert(tok);
  
	if (!tok->is_name()) {
		// expected name of macro to undefine
		report << expected_macro_name << tok->location_get();
		return;
	}
  
	ptr<token_value> name = tok->value_get();

	storage_type::iterator it = storage->find(name);
	if (it == storage->end()) return;
	
	ptr<macro> m = (*it).second;
	
	if (m->predefined_get()) {
		// undefining predefined macro
		report << predefined_macro_undefined << name->content_get() << tok->location_get();
		return;
	}

	storage->erase(it);
}

/*!
  Tests whether macro of the given name is currently defined.
  \pre tok != NULL
  \param tok  The token containing macro name to test.
  \return true  If the macro of this name is defined.
*/
bool macro_storage::defined(const ptr<pp_token> &tok)
{
	lassert(tok);

	if (!tok->is_name()) {
		// expected name of macro
		report << expected_macro_name << tok->location_get();
		// such macro is not defined
		return false;
	}
  
	ptr<token_value> name = tok->value_get();

	return storage->find(name) != storage->end();
}

/*!
  Searches for macro of given name.
  \todo pt return special values for __LINE__ etc
  \pre name != NULL
  \param name  The name of the searched macro.
  \return  The macro of the given name, or NULL if such macro is not defined.
*/
ptr<macro> macro_storage::lookup(const ptr<token_value> &name)
{
	lassert(name);

	// TODO check special names (__LINE__) etc
	storage_type::iterator it = storage->find(name);
	if (it == storage->end()) return NULL;
	return (*it).second;
}

/*!
  Marks the object.
*/
void macro_storage::gc_mark(void)
{
	storage.gc_mark();
	::lestes::std::object::gc_mark();
}

/*!
  Returns new instance of the class.
  \return  The new storage.
*/  
ptr<macro_storage> macro_storage::create(void)
{
	return new macro_storage();
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);
/* vim: set ft=lestes : */
