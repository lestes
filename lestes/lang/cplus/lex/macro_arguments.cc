/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Macro argument list.
  
	Definition of macro_arguments class representing list of macro arguments.
	\author pt
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/macro_arguments.hh>
#include <lestes/lang/cplus/lex/macro_argument.hh>
#include <lestes/lang/cplus/lex/pp_token.hh>
#include <lestes/lang/cplus/lex/token_sequence.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

/*!
  Creates empty list.
  \post state == BEGIN
  \post length() == 0
*/  
macro_arguments::macro_arguments(void):
	state(BEGIN),
	arguments(arguments_type::create())
{
}

/*!
  Parses macro argument list in parentheses.
  \pre state == BEGIN
  \param input  The source for arguments, starting with '(' token.
  \return false  In case of parse error.
*/ 
bool macro_arguments::parse(const ptr<token_input> &input)
{
	lassert(state == BEGIN);

	ptr<pp_token> t;
	t = input->read_front();
	
	lassert(t->type_get() == pp_token::TOK_LEFT_PAR);
	
	bool first = true;
	macro_argument::result_type result;

	do {
		ptr<macro_argument> ma = macro_argument::create();
		result = ma->parse(input,first);
		first = false;
		
		arguments->push_back(ma);

		if (result == macro_argument::EMPTY) {
			state = PARSED_EMPTY;
			return true;
		}
	
	} while (result == macro_argument::CONTINUE);

	if (result == macro_argument::LAST) {
		state = PARSED;
		return true;
	}

	// else result == macro_argument::ERROR
	state = DEAD;
	return false;
}

/*!
  Returns length of the argument list.
  \pre state == PARSED || state == PARSED_EMPTY
  \return Length of the argument list.
*/
ulint macro_arguments::length(void) const
{
	lassert(state == PARSED || state == PARSED_EMPTY);
	return arguments->size();
}

/*!
  Checks whether the arguments match the length of a parameter list.
  Either the lengths are the same, or there is no parameters and the argument list is empty. 
  \pre state == PARSED || state == PARSED_EMPTY
  \par pars_length  The length of the checked parameter list.
  \return true  If the length matches or state == PARSED_EMPTY and pars_length == 0.
*/
bool macro_arguments::check(ulint pars_length) const
{
	lassert(state == PARSED || state == PARSED_EMPTY);
	return length() == pars_length || (pars_length == 0 && state == PARSED_EMPTY);
}

/*!
  Returns argument at specified position.
  \pre state == PARSED || state == PARSED_EMPTY
  \pre index < length()  
  \param index  The index of the desired argument.
  \return The argument at specified index.
*/
ptr<macro_argument> macro_arguments::argument_get(ulint index) const
{
	lassert(state == PARSED || state == PARSED_EMPTY);
	lassert(index < length());
	return arguments->at(index);
}

/*!
  Marks the object.
*/
void macro_arguments::gc_mark(void)
{
	arguments.gc_mark();
	::lestes::std::object::gc_mark();
}

/*!
  Returns new empty argument list.
  \post state == BEGIN
  \post length() == 0
*/
ptr<macro_arguments> macro_arguments::create(void)
{
	return new macro_arguments();
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);
/* vim: set ft=lestes : */
