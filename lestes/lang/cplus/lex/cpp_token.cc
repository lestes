/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief C++ token.
  
  Decfinition of cpp_token class representing C++ token.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/std/source_location.hh>
#include <lestes/std/reflect.hh>
#include <lestes/lang/cplus/lex/cpp_token.hh>
#include <lestes/lang/cplus/lex/token_value.hh>
#include <lestes/lang/cplus/lex/lex_literal.g.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

/*!
  Creates the object, initializes with location, type and value.
  \param a_location  The location of the token.
  \param a_type  The type of the token.
  \param a_value  The value of the token.
  \param a_literal  The literal properties.
*/
cpp_token::cpp_token(const location_type &a_location, const type_type &a_type, const value_type &a_value,
		const ptr<lex_literal> &a_literal):
	basic_cpp_token(a_location,a_type,a_value),
	literal(a_literal)
{
}

/*!
  Returns literal properties.
  \return  The information about literal token.
*/
ptr<lex_literal> cpp_token::literal_get(void) const
{
	lassert(type_get() == TOK_LITERAL);
	return literal;
}

/*!
  Tests equality to other token.
  Does not test literal, only token value.
  \pre other != NULL
  \param other  The token to compare to.
  \return true  If both token represent the same entity.
*/
bool cpp_token::equals(const ptr<cpp_token> &other) const
{
	lassert(other);
	return is_equal(type_get(),other->type_get()) &&
		is_equal(value_get(),other->value_get()) &&
		is_equal(location_get(),other->location_get());
}

/*!
  Returns new token, initializes with token type.
  \pre a_type != TOK_LITERAL
  \param a_location  The location of the token.
  \param a_type  The type of token (syntactic class).
  \return  New C++ token.
*/
ptr<cpp_token> cpp_token::create(const location_type &a_location, const type_type &a_type)
{
	lassert(a_type != TOK_LITERAL);
	return new cpp_token(a_location,a_type,NULL,NULL);
}

/*!
  Returns new token, initializes with token type and value.
  \pre a_type != TOK_LITERAL
  \param a_type  The type of token (syntactic class).
  \param a_location  The location of the token.
  \return  New C++ token.
*/
ptr<cpp_token> cpp_token::create(const location_type &a_location,
		const type_type &a_type, const value_type &a_value)
{
	lassert(a_type != TOK_LITERAL);
	return new cpp_token(a_location,a_type,a_value,NULL);
}

/*!
  Returns new literal token, initializes with token type, value and literal properties.
  \param a_location  The location of the token.
  \param a_literal  The properties of the literal.
  \param a_value  The value of the token.
  \return  New C++ token.
*/
ptr<cpp_token> cpp_token::create_literal(const location_type &a_location,
		const ptr<lex_literal> &a_literal, const value_type &a_value)
{
	return new cpp_token(a_location,TOK_LITERAL,a_value,a_literal);
}

/*!
  Returns the internal description of the token.
  \return  The string representation of the token type.
*/
lstring cpp_token::description_get(void) const
{
	if (!descriptions) {
		descriptions = descriptions_type::create();

		descriptions->insert(::std::make_pair(cpp_token::TOK_EOF,"TOK_EOF"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_LITERAL,"TOK_LITERAL"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_ASM,"TOK_ASM"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_AUTO,"TOK_AUTO"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_BOOL,"TOK_BOOL"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_BREAK,"TOK_BREAK"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_CASE,"TOK_CASE"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_CATCH,"TOK_CATCH"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_CHAR,"TOK_CHAR"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_CLASS,"TOK_CLASS"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_CONST,"TOK_CONST"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_CONST_CAST,"TOK_CONST_CAST"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_CONTINUE,"TOK_CONTINUE"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_DEFAULT,"TOK_DEFAULT"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_DELETE,"TOK_DELETE"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_DO,"TOK_DO"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_DOUBLE,"TOK_DOUBLE"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_DYNAMIC_CAST,"TOK_DYNAMIC_CAST"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_ELSE,"TOK_ELSE"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_ENUM,"TOK_ENUM"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_EXPLICIT,"TOK_EXPLICIT"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_EXPORT,"TOK_EXPORT"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_EXTERN,"TOK_EXTERN"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_FLOAT,"TOK_FLOAT"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_FOR,"TOK_FOR"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_FRIEND,"TOK_FRIEND"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_GOTO,"TOK_GOTO"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_IF,"TOK_IF"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_INLINE,"TOK_INLINE"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_INT,"TOK_INT"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_LONG,"TOK_LONG"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_MUTABLE,"TOK_MUTABLE"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_NAMESPACE,"TOK_NAMESPACE"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_NEW,"TOK_NEW"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_OPERATOR,"TOK_OPERATOR"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_PRIVATE,"TOK_PRIVATE"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_PROTECTED,"TOK_PROTECTED"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_PUBLIC,"TOK_PUBLIC"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_REGISTER,"TOK_REGISTER"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_REINTERPRET_CAST,"TOK_REINTERPRET_CAST"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_RETURN,"TOK_RETURN"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_SHORT,"TOK_SHORT"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_SIGNED,"TOK_SIGNED"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_SIZEOF,"TOK_SIZEOF"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_STATIC,"TOK_STATIC"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_STATIC_CAST,"TOK_STATIC_CAST"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_STRUCT,"TOK_STRUCT"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_SWITCH,"TOK_SWITCH"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_TEMPLATE,"TOK_TEMPLATE"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_THIS,"TOK_THIS"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_THROW,"TOK_THROW"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_TRY,"TOK_TRY"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_TYPEDEF,"TOK_TYPEDEF"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_TYPEID,"TOK_TYPEID"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_TYPENAME,"TOK_TYPENAME"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_UNION,"TOK_UNION"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_UNSIGNED,"TOK_UNSIGNED"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_USING,"TOK_USING"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_VIRTUAL,"TOK_VIRTUAL"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_VOID,"TOK_VOID"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_VOLATILE,"TOK_VOLATILE"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_WCHAR_T,"TOK_WCHAR_T"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_WHILE,"TOK_WHILE"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_IDENT,"TOK_IDENT"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_RIGHT_BRACKET,"TOK_RIGHT_BRACKET"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_LEFT_BRACKET,"TOK_LEFT_BRACKET"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_EXCLAMATION,"TOK_EXCLAMATION"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_TILDE,"TOK_TILDE"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_PERCENT,"TOK_PERCENT"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_SLASH,"TOK_SLASH"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_STAR,"TOK_STAR"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_MINUS,"TOK_MINUS"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_PLUS,"TOK_PLUS"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_GT,"TOK_GT"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_LT,"TOK_LT"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_AMP,"TOK_AMP"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_HAT,"TOK_HAT"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_VBAR,"TOK_VBAR"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_QMARK,"TOK_QMARK"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_COMMA,"TOK_COMMA"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_RIGHT_BRACE,"TOK_RIGHT_BRACE"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_LEFT_BRACE,"TOK_LEFT_BRACE"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_SEMICOLON,"TOK_SEMICOLON"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_RIGHT_PAR,"TOK_RIGHT_PAR"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_LEFT_PAR,"TOK_LEFT_PAR"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_EQ,"TOK_EQ"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_COLON,"TOK_COLON"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_DOT_DOT_DOT,"TOK_DOT_DOT_DOT"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_MINUS_GT,"TOK_MINUS_GT"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_MINUS_GT_STAR,"TOK_MINUS_GT_STAR"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_DOT_STAR,"TOK_DOT_STAR"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_GT_EQ,"TOK_GT_EQ"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_LT_EQ,"TOK_LT_EQ"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_EXCLAMATION_EQ,"TOK_EXCLAMATION_EQ"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_EQ_EQ,"TOK_EQ_EQ"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_AMP_AMP,"TOK_AMP_AMP"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_VBAR_VBAR,"TOK_VBAR_VBAR"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_GT_GT,"TOK_GT_GT"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_LT_LT,"TOK_LT_LT"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_MINUS_MINUS,"TOK_MINUS_MINUS"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_PLUS_PLUS,"TOK_PLUS_PLUS"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_LT_LT_EQ,"TOK_LT_LT_EQ"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_GT_GT_EQ,"TOK_GT_GT_EQ"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_VBAR_EQ,"TOK_VBAR_EQ"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_AMP_EQ,"TOK_AMP_EQ"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_HAT_EQ,"TOK_HAT_EQ"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_PERCENT_EQ,"TOK_PERCENT_EQ"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_SLASH_EQ,"TOK_SLASH_EQ"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_STAR_EQ,"TOK_STAR_EQ"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_MINUS_EQ,"TOK_MINUS_EQ"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_PLUS_EQ,"TOK_PLUS_EQ"));
		descriptions->insert(::std::make_pair(cpp_token::TOK_COLON_COLON,"TOK_COLON_COLON"));
	}

	descriptions_type::iterator it = descriptions->find(type_get());
	lassert(it != descriptions->end());
	return it->second;
}

/*!
  Marks the object.
*/
void cpp_token::gc_mark(void)
{
	literal.gc_mark();
	basic_cpp_token::gc_mark();
}

/*!
  Returns reflection list for cpp_token.
  \return The reflection list.
*/
ptr< object::reflection_list > cpp_token::reflection_get(void) const
{
	if (!reflection) {
		typedef class_reflection::field_metadata md;
		typedef class_reflection::field_metadata_list mdlist;
		ptr<mdlist> mdl = mdlist::create();
		mdl->push_back(md::create("type","lstring"));
		mdl->push_back(md::create("value","ucn_string"));
		reflection = reflection_list::create(object::reflection_get());
		reflection->push_back(class_reflection::create("cpp_token",mdl));
	}
	return reflection;
}

/*!
  Returns values of the fields.
  \return List of field values.
*/
ptr< object::field_list_list > cpp_token::field_values_get(void) const
{
	ptr<field_list_list> result = object::field_values_get();
	result->push_back(value_list::create());
	result->back()->push_back(objectize<lstring>::create(description_get()));
	result->push_back(value_list::create());
	ptr<token_value> tv = value_get();
	if (tv) {
		result->back()->push_back(objectize<ucn_string>::create(tv->content_get()));
	} else {
		result->back()->push_back(NULL);
	}
	return result;
}

/*!
  Descriptions of tokens.
*/
ptr<cpp_token::descriptions_type> cpp_token::descriptions = descriptions;

/*!
  Reflection list for token.
*/
ptr<object::reflection_list> cpp_token::reflection = reflection;

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);
/* vim: set ft=lestes : */
