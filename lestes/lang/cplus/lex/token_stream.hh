/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___token_stream_hh___included
#define lestes__lang__cplus__lex___token_stream_hh___included

/*! \file
  \brief Stream of tokens.

	Declaration of token_stream class representing stream of tokens.
	\author pt
*/

#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/token_input.hh>
#include <lestes/std/list.hh>
#include <iosfwd>

package(lestes);

package(std);
// forward declarations to avoid cycle
class source_location;
end_package(std);

package(lang);
package(cplus);
package(lex);

// forward declaration to break cycle
class pp_token;
class pp_filter;
class token_sequence;
class macro;

/*!
  \brief Stream of tokens.

  Represents input stream of tokens, with access at front.
  The stream is connected to pp_filter to read tokens from.
  Never reads beyond the next newline, if not asked for.
*/
class token_stream: public token_input {
public:
	//! Reads first token.
	ptr<pp_token> read(void);
	//! Returns front token.
	ptr<pp_token> peek_front(void);
	//! Reads front token, squeezing whitespace.
	ptr<pp_token> read_front(void);
	//! Reads front token, skipping front whitespace, but not newline.
	ptr<pp_token> read_front_skip_ws(void);
	//! Skips front whitespace.
	bool skip_front_ws(void);
	//! Reads next line in input.
	ptr<token_sequence> read_line(void);
	//! Expands next line in input.
	ptr<token_sequence> expand_line(const ptr<macro_storage> &macros);
	// TODO pt remove
	//! Prints debug dump.
	void debug_print(::std::ostream &o);
	//! Returns stream with input.
	static ptr<token_stream> create(const ptr<pp_filter> &a_input);
protected:   
	//! Creates stream.
	token_stream(const ptr<pp_filter> &a_input);
	//! Prepends a sequence.
	void prepend(const ptr<token_sequence> &ts);
	//! Marks the object.
	virtual void gc_mark(void);
private:
	//! The type of sequence container.
	typedef ::lestes::std::list< srp<pp_token> > sequence_type;
	//! Loads next line.
	bool load_input(void);
	
	// TODO pt remove //! State of the stream.
	//state_type state;

	//! The eof token.
	srp<pp_token> file_end;
	//! The sequence container.
	srp<sequence_type> sequence;
	//! Input into this sequence.
	// TODO pt remove srp<pp_filter> input;
};

//! Prints debug dump of token stream.
::std::ostream &operator<<(::std::ostream &o, const ptr<token_stream> &ts);

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);
#endif
/* vim: set ft=lestes : */
