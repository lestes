/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief String literal joining filter.
  
  Definition of string_joiner class performing joining of string literals.
  \author pt
*/
#include <lestes/common.hh> 
#include <lestes/std/source_location.hh>
#include <lestes/std/vector.hh>
#include <lestes/lang/cplus/lex/string_joiner.hh>
#include <lestes/lang/cplus/lex/string_joiner.m.hh>
#include <lestes/lang/cplus/lex/pp_token.hh>
#include <lestes/lang/cplus/lex/token_value.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

using namespace ::std;

/*!
  Creates the object.
  \pre a_input != NULL
  \param a_input  The input for the filter.
  \post state == START
*/
string_joiner::string_joiner(const ptr<pp_filter> &a_input):
	pp_filter(checked(a_input)),
	state(START),
	saved(NULL),
	sequence(sequence_type::create())
{
}

/*!
  Reads next token from input.
  Joins all adjacent string literal tokens into single string literal token.
  Joins all adjacent normal and wide string literal tokens into single wide string literal token.
*/
ptr<pp_token> string_joiner::read(void)
{
	if (state == SAVED) {
		ptr<pp_token> tmp = saved;
		saved = NULL;
		state = START;
		return tmp;
	}
		
	ptr<pp_token> t;
	pp_token_type ptt;
	// first literal
	ptr<pp_token> first;

	while (true) {
		t = input_read();
		ptt = t->type_get();
		
		switch (state) {
			case START:
				switch (ptt) {
					case pp_token::TOK_STRING_LIT:
						first = t;
						state = FIRST_STRING;
						break;
					case pp_token::TOK_WSTRING_LIT:
						first = t;
						state = FIRST_WIDE_STRING;
						break;
					default:
						// fast path
						return t;
				}
				break;
			case FIRST_STRING:
				switch (ptt) {
					case pp_token::TOK_STRING_LIT:
						sequence->clear();
						sequence->push_back(first);
						sequence->push_back(t);
						state = NEXT_STRING;
						break;
					case pp_token::TOK_WSTRING_LIT:
						report << mixed_strings << t->location_get();
						sequence->clear();
						sequence->push_back(first);
						sequence->push_back(t);
						state = NEXT_BAD_STRING;
						break;
					default:
						saved = t;
						state = SAVED;
						return first;
				}
				break;
			case NEXT_STRING:
				switch (ptt) {
					case pp_token::TOK_WSTRING_LIT:
						state = NEXT_BAD_STRING;
						report << mixed_strings << t->location_get();
						// fall through
					case pp_token::TOK_STRING_LIT:
						sequence->push_back(t);
						break;
					default:
						saved = t;
						state = SAVED;
						return join_sequence(pp_token::TOK_STRING_LIT);
				}
				break;
			case FIRST_WIDE_STRING:
				switch (ptt) {
					case pp_token::TOK_STRING_LIT:
						report << mixed_strings << t->location_get();
						sequence->clear();
						sequence->push_back(first);
						sequence->push_back(t);
						state = NEXT_BAD_STRING;
						break;
					case pp_token::TOK_WSTRING_LIT:
						sequence->clear();
						sequence->push_back(first);
						sequence->push_back(t);
						state = NEXT_WIDE_STRING;
						break;
					default:
						saved = t;
						state = SAVED;
						return first;
				}
				break;
			case NEXT_WIDE_STRING:
				switch (ptt) {
					case pp_token::TOK_STRING_LIT:
						report << mixed_strings << t->location_get();
						sequence->push_back(t);
						state = NEXT_BAD_STRING;
						break;
					case pp_token::TOK_WSTRING_LIT:
						sequence->push_back(t);
						break;
					default:
						saved = t;
						state = SAVED;
						return join_sequence(pp_token::TOK_WSTRING_LIT);
				}
				break;
			case NEXT_BAD_STRING:
				switch (ptt) {
					case pp_token::TOK_STRING_LIT:
					case pp_token::TOK_WSTRING_LIT:
						sequence->push_back(t);
						break;
					default:
						saved = t;
						state = SAVED;
						return join_sequence(pp_token::TOK_WSTRING_LIT);
				}
				break;
			default:
				lassert2(false,"You should never get here");
		}
	}
}

/*!
  Returns token containing joined string literals. Does not affect the contents of the sequence.
  \pre The sequence is not empty.
  \pre The sequence contains tokens of one type, either pp_token::TOK_STRING_LIT or pp_token::TOK_WSTRING_LIT.
  The location for the new token is taken from the first entry.
  \param ptt  The type of the new token.
  \return String or wide string literal token.
*/
ptr<pp_token> string_joiner::join_sequence(pp_token_type ptt) const
{
	sequence_type::iterator it = sequence->begin();
	sequence_type::iterator end = sequence->end();

	// process first entry
	lassert(it != end);
	ptr<pp_token> t = *it;
	
	ptr<source_location> loc = t->location_get();
	ucn_string str(t->value_get()->content_get());
	++it;
	
	// process second and following entries
	for ( ;it != end; ++it) {
		t = *it;
		str += t->value_get()->content_get();
	}

	return pp_token::create(loc,ptt,token_value::create(str));
}

/*!
  Marks the object.
*/
void string_joiner::gc_mark(void)
{
	sequence.gc_mark();
	saved.gc_mark();
	pp_filter::gc_mark();
}

/*!
  Returns new instance.
  \pre a_input != NULL
  \param a_input The input for the filter.
  \return  New instance of the class.
*/
ptr<string_joiner> string_joiner::create(const ptr<pp_filter> &a_input)
{
	return new string_joiner(a_input);
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

/* vim: set ft=lestes : */
