/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Unit test.

  Unit test for class token_sequence.
  \todo pt Add tests.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/std/source_location.hh>
#include <lestes/lang/cplus/lex/token_sequence.hh>
#include <lestes/lang/cplus/lex/pp_token.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

using namespace ::std;

/*!
  Tests token sequence class.
  \todo pt Add actual tests.
*/
void token_sequence_test(void)
{
	ptr<file_info> fi = file_info::create(string("abc"),NULL);
	ptr<source_location> loc = source_location::create(fi,0,0);
	
	ptr<token_sequence> t1 = token_sequence::create();

	lassert(is_equal(t1,t1));
	lassert(is_equal(t1->empty(),true));
	lassert(is_equal(t1->length(),0U));

	//   pp_token::create(loc,pp_token::TOK_COMMA),

}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

int main(void)
{
	::lestes::lang::cplus::lex::token_sequence_test();
	return 0;
}
/* vim: set ft=lestes : */

