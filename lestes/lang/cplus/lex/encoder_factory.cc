/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Factory for encoders.
  
  Definition of encoder_factory class managing encoder implementations.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/encoder_factory.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

using namespace ::std;

/*!
  Creates the object.
  \post summon() == NULL
*/
encoder_factory::encoder_factory(void):
	storage(storage_type::create())
{
}

/*!
  Inserts new encoder into the database.
  \pre a_create != NULL
  \pre a_create() != NULL (checked only in summon())
  \post summon(a_name) == a_create()
  \param a_name  The name of the encoder to insert.
  \param a_create  The method tho create the encoder.
*/
void encoder_factory::insert(const lstring &a_name, encoder_create_type a_create)
{
	lassert(a_create);
	storage->insert(storage_type::value_type(a_name,a_create));
}

/*!
  Removes encoder from the database.
  \pre a_name is in the database.
  \param a_name  The name of the encoder to remove.
*/
void encoder_factory::remove(const lstring &a_name)
{
	storage_type::iterator it = storage->find(a_name);
	lassert(it != storage->end());
	storage->erase(it);
}

/*!
  Returns new encoder for given name.
  \pre a_name was inserted into the factory
  \post returned != NULL or a_name was not inserted
  \param a_name  The name of the desired encoder.
  \return The new encoder or NULL if not found.
*/
ptr<encoder> encoder_factory::summon(const lstring &a_name) const
{
	storage_type::iterator it = storage->find(a_name);
	if (it == storage->end()) return NULL;
	// call the create function
	ptr<encoder> ret = (it->second)();
	// check whether the create succeeded
	lassert(ret);
	return ret;
}

/*!
  Marks the object.
*/
void encoder_factory::gc_mark(void)
{
	storage.gc_mark();
	::lestes::std::object::gc_mark();
}

/*!
  Returns the only instance of the encoder_factory class.
  \return The singleton encoder_factory object.
*/
ptr<encoder_factory> encoder_factory::instance(void)
{
	if (!singleton) {
		singleton = new encoder_factory();
	}
	return singleton;
}

/*!
  The only instance of the class, holds all encoder registrations.
  Uses lazy instantiation.
*/
ptr<encoder_factory> encoder_factory::singleton;

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

/* vim: set ft=lestes : */
