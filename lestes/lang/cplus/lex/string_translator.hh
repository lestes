/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___string_translator_hh___included
#define lestes__lang__cplus__lex___string_translator_hh___included

/*! \file
  \brief Translator into execution character set.
  
  Declaration of string_translator class representing translator into execution character set.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/pp_filter.hh>

package(lestes);
package(std);
// forward declaration to avoid cycle
class ucn_string;
end_package(std);
package(lang);
package(cplus);
package(lex);

// forward declaration to avoid cycle
class pp_token;
class token_value;

/*!
  \brief Encoder into execution character set. 
  
  Encodes string and character literals into execution character set.
*/
class string_translator: public pp_filter {
public:
	//! Reads next token.
	ptr<pp_token> read(void);
	//! Returns new instance.
	static ptr<string_translator> create(const ptr<pp_filter> &a_input);
protected:
	//! Creates the translator.
	string_translator(const ptr<pp_filter> &a_input);
private:
	//! Translates literal into execution character set.
	ptr<token_value> translate(const ucn_string &str, const ptr<source_location> &loc);
	//! Hides copy constructor.
	string_translator(const string_translator &);
	//! Hides assignment operator.
	string_translator &operator=(const string_translator &);
};

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
