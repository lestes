/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___ucn_token_buffer_hh___included
#define lestes__lang__cplus__lex___ucn_token_buffer_hh___included

/*! \file
  \brief Token buffer.

  Declaration of ucn_token_buffer class representing buffer
  of simple tokens.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/std/list.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

// forward declaration to avoid cycle
class ucn_token;
class token_value;
class line_control;

/*!
  \brief Token buffer.

  Represents flexible buffer holding ucn_token items.
  Items are added mostly one by one at the end and afterwards
  removed from the beginning in longer runs, forming literals
  and identifiers.
*/
class ucn_token_buffer: public ::lestes::std::object {
private:
	//! Type of buffer to hold stored data.
	typedef list< srp<ucn_token> > buffer_type;
public:
	/*!
	  \brief Type for size of the buffer.
	  \author TMA
	 */
	typedef buffer_type::size_type size_type;
	//! Adds single item to the end.
	void add_back(const ptr<ucn_token> &item);
	//! Discards items from the beginning.
	void advance(size_type len);
	//! Returns item at the beginning.
	ptr<ucn_token> peek_front(void) const;
	//! Extracts value until stop character without interpreting.
	ptr<token_value> extract_until(ucn stop);
	//! Extracts value without interpreting.
	ptr<token_value> extract_ordinary(size_type len);
	//! Extracts identifiers and numbers with ucn.
	ptr<token_value> extract_simple_ucn(size_type len, bool identifier);
	//! Extracts identifiers and numbers with bad ucn.
	ptr<token_value> extract_invalid_ucn(size_type len, bool identifier);
	//! Extracts literal with escaped ucn.
	ptr<token_value> extract_ucn_literal(size_type len);
	//! Extracts malformed literal.
	ptr<token_value> extract_bad_literal(size_type len);
	//! Returns length of the buffer.
	size_type length(void) const;
	//! Returns new buffer.
	static ptr<ucn_token_buffer> create(const ptr<line_control> &a_lines);
protected:
	//! Creates empty buffer.
	ucn_token_buffer(const ptr<line_control> &a_lines);
	//! Marks the object.
	virtual void gc_mark(void);
private:
	//! Buffer to hold stored data.
	srp<buffer_type> buffer;
	//! Line control to transform locations.
	srp<line_control> lines;
	//! Hides copy constructor.
	ucn_token_buffer(const ucn_token_buffer &);
	//! Hides assignment operator.
	ucn_token_buffer &operator=(const ucn_token_buffer &);
};

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
