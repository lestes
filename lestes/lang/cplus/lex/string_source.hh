/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___string_source_hh___included
#define lestes__lang__cplus__lex___string_source_hh___included

/*! \file
  \brief String data source.
  
  Declaration of string_source class representing data source reading from string.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/data_source.hh>
#include <lestes/lang/cplus/lex/ucn_token.hh>
#include <string>
package(lestes);
package(lang);
package(cplus);
package(lex);

/*!
  \brief String data source.

	Represents data source reading from host character string.
*/
class string_source: public data_source {
public:
	//! Type of string.
	typedef lstring string_type;
	//! Type of index into the string.
	typedef string_type::size_type index_type;
	//! Reads one token from string.
	ptr<ucn_token> read(void);
	//! Returns new instance, initializes with string.
	static ptr<string_source> create(const string_type &a_str);
protected:
	//! Creates the object, initializes with string.
	string_source(const string_type &a_str);
private:
	//! Hides copy constructor.
	string_source(const string_source &);
	//! Hides assignment operator.
	string_source &operator=(const string_source &);
	//! String to read from.
	string_type str;
	//! Position in string.
	index_type idx;
	// ! Direct reference to the stream.
	//::std::istream *stream;
};

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
