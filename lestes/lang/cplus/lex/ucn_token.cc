/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Single character token.

  Declaration of ucn_token class representing single source character token.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/ucn_token.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

/*!
  Creates the object, initializes with location, type and value.
  \pre a_type != TOK_ERROR
  \post a_error == NULL
  \param a_location  The initial location of the token.
  \param a_type  The initial type of the token.
  \param a_value  The initial value of the token.
*/
ucn_token::ucn_token(const location_type &a_location, const type_type &a_type,
		const value_type &a_value):
	basic_ucn_token(a_location,(lassert(a_type != TOK_ERROR),a_type),a_value),
	error()
{
}

/*!
  Creates the object, initializes with location and error message.
  \pre a_error != NULL
  \param a_location  The location of the token.
  \param a_error  The error message.
*/
ucn_token::ucn_token(const location_type &a_location, const error_type &a_error):
	basic_ucn_token(a_location,TOK_ERROR,0),
	error(checked(a_error))
{
}

/*!
  Marks the object
*/
void ucn_token::gc_mark(void)
{
	error.gc_mark();
	basic_ucn_token::gc_mark();
}

/*!
  Returns error for an error token.
  \pre type_get() == TOK_ERROR
  \return The error assigned to the token.
*/
ucn_token::error_type ucn_token::error_get(void) const
{
	lassert(type_get() == TOK_ERROR);
	lassert(error);
	return error;
}

/* TODO remove !
  Sets error for an error token.
  \pre type_get() == TOK_ERROR
  \pre a_error != NULL
  \param a_error  The error to set.
void ucn_token::error_set(const error_type &a_error)
{
	lassert(type_get() == TOK_ERROR);
	error = a_error;
}
*/

/*!
  Returns copy of this token.
  \post is_equal(returned,this)
  \return  New token which holds the same values.
ptr<ucn_token> ucn_token::clone(void)
{
	return new ucn_token(location_get(),type_get(),value_get());
}
*/

/*!
  Returns copy of this token with new type.
  \pre type_get() != TOK_ERROR
  \return  The token with new value.
*/
ptr<ucn_token> ucn_token::clone_type(const type_type &a_type) const
{
	lassert(type_get() != TOK_ERROR);
	return new ucn_token(location_get(),a_type,value_get());
}

/*!
  Returns copy of this token with new value.
  \pre type_get() != TOK_ERROR
  \return  The token with new value.
*/
ptr<ucn_token> ucn_token::clone_value(const value_type &a_value) const
{
	lassert(type_get() != TOK_ERROR);
	return new ucn_token(location_get(),type_get(),a_value);
}

/*!
  Returns copy of this token with new location.
  \return  The token with new value.
*/
ptr<ucn_token> ucn_token::clone_location(const location_type &a_location) const
{
	if (type_get() == TOK_ERROR)
		return new ucn_token(a_location,error_get());
	return new ucn_token(a_location,type_get(),value_get());
}

/*!
  Returns new token, initializes with token type.
  \post is_equal(type_get(),a_type)
  \param a_type  The initial token type.
  \return New token initialized with token type.
*/
ptr<ucn_token> ucn_token::create(const type_type &a_type)
{
	return new ucn_token(location_type(),a_type,value_type());
}

/*!
  Returns new token, initializes with token type and token value.
  \post is_equal(type_get(),a_type)
  \post is_equal(value_get(),a_value)
  \param a_type  The initial token type.
  \param a_value  The initial value.
  \return  A new token initialized with token type and value.
*/
ptr<ucn_token> ucn_token::create(const type_type &a_type, const value_type &a_value)
{
	return new ucn_token(location_type(),a_type,a_value);
}

/*!
  Returns new token, initializes with location, token type and token value.
  \post is_equal(type_get(),a_type)
  \post is_equal(value_get(),a_value)
  \post is_equal(location_get(),a_location)
  \param a_type  The initial token type.
  \param a_value  The initial token value.
  \param a_location  The initial location.
  \return  A new token initialized with location, token type and token value.
*/
ptr<ucn_token> ucn_token::create(const type_type &a_type, const value_type &a_value,
		const location_type &a_location)
{
	return new ucn_token(a_location,a_type,a_value);
}

/*!
  Returns new token, initializes with error and location.
  \post is_equal(error_get(),a_error)
  \post is_equal(location_get(),a_location)
  \param a_error  The error message.
  \param a_location  The location.
  \return  A new token initialized with location and error message.
*/
ptr<ucn_token> ucn_token::create_error(const error_type &a_error, const location_type &a_location)
{
	return new ucn_token(a_location,a_error);
}

/*!
  Returns new token, initializes with error.
  \post is_equal(error_get(),a_error)
  \param a_error  The error message.
  \param a_location  The location.
  \return  A new token initialized with location and error message.
*/
ptr<ucn_token> ucn_token::create_error(const error_type &a_error)
{
	return new ucn_token(NULL,a_error);
}

/*!
  Tests equality of the token.
  \param rhs  The token to compare with.
  \return true  If both tokens are equal.
*/
bool ucn_token::equals(const ptr<ucn_token> &rhs) const
{
	return basic_ucn_token::equals(rhs.dncast<basic_ucn_token>());
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

/* vim: set ft=lestes : */
