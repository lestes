<?xml version="1.0" encoding="UTF-8"?>
<!--
	The lestes compiler suite
	Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
	Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
	Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
	Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
	Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
	Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
	Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; version 2 of the License.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	See the full text of the GNU General Public License version 2, and
	the limitations in the file doc/LICENSE.

	By accepting the license the licensee waives any and all claims
	against the copyright holder(s) related in whole or in part to the
	work, its use, and/or the inability to use it.
-->
<lmd xmlns="http://lestes.jikos.cz/schemas/lmd" xmlns:h="http://www.w3.org/TR/REC-html40">
	<file-name>ucn_token_buffer</file-name>

	<dox file="both">
		<bri>
			Messages for forming literals and identifiers.
		</bri>
		<det>
			Definition of warnings and errors issued when creating identifiers
			and literals from individual characters and escape sequences.
		</det>
	</dox>

	<packages>
		<p>lestes</p>
		<p>lang</p>
		<p>cplus</p>
		<p>lex</p>
	</packages>
	
	<imports>
	</imports>

	<implementation-imports>
	</implementation-imports>

	<error name="ucn_escape_value_invalid">
		<dox>
			<bri>Error message for ucn translation.</bri>
			<det>
				Issued when the value of ucn escape sequence does not
				fall into the allowed ranges.
			</det>
		</dox>
		<text>value of universal character escape sequence out of range</text>
	</error>

	<error name="ucn_escape_value_invalid_in_identifier">
		<dox>
			<bri>Error message for ucn translation.</bri>
			<det>
				Issued when the value of ucn escape sequence does not
				fall into the allowed ranges for identifiers.
			</det>
		</dox>
		<text>value of universal character escape sequence out of range for identifiers</text>
	</error>

	<error name="ucn_escape_insufficient_digits">
		<dox>
			<bri>Error message for ucn translation.</bri>
			<det>
				Issued when the ucn escape sequence does not
				contain proper number of digits.
			</det>
		</dox>
		<text>insufficient digits in universal character escape sequence</text>
	</error>

	<error name="invalid_escape_sequence">
		<dox>
			<bri>Error message for ucn translation.</bri>
			<det>
				Issued when escape sequence was not recognized.
			</det>
		</dox>
		<text>invalid escape sequence</text>
	</error>

	<error name="missing_hexadecimal_digits">
		<dox>
			<bri>Error message for ucn translation.</bri>
			<det>
				Issued when hexadecimal escape sequence has no digits.
			</det>
		</dox>
		<text>`\\x' with no hexadecimal digits</text>
	</error>

</lmd>
