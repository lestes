/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___condition_hh___included
#define lestes__lang__cplus__lex___condition_hh___included

/*! \file
  \brief Conditional directive.

  Declaration of condition class representing conditional
  preprocessor directives.
  \author pt
*/
#include <lestes/common.hh>
package(lestes);

package(std);
// forward declaration to break cycle
class source_location;
end_package(std);

package(lang);
package(cplus);
package(lex);

/*!
  \brief Conditional directive.

  Represents conditional preprocessor directive for use within condition_stack.
  Contains location in the source, type and state of processing. 
*/
class condition: public ::lestes::std::object {
public:
	//! Types of conditions.
	typedef enum {
		//! Empty, bottom stack entry.
		COND_EMPTY = 0,
		//! Directive #if.
		COND_IF = 1,
		//! Directive #ifdef.
		COND_IFDEF = 2,
		//! Directive #ifndef.
		COND_IFNDEF = 3,
		//! Directive #elif.
		COND_ELIF = 4,
		//! Directive #else.
		COND_ELSE = 5
	} type_type;
	//! Returns location.
	ptr<source_location> location_get(void) const;
	//! Returns type.
	type_type type_get(void) const;
	//! Return whether waiting for true branch.
	bool waiting_get(void) const;
	//! Return whether the output was active.
	bool active_get(void) const;
	//! Returns printable name.
	lstring name_get(void) const;
	//! Tests equality.
	bool equals(const ptr<condition> &other) const;
	//! Returns empty condition.
	static ptr<condition> create_empty(void);
	//! Returns condition.
	static ptr<condition> create(type_type a_cond, bool a_waitning, bool a_active, const ptr<source_location> &a_location);
protected:
	//! Creates empty condition.
	condition(void);
	//! Creates new condition.
	condition(type_type a_cond, bool a_waitning, bool a_active, const ptr<source_location> &a_location);
	//! Marks the object.
	virtual void gc_mark(void);
private:
	//! Type of the condition.
	type_type type;
	//! Waiting for the true branch of conditional.
	bool waiting;
	//! Output is active after leaving the conditional on this level.
	bool active;
	//! Location of usage.
	srp<source_location> location;
	//! Names of the conditions.
	static lstring condition_name[];
	//! Hides copy constructor.
	condition(const condition &copy);
	//! Hides assignment operator.
	condition &operator=(const condition &rhs);
};

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
