/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Unit test.
  
  Unit test for class string_joiner.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/std/file_info.hh>
#include <lestes/std/source_location.hh>
#include <lestes/lang/cplus/lex/string_joiner.hh>
#include <lestes/lang/cplus/lex/pp_token.hh>
#include <lestes/lang/cplus/lex/token_value.hh>
#include <lestes/lang/cplus/lex/token_sequence.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

using namespace ::std;

/*!
  \brief Tests string_joiner class.

  Performs testing of string_joiner class.
*/
void string_joiner_test(void)
{
	ptr<token_sequence> ts = token_sequence::create();
	ptr<file_info> fi = file_info::create("abc",NULL);
	ptr<source_location> loc = source_location::create(fi,1,1);
	ptr<pp_token> sep = pp_token::create(loc,pp_token::TOK_EQ);

	ts->add_back(sep);
	ts->add_back(pp_token::create(loc,pp_token::TOK_STRING_LIT,token_value::create("aaa")));
	
	ts->add_back(sep);
	ts->add_back(pp_token::create(loc,pp_token::TOK_WSTRING_LIT,token_value::create("bbb")));

	ts->add_back(sep);
	ts->add_back(pp_token::create(loc,pp_token::TOK_STRING_LIT,token_value::create("ccc")));
	ts->add_back(pp_token::create(loc,pp_token::TOK_STRING_LIT,token_value::create("ddd")));
	
	ts->add_back(sep);
	ts->add_back(pp_token::create(loc,pp_token::TOK_WSTRING_LIT,token_value::create("eee")));
	ts->add_back(pp_token::create(loc,pp_token::TOK_WSTRING_LIT,token_value::create("fff")));
  
	ts->add_back(sep);
	ts->add_back(pp_token::create(loc,pp_token::TOK_STRING_LIT,token_value::create("xxx")));
	ts->add_back(pp_token::create(loc,pp_token::TOK_WSTRING_LIT,token_value::create("yyy")));
	
	ts->add_back(sep);
	ts->add_back(pp_token::create(loc,pp_token::TOK_STRING_LIT,token_value::create("ggg")));
	ts->add_back(pp_token::create(loc,pp_token::TOK_STRING_LIT,token_value::create("hhh")));
	ts->add_back(pp_token::create(loc,pp_token::TOK_STRING_LIT,token_value::create("iii")));
	
	ts->add_back(sep);
	ts->add_back(pp_token::create(loc,pp_token::TOK_WSTRING_LIT,token_value::create("jjj")));
	ts->add_back(pp_token::create(loc,pp_token::TOK_WSTRING_LIT,token_value::create("kkk")));
	ts->add_back(pp_token::create(loc,pp_token::TOK_WSTRING_LIT,token_value::create("lll")));

	ts->add_back(sep);
	ts->add_back(pp_token::create(loc,pp_token::TOK_STRING_LIT,token_value::create("xxx")));
	ts->add_back(pp_token::create(loc,pp_token::TOK_WSTRING_LIT,token_value::create("yyy")));
	ts->add_back(pp_token::create(loc,pp_token::TOK_STRING_LIT,token_value::create("zzz")));

	ts->add_back(sep);
	ts->add_back(pp_token::create(loc,pp_token::TOK_WSTRING_LIT,token_value::create("www")));
	ts->add_back(pp_token::create(loc,pp_token::TOK_STRING_LIT,token_value::create("vvv")));
	ts->add_back(pp_token::create(loc,pp_token::TOK_WSTRING_LIT,token_value::create("uuu")));

#if 0
	ts->add_back(sep);
	ts->add_back(pp_token::create(loc,pp_token::TOK_STRING_LIT,token_value::create("xxx")));
	ts->add_back(pp_token::create(loc,pp_token::TOK_STRING_LIT,token_value::create("yyy")));
	ts->add_back(pp_token::create(loc,pp_token::TOK_STRING_LIT,token_value::create("zzz")));
	ts->add_back(pp_token::create(loc,pp_token::TOK_WSTRING_LIT,token_value::create("aaa")));
	ts->add_back(pp_token::create(loc,pp_token::TOK_WSTRING_LIT,token_value::create("bbb")));
	ts->add_back(pp_token::create(loc,pp_token::TOK_WSTRING_LIT,token_value::create("ccc")));
	ts->add_back(pp_token::create(loc,pp_token::TOK_STRING_LIT,token_value::create("ddd")));
	ts->add_back(pp_token::create(loc,pp_token::TOK_STRING_LIT,token_value::create("eee")));
	ts->add_back(pp_token::create(loc,pp_token::TOK_STRING_LIT,token_value::create("fff")));
#endif
	ts->add_back(sep);

	ptr<string_joiner> sj = string_joiner::create(ts);

	ptr<pp_token> r = sj->read();
	lassert(r);
	lassert(is_equal(r,sep));
	
	r = sj->read();
	lassert(r);
	lassert(is_equal(r->type_get(),pp_token::TOK_STRING_LIT));
	lassert(is_equal(r->value_get(),token_value::create("aaa")));
	
	r = sj->read();
	lassert(r);
	lassert(is_equal(r,sep));
	
	r = sj->read();
	lassert(r);
	lassert(is_equal(r->type_get(),pp_token::TOK_WSTRING_LIT));
	lassert(is_equal(r->value_get(),token_value::create("bbb")));

	r = sj->read();
	lassert(r);
	lassert(is_equal(r,sep));
	
	r = sj->read();
	lassert(r);
	lassert(is_equal(r->type_get(),pp_token::TOK_STRING_LIT));
	lassert(is_equal(r->value_get(),token_value::create("cccddd")));
	
	r = sj->read();
	lassert(r);
	lassert(is_equal(r,sep));
	
	r = sj->read();
	lassert(r);
	lassert(is_equal(r->type_get(),pp_token::TOK_WSTRING_LIT));
	lassert(is_equal(r->value_get(),token_value::create("eeefff")));
	
	r = sj->read();
	lassert(r);
	lassert(is_equal(r,sep));
	
	r = sj->read();
	lassert(r);
	lassert(is_equal(r->type_get(),pp_token::TOK_WSTRING_LIT));
	lassert(is_equal(r->value_get(),token_value::create("xxxyyy")));
	
	r = sj->read();
	lassert(r);
	lassert(is_equal(r,sep));
	

	r = sj->read();
	lassert(r);
	lassert(is_equal(r->type_get(),pp_token::TOK_STRING_LIT));
	lassert(is_equal(r->value_get(),token_value::create("ggghhhiii")));
	
	r = sj->read();
	lassert(r);
	lassert(is_equal(r,sep));
	
	r = sj->read();
	lassert(r);
	lassert(is_equal(r->type_get(),pp_token::TOK_WSTRING_LIT));
	lassert(is_equal(r->value_get(),token_value::create("jjjkkklll")));
	
	r = sj->read();
	lassert(r);
	lassert(is_equal(r,sep));
	
	r = sj->read();
	lassert(r);
	lassert(is_equal(r->type_get(),pp_token::TOK_WSTRING_LIT));
	lassert(is_equal(r->value_get(),token_value::create("xxxyyyzzz")));
	
	r = sj->read();
	lassert(r);
	lassert(is_equal(r,sep));
	
	r = sj->read();
	lassert(r);
	lassert(is_equal(r->type_get(),pp_token::TOK_WSTRING_LIT));
	lassert(is_equal(r->value_get(),token_value::create("wwwvvvuuu")));

#if 0
	r = sj->read();
	lassert(r);
	lassert(is_equal(r,sep));

	r = sj->read();
	lassert(r);
	lassert(is_equal(r->type_get(),pp_token::TOK_STRING_LIT));
	lassert(is_equal(r->value_get(),token_value::create("xxxyyyzzz")));
	
	r = sj->read();
	lassert(r);
	lassert(is_equal(r->type_get(),pp_token::TOK_WSTRING_LIT));
	lassert(is_equal(r->value_get(),token_value::create("aaabbbccc")));
	
	r = sj->read();
	lassert(r);
	lassert(is_equal(r->type_get(),pp_token::TOK_STRING_LIT));
	lassert(is_equal(r->value_get(),token_value::create("dddeeefff")));
	
	r = sj->read();
	lassert(r);
	lassert(is_equal(r,sep));
#endif
	
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

/*!
  \brief Main function.

  Runs the unit test in different namespace.
*/
int main(void)
{
	::lestes::lang::cplus::lex::string_joiner_test();
	return 0;
}
/* vim: set ft=lestes : */
