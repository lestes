/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/common.hh>
#include <lestes/std/source_location.hh>
#include <lestes/std/map.hh>
#include <lestes/lang/cplus/lex/preprocessor.hh>
#include <lestes/lang/cplus/lex/cpp_token.hh>
#include <lestes/lang/cplus/lex/token_value.hh>
#include <lestes/lang/cplus/lex/file_system.hh>
#include <lestes/lang/cplus/lex/lex_literal.g.hh>
#include <lestes/lang/cplus/lex/lex_loggers.hh>
#include <lestes/msg/report_end.hh>
#include <lestes/msg/report_error_flag.hh>
#include <lestes/msg/report_origin_filter.hh>
#include <lestes/msg/report_ostream.hh>
#include <lestes/msg/reporter.hh>

#include <iostream>
#include <fstream>
#include <utility>

package(lestes);
package(lang);
package(cplus);
package(lex);

void run(void)
{
	::lestes::msg::reporter::instance()->filter_set(
			::lestes::msg::report_origin_filter::create( 
				::lestes::msg::report_ostream::create(
					::lestes::msg::report_end::create(),
					ostream_wrapper::create(&::std::cerr,false))));

	ptr<file_system> fs = file_system::create();
	if (!fs->add_search_path("/usr/include")) {
		::std::cout << "could not add path\n";
	}
	ptr<preprocessor> pp = preprocessor::create(fs,"");

	ptr<cpp_token> tok;
	ptr<source_location> loc;
	cpp_token::type_type type;
	ptr<token_value> tv;
	ptr<lex_literal> literal;

	do {
		tok = pp->read();
		loc = tok->location_get();
		type = tok->type_get();
		tv = tok->value_get();
		literal = type == cpp_token::TOK_LITERAL ? tok->literal_get() : ptr<lex_literal>(NULL);
		
		::std::cout << loc->file_get()->name_get() << ':' << loc->line_get() << ':' << loc->column_get();
		::std::cout << ' ' << tok->description_get();
		
		if (tv) {
			::std::cout << ' ';

			ucn_string str(tv->content_get());
			for (ucn_string::iterator it = str.begin(), end = str.end(); it != end; ++it) {
				::std::cout << static_cast<ulint>(*it);
			}
			::std::cout << ' ' << tv->content_get();
		}

		if (literal) {
			::std::cout << ' ' << "LINFO";
		}
		
		::std::cout << ::std::endl;
		
	} while (tok->type_get() != cpp_token::TOK_EOF);

	if (pp->pragma_flag_get()) ::std::cout << "saw pragma\n";
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);


int main(int argc, const char **argv)
{
	bool log_finish = false;
	if (argc == 1) {
		// okay
	} else if (argc != 3) {
		::std::cerr << "Invalid arguments\n";
		return 1;
	} else if (!strcmp(argv[1],"-s")) {
		::std::ofstream of(argv[2]);
		::lestes::msg::logger::dump_skeleton(of);
	} else if (!strcmp(argv[1],"-l")) {
		log_finish = ::lestes::msg::logger::init(argv[2]);
		if (!log_finish) ::std::cerr << "Unable to init logger\n";
	} else {
		::std::cerr << "Invalid arguments\n";
		return 1;
	}

	::lestes::lang::cplus::lex::run();

	if (log_finish) {
		::lestes::msg::logger::finish();
	}

	return 0;
}

/* vim: set ft=lestes : */
