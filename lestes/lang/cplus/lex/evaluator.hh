/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___evaluator_hh___included
#define lestes__lang__cplus__lex___evaluator_hh___included

/*! \file
  \brief Directives evaluator.

  Declaration of evaluator class representing evaluator of preprocessing directives.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/std/vector.hh>
#include <lestes/lang/cplus/lex/pp_filter.hh>
package(lestes);
package(lang);
package(cplus);
package(lex);

// forward declaration to break cycle
class expander;
class unit_part;
class pp_token;
class token_sequence;
class macro_storage;
class named_istream;
class file_system;

/*!
  \brief Directives evaluator.

  Represents evaluator of preprocessing directives.
  Manages include stack.
*/
class evaluator: public pp_filter {
public:
	//! Returns pragma flag.
	bool pragma_flag_get(void) const;
	//! Reads single token.
	ptr<pp_token> read(void);
	//! Returns new instance.
	static ptr<evaluator> create(const ptr<file_system> &a_fs, const lstring &file_name);
protected:
	//! Creates new object.
	evaluator(const ptr<file_system> &a_fs, const lstring &file_name);
	//! Marks the object.
	virtual void gc_mark(void);
private:
	//! States of the evaluator.
	typedef enum {
		EMPTY,
		START,
		BUFFER,
		END,
	} state_type;
	enum {
		//! Nesting limit of include files.
		NESTING_LIMIT = 16
	};
	//! Type of include stack.
	typedef ::lestes::std::vector< srp<unit_part> > includes_type;
	//! Processes directive in buffer.
	ptr<pp_token> process_directive(void);
	//! Parses include directive in buffer.
	ptr<pp_token> parse_include(void);
	//! Checks extra tokens in directive.
	void check_extra_tokens(const lstring &name);
	//! Pushes stream to process.
	void push_stream(const ptr<named_istream> &ns, const ptr<source_location> &loc);
	//! Pops the processed stream.
	void pop_stream(void);
	//! File system binding.
	srp<file_system> fs;
	//! State of the evaluator.
	state_type state;
	//! Output active flag.
	bool active;
	//! Storage of defined macros.
	srp<macro_storage> macros;
	//! Include stack.
	srp<includes_type> includes;
	//! Input into evaluator.
	srp<expander> input;
	//! Current unit part.
	srp<unit_part> part;
	//! Buffer with pending tokens.
	srp<token_sequence> buffer;
	//! Flag of encountered pragma.
	bool pragma_flag;
	//! Nesting depth of includes.
	ulint nesting;
	//! Hides copy constructor.
	evaluator(const evaluator &copy);
	//! Hides assignment operator.
	evaluator &operator=(const evaluator &rhs);
};

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
