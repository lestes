/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Unit test.
  
  Unit test for template class basic_token.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/equality.hh>
#include <lestes/lang/cplus/lex/basic_token.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

using namespace ::std;

/*!
  \brief Test location.

  Location only for testing purposes.
*/
class loc: public ::lestes::std::object {
public:
	//! Tests equality.
	bool equals(const ptr<loc> &other) const;
	//! Returns new location.
	static ptr<loc> create(ulint a_position);
protected:
	//! Creates new location.
	loc(ulint a_position);
private:
	//! The represented position.
	ulint position;
	//! Hides copy constructor.
	loc(const loc &);
	//! Hides assingment operator.
	loc &operator=(const loc &);
};

/*!
  Creates new location.
  \param a_position  The position to initialize wiht.
*/
loc::loc(ulint a_position):
	position(a_position) 
{
}

/*!
  Tests equality to other location.
  \param other  The location to compare to.
  \return  True if both positions are the same.
*/
bool loc::equals(const ptr<loc> &rhs) const 
{
	return rhs && is_equal(position,rhs->position);
}

/*! 
  Returns new location, initializes with position.
  \param a_position  The position to initialize with.
*/
ptr<loc> loc::create(ulint a_position) {
	return new loc(a_position);
}

//! Token type.
typedef enum { POSITIVE = 1, ZERO = 0, NEGATIVE = -1 } ttype;

//! Helper typedef for basic_token template test.
typedef basic_token<ttype,ptr<loc>,int> basic_testing_token;

/*!
  \brief Testing token.

  Template instance to test basic_token.
*/
class testing_token: public basic_testing_token {
public:
	//! Clones the token.
	ptr<testing_token> clone(void) const;
	//! Returns new token.
	static ptr<testing_token> create(const location_type &a_location, const type_type &a_type,
			const value_type &a_value);
protected:
	//! Creates new token.
	testing_token(const location_type &a_location, const type_type &a_type,
			const value_type &a_value);   
};

/*!
  Creates new token, initializes all fields.
  \param a_location  The initial location.
  \param a_type  The initial token type.
  \param a_value  The initial token value.
*/
testing_token::testing_token(const location_type &a_location, const type_type &a_type,
			const value_type &a_value):
	basic_testing_token(a_location,a_type,a_value)
{   
}

/*!
  Clones the token.
  \post is_equal(this,returned)
  \return  New token with equal values.
*/
ptr<testing_token> testing_token::clone(void) const
{
	return new testing_token(location_get(),type_get(),value_get());
}

/*!
  Creates new token, initializes all fields.
  \param a_location  The initial location.
  \param a_type  The initial token type.
  \param a_value  The initial token value.
*/
ptr<testing_token> testing_token::create(const location_type& a_location, const type_type &a_type,
			const value_type &a_value)
{
	return new testing_token(a_location,a_type,a_value);
}

/*!
  \brief Tests basic token.

  Performs testing of basic token.
*/
void basic_token_test(void)
{
	ptr<loc> el = loc::create(13);
	ptr<loc> em = loc::create(17);
	ptr<testing_token> a = testing_token::create(em,ZERO,0);
	
	lassert(is_equal(a,a));
	lassert(is_equal(a->location_get(),em));
	lassert(is_equal(a->type_get(),ZERO));
	lassert(is_equal(a->value_get(),0));
	
	a = testing_token::create(el,POSITIVE,13);

	lassert(is_equal(a,a));
	lassert(is_equal(a->location_get(),el));
	lassert(is_equal(a->type_get(),POSITIVE));
	lassert(is_equal(a->value_get(),13));

	ptr<testing_token> b = testing_token::create(em,NEGATIVE,-4);
	ptr<testing_token> c = b->clone();

	lassert(is_equal(c,b));
	lassert(is_equal(b,c));
	lassert(is_equal(b->type_get(),c->type_get()));
	lassert(is_equal(b->value_get(),c->value_get()));
	lassert(is_equal(b->location_get(),c->location_get()));

	ptr<testing_token> d;

	d = a;

	lassert(is_equal(d,a));
	lassert(is_equal(a,d));
	lassert(is_equal(d->type_get(),POSITIVE));
	lassert(is_equal(d->value_get(),13));
	lassert(is_equal(d->location_get(),el));
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

/*!
  \brief Main function.

  Runs the unit test in different namespace.
*/
int main(void)
{
	::lestes::lang::cplus::lex::basic_token_test();
	return 0;
}
/* vim: set ft=lestes : */
