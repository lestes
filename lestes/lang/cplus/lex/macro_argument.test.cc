/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  Unit test for class macro_argument.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/std/source_location.hh>
#include <lestes/std/ucn_string.hh>
#include <lestes/lang/cplus/lex/macro_argument.hh>
#include <lestes/lang/cplus/lex/token_sequence.hh>
#include <lestes/lang/cplus/lex/pp_token.hh>
#include <string>

package(lestes);
package(lang);
package(cplus);
package(lex);

using namespace ::std;

void macro_argument_test(void)
{
	ptr<file_info> fi = file_info::create(string("abc"),NULL);
	ptr<source_location> loc = source_location::create(fi,0,0);
	
	ptr<token_sequence> ts = token_sequence::create();
	ts->add_back(pp_token::create(loc,pp_token::TOK_DOT));
	ts->add_back(pp_token::create(loc,pp_token::TOK_DOT));
	ts->add_back(pp_token::create(loc,pp_token::TOK_FILE_END));

	ts->add_back(pp_token::create(loc,pp_token::TOK_RIGHT_PAR));

	ts->add_back(pp_token::create(loc,pp_token::TOK_RIGHT_PAR));
		
	ts->add_back(pp_token::create(loc,pp_token::TOK_HAT));
	ts->add_back(pp_token::create(loc,pp_token::TOK_LT));
	ts->add_back(pp_token::create(loc,pp_token::TOK_GT));
	ts->add_back(pp_token::create(loc,pp_token::TOK_COMMA));

	ts->add_back(pp_token::create(loc,pp_token::TOK_EQ));
	ts->add_back(pp_token::create(loc,pp_token::TOK_DOT));
	ts->add_back(pp_token::create(loc,pp_token::TOK_RIGHT_PAR));

	ts->add_back(pp_token::create(loc,pp_token::TOK_STAR));
	ts->add_back(pp_token::create(loc,pp_token::TOK_LEFT_PAR));
	ts->add_back(pp_token::create(loc,pp_token::TOK_EMARK));
	ts->add_back(pp_token::create(loc,pp_token::TOK_RIGHT_PAR));
	ts->add_back(pp_token::create(loc,pp_token::TOK_QMARK));
	ts->add_back(pp_token::create(loc,pp_token::TOK_COMMA));
		
	ts->add_back(pp_token::create(loc,pp_token::TOK_LEFT_PAR));
	ts->add_back(pp_token::create(loc,pp_token::TOK_COMMA));
	ts->add_back(pp_token::create(loc,pp_token::TOK_LEFT_PAR));
	ts->add_back(pp_token::create(loc,pp_token::TOK_COMMA));
	ts->add_back(pp_token::create(loc,pp_token::TOK_RIGHT_PAR));
	ts->add_back(pp_token::create(loc,pp_token::TOK_COMMA));
	ts->add_back(pp_token::create(loc,pp_token::TOK_RIGHT_PAR));
	ts->add_back(pp_token::create(loc,pp_token::TOK_RIGHT_PAR));

	ptr<token_sequence> os1 = token_sequence::create();
	os1->add_back(pp_token::create(loc,pp_token::TOK_FILE_END));
	
	ptr<token_sequence> os2 = token_sequence::create();
	os2->add_back(pp_token::create(loc,pp_token::TOK_FILE_END));
	
	ptr<token_sequence> os3 = token_sequence::create();
	os3->add_back(pp_token::create(loc,pp_token::TOK_HAT));
	os3->add_back(pp_token::create(loc,pp_token::TOK_LT));
	os3->add_back(pp_token::create(loc,pp_token::TOK_GT));
	os3->add_back(pp_token::create(loc,pp_token::TOK_FILE_END));

	ptr<token_sequence> os4 = token_sequence::create();
	os4->add_back(pp_token::create(loc,pp_token::TOK_EQ));
	os4->add_back(pp_token::create(loc,pp_token::TOK_DOT));
	os4->add_back(pp_token::create(loc,pp_token::TOK_FILE_END));

	ptr<token_sequence> os5 = token_sequence::create();
	os5->add_back(pp_token::create(loc,pp_token::TOK_STAR));
	os5->add_back(pp_token::create(loc,pp_token::TOK_LEFT_PAR));
	os5->add_back(pp_token::create(loc,pp_token::TOK_EMARK));
	os5->add_back(pp_token::create(loc,pp_token::TOK_RIGHT_PAR));
	os5->add_back(pp_token::create(loc,pp_token::TOK_QMARK));
	os5->add_back(pp_token::create(loc,pp_token::TOK_FILE_END));

	ptr<token_sequence> os6 = token_sequence::create();
	os6->add_back(pp_token::create(loc,pp_token::TOK_LEFT_PAR));
	os6->add_back(pp_token::create(loc,pp_token::TOK_COMMA));
	os6->add_back(pp_token::create(loc,pp_token::TOK_LEFT_PAR));
	os6->add_back(pp_token::create(loc,pp_token::TOK_COMMA));
	os6->add_back(pp_token::create(loc,pp_token::TOK_RIGHT_PAR));
	os6->add_back(pp_token::create(loc,pp_token::TOK_COMMA));
	os6->add_back(pp_token::create(loc,pp_token::TOK_RIGHT_PAR));
	os6->add_back(pp_token::create(loc,pp_token::TOK_FILE_END));

	ptr<macro_argument> ma0 = macro_argument::create();
	
	// created object
	lassert(is_equal(ma0->state_get(),macro_argument::BEGIN));
	lassert(is_equal(ma0,ma0));
	
	// eof in argument
	lassert(is_equal(ma0->parse(ts,true),macro_argument::ERROR));
	lassert(is_equal(ma0->state_get(),macro_argument::DEAD));
	lassert(is_equal(ma0,ma0));
	// discard the eof
	ts->read_front();

	ptr<macro_argument> ma1 = macro_argument::create();
  
	// empty first argument
	lassert(is_equal(ma1->parse(ts,true),macro_argument::EMPTY));
	lassert(is_equal(ma1->state_get(),macro_argument::PARSED));
	lassert(is_equal(ma1,ma1));

	lassert(is_equal(ma1->nonexpanded_get(),os1));

	ptr<macro_argument> ma2 = macro_argument::create();
	lassert(!is_equal(ma2,ma1));
	
	// empty last argument
	lassert(is_equal(ma2->parse(ts,false),macro_argument::LAST));
	lassert(is_equal(ma2->state_get(),macro_argument::PARSED));
	lassert(is_equal(ma2,ma2));
	lassert(is_equal(ma1->nonexpanded_get(),os2));

	ptr<macro_argument> ma3 = macro_argument::create();
	
	// argument with comma
	lassert(is_equal(ma3->parse(ts,true),macro_argument::CONTINUE));
	lassert(is_equal(ma3->state_get(),macro_argument::PARSED));
	lassert(is_equal(ma3,ma3));
	lassert(is_equal(ma3->nonexpanded_get(),os3));

	ptr<macro_argument> ma4 = macro_argument::create();
	
	// argument with right parenthesis
	lassert(is_equal(ma4->parse(ts,true),macro_argument::LAST));
	lassert(is_equal(ma4->state_get(),macro_argument::PARSED));
	lassert(is_equal(ma4,ma4));
	lassert(is_equal(ma4->nonexpanded_get(),os4));

	ptr<macro_argument> ma5 = macro_argument::create();
	
	// argument with inner parentheses
	lassert(is_equal(ma5->parse(ts,true),macro_argument::CONTINUE));
	lassert(is_equal(ma5->state_get(),macro_argument::PARSED));
	lassert(is_equal(ma5,ma5));
	lassert(is_equal(ma5->nonexpanded_get(),os5));
	
	ptr<macro_argument> ma6 = macro_argument::create();
	
	// argument with inner parentheses and commas
	lassert(is_equal(ma6->parse(ts,true),macro_argument::LAST));
	lassert(is_equal(ma6->state_get(),macro_argument::PARSED));
	lassert(is_equal(ma6,ma6));
	lassert(is_equal(ma6->nonexpanded_get(),os6));
	
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

int main(void)
{
	::lestes::lang::cplus::lex::macro_argument_test();
	return 0;
}
/* vim: set ft=lestes : */
