/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  Unit test for class pp_lex.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/std/file_info.hh>
#include <lestes/std/source_location.hh>
#include <lestes/lang/cplus/lex/pp_lex.hh>
#include <lestes/lang/cplus/lex/pp_token.hh>
#include <lestes/lang/cplus/lex/string_source.hh>
#include <lestes/lang/cplus/lex/encoder_ascii7.hh>

#include <string>
#include <iostream>

package(lestes);
package(lang);
package(cplus);
package(lex);

using namespace ::std;

void pp_lex_test(void)
{
	char inp[] = 
		" [<:]:>}%>{<%#%:##%:%:)(;:?.+*%/^xor&bitand|bitor"
		"~compl!not=<>,-::.*+=-=*=/=%=^=xor_eq&=and_eq|=or_eq<<>>"
		">>=<<===!=not_eq<=>=&&and||or++--->*->..."
		"asm auto bool break case catch char class const_cast continue default delete do "
		"double dynamic_cast else enum explicit export extern float for friend goto if "
		"inline int long mutable namespace new operator private protected public register "
		"reinterpret_cast return short signed sizeof static static_cast struct switch "
		"template this throw try typedef typeid typename union unsigned using virtual "
		"void volatile wchar_t while false true define undef pragma error "
		"defined ifdef ifndef elif endif "
		"\n#include\n#line\n"
		"123 007 0xF00 1.23e+45 "
		"1eet 0range 0xygen 1.2.3e+e-45 "
		"'a''bcd''\\\'\\\"\\?\\\\\\a\\b\\r\\f\\n\\r\\t\\v''\\123\\xFaCe''\\udEaD\\U1234cdef'\n"
		"'''\\c\\d\\e\\g\\h\\i''\\xg\\x''\\u\\uabcx\\U\\Uabcdeef\\uf0''unterminated\n"
		"\"\"\"a\"\"bcd\"\"\\\'\\\"\\?\\\\\\a\\b\\r\\f\\n\\r\\t\\v\"\"\\123\\xFaCe\"\"\\udEaD\\U1234cdef\"\n"
		"\"\\c\\d\\e\\g\\h\\i\"\"\\xg\\x\"\"\\u\\uabcx\\U\\Uabcdef\\uf0\"\"unterminated\n"
		"\n";

	pp_token_type out[] = {
		pp_token::TOK_BLANK,
		pp_token::TOK_LEFT_BRACKET,
		pp_token::TOK_LEFT_BRACKET,
		pp_token::TOK_RIGHT_BRACKET,
		pp_token::TOK_RIGHT_BRACKET,
		pp_token::TOK_RIGHT_BRACE,
		pp_token::TOK_RIGHT_BRACE,
		pp_token::TOK_LEFT_BRACE,
		pp_token::TOK_LEFT_BRACE,
		pp_token::TOK_HASH,
		pp_token::TOK_HASH,
		pp_token::TOK_HASH_HASH,
		pp_token::TOK_HASH_HASH,
		pp_token::TOK_RIGHT_PAR,
		pp_token::TOK_LEFT_PAR,
		pp_token::TOK_SEMICOLON,
		pp_token::TOK_COLON,
		pp_token::TOK_QMARK,
		pp_token::TOK_DOT,
		pp_token::TOK_PLUS,
		pp_token::TOK_STAR,
		pp_token::TOK_PERCENT,
		pp_token::TOK_SLASH,
		pp_token::TOK_HAT,
		pp_token::TOK_HAT,
		pp_token::TOK_AMP,
		pp_token::TOK_AMP,
		pp_token::TOK_VBAR,
		pp_token::TOK_VBAR,
		pp_token::TOK_TILDE,
		pp_token::TOK_TILDE,
		pp_token::TOK_EMARK,
		pp_token::TOK_EMARK,
		pp_token::TOK_EQ,
		pp_token::TOK_LT,
		pp_token::TOK_GT,
		pp_token::TOK_COMMA,
		pp_token::TOK_MINUS,
		pp_token::TOK_COLON_COLON,
		pp_token::TOK_DOT_STAR,
		pp_token::TOK_PLUS_EQ,
		pp_token::TOK_MINUS_EQ,
		pp_token::TOK_STAR_EQ,
		pp_token::TOK_SLASH_EQ,
		pp_token::TOK_PERCENT_EQ,
		pp_token::TOK_HAT_EQ,
		pp_token::TOK_HAT_EQ,
		pp_token::TOK_AMP_EQ,
		pp_token::TOK_AMP_EQ,
		pp_token::TOK_VBAR_EQ,
		pp_token::TOK_VBAR_EQ,
		pp_token::TOK_LT_LT,
		pp_token::TOK_GT_GT,
		pp_token::TOK_GT_GT_EQ,
		pp_token::TOK_LT_LT_EQ,
		pp_token::TOK_EQ_EQ,
		pp_token::TOK_EMARK_EQ,
		pp_token::TOK_EMARK_EQ,
		pp_token::TOK_LT_EQ,
		pp_token::TOK_GT_EQ,
		pp_token::TOK_AMP_AMP,
		pp_token::TOK_AMP_AMP,
		pp_token::TOK_VBAR_VBAR,
		pp_token::TOK_VBAR_VBAR,
		pp_token::TOK_PLUS_PLUS,
		pp_token::TOK_MINUS_MINUS,
		pp_token::TOK_MINUS_GT_STAR,
		pp_token::TOK_MINUS_GT,
		pp_token::TOK_DOT_DOT_DOT,

		pp_token::TOK_ASM,
		pp_token::TOK_BLANK,
		pp_token::TOK_AUTO,
		pp_token::TOK_BLANK,
		pp_token::TOK_BOOL,
		pp_token::TOK_BLANK,
		pp_token::TOK_BREAK,
		pp_token::TOK_BLANK,
		pp_token::TOK_CASE,
		pp_token::TOK_BLANK,
		pp_token::TOK_CATCH,
		pp_token::TOK_BLANK,
		pp_token::TOK_CHAR,
		pp_token::TOK_BLANK,
		pp_token::TOK_CLASS,
		pp_token::TOK_BLANK,
		pp_token::TOK_CONST_CAST,
		pp_token::TOK_BLANK,
		pp_token::TOK_CONTINUE,
		pp_token::TOK_BLANK,
		pp_token::TOK_DEFAULT,
		pp_token::TOK_BLANK,
		pp_token::TOK_DELETE,
		pp_token::TOK_BLANK,
		pp_token::TOK_DO,
		pp_token::TOK_BLANK,
		pp_token::TOK_DOUBLE,
		pp_token::TOK_BLANK,
		pp_token::TOK_DYNAMIC_CAST,
		pp_token::TOK_BLANK,
		pp_token::TOK_ELSE,
		pp_token::TOK_BLANK,
		pp_token::TOK_ENUM,
		pp_token::TOK_BLANK,
		pp_token::TOK_EXPLICIT,
		pp_token::TOK_BLANK,
		pp_token::TOK_EXPORT,
		pp_token::TOK_BLANK,
		pp_token::TOK_EXTERN,
		pp_token::TOK_BLANK,
		pp_token::TOK_FLOAT,
		pp_token::TOK_BLANK,
		pp_token::TOK_FOR,
		pp_token::TOK_BLANK,
		pp_token::TOK_FRIEND,
		pp_token::TOK_BLANK,
		pp_token::TOK_GOTO,
		pp_token::TOK_BLANK,
		pp_token::TOK_IF,
		pp_token::TOK_BLANK,
		pp_token::TOK_INLINE,
		pp_token::TOK_BLANK,
		pp_token::TOK_INT,
		pp_token::TOK_BLANK,
		pp_token::TOK_LONG,
		pp_token::TOK_BLANK,
		pp_token::TOK_MUTABLE,
		pp_token::TOK_BLANK,
		pp_token::TOK_NAMESPACE,
		pp_token::TOK_BLANK,
		pp_token::TOK_NEW,
		pp_token::TOK_BLANK,
		pp_token::TOK_OPERATOR,
		pp_token::TOK_BLANK,
		pp_token::TOK_PRIVATE,
		pp_token::TOK_BLANK,
		pp_token::TOK_PROTECTED,
		pp_token::TOK_BLANK,
		pp_token::TOK_PUBLIC,
		pp_token::TOK_BLANK,
		pp_token::TOK_REGISTER,
		pp_token::TOK_BLANK,
		pp_token::TOK_REINTERPRET_CAST,
		pp_token::TOK_BLANK,
		pp_token::TOK_RETURN,
		pp_token::TOK_BLANK,
		pp_token::TOK_SHORT,
		pp_token::TOK_BLANK,
		pp_token::TOK_SIGNED,
		pp_token::TOK_BLANK,
		pp_token::TOK_SIZEOF,
		pp_token::TOK_BLANK,
		pp_token::TOK_STATIC,
		pp_token::TOK_BLANK,
		pp_token::TOK_STATIC_CAST,
		pp_token::TOK_BLANK,
		pp_token::TOK_STRUCT,
		pp_token::TOK_BLANK,
		pp_token::TOK_SWITCH,
		pp_token::TOK_BLANK,
		pp_token::TOK_TEMPLATE,
		pp_token::TOK_BLANK,
		pp_token::TOK_THIS,
		pp_token::TOK_BLANK,
		pp_token::TOK_THROW,
		pp_token::TOK_BLANK,
		pp_token::TOK_TRY,
		pp_token::TOK_BLANK,
		pp_token::TOK_TYPEDEF,
		pp_token::TOK_BLANK,
		pp_token::TOK_TYPEID,
		pp_token::TOK_BLANK,
		pp_token::TOK_TYPENAME,
		pp_token::TOK_BLANK,
		pp_token::TOK_UNION,
		pp_token::TOK_BLANK,
		pp_token::TOK_UNSIGNED,
		pp_token::TOK_BLANK,
		pp_token::TOK_USING,
		pp_token::TOK_BLANK,
		pp_token::TOK_VIRTUAL,
		pp_token::TOK_BLANK,
		pp_token::TOK_VOID,
		pp_token::TOK_BLANK,
		pp_token::TOK_VOLATILE,
		pp_token::TOK_BLANK,
		pp_token::TOK_WCHAR,
		pp_token::TOK_BLANK,
		pp_token::TOK_WHILE,
		pp_token::TOK_BLANK,
		pp_token::TOK_BOOL_LIT,
		pp_token::TOK_BLANK,
		pp_token::TOK_BOOL_LIT,
		pp_token::TOK_BLANK,
		pp_token::TOK_DEFINE,
		pp_token::TOK_BLANK,
		pp_token::TOK_UNDEF,
		pp_token::TOK_BLANK,
		pp_token::TOK_PRAGMA,
		pp_token::TOK_BLANK,
		pp_token::TOK_ERROR,
		pp_token::TOK_BLANK,
		pp_token::TOK_DEFINED,
		pp_token::TOK_BLANK,
		pp_token::TOK_IFDEF,
		pp_token::TOK_BLANK,
		pp_token::TOK_IFNDEF,
		pp_token::TOK_BLANK,
		pp_token::TOK_ELIF,
		pp_token::TOK_BLANK,
		pp_token::TOK_ENDIF,
		pp_token::TOK_BLANK,
		pp_token::TOK_LINE_END,
		pp_token::TOK_HASH,
		pp_token::TOK_INCLUDE,
		pp_token::TOK_INCL_SIG,
		pp_token::TOK_LINE_END,
		pp_token::TOK_HASH,
		pp_token::TOK_LINE,
		pp_token::TOK_LINE_SIG,
		pp_token::TOK_LINE_END,

		pp_token::TOK_NUMBER_LIT,
		pp_token::TOK_BLANK,
		pp_token::TOK_NUMBER_LIT,
		pp_token::TOK_BLANK,
		pp_token::TOK_NUMBER_LIT,
		pp_token::TOK_BLANK,
		pp_token::TOK_NUMBER_LIT,
		pp_token::TOK_BLANK,

		pp_token::TOK_NUMBER_LIT,
		pp_token::TOK_BLANK,
		pp_token::TOK_NUMBER_LIT,
		pp_token::TOK_BLANK,
		pp_token::TOK_NUMBER_LIT,
		pp_token::TOK_BLANK,
		pp_token::TOK_NUMBER_LIT,
		pp_token::TOK_BLANK,

		pp_token::TOK_CHAR_LIT,
		pp_token::TOK_CHAR_LIT,
		pp_token::TOK_CHAR_LIT,
		pp_token::TOK_CHAR_LIT,
		pp_token::TOK_CHAR_LIT,
		pp_token::TOK_LINE_END,
		
		pp_token::TOK_CHAR_LIT,
		pp_token::TOK_CHAR_LIT,
		pp_token::TOK_CHAR_LIT,
		pp_token::TOK_CHAR_LIT,
		pp_token::TOK_CHAR_LIT,
		pp_token::TOK_LINE_END,
		
		pp_token::TOK_STRING_LIT,
		pp_token::TOK_STRING_LIT,
		pp_token::TOK_STRING_LIT,
		pp_token::TOK_STRING_LIT,
		pp_token::TOK_STRING_LIT,
		pp_token::TOK_STRING_LIT,
		pp_token::TOK_LINE_END,
		
		pp_token::TOK_STRING_LIT,
		pp_token::TOK_STRING_LIT,
		pp_token::TOK_STRING_LIT,
		pp_token::TOK_STRING_LIT,
		pp_token::TOK_LINE_END,
		
		pp_token::TOK_LINE_END,
		pp_token::TOK_FILE_END
	};

	ptr<file_info> fi = file_info::create("abcd",NULL);

	ptr<pre_lex> plx = pre_lex::create(string_source::create(inp),encoder_ascii7::create());
	ptr<pp_lex> pp = pp_lex::create(plx,fi);
	ptr<pp_token> tok;

	ulint i = 0;
	do {
		
		tok = pp->read(tok && tok->type_get() == pp_token::TOK_LINE_END);
#if 0
		::std::cerr << " test " << i << " : " <<
			tok->name_get() << " " <<
			tok->type_get() << " ?== " << (int)out[i] << ""
			<< ::std::endl;
#if 0         
			(int)tok->value_get() << " ?== " << (int)ouv[i] << " offset " <<
			(int)tok->location_get()->offset_get() << " ?== " << loc[i]->offset_get() << " line " <<
			(int)tok->location_get()->line_get() << " ?== " << loc[i]->line_get() << " column " <<
			(int)tok->location_get()->column_get() << " ?== " << loc[i]->column_get() << " order " <<
			(int)tok->location_get()->order_get() << " ?== " << loc[i]->order_get() << ::std::endl;
#endif
#endif
		lassert(is_equal(tok->type_get(),out[i]));

		if (tok->type_get() == pp_token::TOK_FILE_END) break;
		i++;
	} while (true);
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

int main(void)
{
	::lestes::lang::cplus::lex::pp_lex_test();
	return 0;
}
/* vim: set ft=lestes : */

