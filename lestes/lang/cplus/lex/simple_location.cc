/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Simple token location.

  Definition of simple_location class representing simple token location.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/equality.hh>
#include <lestes/lang/cplus/lex/simple_location.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

using namespace ::std;

/*!
  Creates new object, initializes with file information and position.
  \param a_line  The line number.
  \param a_column  The column on the line.
*/
simple_location::simple_location(ulint a_line, ulint a_column):
	line(a_line),
	column(a_column)
{
}

/*!
  Returns position in the file.
  \return The line number.
*/
ulint simple_location::line_get(void) const
{
	return line;
}

/*!
  Returns position on the line.
  \return The column number.
*/
ulint simple_location::column_get(void) const
{
	return column;
}

/*!
  Tests equality to other simple_location.
  \param rhs  The location to compare to.
*/
bool simple_location::equals(const ptr<simple_location> &rhs) const
{
	return rhs &&
	  is_equal(line,rhs->line_get()) &&
	  is_equal(column,rhs->column_get());
}

/*!
  Returns new instance, initializes with position.
  \param a_line  The line number.
  \param a_column  The column on the line.
*/
ptr<simple_location> simple_location::create(ulint a_line, ulint a_column)
{
	return new simple_location(a_line,a_column);
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);
/* vim: set ft=lestes : */
