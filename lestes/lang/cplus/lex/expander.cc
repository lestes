/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Macro expander.
  
  Definition of expander class performing macro expansion.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/expander.hh>
#include <lestes/lang/cplus/lex/pp_filter.hh>
#include <lestes/lang/cplus/lex/pp_token.hh>
#include <lestes/lang/cplus/lex/token_stream.hh>
#include <lestes/lang/cplus/lex/token_sequence.hh>
#include <lestes/lang/cplus/lex/macro_storage.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

/*!
  Creates new expander.
  \pre a_input != NULL
  \pre a_macros != NULL
  \param a_input  The input into the expander.
  \param a_macros  The macro storage for macro expansion.
*/
expander::expander(const ptr<pp_filter> &a_input, const ptr<macro_storage> &a_macros):
	macros(checked(a_macros)),
	stream(token_stream::create(checked(a_input)))
{
}

/*!
  Returns current mode of the input.
  \return The mode of input.
*/
expander::mode_type expander::mode_get(void)
{
	ptr<pp_token> tok = stream->peek_front();

	switch (tok->type_get()) {
		case pp_token::TOK_HASH:
			return DIRECTIVE;
		case pp_token::TOK_TERMINATOR:
			return FILE_END;
		default:
			break;
	}
	return NORMAL;
}

/*!
  Returns the next line without expansion.
  \return  The next line.
*/
ptr<token_sequence> expander::read_line(void)
{
	return stream->read_line();
}

/*!
  Returs the next line expanded.
  \return The expanded line.
*/
ptr<token_sequence> expander::read_expanded(void)
{
	ptr<token_sequence> ts = stream->expand_line(macros);
	return ts;
}

/*!
  Returns new expander.
  \pre a_input != NULL
  \pre a_macros != NULL
  \param a_input  The input into the expander.
  \param a_macros  The macro storage for macro expansion.
*/
ptr<expander> expander::create(const ptr<pp_filter> &a_input, const ptr<macro_storage> &a_macros)
{
	return new expander(a_input,a_macros);
}

/*!
  Marks the object.
*/
void expander::gc_mark(void)
{
	macros.gc_mark();
	stream.gc_mark();
	object::gc_mark();
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);
/* vim: set ft=lestes : */
