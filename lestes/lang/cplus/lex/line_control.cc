/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Line number control.

  Definition of class line_control responsible for creating full location information
  from physical location and effects of #line directives.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/line_control.hh>
#include <lestes/lang/cplus/lex/simple_location.hh>
#include <lestes/std/file_info.hh>
#include <lestes/std/source_location.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

/*!
  Creates line control, initializes with file information.
  \pre a_file != NULL
  \param a_file  The file information for the created locations.
*/
line_control::line_control(const ptr<file_info> &a_file):
	file(checked(a_file)),
	delta(0)
{
}

/*!
  Changes line numbering so that the line after \a location will be to \a line_number.
  The behaviour is as through the #line directive.
  \param before  Location on line just before the change to take effect, due to multiline
  comments, it has to be the location of the newline at the end of the #line directive.
  \param line_number  The new line number for the next line after the #line directive.
*/
void line_control::change_line(const ptr<source_location> &before, ulint line_number)
{
	// the new delta is a correction of line_number by physical line number of the next line
	delta = line_number - (before->line_get() + 1 - delta);
}

/*!
  Changes current file name, so that the location will contain the new file name.
  The inclusion chain is left intact, only the most nested entry is affected.
  \param file_name  The new file name for the current file.
*/
void line_control::change_file(const lstring &file_name)
{
	// TODO pt make this a clone on file_info ???
	file = file_info::create(file_name,file->origin_get());
}

/*!
  Translates physical location to full source location considering changes of file names.
  \param physical  The physical location to translate.
  \return  The location with current file information and line number transformed appropriately.
*/
ptr<source_location> line_control::translate_location(const ptr<simple_location> &physical) const
{
	return source_location::create(file,physical->line_get() + delta,physical->column_get());
}

/*!
  Marks the object.
*/
void line_control::gc_mark(void)
{
	file.gc_mark();
	object::gc_mark();
}

/*!
  Returns new instance, initializes with file information.
  \param a_file  The file information for the created locations.
*/
ptr<line_control> line_control::create(const ptr<file_info> &a_file)
{
	return new line_control(a_file);
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);
/* vim: set ft=lestes : */
