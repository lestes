/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___named_istream_hh___included
#define lestes__lang__cplus__lex___named_istream_hh___included

/*! \file
  \brief Named input stream.
  
  Declaration of named_istream class representing named input stream.
  \author pt
*/
#include <lestes/common.hh>

package(lestes);

package(std);
// forward declaration to avoid cycle
class istream_wrapper;
end_package(std);

package(lang);
package(cplus);
package(lex);

/*!
  \brief Named input stream.
 
  Represents input stream with associated file name
  and print name of the stream.
*/
class named_istream: public ::lestes::std::object {
public:
	//! Returns file name of the source file. 
	lstring file_name_get(void) const;
	//! Returns print name of the stream.
	lstring print_name_get(void) const;
	//! Returns the stream.
	ptr<istream_wrapper> stream_get(void) const;
	//! Returns new instance.
	static ptr<named_istream> create(const ptr<istream_wrapper> &a_stream,
			const lstring &a_file_name, const lstring &a_print_name);
protected:
	//! Creates the object.
	named_istream(const ptr<istream_wrapper> &a_stream,
			const lstring &a_file_name, const lstring &a_print_name);
	//! Marks the object.
	virtual void gc_mark(void);
private:
	//! The input stream. 
	srp<istream_wrapper> stream;
	//! The file path and name.
	lstring file_name;
	//! The print name of the stream.
	lstring print_name;
	//! Hides copy constructor.
	named_istream(const named_istream &);
	//! Hides assignment operator.
	named_istream &operator=(const named_istream &);
};

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
