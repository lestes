/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Conditional directive.

  Definition of condition class representing conditional
  preprocessor directives.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/equality.hh>
#include <lestes/lang/cplus/lex/condition.hh>
#include <lestes/std/source_location.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

/*!
  Creates new condition, initializes with type and properties.
  \pre a_location != NULL
  \pre a_condition != COND_EMPTY
  \param a_location  The location of usage.
  \param a_type  The condition type.
  \param a_waiting  Flag set when waiting for the true branch.
  \param a_active  Flag set when the output was active in the level before this condition.
  \return  New condition with given properties.
*/
condition::condition(type_type a_type, bool a_waiting, bool a_active, const ptr<source_location> &a_location):
	type((lassert(a_type != COND_EMPTY),a_type)),
	waiting(a_waiting),
	active(a_active),
	location(checked(a_location))
{
}

/*!
  Creates empty condition, special value to simplify processing.
  Properties do not have any meaning for this value.
  \post type_get() == condition::COND_EMPTY
  \post active == true
  \return  New empty condition.
*/
condition::condition(void):
	type(COND_EMPTY),
	waiting(false),
	active(true),
	location(NULL)
{
}

/*!
  Returns type of the condition.
  \return  The type of the condition.
*/
condition::type_type condition::type_get(void) const
{
	return type;
}

/*!
  Returns the location of usage.
  \pre type != COND_EMPTY
  \return The usage source_location.
*/
ptr<source_location> condition::location_get(void) const
{
	lassert(type != COND_EMPTY);
	return location;
}

/*!
  Returns waiting flag.
  \pre type != COND_EMPTY
  \return true  If waiting for true branch of the condition.
*/
bool condition::waiting_get(void) const
{
	lassert(type != COND_EMPTY);
	return waiting;
}

/*!
  Returns output activity flag.
  \return true  If output is active  in the previous level of conditionals.
*/
bool condition::active_get(void) const
{
	return active;
}

/*!
  Returns printable name of the condition.
  \pre type != COND_EMPTY
  \return  The name to be used in error messages.
*/
lstring condition::name_get(void) const
{
	lassert(type != COND_EMPTY);
	return condition_name[type];
}

/*!
  Tests equality to other condition.
  \param other  The condition to compare with.
  \return true  If both objects represent the same condition.
*/
bool condition::equals(const ptr<condition> &other) const
{
	type_type t = other->type_get();

	// empty conditions do not have other fields
	if (t == COND_EMPTY) return is_equal(type_get(),COND_EMPTY);
	
	return is_equal(type_get(),other->type_get()) &&
		 is_equal(waiting_get(),other->waiting_get()) &&
		 is_equal(active_get(),other->active_get()) &&
		 is_equal(location_get(),other->location_get());
}

/*!
  Marks the object.
*/
void condition::gc_mark(void)
{
	location.gc_mark();
	::lestes::std::object::gc_mark();
}

/*!
  Returns empty condition, special value to simplify processing.
  \return  New empty condition.
*/
ptr<condition> condition::create_empty(void)
{
	return new condition();
}
  
/*!
  Returns new condition, initializes with type and properties.
  \param a_type  The condition type.
  \param a_waiting  Flag set when waiting for the true branch.
  \param a_active  Flag set when the output was active in the level before this condition.
  \param a_location  The location of usage.
  \return  New condition with given properties.
*/
ptr<condition> condition::create(type_type a_type, bool a_waiting, bool a_active,const ptr<source_location> &a_location)
{
	return new condition(a_type,a_waiting,a_active,a_location);
}

/*!
  Printable names of the conditions for error reporting.
*/
lstring condition::condition_name[] = {
	"","#if","#ifdef","#ifndef","#elif","#else"
};

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

/* vim: set ft=lestes : */
