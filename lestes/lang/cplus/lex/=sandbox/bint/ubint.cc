/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <limits>
#include <iostream>
#include <iomanip>
#include <cassert>

using namespace ::std;

typedef unsigned int ulint;

template <ulint B>
class ubint;

template <ulint B>
ostream& operator<<(ostream &o, const ubint<B> &x);

template <ulint B>
class ubint {
	struct Condition {
		Condition() {}
	private:
		// TODO ???
		void operator delete(void*);
	};
public:
	//! Zero instance.
	static ubint<B> zero;
	//typedef unsigned char item_type;
	typedef unsigned long item_type;

	ubint(void);
	explicit ubint(item_type value);
	// TODO explicit ubint(const lstring &str);
	ubint(const ubint<B> &other);
	~ubint(void);

	//ubint<B> &operator=(item_type value);
	ubint<B> &operator=(const ubint<B> &other);
	ubint<B> operator-(void);
	ubint<B> operator+(void);
	ubint<B> &operator+=(const ubint<B> &other);
	ubint<B> operator+(const ubint<B> &other);
	ubint<B> &operator-=(const ubint<B> &other);
	ubint<B> operator-(const ubint<B> &other);
	bool operator==(const ubint<B> &other);
	bool operator!=(const ubint<B> &other);
	operator Condition*(void) const;
	bool operator!(void);
	ubint<B> operator~(void);
	ubint<B> &operator^=(const ubint<B> &other);
	ubint<B> operator^(const ubint<B> &other);
	ubint<B> &operator&=(const ubint<B> &other);
	ubint<B> operator&(const ubint<B> &other);
	ubint<B> &operator|=(const ubint<B> &other);
	ubint<B> operator|(const ubint<B> &other);
	bool operator<(const ubint<B> &other);
	bool operator>(const ubint<B> &other);
	bool operator<=(const ubint<B> &other);
	bool operator>=(const ubint<B> &other);
	ubint<B> operator++(void);
	ubint<B> operator++(int);
	ubint<B> operator--(void);
	ubint<B> operator--(int);
	ubint<B> &operator>>=(const ubint<B> &other);
	ubint<B> operator>>(const ubint<B> &other);
	ubint<B> &operator<<=(const ubint<B> &other);
	ubint<B> operator<<(const ubint<B> &other);
	ubint<B> &operator*=(const ubint<B> &other);
	ubint<B> operator*(const ubint<B> &other);
	ubint<B> &operator/=(const ubint<B> &other);
	ubint<B> operator/(const ubint<B> &other);
	ubint<B> &operator%=(const ubint<B> &other);
	ubint<B> operator%(const ubint<B> &other);
	
	ostream& print(ostream &o) const;

private:
	void assign(const ubint<B> &other);
	void bit_compl(void);
	void bit_xor(const ubint<B> &other);
	void bit_and(const ubint<B> &other);
	void bit_or(const ubint<B> &other);
	void increment(void);
	void decrement(void);
	void negate(void);
	void add(const ubint<B> &other);
	void sub(const ubint<B> &other);
	void move_left(void);
	void mul(const ubint<B> &other, item_type value);
	void mul(const ubint<B> &other);
	void div(const ubint<B> &other);
	void mod(const ubint<B> &other);
	void shr(item_type len);
	void shl(item_type len);
	void shr(const ubint<B> &other);
	void shl(const ubint<B> &other);
	bool equal(const ubint<B> &other) const;
	bool less_than(const ubint<B> &other) const;
	bool less_than_equal(const ubint<B> &other) const;
	
	item_type upper_half(item_type x);
	item_type lower_half(item_type x);
	item_type add_item(item_type &a, item_type b);
	item_type mul_item(item_type &ab, item_type cd);

	//! Length of item type in bits.
	static const ulint item_len = sizeof(item_type) * CHAR_BIT;
	//! Number of necessary item elements.
	static const ulint n = (B + item_len - 1)/item_len;
	//! Index of the last element.
	static const ulint last = n - 1;
	//! Total length in bits.
	static const ulint total_len = B;
	//! Length of half of item type in bits.
	static const ulint half_len = item_len / 2;
	//! Length of last item in bits.
	static const ulint last_len = item_len - (item_len - (B % item_len)) % item_len;
	// Mask of half of item.
	static const item_type half_mask = (static_cast<item_type>(1) << half_len) - 1;
	// Mask of last element.
	static const item_type last_mask = 
		(((static_cast<item_type>(1) << (last_len - 1)) - 1) << 1) | 1;

	//! The data of the number.
	item_type data[n];
};

template <ulint B>
const ulint ubint<B>::item_len;

template <ulint B>
const ulint ubint<B>::n;

template <ulint B>
const ulint ubint<B>::last;

template <ulint B>
const ulint ubint<B>::total_len;

template <ulint B>
const ulint ubint<B>::half_len;

template <ulint B>
const ulint ubint<B>::last_len;

template <ulint B>
const typename ubint<B>::item_type ubint<B>::half_mask;

template <ulint B>
const typename ubint<B>::item_type ubint<B>::last_mask;

template <ulint B>
ubint<B> ubint<B>::zero;

template <ulint B>
inline typename ubint<B>::item_type ubint<B>::lower_half(item_type x)
{
	return x & half_mask;
}

template <ulint B>
inline typename ubint<B>::item_type ubint<B>::upper_half(item_type x)
{
	return x >> half_len;
}

template <ulint B>
inline ostream& ubint<B>::print(ostream &o) const
{
	o << '<';
	for (ulint i = 0; i < n; i++) {
		o << (i?" ":"") << hex << setw(item_len/4) << setfill('0') << (ulint)data[n - i - 1];
	}
	o << '>';
	return o;
}

template <ulint B>
ostream &operator<<(ostream &o, const ubint<B> &x)
{
	return x.print(o);
}

template <ulint B>
inline ubint<B>::ubint(void)
{
	for (ulint i = 0; i < n; i++) data[i] = 0;
}

template <ulint B>
inline ubint<B>::ubint(const ubint<B> &other)
{
	for (ulint i = 0; i < n; i++) data[i] = other.data[i];
}

/* TODO
template <ulint B>
inline ubint<B>::ubint(const lstring &str)
{
	assign(zero);

	for (lstring::iterator it = str.begin(), end = str.end();
			it != end; ++it) {
		char c = *it;
		
		
	}
	data[0] = value;
	for (ulint i = 1; i < n; i++) data[i] = 0;
	data[last] &= last_mask;
}
*/

template <ulint B>
inline ubint<B>::ubint(item_type value)
{
	data[0] = value;
	for (ulint i = 1; i < n; i++) data[i] = 0;
	data[last] &= last_mask;
}

template <ulint B>
inline ubint<B>::~ubint(void)
{
}

template <ulint B>
inline void ubint<B>::assign(const ubint<B> &other)
{
	for (ulint i = 0; i < n; i++) data[i] = other.data[i];
}

template <ulint B>
inline void ubint<B>::bit_compl(void)
{
	for (ulint i = 0; i < n; i++) {
		data[i] = ~data[i];
	}
	data[last] &= last_mask;
}

template <ulint B>
inline void ubint<B>::bit_xor(const ubint<B> &other)
{
	for (ulint i = 0; i < n; i++) {
		data[i] ^= other.data[i];
	}
	data[last] &= last_mask;
}

template <ulint B>
inline void ubint<B>::bit_and(const ubint<B> &other)
{
	for (ulint i = 0; i < n; i++) {
		data[i] &= other.data[i];
	}
	data[last] &= last_mask;
}

template <ulint B>
inline void ubint<B>::bit_or(const ubint<B> &other)
{
	for (ulint i = 0; i < n; i++) {
		data[i] |= other.data[i];
	}
	data[last] &= last_mask;
}

template <ulint B>
inline void ubint<B>::increment(void)
{
	bool c = true;
	for (ulint i = 0; c && i < n; i++) {
		data[i] = data[i] + 1;
		c = data[i] == 0;
	}
	data[last] &= last_mask;
}

template <ulint B>
inline void ubint<B>::decrement(void)
{
	bool b = true;
	for (ulint i = 0; b && i < n; i++) {
		b = data[i] == 0;
		data[i] = data[i] - 1;
	}
	data[last] &= last_mask;
}

template <ulint B>
inline void ubint<B>::negate(void)
{
	bit_compl();
	increment();
}

/*
	add to item
*/
template <ulint B>
inline typename ubint<B>::item_type ubint<B>::add_item(item_type &a, item_type b)
{
	a += b;
	return a < b;
}

/*
	Multiplies an item by another item and returns carry.
	\param ab  The item to multiply.
	\param cd  The item to multiply with.
	\return The carry value.
*/
template <ulint B>
inline typename ubint<B>::item_type ubint<B>::mul_item(item_type &ab, item_type cd)
{
	// ab * cd = (a*M + b)*(c*M + d) = ac*M*M + (ad + bc)*M + bd

	item_type bd = lower_half(ab) * lower_half(cd);
	item_type ac = upper_half(ab) * upper_half(cd);

	item_type ad = upper_half(ab) * lower_half(cd);
	item_type bc = lower_half(ab) * upper_half(cd);

	item_type x = bd + (lower_half(ad) << (half_len));
	if (x < bd) ac++;

	item_type y = x + (lower_half(bc) << (half_len));
	if (y < x) ac++;

	ac += upper_half(ad) + upper_half(bc);

	ab = y;
	return ac;
}

template <ulint B>
inline void ubint<B>::add(const ubint<B> &other)
{
	item_type x, c = 0; 

	// beware of modifying self before access from other is finished
	for (ulint i = 0; i < n; i++) {
		x = data[i];
		c = add_item(x,c);
		// we know that carry is in at most one operation
		c = c | add_item(x,other.data[i]);
		data[i] = x;
	}

	data[last] &= last_mask;
}

template <ulint B>
inline void ubint<B>::sub(const ubint<B> &other)
{
	if (this == &other) {
		assign(zero);
		return;
	}

	negate();
	add(other);
	negate();
}

template <ulint B>
inline void ubint<B>::move_left(void)
{
	ulint m = n - 1;
	for (ulint i = 0; i < m; i++) {
		data[n - i - 1] = data[n - i - 2];
	}
	data[0] = 0;
}

template <ulint B>
inline void ubint<B>::mul(const ubint<B> &other, item_type value)
{
	item_type x, y, c = 0;
	for (ulint i = 0; i < n; i++) {
		x = other.data[i];
		y = mul_item(x,value);
		// carry cannot overflow
		c = add_item(x,c) + y;
		data[i] = x;
	}

	data[last] &= last_mask;
}

template <ulint B>
inline void ubint<B>::mul(const ubint<B> &other)
{
	if (this == &other) {
		ubint<B> copy(other);
		mul(copy);
		return;
	}

	ubint<B> moved(*this);
	ubint<B> added;

	assign(zero);
	
	ulint i = 0;
	while (true) {
		added.mul(moved,other.data[i]);
		add(added);
		if (++i == n) break;
		moved.move_left();
	}

	data[last] &= last_mask;
}

template <ulint B>
inline void ubint<B>::div(const ubint<B> &other)
{
	// find the highest bit of the divisor
	ulint h = total_len;
	item_type x, y;
	for (ulint i = n; i > 0; i--) {
		x = other.data[i - 1];
		if (x) {
			ulint j = (i == n ?  last_len : item_len) - 1;
			y = 1 << j;
			for ( ; !(x & y); j--, y >>= 1)
				;
			h = item_len * (i - 1) + j;
			break;
		}
	}

	// check nonzero
	assert(h != total_len);
	
	h = total_len - h - 1;
	// the value to be moved 
	ubint<B> moved(other);
	moved.shl(h);

	ubint<B> ratio;

	while (true) {

	//	cout << " h = " << dec << h << '\n';
		if (*this >= moved) {
	//		cout << *this << " >= " << moved << '\n';
	//		cout << "adding to " << (h/item_len) << " val " << (1 << (h % item_len)) << '\n';

			sub(moved);
			ratio.data[h / item_len] |= 1 << (h % item_len);
		} else {
	//		cout << *this << " <  " << moved << '\n';
		}
		
	//	cout << ratio << '\n';

		if (!h--) break;
		moved.shr(1);
	}

	assign(ratio);

	data[last] &= last_mask;
}

template <ulint B>
inline void ubint<B>::mod(const ubint<B> &other)
{
	// find the highest bit of the divisor
	ulint h = total_len;
	item_type x, y;
	for (ulint i = n; i > 0; i--) {
		x = other.data[i - 1];
		if (x) {
			ulint j = (i == n ?  last_len : item_len) - 1;
			y = 1 << j;
			for ( ; !(x & y); j--, y >>= 1)
				;
			h = item_len * (i - 1) + j;
			break;
		}
	}

	// check nonzero
	assert(h != total_len);
	
	h = total_len - h - 1;
	// the value to be moved 
	ubint<B> moved(other);
	moved.shl(h);

	while (true) {

		if (*this >= moved) {
			sub(moved);
		}

		if (!h--) break;
		moved.shr(1);
	}

	data[last] &= last_mask;
}

#define my_shr(x,y) (((y) < item_len) ? ((x) >> (y)) : 0)
#define my_shl(x,y) (((y) < item_len) ? ((x) << (y)) : 0)

template <ulint B>
inline void ubint<B>::shr(item_type len)
{
	// no checking for len > total_len !
	ulint delta = len / item_len;
	ulint gamma = len % item_len;
	ulint beta = item_len - gamma;

	item_type x, y;
	ulint i, j;
	
	for (i = 0; i < n; i++) {
		j = i + delta + 1;
		x = j < n ? data[j] : 0;
		y = (j - 1) < n ? data[j - 1] : 0;
		data[i] = my_shl(x,beta) | my_shr(y,gamma);
	}
	data[last] &= last_mask;
}

template <ulint B>
inline void ubint<B>::shl(item_type len)
{
	//cout << "shl " << *this << " by " << (ulint)len << '\n';

	// no checking for len > total_len !
	ulint delta = len / item_len;
	ulint gamma = len % item_len;
	ulint beta = item_len - gamma;

	item_type x, y;
	ulint i, j;
	for (i = 0; i < n; i++) {
		j = i + delta + 1;
		x = j < n ? data[n - j - 1] : 0;
		y = (j - 1) < n ? data[n - j] : 0;
		data[n - i - 1] = my_shr(x,beta) | my_shl(y,gamma);
	}
	data[last] &= last_mask;
	
	//cout << "end " << *this << '\n';
}

#undef my_shr
#undef my_shl

template <ulint B>
inline void ubint<B>::shl(const ubint<B> &other)
{
	//cout << "shl " << *this << " by " << other << '\n';

	// check if moving out everything
	for (ulint i = 0; i < n; i++) {
		if (other.data[i] > (i ? 0 : total_len - 1)) {
			assign(zero);
			return;
		}
	}

	shl(other.data[0]);
}

template <ulint B>
inline void ubint<B>::shr(const ubint<B> &other)
{
	// check if moving out everything
	for (ulint i = 0; i < n; i++) {
		if (other.data[i] > (i ? 0 : total_len - 1)) {
			assign(zero);
			return;
		}
	}

	shr(other.data[0]);
}

template <ulint B>
inline bool ubint<B>::equal(const ubint<B> &other) const
{
	for (ulint i = 0; i < n; i++) {
		if (data[i] != other.data[i]) return false;
	}
	return true;
}

template <ulint B>
inline bool ubint<B>::less_than(const ubint<B> &other) const
{
	item_type x, y;
	for (ulint i = 0; i < n; i++) {
		x = data[n - i - 1];
		y = other.data[n - i - 1];
		if (x < y) return true;
		if (x > y) return false;
	}
	return false;
}

template <ulint B>
inline bool ubint<B>::less_than_equal(const ubint<B> &other) const
{
	item_type x, y;
	for (ulint i = 0; i < n; i++) {
		x = data[n - i - 1];
		y = other.data[n - i - 1];
		if (x < y) return true;
		if (x > y) return false;
	}
	return true;
}

/* TODO
template <ulint B>
inline ubint<B> &ubint<B>::operator=(item_type value)
{
	assign(zero);
	data[0] = value;
	data[last] &= last_mask;
	return *this;
}
*/

template <ulint B>
inline ubint<B> &ubint<B>::operator=(const ubint<B> &other)
{
	assign(other);
	return *this;
}

template <ulint B>
inline ubint<B> ubint<B>::operator-(void)
{
	ubint<B> result(*this);
	result.negate();
	return result;
}

template <ulint B>
inline ubint<B> ubint<B>::operator+(void)
{
	return *this;
}

template <ulint B>
inline ubint<B> &ubint<B>::operator+=(const ubint<B> &other)
{
	add(other);
	return *this;
}

template <ulint B>
inline ubint<B> ubint<B>::operator+(const ubint<B> &other)
{
	ubint<B> result(*this);
	result.add(other);
	return result;
}

template <ulint B>
inline ubint<B> &ubint<B>::operator-=(const ubint<B> &other)
{
	sub(other);
	return *this;
}

template <ulint B>
inline ubint<B> ubint<B>::operator-(const ubint<B> &other)
{
	ubint<B> result(*this);
	result -= other;
	return result;
}

template <ulint B>
inline bool ubint<B>::operator==(const ubint<B> &other)
{
	return equal(other);
}

template <ulint B>
inline bool ubint<B>::operator!=(const ubint<B> &other)
{
	return !equal(other);
}

template <ulint B>
inline ubint<B>::operator typename ubint<B>::Condition*(void) const {
	if (equal(zero)) return 0;
	static Condition t;
	return &t;
}

/* TODO
template <ulint B>
inline ubint<B>::operator bool(void)
{
	return !equal(zero);
}
*/

template <ulint B>
inline bool ubint<B>::operator!(void)
{
	return equal(zero);
}

template <ulint B>
inline ubint<B> ubint<B>::operator~(void)
{
	ubint<B> result(*this);
	result.bit_compl();
	return result;
}

template <ulint B>
inline ubint<B> &ubint<B>::operator^=(const ubint<B> &other)
{
	bit_xor(other);
	return *this;
}

template <ulint B>
inline ubint<B> ubint<B>::operator^(const ubint<B> &other)
{
	ubint<B> result(*this);
	result.bit_xor(other);
	return result;
}

template <ulint B>
inline ubint<B> &ubint<B>::operator&=(const ubint<B> &other)
{
	bit_and(other);
	return *this;
}

template <ulint B>
inline ubint<B> ubint<B>::operator&(const ubint<B> &other)
{
	ubint<B> result(*this);
	result.bit_and(other);
	return result;
}

template <ulint B>
inline ubint<B> &ubint<B>::operator|=(const ubint<B> &other)
{
	bit_or(other);
	return *this;
}

template <ulint B>
inline ubint<B> ubint<B>::operator|(const ubint<B> &other)
{
	ubint<B> result(*this);
	result.bit_or(other);
	return result;
}

template <ulint B>
inline bool ubint<B>::operator<(const ubint<B> &other)
{
	return less_than(other);
}

template <ulint B>
inline bool ubint<B>::operator>(const ubint<B> &other)
{
	return other.less_than(*this);
}

template <ulint B>
inline bool ubint<B>::operator<=(const ubint<B> &other)
{
	return less_than_equal(other);
}

template <ulint B>
inline bool ubint<B>::operator>=(const ubint<B> &other)
{
	return other.less_than_equal(*this);
}

template <ulint B>
inline ubint<B> ubint<B>::operator++(void)
{
	increment();
	return *this;
}

template <ulint B>
inline ubint<B> ubint<B>::operator++(int)
{
	ubint<B> result(*this);
	increment();
	return result;
}

template <ulint B>
inline ubint<B> ubint<B>::operator--(void)
{
	decrement();
	return *this;
}

template <ulint B>
inline ubint<B> ubint<B>::operator--(int)
{
	ubint<B> result(*this);
	decrement();
	return result;
}

template <ulint B>
inline ubint<B> &ubint<B>::operator>>=(const ubint<B> &other)
{
	shr(other);
	return *this;
}

template <ulint B>
inline ubint<B> ubint<B>::operator>>(const ubint<B> &other)
{
	ubint<B> result(*this);
	result.shr(other);
	return result;
}

template <ulint B>
inline ubint<B> &ubint<B>::operator<<=(const ubint<B> &other)
{
	shl(other);
	return *this;
}

template <ulint B>
inline ubint<B> ubint<B>::operator<<(const ubint<B> &other)
{
	ubint<B> result(*this);
	result.shl(other);
	return result;
}

template <ulint B>
inline ubint<B> &ubint<B>::operator*=(const ubint<B> &other)
{
	mul(other);
	return *this;
}

template <ulint B>
inline ubint<B> ubint<B>::operator*(const ubint<B> &other)
{
	ubint<B> result(*this);
	result.mul(other);
	return result;
}

template <ulint B>
inline ubint<B> &ubint<B>::operator/=(const ubint<B> &other)
{
	div(other);
	return *this;
}

template <ulint B>
inline ubint<B> ubint<B>::operator/(const ubint<B> &other)
{
	ubint<B> result(*this);
	result.div(other);
	return result;
}

template <ulint B>
inline ubint<B> &ubint<B>::operator%=(const ubint<B> &other)
{
	mod(other);
	return *this;
}

template <ulint B>
inline ubint<B> ubint<B>::operator%(const ubint<B> &other)
{
	ubint<B> result(*this);
	result.mod(other);
	return result;
}

int main(void)
{
	typedef ubint<128> tt;

	tt max(100);

#if 0
	for (tt i = tt::zero; i < max; i++) {
		for (tt j = i; j < max; j++) {

			{
				tt x = i;
				tt y = x;

				assert(x == y);
			}

			{
				tt x = i;
				tt y = -x;
				tt z = -y;

				assert(x == z);
			}

			{
				tt x = i;
				tt y = +x;

				assert(x == y);
			}

			{
				tt x = i;
				tt y = j;
				tt z = j;
				tt w = i;

				x += z;
				y += w;

				assert(x == y);
			}

			{
				tt x = i + j + i;
				tt y = j + i + i;

				assert(x == y);
			}

			{
				tt x = j + j;
				tt y = j + i;
				tt z = j;
				tt w = i;

				x -= z;
				y -= w;

				assert(x == y);
			}

			{
				tt x = i + j - i;
				tt y = j - i + i;

				assert(x == y);
			}

			{
				tt x = i;
				tt y = j;

				if (!((x == x) && (y == y) && !(x == x + tt(1)))) {
					::std::cerr << " i = " << i << '\n';
					::std::cerr << " j = " << j << '\n';
					::std::cerr << " x = " << x << '\n';
					::std::cerr << " y = " << y << '\n';
				}

				assert((x == x) && (y == y) && !(x == x + tt(1)));
			}

			{
				tt x = i;
				tt y = j;
				tt z = i;

				assert(!(x != x) && !(y != y) && (x != x + tt(1)));
			}

			{
				tt x = i;
				tt y = j;

				assert((i > j && i) || (i == j) || j);
			}

			{
				tt x = i;

				assert(!x || (x && !!x));
			}

			{
				tt x = i;
				tt y = ~~i;

				assert(x == y);
			}

			{
				tt x = i;
				tt y = j;
				tt z = i;

				x ^= y;
				y ^= x;

				assert(y == z);
			}
				
			{
				tt x = i ^ j;
				tt y = j ^ i;

				assert(x == y);
			}
				
			{
				tt x = i;
				tt y = j;
				tt z = ~i;
				tt w = ~j;

				x &= y;
				z |= w;
				z = ~z;

				assert(x == z);
			}

			{
				tt x = i & j;
				tt y = j & i;

				assert(x == y);
			}

			{
				tt x = i | j;
				tt y = j | i;

				assert(x == y);
			}

			{
				tt x = i;
				tt y = j;

				assert(x == y || (x > y && y < x) || (x < y && y > x));
			}

			{
				tt x = i;
				tt y = j;

				assert(x == y || (x >= y && y <= x) || (x <= y && y >= x));
			}

			{
				tt x = i;
				tt y = i;
				tt z = ++x;
				
				assert(x == z && x != y);
			}

			{
				tt x = i;
				tt y = i;
				tt z = x++;
				
				assert(y == z && x != y);
			}

			{
				tt x = i;
				tt y = i;
				tt z = --x;
				
				assert(x == z && x != y);
			}

			{
				tt x = i;
				tt y = i;
				tt z = x--;
				
				assert(y == z && x != y);
			}

			{
				tt x = i;
				tt y = i;
				tt z(1);
				tt w(2);
				
				x >>= z;
				x >>= z;
				y >>= w;

				assert(x == y);
			}

			{
				tt x = i >> i;
				tt y = i >> j;
				
				x = x >> j;
				y = y >> i;

				assert(x == y);
			}

			{
				tt x = i;
				tt y = i;
				tt z(1);
				tt w(2);
				
				x <<= z;
				x <<= z;
				y <<= w;

				assert(x == y);
			}

			{
				tt x = i << i;
				tt y = i << j;
				
				x = x << j;
				y = y << i;

				assert(x == y);
			}

			{
				tt x = i;
				tt y = j;
				tt z = i;

				x *= y;
				y *= z;     

				assert(x == y);
			}

			{
				tt x = i * j * i;
				tt y = j * i * i;

				assert(x == y);
			}

			{
				tt x = i;
				tt y = i;
				tt z = j;
				tt w = i;

				if (z) {
					x /= z;
					y %= z;
					assert(x * z + y == w);
				}
			}

			{
				tt x = i;
				tt y = j;

				assert(!y || ((x / y)*y + x % y) == x);
			}

		}
	}


	{
		unsigned x(123);
		unsigned y(234);
		unsigned z(5);

		cout << hex << setw(8) << setfill('0') << x << '\n';
		x *= y;
		cout << hex << setw(8) << setfill('0') << x << '\n';
		x *= z;
		cout << hex << setw(8) << setfill('0') << x << '\n';
		x /= y;
		cout << hex << setw(8) << setfill('0') << x << '\n';
		x *= y;
		cout << hex << setw(8) << setfill('0') << x << '\n';
		x <<= z;
		cout << hex << setw(8) << setfill('0') << x << '\n';
		x |= y;
		cout << hex << setw(8) << setfill('0') << x << '\n';
		x >>= z;
		cout << hex << setw(8) << setfill('0') << x << '\n';
		x *= z;
		cout << hex << setw(8) << setfill('0') << x << '\n';
		x -= y;
		cout << hex << setw(8) << setfill('0') << x << '\n';
		x += z;
		cout << hex << setw(8) << setfill('0') << x << '\n';
		x %= y;
		cout << hex << setw(8) << setfill('0') << x << '\n';
	}

	{
		tt x(123);
		tt y(234);
		tt z(5);

		cout << x << '\n';
		x *= y;
		cout << x << '\n';
		x *= z;
		cout << x << '\n';
		x /= y;
		cout << x << '\n';
		x *= y;
		cout << x << '\n';
		x <<= z;
		cout << x << '\n';
		x |= y;
		cout << x << '\n';
		x >>= z;
		cout << x << '\n';
		x *= z;
		cout << x << '\n';
		x -= y;
		cout << x << '\n';
		x += z;
		cout << x << '\n';
		x %= y;
		cout << x << '\n';

	}
	{
		tt x(255);
		tt y(x);
		tt z(8);
		
		x <<= z;
		x += y;

		tt a(17);
		tt b(31);
		tt c(36);

		cout << (x << a) << (x << b) << (x << c) << '\n';
	}
	{
		tt x(123);
		tt y(x*x*x);
		tt z(y*y*y);
		tt w(z*z*z);
		cout << x << '\n';
		cout << y << '\n';
		cout << z << '\n';
		cout << w << '\n';
	}
  #endif 
	{
		tt x(tt(123456789) << tt(100));
		cout << x << x / tt(987654321) << '\n';
	}

	return 0;
}

