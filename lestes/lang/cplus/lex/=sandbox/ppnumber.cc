/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/common.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

/*
   integral/float
   radix
   u,l,f
*/

/*!
  Attempts to parse/interpret preprocessing number.
  \return The filled literal info or NULL. 
*/
ptr<literal_info> classify_pp_number(const ucn_string &str)
{
   enum {
      BEGIN,
      ZERO,
      OCTAL,
      DECIMAL,
      HEXADECIMAL_START,
      HEXADECIMAL,
      FLOAT_FRACTION,
      FLOAT_EXPONENT_SIGN,
      FLOAT_EXPONENT_START,
      FLOAT_EXPONENT,
      FLOAT_SUFFIX,
      INTEGRAL_SUFFIX,
   } fstate = BEGIN;

   ucn u;
   ulint radix = 8;
   bool long_suffix = false;
   bool unsigned_suffix = false;
   bool float_suffix = false;

   for (ucn_string::iterator sit = str.begin(), send = str.end();
         sit != send; ) {
      u = *sit;
      if (!character::is_basic(u)) {
         // TODO pt bad character in number
         return NULL;
      }
      switch (fstate) {
         case BEGIN:
            ++sit;
            if (u == character::ascii_digit_0)
               fstate = ZERO;
            else if (u == character::ascii_dot)
               fstate = FLOAT_FRACTION;
            else if (character::is_digit(u)) {
               fstate = DECIMAL;
               base = 10;
            } else {
               // TODO pt bad character at start of number
               return NULL;
            }
            break;
         case ZERO:
            if (u == character::ascii_lower_x ||
                u == character::ascii_upper_x) {
               fstate = HEXADECIMAL_START;
               ++sit;
            } else if (character::is_odigit(u)) {
               fstate = OCTAL;
            } else 
               fstate = INTEGRAL_SUFFIX;
            break;
         case OCTAL:
            if (character::is_odigit(u))
               ++sit;
            else
               fstate = INTEGRAL_SUFFIX;
            break;
         case DECIMAL:
            if (character::is_digit(u))
               ++sit;
            else if (u == character::ascii_dot) {
               ++sit;
               fstate = FLOAT_FRACTION;
            } else if (u == character::ascii_lower_e ||
                  u == character::ascii_upper_e) {
               ++sit;
               fstate = FLOAT_EXPONENT_START;
            } else
               fstate = INTEGRAL_SUFFIX;
            break;
         case HEXADECIMAL_START:
            if (character::is_xdigit(u)) {
               ++sit;
               fstate = HEXADECIMAL;
               base = 16;
            } else
               fstate = INTEGRAL_SUFFIX;
            break;
         case HEXADECIMAL:
            if (character::is_xdigit(u))
               ++sit;
            else
               fstate = INTEGRAL_SUFFIX;
            break;
         case FLOAT_FRACTION:
            if (character::is_digit(u))
               ++sit;
            else if (u == character::ascii_lower_e ||
                  u == character::ascii_upper_e) {
               ++sit;
               fstate = FLOAT_EXPONENT_SIGN;
            } else
               fstate = FLOAT_SUFFIX;
            break;
         case FLOAT_EXPONENT_SIGN:
            if (character::is_digit(u)) {
               ++sit;
               fstate = FLOAT_EXPONENT;
            } else if (u == character::ascii_plus ||
                  u == character::ascii_minus) {
               ++sit;
               fstate = FLOAT_EXPONENT_START;
            } else {
               // TODO pt report error: no digits in exponent
               return NULL;
            }
            break;
         case FLOAT_EXPONENT_START:
            if (character::is_digit(u)) {
               ++sit;
               fstate = FLOAT_SUFFIX;
            } else {
               // TODO pt report error: no digits in exponent
               return NULL;
            }
            break;
         case FLOAT_EXPONENT:
            if (character::is_digit(u))
               ++sit;
            else {
               fstate = FLOAT_SUFFIX;
            }
            break;
         case FLOAT_SUFFIX:
            ++sit;
            if (u == character::ascii_lower_f ||
                u == character::ascii_upper_f) {
               if (float_suffix) {
                  // TODO pt report error: bad float suffix
                  return NULL;
               }
               float_suffix = true;
            } else if (u == character::ascii_lower_l ||
                  u == character::ascii_upper_l) {
               if (long_suffix) {
                  // TODO pt report error: bad float suffix
                  return NULL;
               }
               long_suffix = true;
            } else {
               // TODO pt report error: bad float suffix
               return NULL;
            }
            break;
         case INTEGRAL_SUFFIX:
            ++sit;
            if (u == character::ascii_lower_u ||
                u == character::ascii_upper_u) {
               if (unsigned_suffix) {
                  // TODO pt report error: bad integral suffix
                  return NULL;
               }
               unsigned_suffix = true;
            } else if (u == character::ascii_lower_l ||
                  u == character::ascii_upper_l) {
               if (long_suffix) {
                  // TODO pt report error: bad integral suffix
                  return NULL;
               }
               long_suffix = true;
            } else {
               // TODO pt report error: bad integral suffix
               return NULL;
            }
            break;
         default:
            lassert(false);
            break;
      }
   }

      ZERO,
      OCTAL,
      DECIMAL,
      HEXADECIMAL_START,
      HEXADECIMAL,
      FLOAT_FRACTION,
      FLOAT_EXPONENT_SIGN,
      FLOAT_EXPONENT_START,
      FLOAT_EXPONENT,
      FLOAT_SUFFIX,
      INTEGRAL_SUFFIX,
      
   switch (fstate) {
      case FLOAT_EXPONENT_SIGN:
      case FLOAT_EXPONENT_START:
         // TODO pt report error: float exponent with no digits
         return NULL;
      case HEXADECIMAL_START:
         // TODO pt report error: hexa with no digits
         return NULL;
   }
}

cpp_number interpret_pp_number(const ucn_string &str)
{
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

int main(void) {
   return 0;
}
