/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__std___ptr_hh___included
#define lestes__std___ptr_hh___included

/*! \file
  Declaration of ptr class representing local, global and return pointer.
  \author pt
*/
package(lestes);
package(std);

// forward declaration to avoid cycle
template <typename T>
class ptr;

end_package(std);
end_package(lestes);

#include <lestes/common.hh>
#include <lestes/std/mem/root_pointer.hh>
#include <lestes/std/srp.hh>

package(lestes);
package(std);

/*!
  Represents structure pointer.
  \param T  The type of object pointed to.
*/
template <typename T>
class ptr : public root_pointer {
public:
	//! declares friendship to all other ptr
	template <typename U>
	friend class ptr<U>;
	//! declares friendship to all srp
	template <typename U>
	friend class srp<U>;
	//! creates ptr, initializes with NULL pointer
	inline ptr(void);
	//! creates ptr, initializes with pointer 
	template <typename U>
	inline ptr(U *a_pointer);
	//! creates ptr, initializes with ptr
	template <typename U>
	inline ptr(const ptr<U> &other);
	//! creates ptr, initializes with srp
	template <typename U>
	inline ptr(const srp<U> &other);
	//! destroys ptr
	inline ~ptr(void);
	//! returns the pointer
	inline T *operator->(void) const;
	//! returns dereferenced pointer
	inline T &operator *(void) const;
	//! assigns a pointer
	template <typename U>
	inline ptr<T> &operator=(U *a_pointer);
	//! assigns ptr
	template <typename U>
	inline ptr<T> &operator=(const ptr<U> &other);
	//! assigns srp
	template <typename U>
	inline ptr<T> &operator=(const srp<U> &other);
	//! tests NULL pointer
	inline operator bool(void) const;
	//! tests non NULL pointer
	inline bool operator!(void) const;
	//! compares to pointer
	template <typename U>
	inline bool operator==(U *a_pointer) const;
	//! compares to ptr
	template <typename U>
	inline bool operator==(const ptr<U> &other) const;
	//! compares to srp
	template <typename U>
	inline bool operator==(const srp<U> &other) const;
	//! compares to pointer
	template <typename U>
	inline bool operator!=(U *a_pointer) const;
	//! compares to ptr
	template <typename U>
	inline bool operator!=(const ptr<U> &other) const;
	//! compares to srp
	template <typename U>
	inline bool operator!=(const srp<U> &other) const;
	//! casts to other srp type
	template <typename U>
	inline operator srp<U>(void) const;
	//! performs dynamic cast to other type
	template <typename U>
	inline srp<U> dncast(void);
protected:
	//! returns the pointer
	inline T *pointer_get(void) const;
	//! sets the pointer
	template <typename U>
	inline void pointer_set(U *a_pointer);
};

/*!
  Creates ptr, initializes with pointer.
  \post pointer_get() == a_pointer
  \param U  The type of the initializer pointer.
  \param a_pointer  The initialization value.
*/
template <typename T, typename U>
inline ptr<T>::ptr(U *a_pointer):
	simple_pointer(static_cast<T *>(a_pointer))
{
}

/*!
  Creates ptr, initializes with NULL pointer.
  \post pointer_get() == NULL
*/
template <typename T>
inline ptr<T>::ptr(void):
	simple_pointer(NULL)
{
}

/*!
  Creates ptr, initializes with ptr.
  \post pointer_get() == other.pointer_get()
  \param U  The type of the initializer ptr.
  \param other  The ptr to initialize with.
*/
template <typename T, typename U>
inline ptr<T>::ptr(const ptr<U> &other):
	simple_pointer(static_cast<T *>(other.pointer_get()))
{
}

/*!
  Creates ptr, initializes with srp.
  \post pointer_get() == other.pointer_get()
  \param U  The type of the initializer srp.
  \param other  The srp to initialize with.
*/
template <typename T, typename U>
inline ptr<T>::ptr(const srp<U> &other):
	simple_pointer(static_cast<T *>(other.pointer_get()))
{
}

/*!
  Destroys ptr.
*/
template <typename T>
inline ptr<T>::~ptr(void)
{
}

/*!
  Returns the pointer.
  \return  The contained pointer.
*/
template <typename T>
inline T *ptr<T>::operator->(void) const
{
	return pointer_get();
}

/*!
  Dereferences the pointer.
  \return  The dereferenced pointer.
*/
template <typename T>
inline T &ptr<T>::operator *(void) const
{
	return *pointer_get();
}

/*!
  Assigns new pointer.
  \param U  The type of the assigned pointer.
  \param a_pointer  The pointer to assign.
  \return  This ptr after the assignment.
*/
template <typename T, typename U>
inline ptr<T> &ptr<T>::operator=(U *a_pointer)
{
	pointer_set(a_pointer);
	return *this;
}

/*!
  Assigns new ptr.
  \param U  The type of the assigned ptr.
  \param other  The ptr to assign.
  \return  This ptr after assignment.
*/
template <typename T, typename U>
inline ptr<T> &ptr<T>::operator=(const ptr<U> &other)
{
	pointer_set(other.pointer_get());
	return *this;
}

/*!
  Assigns new srp.
  \param U  The type of the assigned srp.
  \param other  The other srp to assign.
  \return  This ptr after assignment.
*/
template <typename T, typename U>
inline ptr<T> &ptr<T>::operator=(const srp<U> &other)
{
	pointer_set(other.pointer_get());
	return *this;
}

/*!
  Tests NULL pointer.
  \return  true if the pointer is NULL.
*/  
template <typename T>
inline ptr<T>::operator bool(void) const
{
	return pointer_get();
}

/*!
  Tests non NULL pointer.
  \return  true if the pointer is not NULL.
*/
template <typename T>
inline bool ptr<T>::operator!(void) const
{
	return !pointer_get();
}

/*!
  Compares to pointer.
  \param U  The type of the compared pointer.
  \return  true if both pointers are equal.
*/
template <typename T, typename U>
inline bool operator==(U *a_pointer) const
{
	return pointer_get() == a_pointer;
}

/*!
  Compares to ptr.
  \param U  The type of the compared pointer.
  \return  true if both pointers are equal.
*/
template <typename T, typename U>
inline bool operator==(const ptr<U> &other) const
{
	return pointer_get() == other.pointer_get();
}

/*!
  Compares to other srp.
  \param U  The type of the compared srp.
  \return  true if both pointers are equal.
*/
template <typename T, typename U>
inline bool operator==(const srp<U> &other) const
{
	return pointer_get() == other.pointer_get();
}

/*!
  Casts pointer to other type.
  \param U  The type of the target srp.
  \return  New srp initialized with this ptr pointer.
*/ 
template <typename T, typename U>
operator ptr<T>::srp<U>(void) const
{
	return srp<U>(static_cast<U *>pointer_get());
}

/*!
  Performs dynamic cast on pointers.
  \param U  The type of the target srp.
  \return  New srp initialized with this ptr pointer.
*/
template <typename T, typename U>
inline srp<U> ptr<T>::dncast(void)
{
	return srp<U>(dynamic_cast<U *>(pointer_get()));
}

/*!
  Returns the pointer.
  \return  The contained pointer.
*/
template <typename T>
inline T *pointer_get(void) const
{
	return static_cast<T *>(simple_pointer::pointer_get());
}

/*!
  Sets the pointer.
  \param U  The type of the new pointer.
  \param a_pointer  The new pointer to set.
*/
template <typename T, typename U>
inline void pointer_set(U *a_pointer)
{
	// TODO vigg60 static_cast to keystone ???
	simple_pointer::pointer_set(static_cast<T *>(a_pointer));
}

end_package(std);
end_package(lestes);

#endif
/* vim: set ft=lestes : */

