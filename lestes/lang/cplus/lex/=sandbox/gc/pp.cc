/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <iostream>
#include <list>

using namespace ::std;

class object {
public:
   ~object(void) {}
protected:
   object(void) {}
private:
   object(const object &);
   object &operator=(const object &);
};

class pointer {
public:
   ~pointer(void) {}
   // if protected, does not work!!!
   inline object *bare_get(void) const {
      return bare;
   }
   inline void bare_set(object *a_bare) {
      bare = a_bare;
   }
protected:
   pointer(object *a_bare): bare(a_bare) {}
private:
   object *bare;
   pointer(const pointer &);
   pointer &operator=(const pointer &);
};

template <typename T>
class ptr: public pointer {
public:
   inline ptr(T * a_bare): pointer(a_bare) {}
   inline ptr(void): pointer(NULL) {}
   inline ptr(const ptr<T> &other): pointer(other.bare_get()) {}
   template <typename U>
   inline ptr(const ptr<U> &other):
      pointer(static_cast<T *>(static_cast<U *>(other.bare_get()))) {}
   inline ~ptr(void) {}
   template <typename U>
   friend inline ptr<U> dncast(const ptr<T> &p) {
      return dynamic_cast<U *>(static_cast<T *>((p.bare_get())));
   }
   inline T *operator->(void) const {
      return static_cast<T *>(bare_get());
   }
   inline ptr<T> operator=(const ptr<T> &other) {
      bare_set(other.bare_get());
      return *this;
   }
private:
/*   inline T *typed_get(void) const {
      return static_cast<T *>(bare_get());
   }
*/
//   inline void bare_set(T * a_bare)
};

template <typename T>
class srp {
public:
   inline srp(T * a_bare): bare(a_bare) {}
   inline srp(void): pointer(NULL) {}
   inline srp(const srp<T> &other): pointer(other.bare_get()) {}
   template <typename U>
   inline srp(const srp<U> &other):
      pointer(static_cast<T *>(static_cast<U *>(other.bare_get()))) {}
   inline ~srp(void) {}
   template <typename U>
   friend inline srp<U> dncast(const srp<T> &p) {
      return dynamic_cast<U *>(static_cast<T *>((p.bare_get())));
   }
   inline T *operator->(void) const {
      return static_cast<T *>(bare_get());
   }
   inline srp<T> operator=(const srp<T> &other) {
      bare_set(other.bare_get());
      return *this;
   }
private:
   object *bare;
};

class a: public ::lestes::std::object {
public:
   virtual ~a(void) {}
   virtual void m(void) {
      cout << "a::m()\n";
   }
};

class b : public a {
public:
   virtual void m(void) {
      cout << "b::m()\n";
   }
};

class c : public a {
public:
   virtual void m(void) {
      cout << "c::m()\n";
   }
};

class d: public ::lestes::std::object {
public:
   virtual ~d(void) {}
   virtual void m(void) {
      cout << "d::m()\n";
   }
};

template <typename T>
class lyst: public list< ptr<T> >, public ::lestes::std::object {
public:
   static ptr< lyst<T> > create(void) {
      return ptr< lyst<T> >(new lyst());
   }
protected:
   lyst(void) {}
private:
};

int main(void)
{
   ptr < lyst<a> > la = lyst<a>::create();

   la->push_back(ptr<a>(new a()));
   
   cout << sizeof(object) << ' ' << sizeof(pointer) << ' ' << sizeof(ptr<a>) << endl;
   
   ptr<a> x(new a());
   ptr<b> y(new b());
   ptr<c> z(new c());

   ptr<a> xx(x);
   xx = x;

   ptr<a> yy(y);
   yy = y;

   ptr<a> w(new b());
   ptr<b> ww(w);
   ww = w;

   ptr<d> h(new d());

   x->m();
   y->m();
   z->m();
   w->m();
   x = dncast<a>(h);
   x->m();
   return 0;
}
