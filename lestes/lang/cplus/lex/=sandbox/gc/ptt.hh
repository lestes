/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include "gc.hh"
#include "pointer.hh"

// forward declarations

class gc;

template <typename T>
class srp;

// TODO pt remove implementations from declaration

/*!
  Represents global, stack and return pointer.
  \param T  The type of pointee.
*/
template <typename T>
class ptr : public pointer {
public:
	//! creates ptr from bare pointer
	ptr<T>(T * p) : pointer(), pointee(p) {}
	//! creates ptr from srp
	ptr<T>(const srp<T> & p) : pointer(), pointee(p.get()) {}
	ptr<T>(const ptr<T> & p) : pointer(), pointee(p.get()) {}
	explicit ptr<T>() : pointer(), pointee(NULL) {}
#ifndef CLAUSE_8_5_0_14_OK
	// constructors for compilers, which are not 8.5/14 compliant
	//! creates ptr from ptr to another type
	template<typename Y>
	ptr<T>(const ptr<Y> &p) : pointee(p.get()) {}
	//! creates ptr from srp to another type
	template<typename Y>
	ptr<T>(const srp<Y> &p) : pointee(p.get()) {}
#endif
	//! destructs the pointer
	~ptr<T>(void) {}


	/// pointerlike
	
	T * operator ->() const { return pointee; }
	T & operator *() const { return *pointee; }

	/// testing
	operator bool() const { return pointee; }
	bool operator !() const { return !pointee; }

	/// comparators
//	template <typename Y>bool operator == (Y * a) const
//	{ return a == pointee; }
//??	
template <typename Y>bool operator == (srp<Y> & a) const
	{ return a.get() == pointee; }
	template <typename Y>bool operator == (ptr<Y> & a) const
	{ return a.get() == pointee; }

	template <typename Y>bool operator != (Y a)
	{ return ! operator ==(a); }

	/// casting to other type
	template<typename Y>operator srp<Y>() const
	{ return srp<Y>(get()); }

	//necessary
	template<typename Y>srp<Y> dncast() const
	{ return srp<Y>(dynamic_cast<Y*>(get())); }

	/// assignment
	//template<typename Y>srp<T> operator=(Y * a)
	//{ pointee = a; return srp<T>(*this); }
	template<typename Y>srp<T> operator=(const srp<Y>&a)
	{ pointee = a.get(); return srp<T>(*this); }
	template<typename Y>srp<T> operator=(const ptr<Y>&a)
	{ pointee = a.get(); return srp<T>(*this); }
private:
	//! returns the pointee cast to the proper type
	inline T * pointee_get() const {
		return static_cast<T *>(pointee);
	}
};

/* vim: set ft=lestes : */
