/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__std___object_hh___included
#define lestes__std___object_hh___included

/*! \file
  Declaration of object class representing general object.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/std/mem/keystone.hh>

package(lestes);
package(std);

/*!
  Common ancestor of all classes, adding basic functionality.
*/
class object: public keystone {
public:
	//! finalizes the object
	inline ~object(void);
protected:
	//! creates the object
	inline object(void);
private:
	//! hides copy constructor
	object(const object &);
	//! hides assignment operator
	object &operator=(const object &);
};

/*!
  Creates the object.
  Place for debugging and statistics code.
*/
inline object::object(void) 
{
}

/*!
  Finalizes the object.
  Place for debugging and statistics code.
*/
inline object::~object(void)
{
}

end_package(std);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
