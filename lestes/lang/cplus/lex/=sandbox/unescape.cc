/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/common.hh>
#include <lestes/std/ucn_string.hh>
#include <lestes/std/vector.hh>
#include <lestes/lang/cplus/lex/ucn_token.hh>
#include <lestes/lang/cplus/lex/location.hh>

#include <iostream>

package(lestes);
package(lang);
package(cplus);
package(lex);

using namespace ::std;

#if 0
ucn_string interpret_escape(const ucn_string &s)
{
	ucn u;
	for (ulint i = 0, n = s.size(); i < n; i++) {
		u = s[i];
		if (u == '\\') {
		}
	}
	
}
#endif

typedef vector< srp<ucn_token> > ucn_token_vector;

// can be used for strings and identifiers
// leaves illformed sequences in strings intact
//ucn_string unescape_string(const ptr< ucn_token_vector > &v)

/*!
  Converts ucn sequence representing identifier to ucn string, tranlates ucn escape sequences to ucn.
  Recognized ucn escape sequences are in the form \uxxxx or \Uxxxxxxxx, x represents hexadecimal digit.
  \todo pt Change interface to signal error (return token_value or pp_token, unify with ucn_token_buffer->extract).
  \pre All contained ucn sequences are well-formed, but the value might be out of valid range.
  \param v  Vector containing sequence of ucn tokens representing the identifier.
  \return  The ucn string containing the unescaped identifier.
*/
ucn_string unescape_ident(const ptr< ucn_token_vector > &v)
{
	enum {
		BEGIN,
		BACK,
		UCN
	} fstate = BEGIN;
	ulint count;
	ptr<ucn_token> ut;
	ucn_token_type utt;
	ucn u, curr;
	octet o;
	ucn_token_vector::iterator it = v->begin();
	ucn_token_vector::iterator end = v->end();

	ucn_string result(v->size(),0xbeef);
	
	ulint i;
	for (i = 0; it != end; ++it) {
		ut = *it;
		utt = ut->type_get();
		u = ut->value_get();
		//cout << "fs = " << fstate << " i = " << i << endl;
		//cout << "got utt = " << dec <<(uint) utt << " u = " << hex << setw(8) << setfill('0') << u << endl;
		
		switch (fstate) {
			case BEGIN:
				if (utt == ucn_token::normal_char && u == '\\') {
					//cout << "back\n";
					fstate = BACK;
				} else {
					result[i++] = u;
				}
				break;
			case BACK:
				lassert(utt == ucn_token::normal_char);
				if (u == 'u') {
					//cout << "\\u\n";
					count = 4;
				} else if (u == 'U') {
					//cout << "\\U\n";
					count = 8;
				} else {
					// ICE: expecting well formed escape sequence
					lassert(u == 'u' || u == 'U');
				}
				fstate = UCN;
				curr = 0;
				break;
			case UCN:
				lassert(utt == ucn_token::normal_char && u <= octet_type::MAX_VALUE);
				o = tolower(static_cast<octet>(u));
				lassert(isxdigit(o));
				//cout << " yet " << count << endl;
				// TODO pt not portable
				curr = (curr << 4) | (isdigit(o) ? o - '0' : o - 'a' + 10);
				if (--count == 0) {
					// TODO pt check valid ranges
					result[i++] = curr;
					fstate = BEGIN;
				}
				break;
		}
	}
	lassert(fstate == BEGIN);

	//cout << "final i = " << i << endl;

	return ucn_string(result.begin(),result.begin()+i);
}

/*!
  Converts ucn sequence representing C++ (wide) string or (wide) character.
  Translates escape sequences and ucn escape sequences to ucn.
  Recognized ucn escape sequences are in the form \uhhhh or \Uhhhhhhhh, h represents hexadecimal digit.
  Recognized escape sequences are \' \" \? \\ \a \b \f \n \r \t \v \ooo (octal) \xhhh (hexadecimal).
  Octal and hexadecimal sequences shall be taken literally as values from the execution character
  set. The ucn is, however unicode, so a special ucn bit pattern with msb set to 1 is used to
  pass these execution character set values to the translation phase. All 1 bits designate
  ucn EOF, which is not taken as literal value.
  When a sequence \y is encountered, where y is not in basic source character set,
  the result (according to interpretation of ISO standard) should be \\Uhhhhhhhh where hhhhhhhh
  is the hexadecimal representation of the y character. However, this behaviour would be misleading
  and thus is not supported.
  \todo pt Change interface to signal error (return token_value or pp_token, unify with ucn_token_buffer->extract).
  \pre All contained escape sequences are well-formed, but the value of the ucn escape sequences might be invalid.
  \param v  Vector containing ucn tokens representing the literal content.
  \return  The ucn string containing the unescaped literal content.
*/
ucn_string unescape_literal(const ptr< ucn_token_vector > &v)
{
	enum {
		BEGIN,
		PASS,
		BACK,
		UCN,
		HEX,
		OCT
	} fstate = BEGIN;
	ulint count;
	ptr<ucn_token> ut;
	ucn_token_type utt;
	ucn u, curr;
	octet o;
	ucn_token_vector::iterator it = v->begin();
	ucn_token_vector::iterator end = v->end();

	ucn_string result(v->size(),0xbeef);
	
	ulint i;
	for (i = 0; fstate == PASS || it != end;) {
		if (fstate == PASS) {
			fstate = BEGIN;
		} else {
			ut = *it;
			utt = ut->type_get();
			u = ut->value_get();
			++it;
		}
		cout << "fs = " << fstate << " i = " << i << endl;
		cout << "got utt = " << dec <<(uint) utt << " u = " << hex << setw(8) << setfill('0') << u << endl;
		
		switch (fstate) {
			case BEGIN:
				if (utt == ucn_token::normal_char && u == '\\') {
					//cout << "back\n";
					fstate = BACK;
				} else {
					result[i++] = u;
				}
				break;
			case BACK:
				lassert(utt == ucn_token::normal_char);
				
				o = static_cast<octet>(u);
				switch (o) {
					case 'u':
						count = 4;
						fstate = UCN;
						break;
					case 'U':
						count = 8;
						fstate = UCN;
						break;
					case '\'':
					case '"':
					case '?':
					case '\\':
						result[i++] = o;
						fstate = BEGIN;
						break;
					// TODO pt make translation table ???
					case 'a':
						result[i++] = '\a';
						fstate = BEGIN;
						break;
					case 'b':
						result[i++] = '\b';
						fstate = BEGIN;
						break;
					case 'f':
						result[i++] = '\f';
						fstate = BEGIN;
						break;
					case 'n':
						result[i++] = '\n';
						fstate = BEGIN;
						break;
					case 'r':
						result[i++] = '\r';
						fstate = BEGIN;
						break;
					case 't':
						result[i++] = '\t';
						fstate = BEGIN;
						break;
					case 'v':
						result[i++] = '\v';
						fstate = BEGIN;
						break;
					case 'x':
						fstate = HEX;
						curr = 0;
						count = 0;
						break;
					case '0':
					case '1':
					case '2':
					case '3':
					case '4':
					case '5':
					case '6':
					case '7':
						// TODO pt portably
						curr = o - '0';
						fstate = OCT;
						count = 2;
						break;
					default:
						// ICE: expecting well formed escape sequence
						lassert(false);
						break;
						
				}
				break;
			case HEX:
				if (utt == ucn_token::normal_char && u <= octet_type::MAX_VALUE &&
					 (o = tolower(static_cast<octet>(u)),true) && isxdigit(o)) {
					curr = (curr << 4) | (isdigit(o) ? o - '0' : o - 'a' + 10);
					count = 1;
				} else {
					lassert(count != 0);
					// TODO pt check range
					// TODO pt pass literally as execution charset value
					result[i++] = curr;
					fstate = PASS;
				}
				break;
			case OCT:
				if (utt == ucn_token::normal_char && u <= octet_type::MAX_VALUE &&
					 (o = tolower(static_cast<octet>(u)),true) && isdigit(o) && o != '8' && o != '9') {
					curr = (curr << 3) | (o - '0');
					if (--count == 0) {
						// TODO pt check ranges
						// TODO pt pass literally as execution charset value
						result[i++] = curr;
						fstate = BEGIN;
					}
				} else {
					// TODO pt check ranges
					// TODO pt pass literally as execution charset value
					result[i++] = curr;
					fstate = PASS;
				}
				break;
			case UCN:
				lassert(utt == ucn_token::normal_char && u <= octet_type::MAX_VALUE);
				o = tolower(static_cast<octet>(u));
				lassert(isxdigit(o));
				//cout << " yet " << count << endl;
				// TODO pt not portable
				curr = (curr << 4) | (isdigit(o) ? o - '0' : o - 'a' + 10);
				if (--count == 0) {
					// TODO pt check ranges
					result[i++] = curr;
					fstate = BEGIN;
				}
				break;
			case PASS:
				// ICE: you should never get here
				lassert(fstate != PASS);
				break;
		}
	}

	if (fstate == HEX) {
		lassert(count != 0);
		// TODO pt check range
		// TODO pt pass literally as execution charset value
		result[i++] = curr;
	} else lassert(fstate == BEGIN);

	//cout << "final i = " << i << endl;

	return ucn_string(result.begin(),result.begin()+i);
}

void escape_test(void)
{
	ptr<file_info> fi = file_info::create("xyz",NULL);
	ptr<location> loc = location::create(fi,0);
	ptr<ucn_token_vector> v = ucn_token_vector::create();
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'a'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'b'));
	v->push_back(ucn_token::create(loc,ucn_token::translated,0x1234));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'c'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'\\'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'u'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'b'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'A'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'d'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'7'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'d'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'\\'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'U'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'D'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'e'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'a'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'d'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'B'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'a'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'b'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'e'));
	
	ucn_string s = unescape_ident(v);
	cout << s.size() << endl;
	cout << s << endl;

	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'\\'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'\''));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'\\'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'"'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'\\'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'?'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'\\'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'\\'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'\\'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'a'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'\\'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'b'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'\\'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'f'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'\\'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'n'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'\\'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'r'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'\\'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'t'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'\\'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'v'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'\\'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'0'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'0'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'7'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'\\'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'x'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'2'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'a'));
	v->push_back(ucn_token::create(loc,ucn_token::normal_char,'i'));

	s = unescape_literal(v);
	cout << s.size() << endl;
	cout << s << endl;
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

int main(void)
{
	::lestes::lang::cplus::lex::escape_test();
	return 0;

}
/* vim: set ft=lestes : */
