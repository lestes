%{
/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <stdio.h>
#define YYSTYPE int
int yylex(void);
void yyerror(char const *);
%}

%token NUM
%token TOK_LT_LT
%token TOK_LT_EQ
%token TOK_GT_GT
%token TOK_GT_EQ
%token TOK_EMARK_EQ
%token TOK_EQ_EQ
%token TOK_VBAR_VBAR
%token TOK_AMP_AMP
%nonassoc '+' '-'

%%

input: /* empty */
     | input line
;

line: '\n'
    | expr '\n' { printf("%d\n",$1); }
    | error '\n' { yyerrok; }
;

expr: logor { $$ = $1; }
    | logor '?' expr ':' expr { if ($1) $$ = $3; else $$ = $5; }
    | logor '?' expr ':' error { printf("after :\n"); yyerrok; }
    | logor '?' error { printf("after ?\n"); yyerrok; }
;

logor: logand { $$ = $1; }
     | logor TOK_VBAR_VBAR logand { $$ = $1 || $3; }
;

logand: bitor { $$ = $1; }
      | logand TOK_AMP_AMP bitor { $$ = $1 && $3; }
;

bitor: bitxor { $$ = $1; }
     | bitor '|' bitxor { $$ = $1 | $3; }
;

bitxor: bitand { $$ = $1; }
      | bitxor '^' bitand { $$ = $1 ^ $3; }
;

bitand: equal { $$ = $1; }
      | bitand '&' equal { $$ = $1 & $3; }
;

equal: rel { $$ = $1; }
     | equal TOK_EMARK_EQ rel { $$ = $1 != $3; }
     | equal TOK_EQ_EQ rel { $$ = $1 == $3; }
;

rel: shift { $$ = $1; }
   | rel '<' shift { $$ = $1 < $3; }
   | rel '>' shift { $$ = $1 > $3; }
   | rel TOK_LT_EQ shift { $$ = $1 <= $3; }
   | rel TOK_GT_EQ shift { $$ = $1 >= $3; }
;

shift: add { $$ = $1; }
     | shift TOK_LT_LT add { $$ = $1 << $3; }
     | shift TOK_GT_GT add { $$ = $1 >> $3; }
;

add: mul { $$ = $1; }
   | add '+' mul { $$ = $1 + $3; }
   | add '-' mul { $$ = $1 - $3; }
;

mul: unary { $$ = $1; }
   | mul '*' unary { $$ = $1 * $3; }
   | mul '/' unary { $$ = $1 / $3; }
   | mul '%' unary { $$ = $1 % $3; }
;

unary: prim { $$ = $1; }
     | '-' unary { $$ = -$2; }
     | '+' unary { $$ = $2; }
     | '~' unary { $$ = ~$2; }
     | '!' unary { $$ = !$2; }
;

prim: literal { $$ = $1; }
    | '(' expr ')' { $$ = $2; }
;

literal: NUM { $$ = $1; }
;

%%

#include <ctype.h>

int
yylex (void)
{
 int c;

 /* Skip white space.  */
 while ((c = getchar ()) == ' ' || c == '\t')
   ;
 /* Process numbers.  */
 if (isdigit (c))
   {
     ungetc (c, stdin);
     scanf ("%d", &yylval);
     return NUM;
   }
 /* Return end-of-input.  */
 if (c == EOF)
   return 0;

 if (c == '<' || c == '>' || c == '!' || c == '=' || c == '&' || c == '|') {
   int d = getchar();
   switch (c) {
      case '<':
         if (d == '<') return TOK_LT_LT;
         if (d == '=') return TOK_LT_EQ;
         break;
      case '>':
         if (d == '>') return TOK_GT_GT;
         if (d == '=') return TOK_GT_EQ;
         break;
      case '!':
         if (d == '=') return TOK_EMARK_EQ;
         break;
      case '=':
         if (d == '=') return TOK_EQ_EQ;
         break;
      case '|':
         if (d == '|') return TOK_VBAR_VBAR;
         break;
      case '&':
         if (d == '&') return TOK_AMP_AMP;
         break;
   }
   ungetc(d,stdin);
 }
   return c;
}

void yyerror(char const *s)
{
   fprintf(stderr,"%s\n",s);
}
 
int main(void)
{
   return yyparse();
}
