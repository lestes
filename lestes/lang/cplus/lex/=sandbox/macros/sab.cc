/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <iostream>

using namespace ::std;

class A {
public:
   static int create(void) { return 1; }
};

class B {
public:
   static char create(void) { return 'x'; }
};


class F {
public:
   typedef long (*create_fun)(void);
   template <typename T>
   static long adapt(void)
   {
      return static_cast<long>(T::create());
   }
   long insert(create_fun cf) {
      return cf();
   }
};

int main(void)
{
   F *f = new F();
   cout << f->insert(F::adapt<A>) << ' ' << f->insert(F::adapt<B>) << endl;;
   /*
   pf pfa = adapt<A>;
   pf pfb = adapt<B>;
   cout << pfa() << ' ' << pfb() << endl;
   */
   return 0;
}
