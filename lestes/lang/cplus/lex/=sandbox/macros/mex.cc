/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <iostream>
#include <iterator>
#include <list>
#include <set>
#include <vector>
#include <queue>
#include <deque>
#include <algorithm>
#include <cctype>
// TODO solve this portably
#include <ext/hash_map>
#include <cassert>

using namespace ::std;

// TODO solve this portably
using namespace ::__gnu_cxx;

typedef unsigned int ulint;
typedef int lint;

class macro;
class token;

string char_to_string(char c)
{
   string str(&c,1);
   return string(str);
}

/*!
  Represents set of disallowed macros. The object is constant and can be constructed from
  existing object by adding macro.
  \author pt
*/
class taboo_macros {
public:
   //! finalizer
   ~taboo_macros(void);
   //void add(macro *m);
   //void add_all(const taboo_macros &t);
   //! tests macro membership
   bool contains(macro *a_macro) const;
//   iterator_type begin(void);
//   iterator_type end(void);
   //! returns new set with added macro
   taboo_macros *extend(macro *a_macro) const;
   //! returns empty set
   static taboo_macros *create(void);
private:
   //! creates the object
   taboo_macros(void);
   //! creates copy of the object with added macro
   taboo_macros(taboo_macros *copy, macro *a_macro);
//   typedef macro *value_type;
//   typedef set<value_type> container_type;
   //! type of set of macros
   typedef set<macro *> taboo_type;
   //! type of auxiliary map for sharing newly created instances
   typedef map<macro *,taboo_macros *> shared_type;
   //typedef set_type::iterator iterator_type;
   //! set of taboo macro
   taboo_type taboo;
   //! internal map to remember objects created by extend
   mutable shared_type shared;
};

/*!
  Creates new empty set.
  \post taboo.length() == 0
  \post shared.length() == 0
*/
taboo_macros::taboo_macros(void):
   taboo(),
   shared()
{
}

/*!
  Creates copy of set, adding a new macro.
  \pre copy != NULL
  \post taboo.length() == copy->taboo.length() + (copy->contains(a_macro) ? 0 : 1)
  \post shared.length() == 0
  \param copy  The source object to copy.
  \param a_macro  The macro to insert into new object's set.
*/
taboo_macros::taboo_macros(taboo_macros *copy, macro *a_macro):
   taboo((assert(copy != NULL),copy->taboo)),
   shared()
{
   assert(a_macro != NULL);
   taboo.insert(a_macro);
}

/*!
  Finalizes object.
*/
taboo_macros::~taboo_macros(void)
{
}

/*!
  Returns new empty object.  
*/
taboo_macros *taboo_macros::create(void)
{
   return new taboo_macros();
}

/*!
  Returns new taboo with added macro.
  \pre a_macro != NULL
  \param a_macro  The macro to be added to the new taboo.
  \return  New object with set extended by a_macro.
*/
taboo_macros *taboo_macros::extend(macro *a_macro) const
{
   assert(a_macro != NULL);
   
   // macro is already in this instance
   if (contains(a_macro)) return this;
   
   // lookup the cache of extensions
   shared_type::iterator it = shared.find(a_macro);
   // return shared instance
   if (it != shared.end()) return (*it).second;
   
   // call special "copy and extend" constructor
   taboo_macros *nju = new taboo_macros(this,a_macro);
   
   // record into cache
   shared.add(make_pair(a_macro,nju));
         
   return nju;
}

#if 0
void taboo_macros::add_all(const taboo_macros &t)
{
   // linear because both are sorted
   container.insert(t.container.begin(),t.container.end());
}
#endif

/*!
  Tests whether a macro is contained in the set.
  \pre a_macro != NULL
  \param a_macro  The macro for membership test.
  \return true  If the macro is taboo.
*/  
bool taboo_macros::contains(macro *a_macro) const
{
   assert(a_macro != NULL);
   return taboo.find(m) != container.end();
}

#if 0
taboo_macros::iterator_type taboo_macros::begin(void)
{
   return container.begin();
}

taboo_macros::iterator_type taboo_macros::end(void)
{
   return container.end();
}

ostream& operator<<(ostream &o, const macro &m);

ostream &operator<<(ostream &o,taboo_macros &t)
{
   for (taboo_macros::iterator_type it = t.begin(); it != t.end(); ++it) {
      taboo_macros::value_type m = *it;
      o << *m << '\n';
   }
}
#endif

class token {
public:
   token(void);
   token(char a_value);
   ~token(void);
   char value_get(void) const;
   void value_set(char a_value);
   bool enabled(void) const;
   void disable(void);
   taboo_macros *taboo_get(void);
   void taboo_set(taboo_macros *a_taboo);
   token append(token &t);
private:
   char value;
   bool enable;
   taboo_macros *taboo;
};

token token::append(token &t)
{
   return token('X');
}

token::token(void):
   value(0),
   enable(true),
   taboo()
{
}

token::token(char a_value):
   value(a_value),
   enable(true),
   taboo()
{
}

token::~token(void)
{
}

char token::value_get(void) const
{
   return value;
}

void token::value_set(char a_value)
{
   value = a_value;
}

bool token::enabled(void) const
{
   return enable;
}

void token::disable(void)
{
   enable = false;
}

taboo_macros *token::taboo_get(void)
{
   return taboo;
}

void token::taboo_set(taboo_macros *a_taboo)
{
   taboo = a_taboo;
}

ostream& operator<<(ostream &o, const token &t) {
   return o << t.value_get();
}

class token_value_equals {
public:
   token_value_equals(const char a_value): value(a_value) {}
   bool operator()(const token &arg) { return value == arg.value_get(); }
private:
   char value;
};

// TODO make this a template
// TODO narrow the interface
/*!
  Represents sequence of tokens, with access at both ends.
  \author pt
*/
class token_sequence {
public:
   //! creates the object
   token_sequence(void);
   //! finalizes object
   ~token_sequence(void);
   //! returns front token
   token *peek_front(void);
   //! returns back token
   token *peek_back(void);
   //! reads front token
   token *read_front(void);
   //! read front token, skipping front whitespace, but not newline
   token *read_front_ws(void);
   //! read front token, skipping front whitespace and newlines
   token *read_front_wsnl(void);
   //! reads back token
   token *read_back(void);
   //! adds token to front
   void add_front(token *t);
   //! adds token to back
   void add_back(token *t);
   //! prepends another sequence
   void prepend(token_sequence *ts);
   //! appends another sequence
   void append(token_sequence *ts);
   //! expands first macro within sequence
   token_sequence *expand_first(void);
   //! completely expands all macros within sequence
   void expand_all(void);
   //! completely expands all macros to another object
   token_sequence *expand_all_clone(void) const;
   //! clones the object
   token_sequence *clone(void) const;
   //! returns length of sequence
   ulint length(void);
private:
   typedef list<token *> sequence_type;
   /*
   typedef sequence_type::value_type value_type;
   typedef sequence_type::iterator iterator_type;
   typedef back_insert_iterator<sequence_type> inserter_type;
   iterator_type begin();
   iterator_type end();
   void push(const value_type &item);
   void pop(void);
   void pop_back(void);
   const value_type &front(void) const;
   value_type &front(void);
   const value_type &back(void) const;
   value_type &back(void);
   bool empty(void) const;
   void prepend(token_sequence *ts);
   inserter_type inserter(void);
   value_type &skip_ws(void);
   value_type &skip_ws_nl(void);
   token_sequence *expand_clone(void);
   void expand(void);
   token_sequence *clone(void);
   void append(token_sequence *ts);
   */
   sequence_type sequence;
};

/*!
  Expands first macro within sequence in place.
  \return The tokens before the first macro expansion.
*/
token_sequence *token_sequence::expand_first(void)
{
   // TODO implement
   return NULL;
}

// TODO there is a question, whether to expand in place, or return new
// perhaps it is best to provide both possibilities

/*!
  Completely expands all macros within the sequence.
  \return New sequence containing the expansion.
*/
token_sequence *token_sequence::expand_all_clone(void) const
{
   // TODO implement
   return NULL;
}

/*!
  Completely expands all macros within the sequence in place.
*/
void token_sequence::expand_all(void) {
   // TODO implement
}

/*!
  Returns a copy of the sequence.
  \return The copy of the sequence. 
*/
token_sequence *token_sequence::clone(void) const
{
   // TODO implement
   // TODO deep or shallow?
   return NULL;
}

#if 0
token_sequence::value_type &token_sequence::skip_ws(void)
{
   do {
      token &t = sequence.front();
      if (t.value_get() != ' ') return t;
   } while (true);
}

token_sequence::value_type &token_sequence::skip_ws_nl(void)
{
   do {
      token &t = sequence.front();
      if (t.value_get() != ' ' && t.value_get() != '\n') return t;
   } while (true);
}
#endif

/*!
  Constructs new empty sequence.
*/
token_sequence::token_sequence(void):
   sequence()
{
}

/*!
  Finalizer.
*/
token_sequence::~token_sequence(void)
{
}

/*
token_sequence::iterator_type token_sequence::begin()
{
   return sequence.begin();
}

token_sequence::iterator_type token_sequence::end()
{
   return sequence.end();
}

void token_sequence::push(const value_type &item)
{
   sequence.push_back(item);
}

void token_sequence::pop(void)
{
   sequence.pop_front();
}

void token_sequence::pop_back(void)
{
   sequence.pop_back();
}

const token_sequence::value_type &token_sequence::front(void) const
{
   return sequence.front();
}

token_sequence::value_type &token_sequence::front(void)
{
   return sequence.front();
}

const token_sequence::value_type &token_sequence::back(void) const
{
   return sequence.back();
}

token_sequence::value_type &token_sequence::back(void)
{
   return sequence.back();
}

bool token_sequence::empty(void) const
{
   return sequence.empty();
}
*/

/*!
  Inserts another token sequence before the beginning of the sequence.
  \pre inserted != NULL
  \post inserted->length() == 0
  \param inserted  The sequence to insert, the content is moved into new place.
*/
void token_sequence::prepend(token_sequence *inserted)
{
   assert(inserted != NULL);
   sequence.splice(sequence.begin(),inserted->sequence);
}

/*!
  Inserts another token sequence after the end of the sequence.
  \pre inserted != NULL
  \post inserted->length() == 0
  \param inserted  The sequence to insert, the content is moved into new place.
*/
void token_sequence::append(token_sequence *inserted)
{
   assert(inserted != NULL);
   sequence.splice(sequence.end(),inserted->sequence);
}

/*!
  Returns length of the sequence.
  \return The length of the sequence.
*/
ulint token_sequence::length(void)
{
   return sequence.length();
}

/*
back_insert_iterator<token_sequence::sequence_type> token_sequence::inserter(void)
{
   return back_inserter(sequence);
}
*/

ostream& operator<<(ostream &o, token_sequence &ts) {
   copy(ts.begin(),ts.end(),ostream_iterator<token>(o,""));
   return o;
}

/*!
  Representation of token value. The object is constant and shared.
  \author pt
*/
class token_value {
public:
   //! type of content
   typedef string content_type;
   //! returns object initialized with content
   static token_value *create(const content_type &a_content);
   //! finalizer
   ~token_value(void);
   //! returns content
   const string content_get(void) const;
   //! equality test
   bool equals(token_value *other) const;
   //! returns hash of the content
   ulint hash(void) const;
private:
   //! constructs object with content
   token_value(const content_type &a_content);
   // TODO something easier? array of ucns
   //! the representation of the value 
   content_type content;
   // TODO some static structure for sharing the objects
   // typedef map<content_type *,token_value,special_comparator> shared_type;
   // shared_type shared;
}

/*!
  Constructs object, initializes with a_content.
  \post
  \param a_content  The initialization value.
*/
token_value::token_value(const string &a_content):
   content(a_content)
   //TODO ,shared()
{
}

/*!
  Finalizer.
*/
token_value::~token_value(void)
{
}

token_value *token_value::create(const string &a_content)
{
   // TODO make singleton-like factory
   // shared_type::iterator it = shared.find(&a_content);
   // if (it != shared.end()) return (*it).second;
   // token_value *nju = new token_value(a_content);
   // shared.add(&a_content,nju);
   // return nju;
   return new token_value(a_content);
}

bool token_value::equals(token_value *other) const
{
   // TODO comparison of unique objects
   // return this == other;
   return content == other->content;
}

/*!
  Returns content of value object.
  \return The content of the object.
*/
const string token_value::content_get(void) const
{
   return content;
}

/*!
  Returns hash value of content.
  \return The hash value.
*/
ulint token_value::hash(void) const
{
   // TODO compute hash from content
   return 0;
}

/*!
  Representation of macro parameter list.
  \author pt
*/
class macro_parameters {
public:
   //! adds parameter to list
   bool add(token_value *a_value);
   //! returns index of parameter
   ulint index_of(token_value *a_value) const;
   //! returns list length
   ulint length(void) const;
   //! returns empty parameter list
   static macro_parameters *create(void);
private:
   //! creates empty parameter list
   macro_parameters(void);
   //! token values equality function object
   struct token_values_equal {
      bool operator()(token_value *first, token_value *second) const {
         // TODO some static call on token_value, comparing just ptrs
         return first->equals(second);
         //return first->content_get() == second->content_get();
      }
   };
   
   //! token value hash function object
   struct token_value_hash_function {
      size_t operator()(token_value *hashed) const {
         return static_cast<size_t>(hashed->hash());
      }
   };
   
   //! type of map for storing the parameters
   typedef hash_map<token_value *,ulint,token_hash_function,token_values_equal> map_type;
   //! duplicate of pars.size()
   ulint pars_length;
   //! map of parameters to indexes
   map_type pars;
};

/*!
  Constructs empty parameter list.
  \post pars_length == 0
  \post pars.size() == 0
*/
macro_parameters::macro_parameters(void):
   pars_length(0),
   pars()
{
}

/*!
  Adds parameter to list.
  \pre a_value != NULL
  \param  a_value  Value to be added as parameter.
  \return false  If the value was already present in the list.
*/
bool macro_parameters::add(token_value *a_value)
{
   assert(a_value != NULL);
   typename map_type::const_iterator it = hm.find(a_value);
   if (it != hm.end()) return false;
   hm.insert(make_pair(a_value,pars_length++));
   return true;
}

/*!
  Returns index of parameter in list.
  \pre a_value != NULL
  \return The index of the parameter, length() if parameter is not present.
*/
ulint macro_parameters::index_of(token_value *a_value)
{
   assert(a_value != NULL);
   typename map_type::const_iterator it = hm.find(a_token);
   if (it == hm.end()) return pars_length;
   return (*it).second;
}

/*!
  Returns length of the parameter list, which is also a special return value in index_of method.
  \return Length of the parameter list.
*/
ulint macro_parameters::length(void) const
{
}

/*!
  Internal representation of part of macro expansion list.
  \author pt
*/
class macro_item {
public:
   //! type of action performed within the item
   typedef enum { LITERAL, EXPAND, STRINGIFY, GLUE, NONEXPAND } action_type;
   //! type of index of macro parameter
   // TODO ulint or do we need -1 ?
   typedef int index_type;
   // TODO change create to create_literal etc. to avoid unnecessary asserts
   macro_item(action_type a_action);
   macro_item(action_type a_action, token_sequence *a_value);
   macro_item(action_type a_action, index_type a_index);
   macro_item(action_type a_action, token t);
   //! destructor
   ~macro_item(void);
   //! returns action to be performed
   action_type action_get(void) const;
   //! returns value of literal or glue token
   token_sequence *value_get(void) const;
   //! returns index of referenced macro parameter
   index_type index_get(void) const;
private:
   //! action to be performed
   action_type action;
   //! index of referenced macro parameter
   index_type index;
   //! value of literal or glue token wrapped in sequence
   token_sequence *value;
};

macro_item::macro_item(action_type a_action):
   action(a_action),
   value(NULL),
   index(-1)
{
}

macro_item::macro_item(action_type a_action, token_sequence *a_value):
   action(a_action),
   value(a_value),
   index(-1)
{
}

macro_item::macro_item(action_type a_action, index_type a_index):
   action(a_action),
   value(NULL),
   index(a_index)
{
}

macro_item::macro_item(action_type a_action, token *t):
   action(a_action),
   value(NULL),
   index(-1)
{
   value = new token_sequence();
   value->push(t);
}

/*!
  Finalizer.
*/
macro_item::~macro_item(void)
{
}

/*!
  Returns action of the macro item.
  \return The action represented by the macro item.
*/
macro_item::action_type macro_item::action_get(void) const
{
   return action;
}

/*!
  Returns value of stored token sequence literal.
  \pre action_get() == LITERAL || action_get() == GLUE
  \return The represented value.
*/
token_sequence *macro_item::value_get(void) const
{
   assert(action_get() == LITERAL || action_get() == GLUE);
   return value;
}

/*!
  Returns index of the macro parameter.
  \pre action_get() == EXPAND || action_get() == NONEXPAND || action_get() == STRINGIFY
  \return The index of macro parameter referenced in action.
*/
macro_item::index_type macro_item::index_get(void) const
{
   return index;
}

/*!
  Encapsulation of macro argument with lazy evaluation.
  \author pt
*/
class macro_argument {
public:
   //! parse result type
   typedef enum { CONTINUE, LAST, ERROR, EMPTY } result_type;
   //! constructs empty object
   macro_argument(void);
   //! parses argument
   result_type parse(token_sequence *input, bool first);
   //! returns nonexpanded argument
   token_sequence *nonexpanded_get(void);
   //! returns expanded argument
   token_sequence *expanded_get(void);
   //! returns stringified argument
   token_sequence *stringified_get(void);
private:
   //! type of internal state
   typedef enum { BEGIN, PARSED, DEAD } state_type;
   //! internal state of the object
   state_type state;
   //! the nonexpanded sequence
   token_sequence *nonexpanded;
   //! the expanded sequence, lazy evaluated
   token_sequence *expanded;
   //! the stringified sequence as a token in sequence, lazy evaluated
   token_sequence *stringified;
};

/*!
  Constructs empty macro argument.
*/
macro_argument::macro_argument(void):
   state(BEGIN),
   nonexpanded(NULL),
   expanded(NULL),
   stringified(NULL)
{
}

/*!
  Parses single argument of macro. Value is stored into nonexpanded.
  \pre parse() was not called yet
  \post state == DEAD || nonexpanded == contents of front part of input
  \param input  The source for arguments, parsed tokens (including terminating ones) are removed.
  \param first  Designates whether the argument is first in the argument list.
  \return macro_argument::LAST  When top level right parenthesis was encountered.
  \return macro_argument::EMPTY  When first argument was empty.
  \return macro_argument::CONTINUE  When top level comma was encountered. 
  \return macro_argument::ERROR  For end of file inside input or empty argument.
*/
macro_argument::result_type macro_argument::parse(token_sequence *input, bool first)
{
   assert(state == BEGIN);

   nonexpanded = new token_sequence();
   int depth = 1;
   token t;
   result_type result = ERROR;
   do {
      t = input->skip_ws_nl();
      // TODO this is EOF
      if (t->value_get() == '@') {
         // TODO report error: unexpected EOF
         state = ERROR;
         return ERROR;
      } else if (t->value_get() == '(') {
         depth++;
      } else if (t->value_get() == ')') {
         depth--;
         if (depth == 0) {
            result = LAST;
            break;
         }
      } else if (depth == 1 && t->value_get() == ',') {
         result = CONTINUE;
         break;
      }
      nonexpanded->push(t);
   } while (true);

   if (nonexpanded->empty()) {
      if (first) {
         result = EMPTY;
      } else {
         // TODO report error: empty macro argument         
         result = ERROR;
      }
   }

   if (result == ERROR) state = DEAD;
   return result;
}


/*!
  Returns the nonexpanded macro argument.
  \pre parse() != ERROR
  \return The nonexpanded token sequence.
*/  
token_sequence *macro_argument::nonexpanded_get(void)
{
   assert(state == PARSED);
   return nonexpanded;
}

/*!
  Returns the completely expanded macro argument. Expansion is performed only once.
  \pre parse() != ERROR
  \return The completely expanded token sequence, NULL upon error.
*/
token_sequence *macro_argument::expanded_get(void)
{
   assert(state == PARSED);
   if (expanded == NULL) {
      // TODO returns NULL upon error in completely_expand
      expanded = nonexpanded->expand_clone();
   }
   return expanded;
}

/*!
  Returns stringified macro argument. Stringification is performed only once.
  \pre parse() != ERROR
  \return Token sequence containing single token with value equal to the stringified token sequence.
*/
token_sequence *macro_argument::stringified_get(void)
{
   assert(state == PARSED);
   if (stringified == NULL) {
      // TODO stringification
   }
   return stringified;
}

/*!
  Represents list of macro arguments.
  \author pt
*/
class macro_argument_list {
public:
   //! finalizer
   ~macro_argument_list(void);
   //! parses argument list
   bool parse(token_sequence *input);
   //! returns length of the list
   ulint length();
   //
   macro_argument *argument_get(ulint index);
   // returns empty list
   static macro_argument_list *create(void);
private:
   //! creates empty list
   macro_argument_list(void);
   //! type of internal state
   typedef enum { BEGIN, PARSED, DEAD } state_type;
   //! internal state of the object
   state_type state;
   //! type of list of macro arguments
   typedef vector<macro_argument *> list_type;
   //! list of macro arguments
   list_type arguments;
};

/*!
  Creates empty list.
*/  
macro_argument_list::macro_argument_list(void):
   state(BEGIN),
   arguments()
{
}

/*!
  Finalizer.
*/
macro_argument_list::~macro_argument_list(void)
{
}
  
/*!
  Returns new empty argument list.
  \post arguments.size() == 0
*/
macro_argument_list *macro_argument_list::create(void)
{
   return new macro_argument_list();
}

/*!
  Parses macro argument list in parentheses.
  \pre state == BEGIN
  \param input  The source for arguments.
  \return false  In case of parse error.
*/ 
bool macro_argument_list::parse(token_sequence *input)
{
   assert(state == BEGIN);

   token t;
   t = input->front(); input->pop();
   assert(t.value_get() == '(');
   bool first = true;
   macro_argument::result_type result;

   do {
      macro_argument *ma = new macro_argument();
      result = ma->parse(input,first);
      first = false;
      
      if (result == macro_argument::ERROR) return false;
      if (result == macro_argument::EMPTY) break;
      arguments.push_back(ma);
   } while (result != macro_argument::LAST);
   state = PARSED;
   return true;
}

/*!
  Returns length of the argument list.
  \pre state == PARSED
  \return Length of the argument list.
*/
ulint macro_argument_list::length(void)
{
   assert(state == PARSED);
   return arguments.size();
}

/*!
  Returns argument at specified position.
  \pre state == PARSED
  \pre index < length()  
  \param index  The index of the desired argument.
  \return The argument at specified index.
*/
macro_argument *macro_argument_list::argument_get(ulint index)
{
   assert(state == PARSED);
   assert(index < length());
   return arguments.at(index);
}

/*!
  Represents stored preprocessor macro.
  \author pt
*/
class macro {
public:
//   friend ostream& operator<<(ostream &o, const macro &m);
   // TODO change for vector<string> ??
   //typedef vector<token> head_type;
   //typedef vector<token> body_type;
   //typedef vector<token_sequence *> arguments_type;
   
   //! constructs empty object
   macro(void);
//   macro(const string &a_name, bool a_funlike);   
   //! finalizer
   ~macro(void);
   //! returns macro name
   string name_get(void) const;
   
   //const head_type &head_get(void) const;
   //const body_type &body_get(void) const;
   //head_type &head_get(void);
   //body_type &body_get(void);

   
   //! returns function-like flag
   bool funlike_get(void) const;
   
   //bool add_head(const token &a_token);
   //void add_body(const token &a_token);


   token_sequence *expand(token_sequence *ts);
   token_sequence *expand(void);
   /*
   void clear_arguments(void);
   bool add_argument(token_sequence *arg);
   */
   //! parses the macro definition
   bool parse(token_sequence *ts);

   // TODO equality according to redefinition bool equals(macro *m);
private:
   //! type of internal state
   typedef enum { BEGIN, PARSED, DEAD } state_type;
   //! internal state of the object
   state_type state;
   //! type of parameter list
   // TODO rename
   typedef my_hash<token,int> head_type;
   //! type of expansion list
   // TODO rename
   typedef vector<macro_item *> body_type;
   //! parses name
   bool parse_name(token_sequence *input);
   //! parses parameter list
   bool parse_head(token_sequence *input);
   //! parses expansion list
   bool parse_body(token_sequence *input);
   //! name of the macro
   // TODO change for token value
   string name;
   //! flag designating function-like macro
   bool funlike;
   //head_type head;
   //body_type body;
   //arguments_type arguments;
   // TODO not pointers, members
   //! parameter list of the macro
   head_type *head;
   //! expantsion list of the macro
   body_type *body;
   // TODO add head names, body sequence
};

/*!
  Parses macro parameter list, sets funlike and stores parameter into self.
  \pre state == BEGIN
  \param input  The source for tokens, starting after macro name.
  \return false  In case of duplicate parameters or unterminated parameter list.
*/
bool macro::parse_head(token_sequence *input) 
{
   // TODO move to ctor
   head = new head_type();
   token *t;

   t = input->peek_front();
   // TODO check type
   if (t->value_get() != '(') {
      funlike = false;
      return true;
   }
   
   input->read_front();
   funlike = true;
   
   do {
      t = input->read_front_ws();
      // TODO check idf
      if (!isalpha(t.value_get())) {
         // TODO report error: expected identifier
         state = DEAD;
         return false;
      }

      if (!head->insert(t)) {
         // TODO report error: duplicate macro pars
         state = DEAD;
         return false;
      }

      t = input->read_front_ws();
      if (t->value_get() == ')') break;
      if (t->value_get() != ',') {
         // TODO report parse error: expected `,'
         state = DEAD;
         return false;
      }
   } while (true);
   state = PARSED;
   return true;
}

/*!
  Parses function like macro replacement list and stores values into self.
  \param input  The source for tokens, starting after parameter list.
  \return false  If the replacement list was ill-formed.
*/
bool macro::parse_funlike_body(token_sequence *input)
{
   typedef enum { BEGIN, LITERAL, STRINGIFY, GLUE, NEWLINE, END } fstate_type;
   fstate_type fstate = BEGIN;
   enum { LITERAL, EXPAND, STRINGIFY, GLUE, NEWLINE } choice;
   token *t;
   bool flush = false;
   bool read = true;
   token_sequence *literal;// = new token_sequence();

   int idx;
   do {
      //if (read)
      
      t = input->read_front_ws();
      
      //read = true;
      //state = 0;
      
      // TODO check identifier
      if (isalpha(t->value_get())) {
         idx = head->lookup(t);
         if (idx != -1) {
            flush = true;
            state = 1;
         }
      } else if (t->value_get() == '#') {
         flush = true;
         t = input->read_front_ws();
         idx = head->lookup(t);
         if (idx == head->-1) {
            // TODO error message: # not followed by macro parameter
            return false;
         }
         body->push_back(new macro_item(macro_item::STRINGIFY,idx));
         fstate = STRINGIFY;

         // TODO a substitute for ##
      } else if (t->value_get() == '$') {
         if (fstate == BEGIN) {
            // TODO error message: ## at the beginning of expansion list
            return false;
         } else if (fstate == EXPAND) {
            // check for preceding token in current literal sequence
            macro_item *mbi = body->back();
            body->pop_back();
            // update to avoid expansion
            body->push_back(new macro_item(macro_item::NONEXPAND,mbi->index_get()));
         } else if (fstate == LITERAL) {
            // flush the literal
            body->push_back(new macro_item(macro_item::LITERAL,literal));
         }
         fstate = GLUE;
      } else if (t.value_get() == '\n') {
         choice = NEWLINE;
      }

      // TODO change these ifs to switch according to result of previous if cascade
      if (flush) {
         if (!y->empty()) {
            body->push_back(new macro_item(macro_item::LITERAL,y));
            y == NULL;
         }
      }
      
      // TODO complete
      switch (choice) {
         case EXPAND:
            break;
         case STRINGIFY:
            break;
         case GLUE:
            break;
         case NEWLINE:
            break;
         case 
      }

      if (state == 1) {
         body->push_back(new macro_item(macro_item::EXPAND,idx));
      } else if (state == 2) {
         t = input->skip_ws();
         idx = head->lookup(t);
         if (idx == -1) {
            // TODO error message: # not followed by macro parameter
            return false;
         }
         body->push_back(new macro_item(macro_item::STRINGIFY,idx));
         // TODO a substitute for ##
      } else if (state == 3) {
         body->push_back(new macro_item(macro_item::GLUE,t));
         
         t = input->skip_ws();
         if (isalpha(t.value_get())) {
            idx = head->lookup(t);
            if (idx != -1) {
               body->push_back(new macro_item(macro_item::NONEXPAND,idx));
            } else read = false;
         } else read = false;               
         
      } else if (state == 4) {
         break;
      }
         // this is here due to break on '\n'
      if (flush) {
         if (y == NULL) y = new token_sequence();
      } else
         y->push(t);         
   } while (true);
   return true;
}

/*!
  Parses object-like macro replacement list and stores values into self.  
  \param input  The source for tokens, starting at the beginning of the replacement list.
  \return false  If the replacement list was ill-formed.
*/
bool macro::parse_objlike_body(token_sequence *input)
{
   token_sequence *literal;
   enum { BEGIN, LITERAL, GLUE, END } fstate = BEGIN;
   token *t;
   
   do {
      if (fstate == BEGIN)
         t = input->read_front_ws();
      else
         t = input->read_front_squeeze();
      
      // TODO this is substitute for ##
      if (t->value_get() == '$') {
         // check whether the ## is not at the beginning
         if (fstate == BEGIN) {
            // TODO error message: ## at the beginning of expansion list
            return false;
         }
         
         if (fstate == LITERAL)
            body->push_back(new macro_item(macro_item::LITERAL,literal));
         
         body->push_back(new macro_item(macro_item::GLUE,t));
         fstate = GLUE;
      } else if (t.value_get() == '\n') {
         if (fstate == GLUE) {
            // TODO error message: ## at the end of the expansion list
            return false;
         }
         if (fstate == LITERAL) body->push_back(new macro_item(macro_item::LITERAL,literal));
         fstate = END;
      } else {
         if (fstate != LITERAL) {
            literal = new token_sequence();
            fstate = LITERAL;
         }
         literal->push(t);
      }
   } while (fstate != END);
   return true;
}

/*!
  Parses macro replacement list and stores values into self.  
  \param input  The source for tokens, starting at the beginning of the replacement list.
  \return false  If the replacement list was ill-formed.
*/
bool macro::parse_body(token_sequence *input)
{
   // TODO move to ctor
   body = new body_type();
   
   if (funlike_get()) {
      if (!parse_funlike_body(input);
   } else {
   }

   if (!body->empty()) {
      macro_item *mbi = body->back();
      if (mbi->action_get() == macro_item::GLUE) {
         // TODO report error
         return false;
      }
      
   }
   return true;
}

/*
token_sequence *macro::remove_spaces(token_sequence *ts)
{
   token_sequence *rm = new token_sequence();
   for (token_sequence::iterator_type it = ts->begin(); it != ts->end(); ++it) {
      token &t = *it;
      if (t.value_get() != ' ') rm->push(t);
   }
   
   return rm;
}
*/

/*!
  Parses macro name and stores into self.
  \param input  The source for tokens, starting before macro name.
  \return false  In case of parse error.
*/
bool macro::parse_name(token_sequence *input)
{
   token t;
   t = input->skip_ws();
   // TODO test type: idf
   
   if (isalpha(t.value_get())) {
      // TODO error message
      return false;
   }
   name = char_to_string(t.value_get());
}

/*!
  Parses macro definition and stores data into self.
  \pre state == BEGIN 
  \param input  The source for tokens, starting before macro name.
  \return false  In case of parse error.
*/
bool macro::parse(token_sequence *input)
{
   assert(state == BEGIN);
   if (!parse_name(input)) {
      state = ERROR;
      return false;
   }
   if (!parse_head(input)) {
      state = ERROR;
      return false;
   }
   //token_sequence *cd = remove_spaces(ts);
   if (!parse_body(input)) {
      state = ERROR;
      return false;
   }
   state = PARSED;
   return true;
}

/*!
  Creates new empty macro object.
  \post state == BEGIN
*/
macro::macro():
   state(BEGIN)//,
//   name(""),
//   funlike(false),   
{
}

/*!
  Finalizer.
*/
macro::~macro(void) 
{
}

/*!
  Returns the name of the macro.
  \pre state == PARSED 
  \return The name of the macro.
*/
string macro::name_get(void) const
{
   assert(state == PARSED);
   return name;
}

/*
macro::head_type &macro::head_get(void)
{
   return head;
}

macro::body_type &macro::body_get(void)
{
   return body;
}

const macro::head_type &macro::head_get(void) const
{
   return head;
}

const macro::body_type &macro::body_get(void) const
{
   return body;
}
*/

/*!
  Returns whether the macro is function-like.
  \pre state == PARSED
  \return true  If the macro is function-like.
*/
bool macro::funlike_get(void) const
{
   assert(state == PARSED);
   return funlike;
}

/*
bool macro::add_head(const token &a_token)
{
   // TODO check duplicity
   head.push_back(a_token);
   return true;
}

void macro::add_body(const token &a_token)
{
   body.push_back(a_token);
   
   //char c = a_token.value_get();
   //if (char_to_string(c) == name_get())
   //  body.back().available_set(false);
   
}

void macro::clear_arguments(void)
{
   arguments.clear();
}

bool macro::add_argument(token_sequence *arg)
{
   arguments.push_back(arg);
}
*/

/*!
  Expands this macro, if the call matches the definition, taking possible arguments from input.
  \param input  The source of possible arguments, starting at macro name.
  \return The sequence containing the token denoting the name if the call did not match.
  \return The expanded, but not rescanned sequence if the call matched the definition.
  \return NULL in case of nested error.
*/
token_sequence *macro::expand(token_sequence *input)
{
   // the name of the macro
   token *name = input->read();
   
   if (funlike_get()) {
      token *peek = input->peek_front();
      if (peek->value_get() == '(') return expand_funlike(input);
      // function-like macro name, but no arguments, return the nonexpanded token
      token_sequence *single = new token_sequence();
      single->write(name);
      return single;
   }
   return expand_objlike();   
}

/*!
  Expands this macro, taking arguments from input.
  \param input
*/
token_sequence *macro::expand_funlike(token_sequence *input)
{
   macro_argument_list *mal = new macro_argument_list();
   if (!mal->parse(input)) return NULL;

   //if (mal->length() != head->size()) {
   // TODO report error: par cnt != arg cnt
   //}

   token_sequence *result = new token_sequence();
   body_type::iterator it = body->begin();
   body_type::iterator last = body->end();
   for ( ; it != last; ++it) {
      bool glue = false;
      macro_item *mi = *it;
      if (mi->action_get() == macro_item::GLUE) {
         glue = true;
         ++it;
         // safe, because glue is never at the end
         mi = *it;
         
         //if (mi->action_get() == macro_item::GLUE) {
            // TODO this is hard
            //
         //}
      }

      token_sequence *y;
      switch (mi->action_get()) {
         case macro_item::LITERAL:
            y = mi->value_get();
            break;
         case macro_item::EXPAND:
            macro_argument *ma = mal->argument_get(mi->index_get());
            y = ma->expanded_get();
            break;
         case macro_item::NONEXPAND:
            macro_argument *ma = mal->argument_get(mi->index_get());
            y = ma->nonexpanded_get();
            break;
         case macro_item::STRINGIFY:
            macro_argument *ma = mal->argument_get(mi->index_get());
            y = ma->stringified_get();
            break;
         case macro_item::GLUE:
            // TODO this is the special case when we glue something and ## together
            // god be graceful!
            // it is hidden as a single ## token under literal
            y = mi->value_get();
         default:
            // TODO you should never get here
            assert(false);
      }

      if (glue) {
         token a = result->back();
         token b = y->front();
         token c = a.append(b);
         // TODO error token type
         if (c.value_get() != '!') {
            // TODO report error
            return NULL;
         }
         result->pop();
         y->pop_back();
         y->push(c);
      }

      result->append(y->clone(/* TODO new location */));
   }
   return result;
}

/*!
  Expand this object-like macro.
  \return The expanded, but not rescanned sequence.
  \return NULL in case of error.
*/
token_sequence *macro::expand_objlike(void)
{
   // TODO implement
   return NULL;
}

#if 0
token_sequence *macro::expand(void)
{
   int i = 0;
   for (head_type::iterator it = head.begin(); it != head.end(); ++it, ++i) {
      arguments[i] = completely_expand(*arguments[i]);
   }
   
   token_sequence *ts = new token_sequence();

   body_type::iterator first = body.begin();
   body_type::iterator last = body.end();
   token_sequence::inserter_type result = ts->inserter();
   
   for ( ; first != last; ++result, ++first) {
      if (funlike) {
         // TODO perform this ugly algo via data structure (no arg names, only indexes, pointers)
         token &t = *first;
         head_type::iterator found = find_if(head.begin(),head.end(),token_value_equals(t.value_get()));
         if (found != head.end()) {
            int idx = found - head.begin();
            copy(arguments[idx]->begin(),arguments[idx]->end(),result);            
         } else *result = t;
      } else {
         *result = *first;
      }
      //cerr << (*result).value_get() << " copied\n";
   }

   //copy(body.begin(),body.end(),ts->inserter());

   cerr << "macro::expand expanded to:\n";
   cerr << *ts;
   cerr << "\nmacro::expand expanded to end\n";
      
   return ts;
}
#endif

ostream& operator<<(ostream &o, const macro &m) {
   o << m.name_get() << ' ';
   /*
   const macro::body_type mb = m.body_get();
   copy(mb.begin(),mb.end(),ostream_iterator<token>(o,""));
   */
   return o;
}

class macro_name_equals {
public:
   macro_name_equals(const string &a_name): name(a_name) {}
   bool operator()(macro *arg) { return name == arg->name_get(); }
private:
   string name;
};

token_sequence feed;

/*
int next(void)
{
   char c;
   if (!cin.get(c)) return EOF;
   return c;
}

int skipws(void)
{
   char c;
   do {
      if (!cin.get(c)) return EOF;
   } while (isblank(c));
   return c;
   
}
*/

int next(token &t)
{
   if (feed.empty()) return EOF;
   t = feed.front();
   feed.pop();
   return t.value_get();
}

int skipws(token &t)
{
   do {
      if (feed.empty()) return EOF;
      t = feed.front();      
      feed.pop();
   } while (isblank(t.value_get()));
   return t.value_get();
}

int peek(void)
{
   if (feed.empty()) return EOF;
   token t(feed.front());
   return t.value_get();
}

list<macro *> macros;

macro *macro_lookup(char c)
{
      
   list<macro *>::iterator it = find_if(macros.begin(),macros.end(),
         macro_name_equals(char_to_string(c)));
   if (it == macros.end()) return NULL;
      
   return *it;
}

#if 0
token_sequence *expand(token_sequence &seq, bool stop_newline);

token_sequence *completely_expand(token_sequence &seq)
{
   cerr << "====== called completely_expand ======\n";
   cerr << "sequence to expand\n";
   cerr << seq;
   cerr << "\nsequence to expand end\n";
   
   token_sequence *rs = new token_sequence();
   while (!seq.empty()) {
      token_sequence *ex = expand(seq,false);
      // TODO change for rs->append(ex);
      copy(ex->begin(),ex->end(),rs->inserter());
      delete ex;
   }
   cerr << "====== return completely_expand ======\n";
   return rs;
}

/*!
  expands first found macro in the sequence, terminating at end of line,
  not counting eol inside macro call

  \param seq  the input sequence, is amended (prepended) by tokens from expansion
  \return the tokens before first macro expansion
*/  
token_sequence *expand(token_sequence &seq, bool stop_newline)
{
   cerr << "====== called expand ======\n";
   
   char c;
   token t, u;
   token_sequence *rs = new token_sequence();
   
   while (!seq.empty()) {
      t = seq.front(); seq.pop();
      c = t.value_get();
      cerr << "got " << c << '\n';

      taboo_macros *tm = t.taboo_get();

      cerr << "taboo macros:\n";
      if (tm) cerr << *tm; else cerr << "none\n";
      cerr << "taboo macros end\n";
      
      macro *m;
      if (isalpha(c) /*&& t.enabled()*/ && ((m = macro_lookup(c)) != NULL) &&
            (tm == NULL || !tm->contains(m)) &&
          (!(m->funlike_get()) || (!seq.empty() && seq.front().value_get() == '('))) {
         token_sequence *ex;

         cerr << "found macro to expand: " << *m << '\n';

         if (m->funlike_get()) {
            // the (
            seq.pop();
            
            m->clear_arguments();
            
            token_sequence *arg = new token_sequence();
            int depth = 1;

            while (true) {
               if (seq.empty()) {
                  cerr << "fatal\n";
                  exit(1);
               }
               t = seq.front(); seq.pop();
               c = t.value_get();
               if (c == '(')
                  depth++;
               else if (c == ')') {
                  depth--;
                  if (depth == 0) break;
               }
               if (depth == 1 && c == ',') {
                  // TODO check test
                  m->add_argument(arg);

                  cerr << "added argument\n";
                  cerr << *arg;
                  cerr << "\nadded argument end\n";
                  arg = new token_sequence();
               } else {
                  cerr << "pushing argument " << t << '\n';
                  arg->push(t);
               }
            }
            // TODO actually add_last_argument (with check of proper number)
            m->add_argument(arg);
            
            cerr << "added last argument\n";
            cerr << *arg;
            cerr << "\nadded last argument end\n";
            
            // m contains macro to be expanded
            //ex = m->expand(args);
         }
         /*else {*/
            ex = m->expand();
         //}
         
         cerr << "expansion:\n";
         cerr << *ex;
         //copy(ex->begin(),ex->end(),ostream_iterator<token>(cerr,""));
         cerr << "\nexpansion end\n";

         // TODO still not decided how to rescan after replacement ??

         for (token_sequence::iterator_type it = ex->begin(); it != ex->end(); ++it) {
            token &t = *it;
            
            if (isalpha(t.value_get())) {
               taboo_macros *ttm = t.taboo_get();
               if (ttm == NULL) {
                  ttm = new taboo_macros();
                  t.taboo_set(ttm);
               }
               if (tm != NULL) ttm->add_all(*tm);
               ttm->add(m);
            }
         }

         //prepend ex in O(1)
         seq.prepend(ex);

         delete ex;
         break;
      } else {
         //t.disable();
         cerr << "pushing " << t.value_get() << '\n';
         rs->push(t);

         cerr << "current output\n";
         cerr << *rs << '\n';
         cerr << "current output end\n";
         //copy(rs->begin(),rs->end(),ostream_iterator<token>(cerr,""));
      }
      if (stop_newline && c == '\n') break;
   }
   
   cerr << "current output before return\n";
   cerr << *rs << '\n';
   cerr << "current output before return end\n";
   cerr << "====== return expand ======\n";
   return rs;   
}
#endif

//template struct std::pair<const token,int>;

/* TODO no need to test end of iterator in token_sequence, because
   there will always be EOL or EOF as last token */

int main(void)
{
   token t(0);
   int c;
   char d;

   while (cin.get(d)) {
      // TODO squeeze spaces into a single one
      t.value_set(d);
      feed.push(t);
   }

   // TODO this is EOF
   t.value_set('\');
   feed.push(t);

   cerr << "eof\n";

   while ((c = peek()) != EOF) {

      if (c == '#') {
         c = next(t);

         c = skipws(t);
         if (c == 'd') {
            /*
            while (!isspace(c)) {
               c = next(t);
            }
            c = skipws(t);
            if (c == EOF || !isalpha(c)) {
               cerr << "bug\n";
               return 1;
            }
            d = char(c);
*/
            macro *m = new macro();
            //char_to_string(char(d)));

  /*           
            c = next(t);
            if (c == '(') {
               m = new macro(char_to_string(char(d)),true);

               
               do {
                  c = skipws(t);
                  // TODO various checks (!EOF,id)
                  m->add_head(t);
                  // throw away comma
                  c = skipws(t);
                  // TODO checks
                  if (c == ')') break;
               } while (true);
               
            } else {
               m = new macro(char_to_string(char(d)),false);
            }
            */

            m->parse(&feed);

            /*
            // TODO warning! adds ws before newline!
            c = skipws(t);
            while (c != EOF && c != '\n') {
               m->add_body(t);
            
               c = next(t);
            }
            */
            
            macros.push_back(m);

            cerr << "defined macro " << *m << '\n';

         } else if (c == 'u') {
            while (!isspace(c)) {
               c = next(t);
            }
            c = skipws(t);
            if (c == EOF || !isalpha(c)) {
               cerr << "bug\n";
               return 1;
            }
            char d = c;
            string str(&d,1);
            // TODO change for hashing
            macros.erase(find_if(macros.begin(),macros.end(),macro_name_equals(str)));
         } else {
            cerr << "bug\n";
            return 1;
         }         
      } else {
         cerr << "contents of feed:\n";
         cerr << feed;
         //copy(feed.begin(),feed.end(),ostream_iterator<token>(cerr,""));
         cerr << "\ncontents of feed end\n";

         // TODO FIXME
         token_sequence *ts = feed.expand_clone();

         cerr << "output:\n";
         cout << *ts;
         //copy(ts->begin(),ts->end(),ostream_iterator<token>(cout,""));
         cerr << "\noutput end\n";
         delete ts;
      }
   }

   cerr << "\ncurrently defined macros:\n";

   for (list<macro *>::iterator it = macros.begin(); it != macros.end(); it++) {
      macro *m = *it;
      cerr << *m << '\n';
/*
      if (m->funlike_get()) {
         cout << '(';
         macro::head_type h(m->head_get());
         copy(h.begin(),h.end(),ostream_iterator<token>(cout,","));
         cout << ')';
      }
      cout << " : ";
      macro::body_type b(m->body_get());
      copy(b.begin(),b.end(),ostream_iterator<token>(cout,""));
      cout << '\n';
      */
   }
   
   return 0;
}
