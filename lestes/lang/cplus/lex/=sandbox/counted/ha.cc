/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <ext/hash_map>

using namespace ::std;
using namespace ::__gnu_cxx;

class key {
public:
   key(int a_data): data(a_data) {}
   int data;
};

class value {
public:
   value(void): data(0) {}
   value(int a_data): data(a_data) {}
   int data;
};

struct key_hash_function {
   size_t operator()(const key &k) const {
      return static_cast<size_t>(k.data);
   }
};

struct key_equal_function {
   bool operator()(const key &a, const key &b) {
      return a.data == b.data;
   }
};

hash_map<key,value,key_hash_function,key_equal_function> hm;

int main(void)
{
   key k(3);
   value v(14);
   
   hm[k] = v;

   return 0;   
}
