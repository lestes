/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/lang/cplus/lex/sand/token.hh>
#include <lestes/lang/cplus/lex/sand/counted_ptr.hh>
#include <lestes/lang/cplus/lex/sand/counted.hh>
#include <iostream>

package(lestes);
package(lang);
package(cplus);
package(lex);
package(sand);
using namespace ::std;
using namespace ::lestes::lang::cplus::lex::sand;

/*
typedef counted_ptr<counted> cpc;
int maine(void)
{
   cpc ptr(new counted());
   ptr->reference_print();
   {
      cpc qtr(ptr);
      ptr->reference_print();
   }
   ptr->reference_print();
   cerr << "create second:" << endl;
   counted *c = new counted();
   ptr->reference_print();
   {
      cpc qtr(c);
      qtr->reference_print();
      cerr << "before assignment:" << endl;
      ptr = qtr;
      ptr->reference_print();
   }
   ptr->reference_print();
   
   return 0;
}
*/

typedef counted_ptr<counted> cpt;
int main(void)
{
   cerr << "create first:" << endl;
   cpt ptr(token::create_token(1));
   ptr->reference_print();
   {
      cpt qtr(ptr);
      ptr->reference_print();
   }
   ptr->reference_print();
   cerr << "create second:" << endl;
   token *c = token::create_token(2);
   ptr->reference_print();
   {
      cpt qtr(c);
      qtr->reference_print();
      cerr << "before assignment:" << endl;
      ptr = qtr;
      ptr->reference_print();
   }
   ptr->reference_print();
   
   return 0;
}

end_package(sand);
end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

int main(void) {
   return ::lestes::lang::cplus::lex::sand::main();
}
