/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <iostream>
#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/sand/counted.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);
package(sand);

using namespace ::std;

counted::counted(void):
   reference_count(0)
{
   cerr << "created counted" << endl;
}

counted::~counted(void)
{
   cerr << "deleted counted" << endl;
}

/*inline*/ void counted::reference_acquire(void)
{
   reference_count++;
   // TODO assert(reference_count != 0) - overflow
}

/*inline*/ void counted::reference_release(void)
{
   // TODO assert(reference_count != 0);
   reference_count--;
   if (reference_count == 0) delete this;
}

/*inline*/ void counted::reference_print(void)
{
   cerr << reference_count << endl;
}

end_package(sand);
end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

