/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex__sand___counted_ptr_hh___included
#define lestes__lang__cplus__lex__sand___counted_ptr_hh___included

#include <cstdlib>
#include <lestes/common.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);
package(sand);

/**
  pointer to intrusive reference counted class which provides two functions:
  reference_acquire(void);
  reference_release(void);
*/
template<typename Counted>
class counted_ptr {
public:
   //! create new pointer to object
	explicit counted_ptr(Counted *a_counted = NULL);
   
   //! create pointer to existing object
	counted_ptr(const counted_ptr<Counted> &copy);

   //! assign pointer to object
	counted_ptr<Counted> &operator=(const counted_ptr<Counted> &rhs);
   
   //! release pointer to object
	~counted_ptr(void);
   
   //! return underlying pointer
	inline Counted *operator->(void) const;
   
   //! return underlying reference
	inline Counted &operator*(void) const;
   
private:
   //! the pointee object
	Counted *counted;
   //! acquire new reference to object
   inline void acquire(void);
   
   //! release reference from object
   inline void release(void);
};

template<typename Counted>
counted_ptr<Counted>::counted_ptr(Counted *a_counted):
   counted(a_counted)
{		
   acquire();
}

template<typename Counted>
counted_ptr<Counted>::counted_ptr(const counted_ptr<Counted> &copy):
   counted(copy.counted)
{
   acquire();
}

template<typename Counted>
counted_ptr<Counted> &counted_ptr<Counted>::operator=(const counted_ptr<Counted> &rhs)
{
   if (counted != rhs.counted) {
      // the order is allright as the pointers are different
      release();
      counted = rhs.counted;
      acquire();
   }
   
   return *this;
}

template<typename Counted>
counted_ptr<Counted>::~counted_ptr(void)
{
   release();
}

template<typename Counted>
Counted *counted_ptr<Counted>::operator->(void) const
{
   return counted;
}

template<typename Counted>
Counted &counted_ptr<Counted>::operator*(void) const
{
   return *counted;
}

template<typename Counted>
inline void counted_ptr<Counted>::acquire(void)
{
   if (counted != NULL) counted->reference_acquire();
}
   
template<typename Counted>
inline void counted_ptr<Counted>::release(void)
{
   if (counted != NULL) counted->reference_release();
}
   
end_package(sand);
end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
