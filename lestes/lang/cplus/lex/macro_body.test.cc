/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  Unit test for class macro_body.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/std/source_location.hh>
#include <lestes/std/ucn_string.hh>
#include <lestes/lang/cplus/lex/macro_body.hh>
#include <lestes/lang/cplus/lex/macro_head.hh>
#include <lestes/lang/cplus/lex/token_sequence.hh>
#include <lestes/lang/cplus/lex/token_value.hh>
#include <lestes/lang/cplus/lex/pp_token.hh>
#include <string>

package(lestes);
package(lang);
package(cplus);
package(lex);

using namespace ::std;

void macro_body_test(void)
{
	ptr<file_info> fi = file_info::create(string("abc"),NULL);
	ptr<source_location> loc = source_location::create(fi,0,0);

	ptr<token_sequence> mhts = token_sequence::create();
	mhts->add_back( pp_token::create(loc,pp_token::TOK_IDENT,token_value::create("abc")) );
	mhts->add_back( pp_token::create(loc,pp_token::TOK_COMMA) );
	mhts->add_back( pp_token::create(loc,pp_token::TOK_IDENT,token_value::create("def")) );
	mhts->add_back( pp_token::create(loc,pp_token::TOK_RIGHT_PAR) );
	
	ptr<macro_head> mh = macro_head::create();
	lassert(is_equal(mh->parse(mhts),true));
	
	ptr<token_sequence> ts = token_sequence::create();
	ts->add_back( pp_token::create(loc,pp_token::TOK_BLANK) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_BLANK) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_LINE_END) );
		
	ts->add_back( pp_token::create(loc,pp_token::TOK_HASH_HASH) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_LINE_END) );
		
	ts->add_back( pp_token::create(loc,pp_token::TOK_GT) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_BLANK) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_HASH_HASH) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_LINE_END) );

	ts->add_back( pp_token::create(loc,pp_token::TOK_AMP) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_BLANK) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_HAT) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_LINE_END) );

	ts->add_back( pp_token::create(loc,pp_token::TOK_BLANK) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_BLANK) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_AMP) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_BLANK) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_BLANK) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_HAT) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_BLANK) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_BLANK) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_LINE_END) );

	ts->add_back( pp_token::create(loc,pp_token::TOK_AMP) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_HAT) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_LINE_END) );

	ts->add_back( pp_token::create(loc,pp_token::TOK_EMARK) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_BLANK) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_BLANK) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_HASH_HASH) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_BLANK) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_EQ) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_LINE_END) );

	ts->add_back( pp_token::create(loc,pp_token::TOK_EMARK) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_BLANK) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_HASH_HASH) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_BLANK) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_BLANK) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_EQ) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_LINE_END) );

	ts->add_back( pp_token::create(loc,pp_token::TOK_LT) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_HASH_HASH) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_HASH_HASH) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_EQ) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_LINE_END) );

		// function-like
	ts->add_back( pp_token::create(loc,pp_token::TOK_BLANK) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_BLANK) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_LINE_END) );
		
	ts->add_back( pp_token::create(loc,pp_token::TOK_HASH_HASH) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_LINE_END) );
		
	ts->add_back( pp_token::create(loc,pp_token::TOK_GT) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_BLANK) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_HASH_HASH) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_LINE_END) );

	ts->add_back( pp_token::create(loc,pp_token::TOK_AMP) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_BLANK) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_HAT) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_LINE_END) );

	ts->add_back( pp_token::create(loc,pp_token::TOK_BLANK) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_BLANK) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_AMP) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_BLANK) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_BLANK) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_HAT) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_BLANK) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_BLANK) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_LINE_END) );

	ts->add_back( pp_token::create(loc,pp_token::TOK_AMP) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_HAT) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_LINE_END) );

	ts->add_back( pp_token::create(loc,pp_token::TOK_BLANK) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_EQ) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_BLANK) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_IDENT,token_value::create("iii")) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_BLANK) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_PLUS) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_IDENT,token_value::create("abc")) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_BLANK) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_LINE_END) );

	ts->add_back( pp_token::create(loc,pp_token::TOK_EQ) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_BLANK) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_BLANK) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_IDENT,token_value::create("iii")) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_PLUS) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_IDENT,token_value::create("abc")) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_BLANK) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_LINE_END) );

	ts->add_back( pp_token::create(loc,pp_token::TOK_HASH) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_IDENT,token_value::create("abc")) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_LINE_END) );

	ts->add_back( pp_token::create(loc,pp_token::TOK_HASH) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_IDENT,token_value::create("iii")) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_LINE_END) );

	ts->add_back( pp_token::create(loc,pp_token::TOK_IDENT,token_value::create("iii")) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_HASH_HASH) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_IDENT,token_value::create("abc")) );
	ts->add_back( pp_token::create(loc,pp_token::TOK_LINE_END) );

	ptr<macro_body> mb0 = macro_body::create();
	// created object
	lassert(is_equal(mb0,mb0));
	lassert(is_equal(mb0->state_get(),macro_body::BEGIN));
	
	// object-like macro

	// eol
	lassert(is_equal(mb0->parse(ts),true));
	lassert(is_equal(mb0->state_get(),macro_body::PARSED));
	lassert(is_equal(mb0,mb0));

	ptr<macro_body> mb1 = macro_body::create();
	// ## at beginning 
	lassert(is_equal(mb1->parse(ts),false));
	lassert(is_equal(mb1->state_get(),macro_body::DEAD));
	lassert(is_equal(mb1,mb1));
	lassert(!is_equal(mb1,mb0));
	
	ptr<macro_body> mb2 = macro_body::create();
	// ## at the end
	lassert(is_equal(mb2->parse(ts),false));
	lassert(is_equal(mb2->state_get(),macro_body::DEAD));
	lassert(is_equal(mb2,mb1));

	ptr<macro_body> mb3 = macro_body::create();
	// normal tokens 
	lassert(is_equal(mb3->parse(ts),true));
	lassert(is_equal(mb3->state_get(),macro_body::PARSED));

	ptr<macro_body> mb4 = macro_body::create();
	// the same tokens with front and back ws and extended internal ws
	lassert(is_equal(mb4->parse(ts),true));
	lassert(is_equal(mb4->state_get(),macro_body::PARSED));
	lassert(is_equal(mb4,mb4));
	lassert(is_equal(mb4,mb3));

	ptr<macro_body> mb5 = macro_body::create();
	// the same tokens with different ws
	lassert(is_equal(mb5->parse(ts),true));
	lassert(is_equal(mb5->state_get(),macro_body::PARSED));
	lassert(!is_equal(mb5,mb4));

	ptr<macro_body> mb6 = macro_body::create();
	// tokens with ## inside and ws
	lassert(is_equal(mb6->parse(ts),true));
	lassert(is_equal(mb6->state_get(),macro_body::PARSED));
	lassert(is_equal(mb6,mb6));

	ptr<macro_body> mb7 = macro_body::create();
	// tokens with ## inside and equivalent ws around
	lassert(is_equal(mb7->parse(ts),true));
	lassert(is_equal(mb7->state_get(),macro_body::PARSED));
	lassert(is_equal(mb7,mb7));
	lassert(is_equal(mb7,mb6));

	ptr<macro_body> mb8 = macro_body::create();
	// tokens with two ## successive ##
	lassert(is_equal(mb8->parse(ts),true));
	lassert(is_equal(mb8->state_get(),macro_body::PARSED));
	lassert(is_equal(mb8,mb8));

	// function-like macro
	ptr<macro_body> mb9 = macro_body::create();

	// eol
	lassert(is_equal(mb9->parse(ts,mh),true));
	lassert(is_equal(mb9->state_get(),macro_body::PARSED));
	lassert(is_equal(mb9,mb9));

	ptr<macro_body> mb10 = macro_body::create();
	// ## at the beginning
	lassert(is_equal(mb10->parse(ts,mh),false));
	lassert(is_equal(mb10->state_get(),macro_body::DEAD));
	lassert(!is_equal(mb10,mb9));

	ptr<macro_body> mb11 = macro_body::create();
	// ## at the end
	lassert(is_equal(mb11->parse(ts,mh),false));
	lassert(is_equal(mb11->state_get(),macro_body::DEAD));
	lassert(is_equal(mb11,mb10));

	ptr<macro_body> mb12 = macro_body::create();
	// ordinary tokens
	lassert(is_equal(mb12->parse(ts,mh),true));
	lassert(is_equal(mb12->state_get(),macro_body::PARSED));
	lassert(is_equal(mb12,mb12));

	ptr<macro_body> mb13 = macro_body::create();
	// ordinary tokens with equivalent ws
	lassert(is_equal(mb13->parse(ts,mh),true));
	lassert(is_equal(mb13->state_get(),macro_body::PARSED));
	lassert(is_equal(mb13,mb12));

	ptr<macro_body> mb14 = macro_body::create();
	// ordinary tokens with nonequivalent ws
	lassert(is_equal(mb14->parse(ts,mh),true));
	lassert(is_equal(mb14->state_get(),macro_body::PARSED));
	lassert(!is_equal(mb14,mb13));

	ptr<macro_body> mb15 = macro_body::create();
	// identifier and parameter
	lassert(is_equal(mb15->parse(ts,mh),true));
	lassert(is_equal(mb15->state_get(),macro_body::PARSED));
	lassert(is_equal(mb15,mb15));

	ptr<macro_body> mb16 = macro_body::create();
	// identifier and parameter with equivalent ws
	lassert(is_equal(mb16->parse(ts,mh),true));
	lassert(is_equal(mb16->state_get(),macro_body::PARSED));
	lassert(is_equal(mb16,mb16));

	ptr<macro_body> mb17 = macro_body::create();
	// # and parameter
	lassert(is_equal(mb17->parse(ts,mh),true));
	lassert(is_equal(mb17->state_get(),macro_body::PARSED));
	lassert(is_equal(mb17,mb17));

	ptr<macro_body> mb18 = macro_body::create();
	// # and identifier
	lassert(is_equal(mb18->parse(ts,mh),false));
	lassert(is_equal(mb18->state_get(),macro_body::DEAD));

	ptr<macro_body> mb19 = macro_body::create();
	// identifier ## and parameter
	lassert(is_equal(mb19->parse(ts,mh),true));
	lassert(is_equal(mb19->state_get(),macro_body::PARSED));
	lassert(is_equal(mb19,mb19));
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

int main(void)
{
	::lestes::lang::cplus::lex::macro_body_test();
	return 0;
}
/* vim: set ft=lestes : */

