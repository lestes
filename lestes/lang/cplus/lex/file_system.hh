/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___file_system_hh___included
#define lestes__lang__cplus__lex___file_system_hh___included

/*! \file
  \brief File system binding.
  
  Declaration of file_system class providing access to the file system.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/std/vector.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

// forward declaration to break cycle
class named_istream;

/*!
  \brief File system binding.
 
  Provides acces to the file system.
  Enables opening file streams and include path search.
*/
class file_system: public ::lestes::std::object {
public:
	//! The type of name for classification.
	typedef enum {
		//! Invalid name.
		NM_INVALID,
		//! Relative file name.
		NM_FILE_REL,
		//! Absolute file name.
		NM_FILE_ABS,
		//! Relative path.
		NM_PATH_REL,
		//! Absolute path.
		NM_PATH_ABS
	} name_type;
	//! Opens a file stream, using only working directory.
	ptr<named_istream> open_file(const lstring &name);
	//! Opens a file stream, searching given paths.
	ptr<named_istream> find_file(const lstring &path, const lstring &name, bool system);
#if 0   
	//! Opens a file stream, searching given paths.
	ptr<named_stream> find_file(const ptr<named_stream> &a_parent, const lstring &name, bool system);
#endif   
	//! Adds search path.
	bool add_search_path(const lstring &path);
	//! Returns new instance.
	static ptr<file_system> create(void);
	//! Classifies path and file name.
	static name_type classify_name(const lstring &name);
protected:
	//! Creates the object.
	file_system(void);
	//! Marks the object.
	virtual void gc_mark(void);
private:
	//! Type of sequence of strings, used for search path list.
	typedef ::lestes::std::vector<lstring> lstrings_type;
	//! Splits string into parts.
	static ptr< lstrings_type > split(const lstring &str, hchar delimiter);
	//! System search paths.
	srp<lstrings_type> search_paths;
	//! Hides copy constructor.
	file_system(const file_system &);
	//! Hides assignment operator.
	file_system &operator=(const file_system &);
};

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */

