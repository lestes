/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Integration with flexer.
  
  Definition of pp_lex class wrapping pp_lex_guts flexer.
  \author pt
*/

#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/pp_lex.hh>
#include <lestes/lang/cplus/lex/pp_lex.m.hh>
#include <lestes/lang/cplus/lex/pp_token.hh>
#include <lestes/lang/cplus/lex/pre_lex.hh>
#include <lestes/lang/cplus/lex/token_value.hh>
#include <lestes/lang/cplus/lex/ucn_token_buffer.hh>
#include <lestes/lang/cplus/lex/line_control.hh>
#include <lestes/lang/cplus/lex/lex_loggers.hh>
#include <lestes/std/source_location.hh>
#include <lestes/std/file_info.hh>

// for pp_lex_guts
#include <sstream>
#include <ostream>
#include <iomanip>
#include <cctype>

package(lestes);
package(lang);
package(cplus);
package(lex);

// include the flex generated parser in own separate namespace
package(pp_lex_guts);
#include <lestes/lang/cplus/lex/pp_lex_guts.yy.cc>
end_package(pp_lex_guts);

using namespace ::std;

/*!
  Creates the object, allocates flex buffer.  
  \pre a_input != NULL
  \pre a_lines != NULL
  \post interactive == true
  \param a_input  The input stream of tokens.
  \param a_lines  The line control.
*/
pp_lex::pp_lex(const ptr<pre_lex> &a_input, const ptr<line_control> &a_lines):
	interactive(true),
	input(checked(a_input)),
	physical(NULL),
	lines(checked(a_lines)),
	buffer(ucn_token_buffer::create(lines)),
	errors(ucn_token_buffer::create(lines)),
	yy_buffer(pp_lex_guts::yy_new_buffer(NULL,YY_BUF_SIZE))
{
}

/*!
  Finalizes the object, releases flex buffer.
*/
pp_lex::~pp_lex(void)
{
	pp_lex_guts::yy_delete_buffer(yy_buffer);
}

/*!
  Prepares pp_lex_guts to work with this pp_lex.
  Sets pp_lex_guts shortcut variables current_utb and current_ppl.
*/
void pp_lex::activate(void)
{
	pp_lex_guts::current_utb = buffer.pointer_get();
	pp_lex_guts::current_ppl = this;
	yy_switch_to_buffer(yy_buffer);
}

/*!
  Saves location of current input token.
*/
void pp_lex::location_save(void)
{
	physical = buffer->peek_front()->location_get();
}

/*!
  Creates location from saved token, file information and line number correction.
  \pre location_save() was already called.
  \return New location with correct logical line.
*/
ptr<source_location> pp_lex::location_create(void) const
{
	lassert2(physical,"location_save() was not called yet");
	return lines->translate_location(physical);
}

/*!
  Reads next token from flex.
  \param line_start  Whether reading at start of line.
  \return  The token read from flex.
*/
ptr<pp_token> pp_lex::read(bool line_start)
{
	pp_lex_logger << "pp_lex::read()\n" << msg::eolog;

	pp_lex_logger << "errors length == " << errors->length() << '\n' << msg::eolog;
	while (errors->length()) {
		ptr<ucn_token> tok = errors->peek_front();
		errors->advance(1);

		pp_lex_logger << "handling error from pre_lex\n" << msg::eolog;
		pp_lex_logger << "error == " << tok->error_get() << msg::eolog;

		report->report(tok->error_get(),lines->translate_location(tok->location_get()));
	}

	// this allows concurrent use of pp_lex_guts by different preprocessor instances
	if (pp_lex_guts::current_ppl != this) activate();

	ptr<pp_token> t = pp_lex_guts::lexer_parse(line_start);

	pp_lex_logger << "returning " << t->description_get() << "\n" << msg::eolog;
	pp_lex_logger << "pp_lex::read() end\n" << msg::eolog;

	return t;
}

/*!
  Reads token from input stream, stores it into buffer.
  \return The character representing the class of the token on input.
*/
pp_lex::char_type pp_lex::read_char(void)
{
	pp_lex_logger << "pp_lex::read_char()\n" << msg::eolog;

	ptr<ucn_token> tok = input->read();
	ucn_token_type utt = tok->type_get();
	
	while (utt == ucn_token::TOK_ERROR) {
		pp_lex_logger << "cummulating error\n" << msg::eolog;

		// cummulate errors
		errors->add_back(tok);

		tok = input->read();
		utt = tok->type_get();
	}
		
	// add the regular token
	buffer->add_back(tok);

	char_type c;
	switch (utt) {
		case ucn_token::TOK_BASIC:
			c = static_cast<char_type>(character::extract_value(tok->value_get()));
			break;
		case ucn_token::TOK_EOF:
			// special character class
			c = static_cast<char_type>(ucn_token::TOK_EOF);
			break;
		case ucn_token::TOK_TRANSLATED:
			// special character class
			c = static_cast<char_type>(ucn_token::TOK_TRANSLATED);
			break;
		default:
			lassert2(false,"You should never get here");
	}

	pp_lex_logger << "returning " << static_cast<ulint>(c) << "\n" << msg::eolog;
	pp_lex_logger << "pp_lex::read_char() end\n" << msg::eolog;
	return c;
}

/*!
  Fills buffer with characters from current source file.
  Called through YY_INPUT flex macro.
  \param cbuf  The buffer to fill.
  \param max  Maximum count of characters to fill.  
  \return  The actual count of filled characters.
*/
pp_lex::size_type pp_lex::yy_input(char_type *cbuf, size_type max)
{
	pp_lex_logger << "pp_lex::yy_input()\n" << msg::eolog;

	// force the parser interactive
	if (interactive_get()) max = 1;

	// initialize not to interfere with the condition
	char_type c = ucn_token::TOK_NOT_EOF;
	size_type cnt = 0;
	
	// put all tokens up to max (including TOK_EOF) into buffer
	for (cnt = 0; cnt < max && c != ucn_token::TOK_EOF; cnt++) {
		*cbuf++ = c = read_char();
	}

	pp_lex_logger << "pp_lex::yy_input() end\n" << msg::eolog;

	return cnt;
}

/*!
  Sets interactive flag.
  \param a_interactive  The new flag.
*/
void pp_lex::interactive_set(bool a_interactive)
{
	interactive = a_interactive;
}

/*!
  Returns interactive flag.
  \return The flag.
*/
bool pp_lex::interactive_get(void) const
{
	return interactive;
}

/*!
  Marks the object.
*/
void pp_lex::gc_mark(void)
{
	input.gc_mark();
	physical.gc_mark();
	lines.gc_mark();
	buffer.gc_mark();
	errors.gc_mark();
	::lestes::std::object::gc_mark();
}

/*!
  Returns new instance of the object.
  \pre a_input != NULL
  \pre a_lines != NULL
  \param a_input  The input stream of tokens.
  \param a_lines  The line control.
  \return The new instance with own flexer buffer.
*/
ptr<pp_lex> pp_lex::create(const ptr<pre_lex> &a_input, const ptr<line_control> &a_lines)
{
	return new pp_lex(a_input,a_lines);
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);
/* vim: set ft=lestes : */
