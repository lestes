/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Macro parameter list.
  
  Definition of macro_head class representing list of macro parameters.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/macro_head.hh>
#include <lestes/lang/cplus/lex/macro_head.m.hh>
#include <lestes/lang/cplus/lex/token_value.hh>
#include <lestes/lang/cplus/lex/token_sequence.hh>
#include <lestes/lang/cplus/lex/pp_token.hh>

#include <iostream>

package(lestes);
package(lang);
package(cplus);
package(lex);

using namespace ::std;

/*!
  Constructs empty parameter list.
  \post state == BEGIN
  \post length() == 0
  \post pars->size() == 0
*/
macro_head::macro_head(void):
	state(BEGIN),
	pars(pars_type::create())
{
}

/*!
  Parses macro parameter list.
  \pre state == BEGIN
  \pre input != NULL
  \param input  The source for tokens, starting after left parenthesis.
  \return false  In case of duplicate parameters or unterminated parameter list.
*/
bool macro_head::parse(const ptr<token_input> &input)
{
	lassert(state_get() == BEGIN);
	lassert(input);

	ptr<pp_token> t;
	pp_token_type ptt;
  
	t = input->read_front_skip_ws();

	// function like macro with no parameters
	if (t->type_get() == pp_token::TOK_RIGHT_PAR) {
		state = PARSED;
		return true;
	}

	// first parameter is handled separately
	if (!t->is_name()) {
		// expected macro parameter name identifier
		report << expected_macro_parameter << t->location_get();
		state = DEAD;
		return false;
	}

	// first cannot be duplicate
	add_param(t->value_get());

	// read until terminating `)'
	while (t = input->read_front_skip_ws(), (ptt = t->type_get()) != pp_token::TOK_RIGHT_PAR) {
		
		if (ptt != pp_token::TOK_COMMA) {
			if (ptt == pp_token::TOK_LINE_END) {
				// unterminated macro parameter list
				report << expected_right_par << t->location_get();
			} else if (t->is_name()) {
				// macro parameters must be comma separated
				report << expected_comma << t->location_get();
			} else {
				// invalid token in macro parameter list
				report << expected_comma_right_par << t->spelling_get() << t->location_get();
			}
			state = DEAD;
			return false;
		}
		
		t = input->read_front_skip_ws();
		if (!t->is_name()) {
			// expected parameter name identifier
			report << expected_macro_parameter << t->location_get();
			state = DEAD;
			return false;
		}

		if (!add_param(t->value_get())) {
			// duplicate macro parameter
			report << duplicate_macro_parameter << t->spelling_get() << t->location_get();
			state = DEAD;
			return false;
		}
	}

	state = PARSED;
	return true;
}

/*!
  Adds parameter to list.
  \pre a_value != NULL
  \param  a_value  Value to be added as parameter.
  \return false  If the value was already present in the list.
*/
bool macro_head::add_param(const ptr<token_value> &a_value)
{
	lassert(a_value);
	if (pars->insert(make_pair(a_value,pars->size())).second == false) return false;
	return true;
}

/*!
  Returns index of parameter in list.
  \pre state == PARSED
  \pre a_value != NULL
  \return The index of the parameter, length() if parameter is not present.
*/
ulint macro_head::index_of(const ptr<token_value> &a_value) const
{
	lassert(state_get() == PARSED);
	lassert(a_value);
	pars_type::const_iterator it = pars->find(a_value);
	if (it == pars->end()) return pars->size();
	return (*it).second;
}

/*!
  Returns length of the parameter list, which is also a special return value in index_of method.
  \pre state == PARSED
  \return Length of the parameter list.
*/
ulint macro_head::length(void) const
{
	lassert(state_get() == PARSED);
	return pars->size();
}

/*!
  Returns internal state of the object.
  \return  The internal state of the object.
*/
macro_head::state_type macro_head::state_get(void) const
{
	return state;
}

/*!
  Tests equality to other macro head.
  \param other  The macro head to compare with.
  \return true  If both parameter lists have the same length and spelling.
*/
bool macro_head::equals(const ptr<macro_head> &other) const
{
	::std::cerr << "macro_head::equals\n";
	if (!other || state != other->state_get()) return false;
	::std::cerr << "state ok\n";
	if (state != PARSED) return true;
	ulint len = length();
	if (len != other->length()) return false;
	::std::cerr << "length ok\n";
	
	for (pars_type::iterator it = pars->begin(); it != pars->end(); ++it) {
		ptr<token_value> tv = (*it).first;
		ulint idx = (*it).second;

		if (other->index_of(tv) != idx) return false;
		::std::cerr << "par " << idx << " ok\n";
	}
	
	return true;
}

/*!
  Marks the object.
*/
void macro_head::gc_mark(void)
{
	pars.gc_mark();
	::lestes::std::object::gc_mark();
}

/*!
  Returns empty macro head.
  \post state == BEGIN
  \return A new empty instance of macro head.
*/
ptr<macro_head> macro_head::create(void)
{
	return new macro_head();
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);
/* vim: set ft=lestes : */
