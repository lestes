/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Host chacter set encoder.

  Declaration of encoder_host class performing host character set encoding.
  \author pt
*/

#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/encoder_host.hh>
// TODO
// #include lestes/lang/cplus/lex/encoder_host.lmd>
#include <lestes/msg/message.hh>

// TODO#include <iostream>

package(lestes);
package(lang);
package(cplus);
package(lex);

using namespace ::std;

/*!
  Creates the encoder.
*/
encoder_host::encoder_host(void)
{
}

/*!
  Reads next token. Performs encoding from host character set.
  Checks whether the input contains valid characters, for which the encoding into ucn is known.
  If error is encountered, it returns token with type ucn_token::TOK_ERROR.
  \pre  Input into the filter is set.
  \return  The next token encoded from host character.
  \return  Token with type ucn_token::TOK_ERROR if the source character is invalid (out of range).
  \return  Token with type ucn_token::TOK_EOF in case of previous error.
*/
ptr<ucn_token> encoder_host::read(void)
{
	// TODO remove return the eof token in case of previous error if (bad) return bad;
	
	ptr<ucn_token> t = input_read();
	ucn u = t->value_get();
	ucn_token_type utt = t->type_get();

	if (utt == ucn_token::TOK_NOT_EOF) {
		// check that the character is can be converted to internal
		if (character::is_encodable_host(u)) {
			ulint x = character::extract_value(u);
			// encode value in source charset
			// TODO t->value_set(character::create_from_host(static_cast<hchar>(x)));
			t = t->clone_value(character::create_from_host(static_cast<hchar>(x))); 
		} else {
			// TODO report error: value out of range for ASCII
			//t->type_set(ucn_token::TOK_ERROR);
			// TODO pt remove set flag for next calls
			//bad = ucn_token::create(ucn_token::TOK_EOF);
			// TODO pt
			t = ucn_token::create_error(::lestes::msg::message::create(0xdada,"TODO encoder_host",::lestes::msg::message::FLG_ERROR));
		}
	}

	return t;
}

/*!
  Marks the object.
*/
void encoder_host::gc_mark(void)
{
	encoder::gc_mark();
}

/*!
  Returns new instance of the encoder.
  \return  The new instance.
*/
ptr<encoder_host> encoder_host::create(void)
{
	return new encoder_host();
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

/* vim: set ft=lestes : */
