/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___expander_hh___included
#define lestes__lang__cplus__lex___expander_hh___included

/*! \file
  \brief Macro expander.
  
  Declaration of expander class performing macro expansion.
  \author pt
*/
#include <lestes/common.hh>
package(lestes);
package(lang);
package(cplus);
package(lex);

// forward declaration to break cycle
class token_sequence;
class macro_storage;
class pp_filter;
class token_stream;

/*!
  \brief Macro expander.

  Performs reading of possibly expanded lines of tokens.
*/
class expander: public ::lestes::std::object {
public:
	//! The modes of input.
	typedef enum {
		//! Normal mode.
		NORMAL,
		//! Directive.
		DIRECTIVE,
		//! End of file.
		FILE_END
	} mode_type;
	//! Reads line from input.
	ptr<token_sequence> read_line(void);
	//! Reads expanded line from input.
	ptr<token_sequence> read_expanded(void);
	//! Returns input mode.
	mode_type mode_get(void);
	//! Returns new expander.
	static ptr<expander> create(const ptr<pp_filter> &a_input, const ptr<macro_storage> &a_macros);
protected:
	//! Creates new expander.
	expander(const ptr<pp_filter> &a_input, const ptr<macro_storage> &a_macros);
	//! Marks the object.
	virtual void gc_mark(void);
private:
	//! The macro storage for expansion.
	srp<macro_storage> macros;
	//! The stream connected to input.
	srp<token_stream> stream;
};

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);
#endif
/* vim: set ft=lestes : */
