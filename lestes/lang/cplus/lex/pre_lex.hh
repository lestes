/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___pre_lex_hh___included
#define lestes__lang__cplus__lex___pre_lex_hh___included

/*! \file
  \brief Character level processing.

  Declaration of pre_lex class performing operations before lexical analysis.
  \author pt
*/

#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/ucn_token.hh>
#include <lestes/lang/cplus/lex/data_source.hh>
#include <lestes/lang/cplus/lex/encoder.hh>
#include <lestes/lang/cplus/lex/special_tokens.hh>
#include <lestes/lang/cplus/lex/line_numbers.hh>
#include <lestes/lang/cplus/lex/trigraphs.hh>
#include <lestes/lang/cplus/lex/line_join.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

/*!
  \brief Character level processing.
  
  Performs operations done prior to lexical analysis.
*/
class pre_lex: public ::lestes::std::object {
public:
	//! Reads single token.
	ptr<ucn_token> read(void);
	//! Returns new instance, initializes with data source and encoder.
	static ptr<pre_lex> create(const ptr<data_source> &a_src, const ptr<encoder> &a_enc);
protected:
	//! Creates new object, initializes with data source and encoder.
	pre_lex(const ptr<data_source> &a_src, const ptr<encoder> &a_enc);
	//! Marks the object.
	virtual void gc_mark(void);
private:
	//! Connect all filters.
	void connect(void);
	//! Type of state.
	typedef enum {
		//! Starting state.
		START,
		//! Continuing state.
		CONT,
		//! Newline was read.
		NEWLINE,
		//! Error was returned.
		ERROR,
		//! EOF was read.
		END,
	} state_type;
	//! State of the object.
	state_type state;
	//! Saved token.
	srp<ucn_token> saved;
	//! Data source.
	srp<data_source> src;
	//! Source encoder.
	srp<encoder> enc;
	//! Special tokens translator.
	srp<special_tokens> stok;
	//! Line numbering.
	srp<line_numbers> lnum;
	//! Trigraph translator.
	srp<trigraphs> trig;
	//! Line joining.
	srp<line_join> ljoin;
	//! Hides copy constructor.
	pre_lex(const pre_lex &copy);
	//! Hides assignment operator.
	pre_lex &operator=(const pre_lex &rhs);
};

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
