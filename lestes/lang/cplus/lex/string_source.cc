/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief String data source.

  Definition of string_source class representing data source reading from string.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/string_source.hh>
package(lestes);
package(lang);
package(cplus);
package(lex);

using namespace ::std;

/*!
  Creates the object, initializes with string.
  \post offset == 0
  \post idx = = 0
  \param a_str  The string with data.
*/
string_source::string_source(const string_type &a_str):
	str(a_str),
	idx(0)
{
}

/*!
  Reads next token from string.
  \return  The next character.
*/
ptr<ucn_token> string_source::read(void)
{
	ptr<ucn_token> t;

	if (idx >= str.length()) {
		t = ucn_token::create(ucn_token::TOK_EOF,character::create_external(0));
	} else {
		// note that the meaning of real types is not clear at this stage
		hchar c = str.at(idx);
		idx++;
		// do not interpret the encoding yet
		t = ucn_token::create(ucn_token::TOK_NOT_EOF,character::create_external(static_cast<unsigned char>(c)));
	}
  
	return t;
}

/*!
  Returns new instance, initializes with string.
  \param a_str  The source string.
  \return  The new instance.
*/
ptr<string_source> string_source::create(const string_type &a_str)
{
	return new string_source(a_str);
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

/* vim: set ft=lestes : */
