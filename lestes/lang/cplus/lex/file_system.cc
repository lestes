/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief File system binding.
  
  Definition of file_system class providing access to the file system.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/std/list.hh>
#include <lestes/std/istream_wrapper.hh>
#include <lestes/lang/cplus/lex/file_system.hh>
#include <lestes/lang/cplus/lex/named_istream.hh>

#include <fstream>
#include <iostream>

package(lestes);
package(lang);
package(cplus);
package(lex);

using namespace ::std;

/*!
  Creates the object.
*/
file_system::file_system(void):
	search_paths(lstrings_type::create())
{
	// an extra path starting from base
	search_paths->push_back(lstring(""));
}

/*!
  Splits string into sequence of parts separated by delimiter.
  If delimiter is found at the beginning, or at the end of the string,
  or two delimiters are adjacent, an empty string is added to the appropriate place.
  So empty string results in empty sequence, while string containing sole
  delimiter results in sequence containing two empty strings.
  \param str  The string to split.
  \param delimiter  The delimiter to separate the parts.
  \return  The sequence of parts, in the original order, without the delimiters.
*/
ptr< file_system::lstrings_type > file_system::split(const lstring &str, hchar delimiter)
{
	ptr< lstrings_type > v = lstrings_type::create();
	
	lstring::size_type len = str.length();
	
	// no data means no output
	if (!len) return v;
	
	lstring::size_type i, j;

	i = 0;
	
	// the condition is set so that find is called even after
	// the delimiter was found at the end of the string
	while (i <= len) {
		j = str.find(delimiter,i);
		if (j == lstring::npos)
			// pretend delimiter just after the end
			j = len;

		// the part starts at i and ends at j - 1
		v->push_back(str.substr(i,j-i));
		// skip the delimiter
		i = j + 1;
	}

	return v;
}

/*!
  Classifies the name according to its properties.
  Distinguishes relative and absolute path and file names.
  \param str  The string to classify.
  \return  The kind of name represented by the string.
*/
file_system::name_type file_system::classify_name(const lstring &name)
{
	ptr<lstrings_type> parts = split(name,'/');
	ulint len = parts->size();
	
	if (!len) return NM_PATH_REL;

	bool absolute = parts->at(0).length() == 0;
	bool pathname = parts->at(len - 1).length() == 0;
	
	// check nonempty directory names
	for (ulint i = 1; i < len - 1; i++) {
		if (parts->at(i).length() == 0)
			return NM_INVALID;
	}

	if (absolute) {
		return pathname ? NM_PATH_ABS : NM_FILE_ABS;
	}
	  
	return pathname ? NM_PATH_REL : NM_FILE_REL;
}

/*!
  Opens a stream for the given file name or standard input.
  Relative file name is searched for in the working directory.
  \param name  Absolute or relative file name, "" used for stdin.
  \return  The opened stream or NULL for error (file not found or not accessible).
*/
ptr<named_istream> file_system::open_file(const lstring &name)
{
	if (name.length() == 0) {
		ptr<istream_wrapper> iw = istream_wrapper::create(&::std::cin,false);
		return named_istream::create(iw,"","<stdin>");
	}
	
	name_type nt = classify_name(name);
	
	if (nt != NM_FILE_REL && nt != NM_FILE_ABS)
		return NULL;
	
	fstream *f = new fstream();
	bool error = false;
	try {
		f->open(name.c_str(),ios_base::in);
		error = f->fail();
	} catch (...) {
		error = true;
	}

	if (error) {
		delete f;
		return NULL;
	}

	ptr<istream_wrapper> iw = istream_wrapper::create(f,true);
	
	return named_istream::create(iw,name,name);
}

/*
fstream *named_istream::open_fstream(const lstring &full_name)
{
	
}
*/
/* 
  TODO pt different signature
	
	!
  Opens a stream for a given file name.
  Relative file name is searched for using the given paths and parent path.
  \param a_parent  The parent stream providing additional search path information. 
  \param name  Absolute or relative file name.
  \param system  Flag set to true for system header.
  \return  The opened stream or NULL fo error (file not found or not accessible).
*/
//ptr<named_stream> file_system::find_file(const ptr<named_stream> &a_parent, const lstring &name, bool system)




/*!
  Opens a stream for the given file name.
  Relative names are searched for in search paths if system is set to true, otherwise 
  in the given path, and in the search paths in this order.
  \param base_path  Absolute or relative path for the search.
  \param name  Absolute or relative file name of the file to open.
  \param system  Flag set to true for system header.
  \return  The opened stream or NULL for error (invalid name, file not found or not accessible).
*/
ptr<named_istream> file_system::find_file(const lstring &base_path, const lstring &name, bool system)
{
	name_type nt = classify_name(base_path);
	lassert(nt == NM_PATH_REL || nt == NM_PATH_ABS);
	
	nt = classify_name(name);
	
	if (nt == NM_FILE_ABS)
		return open_file(name);

	if (nt != NM_FILE_REL)
		return NULL;
	
	lstrings_type::iterator it = search_paths->begin(), end = search_paths->end();
	
	if (system) {
		// skip the placeholder
		++it;
	} else {
		// extra entry to enable uniform processing
		(*search_paths)[0] = base_path;
	}
	
	fstream *f = new fstream();
	
	for ( ; it != end; ++it) {
		
		lstring full_name(*it);
		full_name.append(name);

		bool error = false;

		try {
			f->open(full_name.c_str(),ios_base::in);
			error = f->fail();
		} catch (...) {
			error = true;
		}

		if (!error) {
			ptr<istream_wrapper> iw = istream_wrapper::create(f,true);
			return named_istream::create(iw,full_name,full_name);
		}
	}
	
	// the resource is not in use
	delete f;

	// failure
	return NULL;
}

/*!
  Adds search path for file searching.
  \param a_path  A path to add.
  \return false  If the path is invalid.
*/
bool file_system::add_search_path(const lstring &a_path)
{
	name_type nt = classify_name(a_path);
	lstring path(a_path); 
	
	switch (nt) {
		case NM_INVALID:
			return false;
		case NM_FILE_REL:
		case NM_FILE_ABS:
			path.append("/");
			// fall through
		case NM_PATH_REL:
		case NM_PATH_ABS:
			search_paths->push_back(path);
			break;
		default:
			lassert2(false,"You should never get here");
	}
	return true;
}

/*!
  Marks the object.
*/
void file_system::gc_mark(void)
{
	search_paths.gc_mark();
	::lestes::std::object::gc_mark();
}

/*!
  Returns new instance.
  \return  The new instance.
*/
ptr<file_system> file_system::create(void)
{
	return new file_system();
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

/* vim: set ft=lestes : */
