/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  Unit test for class encoder_utf8.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/special_tokens.hh>
#include <lestes/lang/cplus/lex/string_source.hh>
#include <lestes/lang/cplus/lex/encoder_utf8.hh>
#include <lestes/std/file_info.hh>
#include <lestes/std/source_location.hh>
#include <string>

package(lestes);
package(lang);
package(cplus);
package(lex);

using namespace ::std;

#define TEST_CNT 47

void encoder_utf8_test(void)
{
	char *in[TEST_CNT] = {
		
			/* disallowed 1st */
			"\x80"
		,
			/* disallowed 1st */
			"\xC0"
		,
			/* disallowed 1st */
			"\xF5"
		,
			/* missing 2nd */
			"\xC2"
		,
			/* disallowed 2nd */
			"\xC2"
			"\x7F"
		,
			/* disallowed 2nd */
			"\xC2"
			"\xC0"
		,
			/* missing 2nd */
			"\xE0"
		,
			/* missing 3rd */
			"\xE0"
			"\xA0"
		,
			/* disallowed 2nd */
			"\xE0"
			"\x9F"
			"\x80"
		,
			/* disallowed 2nd */
			"\xE0"
			"\xC0"
			"\x80"
		,
			/* disallowed 3rd */
			"\xE0"
			"\xA0"
			"\x7F"
		,
			/* disallowed 3rd */
			"\xE0"
			"\xA0"
			"\xC0"
		,
			/* disallowed 2nd */
			"\xE1"
			"\x7F"
			"\x80"
		,
			/* disallowed 2nd */
			"\xE1"
			"\xC0"
			"\x80"
		,
			/* disallowed 3rd */
			"\xE1"
			"\x80"
			"\x7F"
		,
			/* disallowed 3rd */
			"\xE1"
			"\x80"
			"\xC0"
		,
			/* disallowed 2nd */
			"\xED"
			"\x7F"
			"\x80"
		,
			/* disallowed 2nd */
			"\xED"
			"\xA0"
			"\x80"
		,
			/* disallowed 3rd */
			"\xED"
			"\x80"
			"\x7F"
		,
			/* disallowed 3rd */
			"\xED"
			"\x80"
			"\xC0"
		,
			/* disallowed 2nd */
			"\xEE"
			"\x7F"
			"\x80"
		,
			/* disallowed 2nd */
			"\xEE"
			"\xC0"
			"\x80"
		,
			/* disallowed 3rd */
			"\xEE"
			"\x80"
			"\x7F"
		,
			/* disallowed 3rd */
			"\xEE"
			"\x80"
			"\xC0"
		,
			/* missing 2nd */
			"\xF0"
		,
			/* missing 3rd */
			"\xF0"
			"\x90"
		,
			/* missing 4th */
			"\xF0"
			"\x90"
			"\x80"
		,
			/* disallowed 2nd */
			"\xF0"
			"\x8F"
			"\x80"
			"\x80"
		,
			/* disallowed 2nd */
			"\xF0"
			"\xC0"
			"\x80"
			"\x80"
		,
			/* disallowed 3rd */
			"\xF0"
			"\x90"
			"\x7F"
			"\x80"
		,
			/* disallowed 3rd */
			"\xF0"
			"\x90"
			"\xC0"
			"\x80"
		,
			/* disallowed 4th */
			"\xF0"
			"\x90"
			"\x80"
			"\x7F"
		,
			/* disallowed 4th */
			"\xF0"
			"\x90"
			"\x80"
			"\xC0"
		,
			/* disallowed 2nd */
			"\xF1"
			"\x7F"
			"\x80"
			"\x80"
		,
			/* disallowed 2nd */
			"\xF1"
			"\xC0"
			"\x80"
			"\x80"
		,
			/* disallowed 3rd */
			"\xF1"
			"\x80"
			"\x7F"
			"\x80"
		,
			/* disallowed 3rd */
			"\xF1"
			"\x80"
			"\xC0"
			"\x80"
		,
			/* disallowed 4th */
			"\xF1"
			"\x80"
			"\x80"
			"\x7F"
		,
			/* disallowed 4th */
			"\xF1"
			"\x80"
			"\x80"
			"\xC0"
		,
			/* disallowed 2nd */
			"\xF4"
			"\x7F"
			"\x80"
			"\x80"
		,
			/* disallowed 2nd */
			"\xF4"
			"\x90"
			"\x80"
			"\x80"
		,
			/* disallowed 3rd */
			"\xF4"
			"\x80"
			"\x7F"
			"\x80"
		,
			/* disallowed 3rd */
			"\xF4"
			"\x80"
			"\xC0"
			"\x80"
		,
			/* disallowed 4th */
			"\xF4"
			"\x80"
			"\x80"
			"\x7F"
		,
			/* disallowed 4th */
			"\xF4"
			"\x80"
			"\x80"
			"\xC0"
		,
			/* ordinary characters */
			"a"
			"z"
		,
			/* Czech characters */
			"\xC3\xA1"
			"\xC4\x8D"
			"\xC4\x8F"
			"\xC3\xA9"
			"\xC4\x9B"
			"\xC3\xAD"
			"\xC5\x88"
			"\xC3\xB3"
			"\xC5\x99"
			"\xC5\xA1"
			"\xC5\xA5"
			"\xC3\xBA"
			"\xC5\xAF"
			"\xC5\xBE"
	};

	ucn_token_type out[] = {
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_NOT_EOF,
		ucn_token::TOK_NOT_EOF,
		ucn_token::TOK_EOF,
		ucn_token::TOK_NOT_EOF,
		ucn_token::TOK_NOT_EOF,
		ucn_token::TOK_NOT_EOF,
		ucn_token::TOK_NOT_EOF,
		ucn_token::TOK_NOT_EOF,
		ucn_token::TOK_NOT_EOF,
		ucn_token::TOK_NOT_EOF,
		ucn_token::TOK_NOT_EOF,
		ucn_token::TOK_NOT_EOF,
		ucn_token::TOK_NOT_EOF,
		ucn_token::TOK_NOT_EOF,
		ucn_token::TOK_NOT_EOF,
		ucn_token::TOK_NOT_EOF,
		ucn_token::TOK_NOT_EOF,
		ucn_token::TOK_EOF
	};

	ptr<file_info> fi = file_info::create(string("abc"),NULL);
	ptr<ucn_token> tok;
	ucn_token_type utt;
	ulint test, i;
	
	for (i = test = 0; test < TEST_CNT; test++) {
		ptr<data_source> ds = string_source::create(string_source::string_type(in[test]));
		ptr<encoder_utf8> enc = encoder_utf8::create();

		enc->input_set(ds);
				
		while (true) {
			tok = enc->read();
			utt = tok->type_get();
			lassert(utt == out[i]);
			i++;
			if (utt == ucn_token::TOK_EOF || utt == ucn_token::TOK_ERROR) break;
		}
	}
}
end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

int main(void)
{
	::lestes::lang::cplus::lex::encoder_utf8_test();
	return 0;
}
/* vim: set ft=lestes : */
