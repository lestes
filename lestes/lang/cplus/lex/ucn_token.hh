/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___ucn_token_hh___included
#define lestes__lang__cplus__lex___ucn_token_hh___included

/*! \file
  \brief Single character token.
  
  Declaration of ucn_token class representing single source character token.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/simple_location.hh>
#include <lestes/lang/cplus/lex/basic_token.hh>
#include <lestes/msg/message.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

//! Helper typedef.
typedef basic_token<lc_host_uint_least9,ptr<simple_location>,ucn> basic_ucn_token;

/*!
  \brief Single source character token.
  
  Represents single source character token.
  For character translation and other processing before lex phase.
*/
class ucn_token: public basic_ucn_token {
public:
	//! The type of stored error message.
	typedef ptr< ::lestes::msg::message > error_type;
	//! Returns error for token.
	error_type error_get(void) const;
	//! Returns copy of token with changed type.
	ptr<ucn_token> clone_type(const type_type &a_type) const;
	//! Returns copy of token with changed value.
	ptr<ucn_token> clone_value(const value_type &a_value) const;
	//! Returns copy of token with changed location.
	ptr<ucn_token> clone_location(const location_type &a_location) const;
	//! Returns new token, initializes with token type.
	static ptr<ucn_token> create(const type_type &a_type);
	//! Returns new token, initializes with token type and token value.
	static ptr<ucn_token> create(const type_type &a_type, const value_type &a_value);
	//! Returns new token, initializes with location, token type and value.
	static ptr<ucn_token> create(const type_type &a_type, const value_type &a_value,
			const location_type &a_location);
	//! Returns new error token, initializes with error and location.
	static ptr<ucn_token> create_error(const error_type &a_error, const location_type &a_location);
	//! Returns new error token, initializes with the error.
	static ptr<ucn_token> create_error(const error_type &a_error);
	//! Tests equality.
	bool equals(const ptr<ucn_token> &rhs) const;
	//! Token type constants.
	enum token_types {
		//! End of file.
		TOK_EOF = 0,
		//! Anything which is not basic source character.
		TOK_TRANSLATED = 1,
		//! Any character in early stages of processing.
		TOK_NOT_EOF = 2,
		//! Basic source character.
		TOK_BASIC = 3,
		//! Token for propagation of messages.
		TOK_ERROR = 4
	};
protected:
	//! Creates the object, initializes with location, type and value.
	ucn_token(const location_type &a_location, const type_type &a_type, const value_type &a_value);
	//! Creates the object, initializes with location and error.
	ucn_token(const location_type &a_location, const error_type &a_error);
	//! Marks the object.
	virtual void gc_mark(void);
private:
	//! Message to report for token.
	srp< ::lestes::msg::message > error;
	//! Hides copy constructor.
	ucn_token(const ucn_token &);
	//! Hides assignment operator.
	ucn_token &operator=(const ucn_token &);
};

//! Type of token in ucn_token.
typedef ucn_token::type_type ucn_token_type;

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);
#endif
/* vim: set ft=lestes : */
