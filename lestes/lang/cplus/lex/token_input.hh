/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___token_input_hh___included
#define lestes__lang__cplus__lex___token_input_hh___included

/*! \file
  \brief Abstract input of tokens.

	Declaration of token_input class representing abstract input of tokens.
	\author pt
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/pp_filter.hh>

package(lestes);

package(std);
// forward declarations to avoid cycle
class source_location;
end_package(std);

package(lang);
package(cplus);
package(lex);

// forward declaration to break cycle
class pp_token;
class token_sequence;
class macro_storage;

/*!
  \brief Abstract input of tokens.

  Represents abstract input of tokens, which also behaves as pp_filter.
  Provides sequential access at front and expansion of lines.
*/
class token_input: public pp_filter {
public:
	//! Returns front token.
	virtual ptr<pp_token> peek_front(void) abstract;
	//! Reads front token, squeezing whitespace.
	virtual ptr<pp_token> read_front(void) abstract;
	//! Reads front token, skipping front whitespace, but not newline.
	virtual ptr<pp_token> read_front_skip_ws(void) abstract;
	//! Skips front whitespace.
	virtual bool skip_front_ws(void) abstract;
protected:   
	//! Creates object, initializes with no input.
	inline token_input(void);
	//! Creates object, initializes with input.
	inline token_input(const ptr<pp_filter> &a_input);
private:
	//! Hides copy constructor.
	token_input(const token_input &);
	//! Hides assignment operator.
	token_input &operator=(const token_input &);
};

/*!
  Creates the object, initializes with no input.
  Useful when the data is stored directly in the object.
*/
inline token_input::token_input(void):
	pp_filter()
{
}

/*!
  Creates the object, initializes with input.
  \pre a_input != NULL
  \param a_input  The input to read from.
*/
inline token_input::token_input(const ptr<pp_filter> &a_input):
	pp_filter(checked(a_input))
{
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);
#endif
/* vim: set ft=lestes : */
