/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  Unit test for class ucn_token_buffer.
  \author pt
*/

#include <lestes/common.hh>
#include <lestes/equality.hh>
#include <lestes/lang/cplus/lex/ucn_token.hh>
#include <lestes/lang/cplus/lex/ucn_token_buffer.hh>
#include <lestes/lang/cplus/lex/token_value.hh>
#include <string>

package(lestes);
package(lang);
package(cplus);
package(lex);

using namespace ::std;

#define u(x) character::create_from_host(x)

void ucn_token_buffer_test(void)
{
	ptr<ucn_token> a = ucn_token::create(0,u(':'));
	ptr<ucn_token> b = ucn_token::create(0,u('h'));
	
	ptr<ucn_token_buffer> cb = ucn_token_buffer::create();

	cb->add_back(a);
	lassert(is_equal(cb->peek_front(),a));
	cb->add_back(b);
	lassert(is_equal(cb->peek_front(),a));
	cb->add_back(ucn_token::create(0,u('e')));

	cb->advance(1);
	lassert(is_equal(cb->peek_front(),b));

	cb->add_back(ucn_token::create(0,u('l')));
	cb->add_back(ucn_token::create(0,u('l')));
	cb->add_back(ucn_token::create(0,u('o')));

	ptr<token_value> tv = cb->extract_ordinary(5);
	ucn_string text(tv->content_get());
	
	for (ulint i = 0; i < 6; i++) {
	  // cerr << "text " << text[i] << " string " << (int)("hello"[i]) << endl;
		lassert(is_equal(text[i],static_cast<ucn>("hello"[i])));
	}
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

int main(void)
{
	::lestes::lang::cplus::lex::ucn_token_buffer_test();
	return 0;
}
/* vim: set ft=lestes : */
