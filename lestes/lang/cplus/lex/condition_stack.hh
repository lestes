/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___condition_stack_hh___included
#define lestes__lang__cplus__lex___condition_stack_hh___included

/*! \file
  \brief Conditional directives stack.

  Declaration of condition class representing stack of
  preprocessor conditonal directives.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/std/vector.hh>
package(lestes);

package(std);
// forward declaration to break cycle
class source_location;
end_package(std);

package(lang);
package(cplus);
package(lex);

// forward declaration to break cycle
class condition;

/*!
  \brief Conditional directive stack.

  Represents stack of conditional directives.
  Holds information in stack of unfinished directives.
*/
class condition_stack: public ::lestes::std::object {
public:
	//! Types of directives.
	typedef enum {
		//! Directive #if.
		DIR_IF = 1,
		//! Directive #ifdef.
		DIR_IFDEF = 2,
		//! Directive #ifndef.
		DIR_IFNDEF = 3,
		//! Directive #elif.
		DIR_ELIF = 4,
		//! Directive #else.
		DIR_ELSE = 5,
		//! Directive #endif.
		DIR_ENDIF = 6,
		//! End of file.
		DIR_EOF = 7
	} directive_type;
	//! Tests if the output is active.
	bool active_get(void) const;
	//! Processes a directive.
	bool process(directive_type a_dir, bool value, const ptr<source_location> &a_location);
	//! Returns depth of the stack.
	ulint depth(void) const;
	//! Returns new object.
	static ptr<condition_stack> create(void);
protected:
	//! Creates new object.
	condition_stack(void);
	//! Marks the object.
	virtual void gc_mark(void);
private:   
	//! Conditions stack type.
	typedef ::lestes::std::vector< srp<condition> > cstack_type;
	//! Output activity flag.
	bool active;
	//! Conditions stack.
	srp<cstack_type> cstack;
};

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
