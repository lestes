/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
	Definiton of token_value class representing values of tokens.
	\author pt
*/
#include <lestes/common.hh>
#include <lestes/equality.hh>
#include <lestes/lang/cplus/lex/token_value.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

/*!
  Constructs object, initializes with a_content.
  \post is_equal(content,a_content) 
  \param a_content  The initialization value.
*/
token_value::token_value(const content_type &a_content):
	content(a_content)
{
}


/*!
  Returns new token value, initializes with a_content.
  \param a_content  The initialization value.
*/
ptr<token_value> token_value::create(const content_type &a_content)
{
	// singleton-like factory
	shared_type::iterator it = shared->find(a_content);
	if (it != shared->end()) return (*it).second;
	ptr<token_value> nju = new token_value(a_content);
	shared->insert(shared_type::value_type(a_content,nju));
	return nju;
	// TODO pt remove return ptr<token_value>(new token_value(a_content));
}

/*!
  Tests equality. Because of sharing instances, only pointers are compared.
  \param other  The token value to compare to.
  \return true  If both contents are equal.
*/
bool token_value::equals(const ptr<token_value> &other) const
{
	return other == this;
}

/*!
  Returns content of value object.
  \return The content of the object.
*/
token_value::content_type token_value::content_get(void) const
{
	return content;
}

/*!
  Returns hash value of content.
  \return The hash value.
*/
ulint token_value::hash(void) const
{
	// TODO compute hash from content
	return 0;
}

/*!
  Compares to other token value. The content is compared with < operator.
  \param other  The token value to compare.
  \return true  If the other is not null and the content compares less.
*/
bool token_value::less_than(const ptr<token_value> &other) const
{
	return other && is_less(content_get(),other->content_get());
}

/*!
  Compares two token values. Null is less than everything else.
  Otherwise content is compared with < operator.
  \param left  The left operand for the comparator.
  \param right  The right operand for the comparator.
  \return true  If only left operand is null or left->less_than(right).
*/
bool token_value::compare_less::operator()(const ptr<token_value> &left, const ptr<token_value> &right) const
{
	return is_less(left,right);
}

/*!
  Compares two token value contents with < operator.
  \param left  The left operand for the comparator.
  \param right  The right operand for the comparator.
  \return true  If left operand < right operand.
*/
bool token_value::compare_content::operator()(const content_type &left, const content_type &right) const
{
	return is_less(left,right);
}

/*!
  Structure for sharing instances of token value having the same content.
*/
ptr<token_value::shared_type> token_value::shared = token_value::shared_type::create();

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);
/* vim: set ft=lestes : */
