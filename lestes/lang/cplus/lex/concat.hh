/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___concat_hh___included
#define lestes__lang__cplus__lex___concat_hh___included

/*! \file
  \brief Token concatenator.
  
  Declaration of concat class wrapping concat_guts flexer.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/std/ucn_string.hh>
#include <lestes/lang/cplus/lex/ucn_token.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

package(concat_guts);
//! Forward declaration of flex buffer type.
struct yy_buffer_state;
end_package(concat_guts);

// forward declaration to avoid cycle
class token_sequence;
class pp_token;

/*!
  \brief Token concatenator.
  
  Performs token concatenation.
  Handles integration with concat_guts flexer.
*/
class concat: public ::lestes::std::object {
public:
	//! Type of character in flex.
	typedef char char_type;
	//! Type of size in flex.
	typedef int size_type;
	//! Finalizes object.
	virtual ~concat(void);
	//! Stores next characters for YY_INPUT.
	size_type yy_input(char_type *cbuf, size_type max);
	//! Performs token concatenation.
	ptr<token_sequence> process(const ptr<pp_token> &left, const ptr<pp_token> &right);
	//! Returns the only instance.
	static ptr<concat> instance(void);
protected:
	//! Creates the object.
	concat(void);
	//! Marks the object.
	virtual void gc_mark(void);
private:
	//! Type of flex buffer.
	typedef struct concat_guts::yy_buffer_state *yy_buffer_type;
	//! Flex buffer.
	yy_buffer_type yy_buffer;
	//! Current value of input string.
	ucn_string value;
	//! Current index in the input.
	ucn_string::size_type index;
	//! Current length of the input.
	ucn_string::size_type length;
	//! Hides copy constructor.
	concat(const concat &copy);
	//! Hides assignment operator.
	concat &operator=(const concat &rhs);
	//! The only instance.
	static ptr<concat> singleton;
};

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
