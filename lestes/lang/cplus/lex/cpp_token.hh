/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___cpp_token_hh___included
#define lestes__lang__cplus__lex___cpp_token_hh___included

/*! \file
  \brief C++ token.
  
  Declaration of cpp_token class representing C++ token.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/basic_token.hh>
#include <lestes/std/map.hh>

package(lestes);
package(std);
// forward declaration to avoid cycle
class source_location;
end_package(std);
package(lang);
package(cplus);
package(lex);

// forward declaration to avoid cycle
class lex_literal;
class token_value;

//! Helper typedef.
typedef basic_token<int,ptr<source_location>,ptr<token_value> > basic_cpp_token;

/*!
  \brief C++ token.
  
  Represents C++ token, which is passed to syntactical analysis.
*/
class cpp_token: public basic_cpp_token {
public:
	//! The type of literal properties.
	typedef ptr<lex_literal> literal_type;
	// enum of token types
#include <lestes/lang/cplus/syn/cpp_token_type_enum.s.hh>
	//! Tests equality.
	bool equals(const ptr<cpp_token> &rhs) const;
	//! Returns lex literal properties.
	ptr<lex_literal> literal_get(void) const;
	//! Returns token description.
	lstring description_get(void) const;
	//! Returns new token, initializes with token type.
	static ptr<cpp_token> create(const location_type &a_location, const type_type &a_type);
	//! Returns new token, initializes with token type and value.
	static ptr<cpp_token> create(const location_type &a_location,
			const type_type &a_type, const value_type &a_value);
	//! Returns new literal token, initializes with literal properties and value.
	static ptr<cpp_token> create_literal(const location_type &a_location,
			const literal_type &a_literal, const value_type &a_value);
	//! Returns list of reflection info.
	virtual ptr<reflection_list> reflection_get(void) const;
	//! Returns list of field values.
	virtual ptr<field_list_list> field_values_get(void) const;
protected:
	//! Creates the object.
	cpp_token(const location_type &a_location, const type_type &a_type, const value_type &a_value,
			const literal_type &a_literal);
	//! Marks the object.
	virtual void gc_mark(void);
private:
	//! Type of descriptions map.
	typedef map<type_type,lstring> descriptions_type;
	//! Literal information for semantic analysis.
	srp<lex_literal> literal;
	//! Token descriptions.
	static ptr<descriptions_type> descriptions;
	//! The reflection information.
	static ptr<reflection_list> reflection;
	//! Hides copy constructor.
	cpp_token(const cpp_token &);
	//! Hides assignment operator.
	cpp_token &operator=(const cpp_token &);
};

//! Type of token in cpp_token.
typedef cpp_token::type_type cpp_token_type;

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);
#endif
/* vim: set ft=lestes : */
