/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Part of translation unit.

  Definition of unit_part class representing one part of the translation unit.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/std/file_info.hh>
#include <lestes/lang/cplus/lex/unit_part.hh>
#include <lestes/lang/cplus/lex/pp_token.hh>
#include <lestes/lang/cplus/lex/encoder.hh>
#include <lestes/lang/cplus/lex/pre_lex.hh>
#include <lestes/lang/cplus/lex/data_source.hh>
#include <lestes/lang/cplus/lex/pp_lex.hh>
#include <lestes/lang/cplus/lex/condition_stack.hh>
#include <lestes/lang/cplus/lex/macro_storage.hh>
#include <lestes/lang/cplus/lex/expander.hh>
#include <lestes/lang/cplus/lex/line_control.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

using namespace ::std;

/*!
  Creates the object.
  \pre a_file != NULL
  \pre a_data != NULL
  \pre a_encoder != NULL
  \pre a_macros != NULL
  \param a_file  The file information.
  \param a_data  The data source.
  \param a_encoder  The encoder for the source.
  \param a_macros  The defined macros.
*/
unit_part::unit_part(const ptr<file_info> &a_file, const ptr<data_source> &a_data,
		const ptr<encoder> &a_encoder, const ptr<macro_storage> &a_macros):
	start(false),
	conditions(condition_stack::create()),
	enc(checked(a_encoder)),
	ds(checked(a_data)),
	plx(pre_lex::create(ds,enc)),
	lic(line_control::create(checked(a_file))),
	ppl(pp_lex::create(plx,lic)),
	exp(expander::create(this,checked(a_macros)))
{
}

// TODO pt remove
#if 0
void unit_part::activate(void)
{
	ppl->activate();
}
#endif

/*!
  Sets start of line flag to help pp_lex switch to read tokens within include.
  The flag is cleared automatically after first call to read().
*/
void unit_part::start_of_line(void)
{
	start = true;
}

/*!
  Returns associated condition stack.
  \return The condition stack.
*/
ptr<condition_stack> unit_part::conditions_get(void) const
{
	return conditions;
}

/*!
  Returns the associated expander.
  \return The expander connected to this unit part.
*/
ptr<expander> unit_part::expander_get(void) const
{
	return exp;
}

// TODO pt keep ??
/*!
  Returns the associated line control.
  \return  The line control.
*/
ptr<line_control> unit_part::line_control_get(void) const
{
	return lic;
}

// TODO pt remove
/*!
  Performs updates triggered by #line directive.
  \param file_name  The new name of the current file.
  \param a_line  The new line number of the next line.
void unit_part::line_change(lstring file_name, ulint a_line)
{
	// TODO pt rename ^^^^^^^^
	// TODO pt call the new style
	//ppl->line_change(file_name,a_line);
	//ptr<line_control> lines = ppl->lines_get();
	//lines->change_line(location,a_line);
	//lines->change_file(file_name);
}
*/

/*!
  Reads next token from the unit part.
  Updates token location by the relative location.
  \return  The next token in the unit part.
*/
ptr<pp_token> unit_part::read(void)
{
	// TODO pt check and emit errors, with changed locations
	// if (ppl->has_errors()) {
	// ppl->errors_get();
	// process
	// }
	
	ptr<pp_token> tok = ppl->read(start);
	start = false;

	// TODO pt update location
	// what the hell ???
	// this is obsolete

	return tok;
}

/*!
  Marks the object.
*/
void unit_part::gc_mark(void)
{
	conditions.gc_mark();
	enc.gc_mark();
	ds.gc_mark();
	plx.gc_mark();
	lic.gc_mark();
	ppl.gc_mark();
	exp.gc_mark();
	pp_filter::gc_mark();
}

/*!
  Returns new instance, initializes with file information, stream and encoding.
  \param a_file  The file information for the unit part.
  \param a_data  The data source.
  \param a_encoder  The encoder for the source.
  \param a_macros  The defined macros.
  \return New instance of the class.
*/
ptr<unit_part> unit_part::create(const ptr<file_info> &a_file,
		const ptr<data_source> &a_data, const ptr<encoder> &a_encoder,
		const ptr<macro_storage> &a_macros)
{
	return new unit_part(a_file,a_data,a_encoder,a_macros);
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

/* vim: set ft=lestes : */
