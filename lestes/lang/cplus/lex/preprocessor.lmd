<?xml version="1.0" encoding="UTF-8"?>
<!--
	The lestes compiler suite
	Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
	Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
	Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
	Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
	Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
	Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
	Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; version 2 of the License.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	See the full text of the GNU General Public License version 2, and
	the limitations in the file doc/LICENSE.

	By accepting the license the licensee waives any and all claims
	against the copyright holder(s) related in whole or in part to the
	work, its use, and/or the inability to use it.
-->
<lmd xmlns="http://lestes.jikos.cz/schemas/lmd" xmlns:h="http://www.w3.org/TR/REC-html40">
	<file-name>preprocessor</file-name>

	<dox file="both">
		<bri>
			Messages for preprocessor to C++ token transformation.
		</bri>
		<det>
			Definition of warnings and errors issued for token transformation and
         numeric literal parsing.
		</det>
	</dox>

	<packages>
		<p>lestes</p>
		<p>lang</p>
		<p>cplus</p>
		<p>lex</p>
	</packages>
	
	<imports>
	</imports>

	<implementation-imports>
	</implementation-imports>

	<error name="invalid_character_in_number">
		<dox>
			<bri>Error message for number parsing.</bri>
			<det>
				Issued when a number literal contains invalid character (ucn or underscore).
			</det>
		</dox>
		<text>invalid character in number</text>
	</error>

	<error name="floating_exponent_empty">
		<dox>
			<bri>Error message for number parsing.</bri>
			<det>
				Issued when `e' in floating number literal is not followed by digits.
			</det>
		</dox>
		<text>floating literal exponent with no digits</text>
	</error>

	<error name="floating_suffix_invalid">
		<dox>
			<bri>Error message for number parsing.</bri>
			<det>
				Issued when floating number literal suffix is not one of `f', `F', `l', `L'.
			</det>
		</dox>
		<text>floating literal with invalid suffix</text>
	</error>

	<error name="integral_suffix_invalid">
		<dox>
			<bri>Error message for number parsing.</bri>
			<det>
				Issued when integral number literal suffix is not a proper
				combination of `u', `U', `l', `L'.
			</det>
		</dox>
		<text>integral literal with invalid suffix</text>
	</error>

	<error name="integral_empty">
		<dox>
			<bri>Error message for number parsing.</bri>
			<det>
				Issued when integral number (hexadecimal) literal has no digits.
			</det>
		</dox>
		<text>integral literal with no digits</text>
	</error>

	<error name="invalid_cpp_token">
		<dox>
			<bri>Error message for token conversion.</bri>
			<det>
				Issued when a token which has no meaning
				outside preprocessor would be emitted.
			</det>
		</dox>
		<text>cannot convert `%0' to C++ token</text>
		<param type="ucn_string" />
	</error>

	<error name="stray_character">
		<dox>
			<bri>Error message for token conversion.</bri>
			<det>
				Issued when encountering a token holding a character, which does not
				fall into any token category would be emitted.
			</det>
		</dox>
		<text>stray character `%0'</text>
		<param type="ucn_string" />
	</error>

</lmd>
