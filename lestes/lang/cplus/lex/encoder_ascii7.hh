/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___encoder_ascii7_hh___included
#define lestes__lang__cplus__lex___encoder_ascii7_hh___included

/*! \file
  \brief 7 bit ASCII encoder.
  
  Declaration of encoder_ascii7 class performing 7 bit ASCII character set encoding.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/encoder.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

// forward declaration to break cycle
class ucn_token;

/*!
  \brief 7 bit ASCII encoder.
  
  Performs encoding from 7 bit ASCII character set into internal source character set.
  Expects the input filter to return external characters in ASCII 7 bit,
  which are encoded into internal representation.
*/
class encoder_ascii7: public encoder {
public:
	//! Reads next token.
	ptr<ucn_token> read(void);
	//! Returns new instance.
	static ptr<encoder_ascii7> create(void);
protected:
	//! Creates the object.
	encoder_ascii7(void);
	//! Marks the object.
	virtual void gc_mark(void);
private:
	//! Flag of correctness of the stream and also eof token.
	srp<ucn_token> bad;
	//! Hides copy constructor.
	encoder_ascii7(const encoder_ascii7 &);
	//! Hides assignment operator.
	encoder_ascii7 &operator=(const encoder_ascii7 &);
};

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
