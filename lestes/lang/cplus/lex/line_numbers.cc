/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Token numbering filter.

  Definition of line_numbers class performing token numbering.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/line_numbers.hh>
#include <lestes/lang/cplus/lex/ucn_filter.hh>
#include <lestes/lang/cplus/lex/simple_location.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

/*!
  Creates the object, initializes with first line and column.
  \post line == 1
  \post column == 1
*/
line_numbers::line_numbers(void):
	line(1),
	column(1)
{
}

/*!
  Reads next token, assigns line and column numbers.
  The line number is relative to the physical start of the file.
  It does not reflect the #line directives yet.
*/
ptr<ucn_token> line_numbers::read(void)
{
	ptr<simple_location> loc = simple_location::create(line,column);
	ptr<ucn_token> t = input_read();
	ucn_token_type utt = t->type_get();
	t = t->clone_location(loc);

	switch (utt) {
		case ucn_token::TOK_BASIC:
			// line end
			if (t->value_get() == character::ascii_new_line) {
				// reset column
				column = 1;
				// update line
				line++;
				break;
			}
			// fall through
		case ucn_token::TOK_TRANSLATED:
			// update column
			column++;
			break;
		case ucn_token::TOK_ERROR:
		case ucn_token::TOK_EOF:
			// no need to update line or column
			break;
		default:
			// TODO pt ICE
			lassert(false);
			break;
	}
	return t;
}

/*!
  Returns new instance.
  \return New instance of this class.
*/
ptr<line_numbers> line_numbers::create(void)
{
	return new line_numbers();
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

/* vim: set ft=lestes : */
