/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief C++ preprocessor. 
  
  Definition of preprocessor class representing the entire C++ preprocessor.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/preprocessor.hh>
#include <lestes/lang/cplus/lex/preprocessor.m.hh>
#include <lestes/lang/cplus/lex/cpp_token.hh>
#include <lestes/lang/cplus/lex/evaluator.hh>
#include <lestes/lang/cplus/lex/string_joiner.hh>
#include <lestes/lang/cplus/lex/space_remover.hh>
#include <lestes/lang/cplus/lex/lex_literal.g.hh>
#include <lestes/lang/cplus/lex/file_system.hh>
#include <lestes/lang/cplus/lex/string_translator.hh>
#include <lestes/lang/cplus/lex/token_value.hh>
#include <lestes/lang/cplus/lex/lex_loggers.hh>
#include <lestes/msg/logger.hh>
#include <lestes/msg/logger_util.hh>
#include <lestes/std/source_location.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

/*!
  Creates new preprocessor.
  \pre a_fs != NULL
  \param a_fs  The file system binding.
  \param file_name  The full name of the file to process.
*/
preprocessor::preprocessor(const ptr<file_system> &a_fs, const lstring &file_name):
	order(1),
	fs(checked(a_fs)),
	eva(evaluator::create(a_fs,file_name)),
	spr(space_remover::create(eva)),
	str(string_translator::create(spr)),
	stj(string_joiner::create(str))
{
}

/*!
  Returns pragma flag.
  \return true  If #pragma lestes was encountered.
*/
bool preprocessor::pragma_flag_get(void) const
{
	return eva->pragma_flag_get();
}

/*!
  Reads next token from the translation unit.
  Converts pp_token to cpp_token, reports error if not possible.
  Extracts additional information for literals.
  \return  The next token in translation unit.
*/
ptr<cpp_token> preprocessor::read(void)
{
	preprocessor_logger << "preprocessor::read()\n" << msg::eolog;

	ptr<pp_token> t;
	ptr<source_location> loc;
	ptr<token_value> val;
	bool wide;
	ptr<lex_literal> lit;
	ptr<cpp_token> ct;

	while (!ct) {
		t = stj->read();
		loc = t->location_get();
		// add proper order
		loc = loc->clone_order(order++);
		val = t->value_get();
		wide = false;

		switch (t->type_get()) {
			case pp_token::TOK_FILE_END:
				ct = cpp_token::create(loc,cpp_token::TOK_EOF);
				break;
			case pp_token::TOK_WSTRING_LIT:
				wide = true;
				// fall through
			case pp_token::TOK_STRING_LIT:
				ct = cpp_token::create_literal(loc,lex_string_literal::create(wide),val);
				break;
			case pp_token::TOK_WCHAR_LIT:
				wide = true;
				// fall through
			case pp_token::TOK_CHAR_LIT:
				ct = cpp_token::create_literal(loc,lex_character_literal::create(wide,val->content_get().length() > 1),val);
				break;
			case pp_token::TOK_NUMBER_LIT:
				preprocessor_logger << "number literal\n" << msg::eolog;
				lit = classify_number(loc,val);
				
				if (!lit) {
					preprocessor_logger << "the literal is illformed\n" << msg::eolog;
					// create fake zero
					lit = lex_integral_literal::create(lex_integral_literal::OCTAL,lex_integral_literal::NONE,0,1);
					val = token_value::create("0");
				}

				ct = cpp_token::create_literal(loc,lit,val);
				// error already reported
				break;
			case pp_token::TOK_BOOL_LIT:
				ct = cpp_token::create_literal(loc,lex_boolean_literal::create(),val);
				break;
				
			case pp_token::TOK_LEFT_BRACKET:
				ct = cpp_token::create(loc,cpp_token::TOK_LEFT_BRACKET);
				break;
			case pp_token::TOK_RIGHT_BRACKET:
				ct = cpp_token::create(loc,cpp_token::TOK_RIGHT_BRACKET);
				break;
			case pp_token::TOK_LEFT_BRACE:
				ct = cpp_token::create(loc,cpp_token::TOK_LEFT_BRACE);
				break;
			case pp_token::TOK_RIGHT_BRACE:
				ct = cpp_token::create(loc,cpp_token::TOK_RIGHT_BRACE);
				break;
			case pp_token::TOK_LEFT_PAR:
				ct = cpp_token::create(loc,cpp_token::TOK_LEFT_PAR);
				break;
			case pp_token::TOK_RIGHT_PAR:
				ct = cpp_token::create(loc,cpp_token::TOK_RIGHT_PAR);
				break;
			case pp_token::TOK_SEMICOLON:
				ct = cpp_token::create(loc,cpp_token::TOK_SEMICOLON);
				break;
			case pp_token::TOK_COLON:
				ct = cpp_token::create(loc,cpp_token::TOK_COLON);
				break;
			case pp_token::TOK_QMARK:
				ct = cpp_token::create(loc,cpp_token::TOK_QMARK);
				break;
			case pp_token::TOK_DOT:
				ct = cpp_token::create(loc,cpp_token::TOK_DOT);
				break;
			case pp_token::TOK_PLUS:
				ct = cpp_token::create(loc,cpp_token::TOK_PLUS);
				break;
			case pp_token::TOK_STAR:
				ct = cpp_token::create(loc,cpp_token::TOK_STAR);
				break;
			case pp_token::TOK_PERCENT:
				ct = cpp_token::create(loc,cpp_token::TOK_PERCENT);
				break;
			case pp_token::TOK_SLASH:
				ct = cpp_token::create(loc,cpp_token::TOK_SLASH);
				break;
			case pp_token::TOK_HAT:
				ct = cpp_token::create(loc,cpp_token::TOK_HAT);
				break;
			case pp_token::TOK_AMP:
				ct = cpp_token::create(loc,cpp_token::TOK_AMP);
				break;
			case pp_token::TOK_VBAR:
				ct = cpp_token::create(loc,cpp_token::TOK_VBAR);
				break;
			case pp_token::TOK_TILDE:
				ct = cpp_token::create(loc,cpp_token::TOK_TILDE);
				break;
			case pp_token::TOK_EMARK:
				ct = cpp_token::create(loc,cpp_token::TOK_EXCLAMATION);
				break;
			case pp_token::TOK_EQ:
				ct = cpp_token::create(loc,cpp_token::TOK_EQ);
				break;
			case pp_token::TOK_LT:
				ct = cpp_token::create(loc,cpp_token::TOK_LT);
				break;
			case pp_token::TOK_GT:
				ct = cpp_token::create(loc,cpp_token::TOK_GT);
				break;
			case pp_token::TOK_COMMA:
				ct = cpp_token::create(loc,cpp_token::TOK_COMMA);
				break;
			case pp_token::TOK_MINUS:
				ct = cpp_token::create(loc,cpp_token::TOK_MINUS);
				break;
			case pp_token::TOK_DOT_DOT_DOT:
				ct = cpp_token::create(loc,cpp_token::TOK_DOT_DOT_DOT);
				break;
			case pp_token::TOK_COLON_COLON:
				ct = cpp_token::create(loc,cpp_token::TOK_COLON_COLON);
				break;
			case pp_token::TOK_DOT_STAR:
				ct = cpp_token::create(loc,cpp_token::TOK_DOT_STAR);
				break;
			case pp_token::TOK_PLUS_EQ:
				ct = cpp_token::create(loc,cpp_token::TOK_PLUS_EQ);
				break;
			case pp_token::TOK_MINUS_EQ:
				ct = cpp_token::create(loc,cpp_token::TOK_MINUS_EQ);
				break;
			case pp_token::TOK_STAR_EQ:
				ct = cpp_token::create(loc,cpp_token::TOK_STAR_EQ);
				break;
			case pp_token::TOK_SLASH_EQ:
				ct = cpp_token::create(loc,cpp_token::TOK_SLASH_EQ);
				break;
			case pp_token::TOK_PERCENT_EQ:
				ct = cpp_token::create(loc,cpp_token::TOK_PERCENT_EQ);
				break;
			case pp_token::TOK_HAT_EQ:
				ct = cpp_token::create(loc,cpp_token::TOK_HAT_EQ);
				break;
			case pp_token::TOK_AMP_EQ:
				ct = cpp_token::create(loc,cpp_token::TOK_AMP_EQ);
				break;
			case pp_token::TOK_VBAR_EQ:
				ct = cpp_token::create(loc,cpp_token::TOK_VBAR_EQ);
				break;
			case pp_token::TOK_LT_LT:
				ct = cpp_token::create(loc,cpp_token::TOK_LT_LT);
				break;
			case pp_token::TOK_GT_GT:
				ct = cpp_token::create(loc,cpp_token::TOK_GT_GT);
				break;
			case pp_token::TOK_LT_LT_EQ:
				ct = cpp_token::create(loc,cpp_token::TOK_LT_LT_EQ);
				break;
			case pp_token::TOK_GT_GT_EQ:
				ct = cpp_token::create(loc,cpp_token::TOK_GT_GT_EQ);
				break;
			case pp_token::TOK_EQ_EQ:
				ct = cpp_token::create(loc,cpp_token::TOK_EQ_EQ);
				break;
			case pp_token::TOK_EMARK_EQ:
				ct = cpp_token::create(loc,cpp_token::TOK_EXCLAMATION_EQ);
				break;
			case pp_token::TOK_LT_EQ:
				ct = cpp_token::create(loc,cpp_token::TOK_LT_EQ);
				break;
			case pp_token::TOK_GT_EQ:
				ct = cpp_token::create(loc,cpp_token::TOK_GT_EQ);
				break;
			case pp_token::TOK_AMP_AMP:
				ct = cpp_token::create(loc,cpp_token::TOK_AMP_AMP);
				break;
			case pp_token::TOK_VBAR_VBAR:
				ct = cpp_token::create(loc,cpp_token::TOK_VBAR_VBAR);
				break;
			case pp_token::TOK_PLUS_PLUS:
				ct = cpp_token::create(loc,cpp_token::TOK_PLUS_PLUS);
				break;
			case pp_token::TOK_MINUS_MINUS:
				ct = cpp_token::create(loc,cpp_token::TOK_MINUS_MINUS);
				break;
			case pp_token::TOK_MINUS_GT_STAR:
				ct = cpp_token::create(loc,cpp_token::TOK_MINUS_GT_STAR);
				break;
			case pp_token::TOK_MINUS_GT:
				ct = cpp_token::create(loc,cpp_token::TOK_MINUS_GT);
				break;
			case pp_token::TOK_ASM:
				ct = cpp_token::create(loc,cpp_token::TOK_ASM);
				break;
			case pp_token::TOK_AUTO:
				ct = cpp_token::create(loc,cpp_token::TOK_AUTO);
				break;
			case pp_token::TOK_BOOL:
				ct = cpp_token::create(loc,cpp_token::TOK_BOOL);
				break;
			case pp_token::TOK_BREAK:
				ct = cpp_token::create(loc,cpp_token::TOK_BREAK);
				break;
			case pp_token::TOK_CASE:
				ct = cpp_token::create(loc,cpp_token::TOK_CASE);
				break;
			case pp_token::TOK_CATCH:
				ct = cpp_token::create(loc,cpp_token::TOK_CATCH);
				break;
			case pp_token::TOK_CHAR:
				ct = cpp_token::create(loc,cpp_token::TOK_CHAR);
				break;
			case pp_token::TOK_CLASS:
				ct = cpp_token::create(loc,cpp_token::TOK_CLASS);
				break;
			case pp_token::TOK_CONST:
				ct = cpp_token::create(loc,cpp_token::TOK_CONST);
				break;
			case pp_token::TOK_CONST_CAST:
				ct = cpp_token::create(loc,cpp_token::TOK_CONST_CAST);
				break;
			case pp_token::TOK_CONTINUE:
				ct = cpp_token::create(loc,cpp_token::TOK_CONTINUE);
				break;
			case pp_token::TOK_DEFAULT:
				ct = cpp_token::create(loc,cpp_token::TOK_DEFAULT);
				break;
			case pp_token::TOK_DELETE:
				ct = cpp_token::create(loc,cpp_token::TOK_DELETE);
				break;
			case pp_token::TOK_DO:
				ct = cpp_token::create(loc,cpp_token::TOK_DO);
				break;
			case pp_token::TOK_DOUBLE:
				ct = cpp_token::create(loc,cpp_token::TOK_DOUBLE);
				break;
			case pp_token::TOK_DYNAMIC_CAST:
				ct = cpp_token::create(loc,cpp_token::TOK_DYNAMIC_CAST);
				break;
			case pp_token::TOK_ELSE:
				ct = cpp_token::create(loc,cpp_token::TOK_ELSE);
				break;
			case pp_token::TOK_ENUM:
				ct = cpp_token::create(loc,cpp_token::TOK_ENUM);
				break;
			case pp_token::TOK_EXPLICIT:
				ct = cpp_token::create(loc,cpp_token::TOK_EXPLICIT);
				break;
			case pp_token::TOK_EXPORT:
				ct = cpp_token::create(loc,cpp_token::TOK_EXPORT);
				break;
			case pp_token::TOK_EXTERN:
				ct = cpp_token::create(loc,cpp_token::TOK_EXTERN);
				break;
			case pp_token::TOK_FLOAT:
				ct = cpp_token::create(loc,cpp_token::TOK_FLOAT);
				break;
			case pp_token::TOK_FOR:
				ct = cpp_token::create(loc,cpp_token::TOK_FOR);
				break;
			case pp_token::TOK_FRIEND:
				ct = cpp_token::create(loc,cpp_token::TOK_FRIEND);
				break;
			case pp_token::TOK_GOTO:
				ct = cpp_token::create(loc,cpp_token::TOK_GOTO);
				break;
			case pp_token::TOK_IF:
				ct = cpp_token::create(loc,cpp_token::TOK_IF);
				break;
			case pp_token::TOK_INLINE:
				ct = cpp_token::create(loc,cpp_token::TOK_INLINE);
				break;
			case pp_token::TOK_INT:
				ct = cpp_token::create(loc,cpp_token::TOK_INT);
				break;
			case pp_token::TOK_LONG:
				ct = cpp_token::create(loc,cpp_token::TOK_LONG);
				break;
			case pp_token::TOK_MUTABLE:
				ct = cpp_token::create(loc,cpp_token::TOK_MUTABLE);
				break;
			case pp_token::TOK_NAMESPACE:
				ct = cpp_token::create(loc,cpp_token::TOK_NAMESPACE);
				break;
			case pp_token::TOK_NEW:
				ct = cpp_token::create(loc,cpp_token::TOK_NEW);
				break;
			case pp_token::TOK_OPERATOR:
				ct = cpp_token::create(loc,cpp_token::TOK_OPERATOR);
				break;
			case pp_token::TOK_PRIVATE:
				ct = cpp_token::create(loc,cpp_token::TOK_PRIVATE);
				break;
			case pp_token::TOK_PROTECTED:
				ct = cpp_token::create(loc,cpp_token::TOK_PROTECTED);
				break;
			case pp_token::TOK_PUBLIC:
				ct = cpp_token::create(loc,cpp_token::TOK_PUBLIC);
				break;
			case pp_token::TOK_REGISTER:
				ct = cpp_token::create(loc,cpp_token::TOK_REGISTER);
				break;
			case pp_token::TOK_REINTERPRET_CAST:
				ct = cpp_token::create(loc,cpp_token::TOK_REINTERPRET_CAST);
				break;
			case pp_token::TOK_RETURN:
				ct = cpp_token::create(loc,cpp_token::TOK_RETURN);
				break;
			case pp_token::TOK_SHORT:
				ct = cpp_token::create(loc,cpp_token::TOK_SHORT);
				break;
			case pp_token::TOK_SIGNED:
				ct = cpp_token::create(loc,cpp_token::TOK_SIGNED);
				break;
			case pp_token::TOK_SIZEOF:
				ct = cpp_token::create(loc,cpp_token::TOK_SIZEOF);
				break;
			case pp_token::TOK_STATIC:
				ct = cpp_token::create(loc,cpp_token::TOK_STATIC);
				break;
			case pp_token::TOK_STATIC_CAST:
				ct = cpp_token::create(loc,cpp_token::TOK_STATIC_CAST);
				break;
			case pp_token::TOK_STRUCT:
				ct = cpp_token::create(loc,cpp_token::TOK_STRUCT);
				break;
			case pp_token::TOK_SWITCH:
				ct = cpp_token::create(loc,cpp_token::TOK_SWITCH);
				break;
			case pp_token::TOK_TEMPLATE:
				ct = cpp_token::create(loc,cpp_token::TOK_TEMPLATE);
				break;
			case pp_token::TOK_THIS:
				ct = cpp_token::create(loc,cpp_token::TOK_THIS);
				break;
			case pp_token::TOK_THROW:
				ct = cpp_token::create(loc,cpp_token::TOK_THROW);
				break;
			case pp_token::TOK_TRY:
				ct = cpp_token::create(loc,cpp_token::TOK_TRY);
				break;
			case pp_token::TOK_TYPEDEF:
				ct = cpp_token::create(loc,cpp_token::TOK_TYPEDEF);
				break;
			case pp_token::TOK_TYPEID:
				ct = cpp_token::create(loc,cpp_token::TOK_TYPEID);
				break;
			case pp_token::TOK_TYPENAME:
				ct = cpp_token::create(loc,cpp_token::TOK_TYPENAME);
				break;
			case pp_token::TOK_UNION:
				ct = cpp_token::create(loc,cpp_token::TOK_UNION);
				break;
			case pp_token::TOK_UNSIGNED:
				ct = cpp_token::create(loc,cpp_token::TOK_UNSIGNED);
				break;
			case pp_token::TOK_USING:
				ct = cpp_token::create(loc,cpp_token::TOK_USING);
				break;
			case pp_token::TOK_VIRTUAL:
				ct = cpp_token::create(loc,cpp_token::TOK_VIRTUAL);
				break;
			case pp_token::TOK_VOID:
				ct = cpp_token::create(loc,cpp_token::TOK_VOID);
				break;
			case pp_token::TOK_VOLATILE:
				ct = cpp_token::create(loc,cpp_token::TOK_VOLATILE);
				break;
			case pp_token::TOK_WCHAR:
				ct = cpp_token::create(loc,cpp_token::TOK_WCHAR_T);
				break;
			case pp_token::TOK_WHILE:
				ct = cpp_token::create(loc,cpp_token::TOK_WHILE);
				break;
			case pp_token::TOK_IDENT:
				ct = cpp_token::create(loc,cpp_token::TOK_IDENT,val);
				break;
			case pp_token::TOK_HASH:
			case pp_token::TOK_HASH_HASH:
				// invalid token
				report << invalid_cpp_token << t->spelling_get() << loc;
				break;
			case pp_token::TOK_OTHER:
				// stray character
				report << stray_character << t->spelling_get() << loc;
				break;
			default:
				lassert2(false,"Unexpected token in conversion");
				break;
		}
	}

	preprocessor_logger << "returning " << static_cast<ulint>(ct->type_get()) << "\n" << msg::eolog;
	preprocessor_logger << "preprocessor::read() end\n" << msg::eolog;
	return ct;
}

/*!
  Attempts to parse preprocessing number. Returns literal properties.
  \pre loc != NULL
  \pre num != NULL
  \pre  The value matches preprocessing number.
  \param loc  The location of the number.
  \param num  The number to classify.
  \return The appropriate literal properties or NULL. 
*/
ptr<lex_literal> preprocessor::classify_number(const ptr<source_location> &loc, const ptr<token_value> &num)
{
	preprocessor_logger << "preprocessor::classify_number()\n" << msg::eolog;

	lassert(loc);
	lassert(num);

	// the states of the parser
	enum {
		BEGIN,
		ZERO,
		OCTAL,
		DECIMAL,
		HEXADECIMAL_START,
		HEXADECIMAL,
		FLOAT_INTEGRAL,
		FLOAT_FRACTION,
		FLOAT_EXPONENT_SIGN,
		FLOAT_EXPONENT_START,
		FLOAT_EXPONENT,
		FLOAT_SUFFIX,
		FLOAT_END,
		INTEGRAL_SUFFIX,
	} fstate = BEGIN;

	ucn_string str(num->content_get());
	ucn u;
	lex_integral_literal::base_type base = lex_integral_literal::OCTAL;
	bool long_suffix = false;
	bool unsigned_suffix = false;
	bool float_suffix = false;
	ptr<lex_literal> literal;

	for (ucn_string::iterator sit = str.begin(), send = str.end();
			sit != send; ) {
		u = *sit;

		preprocessor_logger << "fstate = " << fstate["bzodhHI.+eEfFu"] << ' ' << (ulint)u << '\n' << msg::eolog;

		if (!character::is_basic(u) || u == character::ascii_underscore) {
			// bad character in number
			report << invalid_character_in_number << loc;
			goto err;
		}
		switch (fstate) {
			case BEGIN:
				++sit;
				switch (u) {
					case character::ascii_digit_0:
						fstate = ZERO;
						break;
					case character::ascii_digit_1:
					case character::ascii_digit_2:
					case character::ascii_digit_3:
					case character::ascii_digit_4:
					case character::ascii_digit_5:
					case character::ascii_digit_6:
					case character::ascii_digit_7:
					case character::ascii_digit_8:
					case character::ascii_digit_9:
						fstate = DECIMAL;
						break;
					case character::ascii_dot:
						fstate = FLOAT_FRACTION;
						break;
					default:
						lassert2(false,"You should never get here");
				}
				break;
			case ZERO:
				if (u == character::ascii_lower_x ||
					 u == character::ascii_upper_x) {
					fstate = HEXADECIMAL_START;
					++sit;
				} else if (character::is_odigit(u)) {
					fstate = OCTAL;
				} else 
					fstate = INTEGRAL_SUFFIX;
				break;
			case OCTAL:
				if (character::is_odigit(u))
					++sit;
				else if (character::is_digit(u))
					fstate = FLOAT_INTEGRAL;
				else if (u == character::ascii_dot) {
					fstate = FLOAT_FRACTION;
					++sit;
				} else
					fstate = INTEGRAL_SUFFIX;
				break;
			case DECIMAL:
				if (character::is_digit(u))
					++sit;
				else if (u == character::ascii_dot) {
					++sit;
					fstate = FLOAT_FRACTION;
				} else if (u == character::ascii_lower_e ||
						u == character::ascii_upper_e) {
					++sit;
					fstate = FLOAT_EXPONENT_SIGN;
				} else
					fstate = INTEGRAL_SUFFIX;
				break;
			case HEXADECIMAL_START:
				if (character::is_xdigit(u)) {
					++sit;
					fstate = HEXADECIMAL;
					base = lex_integral_literal::HEXADECIMAL;
				} else
					fstate = INTEGRAL_SUFFIX;
				break;
			case HEXADECIMAL:
				if (character::is_xdigit(u))
					++sit;
				else
					fstate = INTEGRAL_SUFFIX;
				break;
			case FLOAT_INTEGRAL:
				if (character::is_digit(u))
					++sit;
				else if (u == character::ascii_dot) {
					++sit;
					fstate = FLOAT_FRACTION;
				} else if (u == character::ascii_lower_e ||
						u == character::ascii_upper_e) {
					++sit;
					fstate = FLOAT_EXPONENT_SIGN;
				} else
					fstate = FLOAT_SUFFIX;
				break;
			case FLOAT_FRACTION:
				if (character::is_digit(u))
					++sit;
				else if (u == character::ascii_lower_e ||
						u == character::ascii_upper_e) {
					++sit;
					fstate = FLOAT_EXPONENT_SIGN;
				} else
					fstate = FLOAT_SUFFIX;
				break;
			case FLOAT_EXPONENT_SIGN:
				if (character::is_digit(u)) {
					++sit;
					fstate = FLOAT_EXPONENT;
				} else if (u == character::ascii_plus) {
					++sit;
					fstate = FLOAT_EXPONENT_START;
				} else if (u == character::ascii_minus) {
					++sit;
					fstate = FLOAT_EXPONENT_START;
				} else {
					// no digits in exponent
					report << floating_exponent_empty << loc;
					goto err;
				}
				break;
			case FLOAT_EXPONENT_START:
				if (character::is_digit(u)) {
					++sit;
					fstate = FLOAT_EXPONENT;
				} else {
					// no digits in exponent
					report << floating_exponent_empty << loc;
					goto err;
				}
				break;
			case FLOAT_EXPONENT:
				if (character::is_digit(u))
					++sit;
				else {
					fstate = FLOAT_SUFFIX;
				}
				break;
			case FLOAT_SUFFIX:
				++sit;
				if (u == character::ascii_lower_f ||
					 u == character::ascii_upper_f) {
					float_suffix = true;
					fstate = FLOAT_END;
					break;
				} else if (u == character::ascii_lower_l ||
						u == character::ascii_upper_l) {
					long_suffix = true;
					fstate = FLOAT_END;
					break;
				} 
				// fall through
			case FLOAT_END:
				// bad float suffix
				report << floating_suffix_invalid << loc;
				goto err;
			case INTEGRAL_SUFFIX:
				++sit;
				if (u == character::ascii_lower_u ||
					 u == character::ascii_upper_u) {
					if (unsigned_suffix) {
						// bad integral suffix
						report << integral_suffix_invalid << loc;
						goto err;
					}
					unsigned_suffix = true;
				} else if (u == character::ascii_lower_l ||
						u == character::ascii_upper_l) {
					if (long_suffix) {
						// bad integral suffix
						report << integral_suffix_invalid << loc;
						goto err;
					}
					long_suffix = true;
				} else {
					// bad integral suffix
					report << integral_suffix_invalid << loc;
					goto err;
				}
				break;
			default:
				lassert2(false,"You should never get here");
				break;
		}
	}
		
	switch (fstate) {
		case ZERO:
		case OCTAL:
		case DECIMAL:
		case HEXADECIMAL:
		case INTEGRAL_SUFFIX:
		{
			ucn_string::size_type start = (base == lex_integral_literal::HEXADECIMAL) ? 2 : 0;
			ucn_string::size_type end = str.length();
			lex_integral_literal::suffix_type suffix = lex_integral_literal::NONE;

			if (unsigned_suffix) {
				if (long_suffix) {
					suffix = lex_integral_literal::UNSIGNED_LONG;
					end -= 2;
				} else {
					suffix = lex_integral_literal::UNSIGNED;
					end--;
				}
			} else if (long_suffix) {
				suffix = lex_integral_literal::LONG;
				end--;
			}

			preprocessor_logger << "integral su = " << (ulint)suffix <<  
				" s = " << start << " e = " << end << "\n" << msg::eolog;

			literal = lex_integral_literal::create(base,suffix,start,end);
			break;
		}
		case FLOAT_FRACTION:
		case FLOAT_EXPONENT:
		case FLOAT_SUFFIX:
		case FLOAT_END:
		{
			lex_floating_literal::suffix_type suffix = lex_floating_literal::NONE;
			if (long_suffix)
				suffix = lex_floating_literal::LONG;
			else if (float_suffix)
				suffix = lex_floating_literal::FLOAT;

			ucn_string::iterator sit = str.begin(), send = str.end();

			ucn_string::size_type is = 0, ie = is;
			
			while (sit != send && character::is_digit(*sit)) {
				++ie;
				++sit;
			}

			ucn_string::size_type fs = ie, fe = fs;
			if (sit != send && *sit == character::ascii_dot) {
				++sit;
				++fs;
				++fe;

				while (sit != send && character::is_digit(*sit)) {
					++fe;
					++sit;
				}
			}
			
			bool neg = false;
			ucn_string::size_type es = fe, ee = es;
			if (sit != send && (*sit == character::ascii_lower_e || *sit == character::ascii_upper_e)) {
				++sit;
				++es;
				++ee;

				if (sit != send && !character::is_digit(*sit)) {
					neg = *sit == character::ascii_minus;
					++sit;
					++es;
					++ee;
				}

				while (sit != send && character::is_digit(*sit)) {
					++ee;
					++sit;
				}
			}

			preprocessor_logger << "floating su = " << (ulint)suffix << " n = " << neg << 
				" is = " << is << " ie = " << ie << " fs = " << fs << " fe = " << fe <<
				" es = " << es << " ee = " << ee << "\n" << msg::eolog;

			literal = lex_floating_literal::create(suffix,neg,is,ie,fs,fe,es,ee);
		}
		break;
		case FLOAT_EXPONENT_SIGN:
		case FLOAT_EXPONENT_START:
			// float exponent with no digits
			report << floating_exponent_empty << loc;
			break;
		case HEXADECIMAL_START:
			// hexa with no digits
			report << integral_empty << loc;
			break;
		default:
			lassert2(false,"You should never get here");
	}
  
err:
	preprocessor_logger << "preprocessor::classify_number() end\n" << msg::eolog;

	return literal;
}

/*!
  Marks the object.
*/
void preprocessor::gc_mark(void)
{
	fs.gc_mark();
	eva.gc_mark();
	spr.gc_mark();
	str.gc_mark();
	stj.gc_mark();
	::lestes::std::object::gc_mark();
}

/*!
  Returns new preprocessor for given file.
  \param a_fs  The file system binding.
  \param file_name  The full name of the file to process.
*/
ptr<preprocessor> preprocessor::create(const ptr<file_system> &a_fs, const lstring &file_name)
{
	return new preprocessor(a_fs,file_name);
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

/* vim: set ft=lestes : */
