/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___preprocessor_hh___included
#define lestes__lang__cplus__lex___preprocessor_hh___included

/*! \file
  \brief C++ preprocessor. 
  
  Declaration of preprocessor class representing the entire C++ preprocessor.
  \author pt
*/
#include <lestes/common.hh>

package(lestes);

package(std);
// forward declaration to avoid cycle
class source_location;
end_package(std);

package(lang);
package(cplus);
package(lex);

// forward declaration to avoid cycle
class cpp_token;
class evaluator;
class string_joiner;
class string_translator;
class space_remover;
class lex_literal;
class file_system;
class cpp_token;
class token_value;

/*!
  \brief C++ preprocessor.
 
  \todo pt The initialization interface is going to change.
  Represents the entire C++ preprocessor.
  Handles conversion from pp_token to cpp_token.
*/
class preprocessor: public ::lestes::std::object {
public:
	//! Returns the pragma flag.
	bool pragma_flag_get(void) const;
	//! Reads next token.
	ptr<cpp_token> read(void);
	//! Returns new instance.
	static ptr<preprocessor> create(const ptr<file_system> &a_fs, const lstring &file_name);
protected:
	//! Creates the object.
	preprocessor(const ptr<file_system> &a_fs, const lstring &file_name);
	//! Marks the object.
	virtual void gc_mark(void);
private:
	//! Classifies number.
	ptr<lex_literal> classify_number(const ptr<source_location> &loc, const ptr<token_value> &tv);
	//! The order of tokens in translation unit.
	ulint order;
	//! File system binding.
	srp<file_system> fs;
	//! Evaluator.
	srp<evaluator> eva;
	//! Whitespace remover.
	srp<space_remover> spr;
	//! String translator.
	srp<string_translator> str;
	//! String joiner.
	srp<string_joiner> stj;
	//! Hides copy constructor.
	preprocessor(const preprocessor &);
	//! Hides assignment operator.
	preprocessor &operator=(const preprocessor &);
};

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
