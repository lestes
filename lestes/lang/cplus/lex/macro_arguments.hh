/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___macro_arguments_hh___included
#define lestes__lang__cplus__lex___macro_arguments_hh___included

/*! \file
  \brief Macro argument list.
  
	Declaration of macro_arguments class representing list of macro arguments.
	\author pt
*/
#include <lestes/common.hh>
#include <lestes/std/vector.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

// forward declarations to avoid cycle
class token_input;
class macro_argument;

/*!
  \brief Macro argument list.

  Represents list of macro arguments. Arguments can be empty, which introduces certain
  ambiguity when dealing with single empty argument, which is the same as no argument at all.
  Such argument list has state == PARSED_EMPTY, length() == 1 and one empty argument.
*/
class macro_arguments: public ::lestes::std::object {
public:
	//! Parses argument list.
	bool parse(const ptr<token_input> &input);
	//! Returns length of the list.
	ulint length(void) const;
	//! Returns argument at specified index.
	ptr<macro_argument> argument_get(ulint index) const;
	//! Checks if the arguments match parameter list length.
	bool check(ulint pars_length) const;
	//! Returns empty list.
	static ptr<macro_arguments> create(void);
protected:
	//! Creates empty list.
	macro_arguments(void);
	//! Marks the object.
	virtual void gc_mark(void);
private:
	//! Type of internal state.
	typedef enum { BEGIN, PARSED, PARSED_EMPTY, DEAD } state_type;
	//! Internal state of the object.
	state_type state;
	//! Type of list of macro arguments.
	typedef vector< srp<macro_argument> > arguments_type;
	//! List of macro arguments.
	srp<arguments_type> arguments;
};

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);
#endif
/* vim: set ft=lestes : */
