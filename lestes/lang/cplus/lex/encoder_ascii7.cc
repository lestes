/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief 7 bit ASCII encoder.

  Definition of encoder_ascii7 class performing 7 bit ASCII character set encoding.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/encoder_ascii7.hh>
#include <lestes/lang/cplus/lex/encoder_ascii7.m.hh>
#include <lestes/lang/cplus/lex/ucn_token.hh>
#include <lestes/std/source_location.hh>
#include <lestes/msg/message.hh>

//TODO #include <iostream>

package(lestes);
package(lang);
package(cplus);
package(lex);

using namespace ::std;

/*!
  Creates the encoder.
*/
encoder_ascii7::encoder_ascii7(void)
{
}

/*!
  Reads next token. Performs encoding from 7 bit ASCII character set.
  Checks whether the input contains valid characters.
  If error is encountered, it returns token with type ucn_token::TOK_ERROR.
  \pre  Input into the filter is set.
  \return  The next token encoded from 7 bit ASCII.
  \return  Token with type ucn_token::TOK_ERROR if the source character is invalid (out of range).
  \return  Token with type ucn_token::TOK_EOF in case of previous error.
*/
ptr<ucn_token> encoder_ascii7::read(void)
{
	ptr<ucn_token> t = input_read();
	ulint x = character::extract_value(t->value_get());
	ucn_token_type utt = t->type_get();

	if (utt == ucn_token::TOK_NOT_EOF) {
		// check that the value is in range 0..127
		if ((x & 0x7F) != x) {
			// TODO report error: value out of range for ASCII
			t = ucn_token::create_error(invalid_ascii_character->format());
		} else {
			// encode value in source charset
			t = t->clone_value(character::create_internal(x));
		}
	}

	return t;
}

/*!
  Marks the object.
*/
void encoder_ascii7::gc_mark(void)
{
	bad.gc_mark();
	encoder::gc_mark();
}

/*!
  Returns new instance of the encoder.
  \return  The new instance.
*/
ptr<encoder_ascii7> encoder_ascii7::create(void)
{
	return new encoder_ascii7();
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

/* vim: set ft=lestes : */
