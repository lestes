%{
/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file concat_guts.l
  \brief  C++ preprocessor operator '##' result checking.
  
  C++ preprocessor token concatenation lexical analyser.
  Checks validity of result of applying operator '##' of token concatenation.
  \author pt
*/

#if __GNUC__ > 3
//! Avoids standard C input and output.
#pragma GCC poison fprintf fwrite getc fread ferror
#endif

//! Avoids implicit flex rules.
#define ECHO lassert2(false, "ECHO should not be used.")

using namespace ::lestes::std;
using namespace ::lestes::lang::cplus::lex;
using namespace ::std;

//! called upon encountering real EOF, which should not happen, formal definition
#define yyterminate() return pp_token::create(a_location,pp_token::TOK_FILE_END)

//! Defines flex input function.
#define YY_INPUT(buf,result,max_size) \
{ \
	result = concat::instance()->yy_input(buf,max_size); \
}

//! Defines prototype for flex parser function.
#define YY_DECL ptr<pp_token> concat_parse(const ptr<source_location> &a_location, const ptr<token_value> &a_value)

/*!
  Returns one token from flex parser.
  \param a_location The location for the result.
  \param a_value The token value for the result.
  \return  The concatenated token or NULL in case of ill-formed concatenation.
*/
YY_DECL;
%}

/* settings for better performance from the knowledge of used features */
%option never-interactive
%option noyywrap
%option nostack
%option noyylineno
%option noinput
%option nounput
%option noyy_push_state
%option noyy_pop_state
%option noyy_top_state
%option noyy_scan_buffer
%option noyy_scan_bytes
%option noyy_scan_string

/* end of file character class */
EOF            "\x00"
/* translated characters character class */
TRANSLATED     "\x01"

/* basic source character set character classes */
TAB            "\x09"
VTAB           "\x0b"
FORM_FEED      "\x0c"
NEW_LINE       "\x0a"
SPACE          "\x20"
EMARK          "\x21"
DQUOTE         "\x22"
HASH           "\x23"
PERCENT        "\x25"
AMP            "\x26"
QUOTE          "\x27"
LEFT_PAR       "\x28"
RIGHT_PAR      "\x29"
STAR           "\x2a"
PLUS           "\x2b"
COMMA          "\x2c"
MINUS          "\x2d"
DOT            "\x2e"
SLASH          "\x2f"
D0             "\x30"
D1             "\x31"
D2             "\x32"
D3             "\x33"
D4             "\x34"
D5             "\x35"
D6             "\x36"
D7             "\x37"
D8             "\x38"
D9             "\x39"
COLON          "\x3a"
SEMICOLON      "\x3b"
LT             "\x3c"
EQ             "\x3d"
GT             "\x3e"
QMARK          "\x3f"
A              "\x41"
B              "\x42"
C              "\x43"
D              "\x44"
E              "\x45"
F              "\x46"
G              "\x47"
H              "\x48"
I              "\x49"
J              "\x4a"
K              "\x4b"
L              "\x4c"
M              "\x4d"
N              "\x4e"
O              "\x4f"
P              "\x50"
Q              "\x51"
R              "\x52"
S              "\x53"
T              "\x54"
U              "\x55"
V              "\x56"
W              "\x57"
X              "\x58"
Y              "\x59"
Z              "\x5a"
LEFT_BRACKET   "\x5b"
BACKSLASH      "\x5c"
RIGHT_BRACKET  "\x5d"
HAT            "\x5e"
_              "\x5f"
a              "\x61"
b              "\x62"
c              "\x63"
d              "\x64"
e              "\x65"
f              "\x66"
g              "\x67"
h              "\x68"
i              "\x69"
j              "\x6a"
k              "\x6b"
l              "\x6c"
m              "\x6d"
n              "\x6e"
o              "\x6f"
p              "\x70"
q              "\x71"
r              "\x72"
s              "\x73"
t              "\x74"
u              "\x75"
v              "\x76"
w              "\x77"
x              "\x78"
y              "\x79"
z              "\x7a"
LEFT_BRACE     "\x7b"
VBAR           "\x7c"
RIGHT_BRACE    "\x7d"
TILDE          "\x7e"

/* helper character classes and sequences */

/* any character */
ANY_CHAR    .|"\n"
/* any character except end of file */
NOT_EOF                 [^\x00]
/* any character except end of line */
NOT_NL                  [^\x0a]
/* any character except star and end of file */
NOT_STAR_EOF            [^\x2a\x00]
/* any character except star, slash and end of file */
NOT_STAR_SLASH_EOF      [^\x2a\x2f\x00]
/* any character except quote, backslash and end of line */
NOT_QUOTE_BACKSLASH_NL  [^\x27\x5c\x0a]
/* any character except double quote, backslash and end of line */
NOT_DQUOTE_BACKSLASH_NL [^\x22\x5c\x0a]
/* any character except greater than and end of line */
NOT_GT_NL               [^\x3e\x0a]
/* any character except double quote and end of line */
NOT_DQUOTE_NL           [^\x22\x0a]

DIGIT       {D0}|{D1}|{D2}|{D3}|{D4}|{D5}|{D6}|{D7}|{D8}|{D9}
OCT_DIGIT   {D0}|{D1}|{D2}|{D3}|{D4}|{D5}|{D6}|{D7}
HEX_LETTER  {a}|{b}|{c}|{d}|{e}|{f}|{A}|{B}|{C}|{D}|{E}|{F}
HEX_DIGIT   {DIGIT}|{HEX_LETTER}
UCN         {TRANSLATED}

LOWER       {a}|{b}|{c}|{d}|{e}|{f}|{g}|{h}|{i}|{j}|{k}|{l}|{m}|{n}|{o}|{p}|{q}|{r}|{s}|{t}|{u}|{v}|{w}|{x}|{y}|{z}
UPPER       {A}|{B}|{C}|{D}|{E}|{F}|{G}|{H}|{I}|{J}|{K}|{L}|{M}|{N}|{O}|{P}|{Q}|{R}|{S}|{T}|{U}|{V}|{W}|{X}|{Y}|{Z}
ALPHA       {LOWER}|{UPPER}
ALNUM       {ALPHA}|{DIGIT}
NONDIGIT    {ALPHA}|{UCN}|{_}
BLANK       {SPACE}|{TAB}|{VTAB}|{FORM_FEED}
ESC_CHAR    {QUOTE}|{DQUOTE}|{QMARK}|{BACKSLASH}|{a}|{b}|{f}|{n}|{r}|{t}|{v}
ESCAPE      {BACKSLASH}({ESC_CHAR}|{x}{HEX_DIGIT}+|{OCT_DIGIT}{1,3})

%%

{LEFT_BRACKET}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_LEFT_BRACKET);
}

{LT}{SEMICOLON}{EOF} {
	return pp_token::create_alternative(a_location,pp_token::TOK_LEFT_BRACKET);
}

{RIGHT_BRACKET}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_RIGHT_BRACKET);
}

{SEMICOLON}{GT}{EOF} {
	return pp_token::create_alternative(a_location,pp_token::TOK_RIGHT_BRACKET);
}

{LEFT_BRACE}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_LEFT_BRACE);
}

{LT}{PERCENT}{EOF} {
	return pp_token::create_alternative(a_location,pp_token::TOK_LEFT_BRACE);
}

{RIGHT_BRACE}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_RIGHT_BRACE);
}

{PERCENT}{GT}{EOF} {
	return pp_token::create_alternative(a_location,pp_token::TOK_RIGHT_BRACE);
}

{HASH}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_HASH);
}

{PERCENT}{COLON}{EOF} {
	return pp_token::create_alternative(a_location,pp_token::TOK_HASH);
}

{HASH}{HASH}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_HASH_HASH);
}

{PERCENT}{COLON}{PERCENT}{COLON}{EOF} {
	return pp_token::create_alternative(a_location,pp_token::TOK_HASH_HASH);
}

{LEFT_PAR}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_LEFT_PAR);
}

{RIGHT_PAR}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_RIGHT_PAR);
}

{SEMICOLON}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_SEMICOLON);
}

{COLON}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_COLON);
}

{QMARK}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_QMARK);
}

{DOT}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_DOT);
}

{PLUS}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_PLUS);
}

{STAR}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_STAR);
}

{PERCENT}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_PERCENT);
}

{SLASH}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_SLASH);
}

{HAT}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_HAT);
}

{x}{o}{r}{EOF} {
	return pp_token::create_alternative(a_location,pp_token::TOK_HAT);
}

{AMP}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_AMP);
}

{b}{i}{t}{a}{n}{d}{EOF} {
	return pp_token::create_alternative(a_location,pp_token::TOK_AMP);
}

{VBAR}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_VBAR);
}

{b}{i}{t}{o}{r}{EOF} {
	return pp_token::create_alternative(a_location,pp_token::TOK_VBAR);
}

{TILDE}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_TILDE);
}

{c}{o}{m}{p}{l}{EOF} {
	return pp_token::create_alternative(a_location,pp_token::TOK_TILDE);
}

{EMARK}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_EMARK);
}

{n}{o}{t}{EOF} {
	return pp_token::create_alternative(a_location,pp_token::TOK_EMARK);
}

{EQ}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_EQ);
}

{LT}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_LT);
}

{GT}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_GT);
}

{COMMA}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_COMMA);
}

{MINUS}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_MINUS);
}

{COLON}{COLON}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_COLON_COLON);
}

{DOT}{STAR}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_DOT_STAR);
}

{PLUS}{EQ}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_PLUS_EQ);
}

{MINUS}{EQ}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_MINUS_EQ);
}

{STAR}{EQ}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_STAR_EQ);
}

{SLASH}{EQ}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_SLASH_EQ);
}

{PERCENT}{EQ}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_PERCENT_EQ);
}

{HAT}{EQ}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_HAT_EQ);
}

{x}{o}{r}_{e}{q}{EOF} {
	return pp_token::create_alternative(a_location,pp_token::TOK_HAT_EQ);
}

{AMP}{EQ}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_AMP_EQ);
}

{a}{n}{d}_{e}{q}{EOF} {
	return pp_token::create_alternative(a_location,pp_token::TOK_AMP_EQ);
}

{VBAR}{EQ}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_VBAR_EQ);
}

{o}{r}_{e}{q}{EOF} {
	return pp_token::create_alternative(a_location,pp_token::TOK_VBAR_EQ);
}

{LT}{LT}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_LT_LT);
}

{GT}{GT}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_GT_GT);
}

{GT}{GT}{EQ}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_GT_GT_EQ);
}

{LT}{LT}{EQ}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_LT_LT_EQ);
}

{EQ}{EQ}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_EQ_EQ);
}

{EMARK}{EQ}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_EMARK_EQ);
}

{n}{o}{t}_{e}{q}{EOF} {
	return pp_token::create_alternative(a_location,pp_token::TOK_EMARK_EQ);
}

{LT}{EQ}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_LT_EQ);
}

{GT}{EQ}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_GT_EQ);
}

{AMP}{AMP}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_AMP_AMP);
}

{a}{n}{d}{EOF} {
	return pp_token::create_alternative(a_location,pp_token::TOK_AMP_AMP);
}

{VBAR}{VBAR}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_VBAR_VBAR);
}

{o}{r}{EOF} {
	return pp_token::create_alternative(a_location,pp_token::TOK_VBAR_VBAR);
}

{PLUS}{PLUS}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_PLUS_PLUS);
}

{MINUS}{MINUS}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_MINUS_MINUS);
}

{MINUS}{GT}{STAR}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_MINUS_GT_STAR);
}

{MINUS}{GT}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_MINUS_GT);
}

{DOT}{DOT}{DOT}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_DOT_DOT_DOT);
}

{a}{s}{m}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_ASM);
}

{a}{u}{t}{o}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_AUTO);
}

{b}{o}{o}{l}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_BOOL);
}

{b}{r}{e}{a}{k}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_BREAK);
}

{c}{a}{s}{e}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_CASE);
}

{c}{a}{t}{c}{h}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_CATCH);
}

{c}{h}{a}{r}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_CHAR);
}

{c}{l}{a}{s}{s}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_CLASS);
}

{c}{o}{n}{s}{t}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_CONST);
}

{c}{o}{n}{s}{t}_{c}{a}{s}{t}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_CONST_CAST);
}

{c}{o}{n}{t}{i}{n}{u}{e}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_CONTINUE);
}

{d}{e}{f}{a}{u}{l}{t}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_DEFAULT);
}

{d}{e}{l}{e}{t}{e}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_DELETE);
}

{d}{o}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_DO);
}

{d}{o}{u}{b}{l}{e}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_DOUBLE);
}

{d}{y}{n}{a}{m}{i}{c}_{c}{a}{s}{t}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_DYNAMIC_CAST);
}

{e}{l}{s}{e}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_ELSE);
}

{e}{n}{u}{m}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_ENUM);
}

{e}{x}{p}{l}{i}{c}{i}{t}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_EXPLICIT);
}

{e}{x}{p}{o}{r}{t}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_EXPORT);
}

{e}{x}{t}{e}{r}{n}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_EXTERN);
}

{f}{l}{o}{a}{t}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_FLOAT);
}

{f}{o}{r}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_FOR);
}

{f}{r}{i}{e}{n}{d}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_FRIEND);
}

{g}{o}{t}{o}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_GOTO);
}

{i}{f}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_IF);
}

{i}{n}{l}{i}{n}{e}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_INLINE);
}

{i}{n}{t}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_INT);
}

{l}{o}{n}{g}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_LONG);
}

{m}{u}{t}{a}{b}{l}{e}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_MUTABLE);
}

{n}{a}{m}{e}{s}{p}{a}{c}{e}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_NAMESPACE);
}

{n}{e}{w}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_NEW);
}

{o}{p}{e}{r}{a}{t}{o}{r}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_OPERATOR);
}

{p}{r}{i}{v}{a}{t}{e}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_PRIVATE);
}

{p}{r}{o}{t}{e}{c}{t}{e}{d}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_PROTECTED);
}

{p}{u}{b}{l}{i}{c}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_PUBLIC);
}

{r}{e}{g}{i}{s}{t}{e}{r}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_REGISTER);
}

{r}{e}{i}{n}{t}{e}{r}{p}{r}{e}{t}_{c}{a}{s}{t}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_REINTERPRET_CAST);
}

{r}{e}{t}{u}{r}{n}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_RETURN);
}

{s}{h}{o}{r}{t}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_SHORT);
}

{s}{i}{g}{n}{e}{d}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_SIGNED);
}

{s}{i}{z}{e}{o}{f}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_SIZEOF);
}

{s}{t}{a}{t}{i}{c}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_STATIC);
}

{s}{t}{a}{t}{i}{c}_{c}{a}{s}{t}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_STATIC_CAST);
}

{s}{t}{r}{u}{c}{t}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_STRUCT);
}

{s}{w}{i}{t}{c}{h}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_SWITCH);
}

{t}{e}{m}{p}{l}{a}{t}{e}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_TEMPLATE);
}

{t}{h}{i}{s}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_THIS);
}

{t}{h}{r}{o}{w}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_THROW);
}

{t}{r}{y}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_TRY);
}

{t}{y}{p}{e}{d}{e}{f}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_TYPEDEF);
}

{t}{y}{p}{e}{i}{d}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_TYPEID);
}

{t}{y}{p}{e}{n}{a}{m}{e}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_TYPENAME);
}

{u}{n}{i}{o}{n}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_UNION);
}

{u}{n}{s}{i}{g}{n}{e}{d}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_UNSIGNED);
}

{u}{s}{i}{n}{g}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_USING);
}

{v}{i}{r}{t}{u}{a}{l}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_VIRTUAL);
}

{v}{o}{i}{d}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_VOID);
}

{v}{o}{l}{a}{t}{i}{l}{e}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_VOLATILE);
}

{w}{c}{h}{a}{r}_{t}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_WCHAR);
}

{w}{h}{i}{l}{e}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_WHILE);
}

{f}{a}{l}{s}{e}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_BOOL_LIT,a_value);
}

{t}{r}{u}{e}{EOF} {
	return pp_token::create(a_location,pp_token::TOK_BOOL_LIT,a_value);
}

	/* pp number */
({DIGIT}|{DOT}{DIGIT})({DIGIT}|{ALPHA}|({e}|{E})[-+]|{DOT})*{EOF} {
	return pp_token::create(a_location,pp_token::TOK_NUMBER_LIT,a_value);
}

	/* pp won't be number (ucn or underscore) - next best match */
({DIGIT}|{DOT}{DIGIT})({DIGIT}|{NONDIGIT}|({e}|{E})[-+]|{DOT})*{EOF} {
	return pp_token::create(a_location,pp_token::TOK_NUMBER_LIT,a_value);
}

	/* pp alphanumeric identifier */
({ALNUM}|_)+{EOF} {
	return pp_token::create(a_location,pp_token::TOK_IDENT,a_value);
}

	/* pp generic identifier (contains ucns) */
{NONDIGIT}({DIGIT}|{NONDIGIT})*{EOF} {
	// check correct ucn in identifiers
	ucn_string us(a_value->content_get());
	ucn u;
	for (ucn_string::iterator it = us.begin(), end = us.end(); it != end; ++it) {
		u = *it;
		if (!(character::is_basic(u) || character::is_translated_identifier(u)))
         // invalid character in identifier
			return NULL;
	}
	return pp_token::create(a_location,pp_token::TOK_IDENT,a_value);
}

	/* character literal */
{QUOTE}({NOT_QUOTE_BACKSLASH_NL}|{ESCAPE})+{QUOTE}{EOF} {
   ucn_string u(a_value->content_get());
   ucn_string v(u.begin() + 1,u.begin() + u.length() - 1);
	return pp_token::create(a_location,pp_token::TOK_CHAR_LIT,token_value::create(v));
}
	/* wide character literal */
{L}{QUOTE}({NOT_QUOTE_BACKSLASH_NL}|{ESCAPE})+{QUOTE}{EOF} {
   ucn_string u(a_value->content_get());
   ucn_string v(u.begin() + 2,u.begin() + u.length() - 1);
	return pp_token::create(a_location,pp_token::TOK_WCHAR_LIT,token_value::create(v));
}
	/* string literal */
{DQUOTE}({NOT_DQUOTE_BACKSLASH_NL}|{ESCAPE})*{DQUOTE}{EOF} {
   ucn_string u(a_value->content_get());
   ucn_string v(u.begin() + 1,u.begin() + u.length() - 1);
	return pp_token::create(a_location,pp_token::TOK_STRING_LIT,token_value::create(v));
}
	/* wide string literal */
{L}{DQUOTE}({NOT_DQUOTE_BACKSLASH_NL}|{ESCAPE})*{DQUOTE}{EOF} {
   ucn_string u(a_value->content_get());
   ucn_string v(u.begin() + 2,u.begin() + u.length() - 1);
	return pp_token::create(a_location,pp_token::TOK_WSTRING_LIT,token_value::create(v));
}

	/* chatch ill-formed concatenation */
[^\0]*{EOF} {
	return NULL;
}

%%

/* vim: set ft=lex : */
