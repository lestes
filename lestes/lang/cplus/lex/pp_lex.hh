/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___pp_lex_hh___included
#define lestes__lang__cplus__lex___pp_lex_hh___included

/*! \file
  \brief Integration with flexer.
  
  Declaration of pp_lex class wrapping pp_lex_guts flexer.
  Enables sharing flexer by concurrent instances of pp_lex via
  switching flexer contexts.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/ucn_token.hh>

package(lestes);

package(std);
// forward declaration to avoid cycle
class file_info;
class source_location;
end_package(std);

package(lang);
package(cplus);
package(lex);

// forward declaration to avoid cycle
class pp_token;
class pre_lex;
class simple_location;
class ucn_token_buffer;
class line_control;

package(pp_lex_guts);
//! Forward declaration of flex buffer type.
struct yy_buffer_state;
end_package(pp_lex_guts);

/*!
  \brief Integration with flexer.
  
  Handles integration with pp_lex_guts flexer. Passes special character classes to flexer,
  storing actual characters in own buffer. Each instance is connected to
  flexer by calling activate() prior to reading tokens (after file change etc.).
*/
class pp_lex: public ::lestes::std::object {
public:
	//! Type of character in lex.
	typedef char char_type;
	//! Type of size in lex.
	typedef int size_type;
	//! Finalizes object.
	virtual ~pp_lex(void);
	//! Prepares pp_lex_guts to communicate with this object.
	void activate(void);
	//! Stores next characters for YY_INPUT.
	size_type yy_input(char_type *cbuf, size_type max);
	//! Saves location of current input token.
	void location_save(void);
	//! Creates location for current token.
	ptr<source_location> location_create(void) const;
	//! Returns the line control.
	ptr<line_control> lines_get(void) const;
	//! Reads token from flex.
	ptr<pp_token> read(bool read_include);
	//! Sets interactive flag.
	void interactive_set(bool a_interactive);
	//! Returns interactive flag.
	bool interactive_get(void) const;
	//! Returns new instance.
	static ptr<pp_lex> create(const ptr<pre_lex> &a_input, const ptr<line_control> &a_lines);
protected:
	//! Creates the object.
	pp_lex(const ptr<pre_lex> &a_input, const ptr<line_control> &a_lines);
	//! Marks the object.
	virtual void gc_mark(void);
private:
	//! Type of flex buffer.
	typedef struct pp_lex_guts::yy_buffer_state *yy_buffer_type;
	//! Reads next character from input.
	char_type read_char(void);
	//! Flag for interactiveness.
	bool interactive;
	//! Input stream of tokens.
	srp<pre_lex> input;
	//! Physical location of currently processed token.
	srp<simple_location> physical;
	//! Line control for transforming locations.
	srp<line_control> lines;
	//! Buffer with ucn tokens.
	srp<ucn_token_buffer> buffer;
	//! Error token buffer.
	srp<ucn_token_buffer> errors;
	//! Flex buffer.
	yy_buffer_type yy_buffer;
	//! Hides copy constructor.
	pp_lex(const pp_lex &copy);
	//! Hides assignment operator.
	pp_lex &operator=(const pp_lex &rhs);
};

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
