/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___special_tokens_hh___included
#define lestes__lang__cplus__lex___special_tokens_hh___included

/*! \file
  \brief Token type assignment filter.

  Declaration of special_tokens class assigning types to tokens.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/ucn_token.hh>
#include <lestes/lang/cplus/lex/ucn_filter.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

/*!
  \brief Token type assignment filter.

  Assigns type to tokens according to their value.
  Checks invalid values of characters.
  Cuts input flooding errors
*/
class special_tokens: public ucn_filter {
public:
	//! Reads next token.
	ptr<ucn_token> read(void);
	//! Returns new instance.
	static ptr<special_tokens> create(void);
protected:
	//! Creates the object.
	special_tokens(void);
	//! Marks the object.
	virtual void gc_mark(void);
private:
	enum {
		//! Number of errors to cut the input.
		ERRORS_LIMIT = 8
	};
	//! Hides copy constructor.
	special_tokens(const special_tokens &copy);
	//! Hides assignment operator.
	special_tokens &operator=(const special_tokens &rhs);
	//! States of the automaton.
	typedef enum {
		//! Ordinary tokens.
		START,
		//! Already encountered end of file.
		END
	} state_type;
	//! Current state of the parser.
	state_type state;
	//! Saved token.
	srp<ucn_token> saved;
	//! Number of incoming errors.
	ulint errors;
};

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
