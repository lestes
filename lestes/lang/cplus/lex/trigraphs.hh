/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___trigraphs_hh___included
#define lestes__lang__cplus__lex___trigraphs_hh___included

/*! \file
  \brief Trigraphs filter.
  
  Declaration of trigraphs class performing trigraph translation.
  \author pt
*/

#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/ucn_filter.hh>
#include <climits> 
package(lestes);
package(lang);
package(cplus);
package(lex);

/*!
  \brief Trigraphs filter.

  Maps trigraph sequences to their translations.
*/
class trigraphs: public ucn_filter {
public:
	//! Reads next token.
	ptr<ucn_token> read(void);
	//! Returns new instance.
	static ptr<trigraphs> create(void);
protected:
	//! Creates new instance.
	trigraphs(void);
	//! Marks the object.
	virtual void gc_mark(void);
private:
	//! Hides copy constructor.
	trigraphs(const trigraphs &copy);
	//! Hides assignment operator.
	trigraphs &operator=(const trigraphs &rhs);
	//! States of the trigraph processor.
	typedef enum { 
		//! No tokens in buffer.
		START = 0,
		//! One question mark in buffer.
		QUE = 1,
		//! Two question marks in buffer.
		QUEQUE  = 2,
		//! One ordinary token in buffer.
		ONE = 3,
		//! Two ordinary tokens in buffer.
		TWO = 4
	} state_type;
	//! Length of map of trigraph translations.
	static const ulint translation_length = 128;
	//! Map of trigraph translations, also serves as flags.
	static const ucn translation_map[translation_length];
	//! Current state of the processor.
	state_type state;
	//! First buffered token.
	srp<ucn_token> first;
	//! Second buffered token.
	srp<ucn_token> second;
};

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
