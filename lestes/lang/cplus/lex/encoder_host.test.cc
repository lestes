/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Unit test.
  
  Unit test for class encoder_host.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/string_source.hh>
#include <lestes/lang/cplus/lex/encoder_host.hh>
#include <lestes/std/set.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

using namespace ::std;

#define TEST_CNT 2

/*!
  \brief Tests encoder_host class.

  Performs testing of encoder_host class.
*/
void encoder_host_test(void)
{
   char *supported =
         "\a\b\t\n\v\f\r !\"#$%&'()*+,-./0123456789:;<=>?@"
         "ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~";

   // search for char value not expressable
   // TODO pt ugly 
   ptr< set<ulint> > sch = set<ulint>::create();
   for (char *s = supported; *s; s++) {
      sch->insert(static_cast<ulint>(*s));
   }

   set<ulint>::iterator it = sch->begin();
   char c = static_cast<char>(*it);
   if (c > CHAR_MIN) {
      c--;
   } else {
      set<ulint>::reverse_iterator rit = sch->rbegin();
      c = static_cast<char>(*rit);
      lassert(c < CHAR_MAX);
      c++;
   }
   
   char unsupported[] = { c, '\0' };

   char *in[TEST_CNT] = {
         /* allowed */
         supported
      ,
         /* disallowed */
         unsupported
   };

   ucn_token_type out[] = {
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_NOT_EOF,
      ucn_token::TOK_EOF,
      ucn_token::TOK_ERROR,
   };

   ptr<ucn_token> tok;
   ucn_token_type utt;
   ulint test, i;

   for (i = test = 0; test < TEST_CNT; test++) {
      ptr<data_source> ds = string_source::create(string_source::string_type(in[test]));
      ptr<encoder_host> enc = encoder_host::create();

      enc->input_set(ds);

      while (true) {
         tok = enc->read();
         utt = tok->type_get();
         lassert(utt == out[i]);
         i++;
         if (utt == ucn_token::TOK_EOF || utt == ucn_token::TOK_ERROR) break;
      }
   }
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

/*!
  \brief Main function.

  Runs the unit test in different namespace.
*/
int main(void)
{
   ::lestes::lang::cplus::lex::encoder_host_test();
   return 0;
}
/* vim: set ft=lestes : */
