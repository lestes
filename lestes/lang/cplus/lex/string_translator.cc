/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Encoder into execution character set.
  
  Definition of string_translator class representing translator into execution character set.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/std/ucn_string.hh>
#include <lestes/std/source_location.hh>
#include <lestes/lang/cplus/lex/string_translator.hh>
#include <lestes/lang/cplus/lex/pp_filter.hh>
#include <lestes/lang/cplus/lex/pp_token.hh>
#include <lestes/lang/cplus/lex/token_value.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

/*!
  Creates the translator.
  \pre a_input != NULL
  \param a_input  The input for the filter.
*/
string_translator::string_translator(const ptr<pp_filter> &a_input):
	pp_filter(checked(a_input))
{
}

/*!
  Reads next token, encodes character and string literals into execution character set.
  \return  The next token.
*/
ptr<pp_token> string_translator::read(void)
{
	ptr<pp_token> t = input_read();
	pp_token_type ptt = t->type_get();
	ptr<source_location> loc;

	switch (ptt) {
		case pp_token::TOK_STRING_LIT:
		case pp_token::TOK_WSTRING_LIT:
		case pp_token::TOK_CHAR_LIT:
		case pp_token::TOK_WCHAR_LIT:
			// TODO pt change the TOK*LIT for TOK*TRN
			loc = t->location_get();
			t = pp_token::create(loc,ptt,translate(t->value_get()->content_get(),loc));
			break;
		default:
			break;
	}

	return t;
}

/*!
  Translates string or character literal to execution character set.
  \pre loc != NULL
  \param str The literal representation to translate.
  \param loc The location of the literal.
  \return The literal in execution character set.
*/ 
ptr<token_value> string_translator::translate(const ucn_string &str, const ptr<source_location> &loc)
{
	// state of the function
	enum {
		BEGIN,
		PASS,
		BACK,
		OCT,
		HEX,
		TRANSLATE
	} fstate = BEGIN;

	ucn_string::size_type len = str.length();
	ulint count = 0xbad;
	ulint value = 0xbad;
	ucn u = 0xbad;
	
	// reserve space
	ucn_string us(len,0xbeef);
	
	ucn_string::const_iterator it = str.begin();
	ucn_string::const_iterator end = str.end();
	ucn_string::iterator sit = us.begin();
	while (true) {
		if (fstate == PASS) {
			fstate = BEGIN;
		} else if (it != end) {
			u = *it;
			++it;
		} else break;

		switch (fstate) {
			case BEGIN:
				if (u == character::ascii_backslash) {
					fstate = BACK;
				} else {
					fstate = TRANSLATE;
				}
				break;
			case BACK:
				switch (u) {
					case character::ascii_lower_x:
						count = 1;
						value = 0;
						fstate = HEX;
						break;
					case character::ascii_quote:
					case character::ascii_dquote:
					case character::ascii_qmark:
					case character::ascii_backslash:
						fstate = TRANSLATE;
						break;
					case character::ascii_lower_a:
						u = character::ascii_bell;
						break;
					case character::ascii_lower_b:
						u = character::ascii_backspace;
						fstate = TRANSLATE;
						break;
					case character::ascii_lower_f:
						u = character::ascii_form_feed;
						fstate = TRANSLATE;
						break;
					case character::ascii_lower_n:
						u = character::ascii_new_line;
						fstate = TRANSLATE;
						break;
					case character::ascii_lower_r:
						u = character::ascii_carriage_return;
						fstate = TRANSLATE;
						break;
					case character::ascii_lower_t:
						u = character::ascii_tab;
						fstate = TRANSLATE;
						break;
					case character::ascii_lower_v:
						u = character::ascii_vtab;
						fstate = TRANSLATE;
						break;
					default:
					  lassert(character::is_odigit(u));
					  value = character::extract_odigit(u);
					  count = 2;
					  fstate = OCT;
					  break;
				}
				break;
			case OCT:
				if (character::is_odigit(u)) {
					value = (value << 3) + character::extract_odigit(u);
					if (--count == 0) {
						*sit = character::create_external(value);
						++sit;
						fstate = BEGIN;
					}
				} else {
					*sit = character::create_external(value);
					++sit;
					fstate = PASS;
				}
				break;
			case HEX:
				if (character::is_xdigit(u)) {
					value = (value << 4) + character::extract_xdigit(u);
				} else {
					lassert(count);
					*sit = character::create_external(value);
					++sit;
					fstate = PASS;
				}
				break;
			default:
				lassert(false);
				break;
		}

		if (fstate == TRANSLATE) {
			// TODO not hardcoded ASCII
			if (!character::is_ascii7(u)) {
				// TODO pt report error: unknown character
				(void)loc;
				u = character::ascii_qmark;
			}
			*sit = character::extract_value(u);
			++sit;

			fstate = BEGIN;
		}
	}

	switch (fstate) {
		case BEGIN:
			break;
		case OCT:
			*sit = character::create_external(value);
			++sit;
			break;
		case HEX:
			lassert(count);
			*sit = character::create_external(value);
			++sit;
			break;
		default:
			lassert2(false,"You should never get here");
	}
	
	// use only filled part of the string
	return token_value::create(ucn_string(us.begin(),sit));
}

/*!
  Returns new translator.
  \pre a_input != NULL
  \param a_input The input for the filter.
  \return  New instance of the translator.
*/
ptr<string_translator> string_translator::create(const ptr<pp_filter> &a_input)
{
	return new string_translator(a_input);
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

/* vim: set ft=lestes : */
