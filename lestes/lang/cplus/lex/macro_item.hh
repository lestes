/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___macro_item_hh___included
#define lestes__lang__cplus__lex___macro_item_hh___included

/*! \file
  \brief Expansion list item.
  
  Declaration of macro_item class representing part of macro expansion list.
  \author pt
*/
#include <lestes/common.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

// forward declaration to break cycle
class pp_token;
class token_sequence;

/*!
  \brief Expansion list item.

  Represents of part of macro expansion list.
  Used as an element in macro_body.
*/
class macro_item: public ::lestes::std::object {
public:
	//! Type of action performed within the item.
	typedef enum {
		//! Insert literal value.
		LITERAL,
		//! Insert expanded parameter.
		EXPANSION,
		//! Insert copy of parameter.
		COPY,
		//! Insert stringification of parameter.
		STR,
		//! Insert concatenation of adjacent tokens.
		CONCAT
	} action_type;
	//! Type of index of macro parameter.
	typedef ulint index_type;
	//! Returns action to be performed.
	action_type action_get(void) const;
	//! Returns value of literal or glue token.
	ptr<token_sequence> value_get(void) const;
	//! Returns index of referenced macro parameter.
	index_type index_get(void) const;
	//! Returns blank flag.
	bool blank_get(void) const;
	//! Tests equality.
	bool equals(const ptr<macro_item> &other) const;
	//! Returns literal macro item.
	static ptr<macro_item> create_literal(const ptr<token_sequence> &a_value);
	//! Returns expansion macro item.
	static ptr<macro_item> create_expansion(index_type a_index);
	//! Returns copy macro item.
	static ptr<macro_item> create_copy(index_type a_index);
	//! Returns stringification macro item.
	static ptr<macro_item> create_str(index_type a_index, const ptr<token_sequence> &a_value);
	//! Returns concatenation macro item.
	static ptr<macro_item> create_concat(const ptr<token_sequence> &a_value);
protected:
	//! Creates item.
	macro_item(action_type a_action, index_type a_index, const ptr<token_sequence> &a_value);
	//! Marks the object.
	virtual void gc_mark(void);
private:
	//! Action to be performed.
	action_type action;
	//! Index of referenced macro parameter.
	index_type index;
	//! Value of literal or operation token wrapped in sequence.
	srp<token_sequence> value;
};

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
