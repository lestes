/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Unit test.

  Unit test for class pp_token.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/equality.hh>
#include <lestes/std/source_location.hh>
#include <lestes/std/file_info.hh>
#include <lestes/lang/cplus/lex/pp_token.hh>
#include <lestes/lang/cplus/lex/token_value.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

using namespace ::std;

/*!
  \brief Tests pp_token class.

  Performs testing of pp_token class.
*/
void pp_token_test(void)
{
	ptr<file_info> fi = file_info::create("foobar.h",NULL);
	ptr<source_location> loc = source_location::create(fi,2,3);
	ptr<token_value> s = token_value::create("123");
	ptr<token_value> t = token_value::create("123");
	ptr<token_value> u = token_value::create("abc");
	ptr<token_value> v = token_value::create("abc");
	
	ptr<pp_token> a = pp_token::create(loc,pp_token::TOK_NUMBER_LIT,s);

	lassert(is_equal(a,a));
	lassert(is_equal(a->type_get(),pp_token::TOK_NUMBER_LIT));
	lassert(is_equal(a->value_get(),t));
	lassert(is_equal(a->location_get()->file_get(),fi));
	lassert(is_equal(a->location_get()->line_get(),2U));
	lassert(is_equal(a->location_get()->column_get(),3U));
	lassert(is_equal(a->flags_get(),pp_token::FLG_LITERAL));
		
	a->type_set(pp_token::TOK_STRING_LIT);
	a->value_set(u);
	a->location_set(source_location::create(fi,0,0));
	a->flags_set(pp_token::FLG_ERROR);

	lassert(is_equal(a->type_get(),pp_token::TOK_STRING_LIT));
	lassert(is_equal(a->value_get(),v));
	lassert(is_equal(a->location_get()->file_get(),fi));
	lassert(is_equal(a->location_get()->line_get(),0U));
	lassert(is_equal(a->location_get()->column_get(),0U));
	lassert(is_equal(a->flags_get(),(pp_token::FLG_LITERAL | pp_token::FLG_ERROR)));
		
	ptr<pp_token> b = a->clone();

	lassert(is_equal(b,a));
	lassert(is_equal(a,b));
	lassert(is_equal(b->type_get(),pp_token::TOK_STRING_LIT));
	lassert(is_equal(b->value_get(),v));
	lassert(is_equal(b->location_get()->file_get(),fi));
	lassert(is_equal(b->location_get()->line_get(),0U));
	lassert(is_equal(b->location_get()->column_get(),0U));
	lassert(is_equal(b->flags_get(),(pp_token::FLG_LITERAL | pp_token::FLG_ERROR)));
		
	ptr<pp_token> c;

	c = a;
	lassert(is_equal(c,a));
	lassert(is_equal(a,c));
	lassert(is_equal(c->type_get(),pp_token::TOK_STRING_LIT));
	lassert(is_equal(c->value_get(),v));
	lassert(is_equal(c->location_get()->file_get(),fi));
	lassert(is_equal(c->location_get()->line_get(),0U));
	lassert(is_equal(c->location_get()->column_get(),0U));
	lassert(is_equal(c->flags_get(),(pp_token::FLG_LITERAL | pp_token::FLG_ERROR)));
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

int main(void)
{
	lestes::lang::cplus::lex::pp_token_test();
	return 0;
}

/* vim: set ft=lestes : */
