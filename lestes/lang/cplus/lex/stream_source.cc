/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Stream data source.

  Definition of stream_source class representing data source reading from stream.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/std/istream_wrapper.hh>
#include <lestes/lang/cplus/lex/stream_source.hh>
#include <lestes/lang/cplus/lex/ucn_token.hh>
#include <istream>

package(lestes);
package(lang);
package(cplus);
package(lex);

using namespace ::std;

/*!
  Creates the object, initializes with wrapper.
  \param a_wrapper  The stream wrapper.
*/
stream_source::stream_source(const ptr<istream_wrapper> &a_wrapper):
	wrapper(checked(a_wrapper)),
	stream(wrapper->stream_get())
{
}

/*!
  Reads next token from stream.
  \return  The next token.
*/
ptr<ucn_token> stream_source::read(void)
{
	ptr<ucn_token> t;
	int c;

	if ((c = stream->get()) == EOF) {
		// TODO pt not necessary to set value, but looks better when debugging
		t = ucn_token::create(ucn_token::TOK_EOF,character::create_external(0));
	} else {
		// note that the meaning of real types is not clear at this stage
		t = ucn_token::create(ucn_token::TOK_NOT_EOF,character::create_external(static_cast<ulint>(c)));
	}
  
	return t;
}

/*!
  Marks the object.
*/
void stream_source::gc_mark(void)
{
	wrapper.gc_mark();
	data_source::gc_mark();
}

/*!
  Returns new instance, initializes with stream and file info.
  \param a_wrapper  The stream wrapper.
  \return  The new instance.
*/
ptr<stream_source> stream_source::create(const ptr<istream_wrapper> &a_wrapper)
{
	return new stream_source(a_wrapper);
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

/* vim: set ft=lestes : */
