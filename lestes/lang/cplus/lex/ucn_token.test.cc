/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  Unit test for class ucn_token.
  \todo Only copied, not refactored yet.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/ucn_token.hh>
#include <lestes/lang/cplus/lex/simple_location.hh>
#include <string>

package(lestes);
package(lang);
package(cplus);
package(lex);

using namespace ::std;

void ucn_token_test(void)
{
	ptr<simple_location> loc = simple_location::create(3,5);
	ptr<ucn_token> a = ucn_token::create(ucn_token::TOK_NOT_EOF);
	
	lassert(is_equal(a,a));
	lassert(is_equal(a->location_get(),ptr<simple_location>(NULL)));
	lassert(is_equal(a->type_get(),ucn_token::TOK_NOT_EOF));
	
	ptr<ucn_token> b = ucn_token::create(ucn_token::TOK_BASIC,character::create_from_host('z'));

	lassert(is_equal(b,b));
	lassert(is_equal(b->type_get(),ucn_token::TOK_BASIC));
	lassert(is_equal(b->value_get(),character::create_from_host('z')));
	lassert(is_equal(a->location_get(),ptr<simple_location>(NULL)));
	
	ptr<ucn_token> c = ucn_token::create(ucn_token::TOK_BASIC,character::create_from_host('v'),loc);
	lassert(is_equal(c->type_get(),ucn_token::TOK_BASIC));
	lassert(is_equal(c->value_get(),character::create_from_host('v')));
	lassert(is_equal(c->location_get(),loc));

	ptr<ucn_token> d;
	d = c;
	lassert(is_equal(c,d));
	lassert(is_equal(d,c));

	lassert(is_equal(d->type_get(),ucn_token::TOK_BASIC));
	lassert(is_equal(d->value_get(),character::create_from_host('v')));
	lassert(is_equal(d->location_get(),loc));
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

int main(void)
{
	::lestes::lang::cplus::lex::ucn_token_test();
	return 0;
}
/* vim: set ft=lestes : */
