/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Loggers for lex.

  Definition of individual lex loggers.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/lex_loggers.hh>
#include <lestes/msg/logger.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

initialize_logger(pp_lex_logger,"pp_lex",lex_logger);
initialize_logger(pp_lex_guts_logger,"pp_lex_guts",lex_logger);
initialize_logger(preprocessor_logger,"preprocessor",lex_logger);
initialize_logger(trigraphs_logger,"trigraphs",lex_logger);
initialize_logger(pre_lex_logger,"pre_lex",lex_logger);

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);
/* vim: set ft=lestes : */
