/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  Unit test for class trigraphs.
  \todo Not updated for the new situation.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/std/source_location.hh>
#include <lestes/lang/cplus/lex/special_tokens.hh>
#include <lestes/std/file_info.hh>
#include <lestes/lang/cplus/lex/string_source.hh>
#include <lestes/lang/cplus/lex/encoder_host.hh>
#include <lestes/lang/cplus/lex/trigraphs.hh>
#include <string>

package(lestes);
package(lang);
package(cplus);
package(lex);

using namespace ::std;

void trigraphs_test(void)
{
	char in[] = "?""?= ?""?/ ?""?( ?""?) ?""?! ?""?< ?""?> ?""?- ?""?' \\?""?/ ?\\?/\n";
	char out[] = "# \\ [ ] | { } ~ ^ \\\\ ?\\?/\n";
	lint i;
	ptr<file_info> fi = file_info::create(string("abcd"),NULL);
	ptr<data_source> ds = string_source::create(in);
	ptr<encoder_host> enc = encoder_host::create();
	ptr<special_tokens> stok = special_tokens::create();
	ptr<trigraphs> tri = trigraphs::create();
	ptr<ucn_token> tok;

	enc->input_set(ds);
	stok->input_set(enc);
	tri->input_set(stok);
	
	i = 0;
	do {
		tok = tri->read();
		//cout << i << ": " << (char)tok.get_value() << endl;
		if (tok->type_get() == ucn_token::TOK_EOF) break;
		lassert((char)tok->value_get() == out[i]);
		
		i++;
	} while (true);
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

int main(void)
{
	::lestes::lang::cplus::lex::trigraphs_test();
	return 0;
}
/* vim: set ft=lestes : */
