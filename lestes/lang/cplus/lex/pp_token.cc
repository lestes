/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Preprocessor token.

  Definition of pp_token class representing preprocessor token.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/equality.hh>
#include <lestes/std/source_location.hh>
#include <lestes/std/ucn_string.hh>
#include <lestes/lang/cplus/lex/pp_token.hh>
#include <lestes/lang/cplus/lex/basic_token.hh>
#include <lestes/lang/cplus/lex/token_value.hh>
#include <lestes/lang/cplus/lex/taboo_macros.hh>

#include <iostream>

package(lestes);
package(lang);
package(cplus);
package(lex);

using namespace ::std;

/*!
  Creates new token, initializes with location, token type, token value and flags.
  \pre a_taboo != NULL
  \post is_equal(location_get(),a_location)
  \post is_equal(type_get(),a_type)
  \post is_equal(value_get(),a_value)
  \param a_location  The initial location.
  \param a_type  The type.
  \param a_value  The value.
  \param a_alternative  The alternative spelling flag.
  \param a_taboo  The taboo.
*/
pp_token::pp_token(const location_type &a_location, const type_type &a_type,
	const value_type &a_value, bool a_alternative, const ptr<taboo_macros> &a_taboo):
	basic_pp_token(a_location,a_type,a_value),
	alternative(a_alternative),
	taboo(checked(a_taboo))
{
}

/*!
  Tests if token is name, that is identifier, keyword, true or false.
  \return true  If token is a name.
*/
bool pp_token::is_name(void) const
{
	return the_flags[type_get()] & FLG_NAME;
}

/*!
  Tests if token has alternative spelling.
  \return true  If token has alternative spelling.
*/
bool pp_token::is_alternative(void) const
{
	return alternative;
}

/*!
  Tests if token has value.
  \return true  If token has alternative spelling.
*/
bool pp_token::is_valued(void) const
{
	return the_flags[type_get()] & (FLG_EXTERNAL | FLG_INTERNAL);
}

/*!
  Returns internal token description dependent on token type, for debugging.
  \return  The token description.
*/
lstring pp_token::description_get(void) const
{
	return the_description[type_get()];
}

/*!
  Returns token spelling, reflecting the source.
  \return  String representation of the token.
*/
ucn_string pp_token::spelling_get(void) const
{
	pp_token_type ptt = type_get();
  
	switch (ptt) {
		case pp_token::TOK_STRING_LIT:
			{
				ucn_string us = "\"";
				us += value_get()->content_get();
				us += character::create_from_host('"');
				return us;
			}
		case pp_token::TOK_CHAR_LIT:
			{
				ucn_string us = "'";
				us += value_get()->content_get();
				us += character::create_from_host('\'');
				return us;
			}
		case pp_token::TOK_WSTRING_LIT:
			{
				ucn_string us = "L\"";
				us += value_get()->content_get();
				us += character::create_from_host('"');
				return us;
			}
		case pp_token::TOK_WCHAR_LIT:
			{
				ucn_string us = "L'";
				us += value_get()->content_get();
				us += character::create_from_host('\'');
				return us;
			}
		default:
			break;
	}

	if (is_valued()) {
		/* TODO pt remove
		if (is_name()) {
			ucn_string u;
			u += value_get()->content_get();
			u += taboo_get()->names_get();
			return u;
		}
		*/
		
		return value_get()->content_get();
	}

	return the_spelling[is_alternative()][ptt];
}

/*!
  Returns taboo macros for the token.
  \return The taboo macros for the token.
*/
ptr<taboo_macros> pp_token::taboo_get(void) const
{
	return taboo;
}

/*!
  Tests congruence of the token, that is equivalence considering
  only type, value and spelling.
  \param other  The token to compare with.
  \return true  If both tokens are congruent.
*/
bool pp_token::congruent(const ptr<pp_token> &other) const
{
	return other &&
		is_equal(type_get(),other->type_get()) &&
		is_equal(is_alternative(),other->is_alternative()) &&
		is_equal(value_get(),other->value_get());
}

/*!
  Tests equality of the token.
  \param other  The token to compare with.
  \return true  If both tokens are equal.
*/
bool pp_token::equals(const ptr<pp_token> &other) const
{
	// TODO pt taboo
	return other &&
		is_equal(type_get(),other->type_get()) &&
		is_equal(is_alternative(),other->is_alternative()) &&
		is_equal(location_get(),other->location_get()) &&
		is_equal(value_get(),other->value_get());
}

/*!
  Returns copy of this token, with different location.
  \pre a_location != NULL
  \param a_location  The new location 
  \return  New token which holds the same values and new location.
*/
ptr<pp_token> pp_token::clone(const location_type &a_location) const
{
	lassert(a_location);
	return new pp_token(a_location,type_get(),value_get(),is_alternative(),taboo_get());
}

/*!
  Returns copy of this token, with different taboo macros.
  \pre a_taboo != NULL
  \pre The token is name.
  \param a_taboo  The new taboo macros. 
  \return  New token which holds the same values and new taboo macros.
*/
ptr<pp_token> pp_token::clone(const ptr<taboo_macros> &a_taboo) const
{
	lassert(a_taboo);
	lassert(is_name());
	return new pp_token(location_get(),type_get(),value_get(),is_alternative(),a_taboo);
}

/*!
  Returns new token, initializes with location, token type.
  \pre The type does not need value or value is initialized internally.
  \post is_equal(location_get(),a_location)
  \post is_equal(type_get(),a_type)
  \param a_location  The location.
  \param a_type  The token type.
  \return  New token initialized with location and token type.
*/
ptr<pp_token> pp_token::create(const location_type &a_location, const type_type &a_type)
{
	initialize();
	lassert(!(the_flags[a_type] & FLG_EXTERNAL));
	return new pp_token(a_location,a_type,the_value[a_type],false,taboo_macros::create());
}

/*!
  Returns new token with alternative spelling, initializes with location and token type.
  \pre The type does not need value or value is initialized internally.
  \pre Alternative spelling is used only when available.
  \post is_equal(location_get(),a_location)
  \post is_equal(type_get(),a_type)
  \param a_location  The location.
  \param a_type  The token type.
  \return  New token initialized with location and token type.
*/
ptr<pp_token> pp_token::create_alternative(const location_type &a_location, const type_type &a_type)
{
	initialize();
	lassert(!(the_flags[a_type] & FLG_EXTERNAL));
	lassert(the_flags[a_type] & FLG_VARIANT);
	return new pp_token(a_location,a_type,the_value[a_type],true,taboo_macros::create());
}

/*!
  Returns new token, initializes with location, token type and token value.
  \pre The token type needs an external value.
  \post is_equal(location_get(),a_location)
  \post is_equal(type_get(),a_type)
  \post is_equal(value_get(),a_value)
  \param a_location  The initial location.
  \param a_type  The initial token type.
  \param a_value  The initial token value.
  \return  New token initialized with location, token type and token value.
*/
ptr<pp_token> pp_token::create(const location_type &a_location, const type_type &a_type,
	const value_type &a_value)
{
	initialize();
	lassert(the_flags[a_type] & FLG_EXTERNAL);
	return new pp_token(a_location,a_type,a_value,false,taboo_macros::create());
}

ptr<pp_token> pp_token::terminator(void)
{
	if (!terminator_instance) {
		terminator_instance = pp_token::create(source_location::zero(),pp_token::TOK_TERMINATOR);
	}
	return terminator_instance;
}

/*!
  Marks the object.
*/
void pp_token::gc_mark(void)
{
	taboo.gc_mark();
	basic_pp_token::gc_mark();
}

/*!
  Initializes helper static structures containing properties of existing token types.
  The initialization is invoked automatically prior to use of any instance.
*/
void pp_token::initialize(void)
{
	if (initialized) return;
	
	ulint i;

	i = 0;
#undef DEF_PP_TOKEN
#define DEF_PP_TOKEN(x,y,z,w) \
	the_description[i++] = #x;
	PP_TOKENS_LIST

	i = 0;
#undef DEF_PP_TOKEN
#define DEF_PP_TOKEN(x,y,z,w) \
	the_spelling[0][i] = y; the_spelling[1][i++] = z;
	PP_TOKENS_LIST

	i = 0;
#undef DEF_PP_TOKEN
#define DEF_PP_TOKEN(x,y,z,w) \
	the_value[i++] = ((w) & FLG_INTERNAL) ? token_value::create(ucn_string(y)) : ptr<token_value>(NULL);
	PP_TOKENS_LIST

	i = 0;
#undef DEF_PP_TOKEN
#define DEF_PP_TOKEN(x,y,z,w) \
	the_flags[i++] = w;
	PP_TOKENS_LIST

	initialized = true;
}

/*!
  Multiple initialization guard.
*/
bool pp_token::initialized;

/*!
  List of token internally initialized values.
*/
ptr<token_value> pp_token::the_value[TOKEN_TYPE_COUNT];

/*!
  List of token descriptions.
*/
lstring pp_token::the_description[TOKEN_TYPE_COUNT];

/*!
  List of token spellings.
*/
ucn_string pp_token::the_spelling[2][TOKEN_TYPE_COUNT];

/*!
  List of token flags.
*/
pp_token::flags_type pp_token::the_flags[TOKEN_TYPE_COUNT];

/*!
  Terminator token instance.
*/
ptr<pp_token> pp_token::terminator_instance = terminator_instance;

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

/* vim: set ft=lestes : */
