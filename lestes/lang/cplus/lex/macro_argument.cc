/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Macro argument.
  
  Definition of macro_argument class representing preprocessor macro argument.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/std/source_location.hh>
#include <lestes/lang/cplus/lex/macro_argument.hh>
#include <lestes/lang/cplus/lex/pp_token.hh>
#include <lestes/lang/cplus/lex/token_sequence.hh>
#include <lestes/lang/cplus/lex/token_value.hh>
#include <lestes/lang/cplus/lex/stringifier.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

/*!
  Creates empty macro argument.
  \post state == BEGIN
  \post nonexpanded == NULL
  \post expanded == NULL
  \post stringified == NULL
*/
macro_argument::macro_argument(void):
	state(BEGIN),
	nonexpanded(NULL),
	expanded(NULL),
	stringified(NULL)
{
}

/*!
  Parses single argument of macro. Front and back whitespace is discarded, newlines are converted to spaces.
  Value is stored into nonexpanded.
  \pre parse() was not called yet.
  \post state == DEAD || nonexpanded == contents of front part of input
  \param input  The source for arguments, parsed tokens (including terminating ones) are removed.
  \param first true If the argument is first in the argument list.
  \return macro_argument::LAST  When top level right parenthesis was encountered.
  \return macro_argument::EMPTY  When first argument was empty.
  \return macro_argument::CONTINUE  When top level comma was encountered. 
  \return macro_argument::ERROR  For end of file inside input.
*/
macro_argument::result_type macro_argument::parse(const ptr<token_input> &input, bool first)
{
	lassert(state == BEGIN);
	
	nonexpanded = token_sequence::create();
	result_type result = CONTINUE;
	ulint depth = 1;

	input->skip_front_ws();
	ptr<pp_token> t;
	pp_token_type ptt;

	ptr<pp_token> blank;
	
	while (input->peek_front()->type_get() == pp_token::TOK_LINE_END) {
		input->read_front();
	}
	
	do {
		t = input->read_front();
		ptt = t->type_get();

		switch (ptt) {
			case pp_token::TOK_TERMINATOR:
				state = DEAD;
				return ERROR;
			case pp_token::TOK_LEFT_PAR:
				depth++;
				break;
			case pp_token::TOK_RIGHT_PAR:
				depth--;
				if (depth != 0) break;
				result = nonexpanded->length() == 0 && first ? EMPTY : LAST;

				state = PARSED;
				return result;
			case pp_token::TOK_COMMA:
				if (depth != 1) break;

				state = PARSED;
				return result;
			case pp_token::TOK_LINE_END:
				if (blank) continue;
				blank = pp_token::create(t->location_get(),pp_token::TOK_BLANK);
				break;
			case pp_token::TOK_BLANK:
				if (blank) continue;
				blank = t;
				break;
			default:
				break;
		}
		if (blank) {
			nonexpanded->add_back(blank);
			blank = NULL;
		}
		nonexpanded->add_back(t);
	} while (true);
}

/*!
  Returns internal state of the object.
  \return  The internal state of the object.
*/
macro_argument::state_type macro_argument::state_get(void) const
{
	return state;
}

/*!
  Returns the nonexpanded macro argument.
  \pre parse() != ERROR
  \return The nonexpanded token sequence.
*/  
ptr<token_sequence> macro_argument::nonexpanded_get(void) const
{
	lassert(state == PARSED);
	return nonexpanded->clone();
}

/*!
  Returns the completely expanded macro argument. Expansion is performed only once.
  \pre parse() != ERROR
  \return The completely expanded token sequence.
*/
ptr<token_sequence> macro_argument::expanded_get(const ptr<macro_storage> &macros)
{
	lassert(state == PARSED);
	if (!expanded) {
		expanded = nonexpanded->clone();
		expanded = expanded->expand_all(macros);
	}
	return expanded->clone();
}

/*!
  Returns stringified macro argument. Stringification is performed only once.
  \pre parse() != ERROR
  \return Token sequence containing single token with value equal to the stringified token sequence.
*/
ptr<token_sequence> macro_argument::stringified_get(void)
{
	lassert(state == PARSED);
	if (!stringified) {
		ptr<pp_token> t = stringifier::instance()->process(nonexpanded->clone());
		stringified = token_sequence::create();
		stringified->add_back(t);
	}
	return stringified->clone();
}

/*!
  Tests equality to other macro argument. Only the nonexpanded sequences are compared.
  \param other  The macro argument to compare with.
  \return true  If both the objects are equal.
*/ 
bool macro_argument::equals(const ptr<macro_argument> &other) const
{
	if (!other || state_get() != other->state_get()) return false;
	if (state != PARSED) return true;
	
	// compare nonexpanded
	return is_equal(nonexpanded_get(),other->nonexpanded_get());
}

/*!
  Marks the object.
*/
void macro_argument::gc_mark(void)
{
	nonexpanded.gc_mark();
	expanded.gc_mark();
	stringified.gc_mark();
	::lestes::std::object::gc_mark();
}

/*!
  Returns new empty macro argument.
  \post state == BEGIN
  \post nonexpanded == NULL
  \post expanded == NULL
  \post stringified == NULL
  \return New instance of the object.
*/
ptr<macro_argument> macro_argument::create(void)
{
	return new macro_argument();
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);
/* vim: set ft=lestes : */
