/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Unit test.
  
  Unit test for class encoder_ascii7.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/string_source.hh>
#include <lestes/lang/cplus/lex/encoder_ascii7.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

using namespace ::std;

#define TEST_CNT 9

/*!
  \brief Tests encoder_ascii7 class.

  Performs testing of encoder_ascii7 class.
*/
void encoder_ascii7_test(void)
{
	char *in[TEST_CNT] = {
			/* allowed */
			"\x01"
		,
			/* allowed */
			"\x4D"
		,
			/* allowed */
			"\x7E"
		,
			/* allowed */
			"\x7F"
		,
			/* disallowed */
			"\x80"
		,
			/* disallowed */
			"\x81"
		,
			/* disallowed */
			"\x90"
		,
			/* disallowed */
			"\xAF"
		,
			/* disallowed */
			"\xFF"
	};

	ucn_token_type out[] = {
		ucn_token::TOK_NOT_EOF,
		ucn_token::TOK_EOF,
		ucn_token::TOK_NOT_EOF,
		ucn_token::TOK_EOF,
		ucn_token::TOK_NOT_EOF,
		ucn_token::TOK_EOF,
		ucn_token::TOK_NOT_EOF,
		ucn_token::TOK_EOF,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
		ucn_token::TOK_ERROR,
	};

	ptr<ucn_token> tok;
	ucn_token_type utt;
	ulint test, i;

	for (i = test = 0; test < TEST_CNT; test++) {
		ptr<data_source> ds = string_source::create(string_source::string_type(in[test]));
		ptr<encoder_ascii7> enc = encoder_ascii7::create();

		enc->input_set(ds);

		while (true) {
			tok = enc->read();
			utt = tok->type_get();
			lassert(utt == out[i]);
			i++;
			if (utt == ucn_token::TOK_EOF || utt == ucn_token::TOK_ERROR) break;
		}
	}
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

/*!
  \brief Main function.

  Runs the unit test in different namespace.
*/
int main(void)
{
	::lestes::lang::cplus::lex::encoder_ascii7_test();
	return 0;
}
/* vim: set ft=lestes : */
