/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___pp_filter_hh___included
#define lestes__lang__cplus__lex___pp_filter_hh___included

/*! \file
  \brief Token filter.

  Declaration of pp_filter class representing token filter.
  \author pt
*/
#include <lestes/common.hh>
package(lestes);
package(lang);
package(cplus);
package(lex);

// forward declaration to avoid cycle
class pp_token;

/*!
  \brief Token filter.

  Represents filter of preprocessing tokens.
*/
class pp_filter: public ::lestes::std::object {
public:
	//! Sets the input coming into filter.
	// TODO pt remove
	//virtual void input_set(const ptr<pp_filter> &a_input);
	//! Returns input coming into filter.
	virtual ptr<pp_filter> input_get(void) const;
	//! Reads single token from the filter.
	virtual ptr<pp_token> read(void) abstract;
	//! Marks the object.
	virtual void gc_mark(void);
protected:
	//! Creates new object, initializes with no input.
	pp_filter(void);
	//! Creates new object, initializes with input.
	pp_filter(const ptr<pp_filter> &a_input);
	//! Reads single token from input.
	ptr<pp_token> input_read(void);
private:
	//! Hides copy constructor.
	pp_filter(const pp_filter &copy);
	//! Hides assignment operator.
	pp_filter &operator=(const pp_filter &rhs);
	//! Input into this filter.
	srp<pp_filter> input;
};

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
