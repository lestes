/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Line joining filter.
  
  Definition of line_join class performing joining of lines.
  \author pt
*/
#include <lestes/common.hh> 
#include <lestes/lang/cplus/lex/line_join.hh>
#include <lestes/lang/cplus/lex/ucn_filter.hh>
package(lestes);
package(lang);
package(cplus);
package(lex);

using namespace ::std;

/*!
  Creates the object.
  \post state == START
*/
line_join::line_join(void):
	state(START)
{
}

/*!
  Reads next token, discards backslash newline sequences.
  If backslash newline is encountered before EOF,
  the newline is returned to avoid reporting no newline
  at the end of file.
  
*/
ptr<ucn_token> line_join::read(void)
{
	ptr<ucn_token> t;
	
	if (state == SAVED) {
		// something left in buffer
		t = saved;
		// release reference
		saved = NULL;
		state = START;
		return t;
	}

	ucn_token_type utt;
	ucn u;

	while (true) {
		// read new token
		t = input_read();
		utt = t->type_get();
		u = t->value_get();
		switch (state) {
			case AFTER:
				if (utt == ucn_token::TOK_EOF) {
					// get the newline from the last pair
					ptr<ucn_token> tmp = saved;
					// save the EOF
					saved = t;
					state = SAVED;
					return tmp;
				}
				saved = NULL;
				state = START;
				// fall through
			case START:
				if (utt == ucn_token::TOK_BASIC && u == character::ascii_backslash) {
					// save the location
					saved = t;
					state = BACKSLASH;
				} else {
					return t;
				}
				break;
			case BACKSLASH:
				// backslash newline combination
				if (utt == ucn_token::TOK_BASIC && u == character::ascii_new_line) {
					// switch to the newline in case EOF follows
					saved = t;
					// start anew
					state = AFTER;
				} else {
					if (utt != ucn_token::TOK_BASIC || u != character::ascii_backslash) {
						// character different from backslash
						state = SAVED;
					}
					// get the backslash
					ptr<ucn_token> tmp = saved;
					// save current
					saved = t;
					// return the backslash
					return tmp;
				}
				break;
			default:
				lassert2(false,"You should never get here");
		}
	}
}

/*!
  Marks the object.
*/
void line_join::gc_mark(void)
{
	saved.gc_mark();
	ucn_filter::gc_mark();
}

/*!
  Returns new instance.
  \return  New instance of the class.
*/
ptr<line_join> line_join::create(void)
{
	return new line_join();
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

/* vim: set ft=lestes : */
