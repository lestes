/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___macro_hh___included
#define lestes__lang__cplus__lex___macro_hh___included

/*! \file
  \brief Preprocessor macro.

	Declaration of macro class representing preprocessor macro.
	\author pt
*/

#include <lestes/common.hh>

package(lestes);

package(std);
// forward declarations to avoid cycle
class source_location;
end_package(std);

package(lang);
package(cplus);
package(lex);

// forward declaration to avoid cycle
class pp_token;
class token_value;
class token_input;
class token_sequence;
class macro_item;
class macro_head;
class macro_storage;
class macro_body;

/*!
  \brief Preprocessor macro.

  Represents stored preprocessor macro.
*/
class macro: public ::lestes::std::object {
public:
	//! Returns definition location.
	ptr<source_location> location_get(void) const;
	//! Returns macro name.
	ptr<token_value> name_get(void) const;
	//! Returns function-like flag.
	bool funlike_get(void) const;
	//! Returns predefined flag.
	bool predefined_get(void) const;
	//! Expands macro.
	ptr<token_sequence> expand(
			// TODO pt rename
			const ptr<pp_token> &name, 
			const ptr<token_input> &input,
		const ptr<macro_storage> &macros);
	//! Parses the macro definition.
	bool parse(const ptr<token_input> &ts);
	//! Tests equality.
	bool equals(const ptr<macro> &other) const;
	//! Creates empty object.
	static ptr<macro> create(void);
	//! Creates predefined macro.
	static ptr<macro> create_predefined(void);
protected:
	//! Constructs empty object.
	macro(void);
	//! Marks the object.
	virtual void gc_mark(void);
private:
	//! Type of internal state.
	typedef enum { BEGIN, PARSED, DEAD } state_type;
	//! Parses name.
	bool parse_name(const ptr<token_input> &input);
	//! Parses parameter list.
	bool parse_head(const ptr<token_input> &input);
	//! Parses expansion list.
	bool parse_body(const ptr<token_input> &input);
	//! Internal state of the object.
	state_type state;
	//! Location of definition.
	srp<source_location> location;
	//! Name of the macro.
	srp<token_value> name;
	//! Flag designating function-like macro.
	bool funlike;
	//! Flag designating predefined macro.
	bool predefined;
	//! Parameter list of the macro.
	srp<macro_head> head;
	//! Expansion list of the macro.
	srp<macro_body> body;
};

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);
#endif
/* vim: set ft=lestes : */
