/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___string_joiner_hh___included
#define lestes__lang__cplus__lex___string_joiner_hh___included

/*! \file
  \brief String literal joining filter.

  Declaration of string_joiner class performing joining of string literals.
  \author pt
*/
#include <lestes/common.hh> 
#include <lestes/std/vector.hh> 
#include <lestes/lang/cplus/lex/pp_filter.hh>
#include <lestes/lang/cplus/lex/pp_token.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

/*!
  \brief String literal joining filter.
  
  Performs joining of adjacent string literals and adjacent wide string literals.
*/
class string_joiner: public pp_filter {
public:
	//! Reads next token.
	ptr<pp_token> read(void);
	//! Returns new instance.
	static ptr<string_joiner> create(const ptr<pp_filter> &a_input);
protected:
	//! Creates the object.
	string_joiner(const ptr<pp_filter> &a_input);
	//! Marks the object.
	virtual void gc_mark(void);
private:
	//! States of the processor.
	typedef enum {
		//! No tokens in buffer.
		START = 0,
		//! Read first string.
		FIRST_STRING = 1,
		//! Reading string sequence.
		NEXT_STRING = 2,
		//! Read first wide string.
		FIRST_WIDE_STRING = 3,
		//! Reading wide string sequence.
		NEXT_WIDE_STRING = 4,
		//! Reading mixed string and wide string sequence.
		NEXT_BAD_STRING = 5,
		//! Ordinary token saved in buffer.
		SAVED = 6,
	} state_type;
	//! Type of sequence of adjacent literals.
	typedef ::lestes::std::vector< srp<pp_token> > sequence_type;
	//! Current state of the processor.
	state_type state;
	//! Buffered token.
	srp<pp_token> saved;
	//! Current sequence of adjacent literals.
	srp<sequence_type> sequence;
	//! Returns joined string literal.
	ptr<pp_token> join_sequence(pp_token_type ptt) const;
	//! Hides copy constructor.
	string_joiner(const string_joiner &);
	//! Hides assignment operator.
	string_joiner &operator=(const string_joiner &);
};

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
