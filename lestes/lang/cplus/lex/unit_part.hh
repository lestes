/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___unit_part_hh___included
#define lestes__lang__cplus__lex___unit_part_hh___included

/*! \file
  \brief Part of translation unit.

  Declaration of unit_part class representing a part of the translation unit.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/pp_filter.hh>

package(lestes);

package(std);
// forward declarations to avoid cycle
class source_location;
class file_info;
end_package(std);

package(lang);
package(cplus);
package(lex);

// forward declaration to break cycle
class pp_token;
class data_source;
class encoder;
class pre_lex;
class pp_lex;
class expander;
class condition_stack;
class macro_storage;
class line_control;

/*!
  \brief Part of translation unit.

  Represents a part of translation unit, which is either the original
  source file itself, or any file included by the source file.
*/
class unit_part: public pp_filter {
public:
	//! Sets start of line flag.
	void start_of_line(void);
	// TODO pt remove
	//! Activates connection to flex before start of reading.
	//void activate(void);
	//! Returns associated condition stack.
	ptr<condition_stack> conditions_get(void) const;
	//! Returs associated expander.
	ptr<expander> expander_get(void) const;
	//! Returs the line control.
	ptr<line_control> line_control_get(void) const;
	/* TODO pt remove
	//! Updates line information.
	void line_change(lstring file_name, ulint a_line);
	*/
	//! Reads next token.
	ptr<pp_token> read(void);
	//! Returns new unit, initializes with file information, data source and encoder.
	static ptr<unit_part> create(const ptr<file_info> &a_file,
			const ptr<data_source> &a_data, const ptr<encoder> &a_encoder, 
			const ptr<macro_storage> &a_macros);
protected:
	//! Creates new unit, initializes with file information, data source and encoding.
	unit_part(const ptr<file_info> &a_file, 
		const ptr<data_source> &a_data, const ptr<encoder> &a_encoder,
		const ptr<macro_storage> &a_macros);
	//! Marks the object.
	virtual void gc_mark(void);
private:
	// TODO pt add representation of file name and path (for nested includes)
	// TODO pt remove
	//! The base location.
	//srp<source_location> location;
	//! Start of line flag.
	bool start;
	//! Conditions stack.
	srp<condition_stack> conditions;
	//! Character set encoder.
	srp<encoder> enc;
	//! Data source.
	srp<data_source> ds;
	//! Operations prior to lexical analysis.
	srp<pre_lex> plx;
	//! Line control.
	srp<line_control> lic;
	//! Flex wrapper.
	srp<pp_lex> ppl;
	//! Expander.
	srp<expander> exp;
	//! Hides copy constructor.
	unit_part(const unit_part &);
	//! Hides assignment operator.
	unit_part &operator=(const unit_part &);
};

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
