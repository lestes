/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Named input stream.
  
  Definition of named_istream class representing named input stream.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/std/istream_wrapper.hh>
#include <lestes/lang/cplus/lex/named_istream.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

/*!
  Creates the object.
  \param a_stream  The represented stream.
  \param a_file_name  The path and name of the associated file.
  \param a_print_name  The print name of the associated file.
*/
named_istream::named_istream(const ptr<istream_wrapper> &a_stream,
		const lstring &a_file_name, const lstring &a_print_name):
	stream(checked(a_stream)),
	file_name(a_file_name),
	print_name(a_print_name)
{
}

/*!
  Returns the name of the associated file.
  \return  The path and file name.
*/
lstring named_istream::file_name_get(void) const
{
	return file_name;
}

/*!
  Returns the print name of the stream
  \return  The print name, usually the file name itself.
*/
lstring named_istream::print_name_get(void) const
{
	return print_name;
}

/*!
  Returns the represented stream.
  \return  The stream.
*/
ptr<istream_wrapper> named_istream::stream_get(void) const
{
	return stream;
}

/*!
  Marks the object.
*/
void named_istream::gc_mark(void)
{
	stream.gc_mark();
	object::gc_mark();
}

/*!
  Returns the named input stream.
  \pre a_stream != NULL
  \param a_stream  The represented stream.
  \param a_file_name  The name of the associated file.
  \param a_print_name  The print name of the associated file.
*/
ptr<named_istream> named_istream::create(const ptr<istream_wrapper> &a_stream,
		const lstring &a_file_name, const lstring &a_print_name)
{
	return new named_istream(a_stream,a_file_name,a_print_name);
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

/* vim: set ft=lestes : */
