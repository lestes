/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___stringifier_hh___included
#define lestes__lang__cplus__lex___stringifier_hh___included

/*! \file
  \brief Token stringifier.
  
  Declaration of stringifier class performing token stringification.
  \author pt
*/
#include <lestes/common.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

// forward declaration to break cycle
class pp_token;
class token_input;

/*!
  \brief Token stringifier. 
  
  Performs token stringification.
*/
class stringifier: public ::lestes::std::object {
public:
	//! Stringifies tokens.
	ptr<pp_token> process(const ptr<token_input> &input);
	//! Returns the only instance.
	static ptr<stringifier> instance(void);
protected:
	//! Creates the object.
	stringifier(void);
private:
	//! Adds guard characters to disable escape sequences.
	ucn_string escape_spelling(const ptr<pp_token> &tok);
	//! Hides copy constructor.
	stringifier(const stringifier &);
	//! Hides assignment operator.
	stringifier &operator=(const stringifier &);
	//! The only instance of the class.
	static ptr<stringifier> singleton;
};

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
