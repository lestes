/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  Unit test for pre_lex class.
  \author pt
*/

#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/ucn_token.hh>
#include <lestes/lang/cplus/lex/pre_lex.hh>
#include <lestes/lang/cplus/lex/string_source.hh>
#include <lestes/lang/cplus/lex/encoder_host.hh>
#include <lestes/lang/cplus/lex/simple_location.hh>
#include <lestes/std/vector.hh>

#include <iostream>

package(lestes);
package(lang);
package(cplus);
package(lex);

typedef vector< srp<simple_location> > vec_loc;

void pre_lex_test(void)
{
	char inp[] = "abc$@`\n?""?""?""?""?=?""?/?""?(?""?)?""?!?""?<?""?>?""?-?""?'?""?+\\\nx\n";
	char ouv[] = "abc$@`\n?""?""?#\\[]|{}~^?""?+x\n";
	ulint out[] = {
		ucn_token::TOK_BASIC,
		ucn_token::TOK_BASIC,
		ucn_token::TOK_BASIC,
		
		ucn_token::TOK_TRANSLATED,
		ucn_token::TOK_TRANSLATED,
		ucn_token::TOK_TRANSLATED,
		
		ucn_token::TOK_BASIC,
		
		ucn_token::TOK_BASIC,
		ucn_token::TOK_BASIC,
		ucn_token::TOK_BASIC,

		ucn_token::TOK_BASIC,
		ucn_token::TOK_BASIC,
		ucn_token::TOK_BASIC,
		ucn_token::TOK_BASIC,
		ucn_token::TOK_BASIC,
		ucn_token::TOK_BASIC,
		ucn_token::TOK_BASIC,
		ucn_token::TOK_BASIC,
		ucn_token::TOK_BASIC,

		ucn_token::TOK_BASIC,
		ucn_token::TOK_BASIC,
		ucn_token::TOK_BASIC,

		ucn_token::TOK_BASIC,
		
		ucn_token::TOK_BASIC,
	};

	ptr<vec_loc> loc = vec_loc::create();

	loc->push_back(simple_location::create(1,1));
	loc->push_back(simple_location::create(1,2));
	loc->push_back(simple_location::create(1,3));
	loc->push_back(simple_location::create(1,4));
	loc->push_back(simple_location::create(1,5));
	loc->push_back(simple_location::create(1,6));
	loc->push_back(simple_location::create(1,7));
		
	loc->push_back(simple_location::create(2,1));
	loc->push_back(simple_location::create(2,2));
	loc->push_back(simple_location::create(2,3));
		
	loc->push_back(simple_location::create(2,4));      
	loc->push_back(simple_location::create(2,7));
	loc->push_back(simple_location::create(2,10));
	loc->push_back(simple_location::create(2,13));
	loc->push_back(simple_location::create(2,16));
	loc->push_back(simple_location::create(2,19));
	loc->push_back(simple_location::create(2,22));
	loc->push_back(simple_location::create(2,25));
	loc->push_back(simple_location::create(2,28));
		
	loc->push_back(simple_location::create(2,31));
	loc->push_back(simple_location::create(2,32));
	loc->push_back(simple_location::create(2,33));
	loc->push_back(simple_location::create(3,1));
	loc->push_back(simple_location::create(3,2));
		
	ulint i;

	ptr<pre_lex> plx = pre_lex::create(string_source::create(inp),encoder_host::create());
	ptr<ucn_token> tok;

	i = 0;
	do {
		
		tok = plx->read();
		if (tok->type_get() == ucn_token::TOK_EOF) break;
#if 0
		::std::cerr << " test " << i << " : " <<
			"type " <<
			(int)tok->type_get() << " ?== " << out[i] << 
			"value " <<
			(int)tok->value_get() << " ?== " << (int)ouv[i] << 
			" line " <<
			(int)tok->location_get()->line_get() << " ?== " << loc->at(i)->line_get() << 
			" column " <<
			(int)tok->location_get()->column_get() << " ?== " << loc->at(i)->column_get() 
			<< ::std::endl;
#endif
		lassert(is_equal((char)tok->value_get(),ouv[i]));
		lassert(is_equal((char)tok->value_get(),ouv[i]));
		lassert(is_equal(tok->type_get(),out[i]));
		lassert(is_equal(tok->location_get(),ptr<simple_location>(loc->at(i))));
		
		i++;
	} while (true);
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

int main(void)
{
	::lestes::lang::cplus::lex::pre_lex_test();
	return 0;
}
/* vim: set ft=lestes : */
