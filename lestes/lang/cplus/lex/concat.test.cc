/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Unit test.
  
  Unit test for class concat.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/equality.hh>
#include <lestes/std/source_location.hh>
#include <lestes/std/file_info.hh>
#include <lestes/lang/cplus/lex/concat.hh>
#include <lestes/lang/cplus/lex/pp_token.hh>
#include <lestes/lang/cplus/lex/token_sequence.hh>
#include <lestes/lang/cplus/lex/token_value.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

using namespace ::std;

/*!
  \brief Tests concat class.

  Performs testing of concat class.
*/
void concat_test(void)
{
	ptr<concat> cat = concat::instance();
	ptr<file_info> fi = file_info::create("<testing>",NULL);
	ptr<token_sequence> ts;
	
	ptr<source_location> l = source_location::create(fi,1,1);
	ptr<source_location> m = source_location::create(fi,1,2);
	ptr<pp_token> t = pp_token::create(l,pp_token::TOK_PLUS);
	ptr<pp_token> u = pp_token::create(m,pp_token::TOK_EQ);
	ts = cat->process(t,u);
	lassert(ts);
	lassert(ts->length() == 1);
	ptr<pp_token> r = ts->read_front();
	lassert(r);
	lassert(is_equal(r->type_get(),pp_token::TOK_PLUS_EQ));
	lassert(is_equal(r->location_get(),l));

	ts = cat->process(u,t);
	lassert(ts);
	lassert(ts->length() == 2);
	r = ts->read_front();
	lassert(is_equal(r,u));
	ptr<pp_token> s = ts->read_front();
	lassert(is_equal(s,t));
	
	t = pp_token::create(l,pp_token::TOK_IDENT,token_value::create("L"));
	u = pp_token::create(m,pp_token::TOK_STRING_LIT,token_value::create("abc"));
	ts = cat->process(t,u);
	lassert(ts->length() == 1);
	r = ts->read_front();
	lassert(r);
	lassert(is_equal(r->type_get(),pp_token::TOK_WSTRING_LIT));
	lassert(is_equal(r->location_get(),l));

}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

/*!
  \brief Main function.

  Runs the unit test in different namespace.
*/
int main(void)
{
	::lestes::lang::cplus::lex::concat_test();
	return 0;
}
/* vim: set ft=lestes : */
