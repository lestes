/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Unit test.

  Unit test for class macro_head.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/std/source_location.hh>
#include <lestes/lang/cplus/lex/macro_head.hh>
#include <lestes/lang/cplus/lex/token_sequence.hh>
#include <lestes/lang/cplus/lex/token_value.hh>
#include <lestes/lang/cplus/lex/pp_token.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

using namespace ::std;

/*!
  \brief Tests macro_head class.

  Performs testing of macro_head class.
*/
void macro_head_test(void)
{
	ptr<file_info> fi = file_info::create("abc",NULL);
	ptr<source_location> loc = source_location::create(fi,0,0);

	ptr<token_value> tv1 = token_value::create("abc");
	ptr<token_value> tv2 = token_value::create("def");
	
	ptr<token_sequence> ts = token_sequence::create();
	
	ts->add_back(pp_token::create(loc,pp_token::TOK_BLANK));
	ts->add_back(pp_token::create(loc,pp_token::TOK_BLANK));
	ts->add_back(pp_token::create(loc,pp_token::TOK_LINE_END));
		
	ts->add_back(pp_token::create(loc,pp_token::TOK_BLANK));
	ts->add_back(pp_token::create(loc,pp_token::TOK_DOT));
		
	ts->add_back(pp_token::create(loc,pp_token::TOK_IDENT,tv1));
	ts->add_back(pp_token::create(loc,pp_token::TOK_BLANK));
	ts->add_back(pp_token::create(loc,pp_token::TOK_DOT));

	ts->add_back(pp_token::create(loc,pp_token::TOK_IDENT,tv1));
	ts->add_back(pp_token::create(loc,pp_token::TOK_COMMA));
	ts->add_back(pp_token::create(loc,pp_token::TOK_IDENT,tv1));

	ts->add_back(pp_token::create(loc,pp_token::TOK_IDENT,tv1));
	ts->add_back(pp_token::create(loc,pp_token::TOK_COMMA));
	ts->add_back(pp_token::create(loc,pp_token::TOK_IDENT,tv2));
	ts->add_back(pp_token::create(loc,pp_token::TOK_RIGHT_PAR));

	ts->add_back(pp_token::create(loc,pp_token::TOK_IDENT,tv2));
	ts->add_back(pp_token::create(loc,pp_token::TOK_COMMA));
	ts->add_back(pp_token::create(loc,pp_token::TOK_IDENT,tv1));
	ts->add_back(pp_token::create(loc,pp_token::TOK_RIGHT_PAR));

	ptr<macro_head> mh0 = macro_head::create();

	// created object
	lassert(is_equal(mh0,mh0));
	lassert(is_equal(mh0->state_get(),macro_head::BEGIN));
	
	// eol 
	lassert(is_equal(mh0->parse(ts),false));
	lassert(is_equal(mh0->state_get(),macro_head::DEAD));
	lassert(is_equal(mh0,mh0));

	ptr<macro_head> mh1 = macro_head::create();

	// nonidentifier 
	lassert(is_equal(mh1->parse(ts),false));
	lassert(is_equal(mh1->state_get(),macro_head::DEAD));

	ptr<macro_head> mh2 = macro_head::create();

	// ident and noncomma 
	lassert(is_equal(mh2->parse(ts),false));
	lassert(is_equal(mh2->state_get(),macro_head::DEAD));

	ptr<macro_head> mh3 = macro_head::create();

	// duplicate ident 
	lassert(is_equal(mh3->parse(ts),false));
	lassert(is_equal(mh3->state_get(),macro_head::DEAD));

	ptr<macro_head> mh4 = macro_head::create();

	// two distinct idents 
	lassert(is_equal(mh4->parse(ts),true));
	lassert(is_equal(mh4->state_get(),macro_head::PARSED));
	lassert(is_equal(mh4,mh4));
	lassert(is_equal(mh4->length(),2U));
	lassert(is_equal(mh4->index_of(tv1),0U));
	lassert(is_equal(mh4->index_of(tv2),1U));
	lassert(is_equal(mh4->index_of(token_value::create("ghi")),mh4->length()));

	ptr<macro_head> mh5 = macro_head::create();

	// swapped idents
	lassert(is_equal(mh5->parse(ts),true));
	lassert(is_equal(mh5->state_get(),macro_head::PARSED));
	lassert(!is_equal(mh5,mh4));
	lassert(is_equal(mh5->length(),2U));
	lassert(is_equal(mh5->index_of(tv2),0U));
	lassert(is_equal(mh5->index_of(tv1),1U));
	lassert(is_equal(mh5->index_of(token_value::create("ghi")),mh5->length()));

}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

/*!
  \brief Main function.

  Runs the unit test in different namespace.
*/
int main(void)
{
	::lestes::lang::cplus::lex::macro_head_test();
	return 0;
}
/* vim: set ft=lestes : */
