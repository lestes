/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Unit test.

  Unit test for class condition.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/equality.hh>
#include <lestes/lang/cplus/lex/condition.hh>
#include <lestes/std/file_info.hh>
#include <lestes/std/source_location.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

using namespace ::std;

/*!
  \brief Tests condition class.

  Performs testing of condition class.
*/
void condition_test(void)
{
	ptr<source_location> loc = source_location::create(file_info::create("abc",NULL),1,1);

	// empty condition
	ptr<condition> c0 = condition::create_empty();
	lassert(is_equal(c0,c0));
	lassert(is_equal(c0->type_get(),condition::COND_EMPTY));
	lassert(is_equal(c0->active_get(),true));

	// normal condition
	ptr<condition> c1 = condition::create(condition::COND_IF,true,false,loc);
	lassert(is_equal(c1,c1));
	lassert(!is_equal(c1,c0));
	lassert(is_equal(c1->type_get(),condition::COND_IF));
	lassert(is_equal(c1->waiting_get(),true));
	lassert(is_equal(c1->active_get(),false));
	lassert(is_equal(c1->location_get(),loc));

	// another normal condition
	ptr<condition> c2 = condition::create(condition::COND_ELIF,false,true,loc);
	lassert(is_equal(c2,c2));
	lassert(!is_equal(c2,c1));
	lassert(is_equal(c2->type_get(),condition::COND_ELIF));
	lassert(is_equal(c2->waiting_get(),false));
	lassert(is_equal(c2->active_get(),true));
	lassert(is_equal(c2->location_get(),loc));

	// same condition
	ptr<condition> c3 = condition::create(condition::COND_ELIF,false,true,loc);
	lassert(is_equal(c3,c3));
	lassert(!is_equal(c3,c1));
	lassert(is_equal(c3,c2));
	lassert(is_equal(c2->type_get(),condition::COND_ELIF));
	lassert(is_equal(c2->waiting_get(),false));
	lassert(is_equal(c2->active_get(),true));
	lassert(is_equal(c2->location_get(),loc));
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

/*!
  \brief Main function.

  Runs the unit test in different namespace.
*/
int main(void)
{
	::lestes::lang::cplus::lex::condition_test();
	return 0;
}
/* vim: set ft=lestes : */
