/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Macro expansion list.
  
  Definition of macro_body class representing macro expansion list.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/std/source_location.hh>
#include <lestes/lang/cplus/lex/macro_body.hh>
#include <lestes/lang/cplus/lex/macro_body.m.hh>
#include <lestes/lang/cplus/lex/macro_head.hh>
#include <lestes/lang/cplus/lex/macro_item.hh>
#include <lestes/lang/cplus/lex/token_sequence.hh>
#include <lestes/lang/cplus/lex/pp_token.hh>
#include <lestes/lang/cplus/lex/token_value.hh>
#include <lestes/lang/cplus/lex/macro_argument.hh>
#include <lestes/lang/cplus/lex/macro_arguments.hh>
#include <lestes/lang/cplus/lex/concat.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

using namespace ::std;

/*!
  Constructs empty expansion list.
  \post state == BEGIN
  \post items->size() == 0
*/
macro_body::macro_body(void):
	state(BEGIN),
	items(items_type::create())
{
}

/*!
  Parses function like macro replacement list and stores values into self.
  \pre state == BEGIN
  \param input  The source for tokens, starting after parameter list.
  \return false  If the replacement list was ill-formed.
*/
bool macro_body::parse(const ptr<token_input> &input, const ptr<macro_head> &head)
{
	lassert(state == BEGIN);
	// state of parser
	enum {
		BEGIN,
		LITERAL,
		EXPAND,
		NONEXPAND,
		STRINGIFY,
		CONCAT,
		NEWLINE,
		END
	} fstate = BEGIN;
	ptr<pp_token> t;
	ptr<token_sequence> literal;
	ulint idx;

	do {
		// TODO pt ::std::cerr << "fstate = " << fstate["blenscXQ"] << '\n';
		if (fstate == BEGIN)
			t = input->read_front_skip_ws();
		else
			t = input->read_front();
		
		if (t->is_name()) {
			idx = head->index_of(t->value_get());
			// if idx is invalid
			if (idx == head->length()) {
				// ordinary identifier is a part of a literal
				if (fstate != LITERAL) {
					literal = token_sequence::create();
					fstate = LITERAL;
				}
				literal->add_back(t);
			} else {
				if (fstate == CONCAT) {
					// no expansion after concatenation
					items->push_back(macro_item::create_copy(idx));
					fstate = NONEXPAND;
				} else {
					if (fstate == LITERAL) {
						// flush the literal
						items->push_back(macro_item::create_literal(literal));
					}
					items->push_back(macro_item::create_expansion(idx));
					fstate = EXPAND;
				}
			}
		} else if (t->type_get() == pp_token::TOK_HASH) {
			if (fstate == LITERAL) {
				// flush the literal
				items->push_back(macro_item::create_literal(literal));
			}
		  
			literal = token_sequence::create();
			// save the hash
			literal->add_back(t);
			t = input->read_front();

			if (t->type_get() == pp_token::TOK_BLANK) {
				// add the blank
				literal->add_back(t);
				// get the parameter
				t = input->read_front_skip_ws();
			}

			if (!t->is_name() ||
				 (idx = head->index_of(t->value_get())) == head->length()) {
				// # operator without parameter
				report << hash_without_parameter << t->location_get();
				// need cleanup till the end of line
				if (t->type_get() != pp_token::TOK_LINE_END) goto error_cleanup;
				state = DEAD;
				return false;
			}
			items->push_back(macro_item::create_str(idx,literal));

			fstate = STRINGIFY;
		} else if (t->type_get() == pp_token::TOK_HASH_HASH) {
			switch (fstate) {
				case BEGIN:
					// ## at the beginning of expansion list
					report << hash_hash_at_beginning << t->location_get();
					goto error_cleanup;
				case LITERAL:
					// check whether there is EXPAND preceding
					if (!(items->size() && literal->length() == 1 && literal->peek_front() == pp_token::TOK_BLANK &&
						 items->back()->action_get() == macro_item::EXPANSION)) break;
					// fall through
				case EXPAND:
					{
						// update preceding expansion
						ptr<macro_item> mbi = items->back();
						items->pop_back();
						// update to avoid expansion
						items->push_back(macro_item::create_copy(mbi->index_get()));
					}
					break;
				case STRINGIFY:
				case CONCAT:
					// okay
					break;
				case END:
				default:
					lassert2(false,"You should never get here");
					break;
			}
			if (fstate == LITERAL) {
				// flush the literal
				items->push_back(macro_item::create_literal(literal));
			}

			literal = token_sequence::create();
			literal->add_back(t);

			items->push_back(macro_item::create_concat(literal));
			fstate = CONCAT;
		} else if (t->type_get() == pp_token::TOK_LINE_END) {
			if (fstate == CONCAT) {
				// ## at the end of the expansion list
				report << hash_hash_at_end << t->location_get();
				state = DEAD;
				return false;
			}
			if (fstate == LITERAL && literal->skip_back_ws()) {
				// flush the literal
				items->push_back(macro_item::create_literal(literal));
			}
			fstate = END;
		} else {
			if (fstate != LITERAL) {
				literal = token_sequence::create();
				fstate = LITERAL;
			}
			literal->add_back(t);
		}
	} while (fstate != END);
	state = PARSED;
	return true;
error_cleanup:
	// discard tokens till end of line
	do {
		t = input->read_front_skip_ws();
	} while (t->type_get() != pp_token::TOK_LINE_END);   
	state = DEAD;
	return false;
}

/*!
  Parses object-like macro replacement list and stores values into self.
  The input is parsed including the TOK_LINE_END even in case it is ill-formed.
  \pre state == BEGIN
  \param input  The source for tokens, starting at the beginning of the replacement list.
  \return false  If the replacement list was ill-formed.
*/
bool macro_body::parse(const ptr<token_input> &input)
{
	lassert(state == BEGIN);
	
	ptr<token_sequence> literal;
	enum { BEGIN, LITERAL, CONCAT, END } fstate = BEGIN;
	ptr<pp_token> t;
	
	do {
		if (fstate == BEGIN)
			t = input->read_front_skip_ws();
		else
			t = input->read_front();

		if (t->type_get() == pp_token::TOK_HASH_HASH) {
			// check whether the ## is not at the beginning
			if (fstate == BEGIN) {
				// ## at the beginning of expansion list
				report << hash_hash_at_beginning << t->location_get();
				// discard tokens till end of line
				do {
					t = input->read_front_skip_ws();
				} while (t->type_get() != pp_token::TOK_LINE_END);   
				state = DEAD;
				return false;
			}
			
			if (fstate == LITERAL) {
				// flush the literal
				items->push_back(macro_item::create_literal(literal));
			}

			literal = token_sequence::create();
			literal->add_back(t);
			
			items->push_back(macro_item::create_concat(literal));
			fstate = CONCAT;
		} else if (t->type_get() == pp_token::TOK_LINE_END) {
			switch (fstate) {
				case BEGIN:
					// macro with no content has no macro items
					break;
				case CONCAT:
					// ## at the end of the expansion list
					report << hash_hash_at_end << t->location_get();
					state = DEAD;
					return false;
				case LITERAL:
					if (literal->skip_back_ws()) {
						// flush the literal
						items->push_back(macro_item::create_literal(literal));
					}
					break;
				case END:
				default:
					lassert2(false,"You should never get here");
					break;
			}
			fstate = END;
		} else {
			if (fstate != LITERAL) {
				// start new literal
				literal = token_sequence::create();
				fstate = LITERAL;
			}
			literal->add_back(t);
		}
	} while (fstate != END);
	state = PARSED;
	return true;
}

/*!
  Expand this object-like macro body.
  \pre state == PARSED
  \param loc  The location of the expansion.
  \return The expanded, but not rescanned sequence.
*/
ptr<token_sequence> macro_body::expand(const ptr<source_location> &loc)
{
	lassert(state == PARSED);
	
	ptr<token_sequence> result = token_sequence::create();
	ptr<token_sequence> current;
	bool concat = false;
	items_type::iterator it = items->begin();
	items_type::iterator last = items->end();

	for ( ; it != last; ++it) {
		ptr<macro_item> mi = *it;

		switch (mi->action_get()) {
			case macro_item::CONCAT:
				if (concat) {
					// concatenating with ## literal
					current = mi->value_get();
				} else {
					concat = true;
					// signal first round
					current = NULL;
				}
				break;
			case macro_item::LITERAL:
				// assign because current is NULL or empty
				current = mi->value_get();
				break;
			default:
				lassert2(false,"You should never get here");
				break;
		}

		if (concat) {

			// if there is token for the right side
			if (current && current->skip_front_ws()) {
				
				// TODO pt remove ::std::cerr << "WILL run concat on " << current << ::std::endl;
				
				// concat is always after tokens
				ptr<pp_token> left = result->read_back_skip_ws();
				// whitespace already cleared
				ptr<pp_token> right = current->read_front();
				
				// errors are handled transparently inside concatenate
				result->append(concat::instance()->process(left,right));
				// add the rest of the right part
				result->append(current);

				concat = false;
			}
		} else {
			// no pending concatenation
			result->append(current);
		}
	}

	// concatenation shall not be at the end
	lassert(concat != true);
	// TODO clone(taboo macros, loc)
	return result->clone(loc);
}

/*!
  Expands this function-like macro body.
  \todo pt Add error message.
  \todo pt Add location setting.
  \pre state == PARSED
  \param mas  The arguments for the macro.
  \param loc  The location of the expansion.
  \return The expanded, but not rescanned sequence.
  \return NULL in case of error.
*/
ptr<token_sequence> macro_body::expand(const ptr<source_location> &loc, 
		const ptr<macro_arguments> &mas, const ptr<macro_storage> &macros)
{
	lassert(state == PARSED);
	
	ptr<token_sequence> result = token_sequence::create();
	ptr<token_sequence> current;
	ptr<pp_token> left;
	bool concat = false;
	items_type::iterator it = items->begin();
	items_type::iterator last = items->end();

	for ( ; it != last; ++it) {
		ptr<macro_item> mi = *it;
		ptr<macro_argument> ma;

		switch (mi->action_get()) {
			case macro_item::CONCAT:
				if (concat) {
					// concatenating with ## literal
					current = mi->value_get();
				} else {
					concat = true;
					current = NULL;
				}
				break;
			case macro_item::LITERAL:
				current = mi->value_get();
				break;
			case macro_item::EXPANSION:
				ma = mas->argument_get(mi->index_get());
				current = ma->expanded_get(macros);
				break;
			case macro_item::COPY:
				ma = mas->argument_get(mi->index_get());
				current = ma->nonexpanded_get();
				break;
			case macro_item::STR:
				ma = mas->argument_get(mi->index_get());
				current = ma->stringified_get();
				break;
			default:
				lassert2(false,"You should never get here");
				break;
		}

		if (concat) {
			// if there is token for the right side
			if (current && current->skip_front_ws()) {
				
				// TODO pt remove ::std::cerr << "WILL run () concat on " << current << ::std::endl;
				
				// check back token
				ptr<pp_token> left = 
					result->skip_back_ws() ? result->read_back() : ptr<pp_token>(NULL);
				ptr<pp_token> right = current->read_front();
				
				// errors are handled transparently inside concatenate
				result->append(concat::instance()->process(left,right));
				// add the rest of the right part
				result->append(current);

				concat = false;
			}
		} else {
			// no pending concatenation
			result->append(current);
		}
			
	}

	if (concat) {
		// check back token
		ptr<pp_token> left = 
			result->skip_back_ws() ? result->read_back() : ptr<pp_token>(NULL);
		// check front token
		ptr<pp_token> right = 
			current->skip_front_ws() ? current->read_front() : ptr<pp_token>(NULL);
		
		// errors are handled transparently inside concatenate
		result->append(concat::instance()->process(left,right));
		// add the rest of the right part
		result->append(current);

		concat = false;
	}
	
	return result->clone(loc);
}

/*!
  Returns internal state of the object.
  \return  The internal state of the object.
*/
macro_body::state_type macro_body::state_get(void) const
{
	return state;
}

/*!
  Tests equality.
  \param other The macro body to compare with.
  \return true If both expansion lists have the same order, spelling and whitespace separation of tokens.
*/
bool macro_body::equals(const ptr<macro_body> &other) const
{
	if (!other || state != other->state_get()) return false;

	if (state != PARSED) return true;

	if (items->size() != other->items->size()) return false;

	items_type::iterator it = items->begin();
	items_type::iterator last = items->end();

	items_type::iterator ot = other->items->begin();

	for ( ; it != last; ++it, ++ot) {
		ptr<macro_item> mi = *it;
		ptr<macro_item> oi = *ot;

		if (!is_equal(mi,oi)) return false;
	}
		
	return true;
}

/*!
  Marks the object.
*/
void macro_body::gc_mark(void)
{
	items.gc_mark();
	::lestes::std::object::gc_mark();
}

/*!
  Returns empty expansion list.
  \post state == BEGIN
  \return The new empty expansion list.
*/  
ptr<macro_body> macro_body::create(void)
{
	return new macro_body();
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);
/* vim: set ft=lestes : */
