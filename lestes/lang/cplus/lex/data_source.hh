/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___data_source_hh___included
#define lestes__lang__cplus__lex___data_source_hh___included

/*! \file
  \brief Abstract data source.

  Declaration of data_source class representing abstract data source.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/ucn_filter.hh>
package(lestes);
package(lang);
package(cplus);
package(lex);

/*!
  \brief Abstract data source.
  
  Represents abstract data source skeleton, used as base class for all data sources.
*/
class data_source: public ucn_filter {
public:
	// ! releases all resources
	// TODO pt void release(void) abstract;
protected:
	//! Creates the object.
	inline data_source(void);
private:
	//! Hides copy constructor.
	data_source(const data_source &copy);
	//! Hides assignment operator.
	data_source &operator=(const data_source &rhs);
};

/*!
	Creates the object.
*/
inline data_source::data_source(void)
{
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
