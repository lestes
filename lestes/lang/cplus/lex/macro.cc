/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Preprocessor macro.
  
  Definition of macro class representing preprocessor macro.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/equality.hh>
#include <lestes/std/source_location.hh>
#include <lestes/lang/cplus/lex/macro.hh>
#include <lestes/lang/cplus/lex/macro.m.hh>
#include <lestes/lang/cplus/lex/pp_token.hh>
#include <lestes/lang/cplus/lex/token_value.hh>
#include <lestes/lang/cplus/lex/token_input.hh>
#include <lestes/lang/cplus/lex/token_sequence.hh>
#include <lestes/lang/cplus/lex/macro_item.hh>
#include <lestes/lang/cplus/lex/macro_head.hh>
#include <lestes/lang/cplus/lex/macro_body.hh>
#include <lestes/lang/cplus/lex/macro_arguments.hh>

#include <iostream>

package(lestes);
package(lang);
package(cplus);
package(lex);

using namespace ::std;

/*!
  Creates new empty macro object.
  \post state == BEGIN
*/
macro::macro():
	state(BEGIN),
	location(),
	name(),
	funlike(false),
	predefined(false),
	head(macro_head::create()),
	body(macro_body::create())
{
}

/*!
  Parses macro parameter list, sets funlike and stores parameters into self.
  \param input  The source for tokens, starting after macro name.
  \return false  In case of duplicate parameters or unterminated parameter list.
*/
bool macro::parse_head(const ptr<token_input> &input) 
{
	ptr<pp_token> t = input->peek_front();
	
	if (t->type_get() != pp_token::TOK_LEFT_PAR) {
		funlike = false;
		// no parameters
		return true;
	}
	
	// discard the left par
	input->read_front();
	
	funlike = true;

	return head->parse(input);
}

/*!
  Parses macro replacement list and stores values into self.  
  \param input  The source for tokens, starting at the beginning of the replacement list.
  \return false  If the replacement list was ill-formed.
*/
bool macro::parse_body(const ptr<token_input> &input)
{
	return funlike ? body->parse(input,head) : body->parse(input);
}

/*!
  Parses macro name and stores into self.
  \param input  The source for tokens, starting before macro name.
  \return false  In case of parse error.
*/
bool macro::parse_name(const ptr<token_input> &input)
{
	ptr<pp_token> t = input->read_front_skip_ws();

	// the location of the macro will be the name in the definition
	location = t->location_get();

	if (t->type_get() == pp_token::TOK_LINE_END) {
		// no macro name at all
		report << definition_expects_macro_name << location;
		return false;
	}

	if (t->is_alternative()) {
		// C++ operator alternative spelling are not identifiers
		report << macro_name_alt_operator << t->spelling_get() << location;
		return false;
	}
	
	if (!t->is_name()) {
		// macro name must be an identifier
		report << macro_name_identifier << location;
		return false;
	}
	
	name = t->value_get();
	
	if (is_equal(name,token_value::create("defined"))) {
		// defined is reserved as macro name
		report << defined_invalid_macro_name << location;
		return false;
	}
	
	return true;
}

/*!
  Parses macro definition and stores data into self.
  \pre state == BEGIN 
  \param input  The source for tokens, starting before macro name.
  \return false  In case of parse error.
*/
bool macro::parse(const ptr<token_input> &input)
{
	lassert(state == BEGIN);
	
	if (!parse_name(input)) {
		state = DEAD;
		return false;
	}
	if (!parse_head(input)) {
		state = DEAD;
		return false;
	}
	if (!parse_body(input)) {
		state = DEAD;
		return false;
	}
	state = PARSED;
	return true;
}

/*!
  Returns the location of definition.
  \pre state == PARSED
  \return  The location of the macro name in the #define.
*/
ptr<source_location> macro::location_get(void) const 
{
	return location;
}

/*!
  Returns the name of the macro.
  \pre state == PARSED 
  \return The name of the macro.
*/
ptr<token_value> macro::name_get(void) const
{
	lassert(state == PARSED);
	return name;
}

/*!
  Returns predefined flag.
  \pre state == PARSED 
  \return true  If the macro is predefined.
*/
bool macro::predefined_get(void) const
{
	lassert(state == PARSED);
	return predefined;
}

/*!
  Returns whether the macro is function-like.
  \pre state == PARSED
  \return true  If the macro is function-like.
*/
bool macro::funlike_get(void) const
{
	lassert(state == PARSED);
	return funlike;
}

/*!
  Expands this macro, if the call matches the definition, taking possible arguments from input.
  \param input  The source of possible arguments, starting at macro name.
  \param loc  The location of the macro expansion.
  \return The sequence containing the token denoting the name if the call did not match.
  \return The expanded, but not rescanned sequence if the call matched the definition.
  \return NULL in case of nested error.
*/
ptr<token_sequence> macro::expand(const ptr<pp_token> &a_name, const ptr<token_input> &input,
		const ptr<macro_storage> &macros)
{
	ptr<source_location> loc = a_name->location_get();
	
	if (funlike_get()) {
		ptr<pp_token> tok = input->peek_front();
		lassert(tok->type_get() == pp_token::TOK_LEFT_PAR);

		ptr<macro_arguments> mas = macro_arguments::create();
		if (!mas->parse(input)) {
			// missing terminating `)'
			report << unterminated_argument_list << name->content_get() << loc;
			return NULL;
		}

		if (!mas->check(head->length())) {
			// argument count is different from parameter count
			report << invalid_argument_count << name->content_get() << mas->length() << loc;
			return NULL;
		}

		// expand function-like
		return body->expand(loc,mas,macros);
	}
	// expand object-like
	return body->expand(loc);
}

/*!
  Returns whether macro definitions are equal.
  \param other  The macro to compare with.
  \return true  If the definition is equal to other.
*/
bool macro::equals(const ptr<macro> &other) const
{
	if (!other) return false;
	
	if (!is_equal(head,other->head)) return false;
	
	if (!is_equal(body,other->body)) return false;
	return true;
}

/*!
  Marks the object.
*/
void macro::gc_mark(void)
{
	location.gc_mark();
	name.gc_mark();
	head.gc_mark();
	body.gc_mark();
	::lestes::std::object::gc_mark();
}

/*! 
  Returns empty macro.
  \post state == BEGIN
  \return A new empty instance of macro.
*/
ptr<macro> macro::create(void)
{
	return new macro();
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);
/* vim: set ft=lestes : */
