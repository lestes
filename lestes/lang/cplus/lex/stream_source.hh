/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___stream_source_hh___included
#define lestes__lang__cplus__lex___stream_source_hh___included

/*! \file
  \brief Stream data source.

  Declaration of stream_source class representing data source reading from stream.
  \author pt
*/

#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/data_source.hh>
#include <istream>

package(lestes);
package(std);
// forward declaration to avoid cycle
class istream_wrapper;
end_package(std);
package(lang);
package(cplus);
package(lex);

// forward declaration to avoid cycle
class ucn_token;

/*!
  \brief Stream data source.

	Represents data source reading from stream.
*/
class stream_source: public data_source {
public:
	//! Reads one token from file.
	ptr<ucn_token> read(void);
	//! Returns new file source, initializes with input stream.
	static ptr<stream_source> create(const ptr<istream_wrapper> &a_wrapper);
protected:
	//! Creates new object, initializes with input stream.
	stream_source(const ptr<istream_wrapper> &a_wrapper);
	//! Marks the object.
	virtual void gc_mark(void);
private:
	//! Hides copy constructor.
	stream_source(const stream_source &);
	//! Hides assignment operator.
	stream_source &operator=(const stream_source &);
	//! Stream to read from.
	srp<istream_wrapper> wrapper;
	//! Direct reference to the stream.
	::std::istream *stream;
};

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
