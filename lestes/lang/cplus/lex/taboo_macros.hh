/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___taboo_macros_hh___included
#define lestes__lang__cplus__lex___taboo_macros_hh___included

/*! \file
  \brief Taboo macros.
  
  Declaration of taboo_macros class representing set of disallowed macros.
  \author pt
*/

#include <lestes/common.hh>
#include <lestes/std/set.hh>
#include <lestes/std/map.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

// forward declaration to avoid cycle
class macro;

/*!
  \brief Set of taboo macros.
  
  Represents a set of macros, which shall not be expanded.
  The set is constant and extending it creates a new set.
*/
class taboo_macros: public ::lestes::std::object {
public:
	// TODO pt remove
	ucn_string names_get(void) const;
	//! Tests macro membership.
	bool contains(const ptr<macro> &a_macro) const;
	//! Returns new set with added macro.
	ptr<taboo_macros> extend(const ptr<macro> &a_macro) const;
	//! Returns empty set.
	static ptr<taboo_macros> create(void);
protected:
	//! Creates empty set.
	taboo_macros(void);
	//! Marks the object.
	virtual void gc_mark(void);
private:
	//! Creates a copy of the object with added macro.
	taboo_macros(const ptr<taboo_macros> &copy, const ptr<macro> &a_macro);
	//! Type of set of macros.
	typedef ::lestes::std::set< srp<macro> > taboo_type;
	//! Type of auxiliary map for sharing newly created instances.
	typedef ::lestes::std::map< srp<macro>, srp<taboo_macros> > shared_type;
	//! Set of taboo macros.
	srp<taboo_type> taboo;
	//! Internal map to remember objects created by extend.
	mutable srp<shared_type> shared;
	//! Internal shared empty instance.
	static ptr<taboo_macros> empty_instance;
};

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
