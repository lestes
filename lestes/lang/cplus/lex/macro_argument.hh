/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___macro_argument_hh___included
#define lestes__lang__cplus__lex___macro_argument_hh___included

/*! \file
  \brief Macro argument.
  
  Declaration of macro_argument class representing preprocessor macro argument.
  \author pt
*/
#include <lestes/common.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

// forward declaration to avoid cycle
class token_input;
class token_sequence;
class macro_storage;

/*!
  \brief Macro argument.

  Encapsulates macro argument with lazy evaluation.
  Provides nonexpanded, expanded and stringified representation
  of the argument.
*/
class macro_argument: public ::lestes::std::object {
public:
	//! Type of internal state.
	typedef enum { BEGIN, PARSED, DEAD } state_type;
	//! Parse result type.
	typedef enum {
		//! The arguments will continue.  
		CONTINUE, 
		//! The last argument in the list.
		LAST,
		//! Error in argument.
		ERROR,
		//! The first argument is empty.
		EMPTY
	} result_type;
	//! Parses argument.
	result_type parse(const ptr<token_input> &input, bool first);
	//! Returns internal state.
	state_type state_get(void) const;
	//! Returns nonexpanded argument.
	ptr<token_sequence> nonexpanded_get(void) const;
	//! Returns expanded argument.
	ptr<token_sequence> expanded_get(const ptr<macro_storage> &macros);
	//! Returns stringified argument.
	ptr<token_sequence> stringified_get(void);
	//! Test equality.
	bool equals(const ptr<macro_argument> &other) const;
	//! Returns new instance.
	static ptr<macro_argument> create(void);
protected:   
	//! Creates empty object.
	macro_argument(void);
	//! Marks the object.
	virtual void gc_mark(void);
private:
	//! Internal state of the object.
	state_type state;
	//! The nonexpanded sequence.
	srp<token_sequence> nonexpanded;
	//! The expanded sequence, lazy evaluated.
	srp<token_sequence> expanded;
	//! The stringified sequence as a token in sequence, lazy evaluated.
	srp<token_sequence> stringified;
};

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);
#endif
/* vim: set ft=lestes : */
