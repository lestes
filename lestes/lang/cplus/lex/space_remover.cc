/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Whitespace remover.

  Definition of space_remover class representing whitespace remove filter.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/space_remover.hh>
#include <lestes/lang/cplus/lex/pp_token.hh>
package(lestes);
package(lang);
package(cplus);
package(lex);

/*!
  Creates new remover.
  \pre a_input != NULL
  \param a_input  The input for the filter.
*/
space_remover::space_remover(const ptr<pp_filter> &a_input):
	pp_filter(checked(a_input))
{
}

/*!
  Reads next token, skips whitespace, i.e. pp_token::TOK_LINE_END and pp_token::TOK_BLANK
  \return  The next non-whitespace token in the stream.
*/
ptr<pp_token> space_remover::read(void)
{
	ptr<pp_token> tok;
	pp_token_type ppt;
	
	do {
		tok = input_read();
		ppt = tok->type_get();
	} while (ppt == pp_token::TOK_LINE_END || ppt == pp_token::TOK_BLANK);
	return tok;
}

/*!
  Returns new remover.
  \pre a_input != NULL
  \param a_input The input for the filter.
  \return  A new instance of remover.
*/
ptr<space_remover> space_remover::create(const ptr<pp_filter> &a_input)
{
	return new space_remover(a_input);
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

/* vim: set ft=lestes : */
