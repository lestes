<?xml version="1.0" encoding="UTF-8"?>
<!--
	The lestes compiler suite
	Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
	Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
	Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
	Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
	Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
	Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
	Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; version 2 of the License.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	See the full text of the GNU General Public License version 2, and
	the limitations in the file doc/LICENSE.

	By accepting the license the licensee waives any and all claims
	against the copyright holder(s) related in whole or in part to the
	work, its use, and/or the inability to use it.
-->
<lmd xmlns="http://lestes.jikos.cz/schemas/lmd" xmlns:h="http://www.w3.org/TR/REC-html40">
	<file-name>condition_stack</file-name>

	<dox file="both">
		<bri>
			Messages for conditionals.
		</bri>
		<det>
			Definition of warnings and errors issued for conditional directives.
		</det>
	</dox>

	<packages>
		<p>lestes</p>
		<p>lang</p>
		<p>cplus</p>
		<p>lex</p>
	</packages>
	
	<imports>
	</imports>

	<implementation-imports>
	</implementation-imports>

	<error name="elif_without_if">
		<dox>
			<bri>Error message for conditional directives.</bri>
			<det>
				Issued when #elif is encountered prior to #if.
			</det>
		</dox>
		<text>#elif without #if</text>
	</error>

	<error name="endif_without_if">
		<dox>
			<bri>Error message for conditional directives.</bri>
			<det>
				Issued when #endif is encountered prior to #if.
			</det>
		</dox>
		<text>#endif without #if</text>
	</error>

	<error name="else_without_if">
		<dox>
			<bri>Error message for conditional directives.</bri>
			<det>
				Issued when #else is encountered prior to #if.
			</det>
		</dox>
		<text>#else without #if</text>
	</error>

	<error name="elif_after_else">
		<dox>
			<bri>Error message for conditional directives.</bri>
			<det>
				Issued when #elif is encountered after #else.
			</det>
		</dox>
		<text>#elif after #else</text>
	</error>

	<error name="else_after_else">
		<dox>
			<bri>Error message for conditional directives.</bri>
			<det>
				Issued when #else is encountered after #else.
			</det>
		</dox>
		<text>#else after #else</text>
	</error>

	<error name="unterminated_conditional">
		<dox>
			<bri>Error message for conditional directives.</bri>
			<det>
				Issued for unterminated conditional directives.
				The zeroth parameter is the string representation of the directive name.
			</det>
		</dox>
		<text>unterminated %0</text>
		<param type="lstring" />
	</error>
	
	<error name="conditional_starts_here">
		<dox>
			<bri>Error message for conditional directives.</bri>
			<det>
				Issued to show the actual start of the conditional.
			</det>
		</dox>
		<text>the conditional starts here</text>
	</error>

</lmd>
