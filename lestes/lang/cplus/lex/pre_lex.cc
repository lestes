/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Character level processing.

  Definition of pre_lex class performing operations before lexical analysis.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/pre_lex.hh>
#include <lestes/lang/cplus/lex/pre_lex.m.hh>
#include <lestes/lang/cplus/lex/lex_loggers.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

using namespace ::std;

/*!
  Creates the object.
  \post state == START
  \pre a_src != NULL
  \pre a_enc != NULL
  \param a_src  The source stream of data.
  \param a_enc  The encoder to apply on the source.
  \return New instance of the class.
*/
pre_lex::pre_lex(const ptr<data_source> &a_src, const ptr<encoder> &a_enc):
	state(START),
	saved(),
	src((lassert(a_src),a_src)),
	enc((lassert(a_enc),a_enc)),
	stok(special_tokens::create()),
	lnum(line_numbers::create()),
	trig(trigraphs::create()), 
	ljoin(line_join::create())
{
	connect();
}

/*!
  Connects all filters together.
*/
void pre_lex::connect(void)
{
	enc->input_set(src);
	stok->input_set(enc);
	lnum->input_set(stok);
	trig->input_set(lnum);
	ljoin->input_set(trig);
}

/*!
  Returns next token passed through all actions before lex.
  Inserts fake newline, if there is none at the end of file.
  Saves the EOF token to be returned forever.
  \return Next token with all fields set to proper values.
*/
ptr<ucn_token> pre_lex::read(void)
{
	pre_lex_logger << "pre_lex::read()\n" << msg::eolog;
	pre_lex_logger << "state = " << state["scn#$"] << '\n' << msg::eolog;

	ptr<ucn_token> t;
	
	switch (state) {
		case END:
			t = saved;
			break;
		case ERROR:
			state = END;
			// return fake newline
			t = ucn_token::create(ucn_token::TOK_BASIC,character::ascii_new_line,saved->location_get());
			break;
		case START:
		case CONT:
		case NEWLINE:
			t = ljoin->read();
			break;
		default:
			lassert2(false,"You should never get here");
	}
	
	ucn_token_type utt = t->type_get();

	switch (state) {
		case START:
			if (utt == ucn_token::TOK_EOF) {
				// save the EOF
				saved = t;
				state = END;
			} else if (utt == ucn_token::TOK_BASIC && t->value_get() == character::ascii_new_line) {
				// save token's location
				saved = t;
				state = NEWLINE;
			} else {
				// continuing
				state = CONT;
				saved = NULL;
			}
			break;
		case CONT:
			if (utt == ucn_token::TOK_EOF) {
				// save the EOF
				saved = t;
				// report error: missing newline at the end of file
				t = ucn_token::create_error(missing_newline->format(),t->location_get());
				state = ERROR;
			} else if (utt == ucn_token::TOK_BASIC && t->value_get() == character::ascii_new_line) {
				// save token's location
				saved = t;
				state = NEWLINE;
			} else {
				saved = NULL;
			}
			break;
		case NEWLINE:
			switch (utt) {
				case ucn_token::TOK_EOF:
					// move the location to that of the newline
					t = saved = t->clone_location(saved->location_get());
					state = END;
					break;
				case ucn_token::TOK_ERROR:
					break;
				case ucn_token::TOK_BASIC:
					if (t->value_get() == character::ascii_new_line)
						break;
					// fall through
				case ucn_token::TOK_TRANSLATED:
					state = CONT;
					break;
				default:
					lassert2(false,"You should never get here");
			}
			break;
		case END:
		case ERROR:
			break;
		default:
			lassert2(false,"You should never get here");
	}

	pre_lex_logger << "returning " << static_cast<ulint>(t->type_get()) << '\n' << msg::eolog;
	pre_lex_logger << "pre_lex::read() end\n" << msg::eolog;
	return t;
}

/*!
  Marks the object.
*/
void pre_lex::gc_mark(void)
{
	saved.gc_mark();
	src.gc_mark();
	enc.gc_mark();
	stok.gc_mark();
	lnum.gc_mark();
	trig.gc_mark();
	ljoin.gc_mark();
	::lestes::std::object::gc_mark();
}

/*!
  Returns new instance, initializes with source and encoding.
  \param a_src  The source stream of data.
  \param a_enc  The encoder to apply on the source.
  \return New instance of the class.
*/
ptr<pre_lex> pre_lex::create(const ptr<data_source> &a_src, const ptr<encoder> &a_enc)
{
	return new pre_lex(a_src,a_enc);
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

/* vim: set ft=lestes : */
