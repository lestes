/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___macro_body_hh___included
#define lestes__lang__cplus__lex___macro_body_hh___included

/*! \file
	Definition of macro_body class representing macro expansion list.
	\author pt
*/

#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/macro_item.hh>
#include <lestes/std/vector.hh>

package(lestes);

package(std);
// forward declarations to avoid cycle
class source_location;
end_package(std);

package(lang);
package(cplus);
package(lex);

// forward declarations to avoid cycle
class token_input;
class token_sequence;
class macro_head;
class macro_arguments;
class macro_storage;

/*!
  Representation of macro expansion list.
*/
class macro_body: public ::lestes::std::object {
public:
	//! type of internal state
	typedef enum { BEGIN, PARSED, DEAD } state_type;
	//! parses object-like macro body
	bool parse(const ptr<token_input> &input);
	//! parses function-like macro body
	bool parse(const ptr<token_input> &input, const ptr<macro_head> &head);
	//! expands object-like macro 
	ptr<token_sequence> expand(const ptr<source_location> &loc);
	//! expands function-like macro
	ptr<token_sequence> expand(const ptr<source_location> &loc, 
			const ptr<macro_arguments> &mas, const ptr<macro_storage> &macros);
	//! returns internal state
	state_type state_get(void) const;
	//! tests equality
	bool equals(const ptr<macro_body> &other) const;
	//! returns empty expansion list
	static ptr<macro_body> create(void);
protected:
	//! creates empty expansion list
	macro_body(void);
	//! Marks the object.
	virtual void gc_mark(void);
private:
	//! type of list for storing the expansion items
	typedef ::lestes::std::vector< srp<macro_item> > items_type;
	//! internal state of the object
	state_type state;
	//! the list of expansion items
	srp<items_type> items;
};

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
