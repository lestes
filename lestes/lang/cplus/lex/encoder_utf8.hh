/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___encoder_utf8_hh___included
#define lestes__lang__cplus__lex___encoder_utf8_hh___included

/*! \file
  \brief UTF-8 encoder.

  Declaration of encoder_utf8 class performing UTF-8 character set encoding.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/ucn_token.hh>
#include <lestes/lang/cplus/lex/encoder.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

/*!
  \brief UTF-8 encoder.

  Performs UTF8 character set encoding.
*/
class encoder_utf8: public encoder {
public:
	//! Reads next token.
	ptr<ucn_token> read(void);
	//! Returns new instance.
	static ptr<encoder_utf8> create(void);
protected:   
	//! Creates the object.
	encoder_utf8(void);
	//! Marks the object.
	virtual void gc_mark(void);
private:
	//! Hides copy constructor.
	encoder_utf8(const encoder_utf8 &);
	//! Hides assignment operator.
	encoder_utf8 &operator=(const encoder_utf8 &);
};

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
