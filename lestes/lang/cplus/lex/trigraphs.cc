/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Trigraphs filter.

  Definition of trigraphs class performing trigraph translation.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/trigraphs.hh>
#include <lestes/lang/cplus/lex/ucn_filter.hh>
#include <lestes/lang/cplus/lex/lex_loggers.hh>
#include <lestes/msg/logger.hh>
#include <lestes/msg/logger_util.hh>
//#include <climits> 

package(lestes);
package(lang);
package(cplus);
package(lex);

using namespace ::std;

/*!
  Creates new instance.
  \post state == START
*/
trigraphs::trigraphs(void):
	ucn_filter(),
	state(START)
{
}

/*!
  Reads next token after translation of trigraph sequences.
  \return  Token possibly originating from trigraph.
*/
ptr<ucn_token> trigraphs::read(void)
{
	trigraphs_logger << "trigraphs::read()\n" << msg::eolog;
	trigraphs_logger << "state == " << state["sqQ12"] << '\n' << msg::eolog;

	ptr<ucn_token> t;

	switch (state) {
		case ONE: 
			// flush one token
			t = first;
			// release reference
			first = NULL;
			state = START;
			trigraphs_logger << "return only saved\ntrigraphs::read() end\n" << msg::eolog;
			return t;
		case TWO:
			// flush one of two tokens
			t = first;
			first = second;
			// release reference
			second = NULL;
			state = ONE;
			trigraphs_logger << "return first saved\ntrigraphs::read() end\n" << msg::eolog;
			return t;
		default:
			break;
	}
	
	while (true) {
		t = input_read();
		ucn_token_type utt = t->type_get();
		// can return out of order, location is already recorded
//      if (utt == ucn_token::TOK_ERROR)
  //       return t;
		
		switch (state) {
			case START:
				if (utt == ucn_token::TOK_BASIC && t->value_get() == character::ascii_qmark) {
					// first '?'
					state = QUE;
					first = t;
				} else {
					trigraphs_logger << "trigraphs::read() end\n" << msg::eolog;
					return t;
				}
				break;
			case QUE:
				if (utt == ucn_token::TOK_BASIC && t->value_get() == character::ascii_qmark) {
					// second '?'
					state = QUEQUE;
					second = t;
				} else {
					ptr<ucn_token> tmp;
					// flush first
					tmp = first;
					// prepare to flush the second
					first = t;
					state = ONE;
					trigraphs_logger << "trigraphs::read() end\n" << msg::eolog;
					return tmp;
				}
				break;
			case QUEQUE:
				// found trigraph sequence
				if (utt == ucn_token::TOK_BASIC) {
					ucn u = t->value_get();
					ucn v = translation_map[character::extract_value(u)];
					if (v != 0) {
						t = first;
						// release the reference
						first = NULL;
						t = t->clone_value(v);
						state = START;
						trigraphs_logger << "trigraphs::read() end\n" << msg::eolog;
						return t;
					}

					// check the third token, with '?' so there will be "??" again
					state = u == character::ascii_qmark ? QUEQUE : TWO;
				} else {
					// there will be ordinary "?X" sequence in the buffer
					state = TWO;
				}
				

				{
					// shift the buffer
					ptr<ucn_token> tmp = first;
					first = second;
					second = t;
					trigraphs_logger << "trigraphs::read() end\n" << msg::eolog;
					return tmp;
				}
			default:
				lassert2(false,"You should never get here");
		}
		trigraphs_logger << "state == " << state["sqQ12"] << '\n' << msg::eolog;
	}
}

/*!
  Marks the object.
*/
void trigraphs::gc_mark(void)
{
	first.gc_mark();
	second.gc_mark();
	ucn_filter::gc_mark();
}

/*!
  Returns new instance.
  \return New instance of the class.
*/
ptr<trigraphs> trigraphs::create(void)
{
	return new trigraphs();
}

/*!
  Trigraph translation map. Defines mapping between ASCII characters.
  Zero values have no defined translation.
*/
const ucn trigraphs::translation_map[trigraphs::translation_length] = {
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	/* index '!' */ character::ascii_vbar,
	0, 0, 0, 0, 0,
	/* index '\'' */ character::ascii_hat,
	/* index '(' */ character::ascii_left_bracket,
	/* index ')' */ character::ascii_right_bracket,
	0, 0, 0,
	/* index '-' */ character::ascii_tilde,
	0,
	/* index '/' */ character::ascii_backslash,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	/* index '<' */ character::ascii_left_brace,
	/* index '=' */ character::ascii_hash,
	/* index '>' */ character::ascii_right_brace,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0
};

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);


/* vim: set ft=lestes : */
