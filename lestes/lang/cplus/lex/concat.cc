/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Token concatenator.
  
  Definition of concat class wrapping concat_guts flexer.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/std/source_location.hh>
#include <lestes/lang/cplus/lex/concat.hh>
#include <lestes/lang/cplus/lex/pp_token.hh>
#include <lestes/lang/cplus/lex/ucn_token.hh>
#include <lestes/lang/cplus/lex/token_sequence.hh>
#include <lestes/lang/cplus/lex/token_value.hh>
#include <lestes/lang/cplus/lex/concat.m.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

// include the flex generated parser in own separate namespace
package(concat_guts);
#include <lestes/lang/cplus/lex/concat_guts.yy.cc>
end_package(concat_guts);

using namespace ::std;

/*!
  Creates the object, allocates flex buffer.  
*/
concat::concat(void):
	yy_buffer(concat_guts::yy_new_buffer(NULL,YY_BUF_SIZE))
{
	concat_guts::yy_switch_to_buffer(yy_buffer);
}

/*!
  Finalizes the object, releases flex buffer.
*/
concat::~concat(void)
{
	concat_guts::yy_delete_buffer(yy_buffer);
}

/*!
  Atttempts to concatenate two tokens.
  \pre left != NULL || right != NULL.
  \param left  The left token to concatenate.
  \param right  The right token to concatenate.
  \return  A token sequence containing the new token, or the input tokens in case of error.
*/
ptr<token_sequence> concat::process(const ptr<pp_token> &left, const ptr<pp_token> &right)
{
	lassert(left || right);

	ptr<token_sequence> result = token_sequence::create();
	ptr<source_location> loc;

	value = "";

	if (left) {
		loc = left->location_get();
		value += left->spelling_get();
	} else {
		loc = right->location_get();
	}

	if (right) {
		value += right->spelling_get();
	}
  
	length = value.length();
	index = 0;

	// pretend having a new file
	concat_guts::yyrestart(NULL);
	ptr<pp_token> tok = concat_guts::concat_parse(loc,token_value::create(value));
	
	if (!tok) {
		// concatenation failed
		report << unable_to_concatenate << loc;
		if (left) result->add_back(left);
		if (right) result->add_back(right);
	} else {
		// successful attempt
		result->add_back(tok);
	}
	return result;
}

/*!
  Fills buffer with characters from current input string.
  Called through YY_INPUT flex macro.
  \param cbuf  The buffer to fill.
  \param max  Maximum count of characters to fill. 
  \return  The actual count of filled characters.
*/
concat::size_type concat::yy_input(char_type *cbuf, size_type max)
{
	size_type cnt;
	
	if (index == length) return YY_NULL;

	// put all tokens up to max into buffer
	for (cnt = 0; cnt < max; cnt++) {
		if (index == length) {
			*cbuf++ = static_cast<char_type>(ucn_token::TOK_EOF);
			cnt++;
			break;
		}
		ucn u = value[index++];
		if (character::is_basic(u)) {
			*cbuf++ = static_cast<char_type>(character::extract_value(u));
		} else {
			*cbuf++ = static_cast<char_type>(ucn_token::TOK_TRANSLATED);
		}
	}

	return cnt;
}

/*!
  Marks the object.
*/
void concat::gc_mark(void)
{
	::lestes::std::object::gc_mark();
}

/*!
  Returns the only instance of the object.
  \return The new instance with own flexer buffer.
*/
ptr<concat> concat::instance(void)
{
	if (!singleton) {
		singleton =  new concat();
	}
	return singleton;
}

/*!
  The only instance of the class, managing the associated flexer.
*/
ptr<concat> concat::singleton;

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);
/* vim: set ft=lestes : */
