/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Unit test.

  Unit test for class condition_stack.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/equality.hh>
#include <lestes/lang/cplus/lex/condition_stack.hh>
#include <lestes/std/file_info.hh>
#include <lestes/std/source_location.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

using namespace ::std;

/*!
  \brief Tests condition class.

  Performs testing of condition_stack class.
*/
void condition_stack_test(void)
{
	ptr<source_location> loc = source_location::create(file_info::create("abc",NULL),1,1);
	ptr<condition_stack> cs = condition_stack::create();

	lassert(is_equal(cs->active_get(),true));
	lassert(is_equal(cs->depth(),0U));

	// error: #else without #if
	lassert(is_equal(cs->process(condition_stack::DIR_ELSE,false,loc),false));
	lassert(is_equal(cs->active_get(),true));
	lassert(is_equal(cs->depth(),0U));

	// error: #elif without #if
	lassert(is_equal(cs->process(condition_stack::DIR_ELIF,false,loc),false));
	lassert(is_equal(cs->active_get(),true));
	lassert(is_equal(cs->depth(),0U));

	// error: #endif without #if
	lassert(is_equal(cs->process(condition_stack::DIR_ENDIF,false,loc),false));
	lassert(is_equal(cs->active_get(),true));
	lassert(is_equal(cs->depth(),0U));


	// #if false
	lassert(is_equal(cs->process(condition_stack::DIR_IF,false,loc),true));
	lassert(is_equal(cs->active_get(),false));
	lassert(is_equal(cs->depth(),1U));

	// #endif
	lassert(is_equal(cs->process(condition_stack::DIR_ENDIF,false,loc),true));
	lassert(is_equal(cs->active_get(),true));
	lassert(is_equal(cs->depth(),0U));


	// #if true
	lassert(is_equal(cs->process(condition_stack::DIR_IF,true,loc),true));
	lassert(is_equal(cs->active_get(),true));
	lassert(is_equal(cs->depth(),1U));

	// #endif
	lassert(is_equal(cs->process(condition_stack::DIR_ENDIF,false,loc),true));
	lassert(is_equal(cs->active_get(),true));
	lassert(is_equal(cs->depth(),0U));


	// #if false
	lassert(is_equal(cs->process(condition_stack::DIR_IF,false,loc),true));
	lassert(is_equal(cs->active_get(),false));
	lassert(is_equal(cs->depth(),1U));

	// #else
	lassert(is_equal(cs->process(condition_stack::DIR_ELSE,false,loc),true));
	lassert(is_equal(cs->active_get(),true));
	lassert(is_equal(cs->depth(),1U));
	
	// #endif
	lassert(is_equal(cs->process(condition_stack::DIR_ENDIF,false,loc),true));
	lassert(is_equal(cs->active_get(),true));
	lassert(is_equal(cs->depth(),0U));


	// #if true
	lassert(is_equal(cs->process(condition_stack::DIR_IF,true,loc),true));
	lassert(is_equal(cs->active_get(),true));
	lassert(is_equal(cs->depth(),1U));

	// #else
	lassert(is_equal(cs->process(condition_stack::DIR_ELSE,false,loc),true));
	lassert(is_equal(cs->active_get(),false));
	lassert(is_equal(cs->depth(),1U));
	
	// #endif
	lassert(is_equal(cs->process(condition_stack::DIR_ENDIF,false,loc),true));
	lassert(is_equal(cs->active_get(),true));
	lassert(is_equal(cs->depth(),0U));


	// #if false
	lassert(is_equal(cs->process(condition_stack::DIR_IF,false,loc),true));
	lassert(is_equal(cs->active_get(),false));
	lassert(is_equal(cs->depth(),1U));

	// #elif true
	lassert(is_equal(cs->process(condition_stack::DIR_ELIF,true,loc),true));
	lassert(is_equal(cs->active_get(),true));
	lassert(is_equal(cs->depth(),1U));
	
	// #elif false
	lassert(is_equal(cs->process(condition_stack::DIR_ELIF,false,loc),true));
	lassert(is_equal(cs->active_get(),false));
	lassert(is_equal(cs->depth(),1U));
	
	// #endif
	lassert(is_equal(cs->process(condition_stack::DIR_ENDIF,false,loc),true));
	lassert(is_equal(cs->active_get(),true));
	lassert(is_equal(cs->depth(),0U));


	// #if false
	lassert(is_equal(cs->process(condition_stack::DIR_IF,false,loc),true));
	lassert(is_equal(cs->active_get(),false));
	lassert(is_equal(cs->depth(),1U));

	// #elif false
	lassert(is_equal(cs->process(condition_stack::DIR_ELIF,false,loc),true));
	lassert(is_equal(cs->active_get(),false));
	lassert(is_equal(cs->depth(),1U));
	
	// #elif true
	lassert(is_equal(cs->process(condition_stack::DIR_ELIF,true,loc),true));
	lassert(is_equal(cs->active_get(),true));
	lassert(is_equal(cs->depth(),1U));
	
	// #endif
	lassert(is_equal(cs->process(condition_stack::DIR_ENDIF,false,loc),true));
	lassert(is_equal(cs->active_get(),true));
	lassert(is_equal(cs->depth(),0U));


	// #if true
	lassert(is_equal(cs->process(condition_stack::DIR_IF,true,loc),true));
	lassert(is_equal(cs->active_get(),true));
	lassert(is_equal(cs->depth(),1U));

	// #elif true
	lassert(is_equal(cs->process(condition_stack::DIR_ELIF,true,loc),true));
	lassert(is_equal(cs->active_get(),false));
	lassert(is_equal(cs->depth(),1U));
	
	// #elif false
	lassert(is_equal(cs->process(condition_stack::DIR_ELIF,false,loc),true));
	lassert(is_equal(cs->active_get(),false));
	lassert(is_equal(cs->depth(),1U));
	
	// #endif
	lassert(is_equal(cs->process(condition_stack::DIR_ENDIF,false,loc),true));
	lassert(is_equal(cs->active_get(),true));
	lassert(is_equal(cs->depth(),0U));


	// #if true
	lassert(is_equal(cs->process(condition_stack::DIR_IF,true,loc),true));
	lassert(is_equal(cs->active_get(),true));
	lassert(is_equal(cs->depth(),1U));

	// #elif false
	lassert(is_equal(cs->process(condition_stack::DIR_ELIF,false,loc),true));
	lassert(is_equal(cs->active_get(),false));
	lassert(is_equal(cs->depth(),1U));
	
	// #elif true
	lassert(is_equal(cs->process(condition_stack::DIR_ELIF,true,loc),true));
	lassert(is_equal(cs->active_get(),false));
	lassert(is_equal(cs->depth(),1U));
	
	// #endif
	lassert(is_equal(cs->process(condition_stack::DIR_ENDIF,false,loc),true));
	lassert(is_equal(cs->active_get(),true));
	lassert(is_equal(cs->depth(),0U));


	// #if false
	lassert(is_equal(cs->process(condition_stack::DIR_IF,false,loc),true));
	lassert(is_equal(cs->active_get(),false));
	lassert(is_equal(cs->depth(),1U));

	// #elif false
	lassert(is_equal(cs->process(condition_stack::DIR_ELIF,false,loc),true));
	lassert(is_equal(cs->active_get(),false));
	lassert(is_equal(cs->depth(),1U));
	
	// #else
	lassert(is_equal(cs->process(condition_stack::DIR_ELSE,false,loc),true));
	lassert(is_equal(cs->active_get(),true));
	lassert(is_equal(cs->depth(),1U));
	
	// #endif
	lassert(is_equal(cs->process(condition_stack::DIR_ENDIF,false,loc),true));
	lassert(is_equal(cs->active_get(),true));
	lassert(is_equal(cs->depth(),0U));


	// #if false
	lassert(is_equal(cs->process(condition_stack::DIR_IF,false,loc),true));
	lassert(is_equal(cs->active_get(),false));
	lassert(is_equal(cs->depth(),1U));

	// #elif true
	lassert(is_equal(cs->process(condition_stack::DIR_ELIF,true,loc),true));
	lassert(is_equal(cs->active_get(),true));
	lassert(is_equal(cs->depth(),1U));
	
	// #else
	lassert(is_equal(cs->process(condition_stack::DIR_ELSE,false,loc),true));
	lassert(is_equal(cs->active_get(),false));
	lassert(is_equal(cs->depth(),1U));
	
	// #endif
	lassert(is_equal(cs->process(condition_stack::DIR_ENDIF,false,loc),true));
	lassert(is_equal(cs->active_get(),true));
	lassert(is_equal(cs->depth(),0U));


	// #if true
	lassert(is_equal(cs->process(condition_stack::DIR_IF,true,loc),true));
	lassert(is_equal(cs->active_get(),true));
	lassert(is_equal(cs->depth(),1U));

	// error: eof in #if
	lassert(is_equal(cs->process(condition_stack::DIR_EOF,false,loc),false));
	lassert(is_equal(cs->active_get(),true));
	lassert(is_equal(cs->depth(),1U));
	
	// #else
	lassert(is_equal(cs->process(condition_stack::DIR_ELSE,false,loc),true));
	lassert(is_equal(cs->active_get(),false));
	lassert(is_equal(cs->depth(),1U));
	
	// error: #else after #else
	lassert(is_equal(cs->process(condition_stack::DIR_ELSE,false,loc),false));
	lassert(is_equal(cs->active_get(),false));
	lassert(is_equal(cs->depth(),1U));
	
	// error: #elif after #else
	lassert(is_equal(cs->process(condition_stack::DIR_ELIF,false,loc),false));
	lassert(is_equal(cs->active_get(),false));
	lassert(is_equal(cs->depth(),1U));
	
	// clear the #if with #endif
	lassert(is_equal(cs->process(condition_stack::DIR_ENDIF,false,loc),true));
	lassert(is_equal(cs->active_get(),true));
	lassert(is_equal(cs->depth(),0U));


	// #if true
	lassert(is_equal(cs->process(condition_stack::DIR_IF,true,loc),true));
	lassert(is_equal(cs->active_get(),true));
	lassert(is_equal(cs->depth(),1U));

	// nested #if true
	lassert(is_equal(cs->process(condition_stack::DIR_IF,true,loc),true));
	lassert(is_equal(cs->active_get(),true));
	lassert(is_equal(cs->depth(),2U));

	// nested #else
	lassert(is_equal(cs->process(condition_stack::DIR_ELSE,false,loc),true));
	lassert(is_equal(cs->active_get(),false));
	lassert(is_equal(cs->depth(),2U));
	
	// nested #endif
	lassert(is_equal(cs->process(condition_stack::DIR_ENDIF,false,loc),true));
	lassert(is_equal(cs->active_get(),true));
	lassert(is_equal(cs->depth(),1U));

	// nested #if false
	lassert(is_equal(cs->process(condition_stack::DIR_IF,false,loc),true));
	lassert(is_equal(cs->active_get(),false));
	lassert(is_equal(cs->depth(),2U));

	// nested #else
	lassert(is_equal(cs->process(condition_stack::DIR_ELSE,false,loc),true));
	lassert(is_equal(cs->active_get(),true));
	lassert(is_equal(cs->depth(),2U));
	
	// nested #endif
	lassert(is_equal(cs->process(condition_stack::DIR_ENDIF,false,loc),true));
	lassert(is_equal(cs->active_get(),true));
	lassert(is_equal(cs->depth(),1U));

	// #else
	lassert(is_equal(cs->process(condition_stack::DIR_ELSE,false,loc),true));
	lassert(is_equal(cs->active_get(),false));
	lassert(is_equal(cs->depth(),1U));

	// nested #if true
	lassert(is_equal(cs->process(condition_stack::DIR_IF,true,loc),true));
	lassert(is_equal(cs->active_get(),false));
	lassert(is_equal(cs->depth(),2U));

	// nested #else
	lassert(is_equal(cs->process(condition_stack::DIR_ELSE,false,loc),true));
	lassert(is_equal(cs->active_get(),false));
	lassert(is_equal(cs->depth(),2U));
	
	// nested #endif
	lassert(is_equal(cs->process(condition_stack::DIR_ENDIF,false,loc),true));
	lassert(is_equal(cs->active_get(),false));
	lassert(is_equal(cs->depth(),1U));

	// #endif
	lassert(is_equal(cs->process(condition_stack::DIR_ENDIF,false,loc),true));
	lassert(is_equal(cs->active_get(),true));
	lassert(is_equal(cs->depth(),0U));

}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

/*!
  \brief Main function.

  Runs the unit test in different namespace.
*/
int main(void)
{
	::lestes::lang::cplus::lex::condition_stack_test();
	return 0;
}
/* vim: set ft=lestes : */
