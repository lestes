/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/lang/cplus/lex/testing.hh>
#include <lestes/lang/cplus/lex/file_reference.hh>
#include <lestes/lang/cplus/lex/equality.hh>
#include <string>

using namespace ::lestes::lang::cplus::lex;
using namespace ::std;

int main(void)
{
	ptr<file_reference> fr = file_reference::create(string("foobar.h"));

/*
	fr.set_name(string("foobar.h"));
	fr.set_parent();
	*/

	holds(equal(fr->name_get(),string("foobar.h")));
	holds(equal(fr->parent_get(),ptr<file_reference>()));
//   holds(fr.has_parent() == false);

	ptr<file_reference> gr = file_reference::create(string("barfoo.h"),fr);

	holds(equal(gr->name_get(),string("barfoo.h")));
	holds(equal(gr->parent_get(),fr));
//   holds(gr.has_parent() == true);

	/*
	file_reference hr;

	hr = gr;

	holds(hr.get_name() == "barfoo.h");
	holds(hr.get_parent() == &fr);
	holds(hr.has_parent() == true);

	hr.set_name("gujikol.h");
	hr.set_parent(gr);

	holds(hr.get_name() == "gujikol.h");
	holds(hr.get_parent() == &gr);
	holds(hr.has_parent() == true);
	*/

	cerr << "testing file_reference OK\n";
	return 0;
}

/* vim: set ft=lestes : */

