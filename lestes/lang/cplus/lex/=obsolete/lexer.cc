/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/lexer.hh>
#include <lestes/lang/cplus/lex/file_source.hh>
#include <lestes/lang/cplus/lex/mapper.hh>

#include <iostream>

package(lestes);
package(lang);
package(cplus);
package(lex);

using namespace ::std;

lexer::lexer(const ptr<file_source> &a_fis, const ptr<mapper> &a_map):
	fis((assert(a_fis),a_fis)),
	plx((assert(a_map), pre_lexer::create(a_fis,a_map))),
	buffer(buffer_type::create())
{
}

lexer::~lexer(void)
{
}

lexer::char_type lexer::next_char(void)
{
	ptr<ucn_token> tok;
	ucn_token_type utt;
	
	tok = plx->read();
	utt = tok->type_get();

	//cerr << "lexer: " << tt << ' ' << tok.get_value() << endl;

	/* there are two solutions how to cope with errors from pre_lexer:
	  1. errors are reported to the error object on the place of occurrence and no error token is created
	  2. error token is created and passed until here, now it is centrally discarded and substituted
		  by end of file, which terminates the processing (I like this one better)
	 */
	
	if (utt == ucn_token::error) {
		// TODO report error for token if not already done
		utt = ucn_token::end_file;
		tok->type_set(utt);
	}

	buffer->add(tok);

	if (utt == ucn_token::normal_char)
		return static_cast<char_type>(tok->value_get());
		
	// this is a hack: special token types correspond to certain characters
	return static_cast<char_type>(utt);
}

lexer::size_type lexer::yy_input(char_type *cbuf, size_type max)
{
	char_type c;
	ulint cnt = 0;
	
	// put all tokens up to max (including end_file) into buffer
	for (cnt = 0; cnt < max; cnt++) {
		*cbuf++ = c = next_char();
		if (c == ucn_token::end_file) break;
	}
	return cnt;
}

srp<lexer::buffer_type> lexer::buffer_get(void)
{
	return buffer;
}

srp<lexer> lexer::create(const ptr<file_source> &a_fis, const ptr<mapper> &a_map)
{
	assert(a_fis);
	assert(a_map);
	return srp<lexer>(new lexer(a_fis,a_map));
}


end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

/* vim: set ft=lestes : */
