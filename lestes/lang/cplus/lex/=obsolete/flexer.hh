/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___flexer_hh___included
#define lestes__lang__cplus__lex___flexer_hh___included

/*! \file
  Declaration of flexer class handling integration with flex.
  \author pt
*/

#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/lexer.hh>
#include <lestes/lang/cplus/lex/pp_token.hh>
#include <lestes/lang/cplus/lex/mapper.hh>
//#include <lestes/lang/cplus/lex/file_source.hh>
#include <lestes/lang/cplus/lex/source_file.hh>
#include <stack>
package(lestes);
package(lang);
package(cplus);
package(lex);

/*!
  Handles include stack and integration with flex. The state variables from include
  stack are saved and restored from local shortcuts for speed optimization.
  \author pt
*/
class flexer {
public:
	//! lexer buffer type
	typedef source_file::buffer_type buffer_type;
	//! location type
	typedef source_file::location_type location_type;
	//! type of character lex
	typedef lexer::char_type char_type;
	//! type of size in lex
	typedef lexer::size_type size_type;
	//! finalizes object
	~flexer(void);
	//! begins processing a source
	void push_file(const ::std::string &name);
	//! ends processing the current source
	void pop_file(void);
	//! returns the only instance
	static ptr<flexer> instance(void);
	//! stores next characters for YY_INPUT
	size_type yy_input(char_type *cbuf, size_type max);
	//! returns current buffer
	ptr<buffer_type> buffer_get(void);
	//! reads from flex
	ptr<pp_token> read(void);
	//! saves the current location
	void location_save(void);
	//! return the saved location
	ptr<location_type> location_get(void);
	//! extracts literal from buffer
	ucn_string extract_literal(size_type len);
private:
	//! type of state
	typedef source_file::state_type state_type;
	//! type of condition
	typedef source_file::condition_type condition_type;
	//! flex buffer type
	typedef source_file::yy_buffer_type yy_buffer_type;
	//! hides constructor
	flexer(void);
	//! hides copy constructor
	flexer(const flexer &copy);
	//! hides assignment operator
	flexer &operator=(const flexer &rhs);
	// TODO pt make this collectible
	//! stack of includes 
	::std::stack< srp<source_file> > includes;
	//! shortcut to lexer (only restore)
	srp<lexer> lex;
	//! shortcut to lex buffer (only restore)
	srp<buffer_type> buffer;
	//! shortcut to flex buffer (only restore)
	yy_buffer_type yy_buffer;
	//! shortcut to the state of the reader
	state_type state;
	//! shortcut to condition for flex
	condition_type condition;
	//! shortcut to saved token
	srp<pp_token> saved_token;
	//! shortcut to saved location
	srp<location_type> saved_location;
	//! only object of flexer class
	static ptr<flexer> singleton;
};

/*!
  Returns the only instance of the flexer.
  \return The singleton flexer object.
*/
inline ptr<flexer> flexer::instance(void)
{
	return singleton;
}

/*!
  Saves the location of the next token in buffer.
  \pre buffer != NULL
*/
inline void flexer::location_save(void)
{
	saved_location = buffer->peek()->location_get();
}

/*!
  Returns the saved location.
  \return  The location saved by location_save().
*/
inline ptr<flexer::location_type> flexer::location_get(void)
{
	return saved_location;
}

/*!
  Returns the current lexer buffer.
  \return Current lexer buffer.
*/  
inline ptr<flexer::buffer_type> flexer::buffer_get(void)
{
	return buffer;
}

/*!
  Fills buffer with characters from current source file.
  \param cbuf  The buffer to fill.
  \param max  Maximum count of characters to fill.  
  \return  The actual count of filled characters.
*/
inline flexer::size_type flexer::yy_input(char_type *cbuf, size_type max)
{
	return lex->yy_input(cbuf,max);
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
