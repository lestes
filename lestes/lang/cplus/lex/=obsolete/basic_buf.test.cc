/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/equality.hh>
#include <lestes/lang/cplus/lex/testing.hh>
#include <lestes/lang/cplus/lex/basic_buf.hh>

using namespace ::std;
using namespace ::lestes::lang::cplus::lex;

srp<char> create(char c)
{
	return srp<char>(new char(c));
}

int main(void)
{
	ptr< basic_buf<char,int> > cb = basic_buf<char,int>::create();

	cb->add(create(':'));
	holds(equal(cb->peek(),create(':')));
	cb->add(create('h'));
	holds(equal(cb->peek(),create(':')));
	cb->add(create('e'));

	cb->advance(1);
	holds(equal(cb->peek(),create('h')));

	cb->add(create('l'));
	cb->add(create('l'));
	cb->add(create('o'));
	cb->add(create('\0'));

	basic_buf<char,int>::vector_type *text = cb->remove(6);

	for (lint i = 0; i < 6; i++) holds(equal((*text)[i],create("hello"[i])));

	delete text;

	cerr << "testing basic_buf OK\n";
	return 0;
}

/* vim: set ft=lestes : */
