/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/flexer.hh>
#include <lestes/lang/cplus/lex/mapper_utf8.hh>
#include <lestes/lang/cplus/lex/cin_wrapper.hh>
#include <lestes/lang/cplus/lex/fstream_wrapper.hh>
#include <iostream>
#include <stack>

package(lestes);
package(lang);
package(cplus);
package(lex);

// include the flex generated parser
package(generated);
#include <lestes/lang/cplus/lex/flexer.cci>
end_package(generated);

using namespace ::std;

/*!
  Creates the object.
*/
flexer::flexer(void):   
{
}

/*!
  Finalizes the object.
*/
flexer::~flexer(void)
{
}

/*!
  Reads next token from flex parser. Handles flex conditions used for recognizing include constructs.
  \pre A file is being processed, i.e. includes.empty() == false
  \return  The next token on input.
*/
ptr<pp_token> flexer::read(void)
{
	ptr<pp_token> pp;
	pp_token_type ppt;

	if (state != END) {
		pp = generated::lexer_parse(condition);
		ppt = pp->type_get();
	/*   cerr << "got(" << state <<", "<<condition<<"):"<<pp.get_name()<<'\n';*/
	}
	
	switch (state) {
		case START:
			if (ppt == pp_token::TOK_HASH) {
				state = HASH;
			} else if (ppt == pp_token::TOK_FILE_END) {
				if (!!saved && saved->type_get() != pp_token::TOK_LINE_END) {
					/* defer EOF, insert fake EOL to simplify processing, checking for missing already done */
					state = END;
					/* FIXME duplicate location */
					saved = pp->clone();
					pp->type_set(pp_token::TOK_LINE_END);
				}
			}
			break;
		case HASH:
			if (ppt == pp_token::TOK_INCLUDE) {
				state = INCL;
				condition = include;
			} else if (ppt != pp_token::TOK_BLANK) {
				state = START;
				/*cerr << "state reset to START\n";*/
			}
			break;
		case INCL:
			if (ppt == pp_token::TOK_INCL_HCHAR ||
				 ppt == pp_token::TOK_INCL_QCHAR) {
				condition = INITIAL;
				state = START;
			} else if (ppt == pp_token::TOK_INCL_SIG) {
				// FIXME something else???
				condition = INITIAL;
				state = START;
			} else if (ppt == pp_token::TOK_BLANK) {
				/* empty */
			} else {
				/*cerr << "!!! this should not happen!!!\n";*/
				/* TODO this should and can not happen */
				assert(false);
			}
			break;
		case END:
			/*
			cerr << "state END in post_lex\n";
			*/
			pp = saved;
			break;
	}

	if (state != END) {
		saved = pp;
	}
	/*
	cerr << "ret " << pp.get_name() << '\n';
	*/
	return pp;
}

// TODO pt change this to istream_wrapper ??? definitely!
void flexer::push_file(const string &name) 
{
	if (!includes.empty()) {
		ptr<source_file> sf = includes.top();
		sf->save_state(state,condition,saved_token,saved_location);
	}

	// TODO pt make this work:

	ulint buffer_size = 1024;
	string encoding("ascii_7bit");
	
	yy_buffer_type yy_buffer = generated::yy_new_buffer(NULL,buffer_size);
	ptr<source_file> sf = source_file::create(name,encoding);
	generated::yy_switch_to_buffer(yy_buffer);

	sf->state_save(state,condition,saved_token,saved_location);
	sf->state_restore(state,condition,saved_token,saved_location,lex);
	buffer = lex->buffer_get();
	
	includes.push(sf);
}

void flexer::pop_file(void)
{
	assert(!includes.empty());

	ptr<source_file> sf = includes.top();
	yy_buffer_type yy_buffer = sf->yy_buffer_get();
	// TODO do not delete, better store to some linked list and reuse:
	// generated::yy_flush_buffer(yy_buffer);
	// yy_buffers.push(yy_buffer)

	generated::yy_delete_buffer(yy_buffer);
	includes.pop();

	// abandoned topmost file
	if (includes.empty()) return;

	sf = includes.top();
	
	// restore state of the parser
	yy_buffer = sf->yy_buffer_get();
	generated::yy_switch_to_buffer(yy_buffer);
	
	// restore shortcut variables
	current_buffer = sf->buffer_get();
	current_lex = sf->lexer_get();
}

ucn_string flexer::extract_literal(size_type len)
{
	buffer_type::vector_type *vec = current_buffer->remove(len);
	// reserve space
	ucn_string us(len,0xbeef);
	
	buffer_type::vector_type::iterator vit = vec->begin();
	ucn_string::iterator sit = us.begin();
	for (ulint i = 0; i < len; i++, vit++, sit++)
		*sit = (*vit)->value_get();   

	// TODO make collectible vector
	delete vec;

	return us;
}

srp<flexer> flexer::singleton(new flexer());

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);
/* vim: set ft=lestes : */

