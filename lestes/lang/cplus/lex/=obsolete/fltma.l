%{
/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
	#pragma GCC poison fprintf fwrite getc fread ferror
	#include <cassert>
	#define ECHO assert(("ECHO should not be used.",0))
	/*
GENERAL WARNING:
the manner in which this file is structured is _VERY_ important, it relies
on the implicit precedence of certain lexical rules:
1) longer match is caught every time (no need to worry about order of + and += rules)
2) in case of tie (i.e. two rules would match the longest portion), the first in the soucefile applies
	e.g. `bitand' is AMP and not ordinary IDENT (identifier)

the error-catching rules are set up so that every possible silly input is
covered and there should be no holes handled by implicit (print) rules
so be careful when modifying the patterns & stuff

IMPORTANT!: call to yyless changes yyleng
thus: yyless(yyleng - 1); SKIP(yyleng - 1); does NOT work properly!
	*/

using namespace ::lestes::std;
using namespace ::lestes::lang::cplus::lex;
using namespace ::std;

//file_src *fs;
//lexer *lex;
static ptr<flexer> flex = flexer::instance();

// formal, should not happen
#define yyterminate() return pp_token::create(loc,pp_token::TOK_FILE_END,pp_token::FLG_ERROR)

#define YY_INPUT(buf,result,max_size) \
{ \
	result = flex->get_lexer()->yy_input(buf,max_size); \
}

#define YY_DECL srp<pp_token> lexer_parse(int begin_condition)

#define ADVANCE     flex->get_lexer()->get_buf()->advance(yyleng)
#define SKIP(num)   flex->get_lexer()->get_buf()->advance(num)
#define REMOVE(num) flex->get_lexer()->get_buf()->remove(num)
#define PEEK        flex->get_lexer()->get_buf()->peek()
#define SAVELOC     loc = flex->get_lexer()->get_buf()->peek()->location_get()

//! prototype for flex parser function
YY_DECL;

/* FIXME move this somewhere else */
//! location of current token
static ptr<ucn_token::location_type> loc;

static ucn_string extract_literal(/* lexer *lex, */ulint len)
{
	ucn_token_buf::vector_type *vec = REMOVE(len);
	// reserve space
	ucn_string us(len,0xbeef);
	
	ucn_token_buf::vector_type::iterator vit = vec->begin();
	ucn_string::iterator sit = us.begin();
	for (ulint i = 0; i < len; i++, vit++, sit++)
		*sit = (*vit)->value_get();   

	// TODO make collectible vector
	delete vec;
	return us;
}

%}

%option noyywrap
%option always-interactive
	/* these setting reportedly yield better performance */
	/*
	%option never-interactive
	%option full
	%option read

	%option noreject
	%option noyymore

	%option noinput
	%option nounput
	%option noyy_push_state
	%option noyy_pop_state
	%option noyy_top_state
	%option noyy_scan_buffer
	%option noyy_scan_bytes
	%option noyy_scan_string
	%option nounput
	%option nounput
	%option nounput
	*/
%option nostack
%option noyylineno

%option noinput
%option nounput
%option noyy_push_state
%option noyy_pop_state
%option noyy_top_state
%option noyy_scan_buffer
%option noyy_scan_bytes
%option noyy_scan_string
%option nounput
%option nounput
%option nounput

ENDFILE    "\0"
NEWLINE    "\n"
TRANSLATED "\1"
DIGIT      [0-9]
NONZERO    [1-9]
OCTDIGIT   [0-7]
HEXDIGIT   ([0-9]|[a-f]|[A-F])
DOT        "."
HEXQUAD    {HEXDIGIT}{HEXDIGIT}{HEXDIGIT}{HEXDIGIT}
UCNSHORT   [\\]u{HEXQUAD}
UCNLONG    [\\]U{HEXQUAD}{HEXQUAD}
UCNESC     {UCNSHORT}|{UCNLONG}
UCNAME     {TRANSLATED}|{UCNESC}
OTHER      @
ALPHA      [abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ]
ALNUM      {ALPHA}|{DIGIT}
NONDIGIT   {ALPHA}|_|{UCNAME}
BLANK      [ \t\v\f]
ANYCHAR    (.|{NEWLINE})
ESCAPE     [ntvbrfa\\]|"\""|"'"|"?"

LONG_COMMENT_WARN ([/][*]([/]+[^*\0/]|[^*\0/]+|[*]+[^/\0*])*[/]+[*]([*]*[^/\0*]([^*\0]+|[*]+[^/\0*])*)?[*]+[/])
LONG_COMMENT ([/][*]([^*\0]+|([*]+[^/\0*]))*[*]+[/])

%x line_comment
%x long_comment
%x string_lit
%x wstring_lit
%x inter_char_lit
%x char_lit
%x wchar_lit
%x include

%%

	/** explicit beginnning condition (as parameter of lexer_parse) */
	BEGIN(begin_condition);

"//" {
	SAVELOC;
	BEGIN(line_comment);
	cerr << "begun line comment\n";
	ADVANCE;
}

"/*" {
	SAVELOC;
	/* prepare for line numbering */
	cerr << "begun long comment\n";
	BEGIN(long_comment);
	ADVANCE;
}

"'" {
	SAVELOC;
	BEGIN(char_lit);
	cerr << "begun charconst\n";
	ADVANCE;
}

"L'" {
	SAVELOC;
	BEGIN(wchar_lit);
	ADVANCE;
}

"\"" {
	SAVELOC;
	BEGIN(string_lit);
	ADVANCE;
}

"L\"" {
	SAVELOC;
	BEGIN(wstring_lit);
	ADVANCE;
}

{BLANK}+ {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_BLANK);
}

{NEWLINE} {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_LINE_END);
}

"[" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_LEFT_BRACKET);
}

"<:" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_LEFT_BRACKET,pp_token::FLG_ALT_SPELL);
}

"]" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_RIGHT_BRACKET);
}

":>" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_RIGHT_BRACKET,pp_token::FLG_ALT_SPELL);
}

"}" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_RIGHT_BRACE);
}

"%>" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_RIGHT_BRACE,pp_token::FLG_ALT_SPELL);
}

"{" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_LEFT_BRACE);
}

"<%" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_LEFT_BRACE,pp_token::FLG_ALT_SPELL);
}

"#" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_HASH);
}

"%:" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_HASH,pp_token::FLG_ALT_SPELL);
}

"##" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_HASH_HASH);
}

"%:%:" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_HASH_HASH,pp_token::FLG_ALT_SPELL);
}

")" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_RIGHT_PAR);
}

"(" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_LEFT_PAR);
}

";" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_SEMICOLON);
}

":" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_COLON);
}

"?" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_QMARK);
}

"." {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_DOT);
}

"+" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_PLUS);
}

"*" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_STAR);
}

"%" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_PERCENT);
}

"/" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_SLASH);
}

"^" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_HAT);
}

"xor" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_HAT,pp_token::FLG_ALT_SPELL);
}

"&" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_AMP);
}

"bitand" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_AMP,pp_token::FLG_ALT_SPELL);
}

"|" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_VBAR);
}

"bitor" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_VBAR,pp_token::FLG_ALT_SPELL);
}

"~" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_TILDE);
}

"compl" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_TILDE,pp_token::FLG_ALT_SPELL);
}

"!" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_EMARK);
}

"not" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_EMARK,pp_token::FLG_ALT_SPELL);
}

"=" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_EQ);
}

"<" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_LT);
}

">" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_GT);
}

"," {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_COMMA);
}

"-" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_MINUS);
}

"::" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_COLON_COLON);
}

".*" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_DOT_STAR);
}

"+=" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_PLUS_EQ);
}

"-=" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_MINUS_EQ);
}

"*=" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_STAR_EQ);
}

"/=" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_SLASH_EQ);
}

"%=" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_PERCENT_EQ);
}

"^=" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_HAT_EQ);
}

"xor_eq" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_HAT_EQ,pp_token::FLG_ALT_SPELL);
}

"&=" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_AMP_EQ);
}

"and_eq" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_AMP_EQ,pp_token::FLG_ALT_SPELL);
}

"|=" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_VBAR_EQ);
}

"or_eq" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_VBAR_EQ,pp_token::FLG_ALT_SPELL);
}

"<<" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_LT_LT);
}

">>" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_GT_GT);
}

">>=" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_GT_GT_EQ);
}

"<<=" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_LT_LT_EQ);
}

"==" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_EQ_EQ);
}

"!=" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_EMARK_EQ);
}

"not_eq" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_EMARK_EQ,pp_token::FLG_ALT_SPELL);
}

"<=" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_LT_EQ);
}

">=" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_GT_EQ);
}

"&&" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_AMP_AMP);
}

"and" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_AMP_AMP,pp_token::FLG_ALT_SPELL);
}

"||" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_VBAR_VBAR);
}

"or" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_VBAR_VBAR,pp_token::FLG_ALT_SPELL);
}

"++" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_PLUS_PLUS);
}

"--" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_MINUS_MINUS);
}

"->*" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_MINUS_GT_STAR);
}

"->" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_MINUS_GT);
}

"..." {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_DOT_DOT_DOT);
}

	/* TODO remove all this? NO, WE _definitely_ _NEED_ IT */

"asm" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_ASM);
}


"auto" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_AUTO);
}


"bool" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_BOOL);
}


"break" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_BREAK);
}


"case" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_CASE);
}


"catch" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_CATCH);
}


"char" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_CHAR);
}


"class" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_CLASS);
}


"const_cast" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_CONST_CAST);
}


"continue" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_CONTINUE);
}


"default" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_DEFAULT);
}


"delete" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_DELETE);
}


"do" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_DO);
}


"double" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_DOUBLE);
}


"dynamic_cast" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_DYNAMIC_CAST);
}


"else" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_ELSE);
}


"enum" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_ENUM);
}


"explicit" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_EXPLICIT);
}


"export" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_EXPORT);
}


"extern" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_EXTERN);
}

"float" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_FLOAT);
}


"for" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_FOR);
}


"friend" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_FRIEND);
}


"goto" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_GOTO);
}


"if" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_IF);
}


"inline" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_INLINE);
}


"int" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_INT);
}


"long" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_LONG);
}


"mutable" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_MUTABLE);
}


"namespace" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_NAMESPACE);
}


"new" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_NEW);
}


"operator" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_OPERATOR);
}


"private" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_PRIVATE);
}


"protected" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_PROTECTED);
}


"public" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_PUBLIC);
}


"register" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_REGISTER);
}


"reinterpret_cast" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_REINTERPRET_CAST);
}


"return" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_RETURN);
}


"short" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_SHORT);
}


"signed" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_SIGNED);
}


"sizeof" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_SIZEOF);
}


"static" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_STATIC);
}


"static_cast" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_STATIC_CAST);
}


"struct" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_STRUCT);
}


"switch" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_SWITCH);
}


"template" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_TEMPLATE);
}


"this" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_THIS);
}


"throw" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_THROW);
}


"try" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_TRY);
}


"typedef" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_TYPEDEF);
}


"typeid" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_TYPEID);
}


"typename" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_TYPENAME);
}


"union" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_UNION);
}


"unsigned" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_UNSIGNED);
}


"using" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_USING);
}


"virtual" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_VIRTUAL);
}


"void" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_VOID);
}


"volatile" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_VOLATILE);
}


"wchar_t" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_WCHAR_T);
}


"while" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_WHILE);
}

"false" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_FALSE_LIT);
}

"true" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_TRUE_LIT);
}

	/* I need to recognize at least "include" in this stage */
	/* don't forget, that "if" && "else" are also keywords!! 
		so we don't declare them here
"if" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_IF);
}

"else" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_ELSE);
}
	*/

"include" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_INCLUDE,chars_to_ucn_string("include"));
}

"define" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_DEFINE,chars_to_ucn_string("define"));
}

"undef" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_UNDEF,chars_to_ucn_string("undef"));
}

"line" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_LINE,chars_to_ucn_string("line"));
}

"pragma" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_PRAGMA,chars_to_ucn_string("pragma"));
}

"error" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_ERROR,chars_to_ucn_string("error"));
}

"defined" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_DEFINED,chars_to_ucn_string("defined"));
}

"ifdef" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_IFDEF,chars_to_ucn_string("ifdef"));
}

"ifndef" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_IFNDEF,chars_to_ucn_string("ifndef"));
}

"elif" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_ELIF,chars_to_ucn_string("elif"));
}

"endif" {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_ENDIF,chars_to_ucn_string("endif"));
}

	/* pp number */
({DIGIT}|{DOT}{DIGIT})({DIGIT}|{ALPHA}|[eE][-+]|{DOT})* {
	SAVELOC;
	return pp_token::create(loc,pp_token::TOK_NUMBER,extract_literal(yyleng));
}

	/* pp won't be number (ucn or underscore) - next best match */
({DIGIT}|{DOT}{DIGIT})({DIGIT}|{NONDIGIT}|[eE][-+]|{DOT})* {
	SAVELOC;
	/* FIXME should we retain ILL_NUMBER, what about signalling in FLG_ERROR? */
	return pp_token::create(loc,pp_token::TOK_ILL_NUMBER,extract_literal(yyleng));
}

	/* pp identifier without digits FIXME (is that necessary?) perhaps for keywords ... */
({ALPHA}|_)+ {
	SAVELOC;
	return pp_token::create(loc,pp_token::TOK_ALPHA_IDENT,extract_literal(yyleng));
}

	/* pp alphanumeric identifier */
({ALNUM}|_)+ {
	SAVELOC;
	return pp_token::create(loc,pp_token::TOK_ALNUM_IDENT,extract_literal(yyleng));
}

	/* pp generic identifier (contains ucns) */
{NONDIGIT}({DIGIT}|{NONDIGIT})* {
	SAVELOC;
	return pp_token::create(loc,pp_token::TOK_UCN_IDENT,extract_literal(yyleng));
}

<include>{LONG_COMMENT_WARN} {
	SAVELOC;
	cerr << "eating long_comment_warn `"<<yytext<<"'\n";
	cerr << "warning: / * sequence(s) within comment\n";
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_BLANK);
}

<include>({BLANK}) {
	SAVELOC;
	cerr << "eating blank `" << yytext << "'\n";
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_BLANK);
}

<include>({LONG_COMMENT}) {
	SAVELOC;
	cerr << "eating long_comment `" << yytext << "'\n";
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_BLANK);
}
	/*
	<include>({BLANK}|{LONG_COMMENT})+ {
	SAVELOC;
	cerr << "eating bunch of blanks & long_comments `" << yytext << "'\n";
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_BLANK);
	}
	*/

<include>[<][^\n\0>]*[>]({BLANK}|{LONG_COMMENT})*("//"[^\0\n]*)*{NEWLINE} {
	SAVELOC;
	/* everything between the <> is significant */
	ulint i, len;
	for (i = 0; i < yyleng; i++) {
		if (yytext[i] == '>') {
			len = i + 1;
			break;
		}
	}
	
	yyless(len); // return back the potential comments
	SKIP(1);
	ucn_string us(extract_literal(len - 2));
	SKIP(1);
	return pp_token::create(loc,pp_token::TOK_INCL_HCHAR,us);
}

<include>[""][^\n\0""]*[""]({BLANK}|{LONG_COMMENT})*("//"[^\0\n]*)*{NEWLINE} {
	/* TODO this does not work, search for " is insufficient (we should not support it as a string)
		<include>[""]([^\n\0\\""]|[\\][^\0])*[""]({BLANK}|{LONG_COMMENT})*("//"[^\0\n]*)*{NEWLINE} {*/
	SAVELOC;
	/* everything between the "" is significant */
	ulint i, len;

	for (i = 1; i < yyleng; i++) {
		if (yytext[i] == '"') {
			len = i + 1;
			break;
		}
	}

	yyless(len); // return back the potential comments
	SKIP(1);
	ucn_string us(extract_literal(len - 2));
	SKIP(1);
	
	return pp_token::create(loc,pp_token::TOK_INCL_QCHAR,us);
}

	/* anything else shall be third type include, reset */
<include>{ANYCHAR} {
	SAVELOC;
	cerr << "include other `" << yytext << "'\n";
	yyless(0); // return everything
	/* do not use ADVANCE here! */
	return pp_token::create(loc,pp_token::TOK_INCL_SIG);
}

<line_comment>[^\f\v\n\0]*{BLANK}*({NEWLINE}|{ENDFILE}) {
	cerr << "eating line comment\n";
	ulint len = yyleng - 1;
	yyless(len);
	SKIP(len); // leave newline or endfile in buffer   
	return pp_token::create(loc,pp_token::TOK_BLANK);
}
	
<line_comment>[^\n\0]+[^ \t\n\v\f\0]+[^\n\0]*({NEWLINE}|{ENDFILE}) {
	/* warning: form feed or vertical tab within a line comment
		not separated by blanks; no diagnostic required */
	cerr << "warning: ff or vt wrong in line comment\n";
	ulint len = yyleng - 1;
	yyless(len);
	SKIP(len); // leave newline in buffer   
	return pp_token::create(loc,pp_token::TOK_BLANK);
}
	
<long_comment>{ENDFILE} {
	/* error: eof within comment == unterminated comment */
	cerr << "ERROR: EOF within comment\n";
	/* FIXME remove return pp_token::create(pp_token::TOK_ERROR);*/
	return pp_token::create(loc,pp_token::TOK_BLANK,pp_token::FLG_ERROR);
}

	/* TODO check this RX */
<long_comment>[^/\0*]+ {
	cerr << "eating comment `" << yytext << "'\n";
	ADVANCE;
}

	/* TODO check this RX */
<long_comment>[/]+[*]+[/] {
	/* TODO beware of the newline */
	cerr << "warning: /* within ending comment `" << yytext << "'\n";
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_BLANK);
}

	/* TODO check this RX */
<long_comment>[/]+[*]+ {
	/* beware of the newline */
	cerr << "warning: /* within comment `" << yytext << "'\n";
	ADVANCE;
}

	/* TODO check this RX */
<long_comment>[/]+[^*\0/]* {
	/* TODO beware of the newline */
	cerr << "eating comment with / `" << yytext << "'\n";
	ADVANCE;
}

	/* TODO check this RX */
<long_comment>[*]+[^*\0/]+ {
	cerr << "eating comment stars `" << yytext << "'\n";
	ADVANCE;
}

	/* TODO check this RX */
<long_comment>[*]+[/] {
	cerr << "comment end `" << yytext << "'\n";
	/*BEGIN(INITIAL);*/
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_BLANK);
}

	/* interpret afterwards */
<char_lit>([^\n\0\\']|[\\][^\0])+['] {
	ulint len = yyleng - 1;
	ucn_string us(extract_literal(yyleng - 1));
	SKIP(1);
	return pp_token::create(loc,pp_token::TOK_CHAR_LIT,us);
}

<char_lit>['] {
	/* error: empty charlit */
	cerr << "ERROR: empty charlit\n";
	ADVANCE;
	/* recover by sending represetnation of zero character */
	return pp_token::create(loc,pp_token::TOK_CHAR_LIT,chars_to_ucn_string("\0"),pp_token::FLG_ERROR);
}

<char_lit>([^\n\0\\']|[\\][^\0])*{ANYCHAR} {
	/* error: unterminated charlit */
	cerr << "ERROR: somehow unterminated charlit\n";
	ulint len = yyleng - 1;
	yyless(len); /* keep the ill character */
	return pp_token::create(loc,pp_token::TOK_CHAR_LIT,extract_literal(len),pp_token::FLG_ERROR);
}

<wchar_lit>([^\n\\']|[\\][^\0])+['] {
	ucn_string us(extract_literal(yyleng - 1));
	SKIP(1);
	return pp_token::create(loc,pp_token::TOK_WCHAR_LIT,us);
}

<wchar_lit>['] {
	/* error: empty wcharlit */
	cerr << "ERROR: empty wcharlit\n";
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_WCHAR_LIT,chars_to_ucn_string("\0"),pp_token::FLG_ERROR);
}

<wchar_lit>([^\n\0\\']|[\\][^\0])*{ANYCHAR} {
	/* error: unterminated charlit */
	cerr << "ERROR: somehow unterminated wcharlit\n";

	ulint len = yyleng - 1;
	yyless(len); /* keep the ill character */
	return pp_token::create(loc,pp_token::TOK_WCHAR_LIT,extract_literal(len),pp_token::FLG_ERROR);
}

<string_lit>([^\n\0\\""]|[\\][^\0])*[""] {
	ulint len = yyleng - 1;
	ucn_string us(extract_literal(len));
	SKIP(1);
	return pp_token::create(loc,pp_token::TOK_STRING_LIT,us);
}

<string_lit>([^\n\0\\""]|[\\][^\0])*{ANYCHAR} {
	/* error: unterminated stringlit */
	cerr << "ERROR: somehow unterminated stringlit\n";
	ulint len = yyleng - 1;
	yyless(len); /* keep the ill character */
	return pp_token::create(loc,pp_token::TOK_STRING_LIT,extract_literal(len),pp_token::FLG_ERROR);
}

<wstring_lit>([^\n\0\\""]|[\\][^\0])*[""] {
	ulint len = yyleng - 1;
	ucn_string us(extract_literal(len));
	SKIP(1);
	return pp_token::create(loc,pp_token::TOK_WSTRING_LIT,us);
}

<wstring_lit>([^\n\0\\""]|[\\][^\0])*{ANYCHAR} {
	/* unterminated wstringlit */
	cerr << "ERROR: somehow unterminated wstringlit\n";
	ulint len = yyleng - 1;
	yyless(len); /* keep the ill character */
	return pp_token::create(loc,pp_token::TOK_WSTRING_LIT,extract_literal(len),pp_token::FLG_ERROR);
}

{ENDFILE} {
	SAVELOC;
	ADVANCE;
	return pp_token::create(loc,pp_token::TOK_FILE_END);
}

	/* this is useful for stupid arguments of macros */
. {
	SAVELOC;
	ucn_token_buf::vector_type *vec = REMOVE(1);
	ucn_string us(1,(*vec)[0]->value_get());

	return pp_token::create(loc,pp_token::TOK_OTHER,us);
}

%%

/* vim: set ft=lestes : */
