/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___source_encoding_hh___included
#define lestes__lang__cplus__lex___source_encoding_hh___included

/*! \file
  Declaration of source_encoding class wrapping source stream character encoding.
  \author pt
*/

#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/ucn_token.hh>
#include <lestes/lang/cplus/lex/ucn_filter.hh>
#include <lestes/lang/cplus/lex/mapper.hh>
package(lestes);
package(lang);
package(cplus);
package(lex);

/*!
  Wrapper for source character set encoders.
*/
class source_encoding: public ucn_filter {
public:
	//! type of encoder class
	typedef encoder encoder_type;
	//! finalizes the object
	~source_encoding(void);
	//! reads next token
	srp<ucn_token> read(void);
	//! sets input into filter
	void input_set(const ptr<ucn_filter> &a_input);
	//! returns input into filter
	srp<ucn_filter> input_get(void);
	//! returns new instance, initialize with encoder
	static srp<source_encoding> create(const ptr<encoder_type> &a_map);
protected:
	//! initialize with non NULL mapper
	source_encoding(const ptr<mapper_type> &a_map);
private:
	//! hides copy constructor
	source_encoding(const source_encoding &copy);
	//! hides assignment operator
	source_encoding &operator=(const source_encoding &rhs);
	//! current mapper
	srp<mapper_type> map;
};

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
