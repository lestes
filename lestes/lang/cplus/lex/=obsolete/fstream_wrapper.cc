/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  Definition of fstream_wrapper class wrapping file stream.
  \author pt
*/

#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/fstream_wrapper.hh>
#include <fstream>

package(lestes);
package(lang);
package(cplus);
package(lex);

using namespace ::std;

/*!
  Creates new file stream for file name.
  \param a_name The name of the file.
*/
fstream_wrapper::fstream_wrapper(const string &a_name):
	stream(a_name.c_str())
{
}

/*!
  Finalizes the object.
*/
fstream_wrapper::~fstream_wrapper(void)
{
}

/*!
  Returns the underlying stream.
  \return The stream
*/
istream *fstream_wrapper::stream_get(void)
{
	return &stream;
}

/*!
  Returns new instance, initializes with file stream.
  \param a_name  The name of the file to open.
  \return  The wrapper for the created file stream.
*/
ptr<fstream_wrapper> fstream_wrapper::create(const string &a_name)
{
	return ptr<fstream_wrapper>(new fstream_wrapper(a_name));
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);
/* vim: set ft=lestes : */
