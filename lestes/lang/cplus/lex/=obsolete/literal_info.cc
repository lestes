/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Literal token properties. 
  
  Definition of literal_info class representing properties of literal cpp_token.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/literal_info.hh>
#include <lestes/lang/cplus/sem/ss_type.g.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

/*!
  Creates the object.
  \param a_type  The SS type of the literal.
  \param a_integral_flag  The flag of integral type.
  \param a_integral  The value of the integral type.
*/
literal_info::literal_info(const ptr<ss_type> &a_type, bool a_integral_flag, integral_type a_integral):
	type(a_type),
	integral_flag(a_integral_flag),
	integral(a_integral)
{
}

/*!
  Returns numerical value for integer literals.
  \pre is_integral() == true
  \return  The value of the literal.
*/
literal_info::integral_type literal_info::integral_get(void) const
{
	lassert(is_integral());
	return integral;
}

/*!
  Returns integral flag.
  \return true  If the represented literal is integral.
*/
bool literal_info::is_integral(void) const
{
	return integral_flag;
}

/*!
  Returns the data type of the literal.
  \return  The ss type.
*/
ptr<ss_type> literal_info::type_get(void) const
{
	return type;
}

/*!
  Returns new literal, initializes with type.
  \param a_type  The data type of the literal.
  \return  The new literal.
*/
ptr<literal_info> literal_info::create(const ptr<ss_type> &a_type)
{
	// integral value is not used
	return new literal_info(a_type,false,0);
}

/*!
  Returns new integral literal, initializes with type and value.
  \param a_type  The data type of the literal.
  \param a_integral  The value of the literal.
  \return  The new literal.
*/
ptr<literal_info> literal_info::create(const ptr<ss_type> &a_type, integral_type a_integral)
{
	return new literal_info(a_type,true,a_integral);
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
