/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  TODO rename to flexer_state
  Definition of source_file class representing internal flexer state.
  \author pt
*/

#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/source_file.hh>
#include <lestes/lang/cplus/lex/fstream_wrapper.hh>
#include <lestes/lang/cplus/lex/pp_token.hh>
#include <string>
#include <stack>

package(lestes);
package(lang);
package(cplus);
package(lex);

using namespace ::std;

/*!
  Creates the object, initializes with TODO pt: change the signature.
  \param a_name  The name of the source file.
  \param a_encoding  The encoding of the source file.
*/
source_file::source_file(
      // TODO pt substitute name  for const ptr<istream_wrapper> &a_wrapper,
      const string &a_name,
      const string &a_encoding):
// TODO pt add parameter checks
   name(a_name),
   encoding(a_encoding),
   // TODO pt create mapper from encoding string
   map(mapper_7bit::create()),
   fs(file_source::create(fstream_wrapper::create(a_name))),
   lex(lexer::create(fs,map))
{
}

/*!
  Returns associated lexer.
  \return The lexer associated with the source file.
*/
ptr<lexer> source_file::lexer_get(void)
{
   return lex;
}

/*!
  Returns new instance, initializes with TODO pt: change the signature.
  \param a_name  The name of the source file.
  \param a_encoding  The encoding of the source file.
*/
ptr<source_file> source_file::create(/*const ptr<istream_wrapper> &a_wrapper,*/
      const string &a_name, const string &a_encoding)
{
   return srp<source_file>(new source_file(a_name,a_encoding,a_yy_buffer));
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);
