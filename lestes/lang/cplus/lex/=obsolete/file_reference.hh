/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___file_reference_hh___included
#define lestes__lang__cplus__lex___file_reference_hh___included

#include <lestes/common.hh>
#include <string>

package(lestes);
package(lang);
package(cplus);
package(lex);

/*!
  reference to file inside the inclusion chain
*/  
class file_reference {
public:
	/*
	file_reference(void);   
	file_reference(const file_reference &copy);
	explicit file_reference(const ::std::string &a_name);
	file_reference(const ::std::string &a_name, file_reference &a_parent);*/
	//! destructor
	~file_reference(void);
	//! return file name
	::std::string name_get(void) const;
	//! return parent file
	srp<file_reference> parent_get(void) const;
	/* FIXME remove ?
	void set_name(const ::std::string &a_name);
	bool has_parent(void);
	void set_parent(file_reference &a_parent);
	void set_parent(void);
	*/
	//! equality test
	bool equals(const ptr<file_reference> &rhs) const;
	//! return file reference initialized with file name
	static srp<file_reference> create(const ::std::string &a_name);
	//! return file reference initialized with file name and parent file
	static srp<file_reference> create(const ::std::string &a_name,
				const ptr<file_reference> &a_parent);
protected:   
	//! initialize with file name and parent file
	file_reference(const ::std::string &a_name, const ptr<file_reference> &a_parent);
private:
	// FIXME this should really be something like ucn_string ...
	//! name of the file
	::std::string name;
	//! parent in the include hierarchy
	srp<file_reference> parent;
	//! hide copy constructor
	file_reference(const file_reference &copy);
	//! hide assingment operator
	file_reference &operator=(const file_reference &rhs);
};

bool equal(const ptr<file_reference> &first, const ptr<file_reference> &second);

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
