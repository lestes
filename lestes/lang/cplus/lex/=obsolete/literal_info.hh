/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___literal_info_hh___included
#define lestes__lang__cplus__lex___literal_info_hh___included

/*! \file
  \brief Literal token properties. 
  
  Declaration of literal_info class representing properties of literal cpp_token.
  \author pt
*/
#include <lestes/common.hh>

package(lestes);
package(lang);
package(cplus);

package(sem);
// forward declaration to break cycle
class ss_type;
end_package(sem);

package(lex);

/*!
  \brief Literal token properties. 
  
  Represents properties of literal C++ tokens.
  C++ grammar does not distinguish literals and so does cpp_token, having only TOK_LITERAL.
  The information gathered while interpreting literals is encapsulated in this class.
*/
class literal_info: public ::lestes::std::object {
public:
	//! Type of integral value.
	// TODO temporary typedef, shall be t_size
	typedef lint integral_type;
	//! Returns numerical value.
	integral_type integral_get(void) const;
	//! Returns integral flag.
	bool is_integral(void) const;
	//! Returns the data type.
	ptr<ss_type> type_get(void) const;
	//! Returns new instance.
	static ptr<literal_info> create(const ptr<ss_type> &a_type);
	//! Returns new integral instance.
	static ptr<literal_info> create(const ptr<ss_type> &a_type, integral_type a_integral);
protected:
	//! Creates the object.
	literal_info(const ptr<ss_type> &a_type, bool a_integral_flag, integral_type a_integral);
private:
	//! The ss type representing the literal.
	srp<ss_type> type;
	//! The integral flag.
	bool integral_flag;
	//! The value for integer literals.
	integral_type integral;
	//! Hides copy constructor.
	literal_info(const literal_info &);
	//! Hides assignment operator.
	literal_info &operator=(const literal_info &);
};

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
