/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___cin_wrapper_hh___included
#define lestes__lang__cplus__lex___cin_wrapper_hh___included

/*! \file
  \brief Standard input wrapper.
  
  Declaration of cin_wrapper class wrapping standard input.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/istream_wrapper.hh>
#include <iostream>

package(lestes);
package(lang);
package(cplus);
package(lex);

/*!
  \brief Standard input wrapper.
  
  Wrapper for the standard input stream.
*/
class cin_wrapper: public istream_wrapper {
public:
	//! Returns the underlying stream.
	::std::istream *stream_get(void);
	//! Returns new instance.
	static ptr<cin_wrapper> create(void);
protected:
	//! Creates the object.
	cin_wrapper(void);
private:
	//! Hides copy constructor.
	cin_wrapper(const cin_wrapper &);
	//! Hides assignment operator.
	cin_wrapper &operator=(const cin_wrapper &);
};

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */

