/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___flexer_hh___included
#define lestes__lang__cplus__lex___flexer_hh___included

/*! \file
  Declaration of flexer_state class representing state of the flexer.
  \author pt
*/

#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/ucn_token.hh>
#include <lestes/lang/cplus/lex/source_file.hh>
#include <lestes/lang/cplus/lex/pp_token.hh>
package(lestes);
package(lang);
package(cplus);
package(lex);

package(generated);
//! lexer buffer state
struct yy_buffer_state;
end_package(generated);

/*!
  Represents state of the flexer, which must be kept for every active included file.
*/
class flexer_state {
public:
	//! type of flex buffer
	typedef struct generated::yy_buffer_state *yy_buffer_type;
	//! states of the flexer reader
	typedef enum {
		//! ordinary situation
		START = 0,
		//! encountered '#' mark
		HASH = 1,
		//! encountered 'include'
		INCL = 2,
		//! end of file
		END = 3,
		//! uninitialized object
		INVALID = 4
	} state_type;
	//! type of condition in flex
	typedef lint condition_type;
	//! location type
	typedef ucn_token::location_type location_type;

	
	//! finalizes object
	~flexer_state(void);
	//! saves state
	void save(const yy_buffer_type &a_yy_buffer, const ptr<source_file> &a_source,
			const state_type &a_state, const condition_type &a_condition,
			const ptr<pp_token> &a_saved_token, const ptr<location_type> &a_saved_location);
	//! restores state
	void restore(yy_buffer_type &a_yy_buffer, ptr<source_file> &a_source,
			state_type &a_state, condition_type &a_condition,
			ptr<pp_token> &a_saved_token, ptr<location_type> &a_saved_location);
	//! returns empty state
	static ptr<flexer_state> create(void);   
protected:
	//! creates empty state
	flexer_state(void);
private:
	//! hides copy constructor
	flexer_state(const flexer_state &);
	//! hides assignment operator
	flexer_state &operator=(const flexer_state &);

	//! yylex buffer state
	yy_buffer_type yy_buffer;
	//! the processed file
	srp<source_file> source;
	//! the state of the reader
	state_type state;
	//! the condition for yylex
	condition_type condition;
	//! saved token
	srp<pp_token> saved_token;
	//! saved location
	srp<location_type> saved_location;
};

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */

