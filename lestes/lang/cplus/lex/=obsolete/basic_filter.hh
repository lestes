/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___basic_filter_hh___included
#define lestes__lang__cplus__lex___basic_filter_hh___included

/*! \file
  Declaration of basic_filter class representing token filter.
  \author pt
*/

#include <lestes/common.hh>
package(lestes);
package(lang);
package(cplus);
package(lex);

/*!
  Template class for all kinds of token filters.
  Performs no operation, just interface skeleton.
  \param Token The underlying processed token for the filter.
*/
template <typename Token>
class basic_filter {
public:
	//! type of processed token
	typedef Token token_type;
	//! finalizes the object
	virtual ~basic_filter(void);
	//! sets the input coming into filter
	virtual void input_set(ptr< basic_filter<Token> >a_input);
	//! returns input coming into filter
	virtual ptr< basic_filter<Token> > input_get(void);
	//! reads single token from the filter
	virtual ptr<Token> read(void) = 0;
	//! returns new instance of filter
	static ptr< basic_filter<Token> > create(void);
protected:
	//! creates new object, initializes with no input
	basic_filter(void);
	//! reads single token from input
	ptr<Token> input_read(void);
private:
	//! hides copy constructor
	basic_filter(const basic_filter<Token> &copy);
	//! hides assignment operator
	basic_filter<Token> &operator=(const basic_filter<Token> &rhs);
	//! input into this filter
	srp< basic_filter<Token> >input;
};

/*!
  Creates new filter with no input.
  \post input == NULL
*/
template <typename Token>
basic_filter<Token>::basic_filter(void):
	input(NULL)
{
}

/*!
  Finalizes the object.
*/
template <typename Token>
basic_filter<Token>::~basic_filter(void)
{
}

/*!
  Reads next token from input into this filter.
  \pre input != NULL
  \return Next token from input.
*/
template <typename Token>
inline ptr<Token> basic_filter<Token>::input_read(void)
{
	lassert(input);
	return input->read();
}

/*!
  Sets input into this filter.
  \param a_input The new input.
*/
template <typename Token>
void basic_filter<Token>::input_set(ptr< basic_filter<Token> > a_input)
{
	input = a_input;
}

/*!
  Returns current input into the filter.
  \return The current input.
*/
template <typename Token>
ptr< basic_filter<Token> > basic_filter<Token>::input_get(void) const
{
	return input;
}

/*!
  Creates new instance of filter.
*/
template <typename Token>
ptr< basic_filter<Token> > basic_filter<Token>::create(void)  
{
	return srp< basic_filter<Token> >(new basic_filter());
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
