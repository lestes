/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  Definition of flexer_state class representing state of the flexer.
  \author pt
*/

#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/flexer_state.hh>
#include <lestes/lang/cplus/lex/pp_token.hh>
#include <string>
package(lestes);
package(lang);
package(cplus);
package(lex);

using namespace ::std;

/*!
  Creates empty flexer state.
  \post state == INVALID
*/
void flexer_state::flexer_state(void):
	yy_buffer(NULL),
	source(),
	state(INVALID),
	condition(),
	saved_token(),
	saved_location()
{
}

/*!
  Finalizes object.
*/
flexer_state::~flexer_state(void)
{
}

/*!
  Saves the processed file dependent flexer context.
  The content is passed in the parameters.
  \pre a_state != INVALID
  \param a_yy_buffer  The yylex internal buffer.
  \param a_source  The source file.
  \param a_state  The flexer parser state.
  \param a_condition  The yylex condition.
  \param a_saved_token  The flexer saved token.
  \param a_saved_location  The flexer saved location.
*/
void save(const yy_buffer_type &a_yy_buffer, const ptr<source_file> &a_source,
		const state_type &a_state, const condition_type &a_condition,
		const ptr<pp_token> a_saved_token, const ptr<location_type> &a_saved_location)
{
	assert(a_state != INVALID);
	yy_buffer = a_yy_buffer;
	source = a_source;
	state = a_state;
	condition = a_condition;
	saved_token = a_saved_token;
	saved_location = a_saved_location;
}

/*!
  Restores the processed file dependent flexer context.
  The content is restored into the parameters.
  \pre state != INVALID
  \param a_yy_buffer  The yylex internal buffer.
  \param a_source  The source file.
  \param a_state  The flexer parser state.
  \param a_condition  The yylex condition.
  \param a_saved_token  The flexer saved token.
  \param a_saved_location  The flexer saved location.
*/
void restore(yy_buffer_type &a_yy_buffer, ptr<source_file> &a_source,
		state_type &a_state, condition_type &a_condition,
		ptr<pp_token> a_saved_token, ptr<location_type> &a_saved_location)
{
	assert(state != INVALID);
	a_yy_buffer = yy_buffer;
	a_source = source;
	a_state = state;
	a_condition = condition;
	a_saved_token = saved_token;
	a_saved_location = saved_location;
}

/*!
  Returns empty flexer state.
  \post state == INVALID
  \return Empty flexer state.
*/
ptr<flexer_state> flexer_state::create(void)
{
	return ptr<flexer_state>(new flexer_state());
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

/* vim: set ft=lestes : */

