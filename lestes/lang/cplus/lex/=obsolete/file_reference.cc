/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/equality.hh>
#include <lestes/lang/cplus/lex/file_reference.hh>
#include <string>
package(lestes);
package(lang);
package(cplus);
package(lex);

using namespace ::std;

/* TODO remove ?
file_reference::file_reference(void):
	name(""),
	parent(NULL)
{
}

file_reference::file_reference(const file_reference &copy):
	name(copy.name),
	parent(copy.parent)
{
}

file_reference::file_reference(const string &a_name):
	name(a_name),
	parent(NULL)
{
}
*/

file_reference::file_reference(const string &a_name, const ptr<file_reference> &a_parent):
	name(a_name),
	parent(a_parent)
{
}

file_reference::~file_reference(void)
{
}

/*
file_reference &file_reference::operator=(const file_reference &rhs)
{
	if (&rhs != this) {
		set_name(rhs.name);
		set_parent(*(rhs.parent));
	}
	return *this;
}
*/

string file_reference::name_get(void) const
{
	return string(name);
}

/*
void file_reference::set_name(const ::std::string &a_name)
{
	name = a_name;
}
*/

srp<file_reference> file_reference::parent_get(void) const
{
	return parent;
}

/* FIXME remove ?
bool file_reference::has_parent(void)
{
	return parent != NULL;
}

void file_reference::set_parent(file_reference &a_parent)
{
	parent = &a_parent;
}

void file_reference::set_parent(void)
{
	parent = NULL;
}
*/

bool file_reference::equals(const ptr<file_reference> &rhs) const
{
	return equal(name,rhs->name_get()) && equal(parent,rhs->parent_get());
}

srp<file_reference> file_reference::create(const string &a_name)
{
	return srp<file_reference>(new file_reference(a_name,ptr<file_reference>()));
}

srp<file_reference> file_reference::create(const string &a_name,
			const ptr<file_reference> &a_parent)
{
	return srp<file_reference>(new file_reference(a_name,a_parent));
}

bool equal(const ptr<file_reference> &first, const ptr<file_reference> &second)
{
	return first->equals(second);
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);
/* vim: set ft=lestes : */
