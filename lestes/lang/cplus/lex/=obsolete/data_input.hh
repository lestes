/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___data_input_hh___included
#define lestes__lang__cplus__lex___data_input_hh___included

#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/ucn_token.hh>
#include <lestes/lang/cplus/lex/ucn_filter.hh>
#include <lestes/lang/cplus/lex/data_source.hh>
package(lestes);
package(lang);
package(cplus);
package(lex);

/*!
  input manager for different sources
  used to read data from descendants of data_source
*/
class data_input: public ucn_filter {
public:
	//! type of source class
	typedef data_source source_type;
	//! destructor
	~data_input(void);
	
	/* TODO remove ?
	//! set new non NULL source for input
	void set_source(const ptr<source_type> &a_source);
	*/
	//! return source for input
	ptr<source_type> get_source(void);
	//! read next token
	ptr<ucn_token> read(void);
	//! return new data_input, initialize with non NULL source for input
	static ptr<data_input> create(const ptr<source_type> &a_source);
protected:
	//! initialize with non NULL source for input
	data_input(const ptr<source_type> &a_source);
private:
	//! hide copy constructor
	data_input(const data_input &copy);
	//! hide assignment operator
	data_input &operator=(const data_input &rhs);
	//! current source
	srp<source_type> source;
};

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
