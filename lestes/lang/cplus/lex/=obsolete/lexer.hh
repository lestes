/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___lexer_hh___included
#define lestes__lang__cplus__lex___lexer_hh___included

#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/file_source.hh>
#include <lestes/lang/cplus/lex/pre_lexer.hh>
#include <lestes/lang/cplus/lex/mapper.hh>
#include <lestes/lang/cplus/lex/basic_buf.hh>
package(lestes);
package(lang);
package(cplus);
package(lex);

/*!
  operations done during lexical analysis
*/
class lexer {
public:
	//! type of character in lex
	typedef char char_type;
	//! type of size in lex
	typedef int size_type;
	//! type of buffer
	typedef class basic_buf<ucn_token,size_type> buffer_type;
	//! destructor
	~lexer(void);
	//! store next characters for YY_INPUT
	lexer::size_type yy_input(char_type *cbuf, size_type max);
	//! return underlying buffer
	srp<buffer_type> buffer_get(void);
	//! return new lexer, initialize with input file and encoding
	static srp<lexer> create(const ptr<file_source> &a_fis, const ptr<mapper> &a_map);
protected:   
	//! initialize with input file and encoding
	lexer(const ptr<file_source> &a_fis, const ptr<mapper> &a_map);
private:
	//! return next character in input
	char_type next_char(void);
	//! hide copy constructor
	lexer(const lexer &copy);
	//! hide assignment operator
	lexer &operator=(const lexer &rhs);
	//! input file
	srp<file_source> fis;
	//! operations prior to lexical analysis
	srp<pre_lexer> plx;
	//! buffer for ucn tokens pending
	srp<buffer_type> buffer;
};

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
