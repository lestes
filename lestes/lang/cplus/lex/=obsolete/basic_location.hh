/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___basic_location_hh___included
#define lestes__lang__cplus__lex___basic_location_hh___included

/*! \file
  Declaration of basic_location class representing token location.
  \author pt
*/

#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/equality.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

// TODO will the null checks be here, or in the instantiations?

/*!
  Represents location of token in file inclusion chain,
  the file itself and the translation unit.
  \param FileInfo  The information about the current file.
  \param Position  The position inside the file.
*/
template <typename FileInfo, typename Position>
class basic_location {
public:
	//! type of file information
	typedef FileInfo file_type;
	//! type of position information
	typedef Position position_type;
	//! finalizes the object
	~basic_location(void);
	//! returns file information
	ptr<FileInfo> file_get(void) const;
	//! returns line in file
	Position line_get(void) const;
	//! returns column on line
	Position column_get(void) const;
	//! returns order in unit
	Position order_get(void) const;   
	//! sets file information
	void file_set(const ptr<FileInfo> &a_file); 
	//! sets line in file
	void line_set(const Position &a_line);
	//! sets column on line
	void column_set(const Position &a_column);
	//! sets order in unit
	void order_set(const Position &a_order);
	//! tests equality
	bool equals(const ptr< basic_location<FileInfo,Position> > &rhs) const;
	//! returns copy of location
	virtual ptr< basic_location<FileInfo,Position> > clone(void);
	//! return new location, initialize with file
	static ptr< basic_location<FileInfo,Position> > create(const ptr<FileInfo> &a_file);
	//! return new location, initialize with file, line and column
	static ptr< basic_location<FileInfo,Position> > create(const ptr<FileInfo> &a_file,
		const Position &a_line,	const Position &a_column);
	//! return new location, initialize with file, line, column and order
	static ptr< basic_location<FileInfo,Position> > create(const ptr<FileInfo> &a_file,
		const Position &a_line,	const Position &a_column, const Position &a_order);
protected:
	//! initialize with file information, line, column and order
	basic_location(const ptr<FileInfo> &a_file, const Position &a_line,
			const Position &a_column, const Position &a_order);
private:
	//! file information structure
	srp<FileInfo> file;
	//! line in file
	Position line;
	//! column inside the line
	Position column;
	//! order in translation unit 
	Position order;
	//! hides copy constructor
	basic_location(const basic_location<FileInfo,Position> &);
	//! hides assigment operator
	basic_location<FileInfo,Position> &operator=(const basic_location<FileInfo,Position> &);
};

/*!
  Creates new object, initializes with file information and position.
  \pre a_file != NULL
  \param a_file  The file information.
  \param a_line  The line in the file.
  \param a_column  The column on the line.
  \param a_order  The order in the translation unit.
*/
template <typename FileInfo, typename Position>
basic_location<FileInfo,Position>::basic_location(const ptr<FileInfo> &a_file,
		const Position &a_line, const Position &a_column, const Position &a_order):
	file((lassert(a_file), a_file)),
	line(a_line),
	column(a_column),
	order(a_order)
{
}

/*!
  Finalizes the object.
*/
template <typename FileInfo, typename Position>
basic_location<FileInfo,Position>::~basic_location(void)
{
}

/*!
  Returns file information.
  \return The file information.
*/
template <typename FileInfo, typename Position>
inline ptr<FileInfo> basic_location<FileInfo,Position>::file_get(void) const
{
	return file;
}

/*!
  Returns position in the file.
  \return The line number.
*/
template <typename FileInfo, typename Position>
inline Position basic_location<FileInfo,Position>::line_get(void) const
{
	return line;
}

/*!
  Returns position on the line.
  \return The column number.
*/
template <typename FileInfo, typename Position>
inline Position basic_location<FileInfo,Position>::column_get(void) const
{
	return column;
}

/*!
  Returns position in the translation unit.
  \return The order in translation unit.
*/
template <typename FileInfo, typename Position>
inline Position basic_location<FileInfo,Position>::order_get(void) const
{
	return order;
}

/*!
  Sets file information.
  \pre a_file != NULL
  \param a_file  The new file information.
*/
template <typename FileInfo, typename Position>
inline void basic_location<FileInfo,Position>::file_set(const ptr<FileInfo> &a_file)
{
	lassert(a_file != NULL);
	file = a_file;
}

/*!
  Sets position in the file.
  \param a_line  The new line number.
*/
template <typename FileInfo, typename Position>
inline void basic_location<FileInfo,Position>::line_set(const Position &a_line)
{
	line = a_line;
}

/*!
  Sets position on the line.
  \param a_column  The new column number.
*/
template <typename FileInfo, typename Position>
inline void basic_location<FileInfo,Position>::column_set(const Position &a_column)
{
	column = a_column;
}

/*!
  Sets position in the translation unit.
  \param  The new position.
*/
template <typename FileInfo, typename Position>
inline void basic_location<FileInfo,Position>::order_set(const Position &a_order)
{
	order = a_order;
}

/*!
  Tests equality to other location.
  \param rhs  The location to compare to.
*/
template <typename FileInfo, typename Position>
bool basic_location<FileInfo,Position>::equals(const ptr < basic_location<FileInfo,Position> > &rhs) const
{
	return 
	  equal(file,rhs->file_get()) &&
	  equal(line,rhs->line_get()) &&
	  equal(column,rhs->column_get()) &&
	  equal(order,rhs->order_get());
}

/*!
  Returns copy of this location.
  \return  New location with values equal to this.
*/
template <typename FileInfo, typename Position>
ptr< basic_location<FileInfo,Position> > basic_location<FileInfo,Position>::clone(void)
{
	return ptr< basic_location<FileInfo,Position> >(
			new basic_location<FileInfo,Position>(file,line,column,order));
}

/*!
  Returns new instance, initializes with file information.
  \pre a_file != NULL
  \param a_file  The file information.
*/
template <typename FileInfo, typename Position>
ptr< basic_location<FileInfo,Position> > basic_location<FileInfo,Position>::create(const ptr<FileInfo> &a_file)
{
	return ptr< basic_location<FileInfo,Position> >(
			new basic_location<FileInfo,Position>(a_file,position_type(),position_type(),position_type()));
}

/*!
  Returns new instance, initializes with file information and position.
  \param a_file  The file information.
  \param a_line  The line in the file.
  \param a_column  The column on the line.
*/
template <typename FileInfo, typename Position>
ptr< basic_location<FileInfo,Position> > basic_location<FileInfo,Position>::create(
		const ptr<FileInfo> &a_file, const Position &a_line, const Position &a_column)
{
	return ptr< basic_location<FileInfo,Position> >(
			new basic_location<FileInfo,Position>(a_file,a_line,a_column,position_type()));
}

/*!
  Returns new instance, initializes with file information, position and order.
  \param a_file  The file information.
  \param a_line  The line in the file.
  \param a_column  The column on the line.
  \param a_order  The order in the translation unit.
*/
template <typename FileInfo, typename Position>
ptr< basic_location<FileInfo,Position> > basic_location<FileInfo,Position>::create(
		const ptr<FileInfo> &a_file, const Position &a_line, const Position &a_column, const Position &a_order)
{
	return ptr< basic_location<FileInfo,Position> >(
			new basic_location<FileInfo,Position>(a_file,a_line,a_column,a_order));
}

/*!
  Tests equality of two tokens.
  \param first  The first token to compare.
  \param second  The second token to compare.
*/
template <typename FileInfo, typename Position>
bool equal(const ptr< basic_location<FileInfo,Position> > &first,
		const ptr< basic_location<FileInfo,Position> > &second)
{
  return first->equals(second);
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
