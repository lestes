/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___source_file_hh___included
#define lestes__lang__cplus__lex___source_file_hh___included

/*! \file
  Declaration of source_file class representing included file.
  \author pt
*/

#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/lexer.hh>
#include <lestes/lang/cplus/lex/file_source.hh>
#include <lestes/lang/cplus/lex/mapper.hh>
#include <lestes/lang/cplus/lex/mapper_7bit.hh>
#include <lestes/lang/cplus/lex/pp_token.hh>
#include <string>

package(lestes);
package(lang);
package(cplus);
package(lex);

/*!
  Represents flexer state of included or basic part of a translation unit.
*/
class source_file {
public:
	//! finalizes object
	~source_file(void);
	/* TODO do not implement ??? should someone need these?
	//! return name of the file
	string get_name(void);
	//! return encoding name
	string get_encoding(void);
	*/
	//! returns lexer
	ptr<lexer> lexer_get(void);
	//! returns new instance
	static ptr<source_file> create(const ::std::string &a_name,
			const ::std::string &a_encoding);
protected:   
	//! initialize with file name, chosen encoding and flex buffer state
	source_file(const ::std::string &a_name, const ::std::string &a_encoding, yy_buffer_type a_yy_buffer);
private:
	//! hides copy constructor
	source_file(const source_file &);
	//! hides assignment operator
	source_file &operator=(const source_file &);
	//! name of source file (empty for stdin)
	::std::string name;   
	//! name of used character encoding
	::std::string encoding;
	//! encoding mapper
	srp<mapper> map;
	//! data source
	srp<file_source> fs;
	//! lexer
	srp<lexer> lex;
};

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */

