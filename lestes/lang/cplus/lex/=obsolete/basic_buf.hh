/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___basic_buf_hh___included
#define lestes__lang__cplus__lex___basic_buf_hh___included

#include <lestes/common.hh>
#include <deque>
#include <vector>

package(lestes);
package(lang);
package(cplus);
package(lex);

/*!
  representation of flexible buffer holding smart pointers to items of desired type
  which are added mostly by one at the end and which are afterwards
  removed from the beginning in longer runs
*/
template <typename Item, typename Len>
class basic_buf {
public:
	//! type of item to store
	typedef Item item_type;
	//! type of length 
	typedef Len len_type;
	//! type of returned value vector
	// TODO make this collectible!
	typedef ::std::vector< srp<Item> > vector_type;
	//! destructor
	~basic_buf(void);
	//! add single item to the end of buffer
	void add(const ptr<Item> &item);
	//! throw away len items from the beginning of buffer
	void advance(len_type len);
	//! return reference to the next item 
	srp<Item> peek(void);
	// TODO the return type will be different
	//! remove len items from the beginning and return them in a new vector
	vector_type *remove(len_type len);
	//! return new buffer
	static srp< basic_buf<Item,Len> > create(void);
protected:
	//! initialize with empty buffer
	basic_buf(void);
private:
	//! type of buffer to hold stored data
	typedef ::std::deque< srp<Item> > buffer_type;
	//! buffer to hold stored data
	buffer_type buffer;
	//! hide copy constructor
	basic_buf(const basic_buf<Item,Len> &);
	//! hide assignment operator
	basic_buf<Item,Len> &operator=(const basic_buf<Item,Len> &);
};

template <typename Item, typename Len>
basic_buf<Item,Len>::basic_buf(void):
	buffer()
{
}

template <typename Item, typename Len>
basic_buf<Item,Len>::~basic_buf(void)
{
}

template <typename Item, typename Len>
void basic_buf<Item,Len>::add(const ptr<Item> &item)
{
	buffer.push_back(item);
}

/* FIXME check for not popping more than inside
	although the use via flex would be safe
	ditto for remove family
*/
template <typename Item, typename Len>
void basic_buf<Item,Len>::advance(len_type len)
{
	while (len-- > 0)
		buffer.pop_front();   
}

template <typename Item, typename Len>
srp<Item> basic_buf<Item,Len>::peek(void)
{
	return buffer.front();
}

/* FIXME check for not popping more than inside
	although the use via flex would be safe
*/
template <typename Item, typename Len>
typename basic_buf<Item,Len>::vector_type *basic_buf<Item,Len>::remove(len_type len)
{
	typename buffer_type::iterator dit = buffer.begin() + len;
	vector_type *vec = new vector_type(buffer.begin(),dit);
	buffer.erase(buffer.begin(),dit);

	return vec;
}

template <typename Item, typename Len>
srp< basic_buf<Item,Len> > basic_buf<Item,Len>::create(void)
{
	return srp< basic_buf<Item,Len> >(new basic_buf());
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
