/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/testing.hh>
#include <lestes/lang/cplus/lex/basic_location.hh>
#include <iostream>

using namespace ::lestes::lang::cplus::lex;
using namespace ::std;

class file_info {
public:
	~file_info(void) {
	}
	char name_get(void) {
		return name;
	}
	bool equals(const ptr<file_info> &rhs) {
		return name == rhs->name_get();
	}
	static srp<file_info> create(char a_name) {
		return srp<file_info>(new file_info(a_name));
	}
protected:
	file_info(char a_name): name(a_name) {
	}
private:
	file_info(const file_info &);
	file_info& operator=(const file_info &);
	char name;
};

bool equal(const ptr<file_info> &first, const ptr<file_info> &second)
{
  return first->equals(second);
}

typedef basic_location<file_info,int> test_location;

int main(void)
{
	ptr<test_location> a = test_location::create(file_info::create('n'));

	holds(equal(a,a));
	holds(equal(a->file_get(),file_info::create('n')));
	holds(equal(a->line_get(),0));
	holds(equal(a->column_get(),0));
	holds(equal(a->order_get(),0));

	ptr<file_info> fi = file_info::create('x');

	a->file_set(fi);
	a->line_set(12);
	a->column_set(5);
	a->order_set(9);

	holds(equal(a,a));
	holds(equal(a->file_get(),fi));
	holds(equal(a->line_get(),12));
	holds(equal(a->column_get(),5));
	holds(equal(a->order_get(),9));
	
	ptr<file_info> gi = file_info::create('y');
	ptr<test_location> b = test_location::create(gi,10,3,4);
	ptr<test_location> c = b->clone();

	holds(equal(b,c));
	holds(equal(c,b));
	holds(equal(b->file_get(),c->file_get()));
	holds(equal(b->line_get(),c->line_get()));
	holds(equal(b->column_get(),c->column_get()));
	holds(equal(b->order_get(),c->order_get()));

	ptr<test_location> d;

	d = a;

	holds(equal(d,a));
	holds(equal(a,d));
	holds(equal(d->file_get(),fi));
	holds(equal(d->line_get(),12));
	holds(equal(d->column_get(),5));
	holds(equal(d->order_get(),9));
	
	cerr << "testing basic_location OK\n";
	//cerr << "FIXME !!! testing basic_location\n";
	return 0;
}
/* vim: set ft=lestes : */
