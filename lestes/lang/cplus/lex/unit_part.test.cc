/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  Unit test for class unit_part.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/std/file_info.hh>
#include <lestes/std/source_location.hh>
#include <lestes/lang/cplus/lex/unit_part.hh>
#include <lestes/lang/cplus/lex/pp_token.hh>
#include <lestes/lang/cplus/lex/string_source.hh>
#include <lestes/lang/cplus/lex/encoder_host.hh>
#include <lestes/lang/cplus/lex/macro_storage.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

using namespace ::std;

void unit_part_test(void)
{
	char inp[] = "+\n";
	pp_token_type out[] = { pp_token::TOK_PLUS, pp_token::TOK_LINE_END, pp_token::TOK_FILE_END };
	ptr<file_info> fi = file_info::create("abcd",NULL);
	ptr<unit_part> up =
		unit_part::create(fi,string_source::create(inp),encoder_host::create(),macro_storage::create());
	ptr<pp_token> tok;
	
	ulint i = 0;
	do {
		
		tok = up->read();
#if 0
		::std::cerr << " test " << i << " : " <<
			tok->name_get() << " " <<
			tok->type_get() << " ?== " << (int)out[i] << ""
			<< ::std::endl;
#if 0         
			(int)tok->value_get() << " ?== " << (int)ouv[i] << " offset " <<
			(int)tok->location_get()->offset_get() << " ?== " << loc[i]->offset_get() << " line " <<
			(int)tok->location_get()->line_get() << " ?== " << loc[i]->line_get() << " column " <<
			(int)tok->location_get()->column_get() << " ?== " << loc[i]->column_get() << " order " <<
			(int)tok->location_get()->order_get() << " ?== " << loc[i]->order_get() << ::std::endl;
#endif
#endif
		lassert(is_equal(tok->type_get(),out[i]));

		if (tok->type_get() == pp_token::TOK_FILE_END) break;
		i++;
	} while (true);
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

int main(void)
{
	::lestes::lang::cplus::lex::unit_part_test();
	return 0;
}
/* vim: set ft=lestes : */

