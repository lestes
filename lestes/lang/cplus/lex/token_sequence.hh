/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___token_sequence_hh___included
#define lestes__lang__cplus__lex___token_sequence_hh___included

/*! \file
	Declaration of token_sequence class representing sequence of tokens.
	\author pt
*/

#include <lestes/common.hh>
#include <lestes/std/list.hh>
#include <lestes/lang/cplus/lex/token_input.hh>
#include <iosfwd>

package(lestes);

package(std);
// forward declarations to avoid cycle
class source_location;
end_package(std);

package(lang);
package(cplus);
package(lex);

// forward declaration to break cycle
class pp_token;
class macro;
class macro_storage;
class taboo_macros;

/*!
  Represents sequence of tokens, with access at both ends.
*/
class token_sequence: public token_input {
private:
	//! The type of sequence container.
	typedef ::lestes::std::list< srp<pp_token> > sequence_type;
public:
	/*!
	  \brief The type for size of the sequence.
	  \author TMA
	 */
	typedef sequence_type::size_type size_type;
	//! Reads first token.
	ptr<pp_token> read(void);
	//! Returns front token.
	ptr<pp_token> peek_front(void);
	//! Returns back token.
	ptr<pp_token> peek_back(void);
	//! Reads front token, squeezing whitespace.
	ptr<pp_token> read_front(void);
	//! Reads front token, skipping front whitespace, but not newline.
	ptr<pp_token> read_front_skip_ws(void);
	//! Reads back token, squeezing whitespace.
	ptr<pp_token> read_back(void);
	//! Reads back token, skipping back whitespace, but not newline.
	ptr<pp_token> read_back_skip_ws(void);
	//! skips back whitespace
	bool skip_front_ws(void);
	//! skips back whitespace
	bool skip_back_ws(void);
	//! Adds token to back.
	void add_back(const ptr<pp_token> &t);
	//! Prepends another sequence.
	void prepend(const ptr<token_sequence> &ts);
	//! Appends another sequence.
	void append(const ptr<token_sequence> &inserted);
	//! Extends taboo set for tokens in sequence.
	void taboo_extend(
	const ptr<taboo_macros> &tm, const ptr<macro_storage> &macros);
	//! completely expands all macros within sequence
	ptr<token_sequence> expand_all(const ptr<macro_storage> &macros);
	//! Clones the sequence.
	ptr<token_sequence> clone(void) const;
	//! clones the sequence with new location.
	ptr<token_sequence> clone(const ptr<source_location> &a_location) const;
	//! Returns length of the sequence.
	size_type length(void) const;
	//! Tests equality.
	bool equals(const ptr<token_sequence> &other) const;
	//! Tests congruence.
	bool congruent(const ptr<token_sequence> &other) const;
	//! TODO prints debug dump
	void debug_print(::std::ostream &o) const;
	//! Returns empty sequence.
	static ptr<token_sequence> create(void);
protected:   
	//! Creates empty sequence.
	token_sequence(void);
	//! Marks the object.
	virtual void gc_mark(void);
private:
	//! For direct acces to sequence in prepend().
	friend class token_stream;
	//! The sequence container.
	srp<sequence_type> sequence;
};

//! prints debug dump of token sequence
::std::ostream &operator<<(::std::ostream &o, const ptr<token_sequence> &ts);

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);
#endif
/* vim: set ft=lestes : */
