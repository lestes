/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___macro_head_hh___included
#define lestes__lang__cplus__lex___macro_head_hh___included

/*! \file
  \brief Macro parameter list.
  
  Definition of macro_head class representing indexed list of macro parameters.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/token_value.hh>
#include <lestes/std/map.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

// forward declaration to avoid cycle
class token_sequence;
class token_input;

/*!
  \brief Macro parameter list.
  
  Representation of indexed macro parameter list.
  The parameter names are added at the end and can be searched for.
*/
class macro_head: public ::lestes::std::object {
public:
	//! Type of internal state.
	typedef enum { BEGIN, PARSED, DEAD } state_type;
	//! Parses macro parameter list.
	bool parse(const ptr<token_input> &input);
	//! Returns index of parameter.
	ulint index_of(const ptr<token_value> &a_value) const;
	//! Returns list length.
	ulint length(void) const;
	//! Returns internal state.
	state_type state_get(void) const;
	//! Tests equality.
	bool equals(const ptr<macro_head> &other) const;
	//! Returns empty parameter list.
	static ptr<macro_head> create(void);
protected:
	//! Creates empty parameter list.
	macro_head(void);
	//! Marks the object.
	virtual void gc_mark(void);
private:
	//! Type of map for storing the parameters.
	typedef ::lestes::std::map< srp<token_value>,ulint,token_value::compare_less > pars_type;
	//! Adds parameter to list.
	bool add_param(const ptr<token_value> &a_value);
	//! Internal state of the object.
	state_type state;
	//! Map of parameters to indexes.
	srp<pars_type> pars;
};

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
