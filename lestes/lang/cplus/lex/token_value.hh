/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___token_value_hh___included
#define lestes__lang__cplus__lex___token_value_hh___included

/*! \file
  \brief Token value.
  
  Declaration of token_value class representing values of tokens.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/std/map.hh>
#include <lestes/std/ucn_string.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

/*!
  \brief Token value.
  
  Represents token value, wrapping ucn_string. The object is constant and shared.
*/
class token_value: public ::lestes::std::object {
public:
	//! Type of content.
	typedef ucn_string content_type;
	//! Returns object initialized with content.
	static ptr<token_value> create(const content_type &a_content);
	//! Returns content.
	ucn_string content_get(void) const;
	//! Tests equality.
	bool equals(const ptr<token_value> &other) const;
	//! Tests inequality.
	bool less_than(const ptr<token_value> &other) const;
	//! returns hash of the content
	ulint hash(void) const;
	//! Less than comparator for token_value.
	struct compare_less {
		//! Compares two token values.
		bool operator()(const ptr<token_value> &left, const ptr<token_value> &right) const;
	};   
private:
	//! Creates object with content.
	token_value(const content_type &a_content);
	//! The representation of the value.
	content_type content;
	//! Less than comparator. 
	struct compare_content {
		bool operator()(const content_type &left, const content_type &right) const;
	};
	//! Type of structure for sharing instances.
	typedef ::lestes::std::map<content_type,srp<token_value>,compare_content> shared_type;
	//! Structure for sharing instances.
	// TODO pt make weak, or mark only the object itself and not its entries
	static ptr<shared_type> shared;
};

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
