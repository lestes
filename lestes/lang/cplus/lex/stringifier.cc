/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Token stringifier.
  
  Definition of stringifier class performing token stringification.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/std/source_location.hh>
#include <lestes/std/ucn_string.hh>
#include <lestes/lang/cplus/lex/stringifier.hh>
#include <lestes/lang/cplus/lex/pp_token.hh>
#include <lestes/lang/cplus/lex/token_input.hh>
#include <lestes/lang/cplus/lex/token_value.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

/*!
  Creates the only object of the class.
*/
stringifier::stringifier(void)
{
}

/*!
  Attempts to stringify tokens. Creates new token of type pp_token::TOK_STRING_LIT,
  with location taken from the first nonblank input token and value representing joined
  stringification of all input tokens, with character and string literals escaped.
  Runs of blank tokens inside the sequence are represented by single space, 
  surrounding blanks are discarded.
  \pre input != NULL
  \todo pt necessary? pre input does not contain pp_token::TOK_LINE_END tokens.
  \param input The tokens to stringify.
  \return  New token containing the stringification, no correctness checks are done.
*/
ptr<pp_token> stringifier::process(const ptr<token_input> &input)
{
	ucn_string str;
	ptr<pp_token> tok = input->read_front();
	// skip the leading blank
	if (tok->type_get() == pp_token::TOK_BLANK) tok = input->read_front();
	ptr<source_location> loc = tok->location_get();
	
	if (tok->type_get() != pp_token::TOK_TERMINATOR) {
		ptr<pp_token> last = tok;
		tok = input->read_front();
		
		while (tok->type_get() != pp_token::TOK_TERMINATOR) {
			str += escape_spelling(last);
			last = tok;
			tok = input->read_front();
		}

		// add the last token iff nonblank
		if (last->type_get() != pp_token::TOK_BLANK) str += escape_spelling(last);
	}
	
	// TODO ??? set error flag, because the literal can be broken
	return pp_token::create(loc,pp_token::TOK_STRING_LIT,token_value::create(str));
}

/*!
  Adds guard backslash characters before double quotes and backslash characters into the spelling
  of string, character and other literals to avoid interpreting them as escape sequences.
  Spelling of tokens with other types are returned intact.
  \pre tok != NULL
  \param tok The token to process.
  \return The spelling  string with special characters escaped.
*/
ucn_string stringifier::escape_spelling(const ptr<pp_token> &tok)
{
	lassert(tok);

	switch (tok->type_get()) {
		case pp_token::TOK_STRING_LIT:
		case pp_token::TOK_WSTRING_LIT:
		case pp_token::TOK_CHAR_LIT:
		case pp_token::TOK_WCHAR_LIT:
		case pp_token::TOK_OTHER:
		case pp_token::TOK_IDENT:
			break;
		default:
			return tok->spelling_get();
	}

	ucn_string str(tok->spelling_get());
	ucn_string::size_type len = str.length();
	ucn u;
	ucn_string work;

	for (ucn_string::size_type i = 0; i < len; i++) {
		u = str[i];
		
		if (character::is_translated(u)) {
			ulint x = character::extract_value(u);
			work += character::ascii_backslash;
			work += character::ascii_backslash;
			if (x <= 0xffff) {
				work += character::ascii_lower_u;
				work += character::create_xdigit((x >> 12) & 0xf);
				work += character::create_xdigit((x >> 4) & 0xf);
				work += character::create_xdigit((x >> 8) & 0xf);
				work += character::create_xdigit(x & 0xf);
			} else {
				work += character::ascii_upper_u;
				work += character::create_xdigit((x >> 28) & 0xf);
				work += character::create_xdigit((x >> 24) & 0xf);
				work += character::create_xdigit((x >> 20) & 0xf);
				work += character::create_xdigit((x >> 16) & 0xf);
				work += character::create_xdigit((x >> 12) & 0xf);
				work += character::create_xdigit((x >> 8) & 0xf);
				work += character::create_xdigit((x >> 4) & 0xf);
				work += character::create_xdigit(x & 0xf);
			}
		} else {
			if (u == character::ascii_dquote || u == character::ascii_backslash)
				work += character::ascii_backslash;
			work += u;
		}
		
	}
	return work;
}

/*!
  Returns the only instance. Lazy initialized.
  \return The singleton.
*/
ptr<stringifier> stringifier::instance(void)
{
	if (!singleton) {
		singleton = new stringifier();
	}
	return singleton;
}

/*!
  The only instance of the class. Lazy initialized in the instance() method.
*/
ptr<stringifier> stringifier::singleton;

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

/* vim: set ft=lestes : */
