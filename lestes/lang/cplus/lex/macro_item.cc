/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Expansion list itemm.
  
  Definition of macro_item class representing part of macro expansion list.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/equality.hh>
#include <lestes/lang/cplus/lex/macro_item.hh>
#include <lestes/lang/cplus/lex/token_sequence.hh>
#include <lestes/lang/cplus/lex/pp_token.hh>

#include <iostream>

package(lestes);
package(lang);
package(cplus);
package(lex);

/*!
  Returns literal macro item, initializes with a_value.
  \pre a_value != NULL
  \param a_value The initialization value.
  \return  Macro item representing literal.
*/
ptr<macro_item> macro_item::create_literal(const ptr<token_sequence> &a_value)
{
	lassert(a_value);
	// the index and blank is not used
	return new macro_item(LITERAL,0,a_value->clone());
}

/*!
  Returns expansion macro item, initializes with a_index.
  \param a_index  The index of referenced parameter.
  \return  Macro item representing parameter expansion.
*/
ptr<macro_item> macro_item::create_expansion(index_type a_index)
{
	// the value parameter is not used
	return new macro_item(EXPANSION,a_index,NULL);
}

/*!
  Returns copy macro item, initializes with a_index.
  \param a_index  The index of referenced parameter.
  \return  Macro item representing parameter copy without expansion.
*/
ptr<macro_item> macro_item::create_copy(index_type a_index)
{
	// the value and blank is not used
	return new macro_item(COPY,a_index,NULL);
}

/*!
  Returns stringification macro item, initializes with index and blank flag.
  \pre a_value != NULL
  \param a_index  The index of referenced parameter.
  \param a_value The stringification operator with optional space.
  \return  Macro item representing parameter stringification.
*/
ptr<macro_item> macro_item::create_str(index_type a_index, const ptr<token_sequence> &a_value)
{
	lassert(a_value);
	// the value is not used
	return new macro_item(STR,a_index,a_value->clone());
}

/*!
  Returns concatenation macro item, initializes with sequence containing a_token.
  \pre a_value != NULL
  \param a_value  The concatenation operator.
  \return Macro item representing token concatenation
*/
ptr<macro_item> macro_item::create_concat(const ptr<token_sequence> &a_value)
{
	lassert(a_value);

	// the index and blank is not used
	return new macro_item(CONCAT,0,a_value->clone());
}

/*!
  Creates item object, not all parameters are valid for every action type.
  No checks of consistency are done on this level.
  \param a_action  The action represented by the item.
  \param a_index  The index of parameter for expand, nonexpand and stringify actions.
  \param a_value  The value of item for literal and operators.
*/
macro_item::macro_item(action_type a_action, index_type a_index, const ptr<token_sequence> &a_value):
	action(a_action),
	index(a_index),
	value(a_value)
{
}

/*!
  Returns action of the macro item.
  \return The action represented by the macro item.
*/
macro_item::action_type macro_item::action_get(void) const
{
	return action;
}

/*!
  Returns value of stored token sequence literal.
  \pre action_get() != EXPANSION && action_get() != COPY
  \return The represented value.
*/
ptr<token_sequence> macro_item::value_get(void) const
{
	lassert(action_get() != EXPANSION && action_get() != COPY);
	return value->clone();
}

/*!
  Returns index of the macro parameter.
  \pre action_get() == EXPAND || action_get() == COPY || action_get() == STR
  \return The index of macro parameter referenced in action.
*/
macro_item::index_type macro_item::index_get(void) const
{
	lassert(action_get() == EXPANSION || action_get() == COPY || action_get() == STR);
	return index;
}

/*!
  Tests equality to other macro item. Only fields relevant for selected actions are compared.
  \param other  The macro item to compare with.
  \return true  If both actions and their respective parameters are equal.
*/
bool macro_item::equals(const ptr<macro_item> &other) const
{
	if (!other || action_get() != other->action_get()) return false;

	// TODO pt make resembles back equals
	switch (action_get()) {
		case LITERAL:
			return value->congruent(other->value);
		case EXPANSION:
		case COPY:
			return is_equal(index_get(),other->index_get());
		case STR:
			return is_equal(index_get(),other->index_get()) && value->congruent(other->value);
		case CONCAT:
			return value->congruent(other->value);
		default:
			lassert2(false,"You should never get here");
	}
	return false;
}

/*!
  Marks the object.
*/
void macro_item::gc_mark(void)
{
	value.gc_mark();
	object::gc_mark();
}

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);
/* vim: set ft=lestes : */
