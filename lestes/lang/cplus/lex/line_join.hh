/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___line_join_hh___included
#define lestes__lang__cplus__lex___line_join_hh___included

/*! \file
  \brief Line joining filter.

  Declaration of line_join class performing joining of lines.
  \author pt
*/
#include <lestes/common.hh> 
#include <lestes/lang/cplus/lex/ucn_filter.hh> 
package(lestes);
package(lang);
package(cplus);
package(lex);

/*!
  \brief Line joining filter.
  
  Performs joining of lines ending with backslash.
*/
class line_join: public ucn_filter {
public:
	//! Reads next token.
	ptr<ucn_token> read(void);
	//! Returns new instance.
	static ptr<line_join> create(void);
protected:
	//! Creates the object.
	line_join(void);
	//! Marks the object.
	virtual void gc_mark(void);
private:
	//! States of the processor.
	typedef enum {
		//! No tokens in buffer.
		START = 0,
		//! Backslash in buffer.
		BACKSLASH = 1,
		//! Ordinary character in buffer.
		SAVED = 2,
		//! After discarding the pair.
		AFTER = 3
	} state_type;
	//! Current state of the processor.
	state_type state;
	//! Buffered token.
	srp<ucn_token> saved;
	//! Hides copy constructor.
	line_join(const line_join &);
	//! Hides assignment operator.
	line_join &operator=(const line_join &);
};

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
