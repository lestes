/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__lang__cplus__lex___encoder_factory_hh___included
#define lestes__lang__cplus__lex___encoder_factory_hh___included

/*! \file
  \brief Factory for encoders.

  Declaration of encoder_factory class managing encoder implementations.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/encoder.hh>
#include <lestes/std/map.hh>
#include <lestes/std/objectize_macros.hh>

package(lestes);
package(lang);
package(cplus);
package(lex);

/*!
  \brief Factory for encoders.
  
  Manages all encoder implementations.
*/
class encoder_factory: public ::lestes::std::object {
public:
	//! Adapts the return value of create methods.
	template <typename Encoder>
	static ptr<encoder> encoder_create_adaptor(void);
	//! Type of common encoder create methods.
	typedef ptr<encoder> (* encoder_create_type)(void);
	//! Inserts new encoder into the database.
	void insert(const lstring &a_name, encoder_create_type a_create);
	//! Removes the encoder from the database.
	void remove(const lstring &a_name);
	//! Returns encoder for name.
	ptr<encoder> summon(const lstring &a_name) const;
	//! Returns the only instance.
	static ptr<encoder_factory> instance(void);
protected:
	//! Creates the object
	encoder_factory(void);
	//! Marks the object.
	virtual void gc_mark(void);
private:
	//! Type of storage for create functions.
	typedef ::lestes::std::map< lstring, encoder_create_type > storage_type;
	/* TODO pt change to hash map
	typedef hash_map< ::std::string, encoder_creator, string_hash_function > storage_type;
	 */
	//! The storage for encoders.
	srp<storage_type> storage;
	//! The only object of the class.
	static ptr<encoder_factory> singleton;
	//! Hides copy constructor.
	encoder_factory(const encoder_factory &);
	//! Hides assignment operator.
	encoder_factory &operator=(const encoder_factory &);
};

/*!
  Adapts return value of concrete encoder create() method to return base class.
  \param Encoder  The concrete encoder to adapt.
  \param create  The creator of the concrete encoder.
*/
template <typename T>
ptr<encoder> encoder_factory::encoder_create_adaptor(void)
{
	return ptr<encoder>(T::create());
}

end_package(lex);
end_package(cplus);
end_package(lang);

package(std);
/*!
  Makes the encoder_create_type nondumpable.
*/
specialize_objectize_nodump(::lestes::lang::cplus::lex::encoder_factory::encoder_create_type);

end_package(std);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
