/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Taboo macros.
  
  Definition of taboo_macros class representing set of disallowed macros.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/lang/cplus/lex/taboo_macros.hh>
#include <lestes/lang/cplus/lex/macro.hh>
#include <lestes/lang/cplus/lex/token_value.hh>

#include <iostream>

package(lestes);
package(lang);
package(cplus);
package(lex);

using namespace ::std;

/*!
  Creates new empty set.
  \post taboo->length() == 0
  \post shared->length() == 0
*/
taboo_macros::taboo_macros(void):
	taboo(taboo_type::create()),
	shared(shared_type::create())
{
}

/*!
  Creates copy of set, adding a new macro.
  \pre source != NULL
  \post taboo->length() == source->taboo->length() + (source->contains(a_macro) ? 0 : 1)
  \post shared->length() == 0
  \param source  The source object to copy.
  \param a_macro  The macro to insert into new object's set.
*/
taboo_macros::taboo_macros(const ptr<taboo_macros> &source, const ptr<macro> &a_macro):
	taboo(taboo_type::create()),
	shared(shared_type::create())
{
	lassert(source);
	lassert(a_macro);
	
	// copy the content
	// TODO pt make more elegant
	// ??? bad bad coder copy(source->taboo->begin(),source->taboo->end(),inserter(*taboo));
	
	for (taboo_type::iterator it = source->taboo->begin(),
			end = source->taboo->end(); it != end; ++it) {
		taboo->insert(*it);
	}

	// add the new macro
	taboo->insert(a_macro);
	/* TODO pt ::std::cerr << "inserted taboo " << a_macro->name_get()->content_get() << " :";
	
	for (taboo_type::iterator it = taboo->begin(),
			end = taboo->end(); it != end; ++it) {
		::std::cerr << ' ' << (*it)->name_get()->content_get();
	}

	::std::cerr << (taboo->find(a_macro) != taboo->end() ? "found" : "BOOM") << '\n';
	*/
}

/*!
  Returns empty taboo set.
  \post taboo->length() == 0
  \return Instance containing no macros.
*/
ptr<taboo_macros> taboo_macros::create(void)
{
	if (!empty_instance) {
		empty_instance = new taboo_macros();
	}
	return empty_instance;
}

/*!
  Tests whether the taboo set contains a macro.
  \param a_macro  The macro to test.
  \return true  If the macro is contained in the set.
*/

bool taboo_macros::contains(const ptr<macro> &a_macro) const
{
	return taboo->find(a_macro) != taboo->end();
}

/*!
  Returns new taboo set with added macro.
  \pre a_macro != NULL
  \param a_macro  The macro to be added to the new taboo.
  \return  New object with set extended by a_macro.
*/
ptr<taboo_macros> taboo_macros::extend(const ptr<macro> &a_macro) const
{
	lassert(a_macro);
	
	// macro is already in this instance
	if (contains(a_macro)) return ptr<taboo_macros>(const_cast<taboo_macros *>(this));
	
	// lookup the cache of extensions
	shared_type::iterator it = shared->find(a_macro);
	// return shared instance
	if (it != shared->end()) return (*it).second;
	
	// call special "copy and extend" constructor
	ptr<taboo_macros> nju = ptr<taboo_macros>(
			new taboo_macros(ptr<taboo_macros>(const_cast<taboo_macros *>(this)),a_macro));
	
	// record into cache
	shared->insert(make_pair(a_macro,nju));
			
	return nju;
}

/*!
  Marks the object.
*/
void taboo_macros::gc_mark(void)
{
	taboo.gc_mark();
	shared.gc_mark();
	::lestes::std::object::gc_mark();
}

ucn_string taboo_macros::names_get(void) const
{
	ucn_string u;
	
	u += character::create_from_host('{');

	for (taboo_type::iterator it = taboo->begin(),
			end = taboo->end(); it != end; ++it) {
		u += character::create_from_host(' ');
		u += (*it)->name_get()->content_get();
	}
	
	u += character::create_from_host('}');
	return u;
}
	
/*!
  The shared empty instance of taboo macros.
  Used for fast initialization of tokens.
*/
ptr<taboo_macros> taboo_macros::empty_instance;

end_package(lex);
end_package(cplus);
end_package(lang);
end_package(lestes);
/* vim: set ft=lestes : */
