<?xml version="1.0" encoding="UTF-8"?>
<!--
	The lestes compiler suite
	Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
	Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
	Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
	Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
	Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
	Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
	Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; version 2 of the License.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	See the full text of the GNU General Public License version 2, and
	the limitations in the file doc/LICENSE.

	By accepting the license the licensee waives any and all claims
	against the copyright holder(s) related in whole or in part to the
	work, its use, and/or the inability to use it.
-->
<lmd xmlns="http://lestes.jikos.cz/schemas/lmd" xmlns:h="http://www.w3.org/TR/REC-html40">
	<file-name>macro_head</file-name>

	<dox file="both">
		<bri>
			Messages for macro parameters.
		</bri>
		<det>
			Definition of warnings and errors issued for macro parameter list.
		</det>
	</dox>

	<packages>
		<p>lestes</p>
		<p>lang</p>
		<p>cplus</p>
		<p>lex</p>
	</packages>
	
	<imports>
	</imports>

	<implementation-imports>
	</implementation-imports>

	<error name="expected_macro_parameter">
		<dox>
			<bri>Error message for macro parameter list.</bri>
			<det>
				Issued when a non-identifier token is found in
				the place of a macro parameter name.
			</det>
		</dox>
		<text>expected macro parameter name</text>
	</error>

	<error name="expected_right_par">
		<dox>
			<bri>Error message for macro parameter list.</bri>
			<det>
				Issued when the macro parameter list is
				not terminated by `)' token.
			</det>
		</dox>
		<text>expected `)' at the end of macro parameter list</text>
	</error>

	<error name="expected_comma">
		<dox>
			<bri>Error message for macro parameter list.</bri>
			<det>
				Issued when two macro parameters are not comma separated.
			</det>
		</dox>
		<text>expected `,' separator between macro parameters</text> 
	</error>

	<error name="expected_comma_right_par">
		<dox>
			<bri>Error message for macro parameter list.</bri>
			<det>
				Issued when an invalid token is encountered
				after macro parameter. The token is passed as
				an argument of the message.
			</det>
		</dox>
		<text>`%0' is invalid after macro parameter name, expected `,' or `)'</text>
		<param type="ucn_string" />
	</error>

	<error name="duplicate_macro_parameter">
		<dox>
			<bri>Error message for macro parameter list.</bri>
			<det>
				Issued when the same identifier is used
				for two macro parameters. The parameter name
				is passed as an argument for the message.
			</det>
		</dox>
		<text>duplicate macro parameter `%0'</text>
		<param type="ucn_string" />
	</error>

</lmd>
