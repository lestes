/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__std___ucn_string_hh___included
#define lestes__std___ucn_string_hh___included

/*! \file
  \brief String of ucn characters.

  Declaration of standard string template specialized for ucn character type.
*/
#include <lestes/common.hh>
#include <lestes/std/ucn_traits.hh>
#include <string>
#include <iosfwd>

package(lestes);
package(std);

//! Defines templated predecessor of ucn string.
typedef ::std::basic_string<ucn> basic_ucn_string;

/*!
  \brief String of ucn characters.

  Represents string of ucn characters. The string is intended for
  storing internal representation of identifiers and literals.
  It shall not be used for ordinary messages or texts.
  All data is implicitly stored in superset of ASCII and has to be
  converted to host character set before displaying
*/
class ucn_string : public basic_ucn_string {
public:
	//! Creates empty string.
	ucn_string(void);
	//! Creates ucn string, initializes with other string.
	ucn_string(const ucn_string &str);
	//! Creates string, initializes with part of string.
	ucn_string(const ucn_string &str, size_type pos, size_type n = npos);
	// TODO pt remove
#if 0
	//! creates ucn string, initializes with part of string and allocator
	inline ucn_string(const value_type *s, size_type n, const allocator_type &a = allocator_type()):
		basic_ucn_string(checked(s),n,a) {}
	//! creates ucn string, initializes with string and allocator
	inline ucn_string(const value_type *s, const allocator_type &a = allocator_type()):
		basic_ucn_string(checked(s),a) {}
#endif
	//! Creates string, fills with character.
	ucn_string(size_type n, value_type c);
	//! Creates ucn string, initializes with iterator.
	template <class InputIterator>
	ucn_string(InputIterator beg, InputIterator end);
	//! Creates string, initializes with host characters.
	ucn_string(const char *data);
	//! Returns escaped host representation.
	lstring to_host_string(void) const;
};

/*!
  Creates empty ucn string.
*/
inline ucn_string::ucn_string(void):
	basic_ucn_string()
{
}

/*!
  Creates ucn string, initializes with another string.
  \param str  The initializer.
*/
inline ucn_string::ucn_string(const ucn_string &str):
	basic_ucn_string(str)
{
}

/*!
  Creates ucn string, initializes with a portion of another string.
  \param str  The initializer.
  \param pos  The starting position in the initializer.
  \param n  The length of the portion.
*/
inline ucn_string::ucn_string(const ucn_string &str, size_type pos, size_type n):
	basic_ucn_string(str,pos,n)
{
}

/*!
  Creates ucn string, fills with characters.
  \param n  The length of the string.
  \param c  The character to fill.
*/
inline ucn_string::ucn_string(size_type n, value_type c):
	basic_ucn_string(n,c,allocator_type())
{
}

/*!
  Creates ucn string, initializes with iterator.
  \param InputIterator  The type of the iterator.
  \param beg  The starting position of the iterator.
  \param end  The ending position of the iterator.
*/
template <class InputIterator>
inline ucn_string::ucn_string(InputIterator beg, InputIterator end):
	basic_ucn_string(beg,end,allocator_type())
{
}

/*!
  Creates ucn string from regular characters ASCIIZ string.
  \param s  The characters to convert.
*/
inline ucn_string::ucn_string(const char *s):
	basic_ucn_string(strlen(s),0xdeadbeef)
{
	for (ucn_string::iterator it = this->begin(), end = this->end(); it != end; ++it, ++s) {
		*it = character::create_from_host(*s);
	}
}

//! Prints ucn string to stream.
::std::ostream& operator<<(::std::ostream &os, const ucn_string &us);

end_package(std);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
