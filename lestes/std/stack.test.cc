/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/common.hh>
#include <lestes/std/stack.hh>
#include <lestes/std/vector.hh>

using namespace ::lestes;
using namespace ::lestes::std;

int main()
{
	ptr < stack < int > > q0 = stack < int > :: create();
	lassert(q0->empty());
	q0->push(1);
	lassert(q0->top() == 1);
	lassert(!q0->empty());
	q0->push(2);
	lassert(q0->top() == 2);
	q0->push(3);
	lassert(q0->top() == 3);
	q0->pop();
	lassert(q0->top() == 2);
	q0->push(4);
	lassert(q0->top() == 4);
	q0->push(5);
	lassert(q0->top() == 5);
	q0->push(6);
	lassert(q0->top() == 6);
	q0->pop();
	lassert(q0->top() == 5);
	q0->pop();
	lassert(q0->top() == 4);
	q0->pop();
	lassert(q0->top() == 2);
	q0->pop();
	lassert(q0->top() == 1);
	q0->pop();
	lassert(q0->empty());

#if vector_is_manhandled_to_be_usable_in_stack
	ptr < stack < int, vector < int > > > q1 = stack < int, vector < int > > :: create();
	lassert(q1->empty());
	q1->push(1);
	lassert(q1->top() == 1);
	lassert(!q1->empty());
	q1->push(2);
	lassert(q1->top() == 2);
	q1->push(3);
	lassert(q1->top() == 3);
	q1->pop();
	lassert(q1->top() == 2);
	q1->push(4);
	lassert(q1->top() == 4);
	q1->push(5);
	lassert(q1->top() == 5);
	q1->push(6);
	lassert(q1->top() == 6);
	q1->pop();
	lassert(q1->top() == 5);
	q1->pop();
	lassert(q1->top() == 4);
	q1->pop();
	lassert(q1->top() == 2);
	q1->pop();
	lassert(q1->top() == 1);
	q1->pop();
	lassert(q1->empty());
#else
	ptr < object > q1 = stack < srp < object > > :: create();
#endif

	ptr < stack < srp < object > > > q2 = stack < srp < object >  > :: create();
	lassert(q2->empty());
	q2->push(q0);
	lassert(q2->top() == q0);
	lassert(!q2->empty());
	q2->push(q1);
	lassert(q2->top() != q0);
	lassert(q2->top() == q1);
	lassert(!q2->empty());
	q2->pop();
	lassert(q2->top() == q0);
	lassert(!q2->empty());
	q2->push(q2);
	lassert(q2->top() == q2);
	q2->pop();
	lassert(q2->top() == q0);
	q2->pop();
	lassert(q2->empty());

	
}
