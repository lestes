/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__std___file_info_hh___included
#define lestes__std___file_info_hh___included

/*! \file
  \brief Source file information.
  
  Declaration of file_info class representing information about source file.
*/
#include <lestes/std/object.hh>

package(lestes);
package(std);

// forward declaration to break cycle
class source_location;

/*!
  \brief Source file information.
  
  Represents information about file inside the inclusion chain.
  The first source has NULL origin.
*/  
class file_info: public object {
public:
	//! Type of file name.
	typedef lstring name_type;   
	//! Returns file name.
	name_type name_get(void) const;
	//! Returns inclusion origin.
	ptr<source_location> origin_get(void) const;
	//! Tests equality.
	bool equals(const ptr<file_info> &rhs) const;
	//! Returns file info initialized with file name and origin.
	static ptr<file_info> create(const name_type &a_name,
				const ptr<source_location> &a_origin);
protected:   
	//! Creates the object, initializes with file name and origin.
	file_info(const lstring &a_name, const ptr<source_location> &a_origin);
	//! Marking routine.
	void gc_mark();
private:
	//! Name of the file.
	lstring name;
	//! Origin of inclusion.
	srp<source_location> origin;
	//! Hides copy constructor.
	file_info(const file_info &);
	//! Hides assingment operator.
	file_info &operator=(const file_info &);
};

end_package(std);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
