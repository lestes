/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/std/collection_refl.hh>
#include <lestes/std/reflect.hh>
#include <lestes/std/pair.hh>
#include <lestes/std/list.hh>

/*! \file
  Defines all the static [cached] reflections for collections together with a
  method that initializes them.

  \author Rudo
*/

package(lestes);
package(std);

typedef class_reflection::field_metadata	field_metadata;
typedef class_reflection::field_metadata_list	field_metadata_list;

ptr<object::reflection_list> collection_refl::list_simple;

void collection_refl::list_simple_init( ptr<object::reflection_list> inherited )
{
	ptr<field_metadata_list> l = field_metadata_list::create();
	l->push_back( field_metadata::create( "item", "simple" ) );

	// create a copy of base class's list and append our own item
	ptr<object::reflection_list> result = object::reflection_list::create(inherited);
	result->push_back( class_reflection::create( "list_of_simple", l ) );

	collection_refl::list_simple = result;
}

ptr<object::reflection_list> collection_refl::list_srp;

void collection_refl::list_srp_init( ptr<object::reflection_list> inherited )
{
	ptr<field_metadata_list> l = field_metadata_list::create();
	l->push_back( field_metadata::create( "item", "srp" ) );

	// create a copy of base class's list and append our own item
	ptr<object::reflection_list> result = object::reflection_list::create(inherited);
	result->push_back( class_reflection::create( "list_of_srp", l ) );

	collection_refl::list_srp = result;
}

ptr<object::reflection_list> collection_refl::vector_simple;

void collection_refl::vector_simple_init( ptr<object::reflection_list> inherited )
{
	ptr<field_metadata_list> l = field_metadata_list::create();
	l->push_back( field_metadata::create( "item", "simple" ) );

	// create a copy of base class's list and append our own item
	ptr<object::reflection_list> result = object::reflection_list::create(inherited);
	result->push_back( class_reflection::create( "vector_of_simple", l ) );

	collection_refl::vector_simple = result;
}

ptr<object::reflection_list> collection_refl::vector_srp;

void collection_refl::vector_srp_init( ptr<object::reflection_list> inherited )
{
	ptr<field_metadata_list> l = field_metadata_list::create();
	l->push_back( field_metadata::create( "item", "srp" ) );

	// create a copy of base class's list and append our own item
	ptr<object::reflection_list> result = object::reflection_list::create(inherited);
	result->push_back( class_reflection::create( "vector_of_srp", l ) );

	collection_refl::vector_srp = result;
}

ptr<object::reflection_list> collection_refl::set_simple;

void collection_refl::set_simple_init( ptr<object::reflection_list> inherited )
{
	ptr<field_metadata_list> l = field_metadata_list::create();
	l->push_back( field_metadata::create( "item", "simple" ) );

	// create a copy of base class's list and append our own item
	ptr<object::reflection_list> result = object::reflection_list::create(inherited);
	result->push_back( class_reflection::create( "set_of_simple", l ) );

	collection_refl::set_simple = result;
}

ptr<object::reflection_list> collection_refl::set_srp;

void collection_refl::set_srp_init( ptr<object::reflection_list> inherited )
{
	ptr<field_metadata_list> l = field_metadata_list::create();
	l->push_back( field_metadata::create( "item", "srp" ) );

	// create a copy of base class's list and append our own item
	ptr<object::reflection_list> result = object::reflection_list::create(inherited);
	result->push_back( class_reflection::create( "set_of_srp", l ) );

	collection_refl::set_srp = result;
}

ptr<object::reflection_list> collection_refl::pair_simple_simple;

void collection_refl::pair_simple_simple_init( ptr<object::reflection_list> inherited )
{
	ptr<field_metadata_list> l = field_metadata_list::create();
	l->push_back( field_metadata::create( "first", "simple" ) );
	l->push_back( field_metadata::create( "second", "simple" ) );

	// create a copy of base class's list and append our own item
	ptr<object::reflection_list> result = object::reflection_list::create(inherited);
	result->push_back( class_reflection::create( "pair_simple_simple", l ) );

	collection_refl::pair_simple_simple = result;
}

ptr<object::reflection_list> collection_refl::pair_srp_simple;

void collection_refl::pair_srp_simple_init( ptr<object::reflection_list> inherited )
{
	ptr<field_metadata_list> l = field_metadata_list::create();
	l->push_back( field_metadata::create( "first", "srp" ) );
	l->push_back( field_metadata::create( "second", "simple" ) );

	// create a copy of base class's list and append our own item
	ptr<object::reflection_list> result = object::reflection_list::create(inherited);
	result->push_back( class_reflection::create( "pair_srp_simple", l ) );

	collection_refl::pair_srp_simple = result;
}

ptr<object::reflection_list> collection_refl::pair_simple_srp;

void collection_refl::pair_simple_srp_init( ptr<object::reflection_list> inherited )
{
	ptr<field_metadata_list> l = field_metadata_list::create();
	l->push_back( field_metadata::create( "first", "simple" ) );
	l->push_back( field_metadata::create( "second", "srp" ) );

	// create a copy of base class's list and append our own item
	ptr<object::reflection_list> result = object::reflection_list::create(inherited);
	result->push_back( class_reflection::create( "pair_simple_srp", l ) );

	collection_refl::pair_simple_srp = result;
}

ptr<object::reflection_list> collection_refl::pair_srp_srp;

void collection_refl::pair_srp_srp_init( ptr<object::reflection_list> inherited )
{
	ptr<field_metadata_list> l = field_metadata_list::create();
	l->push_back( field_metadata::create( "first", "srp" ) );
	l->push_back( field_metadata::create( "second", "srp" ) );

	// create a copy of base class's list and append our own item
	ptr<object::reflection_list> result = object::reflection_list::create(inherited);
	result->push_back( class_reflection::create( "pair_srp_srp", l ) );

	collection_refl::pair_srp_srp = result;
}

ptr<object::reflection_list> collection_refl::map_simple_simple;

void collection_refl::map_simple_simple_init( ptr<object::reflection_list> inherited )
{
	ptr<field_metadata_list> l = field_metadata_list::create();
	l->push_back( field_metadata::create( "item", "pair_simple_simple" ) );

	// create a copy of base class's list and append our own item
	ptr<object::reflection_list> result = object::reflection_list::create(inherited);
	result->push_back( class_reflection::create( "map_simple_simple", l ) );

	collection_refl::map_simple_simple = result;
}

ptr<object::reflection_list> collection_refl::map_srp_simple;

void collection_refl::map_srp_simple_init( ptr<object::reflection_list> inherited )
{
	ptr<field_metadata_list> l = field_metadata_list::create();
	l->push_back( field_metadata::create( "item", "pair_srp_simple" ) );

	// create a copy of base class's list and append our own item
	ptr<object::reflection_list> result = object::reflection_list::create(inherited);
	result->push_back( class_reflection::create( "map_srp_simple", l ) );

	collection_refl::map_srp_simple = result;
}

ptr<object::reflection_list> collection_refl::map_simple_srp;

void collection_refl::map_simple_srp_init( ptr<object::reflection_list> inherited )
{
	ptr<field_metadata_list> l = field_metadata_list::create();
	l->push_back( field_metadata::create( "item", "pair_simple_srp" ) );

	// create a copy of base class's list and append our own item
	ptr<object::reflection_list> result = object::reflection_list::create(inherited);
	result->push_back( class_reflection::create( "map_simple_srp", l ) );

	collection_refl::map_simple_srp = result;
}

ptr<object::reflection_list> collection_refl::map_srp_srp;

void collection_refl::map_srp_srp_init( ptr<object::reflection_list> inherited )
{
	ptr<field_metadata_list> l = field_metadata_list::create();
	l->push_back( field_metadata::create( "item", "pair_srp_srp" ) );

	// create a copy of base class's list and append our own item
	ptr<object::reflection_list> result = object::reflection_list::create(inherited);
	result->push_back( class_reflection::create( "map_srp_srp", l ) );

	collection_refl::map_srp_srp = result;
}

end_package(std);
end_package(lestes);
