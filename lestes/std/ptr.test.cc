/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  Unit test for ptr class.
  \author pt
*/
#include <cstddef>
#include <lestes/common.hh>

package(lestes);
package(std);

class a: public object {
public:
	~a(void) {}
	static ptr<a> create(void) { 
		return ptr<a>(new a());
	}
	bool m(void) { return true; }
	int f;
protected:
	a(void) {}
private:
	a(const a &);
	a &operator=(const a &);
};

class aa: public a {
public:
	~aa(void) {}
	static ptr<aa> create(void) { 
		return ptr<aa>(new aa());
	}
protected:
	aa(void) {}
private:
	aa(const aa &);
	a &operator=(const aa &);
};

class s: public object {
public:
	~s(void) {}
	static ptr<s> create(const ptr<a> &a_fa, const ptr<aa> &a_faa) { 
		return ptr<s>(new s(a_fa,a_faa));
	}
	srp<a> fa;
	srp<aa> faa;
	void gc_mark(void);
private:
	s(const ptr<a> &a_fa, const ptr<aa> &a_faa): fa(a_fa), faa(a_faa) {}
	s(const s &);
	s &operator=(const s &);
};

void s::gc_mark(void)
{
	fa.gc_mark();
	faa.gc_mark();
	object::gc_mark();
}

void ptr_test(void)
{
	// constructor with NULL
	ptr<a> a0(NULL);
	lassert(!a0);
	lassert(!(a0));
	lassert(!(a0 != a0));
	lassert(a0 == a0);
	lassert(a0 == ptr<a>(NULL));
	
	// default constructor
	ptr<a> a1;
	lassert(!a1);
	lassert(!(a1));
	lassert(!(a1 != a1));
	lassert(a1 == a1);
	lassert(a1 == ptr<a>(NULL));
	
	// constructor with pointer
	ptr<a> a2(a::create());
	ptr<a> a3(a2.pointer_get());
	lassert(a3);
	lassert(!(!a3));
	lassert(a3 != ptr<a>(NULL));
	lassert(a3 != a1);
	lassert(a1 != a3);
	lassert(a3 == a3);
	lassert(a3 == a2.pointer_get());
	
	// constructor with pointer to descendant
	ptr<aa> aa1(aa::create());
	lassert(aa1);
	lassert(aa1->m());
	ptr<a> a4(aa1.pointer_get());
	lassert(a4 != ptr<a>(NULL));
	lassert(a4 != a2);
	lassert(a2 != a4);
	lassert(a4 == a4);
	lassert(a4 == aa1.pointer_get());

	// constructor with ptr
	ptr<a> a5(a4);
	lassert(a5 != ptr<a>(NULL));
	lassert(a5 != a2);
	lassert(a2 != a5);
	lassert(a5 == a5);
	lassert(a5 == a4);
	lassert(a4 == a5);
	lassert(a5 == a4.pointer_get());
	
	// constructor with ptr to descendant
	ptr<aa> aa2(aa::create());
	ptr<a> a6(aa2);
	lassert(a6 != ptr<a>(NULL));
	lassert(a6 != a2);
	lassert(a2 != a6);
	lassert(a6 == a6);
	lassert(a6 == aa2);
	
	// constructor with srp
	ptr<s> s1(s::create(a3,aa1));
	ptr<a> a7(s1->fa);
	lassert(a7 != ptr<a>(NULL));
	lassert(a7 != a4);
	lassert(a4 != a7);
	lassert(a7 == a7);
	lassert(a7 == s1->fa);
	
	// constructor with srp to descendant
	ptr<a> a8(s1->faa);
	lassert(a8 != ptr<a>(NULL));
	lassert(a8 != a6);
	lassert(a6 != a8);
	lassert(a8 == a8);
	lassert(a8 == s1->faa);

	// operator ->, operator *
	ptr<a> a9(a2);
	lassert(a9->m());
	lassert(&(a9->f) == &(a2->f));
	lassert(a9.operator->() == a2.operator->());
	lassert(&((*a9).f) == &((*a2).f));
	lassert(&(a9.operator*()) == &(a2.operator*()));

	// assignment from pointer
	ptr<a> a10;
	a10 = a2.pointer_get();
	lassert(a10 != ptr<a>(NULL));
	lassert(a10 != a6);
	lassert(a6 != a10);
	lassert(a10 == a2);

	// assignment from pointer to descendant
	ptr<a> a11;
	a11 = aa1.pointer_get();
	lassert(a11 != ptr<a>(NULL));
	lassert(a11 != a2);
	lassert(a2 != a11);
	lassert(a11 == aa1);

	// assignment from ptr
	ptr<a> a12;
	a12 = a2;
	lassert(a12 != ptr<a>(NULL));
	lassert(a12 != a6);
	lassert(a6 != a12);
	lassert(a12 == a2);

	// assignment from ptr to descendant
	ptr<a> a13;
	a13 = aa1;
	lassert(a13 != ptr<a>(NULL));
	lassert(a13 != a6);
	lassert(a6 != a13);
	lassert(a13 == aa1);

	// assignment from srp
	ptr<a> a14;
	a14 = s1->fa;
	lassert(a14 != ptr<a>(NULL));
	lassert(a14 != a6);
	lassert(a6 != a14);
	lassert(a14 == s1->fa);
	
	// assignment from srp to descendant
	ptr<a> a15;
	a15= s1->faa;
	lassert(a15 != ptr<a>(NULL));
	lassert(a15 != a6);
	lassert(a6 != a15);
	lassert(a15 == s1->faa);

	// dynamic cast to proper type
	ptr<a> a16(aa::create());
	ptr<aa> aa3;
	aa3 = a16.dncast<aa>();
	lassert(aa3 != ptr<a>(NULL));

	// dynamic cast to improper type
	// it crashes as there is an assertion now. commented out by Rudo.
#if 0
	ptr<a> a17(a::create());
	aa3 = a17.dncast<aa>();
	lassert(aa3 == ptr<a>(NULL));
#endif
}

end_package(std);
end_package(lestes);

int main(void)
{
	::lestes::std::ptr_test();
	return 0;
}

/* vim: set ft=lestes : */
