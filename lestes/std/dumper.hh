/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__std___dumper_hh___included
#define lestes__std___dumper_hh___included

/*! \file
  The actual dumper for general use within the project.
  \author Rudo
*/
#include <lestes/common.hh>
#include <lestes/std/dumper_visitor.hh>
#include <lestes/std/list.hh>
#include <lestes/std/set.hh>
#include <lestes/std/ucn_string.hh>
#include <ostream>

package(lestes);
package(std);

class dumper : public dumper_visitor {
public:
	//! Dumps every instance reachable from start_at to given ostream. Returns given ostream.
	static ::std::ostream & dump( ::std::ostream & output_to, ptr<object> start_at,
			bool respect_barriers = true );
	virtual void visit_object( ptr<object> o );
	virtual void visit_nodump();
	virtual void visit_lint( lint );
	virtual void visit_ulint( ulint );
	virtual void visit_bool( bool );
	virtual void visit_lstring( const lstring & );
	virtual void visit_ucn_string( const ucn_string & );
private:
	typedef list< srp<object> >		queue_type;
	typedef set< srp<object> >		set_type;
	//! return value from visit_XXX; null for simple_type visits
	bool was_simple;
	::std::ostream &stream;
	//! enqueues o into queue, iff o was not yet visited
	void try_enqueue( ptr<object> o, ptr<queue_type> queue, ptr<set_type> visited );
	//! should the dumper respect barriers (see object::dump_barrier)
	const bool respect_barriers;
protected:
	void gc_mark();
	dumper( ::std::ostream &a_stream, bool a_respect_barriers );
	//! Does the actual dumping. Has got to be a non-static method, as we are a visitor :)
	void the_dump( ptr<object> start_at );
};

class readable_dumper : public dumper_visitor {
public:
	//! Dumps every instance reachable from start_at to given ostream. Returns given ostream.
	static ::std::ostream & dump( ::std::ostream & output_to, ptr<object> start_at,
			bool respect_barriers = true );
	virtual void visit_object( ptr<object> o );
	virtual void visit_nodump();
	virtual void visit_lint( lint i );
	virtual void visit_ulint( ulint i );
	virtual void visit_bool( bool i );
	virtual void visit_lstring( const lstring &i );
	virtual void visit_ucn_string( const ucn_string &i );
protected:
	void gc_mark();
	void follow( ptr<object> o );
	readable_dumper( ::std::ostream &a_stream, bool a_respect_barriers );
public:
	// for technical (testing:) reasons this "must" be public
	typedef set< srp<object> > set_type;
private:
	srp<set_type> visited;
	::std::ostream &stream;
	ulint uid;
	//! should the dumper respect barriers (see object::dump_barrier)
	const bool respect_barriers;
	bool after_barrier;
};

end_package(std);
end_package(lestes);

#endif	// lestes__std___dumper_hh___included
