/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Unit test.
  
  Unit test for class source_location.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/equality.hh>
#include <lestes/std/source_location.hh>
#include <lestes/std/file_info.hh>

package(lestes);
package(std);

using namespace ::std;

/*!
  \brief Tests source_location class.

  Performs testing of source_location class.
*/
void source_location_test(void)
{
	ptr<file_info> fi = file_info::create("abc",NULL);
	ptr<source_location> a = source_location::create(fi,12,5);

	lassert(is_equal(a,a));
	lassert(is_equal(a->file_get(),fi));
	lassert(is_equal(a->line_get(),12U));
	lassert(is_equal(a->column_get(),5U));
	
	ptr<source_location> b = a;
	ptr<source_location> c = b;

	lassert(is_equal(b,c));
	lassert(is_equal(c,b));
	lassert(is_equal(b->file_get(),c->file_get()));
	lassert(is_equal(b->line_get(),c->line_get()));
	lassert(is_equal(b->column_get(),c->column_get()));

	ptr<source_location> d;

	d = a;

	lassert(is_equal(d,a));
	lassert(is_equal(a,d));
	lassert(is_equal(d->file_get(),fi));
	lassert(is_equal(d->line_get(),12U));
	lassert(is_equal(d->column_get(),5U));
}

end_package(std);
end_package(lestes);

/*!
  \brief Main function.

  Runs the unit test in different namespace.
*/
int main(void)
{
	::lestes::std::source_location_test();
	return 0;
}
/* vim: set ft=lestes : */
