/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__std___collection_refl_hh___included
#define lestes__std___collection_refl_hh___included

/*! \file
  Cached reflections for collections

  Collections are templates, if the cached reflections were their static member
  fields, there would have to be a stand-alone declaration of the field for
  every combination of the template arguments...

  Therefore we create sort-of global variables, one for each of the
  specialisations.

  \author Rudo
*/
#include <lestes/common.hh>

package(lestes);
package(std);

class collection_refl {
public:
	static ptr<object::reflection_list> list_simple;
	static void list_simple_init( ptr<object::reflection_list> inherited );

	static ptr<object::reflection_list> list_srp;
	static void list_srp_init( ptr<object::reflection_list> inherited );

	static ptr<object::reflection_list> vector_simple;
	static void vector_simple_init( ptr<object::reflection_list> inherited );
	
	static ptr<object::reflection_list> vector_srp;
	static void vector_srp_init( ptr<object::reflection_list> inherited );

	static ptr<object::reflection_list> set_simple;
	static void set_simple_init( ptr<object::reflection_list> inherited );

	static ptr<object::reflection_list> set_srp;
	static void set_srp_init( ptr<object::reflection_list> inherited );

	static ptr<object::reflection_list> pair_simple_simple;
	static void pair_simple_simple_init( ptr<object::reflection_list> inherited );

	static ptr<object::reflection_list> pair_srp_simple;
	static void pair_srp_simple_init( ptr<object::reflection_list> inherited );

	static ptr<object::reflection_list> pair_simple_srp;
	static void pair_simple_srp_init( ptr<object::reflection_list> inherited );

	static ptr<object::reflection_list> pair_srp_srp;
	static void pair_srp_srp_init( ptr<object::reflection_list> inherited );

	static ptr<object::reflection_list> map_simple_simple;
	static void map_simple_simple_init( ptr<object::reflection_list> inherited );

	static ptr<object::reflection_list> map_srp_simple;
	static void map_srp_simple_init( ptr<object::reflection_list> inherited );

	static ptr<object::reflection_list> map_simple_srp;
	static void map_simple_srp_init( ptr<object::reflection_list> inherited );

	static ptr<object::reflection_list> map_srp_srp;
	static void map_srp_srp_init( ptr<object::reflection_list> inherited );

	//! Hide constructor
	collection_refl();
};

end_package(std);
end_package(lestes);

#endif	// lestes__std___collection_refl_hh___included
