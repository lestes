/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__std___reflect_hh___included
#define lestes__std___reflect_hh___included

/*! \file
  Class class_reflection, used for dumping.
  \author rudo
*/
#include <lestes/common.hh>

package(lestes);
package(std);

template< typename T >
class list;

template< typename T1, typename T2 >
class pair;

class class_reflection : public object {
public:
	typedef	pair<lstring,lstring>		field_metadata;
	typedef list< srp<field_metadata> >	field_metadata_list;

private:
	const lstring name;
	srp<field_metadata_list> metadata;

protected:
	class_reflection( lstring a_name, ptr<field_metadata_list> a_metadata );
	
	void gc_mark();

public:
	inline lstring name_get() const
	{
		return name;
	}
	//! returns *a copy* of the field list
	//FIXME copy is not needed as long as nobody tampers with the thing
	ptr<field_metadata_list> field_metadata_get();
	static ptr<class_reflection> create( lstring a_name, ptr<field_metadata_list> a_metadata );
};

end_package(std);
end_package(lestes);

#endif	// lestes__std___reflect_hh___included
