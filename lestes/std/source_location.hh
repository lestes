/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__std___source_location_hh___included
#define lestes__std___source_location_hh___included

/*! \file
  \brief Token location.
  
  Declaration of source_location class representing token location.
  \author pt
*/

#include <lestes/std/object.hh>
#include <lestes/std/file_info.hh>

package(lestes);
package(std);

/*!
  Represents location of token in file inclusion chain and the file itself.
  The object is intentionally inmutable to simplify the processing.
*/
class source_location: public object {
public:
	//! Returns file information.
	ptr<file_info> file_get(void) const;
	//! Returns line in file.
	ulint line_get(void) const;
	//! Returns column on line.
	ulint column_get(void) const;
	//! Returns order.
	ulint order_get(void) const;
	//! Tests equality.
	bool equals(const ptr<source_location> &rhs) const;
	//! Clones location with new order.
	ptr<source_location> clone_order(ulint a_order) const;
	//! Clones location with new file information.
	ptr<source_location> clone_file(const ptr<file_info> &a_file) const;
	//! Returns new location, initializes with file information and position.
	static ptr<source_location> create(const ptr<file_info> &a_file, ulint a_line,
			ulint a_column);
	//! Returns new location, initializes with file information, position and order.
	static ptr<source_location> create(const ptr<file_info> &a_file, ulint a_line,
			ulint a_column, ulint a_order);
	//! Returns zero location.
	static ptr<source_location> zero(void);
	//! Returns list of reflection info.
	virtual ptr<reflection_list> reflection_get(void) const;
	//! Returns list of field values.
	virtual ptr<field_list_list> field_values_get(void) const;
protected:
	//! Creates new object, initializes with file information, postion and order.
	source_location(const ptr<file_info> &a_file, ulint a_line,
			ulint a_column, ulint a_order);
	//! Marking routine.
	void gc_mark();
private:
	//! File information structure.
	srp<file_info> file;
	//! Line in file.
	ulint line;
	//! Column inside the line.
	ulint column;
	//! The order in the translation unit.
	ulint order;
	//! Zero location instance.
	static ptr<source_location> zero_instance;
	//! The reflection information.
	static ptr<reflection_list> reflection;
	//! Hides copy constructor.
	source_location(const source_location &);
	//! Hides assigment operator.
	source_location &operator=(const source_location &);
};

end_package(std);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
