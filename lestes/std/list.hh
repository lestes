/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__std___list_hh___included
#define lestes__std___list_hh___included

#include <lestes/common.hh>
#include <lestes/std/collection_refl.hh>
#include <lestes/std/objectize.hh>
#include <list>
#include <iterator>
#include <algorithm>

/*! \file
  \brief Collectible ::std::list
  \author egg

  Encapsulation for STL list class to be compatible with
  our garbage collector. Includes namely marking routine
  and factory method.
 */

package(lestes);
package(std);

template< typename T >
class list : public object, public ::std::list<T> {
public:
	//! Factory method, creates an empty list.
	static ptr< list<T> > create();
#if 0
	//! Factory method, creates a list from a range
	template<typename InputIterator>
	static ptr< list < T > > create(InputIterator first, InputIterator last);
#endif
	//! Factory method creates a copy of given list.
	static ptr< list<T> > create( ptr< list<T> > );
	virtual ptr<reflection_list> reflection_get() const;
	virtual ptr<field_list_list> field_values_get() const;
protected:
	//! Constructor for an empty list.
	list();
	//! Imitation of copy-constructor, *takes a pointer*
	list( ptr< list<T> > );
#if 0
	//! construct from range
	template<typename InputIterator>
	list(InputIterator first, InputIterator last) : ::std::list < T >(first,last) {}
#endif
};

template< typename T >
class list< srp<T> > : public object, public ::std::list< srp<T> > {
public:
	//! Factory method, creates an empty list.
	static ptr< list< srp<T> > > create();
#if 0
	//! Factory method, creates a list from a range
	template<typename InputIterator>
	static ptr< list < srp < T > > > create(InputIterator first, InputIterator last);
#endif
	//! Factory method creates a copy of given list.
 	static ptr< list< srp<T> > > create( ptr< list< srp<T> > > );
	virtual ptr<reflection_list> reflection_get() const;
	virtual ptr<field_list_list> field_values_get() const;
protected:
	//! Constructor for an empty list.
	list();
	//! Imitation of copy-constructor, *takes a pointer*
	list( ptr< list< srp<T> > > );
#if 0
	//! construct from range
	template<typename InputIterator>
	list(InputIterator first, InputIterator last) : ::std::list < srp < T > >(first,last) {}
#endif
	//! Marking routine for class ::lestes::std::list
	void gc_mark(void);
};

template< typename T >
ptr< list<T> > list<T>::create() {
	return new list<T>();
}

template< typename T >
ptr< list<T> > list<T>::create( ptr< list<T> > from ) {
	return new list<T>(from);
}

template< typename T >
ptr<object::reflection_list> list<T>::reflection_get() const
{
	if (!collection_refl::list_simple)
		collection_refl::list_simple_init( object::reflection_get() );
	return collection_refl::list_simple;
}

template< typename T >
ptr<object::field_list_list> list<T>::field_values_get() const
{
	ptr<field_list_list> result = object::field_values_get();
	result->push_back( value_list::create() );
	// wrap all items in objectize, insert the result onto the just created value_list
	transform( this->begin(), this->end(),
			back_inserter( *result->back() ), unary_objectizer<T>() );
	return result;
}

template< typename T >
list<T>::list()
	: object(), ::std::list<T>()
{}

template< typename T >
list<T>::list( ptr< list<T> > from )
	: object(), ::std::list<T>( *checked(from) )
{}

template< typename T >
ptr< list< srp<T> > > list< srp<T> >::create() {
	return new list< srp<T> >();
}

template< typename T >
ptr< list< srp<T> > > list< srp<T> >::create( ptr< list< srp<T> > > from )
{
	return new list< srp<T> >(from);
}

#if 0
template<typename T>
template<typename InputIterator>
ptr < list < T > > list < T >::create(InputIterator first, InputIterator last)
{
	return new list < T >(first, last);
}

template<typename T>
template<typename InputIterator>
ptr < list < srp < T > > > list < srp < T > >::create(InputIterator first, InputIterator last)
{
	return new list < srp < T > >(first, last);
}
#endif

template< typename T >
ptr<object::reflection_list> list< srp<T> >::reflection_get() const
{
	if (!collection_refl::list_srp)
		collection_refl::list_srp_init( object::reflection_get() );
	return collection_refl::list_srp;
}

template< typename T >
ptr<object::field_list_list> list< srp<T> >::field_values_get() const
{
	ptr<field_list_list> result = object::field_values_get();
	result->push_back( value_list::create() );
	result->back()->insert( result->back()->end(), this->begin(), this->end() );
	return result;
}

template< typename T >
list< srp<T> >::list()
	: object(), ::std::list< srp<T> >()
{ }

template< typename T >
list< srp<T> >::list( ptr< list< srp<T> > > from )
	: object(), ::std::list< srp<T> >( *checked(from) )
{}

template< typename T >
void list< srp<T> >::gc_mark() {
	// gc_mark each member
	for (typename list::iterator i = this->begin(); i != this->end(); ++i)
		i->gc_mark();
	object::gc_mark();	// inherited gc_mark
}

end_package(std);
end_package(lestes);

#endif	// lestes__std___list_hh___included
/* vim: set ft=lestes : */
