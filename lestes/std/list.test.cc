/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/std/list.hh>
#include <iostream>

#define LENGTHOFARRAY( ar ) (sizeof(ar)/sizeof(ar[0]))

using namespace ::lestes::std;

int main()
{
	typedef list<int> ltype;
	ptr<ltype> ll = ltype::create();
	ll->push_back(3);
	ll->push_back(1);
	ll->push_back(-1);
	ptr<ltype> l = ltype::create(ll);

	for ( ltype::iterator i = l->begin(); i != l->end(); ++i )
		::std::cout << *i << ::std::endl;

	typedef list< srp<object> > ltype2;
	ptr<ltype2> ll2 = ltype2::create();
	ll2->push_back(ll);
	ll2->push_back(l);
	ll2->push_back(NULL);
	ll2->push_back(ll2);
	ptr<ltype2> l2 = ltype2::create(ll2);

	for ( ltype2::iterator i = l2->begin(); i != l2->end(); ++i )
		::std::cout << (*i ? i->operator->() : 0) << ::std::endl;

	return 0;
}
