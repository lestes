/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__std___octet_hh___included
#define lestes__std___octet_hh___included

/*! \file
  Representation of 8-bit unsigned character.
  \author pt
*/

#include <lestes/common.hh>
#include <host_types.hh>

package(lestes);
package(std);

/** 
  Represents properties of octet type.
*/
class octet_type {
public:
	//! the octet type itself
	typedef lc_host_uint_least8 octet;
	//! properties of octet type
	enum {
		BITS = 8,
		MIN_VALUE = 0,
		RANGE = (1U << BITS),
		MAX_VALUE = RANGE - 1
	};
	//! fake method to suppress warning
	static void fake_method(void);
private:
	//! hides constructor
	octet_type(void);
	//! hides destructor
	~octet_type(void);
	//! hides copy constructor
	octet_type(const octet_type &);
	//! hides assignment operator
	octet_type &operator=(const octet_type &);
};

//! shortcut helper typedef
typedef octet_type::octet octet;

end_package(std);
end_package(lestes);

#endif
/* vim: set ft=lestes : */

