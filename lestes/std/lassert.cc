/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/msg/logger.hh>
#include <lestes/std/backtrace.hh>
#include <lestes/std/event.hh>
#include <lestes/std/lassert.hh>
#include <lestes/std/lassert.m.hh>
#include <iostream>
#include <cstdlib>
#include <cstring>

package(lestes);
package(std);

initialize_event( lassert_event );

static bool lassert_failing;

/*!
 This function can ignore the condition provided that the <code>LESTES_LASSERT</code>
 environment variable is set to the string "continue", "0", or "nil".
 */
void lassert_fail( lstring assertion,
		lstring filename, ulint line, lstring function,
		lstring message, ptr<source_location> loc )
{
	if (loc) {
		if (message.empty())
			report << lassert_msg_default << loc;
		else
			report << lassert_msg << message << loc;
	}
	::std::cerr << filename << ": In function '" << function << "': " << ::std::endl;
	if (!message.empty())
		::std::cerr << filename << ":   " << message << ::std::endl;
	::std::cerr << filename << ':' << line << ":"
		" lassertion failed: \"" << assertion << "\"." << ::std::endl;

#ifndef NO_BACKTRACE_IN_LASSERT
	::std::cerr << assemble_backtrace() << ::std::endl;
#endif

	// do not trigger the event when "double-failing"
	//   it can happen when lassertion fails inside an action run by the event
	// bool flag is enough, as in the second invocation of this function
	//   there is nothing that can fail
	if (!lassert_failing) {
		lassert_failing = true;
		lassert_event->trigger();
	} else
		::std::cerr << filename << ": Double-fail!" << ::std::endl;

#ifdef CONTINUABLE_ASSERTIONS
	const char * lestes_lassert = ::std::getenv("LESTES_LASSERT");
	if (lestes_lassert) {
		lstring value(lestes_lassert);
		if (value == "continue" || value == "0" || value == "nil") {
			::std::cerr << filename << ':' << line <<
				": warning: lassertion continued, i.e. ignored." << ::std::endl;
			lassert_failing = false;
		} else {
			// close log files, if any
			::lestes::msg::logger::finish();

			abort();
		}
	} else
#endif
	{
		// close log files, if any
		::lestes::msg::logger::finish();

		abort();
	}
}

end_package(std);
end_package(lestes);

/* vim: set ft=lestes : */
