/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__std___srp_hh___included
#define lestes__std___srp_hh___included

/*! \file
  \brief Smart pointer template.

  Declaration of template class representing structure pointer.
  Method bodies are in srp_bodies.hh.
  \author pt
*/
#include <lestes/std/mem/simple_pointer.hh>
#include <lestes/std/ptr.hh>

package(lestes);
package(std);

// forward declaration to avoid cycle
template <typename T>
class ptr;

/*!
  Represents structure pointer.
  \param T  The type of object pointed to.
*/
template <typename T>
class srp : public mem::simple_pointer {
#ifdef LESTES_OLD_POINTER_CONDITION
#else
	/*!
	  \brief Condition class.

	  Class for returning boolean conditions without
	  the risk of unwanted implicit conversion to bool.
	*/
	struct condition {
		/*!
		  \brief Creates the condition.

		  Creates the static condition object,
		  used to get reference to non NULL pointer to condition.
		*/
		condition(void) {
		}
	private:
		/*!
		  \brief Avoids deallocation.

		  Avoids calling delete on the pointer to the static condition object
		  returned from operator condition *(). Never defined.
		*/
		void operator delete(void *);
	};
#endif
public:
	/*!
	  \brief the type pointed to

	  \author TMA
	 */
	typedef T pointee_type; 
	//! Creates srp, initializes with NULL pointer.
	inline srp(void);
	//! Creates srp, initializes with pointer .
	inline srp(T *a_pointer);
	//! Creates srp, initializes with pointer .
	template <typename U>
	inline srp(U *a_pointer);
	//! Creates srp, initializes with srp to the same type.
	inline srp(const srp<T> &other);
	//! Creates srp, initializes with srp.
	template <typename U>
	inline srp(const srp<U> &other);
	//! Creates srp, initializes with ptr.
	template <typename U>
	inline srp(const ptr<U> &other);
	//! Destroys srp.
	inline ~srp(void);
	//! Returns the pointer.
	inline T *operator->(void) const;
	//! Returns dereferenced pointer.
	inline T &operator *(void) const;
	//! Assigns a pointer.
	inline srp<T> &operator=(T *a_pointer);
	//! Assigns a pointer.
	template <typename U>
	inline srp<T> &operator=(U *a_pointer);
	//! Assigns srp of the same type.
	inline srp<T> &operator=(const srp<T> &other);
	//! Assigns ptr.
	template <typename U>
	inline srp<T> &operator=(const ptr<U> &other);
	//! Assigns srp.
	template <typename U>
	inline srp<T> &operator=(const srp<U> &other);
#ifdef LESTES_OLD_POINTER_CONDITION
	//! Tests NULL pointer.
	inline operator bool(void) const;
#else
	//! Tests NULL pointer.
	inline operator condition *(void) const;
#endif
	//! Tests non NULL pointer.
	inline bool operator!(void) const;
	//! Compares to pointer.
	inline bool operator==(T *a_pointer) const;
	//! Compares to pointer.
	template <typename U>
	inline bool operator==(U *a_pointer) const;
	//! Compares to NULL.
	inline bool operator==(::std::ptrdiff_t a_pointer) const;
	//! Compares to ptr.
	template <typename U>
	inline bool operator==(const ptr<U> &other) const;
	//! Compares to srp.
	template <typename U>
	inline bool operator==(const srp<U> &other) const;
	//! Compares to pointer.
	inline bool operator!=(T *a_pointer) const;
	//! Compares to pointer.
	template <typename U>
	inline bool operator!=(U *a_pointer) const;
	//! Compares to NULL.
	inline bool operator!=(::std::ptrdiff_t a_pointer) const;
	//! Compares to ptr.
	template <typename U>
	inline bool operator!=(const ptr<U> &other) const;
	//! Compares to srp.
	template <typename U>
	inline bool operator!=(const srp<U> &other) const;
	//! Performs dynamic cast to other type.
	template <typename U>
	inline ptr<U> dncast(void) const;
	//! Returns the pointer.
	inline T *pointer_get(void) const;
	//! Compares two pointers for ordering.
	inline bool operator < (const srp<T> &) const;
private:
	//! Sets the pointer.
	inline void pointer_set(T *a_pointer);
};

end_package(std);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
