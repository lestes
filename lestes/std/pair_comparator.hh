/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__std___pair_comparator_hh___included
#define lestes__std___pair_comparator_hh___included

#include <lestes/common.hh>
#include <functional>

/*! \file
  \brief lexicographic ordering on pairs
  \author TMA

  This file provvides one template class ::lestes::std::pair_comparator, which
  performs lexicographic ordering.
 */

package(lestes);
package(std);

#ifdef _MSC_VER
#define NO_TEMPLATE_TEMPLATE_PARAMETERS 1
#else
#define NO_TEMPLATE_TEMPLATE_PARAMETERS 0
#endif

#if NO_TEMPLATE_TEMPLATE_PARAMETERS
template < typename T >
#else
template < typename T, template < typename U > class ComparatorFirst = ::std::less, template < typename U > class ComparatorSecond = ComparatorFirst >
#endif
class pair_comparator {
public:

#if NO_TEMPLATE_TEMPLATE_PARAMETERS
	typedef ::std::less < typename T::pointee_type::first_type > comparator_first;
	typedef ::std::less < typename T::pointee_type::second_type > comparator_second;
#else
	typedef ComparatorFirst < typename T::pointee_type::first_type > comparator_first;
	typedef ComparatorSecond < typename T::pointee_type::second_type > comparator_second;
#endif

	/*!
	 * \brief compares lexicographically a pair
	 *
	 * The inner workings determine the ordering by examining first the
	 * first part of the pair.  If the first pair's first element is
	 * strictly less (where less is the relation of \p ComparatorFirst)
	 * than the second's, the whole first pair is less (the relation
	 * imposed by this comparator) than the second. If however the second's
	 * first is less than the first's, the first pair is not less than
	 * second.  In the remaining case, i.e. neither first's first nor
	 * second's first is less than the other, the relation on the pair is
	 * determined solely by \p ComparatorSecond on the seconds of both
	 * pairs.
	 */
	 bool operator() (const T & a, const T & b) const
	 {
		 comparator_first ftc;
		 bool ab = ftc(a->first, b->first);
		 if (ab)
			 return true;
		 bool ba = ftc(b->first, a->first);
		 if (ba)
			 return false;
		 comparator_second stc;
		 return stc(a->second, b->second);
	 }
};

end_package(std);
end_package(lestes);

#endif	// lestes__std___pair_comparator_hh___included
/* vim: set ft=lestes : */
