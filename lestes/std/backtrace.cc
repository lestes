/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/std/vector.hh>
#include <cstdlib>
#include <cstring>
#include <sstream>

#if __linux__ && __GXX_ABI_VERSION >= 100
#define HAVE_BACKTRACE_SUPPORT 0
#define HAVE_CXXABI_H 0
#endif

#ifdef HAVE_BACKTRACE_SUPPORT
#include <execinfo.h>
#ifdef HAVE_CXXABI_H
#include <cxxabi.h>
#endif
#endif


package(lestes);
package(std);

lstring assemble_backtrace()
{
	const char * lestes_backtrace = ::std::getenv("LESTES_BACKTRACE");
	if (lestes_backtrace) {
		lstring value(lestes_backtrace);
		if (value == "nil" || value == "0") {
			return "  Backtrace printing disabled.";
		}
	}
#ifdef HAVE_BACKTRACE_SUPPORT
	enum {
		INITIAL_ITEMS = 32,
		MAX_ITEMS = 512
	};
	int items = INITIAL_ITEMS;
	void ** array = NULL;
	int size;
	do {
		if (array) {
			items *= 2;
			delete [] array;
		}
		array = new void *[items];
		size = ::backtrace (array, items);
	} while (items < MAX_ITEMS && size == items);

	char **strings = ::backtrace_symbols (array, size);

	::std::vector < lstring > stringv(strings, strings+size);

	::std::ostringstream oss;
	oss << "Obtained " << size << " stack frames:\n";

	::std::vector < lstring >::iterator it = stringv.begin();
	::std::vector < lstring >::iterator jt = stringv.end();
	for ( ; it != jt ; ++it ) {
#ifdef HAVE_CXXABI_H
		lstring::size_type s1 = it->find('(');
		lstring::size_type lastslash = it->rfind('/', s1);
		lstring::size_type s2 = it->find('+', s1);

		if(s1 == lstring::npos || s2 == lstring::npos ) {
			oss << "(?)  " << *it << '\n';
		} else {
			lstring lib;

			if(lastslash != lstring::npos)
				lib =  it->substr(lastslash + 1, s1 - lastslash - 1);
			else
				lib = it->substr(0, s1+1);

			int status=-1;
			lstring mangled(*it, s1+1, s2-s1-1);

			lstring demangled;
			//this avoids some segfaults but is it generally true
			//that mangled start w/ _ ?
			if(!mangled.size() || mangled[0] == '_') {
				char * raw = ::__cxxabiv1::__cxa_demangle(mangled.c_str(), NULL, 0, &status);
				if (raw) {
					demangled = raw;
					::std::free(raw);
				} else
					demangled = mangled;
			} else {
				status = 666;
				demangled = "XXX";
			}
			//demangling a C-name results in bulshit (but
			//status==OK). Is there some wayto detect this?
			if (status) {
				oss << mangled << " in "  << lib << " (DEMANGLE FAILED)\n";
			} else {
				oss << demangled << " in " << lib << '\n';
			}

			//too much garbage ..
			//+ ' ' + all.substr(s2) + " (mangled: " + mangled + ")\n";
		}
#else
		oss <<  *it << '\n';
#endif
	}
	if(size == items) {
		oss << " (... more stack frames  ...)\n";
	}
	free (strings);
	if (array)
		delete [] array;
	return oss.str();
#else
	return "(no backtrace support enabled)";
#endif
}

end_package(std);
end_package(lestes);

/* vim: set ft=lestes : */
