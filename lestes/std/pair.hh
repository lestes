/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__std___pair_hh___included
#define lestes__std___pair_hh___included

#include <lestes/common.hh>
#include <lestes/std/collection_refl.hh>
#include <lestes/std/list.hh>
#include <utility>

/*! \file
  \brief Collectible ::std::pair
  \author rudo

  Encapsulation for STL pair class to be compatible with
  our garbage collector. Includes namely marking routine
  and factory method.

  make_pair as known from STL is banned, use the create method.
 */

package(lestes);
package(std);

template< typename T1, typename T2 >
class pair : public object, public ::std::pair<T1,T2> {
public:
	static ptr< pair<T1,T2> > create()
	{
		return new pair();
	}
	static ptr< pair<T1,T2> > create( const T1 &a, const T2 &b )
	{
		return new pair( a, b );
	}
	static ptr< pair<T1,T2> > create( const ptr< pair<T1,T2> > &p )
	{
		return new pair(*p);
	}
	static ptr< pair<T1,T2> > create( const ::std::pair<T1,T2> &stdp )
	{
		return create( stdp.first, stdp.second );
	}
	virtual ptr<reflection_list> reflection_get() const
	{
		if (!collection_refl::pair_simple_simple)
			collection_refl::pair_simple_simple_init( object::reflection_get() );
		return collection_refl::pair_simple_simple;
	}
	virtual ptr<field_list_list> field_values_get() const
	{
		ptr<field_list_list> result = object::field_values_get();
		result->push_back( value_list::create() );
		result->back()->push_back( objectize<T1>::create(this->first) );
		result->push_back( value_list::create() );
		result->back()->push_back( objectize<T2>::create(this->second) );
		return result;
	}
protected:
	pair() : object(), ::std::pair<T1,T2>()
	{}
	pair( const T1 &a, const T2 &b ) : object(), ::std::pair<T1,T2>( a, b )
	{}
	pair( const pair &p ) : object(), ::std::pair<T1,T2>(p)
	{}
};


template< typename T1, typename T2 >
class pair< srp<T1>, T2 > : public object, public ::std::pair< srp<T1>, T2 > {
public:
	static ptr< pair< srp<T1>, T2 > > create()
	{
		return new pair();
	}
	static ptr< pair< srp<T1>, T2 > > create( const ptr<T1> &a, const T2 &b )
	{
		return new pair( a, b );
	}
	static ptr< pair< srp<T1>, T2 > > create( const ptr< pair< srp<T1>, T2 > > &p )
	{
		return new pair(*p);
	}
	static ptr< pair< srp<T1>, T2 > > create( const ::std::pair< srp<T1>, T2 > &stdp )
	{
		return create( stdp.first, stdp.second );
	}
	virtual ptr<reflection_list> reflection_get() const
	{
		if (!collection_refl::pair_srp_simple)
			collection_refl::pair_srp_simple_init( object::reflection_get() );
		return collection_refl::pair_srp_simple;
	}
	virtual ptr<field_list_list> field_values_get() const
	{
		ptr<field_list_list> result = object::field_values_get();
		result->push_back( value_list::create() );
		result->back()->push_back( this->first );
		result->push_back( value_list::create() );
		result->back()->push_back( objectize<T2>::create(this->second) );
		return result;
	}
protected:
	pair() : object(), ::std::pair< srp<T1>, T2 >()
	{}
	pair( const srp<T1> &a, const T2 &b ) : object(), ::std::pair< srp<T1>, T2 >( a, b )
	{}
	pair( const pair &p ) : object(), ::std::pair< srp<T1>, T2 >(p)
	{}
	void gc_mark()
	{
		this->first.gc_mark();
	}
};


template< typename T1, typename T2 >
class pair< T1, srp<T2> > : public object, public ::std::pair< T1, srp<T2> > {
public:
	static ptr< pair< T1, srp<T2> > > create()
	{
		return new pair< T1, srp<T2> >();
	}
	static ptr< pair< T1, srp<T2> > > create( const T1 &a, const ptr<T2> &b )
	{
		return new pair< T1, srp<T2> >( a, b );
	}
	static ptr< pair< T1, srp<T2> > > create( const ptr< pair< T1, srp<T2> > > &p )
	{
		return new pair< T1, srp<T2> >(*p);
	}
	static ptr< pair< T1, srp<T2> > > create( const ::std::pair< T1, srp<T2> > &stdp )
	{
		return create( stdp.first, stdp.second );
	}
	virtual ptr<reflection_list> reflection_get() const
	{
		if (!collection_refl::pair_simple_srp)
			collection_refl::pair_simple_srp_init( object::reflection_get() );
		return collection_refl::pair_simple_srp;
	}
	virtual ptr<field_list_list> field_values_get() const
	{
		ptr<field_list_list> result = object::field_values_get();
		result->push_back( value_list::create() );
		result->back()->push_back( objectize<T1>::create(this->first) );
		result->push_back( value_list::create() );
		result->back()->push_back( this->second );
		return result;
	}
protected:
	pair< T1, srp<T2> >()
		: object(), ::std::pair< T1, srp<T2> >()
	{}
	pair< T1, srp<T2> >( const T1 &a, const srp<T2> &b )
		: object(), ::std::pair< T1, srp<T2> >( a, b )
	{}
	pair< T1, srp<T2> >( const pair< T1, srp<T2> > &p )
		: object(), ::std::pair< T1, srp<T2> >(p)
	{}
	void gc_mark()
	{
		this->second.gc_mark();
	}
};


template< typename T1, typename T2 >
class pair< srp<T1>, srp<T2> > : public object, public ::std::pair< srp<T1>, srp<T2> > {
public:
	static ptr< pair< srp<T1>, srp<T2> > > create()
	{
		return new pair();
	}
	static ptr< pair< srp<T1>, srp<T2> > > create( const ptr<T1> &a, const ptr<T2> &b )
	{
		return new pair( a, b );
	}
	static ptr< pair< srp<T1>, srp<T2> > > create( const ptr< pair< srp<T1>, srp<T2> > > &p )
	{
		return new pair(*p);
	}
	static ptr< pair< srp<T1>, srp<T2> > > create( const ::std::pair< srp<T1>, srp<T2> > &stdp )
	{
		return create( stdp.first, stdp.second );
	}
	virtual ptr<reflection_list> reflection_get() const
	{
		if (!collection_refl::pair_srp_srp)
			collection_refl::pair_srp_srp_init( object::reflection_get() );
		return collection_refl::pair_srp_srp;
	}
	virtual ptr<field_list_list> field_values_get() const
	{
		ptr<field_list_list> result = object::field_values_get();
		result->push_back( value_list::create() );
		result->back()->push_back( this->first );
		result->push_back( value_list::create() );
		result->back()->push_back( this->second );
		return result;
	}
protected:
	pair() : object(), ::std::pair< srp<T1>, srp<T2> >()
	{}
	pair( const srp<T1> &a, const srp<T2> &b ) : object(), ::std::pair< srp<T1>, srp<T2> >( a, b )
	{}
	pair( const pair &p ) : object(), ::std::pair< srp<T1>, srp<T2> >(p)
	{}
	void gc_mark()
	{
		this->first.gc_mark();
		this->second.gc_mark();
	}
};

end_package(std);
end_package(lestes);

#endif	// lestes__std___pair_hh___included
/* vim: set ft=lestes : */
