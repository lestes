/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__std__mem___simple_pointer_hh___included
#define lestes__std__mem___simple_pointer_hh___included

/*! \file
  \brief Wrapped bare pointer.
  
  Declaration of simple_pointer class representing wrapped bare pointer.
  \author pt
*/
#include <lestes/package.hh>

package(lestes);
package(std);
package(mem);

// forward declaration to avoid cycle
class keystone;

/*!
  \brief Wrapped bare pointer.
  
  Wrapper for bare pointer to keystone class. Provides only set and get operations.
  Used as common ancestor of all srp template classes.
  It should occupy only the space needed for the wrapped pointer.
*/
class simple_pointer {
public:
	//! Marks the keystone pointed to.
	void gc_mark(void) const;
	//! Destructs the simple pointer.
	inline ~simple_pointer(void);
protected:
	//! Creates the simple pointer without initializing.
	inline simple_pointer(void);
	//! Returns the bare pointer.
	inline keystone *pointer_get(void) const;
	//! Sets the bare pointer.
	inline void pointer_set(keystone *a_pointer);
private:
	//! The wrapped bare pointer.
	keystone *pointer;
	//! Hides copy constructor.
	simple_pointer(const simple_pointer &);
	//! Hides assignment operator.
	simple_pointer &operator=(const simple_pointer &);
};

/*!
  Creates the simple pointer. Intentionally, does not initialize.
*/
simple_pointer::simple_pointer(void)
{
}

/*!
  Destructs the simple pointer. Leaves the referenced object unchanged.
*/
inline simple_pointer::~simple_pointer(void)
{
}

/*!
  Returns the underlying bare pointer.
  \return  The bare pointer.
*/
inline keystone *simple_pointer::pointer_get(void) const {
	return pointer;
}

/*!
  Sets the underlying bare pointer.
  \param a_pointer  The new bare pointer.
*/
inline void simple_pointer::pointer_set(keystone *a_pointer) {
	pointer = a_pointer;
}

end_package(mem);
end_package(std);
end_package(lestes);
#endif
/* vim: set ft=lestes : */
