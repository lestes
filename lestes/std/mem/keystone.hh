/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__std__mem___keystone_hh___included
#define lestes__std__mem___keystone_hh___included

/*! \file
  \brief Collectible object.
  
  Declaration of keystone class representing base of garbage collectible classes.
  \author pt
*/
#include <lestes/package.hh>
#include <lestes/std/mem/gc.hh>

package(lestes);
package(std);
package(mem);

/*!
  Abstract common ancestor of all collectible classes.
  Shall only be allocated dynamically.
*/
class keystone {
public:
	//! Finalizes the keystone.
	virtual ~keystone(void);
	//! Enqueues the keystone into marked list.
	inline void enqueue(void);
protected:
	//! Creates the keystone.
	inline keystone(void);
	//! Marks the keystone.
	virtual void gc_mark(void);
private:
	//! Class gc accesses keystone for speed.
	friend class gc;
	//! Sweeps the keystone.
	inline void sweep(void);
	//! Next keystone in linked list of all keystones.
	keystone *keystones_next;
	/*! 
	  \brief Next keystone in mark queue.
	
	  If the keystone is not in the mark queue, it is set to this
	  to distinguish it from the last NULL entry in the queue.
	*/
	keystone *marked_next;
	//! Hides copy constructor.
	keystone(const keystone &);
	//! Hides assignment operator.
	keystone &operator=(const keystone &);
};

/*!
  Creates the keystone.
  Links into list of all keystones.
*/
inline keystone::keystone(void):
	// insert before first
	keystones_next(gc::keystones),
	// set as unmarked
	marked_next(this)
{
	// make first in list
	gc::keystones = this;
}

/*!
  Enqueues the keystone into marked list if not already marked.
  Later it is processed, resulting into recursive marking.
*/
inline void keystone::enqueue(void) {
	// already marked
	if (marked_next != this) return;
	// link into list
	marked_next = gc::marked;
	gc::marked = this;
}

/*!
  Deletes the keystone if not marked, otherwise links again
  into the list of all keystones.
*/
inline void keystone::sweep(void) {
	// was marked
	if (marked_next != this) {
		// unmark
		marked_next = this;
		// link into list of all keystones
		keystones_next = gc::keystones;
		gc::keystones = this;
	} else {
		// delete the keystone
		delete this;
	}
}

end_package(mem);
end_package(std);
end_package(lestes);
#endif
/* vim: set ft=lestes : */
