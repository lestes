/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__std__mem___root_pointer_hh___included
#define lestes__std__mem___root_pointer_hh___included

/*! \file
  \brief Wrapped root pointer.

  Declaration of root_pointer class representing gc root pointer.
  \author pt
*/
#include <lestes/std/mem/simple_pointer.hh>
#include <lestes/std/mem/gc.hh>
package(lestes);
package(std);
package(mem);

// forward declaration to avoid cycle
class keystone;

/*!
  \brief Wrapped bare pointer.

  Represents starting place for garbage collection.
  Used as common ancestor of all ptr template classes.
*/
class root_pointer/* TODO pt remove : public simple_pointer */ {
public:
	//! Destructs the root pointer.
	inline ~root_pointer(void);
protected:
	//! Creates the root pointer.
	inline root_pointer(void);
	//! Returns the bare pointer.
	inline keystone *pointer_get(void) const;
	//! Sets the bare pointer.
	inline void pointer_set(keystone *a_pointer);
private:
	//! The gc accesses private fields for speed.
	friend class gc;
	//! The wrapped bare pointer.
	keystone *pointer;
	//! Creates the head of list of pointers.
	inline root_pointer(bool);
	//! Previous pointer in linked list of all root pointers.
	root_pointer *previous;
	//! Next pointer in linked list of all root pointers.
	root_pointer *next;
	//! Hides copy constructor.
	root_pointer(const root_pointer &);
	//! Hides assignment operator.
	root_pointer &operator=(const root_pointer &);
};

/*!
  Creates the head of all pointers in gc.
*/
inline root_pointer::root_pointer(bool):
/* TODO pt remove   // pointer is not important
	simple_pointer(), */
	// link to itself
	previous(this),
	// link to itself
	next(this) 
{
}

/*!
  Creates the root pointer without initialization.
  Links the pointer into the list of all root pointers.
*/
inline root_pointer::root_pointer(void):
	/* TODO pt remove simple_pointer(),*/
	// previous is first
	previous(gc::roots),
	// insert between first and first's next
	next(gc::roots->next) 
{
	// link before first's next
	next->previous = this;
	// link after first
	gc::roots->next = this;
}

/*!
  Destructs the pointer. Leaves the referenced object unchanged.
  Unlinks the root pointer from list of all root pointers.
*/
inline root_pointer::~root_pointer(void) {
	// because of dummy head of linked list, this operation is well defined
	previous->next = next;
	next->previous = previous;
}

/*!
  Returns the underlying bare pointer.
  \return  The bare pointer.
*/
inline keystone *root_pointer::pointer_get(void) const {
	return pointer;
}

/*!
  Sets the underlying bare pointer.
  \param a_pointer  The new bare pointer.
*/
inline void root_pointer::pointer_set(keystone *a_pointer) {
	pointer = a_pointer;
}

end_package(mem);
end_package(std);
end_package(lestes);
#endif
/* vim: set ft=lestes : */
