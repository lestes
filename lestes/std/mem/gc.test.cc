/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Unit test.

  Unit test for garbage collector functionality.
  \author pt
*/
#include <lestes/common.hh>

package(lestes);
package(std);
package(mem);

using namespace ::std;

class a: public ::lestes::std::object {
public:
	static ptr<a> create(void);
protected:
	a(void);
	virtual void gc_mark(void);
private:
	a(const a &);
	a &operator=(const a &);
};

a::a(void) {
}

void a::gc_mark(void)
{
	::lestes::std::object::gc_mark();
}

ptr<a> a::create(void)
{ 
	return new a();
}

class aa: public a {
public:
	static ptr<aa> create(void);
protected:
	aa(void);
	virtual void gc_mark(void);
private:
	aa(const aa &);
	aa &operator=(const aa &);
};

aa::aa(void) {
}

void aa::gc_mark(void)
{
	a::gc_mark();
}

ptr<aa> aa::create(void)
{ 
	return new aa();
}

class b: public ::lestes::std::object {
public:
	static ptr<b> create(const ptr<a> &a_fa, const ptr<aa> &a_faa);
	void gc_mark(void);
protected:
	b(const ptr<a> &a_fa, const ptr<aa> &a_faa);
private:
	srp<a> fa;
	srp<aa> faa;
	b(const b &);
	b &operator=(const b &);
};

b::b(const ptr<a> &a_fa, const ptr<aa> &a_faa):
	fa(a_fa),
	faa(a_faa)
{
}

ptr<b> b::create(const ptr<a> &a_fa, const ptr<aa> &a_faa)
{ 
	return new b(a_fa,a_faa);
}

void b::gc_mark(void)
{
	fa.gc_mark();
	faa.gc_mark();
	::lestes::std::object::gc_mark();
}

class c: public b {
public:
	static ptr<c> create(const ptr<a> &a_fa, const ptr<aa> &a_faa, const ptr<a> &a_ga);
	void gc_mark(void);
protected:
	c(const ptr<a> &a_fa, const ptr<aa> &a_faa, const ptr<a> &a_ga);
private:
	srp<a> ga;
	c(const c &);
	c &operator=(const c &);
};

c::c(const ptr<a> &a_fa, const ptr<aa> &a_faa, const ptr<a> &a_ga):
		b(a_fa,a_faa),
		ga(a_ga)
{
}

ptr<c> c::create(const ptr<a> &a_fa, const ptr<aa> &a_faa, const ptr<a> &a_ga)
{ 
	return new c(a_fa,a_faa,a_ga);
}

void c::gc_mark(void)
{
	ga.gc_mark();
	b::gc_mark();
}

class d: public ::lestes::std::object {
public:
	static ptr<d> create(const ptr<c> &a_fc, const ptr<c> &a_gc);
	void gc_mark(void);
protected:
	d(const ptr<c> &a_fc, const ptr<c> &a_gc);
private:
	srp<c> fc;
	srp<c> gc;
};

d::d(const ptr<c> &a_fc, const ptr<c> &a_gc):
	fc(a_fc),
	gc(a_gc)
{
}

ptr<d> d::create(const ptr<c> &a_fc, const ptr<c> &a_gc)
{
	return new d(a_fc, a_gc);
}

void d::gc_mark(void)
{
	fc.gc_mark();
	gc.gc_mark();
	::lestes::std::object::gc_mark();
}

class e: public ::lestes::std::object {
public:
	static ptr<e> create(const ptr<e> &a_link);
	void link_set(const ptr<e> &a_link);
	void gc_mark(void);
protected:
	e(const ptr<e> &a_link);
private:
	srp<e> link;
};

e::e(const ptr<e> &a_link):
	link(a_link) 
{
}

ptr<e> e::create(const ptr<e> &a_link)
{
	return new e(a_link);
}

void e::link_set(const ptr<e> &a_link)
{
	link = a_link;
}

void e::gc_mark(void)
{
	link.gc_mark();
	::lestes::std::object::gc_mark();
}

//! helper to assert count of live root pointers and keystones
#define live(r,k) lassert(gc::live_roots() == (r + static_r) && gc::live_keystones() == (k + static_k))

void gc_test(void)
{
	// sweep the objects created before the test
	gc::run();
	
	// memorize static roots and keystones
	ulint static_r = gc::live_roots();
	ulint static_k = gc::live_keystones();

	// jtbs
	live(0,0);
	gc::run();
	live(0,0);
	
	// single ptr
	{
		ptr<a> a1;
		live(1,0);
		gc::run();
		live(1,0);
	}
	live(0,0);
	
	// single object
	{
		ptr<a> a2(a::create());
		live(1,1);
		gc::run();
		live(1,1);
		a2 = NULL;
		live(1,1);
		gc::run();
		live(1,0);
	}
	live(0,0);
	
	// more objects for one ptr
	{
		ptr<a> a3(a::create());
		live(1,1);
		a3 = a::create();
		a3 = a::create();
		a3 = a::create();
		live(1,4);
		gc::run();
		live(1,1);
	}
	live(0,1);
	gc::run();
	live(0,0);

	// one object for more ptr
	{
		ptr<a> a4(a::create());
		live(1,1);
		{
			ptr<a> a5(a4);
			live(2,1);
			a4 = NULL;
			gc::run();
			live(2,1);
		}
		live(1,1);
		gc::run();
		live(1,0);
	}
	live(0,0);

	// descendant in ancestor ptr
	{
		ptr<a> a6(aa::create());
		live(1,1);
		gc::run();
		live(1,1);
		ptr<a> a7(a6);
		live(2,1);
		a6 = NULL;
		gc::run();
		live(2,1);
		a7 = NULL;
		gc::run();
		live(2,0);
	}
	live(0,0);

	// one level empty structure
	{
		ptr<b> b1(b::create(NULL,NULL));
		live(1,1);
		gc::run();
		live(1,1);
		b1 = NULL;
		gc::run();
		live(1,0);
	}
	live(0,0);

	// one level structure
	{
		ptr<b> b1(b::create(a::create(),aa::create()));
		live(1,3);
		gc::run();
		live(1,3);
		b1 = NULL;
		gc::run();
		live(1,0);
	}
	live(0,0);

	// one level shared structure
	{
		ptr<a> a8(a::create());
		ptr<b> b1(b::create(a8,aa::create()));
		live(2,3);
		gc::run();
		live(2,3);
		b1 = NULL;
		gc::run();
		live(2,1);
	}
	gc::run();
	live(0,0);

	// one level inherited structure
	{
		ptr<c> c1(c::create(a::create(),aa::create(),a::create()));
		live(1,4);
		gc::run();
		live(1,4);
		c1 = NULL;
		gc::run();
		live(1,0);
	}
	live(0,0);

	// two level structure
	{
		ptr<c> c2(c::create(a::create(),aa::create(),a::create()));
		ptr<c> c3(c::create(a::create(),aa::create(),a::create()));
		ptr<d> d1(d::create(c2,c3));
		live(3,9);
		gc::run();
		live(3,9);
		c2 = c3 = NULL;
		gc::run();
		live(3,9);
		d1 = NULL;
		gc::run();
		live(3,0);
	}
	live(0,0);

	// cyclic structure
	{
		ptr<e> e1(e::create(NULL));
		ptr<e> e2(e::create(e::create(e::create(e::create(e1)))));
		e1->link_set(e2);
		live(2,5);
		gc::run();
		live(2,5);
		e2 = NULL;
		gc::run();
		live(2,5);
		e1 = NULL;
		gc::run();
		live(2,0);
	}
	live(0,0);
}

end_package(mem);
end_package(std);
end_package(lestes);

int main(void)
{
	::lestes::std::mem::gc_test();
	return 0;
}
/* vim: set ft=lestes : */
