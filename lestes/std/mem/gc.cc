/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Garbage collector.

  Definition of gc class representing garbage collector.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/std/mem/gc.hh>
#include <lestes/std/mem/keystone.hh>
#include <lestes/std/mem/root_pointer.hh>

package(lestes);
package(std);
package(mem);

using namespace ::std;

/*!
  Constructor of first existing object initializes gc class.
*/
init_gc::init_gc(void)
{
	gc::init();
}

/*!
  Destructor of last existing object runs cleanup of gc class.
*/
init_gc::~init_gc(void)
{
	gc::cleanup();
}

/*!
  Counter to keep track of number of initializaton objects.
  Reaches zero only after all initialization objects are destroyed.
*/
ulint gc::initialized = 0;

/*!
  Initializes static fields of gc class.
  Prevents multiple initialization.
*/
void gc::init(void)
{
	if (initialized++) return;
	// create head of linked list
	roots = new root_pointer(false);
	keystones = NULL;
	marked = NULL;
}

/*!
  Performs cleanup of static fields of gc class.

  TMA: also cleans all remaining objects.
*/
void gc::cleanup(void)
{
	if (--initialized) return;
	run();
	delete roots;
}

/*!
  Start of doubly linked list of all root pointers.
  Initialized to point to the fake head entry.
*/
root_pointer *gc::roots;

/*!
  Start of singly linked list of all keystones.
  Initially the list is empty.
*/
keystone *gc::keystones;

/*! 
  Start of singly linked list of marked keystones.
  Initially the list is empty.
*/
keystone *gc::marked;

/*!
  Counts live root pointers.
  \return The number of live root pointers.
*/
ulint gc::live_roots(void)
{
	ulint cnt = 0;
	for (root_pointer *p = roots->next; p != roots; p = p->next) {
		cnt++;
	}
	return cnt;
}

/*!
  Counts live keysones. 
  \return The number of live keystones.
*/
ulint gc::live_keystones(void)
{
	ulint cnt = 0;
	for (keystone *p = keystones; p != NULL; p = p->keystones_next) {
		cnt++;
	}
	return cnt;
}

/*!
  Runs garbage collection, invoked in new_handler.
  Should be invoked explicitly after completing task.
*/
void gc::run(void)
{
	// initialize the global marked list
	marked = NULL;
	// walk through all root pointers and
	// insert keystones into marked list
	for (root_pointer *p = roots->next; p != roots; p = p->next) {
		keystone *key = p->pointer_get();
		if (key) key->enqueue();
	}
	
	// mark all levels of keystones
	while (marked != NULL) {
		keystone *old = marked;
		// initialize the global list for new marking
		marked = NULL;
		while (old) {
			// mark directly reachable keystones
			old->gc_mark();
			old = old->marked_next;
		}
	}
	
	keystone *key = keystones;

	// initialize the linked list before new connecting
	keystones = NULL;
	
	// walk through all keystones and sweep unmarked
	while (key != NULL) {
		// save the pointer to next
		keystone *tmp = key->keystones_next;
		// after sweep_object, obj may be invalid
		key->sweep();
		// restore pointer to next
		key = tmp;
	}
}

end_package(mem);
end_package(std);
end_package(lestes);
/* vim: set ft=lestes : */
