/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__std__mem___gc_hh___included
#define lestes__std__mem___gc_hh___included
/*! \file
  \brief Garbage collector. 
  
  Declaration of gc class representing garbage collector.
  \author pt
*/

#include <lestes/package.hh>
#include <lestes/std/data_types.hh>

package(lestes);
package(std);
package(mem);

// forward declarations to avoid cycle
class keystone;
class root_pointer;

/*!
  \brief Initializer for gc.

  Initializes static fields of gc class, before they are used.
  Ensures that order of dynamic initialization will not be broken.
*/ 
class init_gc {
public:
	//! Initializes gc static fields once.
	init_gc(void);
	//! Performs cleanup to gc static fields once.
	~init_gc(void);
private:
	//! Hides copy constructor.
	init_gc(const init_gc &);
	//! Hides assignment operator.
	init_gc &operator=(const init_gc &);
};

/*!
  Static initializer object in each translation unit.
  Ensures the proper order of initialization.
*/
static init_gc initializer_gc;

/*!
  \brief Garbage collector.

  Provides garbage collector functionality.
*/
class gc {
public:
	//! Returns number of live root pointers.
	static ulint live_roots(void);
	//! Returns number of live keystones.
	static ulint live_keystones(void);
	//! Runs garbage collection.
	static void run(void);
private:
	//! Initializer accesses gc private methods.
	friend class init_gc;
	//! Initializes static variables.
	static void init(void);
	//! Performs cleanup of static variables.
	static void cleanup(void);
	//! Guard flag to prevent multiple initialization.
	static ulint initialized;
	//! Root pointer accesses gc private fields for speed.
	friend class root_pointer;
	//! Keystone accesses gc private fields for speed.
	friend class keystone;
	//! Doubly linked list of all root pointers.
	static root_pointer *roots;
	//! Singly linked list of all keystones.
	static keystone *keystones;
	//! Singly linked list of marked keystones.
	static keystone *marked;
	//! Hides copy constructor.
	gc(const gc &);
	//! Hides assignment operator.
	gc &operator=(const gc &);
};

end_package(mem);
end_package(std);
end_package(lestes);
#endif
/* vim: set ft=lestes : */
