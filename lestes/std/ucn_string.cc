/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief String of ucn characters.

  Declaration of standard string template specialized for ucn character type.
*/

#include <lestes/std/ucn_string.hh>
#include <ostream>
#include <iomanip>
#include <sstream>

package(lestes);
package(std);

/*!
  Converts ucn string, escapes extended characters with backslash u or U.
  Any external characters are considered to have host encoding.
  \return Host string with representation of the ucn string.
*/
lstring ucn_string::to_host_string(void) const
{
	hchar c;
	ucn u;
	ulint x;

	::std::stringbuf sb;
	::std::ostream os(&sb);
	
	for (ucn_string::const_iterator it = begin(), last = end(); it != last; ++it) {
		u = *it;

		if (character::is_external(u)) {
			c = static_cast<hchar>(character::extract_value(u));
			os << c;
		} else if (character::is_basic(u)) {
			c = character::to_host(u);
			os << c;
		} else {
			x = character::extract_value(u);
			if (x <= 0xFFFF) {
				os << "\\u" << ::std::hex << ::std::setfill('0') << ::std::setw(4) << x;
			} else {
				os << "\\U" << ::std::hex << ::std::setfill('0') << ::std::setw(8) << x;
			}
		}
	}

	return sb.str();
}

/*!
  Prints ucn string to stream, escapes extended characters with backslash u or U.
  Any external characters are considered to have host encoding.
  \param os  The stream to write to.
  \param us  The string to print.
  \todo pt remove
*/
::std::ostream& operator<<(::std::ostream &os, const ucn_string &us)
{
	return os << us.to_host_string();
}

end_package(std);
end_package(lestes);

/* vim: set ft=lestes : */
