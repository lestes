/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__std___ostream_wrapper_hh___included
#define lestes__std___ostream_wrapper_hh___included

/*! \file
  \brief Wrapper for ostream.
  
  Declaration of ostream_wrapper class wrapping ostream object.
  \author pt
*/
#include <lestes/common.hh>
#include <iosfwd>

package(lestes);
package(std);

/*!
  \brief Wrapper for ostream.
  
  Represents output stream wrapper, with ownership policy of the stream.
*/
class ostream_wrapper: public object {
public:
	typedef ::std::ostream *stream_type;
	//! Finalizes the object.
	virtual ~ostream_wrapper(void);
	//! Releases the stream.
	void release(void);
	//! Returns underlying stream.
	stream_type stream_get(void) const;
	//! Returns the ownership flag.
	bool owned_get(void) const;
	//! Returns the wrapper.
	static ptr<ostream_wrapper> create(stream_type a_stream, bool a_owned);
protected:
	//! Creates the wrapper
	ostream_wrapper(stream_type a_stream, bool owned);
private:
	//! The wrapped ostream.
	stream_type stream;
	//! Ownership flag.
	bool owned;
	//! Hides copy constructor.
	ostream_wrapper(const ostream_wrapper &);
	//! Hides assignment operator.
	ostream_wrapper &operator=(const ostream_wrapper &);
};

end_package(std);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
