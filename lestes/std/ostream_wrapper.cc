/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Wrapper for ostream.
  
  Definition of ostream_wrapper class wrapping ostream object.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/std/ostream_wrapper.hh>
#include <ostream>

package(lestes);
package(std);

using namespace ::std;

/*!
  Creates the wrapper.
  \pre a_stream != NULL
  \param a_stream  The stream to wrap.
  \param a_owned  The ownership flag.
*/
ostream_wrapper::ostream_wrapper(stream_type a_stream, bool a_owned):
	stream(checked(a_stream)),
	owned(a_owned)
{
}

/*!
  Finalizes the object.
  Releases the stream if owned.
*/
ostream_wrapper::~ostream_wrapper(void)
{
	release();
}

/*!
  Releases the stream if it was owned.
  \post stream_get() == NULL
*/
void ostream_wrapper::release(void)
{
	if (owned) delete stream;
	// set to NULL anyway
	stream = NULL;
}

/*!
  Returns the ownership.
  \return true  If the stream is owned by the wrapper.
*/
bool ostream_wrapper::owned_get(void) const
{
	return owned;
}

/*!
  Returns the wrapped stream.
  \pre release() was not called (stream_get() != NULL)
  \return The stream.
*/
ostream_wrapper::stream_type ostream_wrapper::stream_get(void) const
{
	lassert2(stream,"The stream is invalid because release() was already called.");
	return stream;
}

/*!
  Creates the wrapper.
  \pre a_stream != NULL
  \pre a_stream is allocated via new when a_owned is true
  \param a_stream  The stream to wrap.
  \param a_owned  The ownership flag.
  \return The new wrapper of a_stream.
*/
ptr<ostream_wrapper> ostream_wrapper::create(stream_type a_stream, bool a_owned)
{
	return new ostream_wrapper(a_stream,a_owned);
}

end_package(std);
end_package(lestes);

/* vim: set ft=lestes : */
