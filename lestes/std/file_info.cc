/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Source file information.

  Definition of file_info class representing information about source file.
*/
#include <lestes/common.hh>
#include <lestes/equality.hh>
#include <lestes/std/file_info.hh>
#include <lestes/std/source_location.hh>

package(lestes);
package(std);

using namespace ::std;

/*!
  Creates the object, initializes with name and origin.
  \param a_name  The name of the file.
  \param a_origin  The origin of inclusion.
*/
file_info::file_info(const name_type &a_name, const ptr<source_location> &a_origin):
	name(a_name),
	origin(a_origin)
{
}

/*!
 * Standard marking routine, called by the garbage collector.
 */
void file_info::gc_mark()
{
	origin.gc_mark();
	object::gc_mark();
}

/*!
  Returns name of the file.
  \return The name of the file.
*/
file_info::name_type file_info::name_get(void) const
{
	return name;
}

/*!
  Returns origin of inclusion.
  \return The location of the include command for this source file.
*/
ptr<source_location> file_info::origin_get(void) const
{
	return origin;
}

/*!
  Tests equality to other file info.
  \param other  The other object to compare to.
*/
bool file_info::equals(const ptr<file_info> &other) const
{
	return other && is_equal(name,other->name_get()) && is_equal(origin,other->origin_get());
}

/*!
  Returns new instance, initializes with name and origin.
  \param a_name  The name of the file.
  \param a_origin  The origin of inclusion.
*/
ptr<file_info> file_info::create(const name_type &a_name,
			const ptr<source_location> &a_origin)
{
	return new file_info(a_name,a_origin);
}

end_package(std);
end_package(lestes);
/* vim: set ft=lestes : */
