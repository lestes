/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/common.hh>
#include <lestes/std/backtrace.hh>
#include <fstream>

using namespace ::lestes;
using namespace ::lestes::std;

::std::ofstream * ofs = NULL;

void foo(int i) {
	if (i == 50) {
		lassert2(ofs, "The file stream shall exist.");
		*ofs << assemble_backtrace() << ::std::endl;
	}
	if (i > 600) {
		lassert2(ofs, "The file stream shall exist.");
		*ofs << assemble_backtrace() << ::std::endl;
	} else {
		foo(i+1);
	}
}
int main()
{
	::std::ofstream f("backtrace.test.txt");
	::ofs = &f;
	f << "Backtrace test\n" << ::std::endl;
	lassert2(ofs, "The file stream shall exist.");
	f << assemble_backtrace() << ::std::endl;
	foo(0);
	::ofs = NULL;
	f << "Backtrace test end." << ::std::endl;
	f.close();
}
