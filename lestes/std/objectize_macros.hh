/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__std___objectize_macros_hh___included
#define lestes__std___objectize_macros_hh___included

#include <lestes/std/objectize.hh>

/*!
 * Specializes objectize<T> for given enum type. The dumper visitor is called
 * as if the value was a lint; the actual value is static_cast to lint.
 *
 * The macro must be used inside ::lestes::std and must not be used more than
 * once for the same type, o for a type that was passed to the other macro,
 * specialize_objectize_nodump().
 *
 * Usually, you would pass fully qualified name as the type argument.
 *
 * Actually, the only thing that is implemented here is the create method. It
 * static casts the argument and returns a pointer to objectize<lint>.
 */
#define specialize_objectize_for_enum( type )					\
template<>									\
class objectize< type > : public objectize<lint> {				\
public:										\
	static ptr< objectize<lint> > create( type a_value )			\
	{									\
		return objectize<lint>::create( static_cast<lint>(a_value) );	\
	}									\
private:									\
	/*! Hide the constructor */						\
	objectize< type >();							\
}

/*!
 * Specializes objectize<T> for given type. The dumper visitor's visit_nodump()
 * is called in accept_dumper_visitor().
 *
 * The macro must be used inside ::lestes::std and must not be used more than
 * once for the same type, or for a type that was passed to the other macro,
 * specialize_objectize_for_enum().
 *
 * Usually, you would pass fully qualified name as the type argument.
 *
 * Note that The create method does not use its argument in any way. This, and
 * the fact that const reference is used, ensures that no further requirements
 * are placed on the given type. If reference was not used, the type would have
 * to publicly support copy-construction. This has a disadvantage, though: The
 * type argument cannot be a reference type. Other macro is provided for those.
 */
#define specialize_objectize_nodump( type )					\
template<>									\
class objectize< type > : public object {					\
public:										\
	static ptr< objectize< type > > create( const type & )			\
	{									\
		/* the agument is not used at all */				\
		return new objectize< type >();					\
	}									\
	virtual void accept_dumper_visitor( ptr<dumper_visitor> v )		\
	{									\
		return v->visit_nodump();					\
	}									\
protected:									\
	objectize< type >()							\
	{}									\
}

/*!
 * Specializes objectize<T> for given _reference_ type. The only difference
 * when compared to specialize_objectize_nodump is that the create method takes
 * argument of the type itself, which makes the generated specialization
 * compilable for a reference type.
 *
 * See documentation for specialize_objectize_nodump macro.
 */
#define specialize_objectize_nodump_reference( type )				\
template<>									\
class objectize< type > : public object {					\
public:										\
	static ptr< objectize< type > > create( const type )			\
	{									\
		/* the agument is not used at all */				\
		return new objectize< type >();					\
	}									\
	virtual void accept_dumper_visitor( ptr<dumper_visitor> v )		\
	{									\
		return v->visit_nodump();					\
	}									\
protected:									\
	objectize< type >()							\
	{}									\
}

#endif	// lestes__std___objectize_macros_hh___included
