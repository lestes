/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__std___ptr_bodies_hh___included
#define lestes__std___ptr_bodies_hh___included

/*! \file
  \brief Smart pointer template bodies.

  Method bodies for the ptr template. Had to be separated from ptr.hh which
  contains the declarations.
  \author pt
*/
#include <lestes/std/ptr.hh>
#include <lestes/std/lassert.hh>

package(lestes);
package(std);

/*!
  Creates ptr, initializes with pointer.
  \post pointer_get() == a_pointer
  \param a_pointer  The initialization value.
*/
template <typename T>
inline ptr<T>::ptr(T *a_pointer):
	root_pointer()
{
	pointer_set(a_pointer);
}

/*!
  Creates ptr, initializes with pointer.
  \post pointer_get() == a_pointer
  \param U  The type of the initializer pointer.
  \param a_pointer  The initialization value.
*/
template <typename T>
template <typename U>
inline ptr<T>::ptr(U *a_pointer):
	root_pointer()
{
	pointer_set(a_pointer);
}

/*!
  Creates ptr, initializes with NULL pointer.
  \post pointer_get() == NULL
*/
template <typename T>
inline ptr<T>::ptr(void):
	root_pointer()
{
	pointer_set(NULL);
}

/*!
  Creates ptr, initializes with ptr to the same type.
  Workaround to avoid implicit copy constructor creation.
  \post pointer_get() == other.pointer_get()
  \param other  The ptr to initialize with.
*/
template <typename T>
inline ptr<T>::ptr(const ptr<T> &other):
	root_pointer()
{
	pointer_set(other.pointer_get());
}

/*!
  Creates ptr, initializes with ptr.
  \post pointer_get() == other.pointer_get()
  \param U  The type of the initializer ptr.
  \param other  The ptr to initialize with.
*/
template <typename T>
template <typename U>
inline ptr<T>::ptr(const ptr<U> &other):
	root_pointer()
{
	pointer_set(other.pointer_get());
}

/*!
  Creates ptr, initializes with srp.
  \post pointer_get() == other.pointer_get()
  \param U  The type of the initializer srp.
  \param other  The srp to initialize with.
*/
template <typename T>
template <typename U>
inline ptr<T>::ptr(const srp<U> &other):
	root_pointer()
{
	pointer_set(other.pointer_get());
}

/*!
  Destroys ptr.
*/
template <typename T>
inline ptr<T>::~ptr(void)
{
}

/*!
  Returns the pointer.
  \return  The contained pointer.
*/
template <typename T>
inline T *ptr<T>::operator->(void) const
{
	T * result = pointer_get();
	lassert2( result, "Dereferencing NULL pointer." );
	return result;
}

/*!
  Dereferences the pointer.
  \return  The dereferenced pointer.
*/
template <typename T>
inline T &ptr<T>::operator *(void) const
{
	T * presult = pointer_get();
	lassert2( presult, "Dereferencing NULL pointer." );
	return *presult;
}

/*!
  Assigns pointer.
  \param a_pointer  The pointer to assign.
  \return  This ptr after the assignment.
*/
template <typename T>
inline ptr<T> &ptr<T>::operator=(T *a_pointer)
{
	pointer_set(a_pointer);
	return *this;
}

/*!
  Assigns pointer.
  \param U  The type of the assigned pointer.
  \param a_pointer  The pointer to assign.
  \return  This ptr after the assignment.
*/
template <typename T>
template <typename U>
inline ptr<T> &ptr<T>::operator=(U *a_pointer)
{
	pointer_set(a_pointer);
	return *this;
}

/*!
  Assigns ptr of the same type.
  Workaround to avoid implicit assignment operator creation.
  \param other  The ptr to assign.
  \return  This ptr after assignment.
*/
template <typename T>
inline ptr<T> &ptr<T>::operator=(const ptr<T> &other)
{
	pointer_set(other.pointer_get());
	return *this;
}

/*!
  Assigns ptr.
  \param U  The type of the assigned ptr.
  \param other  The ptr to assign.
  \return  This ptr after assignment.
*/
template <typename T>
template <typename U>
inline ptr<T> &ptr<T>::operator=(const ptr<U> &other)
{
	pointer_set(other.pointer_get());
	return *this;
}

/*!
  Assigns srp.
  \param U  The type of the assigned srp.
  \param other  The other srp to assign.
  \return  This ptr after assignment.
*/
template <typename T>
template <typename U>
inline ptr<T> &ptr<T>::operator=(const srp<U> &other)
{
	pointer_set(other.pointer_get());
	return *this;
}
#ifdef LESTES_OLD_POINTER_CONDITION
/*!
  Tests NULL pointer.
  \return  true if the pointer is NULL.
*/  
template <typename T>
inline ptr<T>::operator bool(void) const
{
	return pointer_get();
}
#else
/*!
  Tests NULL pointer.
  \return  Condition convertible to true if the pointer is NULL.
*/  
template <typename T>
inline ptr<T>::operator condition*(void) const
{
	static condition c;
	return pointer_get() ? &c : NULL;
}
#endif

/*!
  Tests non NULL pointer.
  \return  true if the pointer is not NULL.
*/
template <typename T>
inline bool ptr<T>::operator!(void) const
{
	return !pointer_get();
}

/*!
  Compares to pointer.
  \return  true if both pointers are equal.
*/
template <typename T>
inline bool ptr<T>::operator==(T *a_pointer) const
{
	return pointer_get() == a_pointer;
}

/*!
  Compares to pointer.
  \param U  The type of the compared pointer.
  \return  true if both pointers are equal.
*/
template <typename T>
template <typename U>
inline bool ptr<T>::operator==(U *a_pointer) const
{
	return pointer_get() == a_pointer;
}

/*!
  Compares to NULL pointer. Workaround to avoid implicit bool conversion.
  \return  true if both pointers are equal.
*/
template <typename T>
inline bool ptr<T>::operator==(::std::ptrdiff_t 
#ifdef LESTES_STRICT_CHECKING      
		a_pointer
#endif      
		) const
{
#ifdef LESTES_STRICT_CHECKING
	// test whether really comparing with NULL
	lassert(!a_pointer);
#endif
	return !pointer_get();
}

/*!
  Compares to ptr.
  \param U  The type of the compared pointer.
  \return  true if both pointers are equal.
*/
template <typename T>
template <typename U>
inline bool ptr<T>::operator==(const ptr<U> &other) const
{
	return pointer_get() == other.pointer_get();
}

/*!
  Compares to other srp.
  \param U  The type of the compared srp.
  \return  true if both pointers are equal.
*/
template <typename T>
template <typename U>
inline bool ptr<T>::operator==(const srp<U> &other) const
{
	return pointer_get() == other.pointer_get();
}

/*!
  Compares to pointer.
  \return  false if both pointers are equal.
*/
template <typename T>
inline bool ptr<T>::operator!=(T *a_pointer) const
{
	return pointer_get() != a_pointer;
}

/*!
  Compares to pointer.
  \param U  The type of the compared pointer.
  \return  false if both pointers are equal.
*/
template <typename T>
template <typename U>
inline bool ptr<T>::operator!=(U *a_pointer) const
{
	return pointer_get() != a_pointer;
}

/*!
  Compares to NULL pointer. Workaround to avoid implicit bool conversion.
  \return  false if both pointers are equal.
*/
template <typename T>
inline bool ptr<T>::operator!=(::std::ptrdiff_t a_pointer) const
{
#ifdef LESTES_STRICT_CHECKING
	// test whether really comparing with NULL
	lassert(!a_pointer);
#endif
	return pointer_get();
}

/*!
  Compares to ptr.
  \param U  The type of the compared pointer.
  \return  false if both pointers are equal.
*/
template <typename T>
template <typename U>
inline bool ptr<T>::operator!=(const ptr<U> &other) const
{
	return pointer_get() != other.pointer_get();
}

/*!
  Compares to other srp.
  \param U  The type of the compared srp.
  \return  false if both pointers are equal.
*/
template <typename T>
template <typename U>
inline bool ptr<T>::operator!=(const srp<U> &other) const
{
	return pointer_get() != other.pointer_get();
}

/*!
  Performs dynamic cast on pointers.

  When the dynamic type of the pointee is not the requested one or one derived from it,
  the function fails with an assertion. Null pointers do not make the function fail.

  \param U  The type of the target ptr.
  \return  New ptr initialized with this ptr pointer.
*/
template <typename T>
template <typename U>
inline ptr<U> ptr<T>::dncast(void) const
{
	register T * source = pointer_get();
	register U * result = dynamic_cast<U *>(source);
	// crash when dncast failed (returned NULL given non-NULL argument)
	lassert2( !source || result, "Dncasting to wrong type." );
	return ptr<U>(result);
}

/*!
  Returns the pointer.
  \return  The contained pointer.
*/
template <typename T>
inline T *ptr<T>::pointer_get(void) const
{
	return static_cast<T *>(root_pointer::pointer_get());
}

/*!
  Compares the pointer with a given one.
  \param b  The pointer to compare with.
*/
template <typename T>
inline bool ptr<T>::operator < (const ptr<T> &b) const
{
	return pointer_get() < b.pointer_get();
}

/*!
  Sets the pointer.
  \param a_pointer  The pointer to set.
*/
template <typename T>
inline void ptr<T>::pointer_set(T *a_pointer)
{
	root_pointer::pointer_set(a_pointer);
}

end_package(std);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
