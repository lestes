/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/common.hh>
#include <lestes/std/reflect.hh>
#include <lestes/std/dumper_visitor.hh>
#include <lestes/std/objectize.hh>
#include <lestes/std/objectize_macros.hh>
#include <lestes/std/map.hh>
#include <lestes/std/set.hh>
#include <lestes/std/list.hh>
#include <lestes/std/pair.hh>
#include <lestes/std/vector.hh>
#include <fstream>
#include <iostream>
#include <lestes/std/dumper.hh>

#define LENGTHOFARRAY( ar ) (sizeof(ar)/sizeof(ar[0]))

package(lestes);
package(std);

class middle_object : public object {
private:
	static ptr<reflection_list> reflection;

protected:
	lint i;
	middle_object( int a_i ) : i(a_i)
	{}
public:
	virtual ptr<reflection_list> reflection_get() const
	{
		if (!reflection) {
			typedef class_reflection::field_metadata md;
			typedef class_reflection::field_metadata_list mdlist;
			ptr<mdlist> mdl = mdlist::create();
			mdl->push_back( md::create( "i", "lint" ) );
			// append our one to a copy of the base class's list
			reflection = reflection_list::create( object::reflection_get() );
			reflection->push_back( class_reflection::create( "middle_object", mdl ) );
		}
		return reflection;
	}

	virtual ptr<field_list_list> field_values_get() const
	{
		ptr<field_list_list> result = object::field_values_get();
		result->push_back( value_list::create() );
		result->back()->push_back( objectize<int>::create(i) );
		return result;
	}
	static ptr<middle_object> create( int a_i )
	{
		return new middle_object(a_i);
	}
};

ptr<object::reflection_list> middle_object::reflection;

class dumped_object : public middle_object {
private:
	static ptr<reflection_list> reflection;

protected:
	srp<object> o;
	srp< ::lestes::std::list< srp<object> > > c;
	dumped_object() : middle_object(0)
	{}
	dumped_object( int a_i, ptr<object> a_o, ptr< list< srp< object> > > a_c ) : middle_object(a_i), o(a_o), c(a_c)
	{}

public:
	virtual ptr<reflection_list> reflection_get() const
	{
		if (!reflection) {
			typedef class_reflection::field_metadata md;
			typedef class_reflection::field_metadata_list mdlist;
			ptr<mdlist> mdl = mdlist::create();
			mdl->push_back( md::create( "o", "object" ) );
			mdl->push_back( md::create( "c", "list&lt;dumped_object&gt;" ) );
			// create a copy of base class's list and append our own item
			reflection = reflection_list::create( middle_object::reflection_get() );
			reflection->push_back( class_reflection::create( "dumped_object", mdl ) );
		}
		return reflection;
	}
	virtual ptr<field_list_list> field_values_get() const
	{
		ptr<field_list_list> result = middle_object::field_values_get();
		result->push_back( value_list::create() );
		result->back()->push_back(o);
		result->push_back( value_list::create() );
		result->back()->push_back(c);
		return result;
	}
	static ptr<dumped_object> create()
	{
		return new dumped_object();
	}
	static ptr<dumped_object> create( int a_i, ptr<dumped_object> a_o, ptr< ::lestes::std::list< srp<object> > > a_c )
	{
		return new dumped_object( a_i, a_o, a_c );
	}
};

ptr<object::reflection_list> dumped_object::reflection;

enum test_enum {
	EN_1 = -10,
	EN_2 = 30,
	EN_3,
	EN_4 = EN_1 + 15,
};

specialize_objectize_for_enum(test_enum);

end_package(std);
end_package(lestes);

using namespace ::lestes::std;
using ::std::cout;
using ::std::cerr;
using ::std::endl;
using ::std::ostream;
using ::std::ofstream;
using ::std::ostringstream;

typedef map< srp<object>, ulint > map_type;

package(lestes);
package(std);

// declare the iterator as simple, non-dumpable type
specialize_objectize_nodump( ::map_type::iterator );
specialize_objectize_nodump_reference( lint & );

end_package(std);
end_package(lestes);

int main()
{
	typedef list< srp<object> > olist;
	ptr<olist> li = olist::create();
	li->push_back( objectize<lint>::create(5) );
	li->push_back( objectize<ulint>::create(6) );
	ptr<dumped_object> o1 = dumped_object::create( 1, dumped_object::create(0,NULL,NULL), li );
	li->push_back( o1 );
	li->push_back( NULL );

	typedef list<bool> blist;
	ptr<blist> bl = blist::create();
	bl->push_back(true);
	bl->push_back(true);
	bl->push_back(false);
	li->push_back(bl);

	typedef set<lint> liset;
	ptr<liset> s = liset::create();
	s->insert(5);
	s->insert(3);
	s->insert(-1);

	typedef map< test_enum, srp<object> > mt;
	ptr<mt> m = mt::create();
	(*m)[EN_3] = m;
	(*m)[EN_1] = NULL;
	(*m)[EN_4] = s;

	typedef vector< srp<object> > vt;
	ptr<vt> v = vt::create();
	v->push_back(m);
	v->push_back(NULL);
	v->push_back(v);
	// the following line tests that reference types as arguments to
	// objectize template are working
	lint i = 5;
	v->push_back( objectize<int &>::create(i) );

	li->push_back(v);

	// test the nondumpable iterator ;)
	typedef map_type::iterator nd_type;
	ptr< list<nd_type> > lnd = list<nd_type>::create();
	lnd->push_back( map_type::create()->begin() );
	li->push_back(lnd);

	// test barrier
	ptr<object> o2 = dumped_object::create( 77, dumped_object::create(88,NULL,NULL), NULL );
	o2->dump_barrier_set( true );
	li->push_back( o2 );

	ofstream f("dumper.test.xml");

	readable_dumper::dump( f, o1 ) << "<!-- end of readable dump -->" << endl;
	dumper::dump( f, o1 ) << "<!-- end of first flat dump -->" << endl;

	li->push_front( vt::create() );
	dumper::dump( f, o1, false );

	f.close();

	return 0;
}
