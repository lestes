/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__std___check_hh___included
#define lestes__std___check_hh___included

/*! \file
  The checked() macro and similar ones.
  \author Rudo
*/

#include <lestes/std/lassert.hh>

/*!
 * \brief Check value for non-NULL-ness and return it
 *
 */
//TODO
#define checked(x) (lassert2((x),"Checked pointer is NULL."),(x))
//#define checked(x) (x)

/*!
 * \brief Checks given string for non-emptiness and return it.
 *
 * \author Rudo
 */
#define checked_ucn_string(x) (lassert2( !((x).empty()), "Checked ucn_string is empty." ),(x))

/*!
 \brief helper macro for specialized checking, which is permisive for the first time only

 \param ward  The name of the protective boolean variable.
 \param expr  The expression to check.

 \author TMA
 */
#define one_shot_unchecked(ward, expr) \
	(lassert2(!(ward) || (expr), \
		"Checked pointer is NULL, and this is not the first attempt " \
		"to set the checked value."),\
	 ((ward) = true), \
	 (expr))

//#define one_shot_unchecked(ward,expr) (expr)

#endif	// lestes__std___check_hh___included
/* vim: set ft=lestes : */
