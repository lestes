/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Unit test.

  Unit test of class file_info.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/equality.hh>
#include <lestes/std/file_info.hh>
#include <lestes/std/source_location.hh>

package(lestes);
package(std);

using namespace ::std;

/*!
  \brief Tests file_info class.

  Performs testing of file_info class.
*/
void file_info_test(void)
{
	ptr<file_info> fr = file_info::create("foobar.h",NULL);

	lassert(is_equal(fr->name_get(),"foobar.h"));
	lassert(is_equal(fr->origin_get(),ptr<source_location>(NULL)));

	ptr<source_location> loc = source_location::create(fr,1,1);
	
	ptr<file_info> gr = file_info::create("barfoo.h",loc);

	lassert(is_equal(gr->name_get(),"barfoo.h"));
	lassert(is_equal(gr->origin_get(),loc));
}

end_package(std);
end_package(lestes);

/*!
  \brief Main function.

  Runs the unit test in different namespace.
*/
int main(void)
{
	::lestes::std::file_info_test();
	return 0;
}
/* vim: set ft=lestes : */
