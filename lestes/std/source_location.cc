/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Token location.

  Definition of location class representing token location.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/equality.hh>
#include <lestes/std/source_location.hh>
#include <lestes/std/file_info.hh>
#include <lestes/std/reflect.hh>
#include <lestes/std/list.hh>
#include <lestes/std/pair.hh>

package(lestes);
package(std);

using namespace ::std;

/*!
  Creates new object, initializes with file information and position.
  \pre a_file != NULL
  \param a_file  The file information.
  \param a_line  The line in the file.
  \param a_column  The column on the line.
  \param a_order  The order in the translation unit.
*/
source_location::source_location(const ptr<file_info> &a_file,
		ulint a_line, ulint a_column, ulint a_order):
	file((lassert(a_file), a_file)),
	line(a_line),
	column(a_column),
	order(a_order)
{
}

/*!
 * Standard marking routine, called by the garbage collector.
 */
void source_location::gc_mark(void)
{
	file.gc_mark();
	object::gc_mark();
}

/*!
  Returns file information.
  \return The file information.
*/
ptr<file_info> source_location::file_get(void) const
{
	return file;
}

/*!
  Returns position in the file.
  \return The line number.
*/
ulint source_location::line_get(void) const
{
	return line;
}

/*!
  Returns position on the line.
  \return The column number.
*/
ulint source_location::column_get(void) const
{
	return column;
}

/*!
  Tests equality to other source_location.
  \param rhs  The location to compare to.
*/
bool source_location::equals(const ptr<source_location> &rhs) const
{
	return rhs && is_equal(file,rhs->file_get()) &&
	  is_equal(line,rhs->line_get()) &&
	  is_equal(column,rhs->column_get());
}

/*!
  Returns order in translation unit.
  \return  The order.
*/
ulint source_location::order_get() const
{
	return order;
}

/*!
  Clones the location with new order.
  \param a_order  The order for the new location.
  \return The location with changed order.
*/
ptr<source_location> source_location::clone_order(ulint a_order) const
{
	return new source_location(file_get(),line_get(),column_get(),a_order);
}

/*!
  Clones the location with new file information.
  \param a_file  The file_information for the new location.
  \return The location with changed file information.
*/
ptr<source_location> source_location::clone_file(const ptr<file_info> &a_file) const
{
	return new source_location(a_file,line_get(),column_get(),order_get());
}

/*!
  Returns new instance, initializes with file information and position.
  \pre a_file != NULL
  \param a_file  The file information.
  \param a_line  The line in the file.
  \param a_column  The column on the line.
  \return The location.
*/
ptr<source_location> source_location::create(const ptr<file_info> &a_file,
		ulint a_line, ulint a_column)
{
	return new source_location(a_file,a_line,a_column,0);
}

/*!
  Returns new instance, initializes with file information, position and order.
  \pre a_file != NULL
  \param a_file  The file information.
  \param a_line  The line in the file.
  \param a_column  The column on the line.
  \param a_order  The order in the translation unit.
  \return The location.
*/
ptr<source_location> source_location::create(const ptr<file_info> &a_file,
		ulint a_line, ulint a_column, ulint a_order)
{
	return new source_location(a_file,a_line,a_column,a_order);
}

/*!
  Returns location at the beginning of the translation unit.
  \return The zero location.
*/
ptr<source_location> source_location::zero(void)
{
	if (!zero_instance) {
		zero_instance = new source_location(file_info::create("<internal>",NULL),1,1,0);
	}
	return zero_instance;
}

/*!
  Returns reflection list for source location.
  \return The reflection list.
*/
ptr< object::reflection_list > source_location::reflection_get(void) const
{
	if (!reflection) {
		typedef class_reflection::field_metadata md;
		typedef class_reflection::field_metadata_list mdlist;
		ptr<mdlist> mdl = mdlist::create();
		mdl->push_back(md::create("file","lstring"));
		mdl->push_back(md::create("line","ulint"));
		mdl->push_back(md::create("column","ulint"));
		mdl->push_back(md::create("order","ulint"));
		reflection = reflection_list::create(object::reflection_get());
		reflection->push_back(class_reflection::create("source_location",mdl));
	}
	return reflection;
}

/*!
  Returns values of the fields.
  \return List of field values.
*/
ptr< object::field_list_list > source_location::field_values_get(void) const
{
	ptr<field_list_list> result = object::field_values_get();
	result->push_back(value_list::create());
	result->back()->push_back(objectize<lstring>::create(file_get()->name_get()));
	result->push_back(value_list::create());
	result->back()->push_back(objectize<ulint>::create(line_get()));
	result->push_back(value_list::create());
	result->back()->push_back(objectize<ulint>::create(column_get()));
	result->push_back(value_list::create());
	result->back()->push_back(objectize<ulint>::create(order_get()));
	return result;
}

/*!
  The zero location instance at the beginning of the translation unit.
*/
ptr<source_location> source_location::zero_instance = zero_instance;

/*!
  Reflection list for source location.
*/
ptr<object::reflection_list> source_location::reflection = reflection;

end_package(std);
end_package(lestes);
/* vim: set ft=lestes : */
