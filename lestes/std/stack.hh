/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__std___stack_hh___included
#define lestes__std___stack_hh___included

#include <lestes/common.hh>
#include <lestes/std/reflect.hh>
#include <lestes/std/objectize.hh>
#include <lestes/std/data_types.hh>
#include <lestes/std/list.hh>
#include <lestes/std/pair.hh>
#include <lestes/std/lassert.hh>
//#include <lestes/std/collection_refl.hh>
#include <stack>
#include <iterator>
#include <algorithm>

/*! \file
  \brief Collectible ::std::stack
  \author TMA

 */

package(lestes);
package(std);

/*!
  \brief Collectible stack of entities

  An analogon for STL stack class tailored to be compatible with our garbage
  collector. Includes namely marking routine and factory method.

  In addition to ::std::stack an iterator is provided.  The sense of the
  iteration is reversed with respect to the underlying container.  Thus the
  ``normal'' iteration starts at the stack top, which is the back of the
  underlying container.

 */
template <class T, class Container = list<T> >
class stack : public object {
public:
	typedef typename Container::value_type value_type;
	typedef typename Container::size_type size_type;
	typedef Container container_type;
	typedef typename Container::const_reverse_iterator const_iterator;
	typedef typename Container::reverse_iterator iterator;
	typedef typename Container::const_iterator const_reverse_iterator;
	typedef typename Container::iterator reverse_iterator;

	static ptr < stack < T > > create(ptr < Container > container = Container::create())
	{ return new stack < T > (container); }
protected:
	srp < Container > c;
	/*explicit*/ stack<T, Container >(ptr < Container > container)
		: c(container) {}
private:
	static ptr < object::reflection_list > reflection;
public:
	virtual ptr<object::reflection_list> reflection_get() const;
	virtual ptr<object::field_list_list> field_values_get() const;
	virtual void gc_mark(void)
	{ c.gc_mark(); return object::gc_mark(); }

	bool empty() const
	{ return c->empty(); }
	size_type size() const
	{ return c->size(); }
	value_type& top()
	{ lassert(!empty()); return c->back(); }
	const value_type& top() const
	{ lassert(!empty()); return c->back(); }
	void push(const value_type& x)
	{ c->push_back(x); }
	void pop()
	{ lassert(!empty()); c->pop_back(); }

	const_iterator begin() const
	{ return c->rbegin(); }
	iterator begin()
	{ return c->rbegin(); }
	const_iterator end() const
	{ return c->rend(); }
	iterator end()
	{ return c->rend(); }

	const_reverse_iterator rbegin() const
	{ return c->begin(); }
	reverse_iterator rbegin()
	{ return c->begin(); }
	const_reverse_iterator rend() const
	{ return c->end(); }
	reverse_iterator rend()
	{ return c->end(); }
};

template < typename T, typename C >
ptr < object::reflection_list > stack < T, C > :: reflection;

template< typename T, typename C >
ptr<object::reflection_list> stack< T, C >::reflection_get() const
{
	if (!reflection) {
		typedef class_reflection::field_metadata md;
		typedef class_reflection::field_metadata_list mdlist;
		ptr<mdlist> mdl = mdlist::create();
		mdl->push_back( md::create( "c", "Collection &lt; T &gt;" ) );
		reflection = reflection_list::create( object::reflection_get() );
		reflection->push_back( class_reflection::create( "stack_of_T", mdl ) );
	}
	return reflection;
}

template< typename T, typename C >
ptr<object::field_list_list> stack< T, C >::field_values_get() const
{
	ptr<field_list_list> result = object::field_values_get();
	result->push_back( value_list::create() );
	result->back()->push_back( c );
	return result;
}

end_package(std);
end_package(lestes);

#endif	// lestes__std___stack_hh___included
/* vim: set ft=lestes : */
