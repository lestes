/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*!
 * \file
 * \brief unit test for lestes/std/ptr.hh
 *
 * \author TMA
 * \author Rudo, added tests for pair::create( ::std::pair & )
 */
#include <lestes/common.hh> 
#include <lestes/std/pair.hh> 
#include <utility>

using namespace ::lestes::std;

int main()
{
	ptr< pair<int,int> > q0 = pair<int,int>::create( 3, 4 );
	ptr< pair<int,int> > x0 = pair<int,int>::create( 3, 4 );
	ptr< pair<int,int> > y0 = pair<int,int>::create( x0 );
	ptr< pair<int,int> > z0 = pair<int,int>::create( );

	lassert(*q0 == *x0);
	lassert(*x0 == *q0);
	lassert(*y0 == *x0);
	lassert(*y0 != *z0);

	ptr< pair< srp <object>, int > > q1 = pair< srp<object>, int >::create( NULL, 4 );
	ptr< pair< srp <object>, int > > x1 = pair< srp<object>, int >::create( NULL, 4 );
	ptr< pair< srp <object>, int > > y1 = pair< srp<object>, int >::create( x1 );
	ptr< pair< srp <object>, int > > z1 = pair< srp<object>, int >::create( );

	lassert(*q1 == *x1);
	lassert(*x1 == *q1);
	lassert(*y1 == *x1);
	lassert(*y1 != *z1);

	ptr< pair< int, srp<object> > > q2 = pair< int, srp<object> >::create( 3, NULL );
	ptr< pair< int, srp<object> > > x2 = pair< int, srp<object> >::create( 3, NULL );
	ptr< pair< int, srp<object> > > y2 = pair< int, srp<object> >::create( x2 );
	ptr< pair< int, srp<object> > > z2 = pair< int, srp<object> >::create( );

	lassert(*q2 == *x2);
	lassert(*x2 == *q2);
	lassert(*y2 == *x2);
	lassert(*y2 != *z2);

	ptr< pair< srp<object>, srp<object> > > r3 = pair< srp<object>, srp <object> >::create( x0, x1 );
	ptr< pair< srp<object>, srp<object> > > q3 = pair< srp<object>, srp <object> >::create( NULL, NULL );
	ptr< pair< srp<object>, srp<object> > > x3 = pair< srp<object>, srp <object> >::create( NULL, NULL );
	ptr< pair< srp<object>, srp<object> > > y3 = pair< srp<object>, srp <object> >::create( x3 );
	ptr< pair< srp<object>, srp<object> > > z3 = pair< srp<object>, srp <object> >::create( );

	lassert(*q3 == *x3);
	lassert(*x3 == *q3);
	lassert(*y3 == *x3);
	lassert(*y3 == *z3);

	lassert(*r3 != *q3);

	/* creating from an instance of ::std::pair
	 *
	 * WARNING: what follows is brain-damaged and redundant. it tests the thing, however ;-)
	 */
	ptr< pair<int,int> > m1 = pair<int,int>::create( ::std::make_pair(2,3) );
	ptr< pair< srp<object>, int > > m2 = pair< srp<object>, int >::create( ::std::make_pair(m1,3) );
	ptr< pair< int, srp<object> > > m3 = pair< int, srp<object> >::create( ::std::make_pair(2,m1) );
	ptr< pair< srp<object>, srp<object> > > m4 = pair< srp<object>, srp<object> >::create( ::std::make_pair(m2,m3) );


	return 0;
}
