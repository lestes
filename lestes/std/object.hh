/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__std___object_hh___included
#define lestes__std___object_hh___included

/*! \file
  Declaration of object class representing general object.
  \author pt
*/
#include <lestes/package.hh>
#include <lestes/std/mem/keystone.hh>
#include <lestes/std/ptr.hh>
#include <lestes/std/srp.hh>

package(lestes);
package(std);

template< typename T >
class list;
class dumper_visitor;
class class_reflection;

/*!
  Common ancestor of all ordinary classes, adding basic functionality.
  Cannot be instantiated, only inherited.
  \todo pt Add hash and equals methods.
*/
class object: public mem::keystone {
public:
	// there is a default virtual destructor
	//! compares two objects for equality
	virtual bool equals(ptr <object> o); 
protected:
	/*!
	  \brief creates the object

	  Creates the object.
	  Place for debugging and statistics code.

	  We had to move the definition inside the class. Inline definition outside the class 
	  was miscompiled by gcc. Completely ouf-of-line definition would be too slow. However,
	  we might have to use it one day, when the following one is miscompiled.
	*/
	object(void) : uid(0), dump_barrier(false)
	{}
private:
	//! hides copy constructor
	object(const object &);
	//! hides assignment operator
	object &operator=(const object &);
	
/* dump part follows */
public:
	typedef list< srp<object> >		value_list;
	typedef list< srp<value_list> >		field_list_list;
	typedef list< srp<class_reflection> >	reflection_list;
	virtual ptr<reflection_list> reflection_get() const;
	virtual ptr<field_list_list> field_values_get() const;
	virtual void accept_dumper_visitor( ptr<dumper_visitor> v );
	// returns unique id for this object; it does not change throughout its lifetime
	ulint uid_get();
	bool dump_barrier_get() const;
	void dump_barrier_set( bool );
private:
	static ptr<reflection_list>	reflection;
	static ulint next_uid;
	//! uid, set to zero on creation, only set on demand in uid_get
	ulint uid;
	//! when set to true, instructs the dumper not to follow pointers from this object
	bool dump_barrier;
};

end_package(std);
end_package(lestes);

#endif

