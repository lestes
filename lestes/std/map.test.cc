/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/common.hh>
#include <lestes/std/map.hh>

package(lestes);
package(std);

struct integer : object { int value; };

typedef map< int,		int		> map_ii;
typedef map< srp<integer>,	int		> map_pi;
typedef map< int,		srp<integer>	> map_ip;
typedef map< srp<integer>,	srp<integer>	> map_pp;

int map_test()
{
	ptr<map_ii> mii1 = map_ii::create();
	mii1->insert( map_ii::value_type( 0, 1 ) );
	mii1->insert( map_ii::value_type( 1, 2 ) );
	ptr<map_ii> mii2 = map_ii::create();
	mii2->insert( map_ii::value_type( 0, 1 ) );
	mii2->insert( map_ii::value_type( 1, 2 ) );
	lassert( *mii1 == *mii2 );
	mii2->insert( map_ii::value_type( 2, 3 ) );
	lassert( *mii1 != *mii2 );
	ptr<map_ii> mii3 = map_ii::create(mii2);
	lassert( *mii3 == *mii2 );

	ptr<map_pi> mpi1 = map_pi::create();
	ptr<map_ip> mip1 = map_ip::create();
	ptr<map_pp> mpp1 = map_pp::create();
	return 0;
}

end_package(std);
end_package(lestes);

extern "C"
int main()
{
	return lestes::std::map_test();
}
