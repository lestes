/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__std___map_hh___included
#define lestes__std___map_hh___included

#include <lestes/common.hh>
#include <lestes/std/collection_refl.hh>
#include <lestes/std/list.hh>
#include <lestes/std/pair.hh>
#include <map>

/*! \file
  \brief Collectible ::std::map
  \author pt
  \author Rudo

  Encapsulation for STL map class to be compatible with
  our garbage collector. Includes namely marking routine,
  factory method, and dump support.
 */

package(lestes);
package(std);

//! Unary function object, wraps ::std::pair into ::lestes::std::pair; used in map::field_values_get()
template< typename T1, typename T2 >
class unary_pair_wrapper {
public:
	ptr< pair<T1,T2> > operator() (const ::std::pair<T1,T2> &p)
	{
		return pair<T1,T2>::create(p);
	}
};

template <typename Key, typename Data, typename Compare = ::std::less<Key> >
class map : public object, public ::std::map<Key, Data, Compare>
{
public:
	static ptr< map<Key,Data,Compare> > create()
	{
		return new map();
	}
	static ptr< map<Key,Data,Compare> > create( ptr< map<Key,Data,Compare> > from )
	{
		return new map(from);
	}
	virtual ptr<reflection_list> reflection_get() const
	{
		if (!collection_refl::map_simple_simple)
			collection_refl::map_simple_simple_init( object::reflection_get() );
		return collection_refl::map_simple_simple;
	}
	virtual ptr<field_list_list> field_values_get() const
	{
		ptr<field_list_list> result = object::field_values_get();
		result->push_back( value_list::create() );
		// wrap all the pairs in lestes pairs, this is unavoidable
		transform( this->begin(), this->end(), back_inserter( *result->back() ),
				unary_pair_wrapper<Key,Data>() );
		return result;
	}
protected:
	map() : object(), ::std::map<Key,Data,Compare>()
	{}
	map( ptr< map<Key,Data,Compare> > from )
		: object(), ::std::map<Key,Data,Compare>( *checked(from) )
	{}
};


template <typename Key, typename Data >
class map< srp<Key>, srp<Data>, ::std::less< ::lestes::std::srp<Key> > > :
	public object, public ::std::map< srp<Key>, srp<Data>, ::std::less< ::lestes::std::srp<Key> > >
{
public:
	static ptr< map< srp<Key>, srp<Data>, ::std::less< srp<Key> > > > create()
	{
		return new map();
	}
	static ptr< map< srp<Key>, srp<Data>, ::std::less< srp<Key> > > > create( ptr< map< srp<Key>, srp<Data>, ::std::less< srp<Key> > > > from )
	{
		return new map(from);
	}
	virtual ptr<reflection_list> reflection_get() const
	{
		if (!collection_refl::map_srp_srp)
			collection_refl::map_srp_srp_init( object::reflection_get() );
		return collection_refl::map_srp_srp;
	}
	virtual ptr<field_list_list> field_values_get() const
	{
		ptr<field_list_list> result = object::field_values_get();
		result->push_back( value_list::create() );
		// wrap all the pairs in lestes pairs, this is unavoidable
		transform( this->begin(), this->end(), back_inserter( *result->back() ),
				unary_pair_wrapper< srp<Key>, srp<Data> >() );
		return result;
	}
protected:
	map() : object(), ::std::map< srp<Key>, srp<Data>, ::std::less< srp<Key> > >()
	{}
	map( ptr< map< srp<Key>, srp<Data>, ::std::less< srp<Key> > > > from )
		: object(), ::std::map< srp<Key>, srp<Data>, ::std::less< srp<Key> > >( *checked(from) )
	{}
	void gc_mark()
	{
		object::gc_mark();
		for (typename map::iterator i = this->begin(); i != this->end(); ++i) {
			i->first.gc_mark();
			i->second.gc_mark();
		}
	}
};

template <typename Key, typename Data, typename Compare >
class map< srp<Key>, srp<Data>, Compare > : public object, public ::std::map< srp<Key>, srp<Data>, Compare >
{
public:
	static ptr< map< srp<Key>, srp<Data>, Compare > > create()
	{
		return new map();
	}
	static ptr< map< srp<Key>, srp<Data>, Compare > > create( ptr< map< srp<Key>, srp<Data>, Compare > > from )
	{
		return new map(from);
	}
	virtual ptr<reflection_list> reflection_get() const
	{
		if (!collection_refl::map_srp_srp)
			collection_refl::map_srp_srp_init( object::reflection_get() );
		return collection_refl::map_srp_srp;
	}
	virtual ptr<field_list_list> field_values_get() const
	{
		ptr<field_list_list> result = object::field_values_get();
		result->push_back( value_list::create() );
		// wrap all the pairs in lestes pairs, this is unavoidable
		transform( this->begin(), this->end(), back_inserter( *result->back() ),
				unary_pair_wrapper< srp<Key>, srp<Data> >() );
		return result;
	}
protected:
	map() : object(), ::std::map< srp<Key>, srp<Data>, Compare >()
	{}
	map( ptr< map< srp<Key>, srp<Data>, Compare > > from )
		: object(), ::std::map< srp<Key>, srp<Data>, Compare >( *checked(from) )
	{}
	void gc_mark()
	{
		map::gc_mark();
		object::gc_mark();
		for (typename map::iterator i = this->begin(); i != this->end(); ++i) {
			i->first.gc_mark();
			i->second.gc_mark();
		}
	}
};


template <typename Key, typename Data >
class map< srp<Key>, Data, ::std::less< ::lestes::std::srp<Key> > > :
	public object, public ::std::map< srp<Key>, Data, ::std::less< ::lestes::std::srp<Key> > >
{
public:
	static ptr< map< srp<Key>, Data, ::std::less< srp<Key> > > > create()
	{
		return new map();
	}
	static ptr< map< srp<Key>, Data, ::std::less< srp<Key> > > > create( ptr< map< srp<Key>, Data, ::std::less< srp<Key> > > > from )
	{
		return new map(from);
	}
	virtual ptr<reflection_list> reflection_get() const
	{
		if (!collection_refl::map_srp_simple)
			collection_refl::map_srp_simple_init( object::reflection_get() );
		return collection_refl::map_srp_simple;
	}
	virtual ptr<field_list_list> field_values_get() const
	{
		ptr<field_list_list> result = object::field_values_get();
		result->push_back( value_list::create() );
		// wrap all the pairs in lestes pairs, this is unavoidable
		transform( this->begin(), this->end(), back_inserter( *result->back() ),
				unary_pair_wrapper< srp<Key>, Data >() );
		return result;
	}
protected:
	map() : object(), ::std::map< srp<Key>, Data, ::std::less< srp<Key> > >()
	{}
	map( ptr< map< srp<Key>, Data, ::std::less< srp<Key> > > > from )
		: object(), ::std::map< srp<Key>, Data, ::std::less< srp<Key> > >( *checked(from) )
	{}
	void gc_mark()
	{
		object::gc_mark();
		for (typename map::iterator i = this->begin(); i != this->end(); ++i)
			i->first.gc_mark();
	}
};

template <typename Key, typename Data, typename Compare >
class map< srp<Key>, Data, Compare > : public object, public ::std::map< srp<Key>, Data, Compare>
{
public:
	static ptr< map< srp<Key>, Data, Compare > > create()
	{
		return new map();
	}
	static ptr< map< srp<Key>, Data, Compare > > create( ptr< map< srp<Key>, Data, Compare > > from )
	{
		return new map(from);
	}
	virtual ptr<reflection_list> reflection_get() const
	{
		if (!collection_refl::map_srp_simple)
			collection_refl::map_srp_simple_init( object::reflection_get() );
		return collection_refl::map_srp_simple;
	}
	virtual ptr<field_list_list> field_values_get() const
	{
		ptr<field_list_list> result = object::field_values_get();
		result->push_back( value_list::create() );
		// wrap all the pairs in lestes pairs, this is unavoidable
		transform( this->begin(), this->end(), back_inserter( *result->back() ),
				unary_pair_wrapper< srp<Key>, Data >() );
		return result;
	}
protected:
	map() : object(), ::std::map< srp<Key>, Data, Compare >()
	{}
	map( ptr< map< srp<Key>, Data, Compare > > from )
		: object(), ::std::map< srp<Key>, Data, Compare >( *checked(from) )
	{}
	void gc_mark()
	{
		object::gc_mark();
		for (typename map::iterator i = this->begin(); i != this->end(); ++i)
			i->first.gc_mark();
	}
};


template <typename Key, typename Data>
class map< Key, srp<Data>, ::std::less<Key> > : public object, public ::std::map<Key, srp<Data>, ::std::less<Key> >
{
public:
	static ptr< map< Key, srp<Data>, ::std::less<Key> > > create()
	{
		return new map();
	}
	static ptr< map< Key, srp<Data>, ::std::less<Key> > > create( ptr< map< Key, srp<Data>, ::std::less<Key> > > from )
	{
		return new map(from);
	}
	virtual ptr<reflection_list> reflection_get() const
	{
		if (!collection_refl::map_simple_srp)
			collection_refl::map_simple_srp_init( object::reflection_get() );
		return collection_refl::map_simple_srp;
	}
	virtual ptr<field_list_list> field_values_get() const
	{
		ptr<field_list_list> result = object::field_values_get();
		result->push_back( value_list::create() );
		// wrap all the pairs in lestes pairs, this is unavoidable
		transform( this->begin(), this->end(), back_inserter( *result->back() ),
				unary_pair_wrapper< Key, srp<Data> >() );
		return result;
	}
protected:
	map() : object(), ::std::map< Key, srp<Data>, ::std::less<Key> >()
	{}
	map( ptr< map< Key, srp<Data>, ::std::less<Key> > > from )
		: object(), ::std::map< Key, srp<Data>, ::std::less<Key> >( *checked(from) )
	{}
	void gc_mark()
	{
		object::gc_mark();
		for (typename map::iterator i = this->begin(); i != this->end(); ++i)
			i->second.gc_mark();
	}
};

template <typename Key, typename Data, typename Compare>
class map< Key, srp<Data>, Compare > : public object, public ::std::map<Key, srp<Data>, Compare >
{
public:
	static ptr< map< Key, srp<Data>, Compare > > create()
	{
		return new map();
	}
	static ptr< map< Key, srp<Data>, Compare > > create( ptr< map< Key, srp<Data>, Compare > > from )
	{
		return new map(from);
	}
	virtual ptr<reflection_list> reflection_get() const
	{
		if (!collection_refl::map_simple_srp)
			collection_refl::map_simple_srp_init( object::reflection_get() );
		return collection_refl::map_simple_srp;
	}
	virtual ptr<field_list_list> field_values_get() const
	{
		ptr<field_list_list> result = object::field_values_get();
		result->push_back( value_list::create() );
		// wrap all the pairs in lestes pairs, this is unavoidable
		transform( this->begin(), this->end(), back_inserter( *result->back() ),
				unary_pair_wrapper< Key, srp<Data> >() );
		return result;
	}
protected:
	map() : object(), ::std::map< Key, srp<Data>, Compare >()
	{}
	map( ptr< map< Key, srp<Data>, Compare > > from )
		: object(), ::std::map< Key, srp<Data>, Compare >( *checked(from) )
	{}
	void gc_mark()
	{
		object::gc_mark();
		for (typename map::iterator i = this->begin(); i != this->end(); ++i)
			i->second.gc_mark();
	}
};

end_package(std);
end_package(lestes);

#endif	// lestes__std___map_hh___included
/* vim: set ft=lestes : */
