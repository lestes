/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Representation of characters in source encoding.

  Defines helper class character containing constants and manipulators.
*/

#include <lestes/common.hh>
#include <lestes/std/character.hh>

package(lestes);
package(std);

// TODO remove doxyments
/*!
  ASCII bell character.
*/
const ucn character::ascii_bell;
/*!
  ASCII backspace character.
*/
const ucn character::ascii_backspace;
/*!
  ASCII tabulator character.
*/
const ucn character::ascii_tab;
/*!
  ASCII vertical tabulator character.
*/
const ucn character::ascii_vtab;
/*!
  ASCII form feed character.
*/
const ucn character::ascii_form_feed;
/*!
  ASCII new line character.
*/
const ucn character::ascii_new_line;
/*!
  ASCII carriage return character.
*/
const ucn character::ascii_carriage_return;
/*!
  ASCII space character.
*/
const ucn character::ascii_space;
/*!
  ASCII exclamation mark character.
*/
const ucn character::ascii_emark;
/*!
  ASCII double quote character.
*/
const ucn character::ascii_dquote;
/*!
  ASCII hash character.
*/
const ucn character::ascii_hash;
/*!
  ASCII dollar character.
*/
const ucn character::ascii_dollar;
/*!
  ASCII percent character.
*/
const ucn character::ascii_percent;
/*!
  ASCII ampersand character.
*/
const ucn character::ascii_amp;
/*!
  ASCII quote character.
*/
const ucn character::ascii_quote;
/*!
  ASCII left parenthesis character.
*/
const ucn character::ascii_left_par;
/*!
  ASCII right parenthesis character.
*/
const ucn character::ascii_right_par;
/*!
  ASCII star character.
*/
const ucn character::ascii_star;
/*!
  ASCII plus character.
*/
const ucn character::ascii_plus;
/*!
  ASCII comma character.
*/
const ucn character::ascii_comma;
/*!
  ASCII minus character.
*/
const ucn character::ascii_minus;
/*!
  ASCII dot character.
*/
const ucn character::ascii_dot;
/*!
  ASCII slash character.
*/
const ucn character::ascii_slash;
/*!
  ASCII digit 0 character.
*/
const ucn character::ascii_digit_0;
/*!
  ASCII digit 1 character.
*/
const ucn character::ascii_digit_1;
/*!
  ASCII digit 2 character.
*/
const ucn character::ascii_digit_2;
/*!
  ASCII digit 3 character.
*/
const ucn character::ascii_digit_3;
/*!
  ASCII digit 4 character.
*/
const ucn character::ascii_digit_4;
/*!
  ASCII digit 5 character.
*/
const ucn character::ascii_digit_5;
/*!
  ASCII digit 6 character.
*/
const ucn character::ascii_digit_6;
/*!
  ASCII digit 7 character.
*/
const ucn character::ascii_digit_7;
/*!
  ASCII digit 8 character.
*/
const ucn character::ascii_digit_8;
/*!
  ASCII digit 9 character.
*/
const ucn character::ascii_digit_9;
/*!
  ASCII colon character.
*/
const ucn character::ascii_colon;
/*!
  ASCII semicolon character.
*/
const ucn character::ascii_semicolon;
/*!
  ASCII less than character.
*/
const ucn character::ascii_lt;
/*!
  ASCII equals character.
*/
const ucn character::ascii_eq;
/*!
  ASCII greater than character.
*/
const ucn character::ascii_gt;
/*!
  ASCII question mark character.
*/
const ucn character::ascii_qmark;
/*!
  ASCII commercial at character (zavinac).
*/
const ucn character::ascii_at;
/*!
  ASCII upper a character.
*/
const ucn character::ascii_upper_a;
/*!
  ASCII upper b character.
*/
const ucn character::ascii_upper_b;
/*!
  ASCII upper c character.
*/
const ucn character::ascii_upper_c;
/*!
  ASCII upper d character.
*/
const ucn character::ascii_upper_d;
/*!
  ASCII upper e character.
*/
const ucn character::ascii_upper_e;
/*!
  ASCII upper f character.
*/
const ucn character::ascii_upper_f;
/*!
  ASCII upper g character.
*/
const ucn character::ascii_upper_g;
/*!
  ASCII upper h character.
*/
const ucn character::ascii_upper_h;
/*!
  ASCII upper i character.
*/
const ucn character::ascii_upper_i;
/*!
  ASCII upper j character.
*/
const ucn character::ascii_upper_j;
/*!
  ASCII upper k character.
*/
const ucn character::ascii_upper_k;
/*!
  ASCII upper l character.
*/
const ucn character::ascii_upper_l;
/*!
  ASCII upper m character.
*/
const ucn character::ascii_upper_m;
/*!
  ASCII upper n character.
*/
const ucn character::ascii_upper_n;
/*!
  ASCII upper o character.
*/
const ucn character::ascii_upper_o;
/*!
  ASCII upper p character.
*/
const ucn character::ascii_upper_p;
/*!
  ASCII upper q character.
*/
const ucn character::ascii_upper_q;
/*!
  ASCII upper r character.
*/
const ucn character::ascii_upper_r;
/*!
  ASCII upper s character.
*/
const ucn character::ascii_upper_s;
/*!
  ASCII upper t character.
*/
const ucn character::ascii_upper_t;
/*!
  ASCII upper u character.
*/
const ucn character::ascii_upper_u;
/*!
  ASCII upper v character.
*/
const ucn character::ascii_upper_v;
/*!
  ASCII upper w character.
*/
const ucn character::ascii_upper_w;
/*!
  ASCII upper x character.
*/
const ucn character::ascii_upper_x;
/*!
  ASCII upper y character.
*/
const ucn character::ascii_upper_y;
/*!
  ASCII upper z character.
*/
const ucn character::ascii_upper_z;
/*!
  ASCII left bracket character.
*/
const ucn character::ascii_left_bracket;
/*!
  ASCII backslash character.
*/
const ucn character::ascii_backslash;
/*!
  ASCII right bracket character.
*/
const ucn character::ascii_right_bracket;
/*!
  ASCII hat character.
*/
const ucn character::ascii_hat;
/*!
  ASCII underscore character.
*/
const ucn character::ascii_underscore;
/*!
  ASCII back quote character.
*/
const ucn character::ascii_bquote;
/*!
  ASCII lower a character.
*/
const ucn character::ascii_lower_a;
/*!
  ASCII lower b character.
*/
const ucn character::ascii_lower_b;
/*!
  ASCII lower c character.
*/
const ucn character::ascii_lower_c;
/*!
  ASCII lower d character.
*/
const ucn character::ascii_lower_d;
/*!
  ASCII lower e character.
*/
const ucn character::ascii_lower_e;
/*!
  ASCII lower f character.
*/
const ucn character::ascii_lower_f;
/*!
  ASCII lower g character.
*/
const ucn character::ascii_lower_g;
/*!
  ASCII lower h character.
*/
const ucn character::ascii_lower_h;
/*!
  ASCII lower i character.
*/
const ucn character::ascii_lower_i;
/*!
  ASCII lower j character.
*/
const ucn character::ascii_lower_j;
/*!
  ASCII lower k character.
*/
const ucn character::ascii_lower_k;
/*!
  ASCII lower l character.
*/
const ucn character::ascii_lower_l;
/*!
  ASCII lower m character.
*/
const ucn character::ascii_lower_m;
/*!
  ASCII lower n character.
*/
const ucn character::ascii_lower_n;
/*!
  ASCII lower o character.
*/
const ucn character::ascii_lower_o;
/*!
  ASCII lower p character.
*/
const ucn character::ascii_lower_p;
/*!
  ASCII lower q character.
*/
const ucn character::ascii_lower_q;
/*!
  ASCII lower r character.
*/
const ucn character::ascii_lower_r;
/*!
  ASCII lower s character.
*/
const ucn character::ascii_lower_s;
/*!
  ASCII lower t character.
*/
const ucn character::ascii_lower_t;
/*!
  ASCII lower u character.
*/
const ucn character::ascii_lower_u;
/*!
  ASCII lower v character.
*/
const ucn character::ascii_lower_v;
/*!
  ASCII lower w character.
*/
const ucn character::ascii_lower_w;
/*!
  ASCII lower x character.
*/
const ucn character::ascii_lower_x;
/*!
  ASCII lower y character.
*/
const ucn character::ascii_lower_y;
/*!
  ASCII lower z character.
*/
const ucn character::ascii_lower_z;
/*!
  ASCII left brace character.
*/
const ucn character::ascii_left_brace;
/*!
  ASCII vertical bar character.
*/
const ucn character::ascii_vbar;
/*!
  ASCII right brace character.
*/
const ucn character::ascii_right_brace;
/*!
  ASCII tilde character.
*/
const ucn character::ascii_tilde;

/*!
  Flags of ASCII part of internal characters, used for predicates.
*/
character::ascii_flags_type character::ascii_flags[character::ascii_length] = {
	/*   0 `\x00' */  FLG_NONE,
	/*   1 `\x01' */  FLG_NONE,
	/*   2 `\x02' */  FLG_NONE,
	/*   3 `\x03' */  FLG_NONE,
	/*   4 `\x04' */  FLG_NONE,
	/*   5 `\x05' */  FLG_NONE,
	/*   6 `\x06' */  FLG_NONE,
	/*   7 `\a'   */  FLG_BASIC,
	/*   8 `\b'   */  FLG_BASIC,
	/*   9 `\t'   */  FLG_BASIC | FLG_SPACE,
	/*  10 `\n'   */  FLG_BASIC | FLG_SPACE,
	/*  11 `\v'   */  FLG_BASIC | FLG_SPACE,
	/*  12 `\f'   */  FLG_BASIC | FLG_SPACE,
	/*  13 `\r'   */  FLG_BASIC | FLG_SPACE,
	/*  14 `\x0e' */  FLG_NONE,
	/*  15 `\x0f' */  FLG_NONE,
	/*  16 `\x10' */  FLG_NONE,
	/*  17 `\x11' */  FLG_NONE,
	/*  18 `\x12' */  FLG_NONE,
	/*  19 `\x13' */  FLG_NONE,
	/*  20 `\x14' */  FLG_NONE,
	/*  21 `\x15' */  FLG_NONE,
	/*  22 `\x16' */  FLG_NONE,
	/*  23 `\x17' */  FLG_NONE,
	/*  24 `\x18' */  FLG_NONE,
	/*  25 `\x19' */  FLG_NONE,
	/*  26 `\x1a' */  FLG_NONE,
	/*  27 `\x1b' */  FLG_NONE,
	/*  28 `\x1c' */  FLG_NONE,
	/*  29 `\x1d' */  FLG_NONE,
	/*  30 `\x1e' */  FLG_NONE,
	/*  31 `\x1f' */  FLG_NONE,
	/*  32 ` '    */  FLG_BASIC | FLG_SPACE,
	/*  33 `!'    */  FLG_BASIC,
	/*  34 `"'    */  FLG_BASIC,
	/*  35 `#'    */  FLG_BASIC,
	/*  36 `$'    */  FLG_NONE,
	/*  37 `%'    */  FLG_BASIC,
	/*  38 `&'    */  FLG_BASIC,
	/*  39 `''    */  FLG_BASIC,
	/*  40 `('    */  FLG_BASIC,
	/*  41 `)'    */  FLG_BASIC,
	/*  42 `*'    */  FLG_BASIC,
	/*  43 `+'    */  FLG_BASIC,
	/*  44 `,'    */  FLG_BASIC,
	/*  45 `-'    */  FLG_BASIC,
	/*  46 `.'    */  FLG_BASIC,
	/*  47 `/'    */  FLG_BASIC,
	/*  48 `0'    */  FLG_BASIC | FLG_DIGIT | FLG_ODIGIT | FLG_XDIGIT,
	/*  49 `1'    */  FLG_BASIC | FLG_DIGIT | FLG_ODIGIT | FLG_XDIGIT,
	/*  50 `2'    */  FLG_BASIC | FLG_DIGIT | FLG_ODIGIT | FLG_XDIGIT,
	/*  51 `3'    */  FLG_BASIC | FLG_DIGIT | FLG_ODIGIT | FLG_XDIGIT,
	/*  52 `4'    */  FLG_BASIC | FLG_DIGIT | FLG_ODIGIT | FLG_XDIGIT,
	/*  53 `5'    */  FLG_BASIC | FLG_DIGIT | FLG_ODIGIT | FLG_XDIGIT,
	/*  54 `6'    */  FLG_BASIC | FLG_DIGIT | FLG_ODIGIT | FLG_XDIGIT,
	/*  55 `7'    */  FLG_BASIC | FLG_DIGIT | FLG_ODIGIT | FLG_XDIGIT,
	/*  56 `8'    */  FLG_BASIC | FLG_DIGIT | FLG_XDIGIT,
	/*  57 `9'    */  FLG_BASIC | FLG_DIGIT | FLG_XDIGIT,
	/*  58 `:'    */  FLG_BASIC,
	/*  59 `;'    */  FLG_BASIC,
	/*  60 `<'    */  FLG_BASIC,
	/*  61 `='    */  FLG_BASIC,
	/*  62 `>'    */  FLG_BASIC,
	/*  63 `?'    */  FLG_BASIC,
	/*  64 `@'    */  FLG_NONE,
	/*  65 `A'    */  FLG_BASIC | FLG_UPPER | FLG_ALPHA | FLG_XDIGIT,
	/*  66 `B'    */  FLG_BASIC | FLG_UPPER | FLG_ALPHA | FLG_XDIGIT,
	/*  67 `C'    */  FLG_BASIC | FLG_UPPER | FLG_ALPHA | FLG_XDIGIT,
	/*  68 `D'    */  FLG_BASIC | FLG_UPPER | FLG_ALPHA | FLG_XDIGIT,
	/*  69 `E'    */  FLG_BASIC | FLG_UPPER | FLG_ALPHA | FLG_XDIGIT,
	/*  70 `F'    */  FLG_BASIC | FLG_UPPER | FLG_ALPHA | FLG_XDIGIT,
	/*  71 `G'    */  FLG_BASIC | FLG_UPPER | FLG_ALPHA,
	/*  72 `H'    */  FLG_BASIC | FLG_UPPER | FLG_ALPHA,
	/*  73 `I'    */  FLG_BASIC | FLG_UPPER | FLG_ALPHA,
	/*  74 `J'    */  FLG_BASIC | FLG_UPPER | FLG_ALPHA,
	/*  75 `K'    */  FLG_BASIC | FLG_UPPER | FLG_ALPHA,
	/*  76 `L'    */  FLG_BASIC | FLG_UPPER | FLG_ALPHA,
	/*  77 `M'    */  FLG_BASIC | FLG_UPPER | FLG_ALPHA,
	/*  78 `N'    */  FLG_BASIC | FLG_UPPER | FLG_ALPHA,
	/*  79 `O'    */  FLG_BASIC | FLG_UPPER | FLG_ALPHA,
	/*  80 `P'    */  FLG_BASIC | FLG_UPPER | FLG_ALPHA,
	/*  81 `Q'    */  FLG_BASIC | FLG_UPPER | FLG_ALPHA,
	/*  82 `R'    */  FLG_BASIC | FLG_UPPER | FLG_ALPHA,
	/*  83 `S'    */  FLG_BASIC | FLG_UPPER | FLG_ALPHA,
	/*  84 `T'    */  FLG_BASIC | FLG_UPPER | FLG_ALPHA,
	/*  85 `U'    */  FLG_BASIC | FLG_UPPER | FLG_ALPHA,
	/*  86 `V'    */  FLG_BASIC | FLG_UPPER | FLG_ALPHA,
	/*  87 `W'    */  FLG_BASIC | FLG_UPPER | FLG_ALPHA,
	/*  88 `X'    */  FLG_BASIC | FLG_UPPER | FLG_ALPHA,
	/*  89 `Y'    */  FLG_BASIC | FLG_UPPER | FLG_ALPHA,
	/*  90 `Z'    */  FLG_BASIC | FLG_UPPER | FLG_ALPHA,
	/*  91 `['    */  FLG_BASIC,
	/*  92 `\\'   */  FLG_BASIC,
	/*  93 `]'    */  FLG_BASIC,
	/*  94 `^'    */  FLG_BASIC,
	/*  95 `_'    */  FLG_BASIC,
	/*  96 ``'    */  FLG_NONE,
	/*  97 `a'    */  FLG_BASIC | FLG_LOWER | FLG_ALPHA | FLG_XDIGIT,
	/*  98 `b'    */  FLG_BASIC | FLG_LOWER | FLG_ALPHA | FLG_XDIGIT,
	/*  99 `c'    */  FLG_BASIC | FLG_LOWER | FLG_ALPHA | FLG_XDIGIT,
	/* 100 `d'    */  FLG_BASIC | FLG_LOWER | FLG_ALPHA | FLG_XDIGIT,
	/* 101 `e'    */  FLG_BASIC | FLG_LOWER | FLG_ALPHA | FLG_XDIGIT,
	/* 102 `f'    */  FLG_BASIC | FLG_LOWER | FLG_ALPHA | FLG_XDIGIT,
	/* 103 `g'    */  FLG_BASIC | FLG_LOWER | FLG_ALPHA,
	/* 104 `h'    */  FLG_BASIC | FLG_LOWER | FLG_ALPHA,
	/* 105 `i'    */  FLG_BASIC | FLG_LOWER | FLG_ALPHA,
	/* 106 `j'    */  FLG_BASIC | FLG_LOWER | FLG_ALPHA,
	/* 107 `k'    */  FLG_BASIC | FLG_LOWER | FLG_ALPHA,
	/* 108 `l'    */  FLG_BASIC | FLG_LOWER | FLG_ALPHA,
	/* 109 `m'    */  FLG_BASIC | FLG_LOWER | FLG_ALPHA,
	/* 110 `n'    */  FLG_BASIC | FLG_LOWER | FLG_ALPHA,
	/* 111 `o'    */  FLG_BASIC | FLG_LOWER | FLG_ALPHA,
	/* 112 `p'    */  FLG_BASIC | FLG_LOWER | FLG_ALPHA,
	/* 113 `q'    */  FLG_BASIC | FLG_LOWER | FLG_ALPHA,
	/* 114 `r'    */  FLG_BASIC | FLG_LOWER | FLG_ALPHA,
	/* 115 `s'    */  FLG_BASIC | FLG_LOWER | FLG_ALPHA,
	/* 116 `t'    */  FLG_BASIC | FLG_LOWER | FLG_ALPHA,
	/* 117 `u'    */  FLG_BASIC | FLG_LOWER | FLG_ALPHA,
	/* 118 `v'    */  FLG_BASIC | FLG_LOWER | FLG_ALPHA,
	/* 119 `w'    */  FLG_BASIC | FLG_LOWER | FLG_ALPHA,
	/* 120 `x'    */  FLG_BASIC | FLG_LOWER | FLG_ALPHA,
	/* 121 `y'    */  FLG_BASIC | FLG_LOWER | FLG_ALPHA,
	/* 122 `z'    */  FLG_BASIC | FLG_LOWER | FLG_ALPHA,
	/* 123 `{'    */  FLG_BASIC,
	/* 124 `|'    */  FLG_BASIC,
	/* 125 `}'    */  FLG_BASIC,
	/* 126 `~'    */  FLG_BASIC,
	/* 127 `\x7f' */  FLG_NONE
};

/*!
	List of unicode character ranges for C++ identifiers.
	Does not include basic source character ranges.
	Shall be sorted in increasing order (for binary search).
*/
character::range_type character::identifier_ranges[] = {
	{ 0x00C0, 0x00D6 },
	{ 0x00D8, 0x00F6 },
	{ 0x00F8, 0x01F5 },
	{ 0x01FA, 0x0217 },
	{ 0x0250, 0x02A8 },
	{ 0x0384, 0x0384 },
	{ 0x0388, 0x038A },
	{ 0x038C, 0x038C },
	{ 0x038E, 0x03A1 },
	{ 0x03A3, 0x03CE },
	{ 0x03D0, 0x03D6 },
	{ 0x03DA, 0x03DA },
	{ 0x03DC, 0x03DC },
	{ 0x03DE, 0x03DE },
	{ 0x03E0, 0x03E0 },
	{ 0x03E2, 0x03F3 },
	{ 0x0401, 0x040C },
	{ 0x040D, 0x040D },
	{ 0x040F, 0x044F },
	{ 0x0451, 0x045C },
	{ 0x045E, 0x0481 },
	{ 0x0490, 0x04C4 },
	{ 0x04C7, 0x04C8 },
	{ 0x04CB, 0x04CC },
	{ 0x04D0, 0x04EB },
	{ 0x04EE, 0x04F5 },
	{ 0x04F8, 0x04F9 },
	{ 0x0531, 0x0556 },
	{ 0x0561, 0x0587 },
	{ 0x05D0, 0x05EA },
	{ 0x05F0, 0x05F2 },
	{ 0x05F3, 0x05F4 },
	{ 0x0621, 0x063A },
	{ 0x0640, 0x0652 },
	{ 0x0670, 0x06B7 },
	{ 0x06BA, 0x06BE },
	{ 0x06C0, 0x06CE },
	{ 0x06E5, 0x06E7 },
	{ 0x0905, 0x0939 },
	{ 0x0958, 0x0962 },
	{ 0x0985, 0x098C },
	{ 0x098F, 0x0990 },
	{ 0x0993, 0x09A8 },
	{ 0x09AA, 0x09B0 },
	{ 0x09B2, 0x09B2 },
	{ 0x09B6, 0x09B9 },
	{ 0x09DC, 0x09DD },
	{ 0x09DF, 0x09E1 },
	{ 0x09F0, 0x09F1 },
	{ 0x0A05, 0x0A0A },
	{ 0x0A0F, 0x0A10 },
	{ 0x0A13, 0x0A28 },
	{ 0x0A2A, 0x0A30 },
	{ 0x0A32, 0x0A33 },
	{ 0x0A35, 0x0A36 },
	{ 0x0A38, 0x0A39 },
	{ 0x0A59, 0x0A5C },
	{ 0x0A5E, 0x0A5E },
	{ 0x0A85, 0x0A8B },
	{ 0x0A8D, 0x0A8D },
	{ 0x0A8F, 0x0A91 },
	{ 0x0A93, 0x0AA8 },
	{ 0x0AAA, 0x0AB0 },
	{ 0x0AB2, 0x0AB3 },
	{ 0x0AB5, 0x0AB9 },
	{ 0x0AE0, 0x0AE0 },
	{ 0x0B05, 0x0B0C },
	{ 0x0B0F, 0x0B10 },
	{ 0x0B13, 0x0B28 },
	{ 0x0B2A, 0x0B30 },
	{ 0x0B32, 0x0B33 },
	{ 0x0B36, 0x0B39 },
	{ 0x0B5C, 0x0B5D },
	{ 0x0B5F, 0x0B61 },
	{ 0x0B85, 0x0B8A },
	{ 0x0B8E, 0x0B90 },
	{ 0x0B92, 0x0B95 },
	{ 0x0B99, 0x0B9A },
	{ 0x0B9C, 0x0B9C },
	{ 0x0B9E, 0x0B9F },
	{ 0x0BA3, 0x0BA4 },
	{ 0x0BA8, 0x0BAA },
	{ 0x0BAE, 0x0BB5 },
	{ 0x0BB7, 0x0BB9 },
	{ 0x0C05, 0x0C0C },
	{ 0x0C0E, 0x0C10 },
	{ 0x0C12, 0x0C28 },
	{ 0x0C2A, 0x0C33 },
	{ 0x0C35, 0x0C39 },
	{ 0x0C60, 0x0C61 },
	{ 0x0C85, 0x0C8C },
	{ 0x0C8E, 0x0C90 },
	{ 0x0C92, 0x0CA8 },
	{ 0x0CAA, 0x0CB3 },
	{ 0x0CB5, 0x0CB9 },
	{ 0x0CE0, 0x0CE1 },
	{ 0x0D05, 0x0D0C },
	{ 0x0D0E, 0x0D10 },
	{ 0x0D12, 0x0D28 },
	{ 0x0D2A, 0x0D39 },
	{ 0x0D60, 0x0D61 },
	{ 0x0E01, 0x0E30 },
	{ 0x0E32, 0x0E33 },
	{ 0x0E40, 0x0E46 },
	{ 0x0E50, 0x0E59 },
	{ 0x0E5A, 0x0E5B },
	{ 0x0E81, 0x0E82 },
	{ 0x0E84, 0x0E84 },
	{ 0x0E87, 0x0E88 },
	{ 0x0E8A, 0x0E8A },
	{ 0x0E8D, 0x0E8D },
	{ 0x0E94, 0x0E97 },
	{ 0x0E99, 0x0E9F },
	{ 0x0EA1, 0x0EA3 },
	{ 0x0EA5, 0x0EA5 },
	{ 0x0EA7, 0x0EA7 },
	{ 0x0EAA, 0x0EAB },
	{ 0x0EAD, 0x0EAE },
	{ 0x0EAF, 0x0EAF },
	{ 0x0EB0, 0x0EB0 },
	{ 0x0EB2, 0x0EB3 },
	{ 0x0EBD, 0x0EBD },
	{ 0x0EC0, 0x0EC4 },
	{ 0x0EC6, 0x0EC6 },
	{ 0x10A0, 0x10C5 },
	{ 0x10D0, 0x10F6 },
	{ 0x1100, 0x1159 },
	{ 0x1161, 0x11A2 },
	{ 0x11A8, 0x11F9 },
	{ 0x1E00, 0x1E9A },
	{ 0x1EA0, 0x1EF9 },
	{ 0x1F00, 0x1F15 },
	{ 0x1F18, 0x1F1D },
	{ 0x1F20, 0x1F45 },
	{ 0x1F48, 0x1F4D },
	{ 0x1F50, 0x1F57 },
	{ 0x1F59, 0x1F59 },
	{ 0x1F5B, 0x1F5B },
	{ 0x1F5D, 0x1F5D },
	{ 0x1F5F, 0x1F7D },
	{ 0x1F80, 0x1FB4 },
	{ 0x1FB6, 0x1FBC },
	{ 0x1FC2, 0x1FC4 },
	{ 0x1FC6, 0x1FCC },
	{ 0x1FD0, 0x1FD3 },
	{ 0x1FD6, 0x1FDB },
	{ 0x1FE0, 0x1FEC },
	{ 0x1FF2, 0x1FF4 },
	{ 0x1FF6, 0x1FFC },
	{ 0x3041, 0x3093 },
	{ 0x3094, 0x3094 },
	{ 0x309B, 0x309C },
	{ 0x309D, 0x309E },
	{ 0x30A1, 0x30F6 },
	{ 0x30F7, 0x30FA },
	{ 0x30FB, 0x30FC },
	{ 0x30FD, 0x30FE },
	{ 0x3105, 0x312C },
	{ 0x4E00, 0x9FA5 },
	{ 0xF900, 0xFA2D },
	{ 0xFB1F, 0xFB36 },
	{ 0xFB38, 0xFB3C },
	{ 0xFB3E, 0xFB3E },
	{ 0xFB40, 0xFB44 },
	{ 0xFB46, 0xFBB1 },
	{ 0xFBD3, 0xFD3F },
	{ 0xFD50, 0xFD8F },
	{ 0xFD92, 0xFDC7 },
	{ 0xFDF0, 0xFDFB },
	{ 0xFE70, 0xFE72 },
	{ 0xFE74, 0xFE74 },
	{ 0xFE76, 0xFEFC },
	{ 0xFF21, 0xFF3A },
	{ 0xFF41, 0xFF5A },
	{ 0xFF66, 0xFFBE },
	{ 0xFFC2, 0xFFC7 },
	{ 0xFFCA, 0xFFCF },
	{ 0xFFD2, 0xFFD7 },
	{ 0xFFDA, 0xFFDC }
};

/*!
  Internal to host encoding of basic characters translation table.
  Unused fields are ignored.
  Assumes that no basic character has code hchar_unknown in host encoding.
*/
hchar character::internal_to_host[character::ascii_length] = {
	character::hchar_unknown,
	character::hchar_unknown,
	character::hchar_unknown,
	character::hchar_unknown,
	character::hchar_unknown,
	character::hchar_unknown,
	character::hchar_unknown,
	'\a',
	'\b',
	'\t',
	'\n',
	'\v',
	'\f',
	'\r',
	character::hchar_unknown,
	character::hchar_unknown,
	character::hchar_unknown,
	character::hchar_unknown,
	character::hchar_unknown,
	character::hchar_unknown,
	character::hchar_unknown,
	character::hchar_unknown,
	character::hchar_unknown,
	character::hchar_unknown,
	character::hchar_unknown,
	character::hchar_unknown,
	character::hchar_unknown,
	character::hchar_unknown,
	character::hchar_unknown,
	character::hchar_unknown,
	character::hchar_unknown,
	character::hchar_unknown,
	' ',
	'!',
	'"',
	'#',
	'$',
	'%',
	'&',
	'\'',
	'(',
	')',
	'*',
	'+',
	',',
	'-',
	'.',
	'/',
	'0',
	'1',
	'2',
	'3',
	'4',
	'5',
	'6',
	'7',
	'8',
	'9',
	':',
	';',
	'<',
	'=',
	'>',
	'?',
	'@',
	'A',
	'B',
	'C',
	'D',
	'E',
	'F',
	'G',
	'H',
	'I',
	'J',
	'K',
	'L',
	'M',
	'N',
	'O',
	'P',
	'Q',
	'R',
	'S',
	'T',
	'U',
	'V',
	'W',
	'X',
	'Y',
	'Z',
	'[',
	'\\',
	']',
	'^',
	'_',
	'`',
	'a',
	'b',
	'c',
	'd',
	'e',
	'f',
	'g',
	'h',
	'i',
	'j',
	'k',
	'l',
	'm',
	'n',
	'o',
	'p',
	'q',
	'r',
	's',
	't',
	'u',
	'v',
	'w',
	'x',
	'y',
	'z',
	'{',
	'|',
	'}',
	'~',
	character::hchar_unknown
};

/*!
  Host to internal encoding translation table.
  This is reasonable only for narrow range host characters, could be substituted by map.
  Zeroed statically, initialization is done in constructor.
*/
ulint character::host_to_internal[host_length] = {
};

/*!
  Initializes some of the internal tables.
*/
character::character(void)
{
	if (initialized) return;
	initialized = 1;
	// fill the table according the reverse
	for (ulint i = 0; i < ascii_length; i++) {
		hchar c = internal_to_host[i];
		if (c != hchar_unknown) {
			host_to_internal[static_cast<ulint>(c)] = i;
		}
	}
}

/*!
  Destructs the initializer instance.
*/
character::~character(void)
{   
}

/*!
  Tests if value is translated unicode C++ identifier character.
  Performs a binary search, idea taken from gcc.
  \param u The value to test.
  \return true  If the value falls into a range for valid C++ identifier.
*/
bool character::is_translated_identifier(ucn u)
{
	if (!is_translated(u)) return false;

	ulint x = extract_value(u);

	ulint i = 0;
	ulint j = sizeof(identifier_ranges)/sizeof(identifier_ranges[0]);
	ulint k;

	while (i < j) {
		k = i + ((j - i) >> 1);
		if (x < identifier_ranges[k].low)
			j = k;
		else if (x > identifier_ranges[k].high)
			i = k + 1;
		else return true;
	}
	return false;
}

/*!
  Multiple initialization guard, set to true after first constructor run.
*/
bool character::initialized = false;

end_package(std);
end_package(lestes);

/* vim: set ft=lestes : */
