/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/std/dumper.hh>
#include <lestes/std/reflect.hh>
#include <lestes/std/objectize.hh>
#include <lestes/std/objectize_macros.hh>
#include <lestes/std/map.hh>
#include <lestes/std/set.hh>
#include <lestes/std/list.hh>
#include <lestes/std/pair.hh>
#include <lestes/std/vector.hh>

package(lestes);
package(std);

using ::std::endl;

::std::ostream & dumper::dump( ::std::ostream & output_to,
		ptr<object> start_at, bool respect_barriers )
{
	ptr<dumper> instance = new dumper( output_to, respect_barriers );
	instance->the_dump(start_at);
	return output_to;
}

void dumper::visit_object( ptr<object> o )
{
	lassert(o);
	was_simple = false;
}

void dumper::visit_nodump()
{
	stream << "<nondumpable />";
	was_simple = true;
}

void dumper::visit_lint( lint i )
{
	stream << "<simple type=\"lint\" value=\"" << i << "\" />";
	was_simple = true;
}

void dumper::visit_ulint( ulint i )
{
	stream << "<simple type=\"ulint\" value=\"" << i << "\" />";
	was_simple = true;
}

void dumper::visit_bool( bool i )
{
	// ostream does not seem to support bool (or vice versa?)
	stream << "<simple type=\"bool\" value=\"" << (i ? "true" : "false") << "\" />";
	was_simple = true;
}

void dumper::visit_lstring( const lstring &i )
{
	lstring tmp(i);
	lstring sought("<>&");
	lstring lt("&lt;");
	lstring gt("&gt;");
	lstring amp("&amp;");
	lstring::size_type mark = tmp.find_first_of(sought);
	if (mark != lstring::npos)
		do {
			switch (tmp[mark]) {
			case '<':
				tmp.replace(mark, 1, lt);
				break;
			case '>':
				tmp.replace(mark, 1, gt);
				break;
			case '&':
				tmp.replace(mark, 1, amp);
				break;
			}
		} while ((mark = tmp.find_first_of(sought, mark+1)) != lstring::npos);
	stream << "<simple type=\"lstring\" value=\"" << tmp << "\" />";
	was_simple = true;
}

void dumper::visit_ucn_string( const ucn_string &i )
{
	ucn_string tmp(i);
	ucn_string sought("\x3c\x3e\x26");
	ucn_string lt("\x26\x6c\x74\x3b");
	ucn_string gt("\x26\x67\x74\x3b");
	ucn_string amp("\x26\x61\x6d\x70\x3b");
	ucn_string::size_type mark = tmp.find_first_of(sought);
	if (mark != ucn_string::npos)
		do {
			switch (tmp[mark]) {
			case 0x3c:
				tmp.replace(mark, 1, lt);
				break;
			case 0x3e:
				tmp.replace(mark, 1, gt);
				break;
			case 0x26:
				tmp.replace(mark, 1, amp);
				break;
			}
		} while ((mark = tmp.find_first_of(sought, mark+1)) != ucn_string::npos);
	stream << "<simple type=\"ucn_string\" value=\"" << tmp << "\" />";
	was_simple = true;
}

void dumper::try_enqueue( ptr<object> o, ptr<queue_type> queue, ptr<set_type> visited )
{
	bool not_yet_visited = visited->insert(o).second;
	if (not_yet_visited)
		queue->push_back(o);
}

void dumper::gc_mark()
{
	dumper_visitor::gc_mark();
}

dumper::dumper( ::std::ostream &a_stream, bool a_respect_barriers )
	: dumper_visitor(), stream(a_stream), respect_barriers(a_respect_barriers)
{}

void dumper::the_dump( ptr<object> start_at )
{
	ptr<queue_type> queue = queue_type::create();
	ptr<set_type> visited = set_type::create();

	stream << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
		"<dump xmlns=\"http://lestes.jikos.cz/schemas/dump\">" << endl;
	if (start_at) {
		// if start_at is simple type, do not bother to run the algorithm :)
		start_at->accept_dumper_visitor(this);
		if (!was_simple) {
			try_enqueue( start_at, queue, visited );
		}
	} else {
		stream << "<null />" << endl;
	}
	while ( !queue->empty() ) {
		ptr<object> current = queue->front();
		queue->pop_front();
		// we do not enqueue null values
		lassert(current);

		bool at_barrier = current->dump_barrier_get();

		ptr<reflection_list> refls = current->reflection_get();
		lassert( refls );

		stream << "<instance type=\"" << refls->back()->name_get() << "\""
			" id=\"i_" << current->uid_get() << "\">" << endl;
		ptr<field_list_list> fields = current->field_values_get();
		field_list_list::iterator field_it = fields->begin();
		for ( reflection_list::iterator refl_it = refls->begin();
				refl_it != refls->end(); ++refl_it ) {

			ptr<class_reflection::field_metadata_list> metadata =
				(*refl_it)->field_metadata_get();

			for (class_reflection::field_metadata_list::iterator
					meta_it = metadata->begin();
					meta_it != metadata->end();
					++meta_it, ++field_it ) {

				// we must not run out of fields
				lassert( field_it != fields->end() );

				ulint item_no = 0;
				bool do_append_no = (*field_it)->size() > 1;
				for (value_list::iterator val_it = (*field_it)->begin();
						val_it != (*field_it)->end();
						++val_it, ++item_no ) {

					stream << "<field name=\"" << (*meta_it)->first;
					if (do_append_no)
						stream << item_no;
					stream << "\" type=\"" << (*meta_it)->second << "\""
						" origin=\"" << (*refl_it)->name_get() << "\">";
					if (*val_it) {
						(*val_it)->accept_dumper_visitor(this);
						if (!was_simple) {
							if (respect_barriers && at_barrier)
								stream << "<after-barrier />";
							else {
								try_enqueue( *val_it, queue, visited );
								stream << "<pointer idref=\"i_" <<
									(*val_it)->uid_get() << "\" />";
							}
						}
					} else
						stream << "<null />";
					stream << "</field>" << endl;
				}
			}
		}

		// just an assert: there should not be any value lists left
		lassert( field_it == fields->end() );
		stream << "</instance>" << endl;
	};
	stream << "</dump>" << endl;
}

::std::ostream & readable_dumper::dump( ::std::ostream & output_to,
		ptr<object> start_at, bool respect_barriers )
{
	ptr<readable_dumper> instance =
		new readable_dumper( output_to, respect_barriers );
	instance->visited = set_type::create();
	instance->after_barrier = false;
	if (start_at) {
		output_to << "<dump>" << endl;
		start_at->accept_dumper_visitor(instance);
		output_to << "</dump>" << endl;
	} else
		output_to << "<null />" << endl;
	return output_to;
}

void readable_dumper::gc_mark()
{
	visited.gc_mark();
	dumper_visitor::gc_mark();
}

void readable_dumper::follow( ptr<object> o )
{
	ptr<reflection_list> refls = o->reflection_get();
	lassert( refls );

	bool already_visited = !(visited->insert(o).second);
	if (already_visited) {
		stream << '<' << refls->back()->name_get() << 
			" idref=\"i_" << o->uid_get() << "\" />" << endl;
		return;
	}
	stream << '<' << refls->back()->name_get() << " id=\"i_" << o->uid_get() << "\">" << endl;

	bool at_barrier = o->dump_barrier_get();

	ptr<field_list_list> fields = o->field_values_get();
	field_list_list::iterator field_it = fields->begin();
	for ( reflection_list::iterator refl_it = refls->begin();
			refl_it != refls->end(); ++refl_it ) {

		ptr<class_reflection::field_metadata_list> metadata =
			(*refl_it)->field_metadata_get();

		for (class_reflection::field_metadata_list::iterator
				meta_it = metadata->begin();
				meta_it != metadata->end();
				++meta_it, ++field_it ) {

			// we must not run out of fields
			lassert( field_it != fields->end() );

			ulint item_no = 0;
			bool do_append_no = (*field_it)->size() > 1;
			for (value_list::iterator val_it = (*field_it)->begin();
					val_it != (*field_it)->end(); ++val_it, ++item_no ) {
				stream << '<' << (*meta_it)->first;
				if (do_append_no)
					stream << item_no;
				stream << " type=\"" << (*meta_it)->second << "\""
					" origin=\"" << (*refl_it)->name_get() << "\">" <<
					endl;
				if (*val_it) {
					if (respect_barriers && at_barrier)
						after_barrier = true;
					(*val_it)->accept_dumper_visitor(this);
					after_barrier = false;
				} else
					stream << "<null />" << endl;
				stream << "</" << (*meta_it)->first;
				if (do_append_no)
					stream << item_no;
				stream << '>' << endl;
			}
		}
	}

	// just an assert: there should not be any value lists left
	lassert( field_it == fields->end() );

	stream << "</" << refls->back()->name_get() << '>' << endl;
}

readable_dumper::readable_dumper( ::std::ostream &a_stream, bool a_respect_barriers )
	: dumper_visitor(), stream(a_stream), respect_barriers(a_respect_barriers)
{}

void readable_dumper::visit_object( ptr<object> o )
{
	if (after_barrier)
		stream << "<after-barrier />" << endl;
	else
		follow(o);
}

void readable_dumper::visit_nodump()
{
	stream << "<nondumpable />" << endl;
}

void readable_dumper::visit_lint( lint i )
{
	stream << "<lint>" << i << "</lint>" << endl;
}

void readable_dumper::visit_ulint( ulint i )
{
	stream << "<ulint>" << i << "</ulint>" << endl;
}

void readable_dumper::visit_bool( bool i )
{
	// ostream does not seem to support bool, it would cast to int or something
	stream << "<bool>" << (i ? "true" : "false") << "</bool>" << endl;
}

void readable_dumper::visit_lstring( const lstring &i )
{
	lstring tmp(i);
	lstring sought("<>&");
	lstring lt("&lt;");
	lstring gt("&gt;");
	lstring amp("&amp;");
	lstring::size_type mark = tmp.find_first_of(sought);
	if (mark != lstring::npos)
		do {
			switch (tmp[mark]) {
			case '<':
				tmp.replace(mark, 1, lt);
				break;
			case '>':
				tmp.replace(mark, 1, gt);
				break;
			case '&':
				tmp.replace(mark, 1, amp);
				break;
			}
		} while ((mark = tmp.find_first_of(sought, mark+1)) != lstring::npos);
	stream << "<lstring>" << tmp << "</lstring>" << endl;
}

void readable_dumper::visit_ucn_string( const ucn_string &i )
{
	ucn_string tmp(i);
	ucn_string sought("\x3c\x3e\x26");
	ucn_string lt("\x26\x6c\x74\x3b");
	ucn_string gt("\x26\x67\x74\x3b");
	ucn_string amp("\x26\x61\x6d\x70\x3b");
	ucn_string::size_type mark = tmp.find_first_of(sought);
	if (mark != ucn_string::npos)
		do {
			switch (tmp[mark]) {
			case 0x3c:
				tmp.replace(mark, 1, lt);
				break;
			case 0x3e:
				tmp.replace(mark, 1, gt);
				break;
			case 0x26:
				tmp.replace(mark, 1, amp);
				break;
			}
		} while ((mark = tmp.find_first_of(sought, mark+1)) != ucn_string::npos);
	stream << "<ucn_string>" << tmp << "</ucn_string>" << endl;
}

end_package(std);
end_package(lestes);
