/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__std___data_types_hh___included
#define lestes__std___data_types_hh___included

/*! \file
  Definition of target and host data types common to all sources.
  The host is the machine that runs lestes, the data types are native.
  The target is the machine that lestes compiles for, types are supplied via configuration.
  \author pt
  \author Rudo, added lstring, removed #include <lestes/common.h>
*/

#include <host_types.hh>
#include <target_types.hh>
#include <string>

package(lestes);
package(std);

//! Defines lestes host 32 bit integer.
typedef lc_host_int_least32 lint;
//! Defines lestes host 32 bit unsigned integer.
typedef lc_host_uint_least32 ulint;

//! Defines lestes string; not to be used for anything concerning the processed source file.
typedef ::std::string lstring;

end_package(std);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
