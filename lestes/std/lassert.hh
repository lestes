/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__std___lassert_hh___included
#define lestes__std___lassert_hh___included

/*! \file
  Assertion checking macros.
  \author Rudo, the new lassert2 macro
  \author Rudo, added lassert_event
  \author Rudo, the new lassert3 and lassert2l macros
  \author JAZ, include file messages.hh with standard assertation messages.
*/

#include <lestes/package.hh>
#include <lestes/std/data_types.hh>
#include <lestes/std/event_macros.hh>
#include <lestes/std/messages.hh>

#include <lestes/std/source_location.hh>

/* __func__ absence workaround */
#if (defined(__STDC_VERSION__) && __STDC_VERSION__ >= 199901L) \
	|| (defined(__GNUC__) && (__GNUC__*1000+__GNUC_MINOR__) >= 3000)

	#define LESTES__func__ __func__
#else
	#if (defined(_MSC_VER) && _MSC_VER < 1300)
		#define LESTES__func__ "unknown funtion or unsupported compiler"
	#else
		#define LESTES__func__ __FUNCTION__ /* hope for the best */
	#endif
#endif


/*!
 * \brief When given expression evaluates to false, the program is aborted.
 *
 * Should be used instead of the standard assert macro.
 *
 * \param e	The expression to evaluate, it is evaluated only once.
 * \param msg	If not empty, it is printed after the "assertion failed" info.
 * \param loc	source_location; if not NULL, it is printed before the "assertion failed" info.
 */
#define lassert3( e, msg, loc ) \
	((e) ? (void)0 : ::lestes::std::lassert_fail( #e, __FILE__, __LINE__, LESTES__func__, (msg), (loc) ))

//! a shortcut to lestes3 without a location
#define lassert2( e, msg )	lassert3( e, (msg), NULL )

//! a shortcut to lassert3 without a message
#define lassert2loc( e, loc )	lassert3( e, "", (loc) )

//! a shortcut to lassert2 without a message
#define lassert( e )		lassert2( e, "" )

package(lestes);
package(std);

//! lassert_event is trigerred when a lassertion fails (including ignored lassertions)
declare_event( lassert_event );

//! Called when an assertion fails.
void lassert_fail( lstring assertion,
		lstring filename, ulint line, lstring function,
		lstring message, ptr<source_location> )
#ifdef __GNUC__
#ifndef CONTINUABLE_ASSERTIONS
__attribute__ ((noreturn))
#endif
#endif
;

end_package(std);
end_package(lestes);

#endif	// lestes__std___lassert_hh___included
/* vim: set ft=lestes : */
