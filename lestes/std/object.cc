/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  Definition of object class representing general object.
  \author pt
  \author rudo, the dumper code
*/
#include <lestes/common.hh>
#include <lestes/std/object.hh>
#include <lestes/std/reflect.hh>
#include <lestes/std/list.hh>
#include <lestes/std/pair.hh>
#include <lestes/std/dumper_visitor.hh>

package(lestes);
package(std);

ptr<object::reflection_list> object::reflection;

ptr<object::reflection_list> object::reflection_get() const
{
	if (!reflection) {
		reflection = reflection_list::create();
		reflection->push_back(
			class_reflection::create( "object",
				class_reflection::field_metadata_list::create() ) );
	}
	return reflection;
}

ptr<object::field_list_list> object::field_values_get() const
{
	return field_list_list::create();
}

void object::accept_dumper_visitor( ptr<dumper_visitor> v )
{
	return v->visit_object(this);
}

ulint object::next_uid = 1;

ulint object::uid_get()
{
	if (uid == 0)
		uid = next_uid++;
	return uid;
}

bool object::dump_barrier_get() const
{
	return dump_barrier;
}

void object::dump_barrier_set( bool b )
{
	dump_barrier = b;
}

/*!
 * The comparision is done by comparing the object to be the same entity.
 * \author TMA
 */
bool object::equals(ptr <object> o)
{
	return o == this;
}
end_package(std);
end_package(lestes);

/* vim: set ft=lestes : */
