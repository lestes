/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__std___ucn_traits_hh___included
#define lestes__std___ucn_traits_hh___included

/*! \file
  Specialization of char_traits for ucn.
*/

#include <cstdlib>
#include <string>
#include <lestes/common.hh>
#include <lestes/std/character.hh>

// this is ::std NOT ::lestes::std
package(std);

/*!
  Defines ucn specific char traits for ::std::basic_string.
*/
template<>
struct char_traits< ::lestes::std::ucn > {
	typedef ::lestes::std::ucn              char_type;
	// int_type has at least one extra bit than ucn
	typedef ::lestes::lc_host_uint_least32  int_type;
	typedef streampos                       pos_type;
	typedef streamoff                       off_type;
	typedef mbstate_t                       state_type;

	static void 
	assign(char_type& c1, const char_type& c2)
	{
		c1 = c2;
	}

	static bool 
	eq(const char_type& c1, const char_type& c2)
	{
		return c1 == c2;
	}

	static bool 
	lt(const char_type& c1, const char_type& c2)
	{
		return c1 < c2;
	}

	static int 
	compare(const char_type* s1, const char_type* s2, size_t n)
	{
		while (n && *s1 == *s2) {
			s1++;
			s2++;
			n--;
		}
		return (n == 0) ? 0 : (*s1 > *s2) ? 1 : -1;
	}

	static size_t
	length(const char_type* s)
	{
		size_t n = 0;
		while (*s++)
		  n++;

		return n; 
	}

	static const char_type* 
	find(const char_type* s, size_t n, const char_type& a)
	{
		while (n--) {
			if (*s == a)
				return s;
			s++;
		}

		return static_cast<const char_type*>(0);
	}

	static char_type* 
	move(char_type* s1, const char_type* s2, size_t n)
	{
		return static_cast<char_type*>(memmove(s1,s2,n * sizeof(char_type)));
	}

	static char_type* 
	copy(char_type* s1, const char_type* s2, size_t n)
	{
		std::copy(s2,s2 + n,s1);
		return s1;
	}

	static char_type* 
	assign(char_type* s, size_t n, char_type a)
	{
		fill_n(s,n,a);
		return s;
	}

	static char_type 
	to_char_type(const int_type& c)
	{
		return static_cast<char_type>(c);
	}

	static int_type 
	to_int_type(const char_type& c)
	{
		return static_cast<int_type>(c);
	}

	static bool 
	eq_int_type(const int_type& c1, const int_type& c2)
	{
		return c1 == c2;
	}

	static int_type 
	eof(void) 
	{
		return ~static_cast<int_type>(0);
	}

	static int_type 
	not_eof(const int_type& c)
	{
		return (c == eof()) ? 0 : c;
	}
};

end_package(std);

#endif
/* vim: set ft=lestes : */
