/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__std___broadcaster_hh___included
#define lestes__std___broadcaster_hh___included

#include <lestes/common.hh>
#include <lestes/std/broadcast_listener.hh>
#include <lestes/std/list.hh>

package(lestes);
package(std);

/*!
  \brief A template for broadcasting class.

  The concept of broadcasting is very similar to the concept of events.  At
  a certain point of the program's run there occurs an important event.  Such
  events can be divided into several categories.  The first category of events,
  and by far the most numerous, is the category that encompasses all events
  that spring from a single source.  These events are represented by the class
  event, q.v.  The second category consists of events, that affect an object of
  a given type.  These events could represent the creation / alteration
  / destruction of an object.  The second category of events need not
  necesarily come from a single source.  The remaining kinds of events are not
  discussed here.

  The second category of events, i.e. the events affecting a single object, can
  be handled by a simple means: all parties interested in the event would
  enqueue themselves in a designated place.  Then, at the moment of the event,
  the designated place will be accessed and all enqueued listeners would be
  notified.  The mechanics involved are the same as for the first category.
  However, the affected object shall be announced to the listeners as well.

 */
template <typename T, typename Y = T> class broadcaster : public object {
public:
	//! Inform the listeners, that the event has occured on the parameter.
	void broadcast(ptr < T > what)
	{
		typename list < srp < broadcast_listener < Y > > >::iterator it = listeners->begin(),
		     end = listeners->end();
		for ( ; it != end ; ++it) {
			(*it)->run(what);
		}
	}

	//! Register with the broadcaster to listen for the events.
	void attach(ptr < broadcast_listener < Y > > listener)
	{
		listeners->push_back(listener);
	}

	//! Create the broadcaster/event.
	static ptr < broadcaster < T, Y > > create()
	{
		return new broadcaster < T, Y >();
	}
protected:
	void gc_mark()
	{
		listeners.gc_mark();
		object::gc_mark();
	}

	broadcaster < T, Y >()
		: listeners(list < srp < broadcast_listener < Y > > >::create())
	{}
private:
	srp < list < srp < broadcast_listener < Y > > > > listeners;
};


end_package(std);
end_package(lestes);

#endif	// lestes__std___broadcaster_hh___included
