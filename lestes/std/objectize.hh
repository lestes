/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__std___objectize_hh___included
#define lestes__std___objectize_hh___included

/*! \file
  Support for dumping: objectize<>.
  \author rudo
*/
#include <lestes/common.hh>
#include <lestes/std/ucn_string.hh>
#include <lestes/std/dumper_visitor.hh>

package(lestes);
package(std);

/*!
 * Wraps simple types so that a ptr<> can exist to them.
 *
 * Thie wrappe is needed for the dumper to be simple :-)
 *
 * For all the lestes collections to work, specialization for T must have a
 * static create method that takes a value of type T (or a reference to T).
 */
template< typename T >
class objectize : public object {
private:
	//! this one is never meant to be instantiated, only the specializations are
	objectize();
};

//! Unary function object, wraps T in objectize<T>; used to transform collections of simple type
template< typename T >
class unary_objectizer {
public:
	ptr<object> operator() (const T &a)
	{
		return objectize<T>::create(a);
	}
};

template<>
class objectize<lint> : public object {
private:
	const lint value;
protected:
	objectize(lint a_value) : value(a_value)
	{}
public:
	static ptr< objectize<lint> > create(lint a_value)
	{
		return new objectize<lint>(a_value);
	}
	virtual void accept_dumper_visitor( ptr<dumper_visitor> v )
	{
		return v->visit_lint(value);
	}
};

template<>
class objectize<ulint> : public object {
private:
	const ulint value;
protected:
	objectize(ulint a_value) : value(a_value)
	{}
public:
	static ptr< objectize<ulint> > create(ulint a_value)
	{
		return new objectize<ulint>(a_value);
	}
	virtual void accept_dumper_visitor( ptr<dumper_visitor> v )
	{
		return v->visit_ulint(value);
	}
};

template<>
class objectize<bool> : public object {
private:
	const bool value;
protected:
	objectize(bool a_value) : value(a_value)
	{}
public:
	static ptr< objectize<bool> > create(bool a_value)
	{
		return new objectize<bool>(a_value);
	}
	virtual void accept_dumper_visitor( ptr<dumper_visitor> v )
	{
		return v->visit_bool(value);
	}
};

template<>
class objectize<lstring> : public object {
private:
	const lstring value;
protected:
	objectize(const lstring &a_value) : value(a_value)
	{}
public:
	static ptr< objectize<lstring> > create(const lstring &a_value)
	{
		return new objectize<lstring>(a_value);
	}
	virtual void accept_dumper_visitor( ptr<dumper_visitor> v )
	{
		return v->visit_lstring(value);
	}
};

template<>
class objectize<ucn_string> : public object {
private:
	const ucn_string value;
protected:
	objectize(const ucn_string &a_value) : value(a_value)
	{}
public:
	static ptr< objectize<ucn_string> > create(const ucn_string &a_value)
	{
		return new objectize<ucn_string>(a_value);
	}
	virtual void accept_dumper_visitor( ptr<dumper_visitor> v )
	{
		return v->visit_ucn_string(value);
	}
};

end_package(std);
end_package(lestes);

#endif	// lestes__std___objectize_hh___included
