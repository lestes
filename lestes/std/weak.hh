/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__std___weak_hh___included
#define lestes__std___weak_hh___included

#include <lestes/common.hh>

/*! \file
 * \brief Definition for weak pointers.
 *
 * The idea behind weak pointers is simple. The weak pointers will be linked
 * together in a double circular list. Their gc_mark() method will do nothing,
 * so as to support for the use of pointer.gc_mark(), in order to retain coherence
 * of pointer interface. After the gc_mark phase the list of live weak pointers
 * will be updated by mapping a function (gc_base's friend) that will clear the
 * internal pointer of each weak pointer that points to an unmarked object.
 * The objects themselves will be reclaimed only after this weak pointer update
 * phase was completed.
 *
 * The generator could be extended to handle weak pointers by means of specifying
 * the pointer class for each field:
 * 
 * - \<field name="..." type="..." pointer-kind="weak" /\>
 * - \<collection name="..." type="..." pointer-kind="weak" /\>
 *
 * As the template weak would have the same interface as ordinary srp or ptr, it
 * will not be necessary to modify other parts of the generator, namely neither
 * factory method, nor marking routine generation needs to be modified.
 */
package(lestes);
package(std);

template < typename T > class weak {
};

end_package(std);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
