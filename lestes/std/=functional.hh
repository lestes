/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__std___functional_hh___included
#define lestes__std___functional_hh___included

#include <lestes/common.hh>
#include <functional>

/*! \file
 * \brief Extension of standard C++ header \<functional\>
 *
 * This file contains extensions of standard C++ header \<functional\>.
 * The first extension is that the objects are now garbage collectible.
 * The second extension is that the objects now allow for the types
 * to be implicitly converted.
 */

package(lestes);
package(std);

/*!
 * \brief Base class for nullary functionals.
 */
template<typename Result>
class abstract_nullary_function : public object {
public:
	typedef Result result_type;
	virtual Result operator () () const abstract;
};

/*!
 * \brief adaptor for nullary function pointers.
 *
 * \param Result The result type this functional object yields.
 */

template<typename Result, typename ResultAdaptedFrom>
class result_adapted_pointer_to_nullary_function : public abstract_nullary_function<Result> {
public:
	virtual Result operator () () const { return (*pointer)(); }
	static ptr< result_adapted_pointer_to_nullary_function<Result, ResultAdaptedFrom> >
		create(ResultAdaptedFrom (*p)())
	{ return new result_adapted_pointer_to_nullary_function<Result,ResultAdaptedFrom>(p); }
protected:
	ResultAdaptedFrom (*pointer)();
	explicit result_adapted_pointer_to_nullary_function<Result,ResultAdaptedFrom>(
			ResultAdaptedFrom (*p)()) : pointer(p) {}
};

/*!
 * \brief Base class for unary functionals.
 * 
 * This class provides for functional objects of one argument, that can be used
 * as arguments to STL calls.  Garbage collectivity and STL fitness is achieved
 * by means of multiple inheritance.
 *
 * \param Argument The real argument type this functional object accepts.
 * \param Result The result type this functional object yields.
 */
template <typename Argument, typename Result>
class abstract_unary_function : public object, public ::std::unary_function<Argument, Result> {
public:
	virtual Result operator()(Argument a) const abstract;
};

/*!
 * \brief Adaptor for pointers to unary functions.
 *
 * This class provides for functional objects of one argument, that can be used
 * as arguments to STL calls.  Garbage collectivity and STL fitness is achieved
 * by means of multiple inheritance in the base class.
 *
 * \param Argument The real argument type this functional object accepts.
 * \param Result The result type this functional object yields.
 * \param ArgumentAdaptedFrom The type of parameter to the underlying function pointer.
 * \param ResultAdaptedFrom The result type of the underlying function pointer.
 */
template <typename Argument, typename Result,
	typename ArgumentAdaptedFrom = Argument, typename ResultAdaptedFrom = Result>
class pointer_to_unary_function : public abstract_unary_function<Argument, Result> {
	explicit pointer_to_unary_function(ResultAdaptedFrom (*p)(ArgumentAdaptedFrom))
		: pointer(p) {}
public:
	virtual Result operator()(Argument a) const
	{ return pointer(a); }
	static ptr<pointer_to_unary_function> create(ResultAdaptedFrom (*p)(ArgumentAdaptedFrom))
	{ return ptr<pointer_to_unary_function>(new pointer_to_unary_function(p)); }
protected:
	ResultAdaptedFrom (*pointer)(ArgumentAdaptedFrom);
	explicit pointer_to_unary_function(ResultAdaptedFrom (*ptr)(ArgumentAdaptedFrom))
		: pointer(ptr) {}
};


/*!
 * \brief Adaptor for pointers to unary functions.
 *
 * This class provides a subset of functionality of the class pointer_to_unary_function.
 * It is designed to be simpler to use in case of wrapping objects' unary factory methods.
 *
 * \param Argument The real argument type this functional object accepts, also
 * the parameter type of the underlying function pointer.
 * \param Result The result type this functional object yields.
 * \param ResultAdaptedFrom The result type of the underlying function pointer.
 */
template <typename Argument, typename Result, typename ResultAdaptedFrom>
class result_adapted_pointer_to_unary_function
: public pointer_to_unary_function<Argument, Result, Argument, ResultAdaptedFrom> {
public:
	static ptr<result_adapted_pointer_to_unary_function>
		create(ResultAdaptedFrom (*p)(ArgumentAdaptedFrom))
	{
		return ptr<result_adapted_pointer_to_unary_function>(
				new result_adapted_pointer_to_unary_function(p));
	}
protected:
	explicit result_adapted_pointer_to_unary_function(
			ResultAdaptedFrom (*p)(ArgumentAdaptedFrom))
		: pointer_to_unary_function(p) {}
};

/*!
 * \brief Adaptor for pointers to binary functions.
 *
 * This class provides for functional objects of two argument, that can be used
 * as arguments to STL calls.  Garbage collectivity and STL fitness is achieved
 * by means of multiple inheritance.
 */
template <typename Argument1, typename Argument2, typename Result>
class abstract_binary_function
: public object, public ::std::binary_function<Argument1, Argument2, Result> {
public:
	virtual Result operator()(Argument1 a1, Argument2 a2) const abstract;
};

/*!
 * \brief Adaptor for pointers to binary functions.
 *
 * This class provides for functional objects of two argument, that can be used
 * as arguments to STL calls.  Garbage collectivity and STL fitness is achieved
 * by means of multiple inheritance in the base class.
 *
 * \param Argument1 The real first argument type this functional object accepts.
 * \param Argument2 The real second argument type this functional object accepts.
 * \param Result The result type this functional object yields.
 * \param Argument1AdaptedFrom The type of first parameter to the underlying function pointer.
 * \param Argument2AdaptedFrom The type of second parameter to the underlying function pointer.
 * \param ResultAdaptedFrom The result type of the underlying function pointer.
 *
 */
template <typename Argument1, typename Argument2, typename Result,
	typename Argument1AdaptedFrom = Argument1,
	typename Argument2AdaptedFrom = Argument2,
	typename ResultAdaptedFrom = Result>
class pointer_to_binary_function
	: public object, public ::std::binary_function<Argument1, Argument2, Result> {
public:
	static ptr<pointer_to_binary_function>
		create(ResultAdaptedFrom (*p)(Argument1AdaptedFrom, Argument2AdaptedFrom))
	{ return ptr<pointer_to_binary_function>(new pointer_to_binary_function(p)); }
	virtual Result operator()(Argument1 a1, Argument2 a2) const
	{ return pointer(a1, a2); }
protected:
	ResultAdaptedFrom (*pointer)(Argument1AdaptedFrom, Argument2AdaptedFrom);
	explicit pointer_to_binary_function(
			ResultAdaptedFrom (*p)(Argument1AdaptedFrom, Argument2AdaptedFrom))
		: pointer(p) {}
};

/*!
 * \brief Adaptor for pointers to binary functions.
 *
 * This class provides a subset of functionality of the class pointer_to_binary_function.
 * It is designed to be simpler to use in case of wrapping objects' binary factory methods.
 *
 * \param Argument1 The real first argument type this functional object accepts, also
 * the first parameter type of the underlying function pointer.
 * \param Argument2 The real second argument type this functional object accepts, also
 * the second parameter type of the underlying function pointer.
 * \param Result The result type this functional object yields.
 * \param ResultAdaptedFrom The result type of the underlying function pointer.
 */
template <typename Argument1, typename Argument2, typename Result, typename ResultAdaptedFrom>
class result_adapted_pointer_to_binary_function
	: public pointer_to_binary_function<Argument1, Argument2, Result,
		Argument1, Argument2, ResultAdaptedFrom> {
public:
	static ptr<result_adapted_pointer_to_binary_function>
		create(ResultAdaptedFrom (*p)(Argument1AdaptedFrom, Argument2AdaptedFrom))
	{
		return ptr<result_adapted_pointer_to_binary_function>(
				new result_adapted_pointer_to_binary_function(p));
	}
protected:
	explicit result_adapted_pointer_to_binary_function(
			ResultAdaptedFrom (*p)(Argument1AdaptedFrom, Argument2AdaptedFrom))
		: pointer_to_binary_function(p) {}
};

end_package(std);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
