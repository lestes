/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__std___set_hh___included
#define lestes__std___set_hh___included

#include <lestes/common.hh>
#include <lestes/std/collection_refl.hh>
#include <lestes/std/list.hh>
#include <set>

/*! \file
  \brief Collectible ::std::set
  \author rudo

  Encapsulation for STL set class to be compatible with
  our garbage collector. Includes namely marking routine
  and factory method.
 */

package(lestes);
package(std);


template <typename Key, typename Compare = ::std::less<Key> >
class set : public object, public ::std::set<Key, Compare>
{
public:
	static ptr< set<Key,Compare> > create()
	{
		return new set();
	}
	static ptr< set<Key,Compare> > create( ptr< set<Key,Compare> > from )
	{
		return new set(from);
	}
	virtual ptr<reflection_list> reflection_get() const
	{
		if (!collection_refl::set_simple)
			collection_refl::set_simple_init( object::reflection_get() );
		return collection_refl::set_simple;
	}
	virtual ptr<field_list_list> field_values_get() const
	{
		ptr<field_list_list> result = object::field_values_get();
		result->push_back( value_list::create() );
		// wrap all items in objectize, insert the result onto the just created value_list
		transform( this->begin(), this->end(),
				back_inserter( *result->back() ), unary_objectizer<Key>() );
		return result;
	}
protected:
	set() : object(), ::std::set<Key,Compare>()
	{}
	set( ptr< set<Key,Compare> > from ) : object(), ::std::set<Key,Compare>( *checked(from) )
	{}
};


template <typename Key>
class set< srp<Key>, ::std::less< srp<Key> > > :
	public object, public ::std::set< srp<Key>, ::std::less< srp<Key> > >
{
public:
	static ptr< set< srp<Key>, ::std::less< srp<Key> > > > create()
	{
		return new set();
	}
	static ptr< set< srp<Key> > > create( ptr< set< srp<Key> > > from )
	{
		return new set(from);
	}
	virtual ptr<reflection_list> reflection_get() const
	{
		if (!collection_refl::set_srp)
			collection_refl::set_srp_init( object::reflection_get() );
		return collection_refl::set_srp;
	}
	virtual ptr<field_list_list> field_values_get() const
	{
		ptr<field_list_list> result = object::field_values_get();
		result->push_back( value_list::create() );
		result->back()->insert( result->back()->end(), this->begin(), this->end() );
		return result;
	}
protected:
	set() : object(), ::std::set< srp<Key>, ::std::less< srp<Key> > >()
	{}
	set( ptr< set< srp<Key> > > from )
		: object(), ::std::set< srp<Key>, ::std::less< srp<Key> > >( *checked(from) )
	{}
	void gc_mark()
	{
		object::gc_mark();
		for (typename set::iterator i = this->begin(); i != this->end(); ++i)
			i->gc_mark();
	}
};

template <typename Key, typename Compare>
class set< srp<Key>, Compare > :
	public object, public ::std::set< srp<Key>, Compare >
{
public:
	static ptr< set< srp<Key>, Compare > > create()
	{
		return new set();
	}
	static ptr< set< srp<Key>, Compare > > create( ptr< set< srp<Key>, Compare > > from )
	{
		return new set(from);
	}
	virtual ptr<reflection_list> reflection_get() const
	{
		if (!collection_refl::set_srp)
			collection_refl::set_srp_init( object::reflection_get() );
		return collection_refl::set_srp;
	}
	virtual ptr<field_list_list> field_values_get() const
	{
		ptr<field_list_list> result = object::field_values_get();
		result->push_back( value_list::create() );
		result->back()->insert( result->back()->end(), this->begin(), this->end() );
		return result;
	}
protected:
	set() : object(), ::std::set< srp<Key>, Compare >()
	{}
	set( ptr< set< srp<Key>, Compare > > from )
		: object(), ::std::set< srp<Key>, Compare >( *checked(from) )
	{}
	void gc_mark()
	{
		object::gc_mark();
		for (typename set::iterator i = this->begin(); i != this->end(); ++i)
			i->gc_mark();
	}
};


end_package(std);
end_package(lestes);

#endif	// lestes__std___set_hh___included
/* vim: set ft=lestes : */
