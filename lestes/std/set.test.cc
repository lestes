/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/common.hh>
#include <lestes/std/set.hh>

#include <algorithm>

package(lestes);
package(std);

struct integer : public object {
	int value;
	static ptr<integer> create( int i )
	{
		integer * result = new integer();
		result->value = i;
		return result;
	}
};
struct smart_int : public object {
	int value;
	bool operator < ( const smart_int &si )
	{
		return value < si.value;
	}
	static ptr<smart_int> create( int i )
	{
		smart_int * result = new smart_int();
		result->value = i;
		return result;
	}
};

template<typename T>
struct test_less {
	bool operator()( T x, T y )
	{
		return x < y;
	}
};

template<>
struct test_less< srp<integer> > {
	bool operator()( srp<integer> x, srp<integer> y )
	{
		return x->value > y->value;
	}
};

template<typename T>
struct deref_less {
	bool operator()( ptr<T> x, ptr<T> y )
	{
		return *x < *y;
	}
};

template<typename T>
struct deref_eqeq {
	bool operator()( ptr<T> x, ptr<T> y )
	{
		return x->value == y->value;
	}
};

template class set< int >;
template class set< int, test_less<int> >;
template class set< srp<integer> >;
template class set< srp<integer>, deref_less<integer> >;

typedef set< int >					set_i;
typedef set< int,		test_less<int> >	set_ic;
typedef set< srp<integer> >				set_p;
typedef set< srp<smart_int>,	deref_less<smart_int> >	set_pc;

int set_test()
{
	ptr<set_i> s1 = set_i::create();
	ptr<set_i> s2 = set_i::create();
	s1->insert(0);
	s2->insert(0);
	lassert( *s1 == *s2 );
	s2->insert(1);
	lassert( *s1 != *s2 );

	ptr<set_pc> x1 = set_pc::create();
	ptr<set_pc> x2 = set_pc::create();

	ptr<smart_int> p = smart_int::create(1);

	x1->insert( p );
	x2->insert( p );
	lassert( *x1 == *x2 );

	x1->insert( smart_int::create(2) );
	x2->insert( smart_int::create(2) );
	lassert( x1->size() == x2->size() );
	deref_eqeq<smart_int> comparator;
	lassert( equal( x1->begin(), x1->end(), x2->begin(), comparator ) );

	x1->insert( smart_int::create(3) );
	x2->insert( smart_int::create(4) );
	lassert( x1->size() == x2->size() );
	lassert( !equal( x1->begin(), x1->end(), x2->begin(), comparator ) );

	ptr<set_pc> x3 = set_pc::create(x2);
	lassert( equal( x2->begin(), x2->end(), x3->begin(), comparator ) );

	return 0;
}

end_package(std);
end_package(lestes);

extern "C"
int main()
{
	return lestes::std::set_test();
}
