/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__std___vector_hh___included
#define lestes__std___vector_hh___included

#include <lestes/common.hh>
#include <lestes/std/collection_refl.hh>
#include <lestes/std/list.hh>
#include <lestes/std/objectize.hh>
#include <vector>
#include <iterator>
#include <algorithm>

/*! \file
  \brief Collectible ::std::vector
  \author egg

  Encapsulation for STL vector class to be compatible with
  our garbage collector. Includes namely marking routine
  and factory method.
 */

package(lestes);
package(std);

template< typename T >
class vector : public object, public ::std::vector<T> {
public:
	//! Factory method, creates an empty vector.
	static ptr< vector<T> > create();
#if 0
	//! Factory method, creates a vector from a range
	template<typename InputIterator>
	static ptr< vector < T > > create(InputIterator first, InputIterator last);
#endif
	//! Factory method creates a copy of given vector.
	static ptr< vector<T> > create( ptr< vector<T> > );
	virtual ptr<reflection_list> reflection_get() const;
	virtual ptr<field_list_list> field_values_get() const;
protected:
	//! Constructor for an empty vector.
	vector();
	//! Imitation of copy-constructor, *takes a pointer*
	vector( ptr< vector<T> > );
#if 0
	//! construct from range
	template<typename InputIterator>
	vector(InputIterator first, InputIterator last) : ::std::vector < T >(first,last) {}
#endif
};

template< typename T >
class vector< srp<T> > : public object, public ::std::vector< srp<T> > {
public:
	//! Factory method, creates an empty vector.
	static ptr< vector< srp<T> > > create();
#if 0
	//! Factory method, creates a vector from a range
	template<typename InputIterator>
	static ptr< vector < srp < T > > > create(InputIterator first, InputIterator last);
#endif
	//! Factory method creates a copy of given vector.
 	static ptr< vector< srp<T> > > create( ptr< vector< srp<T> > > );
	virtual ptr<reflection_list> reflection_get() const;
	virtual ptr<field_list_list> field_values_get() const;
protected:
	//! Constructor for an empty vector.
	vector();
	//! Imitation of copy-constructor, *takes a pointer*
	vector( ptr< vector< srp<T> > > );
#if 0
	//! construct from range
	template<typename InputIterator>
	vector(InputIterator first, InputIterator last) : ::std::vector < srp < T > >(first,last) {}
#endif
	//! Marking routine for class ::lestes::std::vector
	void gc_mark(void);
};

template< typename T >
ptr< vector<T> > vector<T>::create() {
	return new vector<T>();
}

template< typename T >
ptr< vector<T> > vector<T>::create( ptr< vector<T> > from ) {
	return new vector<T>(from);
}

template< typename T >
ptr<object::reflection_list> vector<T>::reflection_get() const
{
	if (!collection_refl::vector_simple)
		collection_refl::vector_simple_init( object::reflection_get() );
	return collection_refl::vector_simple;
}

template< typename T >
ptr<object::field_list_list> vector<T>::field_values_get() const
{
	ptr<field_list_list> result = object::field_values_get();
	result->push_back( value_list::create() );
	// wrap all items in objectize, insert the result onto the just created value_list
	transform( this->begin(), this->end(),
			back_inserter( *result->back() ), unary_objectizer<T>() );
	return result;
}

template< typename T >
vector<T>::vector()
	: object(), ::std::vector<T>()
{}

template< typename T >
vector<T>::vector( ptr< vector<T> > from )
	: object(), ::std::vector<T>( *checked(from) )
{}

template< typename T >
ptr< vector< srp<T> > > vector< srp<T> >::create() {
	return new vector< srp<T> >();
}

template< typename T >
ptr< vector< srp<T> > > vector< srp<T> >::create( ptr< vector< srp<T> > > from )
{
	return new vector< srp<T> >(from);
}

#if 0
template<typename T>
template<typename InputIterator>
ptr < vector < T > > vector < T >::create(InputIterator first, InputIterator last)
{
	return new vector < T >(first, last);
}

template<typename T>
template<typename InputIterator>
ptr < vector < srp < T > > > vector < srp < T > >::create(InputIterator first, InputIterator last)
{
	return new vector < srp < T > >(first, last);
}

#endif

template< typename T >
ptr<object::reflection_list> vector< srp<T> >::reflection_get() const
{
	if (!collection_refl::vector_srp)
		collection_refl::vector_srp_init( object::reflection_get() );
	return collection_refl::vector_srp;
}

template< typename T >
ptr<object::field_list_list> vector< srp<T> >::field_values_get() const
{
	ptr<field_list_list> result = object::field_values_get();
	result->push_back( value_list::create() );
	result->back()->insert( result->back()->end(), this->begin(), this->end() );
	return result;
}

template< typename T >
vector< srp<T> >::vector()
	: object(), ::std::vector< srp<T> >()
{ }

template< typename T >
vector< srp<T> >::vector( ptr< vector< srp<T> > > from )
	: object(), ::std::vector< srp<T> >( *checked(from) )
{}

template< typename T >
void vector< srp<T> >::gc_mark() {
	// gc_mark each member
	for (typename vector::iterator i = this->begin(); i != this->end(); ++i)
		i->gc_mark();
	object::gc_mark();	// inherited gc_mark
}

end_package(std);
end_package(lestes);

#endif	// lestes__std___vector_hh___included
/* vim: set ft=lestes : */
