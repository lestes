#ifndef lestes__debug_v2__debug__debug_hh__included
#define lestes__debug_v2__debug__debug_hh__included

/*! \file
	\brief Debugging stuff.
	\author jaz
*/

#include <fstream>
#include <iostream>
#include <sstream>
#include <lestes/common.hh>

package(lestes);
package(backend_v2);
package(debug);

void b_dump(ptr<object> o, const char* filename);

end_package(debug);
end_package(backend_v2);
end_package(lestes);

#endif 
