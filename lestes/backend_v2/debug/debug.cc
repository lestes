/*! \file
	\brief Debugging stuff.
	\author jaz
*/

#include <lestes/backend_v2/common.hh>
#include <lestes/backend_v2/debug/debug.hh>
#include <lestes/std/dumper.hh>

package(lestes);
package(backend_v2);
package(debug);

/*!
	\brief Dumps object through ::lestes::std::dumper into a file. 
	
	Note: For debugging purposes.
	
	\param o The object to be dumped.
	\param id Portion of the output filename. Resulting filename is extended to form ./dump/$id$.dmp.
*/
void b_dump(ptr<object> o, const char* id) {
	::std::ostringstream oss;
	oss << "./dump/" << id << ".dmp";
	::std::ofstream f1(oss.str().c_str());
	dumper::dump(f1, o);
	f1.close();
}


end_package(debug);
end_package(backend_v2);
end_package(lestes);
