#ifndef lestes__backend_v2__common_hh__included
#define lestes__backend_v2__common_hh__included

/*! \file
	\brief Common header for backend_v2 tree.
	\author jaz
*/

#include <lestes/common.hh>
#include <lestes/std/dumper.hh>
#include <lestes/msg/logger.hh>
#include <lestes/msg/logger_util.hh>
#include <lestes/std/reflect.hh>
#include <fstream>
#include <iostream>
#include <sstream>

/*!
	\brief Switch on/off debugging output.
*/
//#define BACKEND_V2_DEBUG 1

#ifdef BACKEND_V2_DEBUG
#include <lestes/backend_v2/debug/debug.hh>
#endif

package(lestes);
package(backend_v2);

//Root logger for backend_v2.
declare_logger(backend_v2_logger);

end_package(backend_v2);
end_package(lestes);

#endif 
