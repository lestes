#include <lestes/std/vector.hh>
#include <lestes/backend_v2/backend.g.hh>
#include <lestes/backend_v2/debug/debug.hh>
#include <lestes/backend_v2/interface/backend_data_builder.g.hh>
#include <lestes/backend_v2/structs/func_data.g.hh>
#include <lestes/backend_v2/workers/ge_generator.g.hh>
#include <lestes/backend_v2/workers/order_governor.g.hh>
#include <lestes/backend_v2/workers/scheduler.g.hh>
#include <lestes/backend_v2/workers/bb_finder.g.hh>
#include <lestes/backend_v2/workers/alloc_int_finder.g.hh>
#include <lestes/backend_v2/workers/linscan_regalloc.g.hh>
#include <lestes/backend_v2/workers/spillgen.g.hh>
#include <lestes/backend_v2/workers/simple_spillgen.g.hh>
#include <lestes/backend_v2/workers/body_preprocessor.g.hh>
#include <lestes/backend_v2/workers/literal_inliner.g.hh>
#include <lestes/backend_v2/workers/memory_inliner.g.hh>
#include <lestes/backend_v2/workers/dead_code_eliminator.g.hh>
#include <lestes/backend_v2/workers/useless_code_eliminator.g.hh>
#include <lestes/backend_v2/workers/pi_cond_jmp_rewriter.g.hh>
#include <lestes/md/tasm/asm_generator.g.hh>
#include <lestes/md/mem/memory_allocators.g.hh>
#include <lestes/md/symbols/symbol_register.g.hh>
#include <lestes/md/functions/preasmgen_body_changes.g.hh>

package(lestes);
package(backend_v2);

using namespace ::lestes::msg;
using namespace ::lestes::backend_v2::interface;
using namespace ::lestes::backend_v2::structs;
using namespace ::lestes::backend_v2::workers;
using namespace ::lestes::backend_v2::debug;
using namespace ::lestes::md::tasm;
using namespace ::lestes::md::mem;
using namespace ::lestes::md::symbols;
using namespace ::lestes::md::functions;

initialize_top_logger( backend_v2_logger, "backend_v2" );

/*!
	\brief The backend's main.
*/
void backend::main() {

	backend_v2_logger << "main - start\n" << eolog;
	
	ptr<vector<srp<builder_func_data> > > builder_data = backend_data_builder::instance()->get_result();
	ptr<vector<srp<func_data> > > tu_data = vector<srp<func_data> >::create();
	
	for(ulint i=0; i<builder_data->size(); ++i) {
		
		backend_v2_logger << "function body processing - start\n" << eolog;
		
		ptr<builder_func_data> bfd = (*builder_data)[i]; 
		
		b_dump(bfd,"function_data");
		
		ptr<func_data> fd = func_data::create(bfd->function_decl_get());
		fd->pi_body_set(bfd->pi_body_get()); 

		backend_v2_logger << "registering function in symbol register\n" << eolog;
		symbol_register::instance()->register_symbol(fd->function_decl_get());
		
		if ( optimization_conditional_jumps ) {
			backend_v2_logger << "optimizing conditional jumps\n" << eolog;
			ptr<pi_cond_jmp_rewriter> jmps = pi_cond_jmp_rewriter::create(fd);
			jmps->process();
			fd = jmps->get_result();
		}
		
		backend_v2_logger << "ordering pi-pseudoinstructions\n" << eolog;
		ptr<order_governor> linearizator = order_governor::create(fd);
		linearizator->process();
		fd = linearizator->get_result();
		
		backend_v2_logger << "generating ge-code\n" << eolog;
		ptr<ge_generator> ge_gen = ge_generator::create(fd);
		ge_gen->process();
		fd = ge_gen->get_result();
		
		if ( optimization_immediate_inlining ) {
			backend_v2_logger << "inlining immediates\n" << eolog;
			ptr<literal_inliner> lit_inliner = literal_inliner::create(fd);
			lit_inliner->process();
			fd = lit_inliner->get_result();
		}
		
		
		if ( optimization_memory_inlining ) {
			backend_v2_logger << "inlining memory operands\n" << eolog;
			ptr<memory_inliner> mem_inliner = memory_inliner::create(fd);
			mem_inliner->process();
			fd = mem_inliner->get_result();
		}
		
		if ( optimization_deadcode_elimination ) {
			backend_v2_logger << "dead code elimination\n" << eolog;
			ptr<dead_code_eliminator> dead = dead_code_eliminator::create(fd);
			dead->process();
			fd = dead->get_result();
		}
		
		backend_v2_logger << "preprocessing body\n" << eolog;
		ptr<body_preprocessor> bp = body_preprocessor::create(fd);
		bp->process();
		fd = bp->get_result();
		
		backend_v2_logger << "analyzing basic blocks\n" << eolog;
		ptr<bb_finder> bb = bb_finder::create(fd);
		bb->process();
		fd = bb->get_result();
		
		backend_v2_logger << "scheduling ge-code - pass 1\n" << eolog;
		ptr<scheduler> sched = scheduler::create(fd);
		if ( !optimization_code_scheduling ) {
			sched->dumb_scheduling_set(true);
		}
		sched->process();
		fd = sched->get_result();
				
		backend_v2_logger << "analyzing intervals for register allocation and spill code generation\n" << eolog;
		ptr<alloc_int_finder> ints = alloc_int_finder::create(fd);
		ints->process();
		fd = ints->get_result();
		
		if ( optimization_register_allocation ) {
			backend_v2_logger << "register allocation\n" << eolog;
			ptr<linscan_regalloc> regalloc = linscan_regalloc::create(fd);
			regalloc->process();
			fd = regalloc->get_result();
		}
		
		backend_v2_logger << "spillcode generation - pass 1\n" << eolog;
		ptr<spillgen> spill = spillgen::create(fd);
		spill->process();
		fd = spill->get_result();
		
		backend_v2_logger << "scheduling ge-code - pass 2\n" << eolog;
		sched = scheduler::create(fd);
		if ( !optimization_code_scheduling ) {
			sched->dumb_scheduling_set(true);
		}
		sched->process();
		fd = sched->get_result();
		
		backend_v2_logger << "spillcode generation - pass 2\n" << eolog;
		ptr<simple_spillgen> simple_spill = simple_spillgen::create(fd);
		simple_spill->process();
		fd = simple_spill->get_result();
		
		if ( optimization_uselesscode_elimination ) {
			backend_v2_logger << "useless code elimination\n" << eolog;
			ptr<useless_code_eliminator> useless = useless_code_eliminator::create(fd);
			useless->process();
			fd = useless->get_result();
		}
		
		backend_v2_logger << "computing placement of the function's local variables\n" << eolog;
		local_variable_allocator::instance(fd->function_decl_get())->calculate_placement();
		
		ptr<preasmgen_body_changes> preasm = preasmgen_body_changes::create(fd->function_decl_get(),fd->ge_body_get());
		preasm->process_body();
		fd->ge_body_set(preasm->get_result());
		
		tu_data->push_back(fd);	
		
		backend_v2_logger << "function body processing - end\n" << eolog;	
	}
	
	backend_v2_logger << "computing placement of global variables\n" << eolog;
	global_variable_allocator::instance()->calculate_placement();
	
	backend_v2_logger << "generating asm-code for TU\n" << eolog;
	ptr<asm_generator> asm_gen = asm_generator::create(output);
	asm_gen->generate_tu_prologue();
	asm_gen->generate_tu_body(tu_data);
	asm_gen->generate_tu_epilogue();
	
	backend_v2_logger << "main - end\n" << eolog;
}

end_package(backend_v2);
end_package(lestes);



