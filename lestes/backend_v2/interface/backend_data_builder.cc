/*!
	\file
	\brief Data builder class.
	\author jaz
*/
#include <lestes/backend_v2/interface/backend_data_builder.g.hh>
#include <lestes/backend_v2/intercode/pi.g.hh>
#include <lestes/lang/cplus/sem/ss_misc.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>
#include <lestes/msg/logger.hh>
#include <lestes/msg/logger_util.hh>
#include <lestes/std/reflect.hh>

package(lestes);
package(backend_v2);
package(interface);

using ::lestes::lang::cplus::sem::ss_function_declaration;
using ::lestes::backend_v2::intercode::pi_pi;
using ::lestes::backend_v2::intercode::pi_sp;
using ::lestes::backend_v2::structs::backend_data;

using namespace ::lestes::msg;

typedef vector<srp<builder_func_data> > builder_func_data__vector;

declare_logger(log);
initialize_logger( log, "backend_v2_data_builder", backend_v2_logger );

/*!
	\brief Returns instance of builder.
*/
ptr<backend_data_builder> backend_data_builder::instance() {
	if ( !singleton_instance ) {
		//An singleton instance has not been created yet. Create it!
		singleton_instance_set(backend_data_builder::create());
	}
	//Return instance.
	return singleton_instance;
}


/*!
	\brief Returns builder's result.
*/
ptr<builder_func_data__vector> backend_data_builder::get_result(){
	return functions;
}

/*!
	\brief Adds a sequencepoint to the currently built function's body.
	
	\param sp The sequencepoint.
*/
void backend_data_builder::add_sp(ptr<pi_sp> sp) {
	lassert(sp);
	
	//Add sp to the body.
    current_function_body->pi_body_get()->push_back(sp);

#ifdef BACKEND_V2_DEBUG
	::std::ostringstream oss;
	
    oss << "function(" << current_function_body->function_decl_get()->uid_get() << "): pi_sp(" << sp->uid_get() << ")";
	
	oss << "[psp=";
	if (sp->psp_get()) 
		oss << sp->psp_get()->uid_get();
    else 
		oss << "NULL";
		
	oss << ",nsp=";
	if (sp->nsp_get()) 
		oss << sp->nsp_get()->uid_get();
    else 
		oss << "NULL";
	oss <<",level=" << sp->level_get() << "] added\n";
	
	log << oss.str() << eolog;
#endif

}

/*!
	\brief Adds a pseudoinstruction to the currently built function's body.
	
	\param pi The pseudoinstruction.
*/
void backend_data_builder::add_pi(ptr<pi_pi> pi) {
	lassert(pi);
	
    current_function_body->pi_body_get()->push_back(pi);
    
#ifdef BACKEND_V2_DEBUG	
	::std::ostringstream oss;
	
	oss << "function(" << current_function_body->function_decl_get()->uid_get() << "): " << pi->reflection_get()->back()->name_get() << "(" << pi->uid_get() << ")";
	
	oss << "[psp=";
	if (pi->psp_get()) 
		oss << pi->psp_get()->uid_get();
    else 
		oss << "NULL";
		
	oss << ",nsp=";
	if (pi->nsp_get()) 
		oss << pi->nsp_get()->uid_get();
    else 
		oss << "NULL ";
	oss <<",level=" << pi->level_get() << "] added\n";
	
	log << oss.str() << eolog;
#endif
}

/*!
	\brief Adds a list of pseudoinstructions to the currently built function's body.
	
	\param pis The list to be added.
*/
void backend_data_builder::add_pis( ptr < ::lestes::std::list < srp<pi_pi> > > pis) {
	//Add pis to the body.
    for (::lestes::std::list < srp<pi_pi > >::iterator i = pis->begin(); i != pis->end(); i++) {
    	add_pi(*i);
    }
}


/*!
	\brief Finishes processing of particular sematic structure.
	
	For debuging purposes.
*/
void backend_data_builder::add_rail() {
    //for debug purposes only
	log << "function(" << current_function_body->function_decl_get()->uid_get() <<"): rail added\n" << eolog;
}


/*!
	\brief Initiates processing of another function's body.
	
	\param function The new function.
*/
void backend_data_builder::add_function_start(ptr<ss_function_declaration> function) {
	//Assure that we are not inside an function body. Function nesting is not allowed.
	lassert(!current_function_body);

	ptr<builder_func_data> next_body = builder_func_data::create(function);
	
	//Get ready for processing of the function	
	current_function_body = next_body;
	
	log << "function(" << function->uid_get() <<"): begin\n" << eolog;
}

/*!
	\brief Finishes processing of the current function's body.
*/
void backend_data_builder::add_function_end() {
	//Assure that we are inside an function body.
	lassert(current_function_body);
	
	//Finish processing of the current function and insert it to the list of translation unit's functions.
	functions->push_back(current_function_body);	
		
	log << "function(" << current_function_body->function_decl_get()->uid_get() <<"): end\n" << eolog;
	
	//Clear fields holding info about processed function.
	current_function_body = NULL;
}

end_package(interface);
end_package(backend_v2);
end_package(lestes);

