#include <lestes/backend_v2/intercode/visitor_pi_pi2pi_operands.g.hh>
#include <lestes/backend_v2/intercode/pi.g.hh>
#include <lestes/backend_v2/structs/pi_operands.g.hh>
#include <lestes/md/types/tm_data_type_base.g.hh>

package(lestes);
package(backend_v2);
package(intercode);

using ::lestes::md::types::tm_data_type_base;
using ::lestes::backend_v2::structs::pi_operands;

typedef vector<srp<pi_operand> > pi_operand_vector__type;
typedef list<srp<pi_mem> > pi_mem_list_type;
typedef list<srp<pi_operand> > pi_operand_list_type;
typedef vector<srp<tm_data_type_base> > tm_data_type_vector__type;
typedef vector<srp<pi_sp> > pi_sp_vector__type;

#define CREATE_PI_OPERANDS_LISTS()  \
	ptr<pi_operand_vector__type> in_ops = pi_operand_vector__type::create(); \
	ptr<pi_operand_vector__type> out_ops = pi_operand_vector__type::create(); \
	ptr<tm_data_type_vector__type> in_types = tm_data_type_vector__type::create(); \
	ptr<tm_data_type_vector__type> out_types = tm_data_type_vector__type::create(); \
	ptr<pi_sp_vector__type> jmp_targets = pi_sp_vector__type::create(); 



ptr< pi_operands > visitor_pi_pi2pi_operands::tstpi_operands_get(ptr< pi_tstpi > pi) {
	CREATE_PI_OPERANDS_LISTS();
	
	in_ops->push_back(pi->left_get());
	in_types->push_back(pi->type_get());
	in_ops->push_back(pi->right_get());
	in_types->push_back(pi->type_get());
	
	out_ops->push_back(pi->destination_get());
	out_types->push_back(pi->type_get());
	
	return pi_operands::create(in_ops, out_ops, in_types, out_types, jmp_targets);
}

ptr< pi_operands > visitor_pi_pi2pi_operands::tdtpi_operands_get(ptr< pi_tdtpi > pi){
	CREATE_PI_OPERANDS_LISTS();
	
	in_ops->push_back(pi->left_get());
	in_types->push_back(pi->type2_get());
	in_ops->push_back(pi->right_get());
	in_types->push_back(pi->type2_get());
	
	out_ops->push_back(pi->destination_get());
	out_types->push_back(pi->type1_get());
	
	return pi_operands::create(in_ops, out_ops, in_types, out_types, jmp_targets);
	
}

ptr< pi_operands > visitor_pi_pi2pi_operands::bstpi_operands_get(ptr< pi_bstpi > pi){
	CREATE_PI_OPERANDS_LISTS();
	
	in_ops->push_back(pi->source_get());
	in_types->push_back(pi->type_get());
	
	out_ops->push_back(pi->destination_get());
	out_types->push_back(pi->type_get());
	
	return pi_operands::create(in_ops, out_ops, in_types, out_types, jmp_targets);
}

ptr< pi_operands > visitor_pi_pi2pi_operands::bdtpi_operands_get(ptr< pi_bdtpi > pi){
	CREATE_PI_OPERANDS_LISTS();
	
	in_ops->push_back(pi->source_get());
	in_types->push_back(pi->type2_get());
	
	out_ops->push_back(pi->destination_get());
	out_types->push_back(pi->type1_get());
	
	return pi_operands::create(in_ops, out_ops, in_types, out_types, jmp_targets);
}

ptr< pi_operands > visitor_pi_pi2pi_operands::st_move_operands_get(ptr< pi_abstract_move_st > pi){
	CREATE_PI_OPERANDS_LISTS();
	
	in_ops->push_back(pi->source_get());
	in_types->push_back(pi->type_get());
	
	out_ops->push_back(pi->destination_get());
	out_types->push_back(pi->type_get());
	
	return pi_operands::create(in_ops, out_ops, in_types, out_types, jmp_targets);
}

ptr< pi_operands > visitor_pi_pi2pi_operands::dt_move_operands_get(ptr< pi_abstract_move_dt > pi){
	CREATE_PI_OPERANDS_LISTS();
	
	in_ops->push_back(pi->source_get());
	in_types->push_back(pi->type2_get());
	
	out_ops->push_back(pi->destination_get());
	out_types->push_back(pi->type1_get());
	
	return pi_operands::create(in_ops, out_ops, in_types, out_types, jmp_targets);
}

ptr< pi_operands > visitor_pi_pi2pi_operands::sbp_operands_get(ptr< pi_indirect_store > pi){
	CREATE_PI_OPERANDS_LISTS();
	
	out_ops->push_back(pi->address_get());
	out_types->push_back(pi->type1_get());
	in_ops->push_back(pi->value_get());
	in_types->push_back(pi->type2_get());
	in_ops->push_back(pi->address_get()->factory_get().dncast<pi_mf_ptr_deref>()->addr_get());
	in_types->push_back(pi->address_get()->factory_get().dncast<pi_mf_ptr_deref>()->addr_get()->type_get());
	
	return pi_operands::create(in_ops, out_ops, in_types, out_types, jmp_targets);
}

ptr< pi_operands > visitor_pi_pi2pi_operands::lbp_operands_get(ptr< pi_indirect_load > pi){
	CREATE_PI_OPERANDS_LISTS();
	
	out_ops->push_back(pi->value_get());
	out_types->push_back(pi->type1_get());
	
	in_ops->push_back(pi->address_get());
	in_types->push_back(pi->type2_get());
	in_ops->push_back(pi->address_get()->factory_get().dncast<pi_mf_ptr_deref>()->addr_get());
	in_types->push_back(pi->address_get()->factory_get().dncast<pi_mf_ptr_deref>()->addr_get()->type_get());
	
	return pi_operands::create(in_ops, out_ops, in_types, out_types, jmp_targets);
}

ptr< pi_operands > visitor_pi_pi2pi_operands::visit_pi_sp(ptr< pi_sp >){
	CREATE_PI_OPERANDS_LISTS();
	return pi_operands::create(in_ops, out_ops, in_types, out_types, jmp_targets);
}

ptr< pi_operands > visitor_pi_pi2pi_operands::visit_pi_leave(ptr< pi_leave >){
	CREATE_PI_OPERANDS_LISTS();
	
	return pi_operands::create(in_ops, out_ops, in_types, out_types, jmp_targets);
}

ptr< pi_operands > visitor_pi_pi2pi_operands::visit_pi_call(ptr< pi_call > pi){
	CREATE_PI_OPERANDS_LISTS();
	
	pi_operand_list_type::iterator it;
	for(it=pi->args_get()->begin(); it!=pi->args_get()->end(); it++) {
		in_ops->push_back(*it);
		in_types->push_back((*it)->type_get());
	}
	
	if ( pi->this_arg_get() ) {
		in_ops->push_back(pi->this_arg_get());
		in_types->push_back(pi->this_arg_get()->type_get());
	}
	
	out_ops->push_back(pi->rv_get());
	out_types->push_back(pi->rv_get()->type_get());
	
	return pi_operands::create(in_ops, out_ops, in_types, out_types, jmp_targets);
}

ptr< pi_operands > visitor_pi_pi2pi_operands::visit_pi_icall(ptr< pi_icall >){
	lassert2(false,msg_not_implemented_yet);
	return pi_operands::create(NULL, NULL, NULL, NULL,NULL);
}

ptr< pi_operands > visitor_pi_pi2pi_operands::visit_pi_callv(ptr< pi_callv > pi){
	CREATE_PI_OPERANDS_LISTS();
	
	pi_operand_list_type::iterator it;
	for(it=pi->args_get()->begin(); it!=pi->args_get()->end(); it++) {
		in_ops->push_back(*it);
		in_types->push_back((*it)->type_get());
	}
	
	if ( pi->this_arg_get() ) {
		in_ops->push_back(pi->this_arg_get());
		in_types->push_back(pi->this_arg_get()->type_get());
	}
	return pi_operands::create(in_ops, out_ops, in_types, out_types, jmp_targets);
}

ptr< pi_operands > visitor_pi_pi2pi_operands::visit_pi_icallv(ptr< pi_icallv >){
	lassert2(false,msg_not_implemented_yet);
	return NULL;
}

ptr< pi_operands > visitor_pi_pi2pi_operands::cb_sl_operands_get(ptr< pi_cb_single_label_target > pi) {
	CREATE_PI_OPERANDS_LISTS();
	
	in_ops->push_back(pi->condition_get());
	in_types->push_back(pi->condition_get()->type_get());
	
	jmp_targets->push_back(pi->destination_get());
	
	return pi_operands::create(in_ops, out_ops, in_types, out_types, jmp_targets);
}

ptr< pi_operands > visitor_pi_pi2pi_operands::cb_sa_operands_get(ptr< pi_cb_single_address_target >) {
	lassert2(false,msg_not_implemented_yet);
	return NULL;
}

ptr< pi_operands > visitor_pi_pi2pi_operands::ub_sl_operands_get(ptr< pi_ub_single_label_target > pi) {
	CREATE_PI_OPERANDS_LISTS();
	
	jmp_targets->push_back(pi->destination_get());
	
	return pi_operands::create(in_ops, out_ops, in_types, out_types, jmp_targets);
}

ptr< pi_operands > visitor_pi_pi2pi_operands::ub_sa_operands_get(ptr< pi_ub_single_address_target >) {
	lassert2(false,msg_not_implemented_yet);
	return NULL;
}

ptr< pi_operands > visitor_pi_pi2pi_operands::bm_l_operands_get(ptr< pi_branch_multiple_label_target >) {
	lassert2(false,msg_not_implemented_yet);
	return NULL;
}

ptr< pi_operands > visitor_pi_pi2pi_operands::bm_a_operands_get(ptr< pi_branch_multiple_address_target >) {
	lassert2(false,msg_not_implemented_yet);
	return NULL;
}

end_package(intercode);
end_package(backend_v2);
end_package(lestes);

