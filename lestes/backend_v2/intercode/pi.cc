/*!
	\file
	\brief Pi-level intercode classes.
	\author jaz
*/
#include <lestes/backend_v2/intercode/pi.g.hh>

package(lestes);
package(backend_v2);
package(intercode);

/*!
	\brief Returns kind of the operand.
*/
pi_operand::kind_type pi_preg::kind_get() {
	return pi_operand::PREG;
}

/*!
	\brief Returns kind of the operand.
*/
pi_operand::kind_type pi_lit::kind_get() {
	return pi_operand::LIT;
}

/*!
	\brief Returns kind of the operand.
*/
pi_operand::kind_type pi_mem_decl::kind_get() {
	return pi_operand::MEM_DECL;
}

/*!
	\brief Returns kind of the operand.
*/
pi_operand::kind_type pi_mem_lit::kind_get() {
	return pi_operand::MEM_LIT;
}

/*!
	\brief Returns kind of the operand.
*/
pi_operand::kind_type pi_mem_stack::kind_get() {
	return pi_operand::MEM_STACK;
}

/*!
	\brief Returns kind of the operand.
*/
pi_operand::kind_type pi_mem_preg::kind_get() {
	return pi_operand::MEM_PREG;
}

/*!
	\brief Returns kind of the operand.
*/
pi_operand::kind_type pi_mem_temp::kind_get() {
	return pi_operand::MEM_TEMP;
}

/*!
	\brief Returns kind of the operand.
*/
pi_operand::kind_type pi_mem_member::kind_get() {
	return pi_operand::MEM_MEMBER;
}


/*!
	\brief Returns kind of the operand.
*/
pi_operand::kind_type pi_mem_ptr_deref::kind_get() {
	return pi_operand::MEM_PTR_DEREF;
}

end_package(intercode);
end_package(backend_v2);
end_package(lestes);

