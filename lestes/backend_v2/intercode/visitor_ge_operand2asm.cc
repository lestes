/*!
	\file
	\author jaz
*/

#include <lestes/backend_v2/intercode/ge.g.hh>
#include <lestes/backend_v2/intercode/pi.g.hh>
#include <lestes/backend_v2/intercode/pi_mem_factory.g.hh>
#include <lestes/backend_v2/intercode/visitor_ge_operand2asm.g.hh>
#include <lestes/md/registers/tm_register.g.hh>
#include <lestes/md/types/tm_data_type.g.hh>
#include <lestes/md/tasm/tm_asm.mdg.hh>
#include <lestes/md/literals/literal_loader.g.hh>
#include <lestes/md/symbols/name_mangler.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>
#include <sstream>

package(lestes);
package(backend_v2);
package(intercode);

using namespace ::lestes::md::instructions;
using namespace ::lestes::md::registers;
using namespace ::lestes::md::tasm;
using namespace ::lestes::md::literals;
using namespace ::lestes::md::symbols;
using namespace ::lestes::md;

typedef map< srp<ge_pi>, ulint> ge_pi2reg__type;

lstring visitor_ge_operand2asm::visit_ge_operand_reg(ptr< ::lestes::backend_v2::intercode::ge_operand_reg > ge) {
	lstring code;	
	
	ge_pi2reg__type::iterator it = ge->assigned_registers_get()->find(instruction);
	
	if ( it!=ge->assigned_registers_get()->end() ) {
		code = tm_register::instance((tm_register_base::id_type)it->second)->asm_output_get();
	} else {
		::std::ostringstream oss;
		oss << ge->uid_get();
		code = "!NO_REG(ge_operand_reg=" + oss.str() + ")";
	}
	
	return code;
}

lstring visitor_ge_operand2asm::visit_ge_operand_mem(ptr< ::lestes::backend_v2::intercode::ge_operand_mem > ge) {
	ptr<pi_mem_factory> factory = ge->factory_get();
	
	lstring code;

	switch (factory->kind_get()) {
		case pi_mem_factory::MF_DECL: {
			ptr<pi_mf_decl> mf = factory.dncast<pi_mf_decl>();
	
			//Get id of the global declaration.
			lstring id = name_mangler::instance()->mangle(mf->decl_get());

			code = tm_asm::ent_address_disp_get();
			code = string_replace(code, "$disp", id);
		} break;		
		
		case pi_mem_factory::MF_LIT: {
			ptr<pi_lit> lit = factory.dncast<pi_mf_lit>()->lit_get();
	
			ptr<literal_loader> loader = literal_loader::instance();
	
			//Get id of literal.
			lstring id = loader->get_id_for_managed_literal(lit);
			lassert(!id.empty());	
	
			code = tm_asm::ent_address_disp_get();
			code = string_replace(code, "$disp", id);
		} break;		
		
		case pi_mem_factory::MF_STACK: {
			ptr<pi_mf_stack> mf = factory.dncast<pi_mf_stack>();
	
			//Get stack offset.
			t_ssize offset = mf->offset_get();
			lassert(!(offset%32));
	
			code = tm_asm::ent_address_disp_base_get();
	
			::std::ostringstream oss;
			oss << offset/8;
	
			code = string_replace(code, "$disp", oss.str());
			code = string_replace(code, "$psign", offset >= 0 ? "+" : "");
			code = string_replace(code, "$base", tm_register::instance(R_EBP)->asm_output_get());
		} break;
			
		case pi_mem_factory::MF_PTR_DEREF: {
			ptr<pi_mf_ptr_deref> mf = factory.dncast<pi_mf_ptr_deref>();
	
			//Get id of register holding address to be dereferenced.
			lstring reg_id = mf->ge_addr_get()->accept_visitor_ge_operand2lstring_gen_base(this);

			code = tm_asm::ent_address_base_get();
			code = string_replace(code, "$base", reg_id);
		} break;
		
		default: lassert(false);									
	}
	
	return  code;
}

lstring visitor_ge_operand2asm::visit_ge_operand_imm(ptr< ::lestes::backend_v2::intercode::ge_operand_imm > ge) {
	return ge->value_get()->get_asm_definition_val();
}


end_package(intercode);
end_package(backend_v2);
end_package(lestes);

