#include <lestes/backend_v2/intercode/ge.g.hh>
#include <lestes/backend_v2/intercode/pi.g.hh>
#include <lestes/backend_v2/intercode/visitor_ge_pi2asm.g.hh>
#include <lestes/backend_v2/intercode/visitor_ge_operand2asm.g.hh>
#include <lestes/backend_v2/debug/debug.hh>
#include <lestes/backend_v2/workers/bb_finder.g.hh>
#include <lestes/md/instructions/tm_instr.g.hh>
#include <lestes/md/symbols/name_mangler.g.hh>
#include <lestes/md/tasm/tm_asm.mdg.hh>
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>
#include <sstream>

package(lestes);
package(backend_v2);
package(intercode);

using namespace ::lestes::backend_v2::debug;
using namespace ::lestes::backend_v2::workers;
using namespace ::lestes::md::instructions;
using namespace ::lestes::md::symbols;
using namespace ::lestes::md::tasm;
using namespace ::lestes::md;

typedef vector<srp<ge_operand> > ge_operand_vector__type;
typedef vector<srp<ge_sp> > ge_sp_vector__type;
typedef map<ulint,lstring > id2lstring__type;

/*!
	\brief Generates output asm code for generic ge_pi pseudoinstruction.
*/
lstring visitor_ge_pi2asm::generate_generic_code(ptr< ::lestes::backend_v2::intercode::ge_pi > ge) {
	//Get target instruction that ge is mapped to
	ptr<tm_instr_base> tm = ge->instruction_get();

	lstring asm_code;
	
	if ( !tm ) {
		asm_code = lstring();
	}  else {	
		//Get asm output template
		asm_code = tm->asm_output_get();
	
		//Replace operand placeholders in template
		ptr<visitor_ge_operand2asm> op2asm = visitor_ge_operand2asm::create();
		op2asm->instruction_set(ge);
	
		ptr<ge_operand_vector__type> input_ops = ge->operands_input_get();
		for(ulint i=0; i<input_ops->size(); ++i) {
			ptr<ge_operand> op = (*input_ops)[i];
		
			lstring op_asm_code = op->accept_visitor_ge_operand2lstring_gen_base(op2asm);
			lstring op_asm_type = op->type_get()->asm_output_get();
		
			::std::ostringstream oss1;
			oss1 << "I_" << i+1;
		
			asm_code = string_replace(asm_code, "$" + oss1.str(), op_asm_code);	
			asm_code = string_replace(asm_code, "$TYPE_" + oss1.str(), op_asm_type);	
		}
	
		ptr<ge_operand_vector__type> output_ops = ge->operands_output_get();
		for(ulint i=0; i<output_ops->size(); ++i) {
			ptr<ge_operand> op = (*output_ops)[i];
		
			lstring op_asm_code = op->accept_visitor_ge_operand2lstring_gen_base(op2asm);
			lstring op_asm_type = op->type_get()->asm_output_get();
		
			::std::ostringstream oss1;
			oss1 << "O_" << i+1;
		
			asm_code = string_replace(asm_code, "$" + oss1.str(), op_asm_code);	
			asm_code = string_replace(asm_code, "$TYPE_" + oss1.str(), op_asm_type);		
		}

		//Replace jump target placeholders
		if ( ge->jmp_targets_get() ) {
		
			ptr<ge_sp_vector__type> targets = ge->jmp_targets_get();
			for(ulint i=0; i<targets->size(); ++i) {
				ptr<ge_sp> sp = (*targets)[i];
				
				lstring label = tm_asm::ent_label_get();
		
				::std::ostringstream oss1;
				oss1 << sp->uid_get();
			
				::std::ostringstream oss2;
				oss2 <<  "$label_" << i+1;
			
				label = string_replace(label, "$id", oss1.str());
		
				asm_code = string_replace(asm_code, oss2.str(), label);	
			}
		}
	}

#ifdef BACKEND_V2_DEBUG
	::std::ostringstream oss;
		
	oss << "id=" << ge->uid_get()  << " (sch_pos:" << ge->schedule_pos_get() << ")";
	
	if ( ge->pi_source_get() ) {
		oss << " pi_src=" << ge->pi_source_get()->reflection_get()->back()->name_get() << "(id:" << ge->pi_source_get()->uid_get() << ")";
	}
	
	if ( ge->bb_get() ) {
		oss << " bb=" << ge->bb_get()->uid_get();
	}
	
	if ( ge->properties_get()) {
		id2lstring__type::iterator it = ge->properties_get()->find(ge_pi::PROPERTY_SPILLGEN_INFO);
		
		if ( it!=ge->properties_get()->end() ) {
			oss << " SPILLGEN: " << it->second;
		}
	}
	
	asm_code += string_replace(tm_asm::ent_inline_comment_get(),"$text",oss.str()) ;	
#endif
	
	return asm_code;
}

lstring visitor_ge_pi2asm::visit_ge_pi(ptr< ::lestes::backend_v2::intercode::ge_pi > ge) {
	return generate_generic_code(ge);	
}


lstring visitor_ge_pi2asm::visit_ge_sp(ptr< ::lestes::backend_v2::intercode::ge_sp > ge) {
	lstring asm_code;
	
	if ( ge->is_jmp_target_get() ) {
		::std::ostringstream oss1;
		oss1 << ge->uid_get();
		
		asm_code = string_replace(tm_asm::ent_label_get(), "$id", oss1.str());
		asm_code = string_replace(tm_asm::ent_label_def_get(), "$label", asm_code);
	}

#ifdef BACKEND_V2_DEBUG
	::std::ostringstream oss;
	oss << "id=" << ge->uid_get()  << " (sch_pos:" << ge->schedule_pos_get() << ")";
	
	if ( ge->pi_source_get() ) {
		oss << " pi_src=" << ge->pi_source_get()->reflection_get()->back()->name_get() << "(id:" << ge->pi_source_get()->uid_get() << ")";
	}
	
	if ( ge->bb_get() ) {
		oss << " bb=" << ge->bb_get()->uid_get();
	}
	
	if ( ge->properties_get()) {
		id2lstring__type::iterator it = ge->properties_get()->find(ge_pi::PROPERTY_SPILLGEN_INFO);
		
		if ( it!=ge->properties_get()->end() ) {
			oss << " SPILLGEN: " << it->second;
		}
	}
	
	asm_code += string_replace(tm_asm::ent_inline_comment_get(),"$text",oss.str()) ;	
#endif
	
	return asm_code;
}

lstring visitor_ge_pi2asm::visit_ge_call(ptr< ::lestes::backend_v2::intercode::ge_call > ge) {
	//Get asm output template
	lstring asm_code = generate_generic_code(ge);
	
	//Mangle name
	lstring mangled_name = name_mangler::instance()->mangle(ge->function_decl_get());	
	
	asm_code = string_replace(asm_code, "$name", mangled_name);	
	
	return asm_code;
}

end_package(intercode);
end_package(backend_v2);
end_package(lestes);

