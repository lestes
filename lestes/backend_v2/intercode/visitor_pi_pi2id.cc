#include <lestes/backend_v2/intercode/visitor_pi_pi2id.g.hh>
#include <lestes/backend_v2/intercode/pi.g.hh>

package(lestes);
package(backend_v2);
package(intercode);


ulint visitor_pi_pi2id::visit_pi_sp(ptr<pi_sp> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_SP;
}

ulint visitor_pi_pi2id::visit_pi_add(ptr<pi_add> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_ADD;
}

ulint visitor_pi_pi2id::visit_pi_sub(ptr<pi_sub> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_SUB;
}

ulint visitor_pi_pi2id::visit_pi_mul(ptr<pi_mul> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_MUL;
}

ulint visitor_pi_pi2id::visit_pi_div(ptr<pi_div> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_DIV;
}

ulint visitor_pi_pi2id::visit_pi_divrni(ptr<pi_divrni> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_DIVRNI;
}

ulint visitor_pi_pi2id::visit_pi_divrpi(ptr<pi_divrpi> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_DIVRPI;
}

ulint visitor_pi_pi2id::visit_pi_divrz(ptr<pi_divrz> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_DIVRZ;
}

ulint visitor_pi_pi2id::visit_pi_mod(ptr<pi_mod> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_MOD;
}

ulint visitor_pi_pi2id::visit_pi_rem(ptr<pi_rem> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_REM;
}

ulint visitor_pi_pi2id::visit_pi_neg(ptr<pi_neg> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_NEG;
}

ulint visitor_pi_pi2id::visit_pi_gat(ptr<pi_gat> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_GAT;
}

ulint visitor_pi_pi2id::visit_pi_shl(ptr<pi_shl> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_SHL;
}

ulint visitor_pi_pi2id::visit_pi_shr(ptr<pi_shr> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_SHR;
}

ulint visitor_pi_pi2id::visit_pi_sal(ptr<pi_sal> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_SAL;
}

ulint visitor_pi_pi2id::visit_pi_sar(ptr<pi_sar> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_SAR;
}

ulint visitor_pi_pi2id::visit_pi_band(ptr<pi_band> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_BAND;
}

ulint visitor_pi_pi2id::visit_pi_bor(ptr<pi_bor> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_BOR;
}

ulint visitor_pi_pi2id::visit_pi_bxor(ptr<pi_bxor> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_BXOR;
}

ulint visitor_pi_pi2id::visit_pi_bnot(ptr<pi_bnot> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_BNOT;
}

ulint visitor_pi_pi2id::visit_pi_land(ptr<pi_land> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_LAND;
}

ulint visitor_pi_pi2id::visit_pi_lor(ptr<pi_lor> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_LOR;
}

ulint visitor_pi_pi2id::visit_pi_lxor(ptr<pi_lxor> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_LXOR;
}

ulint visitor_pi_pi2id::visit_pi_lnot(ptr<pi_lnot> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_LNOT;
}

ulint visitor_pi_pi2id::visit_pi_mov(ptr<pi_mov> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_MOV;
}

ulint visitor_pi_pi2id::visit_pi_cmp(ptr<pi_cmp> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_CMP;
}

ulint visitor_pi_pi2id::visit_pi_cvt(ptr<pi_cvt> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_CVT;
}

ulint visitor_pi_pi2id::visit_pi_ld(ptr<pi_ld> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_LD;
}

ulint visitor_pi_pi2id::visit_pi_lda(ptr<pi_lda> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_LDA;
}

ulint visitor_pi_pi2id::visit_pi_ldi(ptr<pi_ldi> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_LDI;
}

ulint visitor_pi_pi2id::visit_pi_ldp(ptr<pi_ldp> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_LDP;
}

ulint visitor_pi_pi2id::visit_pi_ldv(ptr<pi_ldv> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_LDV;
}

ulint visitor_pi_pi2id::visit_pi_ldpv(ptr<pi_ldpv> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_LDPV;
}

ulint visitor_pi_pi2id::visit_pi_st(ptr<pi_st> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_ST;
}

ulint visitor_pi_pi2id::visit_pi_sti(ptr<pi_sti> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_STI;
}

ulint visitor_pi_pi2id::visit_pi_stp(ptr<pi_stp> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_STP;
}

ulint visitor_pi_pi2id::visit_pi_stv(ptr<pi_stv> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_STV;
}

ulint visitor_pi_pi2id::visit_pi_stiv(ptr<pi_stiv> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_STIV;
}

ulint visitor_pi_pi2id::visit_pi_stpv(ptr<pi_stpv> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_STPV;
}

ulint visitor_pi_pi2id::visit_pi_call(ptr<pi_call> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_CALL;
}

ulint visitor_pi_pi2id::visit_pi_icall(ptr<pi_icall> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_ICALL;
}

ulint visitor_pi_pi2id::visit_pi_callv(ptr<pi_callv> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_CALLV;
}

ulint visitor_pi_pi2id::visit_pi_icallv(ptr<pi_icallv> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_ICALLV;
}

ulint visitor_pi_pi2id::visit_pi_ba(ptr<pi_ba> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_BA;
}

ulint visitor_pi_pi2id::visit_pi_bn(ptr<pi_bn> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_BN;
}

ulint visitor_pi_pi2id::visit_pi_bt(ptr<pi_bt> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_BT;
}

ulint visitor_pi_pi2id::visit_pi_bf(ptr<pi_bf> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_BF;
}

ulint visitor_pi_pi2id::visit_pi_bm(ptr<pi_bm> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_BM;
}

ulint visitor_pi_pi2id::visit_pi_bg(ptr<pi_bg> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_BG;
}

ulint visitor_pi_pi2id::visit_pi_bl(ptr<pi_bl> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_BL;
}

ulint visitor_pi_pi2id::visit_pi_be(ptr<pi_be> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_BE;
}

ulint visitor_pi_pi2id::visit_pi_bng(ptr<pi_bng> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_BNG;
}

ulint visitor_pi_pi2id::visit_pi_bnl(ptr<pi_bnl> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_BNL;
}

ulint visitor_pi_pi2id::visit_pi_bne(ptr<pi_bne> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_BNE;
}

ulint visitor_pi_pi2id::visit_pi_iba(ptr<pi_iba> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_IBA;
}

ulint visitor_pi_pi2id::visit_pi_ibn(ptr<pi_ibn> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_IBN;
}

ulint visitor_pi_pi2id::visit_pi_ibm(ptr<pi_ibm> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_IBM;
}

ulint visitor_pi_pi2id::visit_pi_ibg(ptr<pi_ibg> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_IBG;
}

ulint visitor_pi_pi2id::visit_pi_ibl(ptr<pi_ibl> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_IBL;
}

ulint visitor_pi_pi2id::visit_pi_ibe(ptr<pi_ibe> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_IBE;
}

ulint visitor_pi_pi2id::visit_pi_ibng(ptr<pi_ibng> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_IBNG;
}

ulint visitor_pi_pi2id::visit_pi_ibnl(ptr<pi_ibnl> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_IBNL;
}

ulint visitor_pi_pi2id::visit_pi_ibne(ptr<pi_ibne> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_IBNE;
}

ulint visitor_pi_pi2id::visit_pi_sbg(ptr<pi_sbg> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_SBG;
}

ulint visitor_pi_pi2id::visit_pi_sbl(ptr<pi_sbl> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_SBL;
}

ulint visitor_pi_pi2id::visit_pi_sbe(ptr<pi_sbe> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_SBE;
}

ulint visitor_pi_pi2id::visit_pi_sbng(ptr<pi_sbng> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_SBNG;
}

ulint visitor_pi_pi2id::visit_pi_sbnl(ptr<pi_sbnl> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_SBNL;
}

ulint visitor_pi_pi2id::visit_pi_sbne(ptr<pi_sbne> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_SBNE;
}

ulint visitor_pi_pi2id::visit_pi_leave(ptr<pi_leave> pi){
	lassert(pi);
	return visitor_pi_pi2id::PI_LEAVE;
}

end_package(intercode);
end_package(backend_v2);
end_package(lestes);

