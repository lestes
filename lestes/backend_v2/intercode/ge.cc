#include <lestes/backend_v2/intercode/ge.g.hh>
#include <lestes/md/instructions/tm_instr_base.g.hh>
#include <lestes/md/types/tm_data_type_base.g.hh>
#include <lestes/std/set.hh>
#include <lestes/std/vector.hh>

package(lestes);
package(backend_v2);
package(intercode);

using namespace ::lestes::md::instructions;

typedef vector<srp<ge_operand> >  ge_operand_vector__type;
typedef vector<srp<tm_instr_op_base> >  tm_operand_vector__type;

/*!
	\brief Returns kind of the pseudoinstruction.
*/
ge_pi::kind_type ge_pi::kind_get()
{
    return ge_pi::PI;
}

/*!
	\brief Returns kind of the pseudoinstruction.
*/
ge_pi::kind_type ge_sp::kind_get()
{
    return ge_pi::SP;
}

/*!
	\brief Returns kind of the pseudoinstruction.
*/
ge_pi::kind_type ge_call::kind_get()
{
    return ge_pi::CALL;
}

/*!
	\brief Tells whether the pseudoinstruction is consistent with target machine instruction that it represents.
	
	\return If the pseudoinstruction with instruction is consistent then true. False otherwise.
*/
bool ge_pi::validate() {
	if ( !instruction && kind_get()!=SP ) {
		return false;
	}

	ptr<ge_operand_vector__type> ge_ops = operands_input;
	ptr<tm_operand_vector__type> tm_ops = instruction->operands_input_get();
	
	if ( ge_ops->size() != tm_ops->size() ) {
		return false;
	}
	
	tm_operand_vector__type::iterator it2 = tm_ops->begin();
	for(ge_operand_vector__type::iterator it1 = ge_ops->begin(); it1!= ge_ops->end(); ++it1, ++it2) {
		ptr<ge_operand> op1 = *it1;
		ptr<tm_instr_op_base> op2 = *it2;
		
		if ( (ulint)op1->kind_get() != (ulint)op2->kind_get() ) {
			return false;
		}
		
		if ( op2->allowed_types_get()->find(op1->type_get()->id_get())==op2->allowed_types_get()->end() ) {
			return false;
		}
	}
	
	ge_ops = operands_output;
	tm_ops = instruction->operands_output_get();
	
	if ( ge_ops->size() != tm_ops->size() ) {
		return false;
	}
	
	it2 = tm_ops->begin();
	for(ge_operand_vector__type::iterator it1 = ge_ops->begin(); it1!= ge_ops->end(); ++it1, ++it2) {
		ptr<ge_operand> op1 = *it1;
		ptr<tm_instr_op_base> op2 = *it2;
		
		if ( (ulint)op1->kind_get() != (ulint)op2->kind_get() ) {
			return false;
		}
		
		if ( op2->allowed_types_get()->find(op1->type_get()->id_get())==op2->allowed_types_get()->end() ) {
			return false;
		}
	}
	
	return true;
}


/*!
	\brief Returns kind of the operand.
*/
ge_operand::kind_type ge_operand_mem::kind_get()
{
    return ge_operand::MEMORY;
}


/*!
	\brief Returns kind of the operand.
*/
ge_operand::kind_type ge_operand_imm::kind_get()
{
    return ge_operand::IMMEDIATE;
}


/*!
	\brief Returns kind of the operand.
*/
ge_operand::kind_type ge_operand_reg::kind_get()
{
    return ge_operand::REGISTER;
}

end_package(intercode);
end_package(backend_v2);
end_package(lestes);

