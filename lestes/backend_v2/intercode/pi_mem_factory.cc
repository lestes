/*! \file
\author jaz
*/

#include <lestes/backend_v2/intercode/pi.g.hh>
#include <lestes/backend_v2/intercode/ge.g.hh>
#include <lestes/md/types/tm_data_type_base.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>

#include "pi_mem_factory.g.hh"

package(lestes);
package(backend_v2);
package(intercode);

using namespace ::lestes::backend_v2::intercode;


ptr<pi_mem> pi_mf_stack::get_mem(ptr<pi_pi> origin) {
	return pi_mem_stack::create(origin, this->type_get(), this);
}

ptr<ge_operand_mem> pi_mf_stack::get_ge_mem(ptr<ge_pi> origin) {
	return ge_operand_mem::create(this->type_get(),origin,NULL,this);
}

/*!
	\brief Returns kind of the factory.
*/
pi_mem_factory::kind_type pi_mf_stack::kind_get() {
	return pi_mem_factory::MF_STACK;
}


/*!
	\brief Returns memory operand that references place in memory represented by the factory.
	
	\param origin A pseudoinstruction that should be set as origin of a created operand.
	\return A new memory operand.
*/
ptr<pi_mem> pi_mf_decl::get_mem(ptr<pi_pi> origin) {
	return pi_mem_decl::create(origin, this->type_get(), this);
}

/*!
	\brief Returns memory operand that references place in memory represented by the factory.
	
	\param origin A pseudoinstruction that should be set as origin of a created operand.
	\return A new memory operand.
*/
ptr<ge_operand_mem> pi_mf_decl::get_ge_mem(ptr<ge_pi> origin) {
	return ge_operand_mem::create(this->type_get(),origin,NULL,this);
}

/*!
	\brief Returns kind of the factory.
*/
pi_mem_factory::kind_type pi_mf_decl::kind_get() {
	return pi_mem_factory::MF_DECL;
}


/*!
	\brief Returns memory operand that references place in memory represented by the factory.
	
	\param origin A pseudoinstruction that should be set as origin of a created operand.
	\return A new memory operand.
*/
ptr<pi_mem> pi_mf_lit::get_mem(ptr<pi_pi> origin) {
	return pi_mem_lit::create(origin, this->type_get(), this);
}

/*!
	\brief Returns memory operand that references place in memory represented by the factory.
	
	\param origin A pseudoinstruction that should be set as origin of a created operand.
	\return A new memory operand.
*/
ptr<ge_operand_mem> pi_mf_lit::get_ge_mem(ptr<ge_pi> origin) {
	return ge_operand_mem::create(this->type_get(),origin,NULL,this);
}

/*!
	\brief Returns kind of the factory.
*/
pi_mem_factory::kind_type pi_mf_lit::kind_get() {
	return pi_mem_factory::MF_LIT;
}

/*!
	\brief Returns memory operand that references place in memory represented by the factory.
	
	\param origin A pseudoinstruction that should be set as origin of a created operand.
	\return A new memory operand.
*/
ptr<pi_mem> pi_mf_member::get_mem(ptr<pi_pi> origin) {
	return pi_mem_member::create(origin, this->type_get(), this);
}

/*!
	\brief Returns memory operand that references place in memory represented by the factory.
	
	\param origin A pseudoinstruction that should be set as origin of a created operand.
	\return A new memory operand.
*/
ptr<ge_operand_mem> pi_mf_member::get_ge_mem(ptr<ge_pi> origin) {
	return ge_operand_mem::create(this->type_get(),origin,NULL,this);
}

/*!
	\brief Returns kind of the factory.
*/
pi_mem_factory::kind_type pi_mf_member::kind_get() {
	return pi_mem_factory::MF_MEMBER;
}

/*!
	\brief Returns memory operand that references place in memory represented by the factory.
	
	\param origin A pseudoinstruction that should be set as origin of a created operand.
	\return A new memory operand.
*/
ptr<pi_mem> pi_mf_preg::get_mem(ptr<pi_pi> origin) {
	return pi_mem_preg::create(origin, this->type_get(), this);
}

/*!
	\brief Returns memory operand that references place in memory represented by the factory.
	
	\param origin A pseudoinstruction that should be set as origin of a created operand.
	\return A new memory operand.
*/
ptr<ge_operand_mem> pi_mf_preg::get_ge_mem(ptr<ge_pi>) {
	lassert(false);
}

/*!
	\brief Returns kind of the factory.
*/
pi_mem_factory::kind_type pi_mf_preg::kind_get() {
	return pi_mem_factory::MF_PREG;
}


/*!
	\brief Returns memory operand that references place in memory represented by the factory.
	
	\param origin A pseudoinstruction that should be set as origin of a created operand.
	\return A new memory operand.
*/
ptr<pi_mem> pi_mf_ptr_deref::get_mem(ptr<pi_pi> origin) {
	return pi_mem_ptr_deref::create(origin, this->type_get(), this);
}

/*!
	\brief Returns memory operand that references place in memory represented by the factory.
	
	\param origin A pseudoinstruction that should be set as origin of a created operand.
	\return A new memory operand.
*/
ptr<ge_operand_mem> pi_mf_ptr_deref::get_ge_mem(ptr<ge_pi> origin) {
	return ge_operand_mem::create(this->type_get(),origin,NULL,this);
}

/*!
	\brief Returns kind of the factory.
*/
pi_mem_factory::kind_type pi_mf_ptr_deref::kind_get() {
	return pi_mem_factory::MF_PTR_DEREF;
}


end_package(intercode);
end_package(backend_v2);
end_package(lestes);

