#include <lestes/backend_v2/intercode/ge.g.hh>
#include <lestes/backend_v2/structs/func_data.g.hh>
#include <lestes/backend_v2/workers/literal_inliner.g.hh>
#include <lestes/md/literals/literal_info_base.g.hh>
#include <lestes/md/instructions/tm_instr.g.hh>
#include <lestes/std/list.hh>
#include <lestes/std/map.hh>
#include <lestes/std/vector.hh>

package(lestes);
package(backend_v2);
package(workers);

using namespace ::lestes::backend_v2::structs;
using namespace ::lestes::backend_v2::intercode;
using namespace ::lestes::md::instructions;
using namespace ::lestes::md::literals;

typedef list< srp<ge_pi> > ge_pi_list__type;
typedef vector< srp<ge_operand> > ge_op_vector__type;
typedef map< srp<ge_operand_reg>, srp<ge_operand_imm> > reg2imm__type;
typedef set<ulint> id_set__type;

/*!
	\brief Performs immediate inlining.
*/
void literal_inliner::process() {
	
	ptr<reg2imm__type> reg2imm = reg2imm__type::create();
	
	ptr<ge_pi_list__type> body = data_get()->ge_body_get();	

	for(ge_pi_list__type::iterator it = body->begin(); it!=body->end(); ++it) {
		ptr<ge_pi> ge = *it;
		ptr<tm_instr_base> tm = ge->instruction_get();
		
		if ( !tm ) {
			continue;
		}
		
		if ( tm->is_load() 
			&& ge->operands_input_get()->front()->kind_get()==ge_operand::IMMEDIATE
			&& ge->operands_output_get()->front()->kind_get()==ge_operand::REGISTER ) {
				//The instruction is load instruction that loads an immediate to register.
				(*reg2imm)[ge->operands_output_get()->front().dncast<ge_operand_reg>()] = ge->operands_input_get()->front().dncast<ge_operand_imm>();
		} else if ( tm->versions_get() ) {
			//The instruction has exchangeable tm_instr version available.
			//Go through input operands and search for a register operand that can be echanged for its immediate source.
			ptr<ge_op_vector__type> operands = ge->operands_input_get();
			for(ge_op_vector__type::iterator it_op = operands->begin(); it_op!=operands->end();) {
				if ( (*it_op)->kind_get()!=ge_operand::REGISTER ) {
					++it_op;
					continue;
				}
				
				ptr<ge_operand_reg> reg = (*it_op).dncast<ge_operand_reg>();
				
				reg2imm__type::iterator it_imm = reg2imm->find(reg);
				
				if ( it_imm==reg2imm->end() ) {
					++it_op;
					continue;
				}
				
				//Ok, the immediate source has been found.
				ptr<ge_operand_imm> imm = it_imm->second;
				
				//Exchange immediate after register.
				*it_op = imm;
				
				//Try to find compatible tm_instr version that is compatible with this operand.
				if ( find_compatible_version(ge) ) {
					//Compatible version exists and it has been set to the ge_pi. 
					//Remove dependence on the origin of the register (load of immediate).
					ge->dependencies_get()->erase(reg->origin_get());
					
					//Restart search for exchangeable operans.
					it_op = operands->begin();
				} else {
					//Restore operands.
					*it_op = reg;
					++it_op;
				}
			}
		}
	}
}

/*!
	\brief Finds a target machine instruction description that is compatible with given pseudoinstruction.
	
	\param ge The pseudoinstruction.
*/
bool literal_inliner::find_compatible_version(ptr<ge_pi> ge) {
	ptr<tm_instr_base> backup = ge->instruction_get();
	ptr<id_set__type> versions = backup->versions_get();
	
	for(id_set__type::iterator it = versions->begin(); it!=versions->end();++it) {
		ge->instruction_set(tm_instr::instance(*it));
		
		if ( ge->validate() ) {
			//Compatible version found.
			return true;
		}
	}
	
	ge->instruction_set(backup);
	return false;
}

/*!
	\brief Returns data of the currently processed function with immediates inlined whereever possible.
*/
ptr<func_data> literal_inliner::get_result() {
	return data_get();
}



end_package(workers);
end_package(backend_v2);
end_package(lestes);

