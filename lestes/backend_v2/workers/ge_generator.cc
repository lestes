#include <lestes/backend_v2/structs/func_data.g.hh>
#include <lestes/backend_v2/intercode/pi.g.hh>
#include <lestes/backend_v2/intercode/ge.g.hh>
#include <lestes/backend_v2/intercode/visitor_pi_pi2pi_operands.g.hh>
#include <lestes/backend_v2/workers/ge_generator.g.hh>
#include <lestes/backend_v2/structs/pi_operands.g.hh>
#include <lestes/md/instructions/pi_pi2ge_pi.g.hh>
#include <lestes/backend_v2/debug/debug.hh>

package(lestes);
package(backend_v2);
package(workers);

using namespace ::lestes::msg;
using namespace ::lestes::backend_v2::structs;
using namespace ::lestes::backend_v2::intercode;
using namespace ::lestes::md::instructions;

typedef list< srp<pi_pi> > pi_pi_list__type;
typedef list< srp<pi_sp> > pi_sp_list__type;
typedef list< srp<ge_pi> > ge_pi_list__type;
typedef vector< srp<ge_pi> > ge_pi_vector__type;
typedef vector< srp<pi_operand> > pi_operand_vector__type;
typedef map< srp<pi_pi>, srp<vector<srp<ge_pi> > > > pi_pi2ge_pi_vector__type;
typedef map< srp<pi_operand>, ulint > pi_op2count__type;

declare_logger(log);
initialize_logger( log, "ge_generator", backend_v2_logger );


/*!
	\brief Generates ge-level intercode for a function body in pi-level intercode.
*/
void ge_generator::process() {
	
	log << "ge-code generation - start\n" << eolog;
	
	
	ptr<visitor_pi_pi2pi_operands> pi2op = visitor_pi_pi2pi_operands::create();
	ptr<visitor_pi_pi2id> pi_id_getter = visitor_pi_pi2id::create();
		
	ptr<pi_pi_list__type> pi_pi_list = data_get()->pi_body_get();
	ptr<ge_pi_list__type> ge_pi_list = data_get()->ge_body_get();
	
	//Count operand uses.
	ptr<pi_op2count__type> op_use_count = pi_op2count__type::create();
	for(pi_pi_list__type::iterator it_pi_list = pi_pi_list->begin(); it_pi_list!=pi_pi_list->end(); ++it_pi_list) {
		ptr<pi_pi> pi = *it_pi_list;
		ptr<pi_operands> ops = pi->accept_visitor_pi_pi2pi_operands_gen_base(pi2op);
		
		ptr<pi_operand_vector__type> in_ops = ops->operands_input_get();
		for(pi_operand_vector__type::iterator it_op = in_ops->begin(); it_op!=in_ops->end(); ++it_op) {
			ptr<pi_operand> op = *it_op;
			
			pi_op2count__type::iterator it_search = op_use_count->find(op);
			
			if ( it_search==op_use_count->end() ) {
				(*op_use_count)[op] = 1;
			} else {
				(*op_use_count)[op] = it_search->second + 1;
			}
		}
	}
	
	ptr<pi_pi2ge_pi> convertor = pi_pi2ge_pi::create(op_use_count);
	
	ptr<pi_pi2ge_pi_vector__type> pi2ge = pi_pi2ge_pi_vector__type::create();	
	//Convert sequencepoints
	for(pi_pi_list__type::iterator it_pi_list = pi_pi_list->begin(); it_pi_list!=pi_pi_list->end(); ++it_pi_list) {
		ptr<pi_pi> pi = *it_pi_list;
		
		if ( (visitor_pi_pi2id::kind_type)pi->accept_visitor_pi_pi2ulint_gen_base(pi_id_getter)==visitor_pi_pi2id::PI_SP ) {
			ptr<ge_pi_vector__type> ge_code = pi->accept_visitor_pi_pi2ge_pi_gen_base(convertor);
			(*pi2ge)[pi] = ge_code;
		}
	}
	
	/*
		Convert pseudoinstructions.
		Sequencepoints have to be converted before this.
	*/
	for(pi_pi_list__type::iterator it_pi_list = pi_pi_list->begin(); it_pi_list!=pi_pi_list->end(); ++it_pi_list) {
		ptr<pi_pi> pi = *it_pi_list;
		
		ptr<ge_pi_vector__type> ge_code;
		
		if ( (visitor_pi_pi2id::kind_type)pi->accept_visitor_pi_pi2ulint_gen_base(pi_id_getter)==visitor_pi_pi2id::PI_SP ) {
			ge_code = (*pi2ge)[pi];
		} else {
			ge_code = pi->accept_visitor_pi_pi2ge_pi_gen_base(convertor);
		}
		
		append_vector_to_list(ge_pi_list,ge_code);				
	}
	
	//Setup dependencies of the ge-pseudoinstructions.
	for(ge_pi_list__type::iterator it_ge_list = ge_pi_list->begin(); it_ge_list!=ge_pi_list->end(); ++it_ge_list) {
		convertor->setup_dependencies(*it_ge_list);
	}
	
	log << "ge-code generation - end\n" << eolog;
}

/*!
	\brief Returns the function's data with generated body in ge-intercode.
*/
ptr<func_data> ge_generator::get_result() {
	return data_get();
}

/*!
	\brief Appends a vector to a list.
*/
void ge_generator::append_vector_to_list(ptr<ge_pi_list__type> l, ptr<ge_pi_vector__type> v) {
	for(ulint i=0; i<v->size(); ++i) {
		l->push_back((*v)[i]);
	}
}

end_package(workers);
end_package(backend_v2);
end_package(lestes);

