#include <lestes/backend_v2/structs/func_data.g.hh>
#include <lestes/backend_v2/workers/body_preprocessor.g.hh>
#include <lestes/backend_v2/intercode/ge.g.hh>
#include <lestes/backend_v2/intercode/visitor_pi_pi2id.g.hh>
#include <lestes/md/symbols/symbol_register.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>

package(lestes);
package(backend_v2);
package(workers);

using namespace ::lestes::backend_v2::intercode;
using namespace ::lestes::backend_v2::structs;
using namespace ::lestes::md::symbols;

typedef list<srp<ge_pi> > ge_pi_list__type;

/*!
	\brief Preprocesses a function body.
	
	For now, it just registers all function calls in symbol_register.
*/
void body_preprocessor::process() {
	
	ptr<ge_pi_list__type> body = data_get()->ge_body_get();
	
	for(ge_pi_list__type::iterator it = body->begin(); it!=body->end(); ++it) {
		
		ptr<ge_pi> ge = *it;
		
		if ( ge->kind_get()==ge_pi::CALL ) {
			symbol_register::instance()->register_symbol(ge.dncast<ge_call>()->function_decl_get());
		}
	}
}


/*!
	\brief Returns preprocessed function body.
*/
ptr<func_data> body_preprocessor::get_result() {
	return data_get();
}



end_package(workers);
end_package(backend_v2);
end_package(lestes);

