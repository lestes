#include <lestes/backend_v2/structs/func_data.g.hh>
#include <lestes/backend_v2/intercode/ge.g.hh>
#include <lestes/backend_v2/intercode/pi.g.hh>
#include <lestes/backend_v2/workers/simple_spillgen.g.hh>
#include <lestes/md/mem/mem_alloc_manager.g.hh>
#include <lestes/md/registers/tm_register.g.hh>
#include <lestes/md/registers/move_generator.g.hh>
#include <lestes/md/instructions/tm_instr.g.hh>
#include <lestes/md/types/tm_data_type.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>
#include <lestes/backend_v2/debug/debug.hh>
#include <sstream>

package(lestes);
package(backend_v2);
package(workers);

using namespace ::lestes::backend_v2::debug;
using namespace ::lestes::backend_v2::structs;
using namespace ::lestes::backend_v2::intercode;
using namespace ::lestes::md::instructions;
using namespace ::lestes::md::mem;
using namespace ::lestes::md::types;
using namespace ::lestes::md::registers;
using namespace ::lestes::lang::cplus::sem;

typedef set< srp<ge_pi> > ge_pi_set__type;
typedef set<ulint> ulint_set__type;
typedef list< srp<ge_pi> > ge_pi_list__type;
typedef vector< srp<ge_pi> > ge_pi_vector__type;
typedef vector< srp<ge_operand> > ge_operand_vector__type;
typedef list<srp<pi_mem_factory> > mf_list__type;
typedef map<srp<ge_operand_reg>,srp<pi_mem_factory> > op2mf__type;
typedef map<ulint,lstring> id2lstring__type;

/*!
	\brief Performs spill-code generation.
*/
void simple_spillgen::process() {

	ptr<ge_pi_list__type> body = data_get()->ge_body_get();
	
	ptr<move_generator> move_gen = move_generator::create();
	
	ptr<mf_list__type> free_spill_places = mf_list__type::create();
	ptr<mf_list__type> spill_places_to_free = mf_list__type::create();
	ptr<op2mf__type> used_spill_places = op2mf__type::create();
	
	for(ge_pi_list__type::iterator it = body->begin(); it!=body->end(); ++it) {
		ptr<ge_pi> ge = *it;
		ptr<tm_instr_base> tm = ge->instruction_get();
		
		if ( !tm ) {
			continue;
		}
		
		ge_pi_list__type::iterator it_next = it;
		++it_next;
		
		ulint i_pos = 0;
		ptr<ge_operand_vector__type> operands = ge->operands_input_get();
		for(ge_operand_vector__type::iterator it_op = operands->begin(); it_op!=operands->end(); ++it_op, ++i_pos) {
			if ( (*it_op)->kind_get()!=ge_operand::REGISTER ) {
				continue;
			}
			
			ptr<ge_operand_reg> op = (*it_op).dncast<ge_operand_reg>();
			
			if ( op->assigned_registers_get()->find(ge)!=op->assigned_registers_get()->end() ) {
				continue;
			}
			
			ptr<tm_instr_op_reg_base> tmop = (*tm->operands_input_get())[i_pos].dncast<tm_instr_op_reg_base>();
			ptr<tm_register> reg = pick_reg_by_data_type(tmop->allowed_registers_get(),op->type_get()->id_get());
			
			//Backup the register's value.
			ptr<ge_operand_reg> backuped_reg = ge_operand_reg::create(op->type_get(),NULL,NULL);
			ptr<pi_mem_factory> backup_place = get_free_spill_place(free_spill_places,op->type_get());
			ptr<ge_operand_mem> backup_mem = backup_place->get_ge_mem(NULL);
			ptr<ge_pi_vector__type> code = move_gen->generate_store_to_memory(backuped_reg,reg,backup_mem);
			body->insert(it,code->begin(),code->end());

#ifdef BACKEND_V2_DEBUG		
			::std::ostringstream oss1;
			oss1 << "backup code (ge_pi=" << ge->uid_get() << ")";
			set_instruction_property(code,ge_pi::PROPERTY_SPILLGEN_INFO,oss1.str());
#endif
			
			//Load the spilled operand's value.
			ptr<pi_mem_factory> spill_place = (*used_spill_places)[op];
			lassert(spill_place);
			ptr<ge_operand_mem> spill_mem = spill_place->get_ge_mem(NULL);
			code = move_gen->generate_load_from_memory(spill_mem,op,reg);
			body->insert(it,code->begin(),code->end());

#ifdef BACKEND_V2_DEBUG		
			::std::ostringstream oss2;
			oss2 << "load code (ge_pi=" << ge->uid_get() << ")";
			set_instruction_property(code,ge_pi::PROPERTY_SPILLGEN_INFO,oss2.str());
#endif
			
			//Restore the register's value.
			code = move_gen->generate_load_from_memory(backup_mem,backuped_reg,reg);
			body->insert(it_next,code->begin(),code->end());

#ifdef BACKEND_V2_DEBUG		
			::std::ostringstream oss3;
			oss3 << "restore code (ge_pi=" << ge->uid_get() << ")";
			set_instruction_property(code,ge_pi::PROPERTY_SPILLGEN_INFO,oss3.str());
#endif
			
			//Free spill places
			spill_places_to_free->push_back(spill_place);
			(*used_spill_places)[op] = NULL;
			spill_places_to_free->push_back(backup_place);
			
			//Assign the register to the operand.
			(*op->assigned_registers_get())[ge] = reg->id_get();
		}
		
		i_pos = 0;
		operands = ge->operands_output_get();
		for(ge_operand_vector__type::iterator it_op = operands->begin(); it_op!=operands->end(); ++it_op, ++i_pos) {
			if ( (*it_op)->kind_get()!=ge_operand::REGISTER ) {
				continue;
			}
			
			ptr<ge_operand_reg> op = (*it_op).dncast<ge_operand_reg>();
			
			if ( op->assigned_registers_get()->find(ge)!=op->assigned_registers_get()->end() ) {
				continue;
			}
			
			ptr<tm_instr_op_reg_base> tmop = (*tm->operands_output_get())[i_pos].dncast<tm_instr_op_reg_base>();
			ptr<tm_register> reg = pick_reg_by_data_type(tmop->allowed_registers_get(),op->type_get()->id_get());
			
			//Backup the register's value.
			ptr<ge_operand_reg> backuped_reg = ge_operand_reg::create(op->type_get(),NULL,NULL);
			ptr<pi_mem_factory> backup_place = get_free_spill_place(free_spill_places,op->type_get());
			ptr<ge_operand_mem> backup_mem = backup_place->get_ge_mem(NULL);
			ptr<ge_pi_vector__type> code = move_gen->generate_store_to_memory(backuped_reg,reg,backup_mem);
			body->insert(it,code->begin(),code->end());

#ifdef BACKEND_V2_DEBUG		
			::std::ostringstream oss1;
			oss1 << "backup code (ge_pi=" << ge->uid_get() << ")";
			set_instruction_property(code,ge_pi::PROPERTY_SPILLGEN_INFO,oss1.str());
#endif
			
			//Store the spilled operand's value.
			ptr<pi_mem_factory> spill_place = get_free_spill_place(free_spill_places,op->type_get());
			(*used_spill_places)[op] = spill_place;
			ptr<ge_operand_mem> spill_mem = spill_place->get_ge_mem(NULL);
			code = move_gen->generate_store_to_memory(op,reg,spill_mem);
			body->insert(it_next,code->begin(),code->end());

#ifdef BACKEND_V2_DEBUG		
			::std::ostringstream oss2;
			oss2 << "store code (ge_pi=" << ge->uid_get() << ")";
			set_instruction_property(code,ge_pi::PROPERTY_SPILLGEN_INFO,oss2.str());
#endif
			
			//Restore the register's value.
			code = move_gen->generate_load_from_memory(backup_mem,backuped_reg,reg);
			body->insert(it_next,code->begin(),code->end());

#ifdef BACKEND_V2_DEBUG		
			::std::ostringstream oss3;
			oss3 << "restore code (ge_pi=" << ge->uid_get() << ")";
			set_instruction_property(code,ge_pi::PROPERTY_SPILLGEN_INFO,oss3.str());
#endif
			
			//Free spill places
			spill_places_to_free->push_back(backup_place);
			
			//Assign the register to the operand.
			(*op->assigned_registers_get())[ge] = reg->id_get();
		}
	
		free_spill_places->insert(free_spill_places->begin(),spill_places_to_free->begin(),spill_places_to_free->end());
		spill_places_to_free->clear();
	}
		
}

/*!
	\brief Adds a custom property value to a list of pseudoinstructions.
	
	\param instrs The list.
	\param property_id A id of the property.
	\param property_Value The value.
*/
void simple_spillgen::set_instruction_property(ptr<ge_pi_vector__type> instrs,ulint property_id,lstring property_value) {
	for(ge_pi_vector__type::iterator it = instrs->begin(); it!=instrs->end(); ++it) {	
		ptr<ge_pi> ge = *it;
		
		if ( !ge->properties_get() ) {
			ge->properties_set(id2lstring__type::create());
		}
		
		(*ge->properties_get())[property_id] = property_value;
	}
}	

/*!
	\brief Picks a spill-place from a list of free spill-places that is compatible with a data type.
	
	If no free spill-pace is available then it allocates a new one.
	
	\param mfs The list of free spill-places.
	\param type The data type.
	\return The spill-place.
*/
ptr<pi_mem_factory> simple_spillgen::get_free_spill_place(ptr<mf_list__type> mfs,ptr<tm_data_type_base> type) {
	ulint bitwidth = type->bitwidth_get();
	
	for(mf_list__type::iterator it = mfs->begin(); it!=mfs->end(); ++it) {
		ptr<pi_mem_factory> mf = *it;
		if ( mf->type_get()->bitwidth_get() >= bitwidth ) {
			mfs->erase(it);
			return mf;
		}
	}
	
	return mem_alloc_manager::instance()->allocate_local_tmp(data_get()->function_decl_get(),type);
}

/*!
	\brief Picks a register from set of registers that is compatible with a data type.
	
	\param reg_set The set.
	\param type_id An id of the data type.
	\return The register.
*/
ptr<tm_register> simple_spillgen::pick_reg_by_data_type(ptr<ulint_set__type> reg_set, ulint type_id) {
	for(ulint_set__type::iterator it = reg_set->begin(); it!=reg_set->end(); ++it) {
		ptr<tm_register> reg =  tm_register::instance(*it);
		
		if ( reg->compatible_types_get()->find(type_id)!=reg->compatible_types_get()->end() ) {
			return reg;
		}
	}
	
	lassert(false);
	return NULL;
}


/*!
	\brief Returns data of the processed function with spill-code included.
*/
ptr<func_data> simple_spillgen::get_result() {
	return data_get();
}

end_package(workers);
end_package(backend_v2);
end_package(lestes);

