<?xml version="1.0" encoding="UTF-8"?>
<lsd xmlns="http://lestes.jikos.cz/schemas/lsd" xmlns:h="http://www.w3.org/TR/REC-html40">

<dox file="both">
	<det>\author jaz</det>
</dox>

<file-name>bb_finder</file-name>

<packages>
	<p>lestes</p>
	<p>backend_v2</p>
	<p>workers</p>
</packages>

<imports>
	<i>lestes/std/list.hh</i>
	<i>lestes/backend_v2/common.hh</i>
	<i>lestes/backend_v2/workers/worker_base.g.hh</i>
</imports>

<implementation-imports>
	<i>lestes/backend_v2/structs/func_data.g.hh</i>
	<i>lestes/backend_v2/intercode/ge.g.hh</i>
</implementation-imports>

<foreign-class name="func_data" >
	<p>lestes</p>
	<p>backend_v2</p>
	<p>structs</p>
</foreign-class>

<foreign-class name="ge_pi" >
	<p>lestes</p>
	<p>backend_v2</p>
	<p>intercode</p>
</foreign-class>

<foreign-class name="ge_sp" >
	<p>lestes</p>
	<p>backend_v2</p>
	<p>intercode</p>
</foreign-class>

<include href="worker_base.lsd" />

<class name="bb_finder" base="worker_base">
	<dox>
		<bri>Identifies basic block in a function body.</bri>
	</dox>
	
	<method name="process" type="void" visibility="public"/>
	<method name="get_result" type="func_data" visibility="public" />	
	
	<method name="is_instruction_safe" type="bool" visibility="protected">
		<param name="ge" type="ge_pi" />
	</method>	
	
	<method name="join_blocks" type="basic_block" visibility="protected">
		<param name="bb1" type="basic_block" />
		<param name="bb2" type="basic_block" />
	</method>	

</class>

<class name="basic_block" base="::lestes::std::object">
	<dox>
		<bri>Represents a basic block.</bri>
		<det>A basic block is a straight-line piece of code without any jumps or jump targets in the middle; jump targets, if any, start a block, and jumps end a block.</det>
	</dox>
	
	<field name="fsp" type="ge_sp" get="public" set="public" alt="protected" init="NULL" check="">
		<dox>
			<bri>A sequencepoint that encloses beginning of the basic block.</bri>
		</dox>
	</field>
	
	<field name="lsp" type="ge_sp" get="public" set="public" alt="protected" init="NULL" check="">
		<dox>
			<bri>A sequencepoint that encloses end of the basic block.</bri>
		</dox>
	</field>
	
	<collection name="instructions" type="ge_pi" kind="vector" get="public" set="protected" alt="protected" init="" >
		<dox>
			<bri>A list of the basic block's instructions.</bri>
		</dox>
	</collection>
	
</class>

</lsd>

