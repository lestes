<?xml version="1.0" encoding="UTF-8"?>
<lsd xmlns="http://lestes.jikos.cz/schemas/lsd" xmlns:h="http://www.w3.org/TR/REC-html40">

<dox file="both">
	<det>\author jaz</det>
</dox>

<file-name>spillgen</file-name>

<packages>
	<p>lestes</p>
	<p>backend_v2</p>
	<p>workers</p>
</packages>

<imports>
	<i>lestes/std/list.hh</i>
	<i>lestes/std/set.hh</i>
	<i>lestes/backend_v2/common.hh</i>
	<i>lestes/backend_v2/workers/worker_base.g.hh</i>
</imports>

<implementation-imports>
	<i>lestes/backend_v2/structs/func_data.g.hh</i>
	<i>lestes/backend_v2/intercode/ge.g.hh</i>
	<i>lestes/backend_v2/intercode/pi_mem_factory.g.hh</i>
	<i>lestes/backend_v2/workers/alloc_int_finder.g.hh</i>
	<i>lestes/backend_v2/workers/bb_finder.g.hh</i>
	<i>lestes/md/registers/tm_register.g.hh</i>
	<i>lestes/md/registers/move_generator.g.hh</i>
	<i>lestes/md/types/tm_data_type_base.g.hh</i>
</implementation-imports>

<foreign-class name="func_data" >
	<p>lestes</p>
	<p>backend_v2</p>
	<p>structs</p>
</foreign-class>

<foreign-class name="ge_pi" >
	<p>lestes</p>
	<p>backend_v2</p>
	<p>intercode</p>
</foreign-class>

<foreign-class name="ge_operand_mem" >
	<p>lestes</p>
	<p>backend_v2</p>
	<p>intercode</p>
</foreign-class>

<foreign-class name="ge_operand_reg" >
	<p>lestes</p>
	<p>backend_v2</p>
	<p>intercode</p>
</foreign-class>

<foreign-class name="alloc_interval" >
	<p>lestes</p>
	<p>backend_v2</p>
	<p>workers</p>
</foreign-class>

<foreign-class name="tm_instr_op_reg_base" >
	<p>lestes</p>
	<p>md</p>
	<p>instructions</p>
</foreign-class>

<foreign-class name="pi_mem_factory" >
	<p>lestes</p>
	<p>backend_v2</p>
	<p>intercode</p>
</foreign-class>

<foreign-class name="tm_register" >
	<p>lestes</p>
	<p>md</p>
	<p>registers</p>
</foreign-class>

<foreign-class name="tm_data_type_base" >
	<p>lestes</p>
	<p>md</p>
	<p>types</p>
</foreign-class>

<foreign-class name="move_generator" >
	<p>lestes</p>
	<p>md</p>
	<p>registers</p>
</foreign-class>

<foreign-class name="basic_block" >
	<p>lestes</p>
	<p>backend_v2</p>
	<p>workers</p>
</foreign-class>

<include href="worker_base.lsd" />

<class name="spillgen" base="worker_base">
	<dox>
		<bri>Performs spillgen generation.</bri>
	</dox>
	
	<collection name="active_intervals" type="alloc_interval" kind="vector" get="protected" set="protected" alt="protected" init="" >
		<dox>
			<bri>A list of active intervals.</bri>
		</dox>
	</collection>
	
	<collection name="expired_intervals" type="alloc_interval" kind="vector" get="protected" set="protected" alt="protected" init="" >
		<dox>
			<bri>A list of already processed intervals.</bri>
		</dox>
	</collection>
	
	<collection name="waiting_intervals" type="alloc_interval" kind="vector" get="protected" set="protected" alt="protected" init="NULL" check="">
		<dox>
			<bri>A list of intervals witing for processing.</bri>
		</dox>
	</collection>

	<collection name="free_registers" type="ulint" kind="set" get="protected" set="protected" alt="protected" init="" >
		<dox>
			<bri>A set of free registers.</bri>
		</dox>
	</collection>
	
	<collection name="used_registers" type="ulint" kind="set" get="protected" set="protected" alt="protected" init="" >
		<dox>
			<bri>A set of used registers.</bri>
		</dox>
	</collection>
	
	<collection name="all_registers" type="ulint" kind="set" get="protected" set="protected" alt="protected" init="" >
		<dox>
			<bri>A set of all the registers that machine description contains.</bri>
		</dox>
	</collection>	

	<collection name="register_owners" key="ulint" type="alloc_interval" kind="map" get="protected" set="protected" alt="protected" init="" >
		<dox>
			<bri>A map that holds information about which interval has which register allocated.</bri>
		</dox>
	</collection>	
	
	<collection name="free_spill_spaces" type="pi_mem_factory" kind="set" get="protected" set="protected" alt="protected" init="" >
		<dox>
			<bri>A set of free spill-places.</bri>
		</dox>
	</collection>
	
	<field name="move_gen" type="move_generator" get="protected" set="protected" alt="protected" init="NULL" check="">
		<dox>
			<bri>Provides generation of copy-code between register and memory.</bri>
		</dox>
	</field>
	
	<field name="registers_freed" type="bool" init="false" />
		
	<collection name="spill_space_last_use" key="pi_mem_factory" type="ge_pi" kind="map" get="protected" set="protected" alt="protected" init="" >
		<dox>
			<bri>A map between a spill-space and its last use.</bri>
		</dox>
	</collection>
	
	<collection name="register_last_use" key="ulint" type="ge_pi" kind="map" get="protected" set="protected" alt="protected" init="" >
		<dox>
			<bri>A map between a register and its last use.</bri>
		</dox>
	</collection>
	
	<collection name="op2interval" key="ge_operand_reg" type="alloc_interval" kind="map" get="protected" set="protected" alt="protected" init="" >
		<dox>
			<bri>A map between an operand and its active interval.</bri>
		</dox>
	</collection>
	
	<collection name="op2group" key="ge_operand_reg" type="spillgen_group" kind="map" get="protected" set="protected" alt="protected" init="" >
		<dox>
			<bri>A map between an operand and its active group.</bri>
		</dox>
	</collection>
	
	<collection name="groups" type="spillgen_group" kind="list" get="protected" set="protected" alt="protected" init="" >
		<dox>
			<bri>Active groups.</bri>
		</dox>
	</collection>
	
	<collection name="regs_used_by_groups" type="ulint" kind="set" get="protected" set="protected" alt="protected" init="" >
		<dox>
			<bri>A set of registers used by active groups.</bri>
		</dox>
	</collection>
	
	<collection name="curr_generated_instructions" type="ge_pi" kind="set" get="protected" set="protected" alt="protected" init="" >
		<dox>
			<bri>A spill-code that has been generated for a current intruction.</bri>
		</dox>
	</collection>
	
	<method name="process" type="void" visibility="public"/>
	
	<method name="get_result" type="func_data" visibility="public" />		
	
	<method name="find_last_use_of_register" type="ge_pi" visibility="protected" >
		<param name="regid" type="ulint" />
	</method>
	
	<method name="set_last_use_of_register" type="void" visibility="protected" >
		<param name="regid" type="ulint" />
		<param name="ge" type="ge_pi" />
	</method>
	
	<method name="process_instruction" type="void" visibility="protected" >
		<param name="ge" type="ge_pi" />
		<param name="output" type="ptr&lt;list&lt;srp&lt; ::lestes::backend_v2::intercode::ge_pi &gt; &gt; &gt;" />
		<param name="insert_pos" type="list&lt;srp&lt; ::lestes::backend_v2::intercode::ge_pi &gt; &gt;::iterator" />
	</method>
	
	<method name="generate_spill_code" type="void" visibility="protected" >
		<param name="ge" type="ge_pi" />
		<param name="output" type="ptr&lt;list&lt;srp&lt; ::lestes::backend_v2::intercode::ge_pi &gt; &gt; &gt;" />
		<param name="insert_pos" type="list&lt;srp&lt; ::lestes::backend_v2::intercode::ge_pi &gt; &gt;::iterator" />
	</method>
	
	<method name="generate_backup_code" type="ptr&lt;vector&lt;srp&lt; ::lestes::backend_v2::intercode::ge_pi &gt; &gt; &gt;" visibility="protected" >
		<param name="ge" type="ge_pi" />
		<param name="group" type="spillgen_group" />
		<param name="reg_orig_owners" type="ptr&lt;set&lt;srp&lt; ::lestes::backend_v2::workers::alloc_interval &gt; &gt; &gt;" />
	</method>
	
	<method name="generate_restore_code" type="ptr&lt;vector&lt;srp&lt; ::lestes::backend_v2::intercode::ge_pi &gt; &gt; &gt;" visibility="protected" >
		<param name="ge" type="ge_pi" />
		<param name="group" type="spillgen_group" />
		<param name="reg_orig_owners" type="ptr&lt;set&lt;srp&lt; ::lestes::backend_v2::workers::alloc_interval &gt; &gt; &gt;" />
	</method>
	
	<method name="generate_load_code" type="ptr&lt;vector&lt;srp&lt; ::lestes::backend_v2::intercode::ge_pi &gt; &gt; &gt;" visibility="protected" >
		<param name="ge" type="ge_pi" />
		<param name="group" type="spillgen_group" />
		<param name="interval" type="alloc_interval" />
		<param name="operand" type="ge_operand_reg" />
		<param name="reg" type="tm_register" />
	</method>
	
	<method name="generate_store_code" type="ptr&lt;vector&lt;srp&lt; ::lestes::backend_v2::intercode::ge_pi &gt; &gt; &gt;" visibility="protected" >
		<param name="ge" type="ge_pi" />
		<param name="group" type="spillgen_group" />
		<param name="interval" type="alloc_interval" />
		<param name="operand" type="ge_operand_reg" />
		<param name="reg" type="tm_register" />
	</method>
	
	<method name="insert_code_to_bb" type="void" visibility="protected" >
		<param name="bb" type="basic_block" />
		<param name="code" type="ptr&lt;vector&lt;srp&lt; ::lestes::backend_v2::intercode::ge_pi &gt; &gt; &gt;" />
		<param name="back" type="bool" />
	</method>
	
	<method name="free_group_resources" type="void" visibility="protected" >
	</method>
	
	<method name="interval_find_previous_instruction" type="ge_pi" visibility="protected" >
		<param name="interval" type="alloc_interval" />
		<param name="pos" type="ulint" />
	</method>
	
	<method name="interval_find_next_instruction" type="ge_pi" visibility="protected" >
		<param name="interval" type="alloc_interval" />
		<param name="pos" type="ulint" />
	</method>
	
	<method name="find_register_owners" type="ptr&lt;set&lt;srp&lt; ::lestes::backend_v2::workers::alloc_interval &gt; &gt; &gt;" visibility="protected" >
		<param name="reg" type="tm_register" />
	</method>
	
	<method name="find_groups_by_reg" type="ptr&lt;set&lt;srp&lt; ::lestes::backend_v2::workers::spillgen_group &gt; &gt; &gt;" visibility="protected" >
		<param name="reg" type="tm_register" />
	</method>
	
	<method name="allocate_regs_for_groups" type="void" visibility="protected" >
	</method>
	
	<method name="allocate_reg_for_group" type="void" visibility="protected" >
		<param name="group" type="spillgen_group" />
	</method>
	
	<method name="steal_register_from_siblings" type="void" visibility="protected" >
		<param name="thief" type="spillgen_group" />
		<param name="allowed_regs" type="ptr&lt;set&lt;ulint&gt; &gt;" />
	</method>
	
	<method name="identify_groups" type="bool" visibility="protected" >
		<param name="ge" type="ge_pi" />
	</method>
	
	<method name="filter_regs_by_data_type" type="ptr&lt;set&lt;ulint&gt; &gt;" visibility="protected" >
		<param name="reg_set" type="ptr&lt;set&lt;ulint&gt; &gt;" />
		<param name="type" type="ulint" />
	</method>
	
	<method name="setup_registers" type="void" visibility="protected" >
	</method>
	
	<method name="find_free_registers" type="void" visibility="protected" >
	</method>
	
	<method name="get_free_spill_space" type="pi_mem_factory" visibility="protected" >
		<param name="type" type="tm_data_type_base" />
	</method>
	
	<method name="expire_old_intervals" type="bool" visibility="protected" >
		<param name="curr_point" type="ulint" />
	</method>
	
	<method name="activate_waiting_intervals" type="bool" visibility="protected" >
		<param name="curr_point" type="ulint" />
	</method>
	
	<method name="set_instruction_property" type="void" visibility="protected" >
		<param name="instrs" type="ptr&lt;vector&lt;srp&lt; ::lestes::backend_v2::intercode::ge_pi &gt; &gt; &gt;" />
		<param name="property_id" type="ulint" />
		<param name="property_value" type="lstring" />
	</method>
	
</class>

<class name="spillgen_group" base="::lestes::std::object">
	<dox>
		<bri>Represents a group of operands of a pseudoinstruction that occupy the same register.</bri>
	</dox>
	
	<collection name="operands" type="ge_operand_reg" kind="set" get="public" set="public" alt="protected" init="" >
		<dox>
			<bri>The registers.</bri>
		</dox>
	</collection>
	
	<collection name="allowed_registers" type="ulint" kind="set" get="public" set="public" alt="protected" init="NULL"  check="">
		<dox>
			<bri>A list of allowed registers for all the operands together.</bri>
		</dox>
	</collection>
	
	<collection name="avail_regs_for_input_ops" type="ulint" kind="set" get="public" set="public" alt="protected" init="NULL"  check="">
		<dox>
			<bri>A list of registers that is alvailable for input operands of the group.</bri>
		</dox>
	</collection>
	
	<collection name="avail_regs_for_output_ops" type="ulint" kind="set" get="public" set="public" alt="protected" init="NULL"  check="">
		<dox>
			<bri>A list of registers that is alvailable for output operands of the group.</bri>
		</dox>
	</collection>
	
	<field name="reg" type="tm_register" init="NULL" check="" >
		<dox>
			<bri>An assigned spill-register.</bri>
		</dox>
	</field>
	
	<field name="guarded_group" type="spillgen_group" init="NULL" check="" >
		<dox>
			<bri>A group whose register can't be stolen by this group.</bri>
		</dox>
	</field>
	
	<field name="is_input" type="bool" init="false" >
		<dox>
			<bri>Tells whether the group contains an input operand.</bri>
		</dox>
	</field>
	
	<field name="is_output" type="bool" init="false" >
		<dox>
			<bri>Tells whether the group contains an output operand.</bri>
		</dox>
	</field>
	
	<field name="backup_space" type="ge_operand_mem" init="NULL" check="" >
		<dox>
			<bri>An assigned spill-place.</bri>
		</dox>
	</field>
	
	<collection name="backup_instructions" type="ge_pi" kind="vector" get="public" set="public" alt="protected" init="NULL" check="">
		<dox>
			<bri>A code that backups previous value of the spill-register to a backup place.</bri>
		</dox>
	</collection>
	
	<collection name="load_instructions" type="ge_pi" kind="set" get="public" set="public" alt="protected" init="" >
		<dox>
			<bri>A code that loads value of the group's operands to a spill-register.</bri>
		</dox>
	</collection>
	
	<collection name="store_instructions" type="ge_pi" kind="set" get="public" set="public" alt="protected" init="" >
		<dox>
			<bri>A code that stores value of the group's operands to a spill-place.</bri>
		</dox>
	</collection>
	
	<collection name="restore_instructions" type="ge_pi" kind="vector" get="public" set="public" alt="protected" init="NULL" check="">
		<dox>
			<bri>A code that restores previous value of the spill-register from a backup place.</bri>
		</dox>
	</collection>
</class>


</lsd>

