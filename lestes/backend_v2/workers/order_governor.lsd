<?xml version="1.0" encoding="UTF-8"?>
<lsd xmlns="http://lestes.jikos.cz/schemas/lsd" xmlns:h="http://www.w3.org/TR/REC-html40">

	<dox file="both">
		<bri>Fuction body linearizator.</bri>
		<det>\author jaz</det>
	</dox>

	<file-name>order_governor</file-name>
    
	<packages>
		<p>lestes</p>
		<p>backend_v2</p>
		<p>workers</p>
	</packages>
    
	<imports>
		<i>lestes/backend_v2/common.hh</i>
		<i>lestes/std/map.hh</i>
		<i>lestes/std/vector.hh</i>
		<i>lestes/backend_v2/workers/worker_base.g.hh</i>
	</imports>
	
	<implementation-imports>
		<i>lestes/backend_v2/intercode/pi.g.hh</i>
		<i>lestes/backend_v2/structs/func_data.g.hh</i>
	</implementation-imports>
    
    
	<foreign-class name="pi_pi">
		<p>lestes</p>
		<p>backend_v2</p>
		<p>intercode</p>
	</foreign-class>
	
	<foreign-class name="pi_sp">
		<p>lestes</p>
		<p>backend_v2</p>
		<p>intercode</p>
	</foreign-class>
	
	<foreign-class name="pi_operand">
		<p>lestes</p>
		<p>backend_v2</p>
		<p>intercode</p>
	</foreign-class>
	
	<foreign-class name="func_data">
		<p>lestes</p>
		<p>backend_v2</p>
		<p>structs</p>
	</foreign-class>
    
	<include href="worker_base.lsd" />
	
	<class name="order_governor" base="worker_base" >
		<dox>
			<bri>Linearizes pseudopinstructions within function's body.</bri>
			<det>Result of linearization is order where every pseudoinstruction is positioned
between its psp and nsp sequencepoints and after origins of its operands. It means that pseudoinstruction
is executed after all its origins are ready.</det>
		</dox>

		<method name="process" type="void" visibility="public"/>
		<method name="get_result" type="func_data" visibility="public" />			
		
		<collection name="pis_ordered" type="pi_pi" kind="list" get="private" set="private" alt="private" init="" >
			<dox>
				<bri>List of already linearized pseudoinstructions.</bri>
			</dox>
		</collection>
		
		<collection name="pis_unordered" type="pi_pi" kind="list" get="private" set="private" alt="private" init="NULL" check="">
			<dox>
				<bri>List of not yet linerized pseudoinstructions.</bri>
			</dox>
		</collection>
		
		<collection name="is_ordered" type="bool" kind="map" key="pi_pi" get="private" set="private" alt="private" init="" >
			<dox>
				<bri>Map that tells whether a pseudoinstruction is already in ordered list.</bri>
			</dox>
		</collection>
		
		<typedef name="level_t" type="ulint" />
	
		<method name="merge_ordered_with_unordered_level_X" type="void" visibility="private">
			<dox>
				<bri>Merges unordered pseudoinstructions of given level with ordered pseudoinstructions.</bri>
			</dox>
	    	<param name="level_to_merge" type="level_t" />
		</method>
	
		<method name="torder_ordered" type="void" visibility="private" >
			<dox>
				<bri>Linearizes pis_ordered list.</bri>
			</dox>
		</method>
	
		<method name="pi_operands_origins_ordered" type="bool" visibility="private">
			<dox>
				<bri>Tells whether origin of each operand in list is ordered.</bri>
			</dox>
	    	<param name="operand_list" type="ptr&lt;vector&lt; srp&lt; ::lestes::backend_v2::intercode::pi_operand &gt; &gt; &gt;" />
		</method>
	
		<method name="renumber_levels" type="void" visibility="private" >
			<dox>
				<bri>Decrease by 1 level of pseudoinstructions in a list.</bri>
			</dox>
	    	<param name="pi_list" type="ptr&lt;list&lt; srp&lt; ::lestes::backend_v2::intercode::pi_pi &gt; &gt; &gt;" />
		</method>
		
		<method name="nsp_for_any" type="bool" visibility="private">
			<dox>
				<bri>Tells whether a sequencepoint is psp for any of pseudoinstructions in a list.</bri>
			</dox>
			<param name="sp" type="pi_pi" />
			<param name="list" type="ptr&lt;list&lt; srp&lt; ::lestes::backend_v2::intercode::pi_pi &gt; &gt; &gt;" />
		</method>
		
		<method name="identify_level_0" type="void" visibility="private" >
			<dox>
				<bri>Moves pseudoinstructions with level 0 from pis_unordered to pis_ordered list.</bri>
			</dox>
		</method>
		
		<method name="linearize_ordered_sps" type="void" visibility="private" >
			<dox>
				<bri>Linearizes sequencepoints in pis_ordered list.</bri>
			</dox>
		</method>
		
		<method name="shift_unordered_nsp_psp" type="void" visibility="private" >
			<dox>
				<bri>Shifts psp and nsp of pis_unordered pseudoinstructions.</bri>
			</dox>
		</method>
	
		<method name="get_pi_level" type="lint" visibility="private">
			<dox>
				<bri>Returns level of a pi_pi pseudoinstruction.</bri>
			</dox>
			<param name="pi" type="pi_pi" />
		</method>
		
		<method name="conjugated_chain_end_find" type="pi_pi" visibility="private">
			<dox>
				<bri>Gets last pi_pi pseudoinstruction of chain where two consequent pi_pi point with psp and nsp to each other.</bri>
			</dox>
			<param name="pi" type="pi_pi" />
		</method>
		
		<method name="conjugated_chain_start_find" type="pi_pi" visibility="private">
			<dox>
				<bri>Gets first pi_pi pseudoinstruction of chain where two consequent pi_pi point with psp and nsp to each other.</bri>
			</dox>
			<param name="pi" type="pi_pi" />
		</method>
    </class>

</lsd>
