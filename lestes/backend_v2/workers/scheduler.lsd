<?xml version="1.0" encoding="UTF-8"?>
<lsd xmlns="http://lestes.jikos.cz/schemas/lsd" xmlns:h="http://www.w3.org/TR/REC-html40">

<dox file="both">
	<det>\author jaz</det>
</dox>

<file-name>scheduler</file-name>

<packages>
	<p>lestes</p>
	<p>backend_v2</p>
	<p>workers</p>
</packages>

<imports>
	<i>lestes/std/list.hh</i>
	<i>lestes/std/set.hh</i>
	<i>lestes/std/map.hh</i>
	<i>lestes/backend_v2/common.hh</i>
	<i>lestes/backend_v2/workers/worker_base.g.hh</i>
</imports>

<implementation-imports>
	<i>lestes/backend_v2/structs/func_data.g.hh</i>
	<i>lestes/backend_v2/intercode/pi.g.hh</i>
	<i>lestes/backend_v2/intercode/ge.g.hh</i>
	<i>lestes/backend_v2/workers/bb_finder.g.hh</i>
</implementation-imports>

<foreign-class name="func_data" >
	<p>lestes</p>
	<p>backend_v2</p>
	<p>structs</p>
</foreign-class>

<foreign-class name="basic_block" >
	<p>lestes</p>
	<p>backend_v2</p>
	<p>workers</p>
</foreign-class>

<foreign-class name="pi_pi" >
	<p>lestes</p>
	<p>backend_v2</p>
	<p>intercode</p>
</foreign-class>

<foreign-class name="pi_sp" >
	<p>lestes</p>
	<p>backend_v2</p>
	<p>intercode</p>
</foreign-class>

<foreign-class name="ge_pi" >
	<p>lestes</p>
	<p>backend_v2</p>
	<p>intercode</p>
</foreign-class>

<include href="worker_base.lsd" />

<class name="scheduler" base="worker_base">
	<dox>
		<bri>Performs list scheduling on a function's body based on critical path method.</bri>
	</dox>
	
	<method name="process" type="void" visibility="public"/>
	<method name="get_result" type="func_data" visibility="public" />	
	
	<field name="dumb_scheduling" type="bool" get="public" set="protected" alt="protected" init="false">
		<dox>
			<bri>A switch that turns on/off "dumb scheduling" </bri>
			<det>Dumb scheduling = instruction execution time is ignored; it takes care only about instruction dependencies.</det>
		</dox>
	</field>
	
	<field name="current_schedule_pos" type="ulint" get="public" set="protected" alt="protected" init="0" >
		<dox>
			<bri>A schedule position of a currently scheduled pseudoinstruction.</bri>
		</dox>
	</field>
	
	<collection name="ge2si" key="ge_pi" type="schedule_item" kind="map" get="public" set="public" alt="protected" init="">
		<dox>
			<bri>A map between a pseudoinstruction and a corresponding schedule_item.</bri>
		</dox>
	</collection>	
	
	<method name="process_bb" type="ptr&lt;vector&lt;srp&lt; ::lestes::backend_v2::intercode::ge_pi&gt; &gt; &gt;" >
		<param name="bb" type="basic_block" />
	</method>
	
	<method name="find_schedule_items" type="ptr&lt;set&lt;srp&lt; ::lestes::backend_v2::workers::schedule_item&gt; &gt; &gt;" >
		<param name="bb" type="basic_block" />
	</method>
	
	<method name="find_critical_paths" type="void" >
		<param name="items" type="ptr&lt;set&lt;srp&lt; ::lestes::backend_v2::workers::schedule_item&gt; &gt; &gt;" />
	</method>
	
	<method name="schedule_items" type="ptr&lt;vector&lt;srp&lt; ::lestes::backend_v2::intercode::ge_pi&gt; &gt; &gt;" >
		<param name="items" type="ptr&lt;set&lt;srp&lt;schedule_item&gt; &gt; &gt;" />
	</method>
	
	<method name="create_schedule_item" type="schedule_item" >
		<param name="ge" type="ge_pi" />
	</method>

	<method name="check_waiting_set" type="void" >
		<param name="waiting" type="ptr&lt;set&lt;srp&lt;schedule_item&gt; &gt; &gt;" />
	</method>	
	
	<method name="find_dependence_loop" type="bool" >
		<param name="start" type="schedule_item" />
		<param name="curr" type="schedule_item" />
		<param name="depth" type="ulint" />
	</method>
	
</class>

<class name="schedule_item" base="::lestes::std::object">
	<dox>
		<bri>Represents item of a schedule.</bri>
	</dox>
	
	<field name="instruction" type="ge_pi" get="public" set="protected" alt="protected">
		<dox>
			<bri>The pseudonstruction that is represented by this instance of schedule_item.</bri>
		</dox>
	</field>
	
	<collection name="in_deps" type="schedule_item" kind="set" get="public" set="public" alt="protected" init="" check="">
		<dox>
			<bri>A set of schedule_items on which this item is dependent.</bri>
		</dox>
	</collection>
		
	<collection name="in_deps_copy" type="schedule_item" kind="set" get="public" set="public" alt="protected" init="" check="">	
		<dox>
			<bri>A copy of the in_deps set.</bri>
		</dox>
	</collection>
	
	<collection name="out_deps" type="schedule_item" kind="set" get="public" set="public" alt="protected" init="" check="">	
		<dox>
			<bri>A set of schedule_items that are dependent on this item.</bri>
		</dox>
	</collection>
	
	<collection name="out_deps_copy" type="schedule_item" kind="set" get="public" set="public" alt="protected" init="" check="">	
		<dox>
			<bri>A copy of the out_deps set.</bri>
		</dox>
	</collection>
	
	<field name="etime" type="ulint" get="public" set="protected" alt="protected">
		<dox>
			<bri>Execution time of the instruction.</bri>
		</dox>
	</field>
	
	<field name="ctime" type="ulint" get="public" set="protected" alt="protected">
		<dox>
			<bri>A time perid that elapses after the instruction's execution before an output operand is ready.</bri>
		</dox>
	</field>
	
	<field name="start_time" type="ulint" get="public" set="protected" alt="protected" init="0">
		<dox>
			<bri>A start time within critical path method.</bri>
		</dox>
	</field>
	
	<field name="end_time" type="ulint" get="public" set="protected" alt="protected" init="0">
		<dox>
			<bri>An end time within critical path method.</bri>
		</dox>
	</field>
	
	<field name="latest_start_time" type="ulint" get="public" set="protected" alt="protected" init="0xfffffffe">
		<dox>
			<bri>A latest start time within critical path method.</bri>
		</dox>
	</field>
	
</class>

</lsd>

