#include <lestes/backend_v2/structs/func_data.g.hh>
#include <lestes/backend_v2/workers/pi_cond_jmp_rewriter.g.hh>
#include <lestes/backend_v2/intercode/pi.g.hh>
#include <lestes/backend_v2/intercode/visitor_pi_pi2id.g.hh>
#include <lestes/md/types/ss_type2tm_type_convertor.g.hh>

package(lestes);
package(backend_v2);
package(workers);

using namespace ::lestes::backend_v2::intercode;
using namespace ::lestes::backend_v2::structs;
using namespace ::lestes::md::types;

typedef list<srp<pi_pi> > pi_list__type;

/*!
	\brief Rewrites conditional jumps.
*/
void pi_cond_jmp_rewriter::process() {

	ptr<visitor_pi_pi2id> pi_id_getter = visitor_pi_pi2id::create();
	
	ptr<pi_list__type> body = data_get()->pi_body_get();
	
	for(pi_list__type::iterator it = body->begin(); it!=body->end(); ++it) {
		
		if ( visitor_pi_pi2id::PI_BF!= (visitor_pi_pi2id::kind_type)(*it)->accept_visitor_pi_pi2ulint_gen_base(pi_id_getter) ) {
			continue;
		}
		
		ptr<pi_bf> bf = (*it).dncast<pi_bf>();
		ptr<pi_pi> setter_generic = bf->condition_get()->origin_get();
		
		ptr<pi_preg> out_cmp_res = pi_preg::create(NULL,ss_type2tm_type_convertor::instance()->conditional_preg_type_get());
									
		ptr<pi_cb_single_label_target> out_jmp = NULL;
		ptr<pi_tdtpi> setter = NULL;
		
		switch (setter_generic->accept_visitor_pi_pi2ulint_gen_base(pi_id_getter) ) {
			case visitor_pi_pi2id::PI_SBG: {		
				setter = setter_generic.dncast<pi_tdtpi>();
				
				out_jmp = pi_bng::create(
					bf->psp_get(),
					bf->nsp_get(),
					bf->level_get(),
					out_cmp_res,
					bf->destination_get());	
			} break;
			
			case visitor_pi_pi2id::PI_SBL: {		
				setter = setter_generic.dncast<pi_tdtpi>();
				
				out_jmp = pi_bnl::create(
					bf->psp_get(),
					bf->nsp_get(),
					bf->level_get(),
					out_cmp_res,
					bf->destination_get());	
			} break;
			
			case visitor_pi_pi2id::PI_SBE: {		
				setter = setter_generic.dncast<pi_tdtpi>();
				
				out_jmp = pi_bne::create(
					bf->psp_get(),
					bf->nsp_get(),
					bf->level_get(),
					out_cmp_res,
					bf->destination_get());	
			} break;
			
			case visitor_pi_pi2id::PI_SBNG: {		
				setter = setter_generic.dncast<pi_tdtpi>();
				
				out_jmp = pi_bg::create(
					bf->psp_get(),
					bf->nsp_get(),
					bf->level_get(),
					out_cmp_res,
					bf->destination_get());	
			} break;
			
			case visitor_pi_pi2id::PI_SBNL: {		
				setter = setter_generic.dncast<pi_tdtpi>();
				
				out_jmp = pi_bl::create(
					bf->psp_get(),
					bf->nsp_get(),
					bf->level_get(),
					out_cmp_res,
					bf->destination_get());	
			} break;
			
			case visitor_pi_pi2id::PI_SBNE: {		
				setter = setter_generic.dncast<pi_tdtpi>();
				
				out_jmp = pi_be::create(
					bf->psp_get(),
					bf->nsp_get(),
					bf->level_get(),
					out_cmp_res,
					bf->destination_get());	
			} break;
			
		}
		
		if ( !out_jmp ) {
			continue;
		}
		
		ptr<pi_cmp> out_cmp = pi_cmp::create(
								setter->psp_get(),
								setter->nsp_get(),
								setter->level_get(),
								setter->left_get(),
								setter->right_get(),
								out_cmp_res,
								out_cmp_res->type_get(),
								setter->type2_get());
								
		out_cmp_res->origin_set(out_cmp);
		
		pi_list__type::iterator it_replace = ::std::find(body->begin(),body->end(),setter);
		
		*it_replace = out_cmp;
		*it = out_jmp;
	}
}

/*!
	\brief Returns data of the currently processed function with rewritten jumps.
*/
ptr<func_data> pi_cond_jmp_rewriter::get_result() {
	return data_get();
}

end_package(workers);
end_package(backend_v2);
end_package(lestes);

