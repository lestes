#include <lestes/backend_v2/structs/func_data.g.hh>
#include <lestes/backend_v2/workers/useless_code_eliminator.g.hh>
#include <lestes/backend_v2/intercode/ge.g.hh>
#include <lestes/backend_v2/intercode/pi_mem_factory.g.hh>
#include <lestes/backend_v2/intercode/visitor_pi_pi2id.g.hh>
#include <lestes/md/instructions/tm_instr.g.hh>
#include <lestes/md/registers/tm_register.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>

#include <lestes/backend_v2/debug/debug.hh>

package(lestes);
package(backend_v2);
package(workers);

using namespace ::lestes::backend_v2::intercode;
using namespace ::lestes::backend_v2::structs;
using namespace ::lestes::md::instructions;
using namespace ::lestes::md::registers;

typedef list<srp<ge_pi> > ge_pi_list__type;
typedef vector<srp<ge_operand> > ge_operand_vector__type;
typedef map<ulint, srp<pi_mem_factory> > id2mf__type;
typedef set<ulint> id_set__type;

/*!
	\brief Eliminates a uselesss code.
*/
void useless_code_eliminator::process() {
	
	ptr<ge_pi_list__type> body = data_get()->ge_body_get();

	ptr<id2mf__type> reg2mem = id2mf__type::create();
		
	for(ge_pi_list__type::iterator it = body->begin(); it!=body->end(); ++it) {
		
		ptr<ge_pi> ge = *it;
		
		ptr<tm_instr_base> tm = ge->instruction_get();
		
		if ( ge->kind_get()==ge_pi::SP && ge.dncast<ge_sp>()->is_jmp_target_get() ) {
			//Instruction invalidates reg2mem mapping.
			reg2mem->clear();
			continue;
		}
		
		if ( !tm ) {
			continue;
		}
		
		if ( tm->is_system() ) {
			//Instruction invalidates reg2mem mapping.
			reg2mem->clear();
	 	} else if ( tm->is_copy() ) {
			ptr<ge_operand_reg> in = ge->operands_input_get()->front().dncast<ge_operand_reg>();
			ptr<ge_operand_reg> out = ge->operands_output_get()->front().dncast<ge_operand_reg>();
			ulint assigned_reg1 = (*in->assigned_registers_get())[ge];
			ulint assigned_reg2 = (*out->assigned_registers_get())[ge];
			
			if ( assigned_reg1==assigned_reg2 ) {
				 //Move eax,eax
				 //Erase output.	 
				 ge->instruction_set(NULL);
			}
			
			set_register_user(assigned_reg2,(*reg2mem)[assigned_reg1],reg2mem);
		} else if ( tm->is_load() && ge->operands_input_get()->size()==1 ) {
			//Instruction is load but it's not indirect load.
			ptr<ge_operand> in = ge->operands_input_get()->front();
			ptr<ge_operand> out = ge->operands_output_get()->front();
			
			if ( out->kind_get()==ge_operand::REGISTER ) {
				ptr<ge_operand_reg> reg = out.dncast<ge_operand_reg>();
				ulint assigned_reg = (*reg->assigned_registers_get())[ge];
			
				if ( in->kind_get()==ge_operand::MEMORY ) {
					ptr<pi_mem_factory> mf = in.dncast<ge_operand_mem>()->factory_get();
				
					if ( mf && (*reg2mem)[assigned_reg]==mf ) {
						//The register already contains the mem's value. No load needed.
						ge->instruction_set(NULL);	
					} else {
						//Set flag that the mem's value has been loaded to the reg.
						set_register_user(assigned_reg,mf,reg2mem);
					}
				} else if ( in->kind_get()==ge_operand::IMMEDIATE ) {
					set_register_user(assigned_reg,NULL,reg2mem);
				} else lassert(false);
			}
			
		} else if ( tm->is_store() && ge->operands_input_get()->size()==1 ) {
			//Instruction is store but it's not indirect store.
			ptr<ge_operand> in = ge->operands_input_get()->front();
			ptr<ge_operand> out = ge->operands_output_get()->front();
			
			if ( out->kind_get()==ge_operand::MEMORY && in->kind_get()==ge_operand::REGISTER) {			
				//Set flag that the the reg's value has been stored to the mem.
				set_register_user((*in.dncast<ge_operand_reg>()->assigned_registers_get())[ge],out.dncast<ge_operand_mem>()->factory_get(),reg2mem);
			} else lassert(false);
		} else {
			ptr<ge_operand_vector__type> operands = ge->operands_output_get();
			
			for(ge_operand_vector__type::iterator it_operand = operands->begin(); it_operand!=operands->end(); ++it_operand) {
				if ( (*it_operand)->kind_get()!=ge_operand::REGISTER ) {
					continue;
				}
				
				ptr<ge_operand_reg> op = (*it_operand).dncast<ge_operand_reg>();
				ulint assigned_reg = (*op->assigned_registers_get())[ge];
				
				set_register_user(assigned_reg,NULL,reg2mem);
			}
		}
	}
}

/*!
	\brief Sets a memory place to a register from where a value has been loaded to the register.
	
	\param reg_id The register.
	\param user The memory.
	\param reg2mem A map that holds couples.
*/
void useless_code_eliminator::set_register_user(ulint reg_id,ptr<pi_mem_factory> user,ptr<id2mf__type> reg2mem) {
	ptr<tm_register> reg = tm_register::instance(reg_id);
	ptr<id_set__type> aliases = reg->aliases_get();
	
	for(id_set__type::iterator it = aliases->begin(); it!=aliases->end(); ++it) {
		(*reg2mem)[*it] = user;
	}
}

/*!
	\brief Returns data of a currently processed function without a useless code.
*/
ptr<func_data> useless_code_eliminator::get_result() {
	return data_get();
}



end_package(workers);
end_package(backend_v2);
end_package(lestes);

