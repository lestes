#include <lestes/backend_v2/workers/helpers.hh>
#include <lestes/backend_v2/workers/alloc_int_finder.g.hh>

package(lestes);
package(backend_v2);
package(workers);

/*!
	\brief Compares two intervals.
	
	\param a An interval.
	\param b An interval.
	\return If start of the first interval is less than start of the second interval, it returns true. False otherwise.
*/
bool alloc_int_cmp1(srp<alloc_interval> a, srp<alloc_interval> b) {
	return a->start_get() < b->start_get();
}


/*!
	\brief Compares two intervals.
	
	\param a An interval.
	\param b An interval.
	\return If end of the first interval is greater than end of the second interval, it returns true. False otherwise.
*/
bool alloc_int_cmp2(srp<alloc_interval> a, srp<alloc_interval> b) {
	return a->end_get() > b->end_get();
}


end_package(workers);
end_package(backend_v2);
end_package(lestes);

