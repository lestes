#include <lestes/backend_v2/intercode/ge.g.hh>
#include <lestes/backend_v2/intercode/pi.g.hh>
#include <lestes/backend_v2/intercode/pi_mem_factory.g.hh>
#include <lestes/backend_v2/structs/func_data.g.hh>
#include <lestes/backend_v2/workers/dead_code_eliminator.g.hh>
#include <lestes/md/literals/literal_info_base.g.hh>
#include <lestes/md/instructions/tm_instr.g.hh>
#include <lestes/md/functions/function_parameter_accessor.g.hh>
#include <lestes/lang/cplus/sem/ss_declaration.g.hh>
#include <lestes/std/list.hh>
#include <lestes/std/map.hh>
#include <lestes/std/vector.hh>

package(lestes);
package(backend_v2);
package(workers);

using namespace ::lestes::backend_v2::structs;
using namespace ::lestes::backend_v2::intercode;
using namespace ::lestes::md::instructions;
using namespace ::lestes::md::literals;
using namespace ::lestes::md::functions;

typedef list< srp<ge_pi> > ge_pi_list__type;
typedef vector< srp<ge_operand> > ge_op_vector__type;
typedef set< srp<ge_operand> > ge_op_set__type;
typedef set<ulint> id_set__type;

/*!
	\brief Eliminates a dead code.
	
	It removes any pseudoinstruction whose output operands are not used by another pseudoinstruction.
*/
void dead_code_eliminator::process() {
	
	ptr<ge_op_set__type> used_operands = ge_op_set__type::create();

	//Sweep used operands.	
	ptr<ge_pi_list__type> body = data_get()->ge_body_get();	

	for(ge_pi_list__type::iterator it = body->begin(); it!=body->end(); ++it) {
		ptr<ge_pi> ge = *it;
		
		ptr<ge_op_vector__type> operands = ge->operands_input_get();
		for(ge_op_vector__type::iterator it_op = operands->begin(); it_op!=operands->end(); ++it_op) {
			used_operands->insert(*it_op);
		}
	}
	
	//Operand that represents return value from the function.
	ptr<pi_mem_factory> ret_val = function_parameter_accessor::instance(data_get()->function_decl_get())->get_ret_val();
	
	//Delete instructions that product unused operands.
	for(ge_pi_list__type::iterator it = body->begin(); it!=body->end();) {
		ptr<ge_pi> ge = *it;
		
		if ( ge->kind_get()!=ge_pi::PI ) {
			++it;
			continue;
		}
		
		ptr<tm_instr_base> tm = ge->instruction_get();
		
		if ( !tm || tm->is_store() || tm->is_jump() || tm->is_system() ) {
			++it;
			continue;
		}
		
		bool b_delete = true;
		
		ptr<ge_op_vector__type> operands = ge->operands_output_get();
		for(ge_op_vector__type::iterator it_op = operands->begin(); it_op!=operands->end();++it_op) {
			ptr<ge_operand> op = *it_op;
			
			if ( used_operands->find(op)!=used_operands->end() || 
					(op->pi_source_get() 
					 && op->pi_source_get()->kind_get()!=pi_operand::PREG
					 && op->pi_source_get()->kind_get()!=pi_operand::LIT
					 && op->pi_source_get().dncast<pi_mem>()->factory_get()==ret_val) 
				) {
				/*
					This instruction isn't to be deleted because of an output operand is used by another instruction
					or an output operand is return value from the function.
				*/
				b_delete = false;
				break;
			}
		}
		
		if ( b_delete ) {
			it = body->erase(it);
		} else {
			++it;
		}
		
	}
}

/*!
	\brief Returns function body without any dead code.
*/
ptr<func_data> dead_code_eliminator::get_result() {
	return data_get();
}



end_package(workers);
end_package(backend_v2);
end_package(lestes);

