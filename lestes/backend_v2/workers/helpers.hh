#ifndef lestes__backend_v2__workers__helpers_hh__included
#define lestes__backend_v2__workers__helpers_hh__included

#include <lestes/common.hh>

package(lestes);
package(backend_v2);
package(workers);

class alloc_interval;

bool alloc_int_cmp1(srp<alloc_interval> a, srp<alloc_interval> b);
bool alloc_int_cmp2(srp<alloc_interval> a, srp<alloc_interval> b);

end_package(workers);
end_package(backend_v2);
end_package(lestes);

#endif
