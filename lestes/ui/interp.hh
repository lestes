/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__ui__interp_hh__included
#define lestes__ui__interp_hh__included

#include <lestes/common.hh>
#include <lestes/ui/interp.g.hh>
#include <lestes/ui/interp_actions.g.hh>

package(lestes);
package(ui);

typedef ptr < l_object > lp;

lp car(lp x);
lp cdr(lp x);
lp cons(lp a, lp d);
lp eval(lp a);
ptr < l_cons > extra_cons(lp a, lp d);
lp consp(lp x);
bool endp(lp x);
lp last_eval(lp x);
lp list_eval(lp x);
lp print(lp x);

void scan_string(lstring str);

end_package(ui);
end_package(lestes);

#endif
