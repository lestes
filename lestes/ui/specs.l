%{
/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
%}
ASTART	[abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_$%^*!@]
ACHAR	[abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_$%^:&*!@]
ATOM	{ASTART}{ACHAR}*
CATOM	[&:]{ACHAR}*
STR	\"([^"]|\\.)*\"

%option noyywrap

%%
;.*	/* comment */;
"("	return TOK_LPAREN;
")"	return TOK_RPAREN;
{ATOM}	yylval = l_atom::instance(yytext); return TOK_ATOM;
{STR}	yylval = l_string::create(yytext); return TOK_STRING;
{CATOM} {ptr <l_atom> at = l_atom::instance(yytext); yylval = at; at->set(at); return TOK_ATOM;}
"."	return TOK_DOT;
"'"	return TOK_QUOTE;
"#'"	return TOK_HASH_QUOTE;
[ \t\n]+ ;
. 	return TOK_ERROR;
%%
void scan_string(lstring str)
{
	yy_scan_bytes(str.c_str(), str.size());
}
