/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/common.hh>
#include <lestes/ui/interp.hh>
#include <lestes/ui/interp.g.hh>
#include <lestes/ui/interp_actions.g.hh>

package(lestes);
package(ui);

typedef ptr < l_object > lp;
typedef ptr < l_atom > lap;

ptr < l_object > car(ptr < l_object > x)
{
	return car_evaluator::instance()->go(x);
}

ptr < l_object > cdr(ptr < l_object > x)
{
	return cdr_evaluator::instance()->go(x);
}

ptr < l_object > cons(ptr < l_object > a, ptr < l_object > d)
{
	return l_cons::create(a, d);
}

ptr < l_cons > extra_cons(ptr < l_object > a, ptr < l_object > d)
{
	return l_cons::create(a, d);
}

lp consp(lp x)
{
	return consp_evaluator::instance()->go(x);
}

bool endp(lp x)
{
	return consp(x) == l_interpreter::nil_get();
}

lp last_eval(lp x)
{
	lp res;
	while (!endp(x)) {
		res = eval(car(x));
		x = cdr(x);
	}
	return res;
}

lp list_eval(lp x)
{
	ptr < l_cons > res, run, tmp;
	if (endp(x))
		return eval(x);
	run = res = extra_cons(l_interpreter::nil_get(), l_interpreter::nil_get());
	while (!endp(x)) {
		run->car_set(eval(car(x)));
		x = cdr(x);
		if (!endp(x)) {
			tmp = extra_cons(l_interpreter::nil_get(), l_interpreter::nil_get());
			run->cdr_set(tmp);
			run = tmp;
		}
	}
	run->cdr_set(eval(x));
	return res;
}

lp print(lp a)
{
	printer::instance()->go(a);
	return a;
}

ptr < l_object > eval(ptr < l_object > x)
{
	return basic_evaluator::instance()->go(x);
}

lp lb_function::run(lp x)
{
	return function_evaluator::instance()->go(car(x));
}

lp lb_eq::run(lp x)
{
	lp a = eval(car(x));
	lp b = eval(car(cdr(x)));
	
	return a==b ? l_interpreter::t_get() : l_interpreter::nil_get();
}

ptr < l_object > lb_car::run(ptr < l_object > args)
{
	return car(eval(car(args)));
}

ptr < l_object > lb_cdr::run(ptr < l_object > args)
{
	return cdr(eval(car(args)));
}

ptr < l_object > lb_cond::run(ptr < l_object > args)
{
	lp res = l_interpreter::nil_get();

	while (!endp(args)) {
		lp clause = car(args);
		args = cdr(args);

		lp pred = eval(car(clause));
		if (pred != l_interpreter::nil_get()) {
			lp body = cdr(clause);
			res = endp(body) ? pred : last_eval(body);
			break;
		}
	}

	return res;
}

ptr < l_object > lb_lambda::run(ptr < l_object > a)
{
	ptr < list < srp < l_atom > > > args = list < srp < l_atom > >::create();
	ptr < l_atom > rest_arg;
	lp body = cdr(a);
	lp i;
	for (i = car(a) ; !endp(i) ; i = cdr(i)) {
		args->push_back(car(i).dncast<l_atom>());
	}
	if (i != l_interpreter::nil_get())
		rest_arg = i.dncast<l_atom>();
	return l_lambda::create(args, rest_arg, body);
}

ptr < l_object > lb_macro::run(ptr < l_object > a)
{
	ptr < list < srp < l_atom > > > args = list < srp < l_atom > >::create();
	ptr < l_atom > rest_arg;
	lp body = cdr(a);
	lp i;
	for (i = car(a) ; !endp(i) ; i = cdr(i)) {
		args->push_back(car(i).dncast<l_atom>());
	}
	if (i != l_interpreter::nil_get())
		rest_arg = i.dncast<l_atom>();
	return l_macro::create(args, rest_arg, body);
}

ptr < l_object > lb_quote::run(ptr < l_object > args)
{
	return car(args);
	lstring a,b;
	a<b;
}

ptr < l_object > lb_cons::run(ptr < l_object > args)
{
	lp a = eval(car(args));
	lp d = eval(car(cdr(args)));
	return cons(a, d);
}

ptr < l_object > lb_consp::run(ptr < l_object > args)
{
	return consp(eval(car(args)));
}

lp lb_set::run(lp args)
{
	lp s = eval(car(args));
	lp v = eval(car(cdr(args)));

	s.dncast<l_atom>()->set(v);
	return v;
	
}

lp lb_eval::run(lp a)
{
	return eval(eval(car(a)));
}

lp lb_fset::run(lp args)
{
	lp s = eval(car(args));
	lp v = eval(car(cdr(args)));

	s.dncast<l_atom>()->fset(v);
	return v;
}

lp l_built_in::run(lp x)
{
	return foo->run(x);
}

typedef list < srp < l_atom > >::iterator lait;

lp l_lambda::run(lp x)
{
	for (lait it = args_get()->begin() ;
			it != args_get()->end() ; ++it) {
		(*it)->bind(eval(car(x)));
		x = cdr(x);
	}
	if (rest_get()) {
		rest_get()->bind(list_eval(x));
	}
	lp res = last_eval(body_get());
	for (lait it = args_get()->begin() ;
			it != args_get()->end() ; ++it) {
		(*it)->unbind();
	}
	if (rest_get())
		rest_get()->unbind();
	return res;
}

lp l_macro::run(lp x)
{
	for (lait it = args_get()->begin() ;
			it != args_get()->end() ; ++it) {
		(*it)->bind((car(x)));
		x = cdr(x);
	}
	if (rest_get()) {
		rest_get()->bind((x));
	}
	lp res = last_eval(body_get());
	for (lait it = args_get()->begin() ;
			it != args_get()->end() ; ++it) {
		(*it)->unbind();
	}
	if (rest_get())
		rest_get()->unbind();
	return eval(res);
}

void l_atom::bind(lp x)
{
	lassert(x);
	vstack->push(x);
}

void l_atom::unbind()
{
	lassert(!vstack->empty());
	vstack->pop();
}

void l_atom::set(lp x)
{
	if (vstack->empty())
		bind(x);
	else
		vstack->top()=x;
}

void l_atom::fbind(lp x)
{
	lassert(x);
	fstack->push(x);
}

void l_atom::funbind()
{
	lassert(!fstack->empty());
	fstack->pop();
}

void l_atom::fset(lp x)
{
	if (fstack->empty())
		fbind(x);
	else
		fstack->top()=x;
}

lstring l_string::unescape(lstring str)
{
	char s[str.size()+1];
	const char *p = str.c_str();
	char *q = s;

	for ( ; *p ; ++p) {
		if (*p == '\\')
			switch (*++p) {
			case 'a':
				*q++ = '\a';
				break;
			case 'b':
				*q++ = '\b';
				break;
			case 't':
				*q++ = '\t';
				break;
			case 'f':
				*q++ = '\f';
				break;
			case 'v':
				*q++ = '\v';
				break;
			case 'r':
				*q++ = '\r';
				break;
			case 'n':
				*q++ = '\n';
				break;
			case 'e':
				*q++ = '\033';
				break;
			}
		else
			*q++ = *p;
	}
	*q = '\0';
	return s;
}

lap l_atom::instance(lstring n)
{
	srp < l_atom > & lar = (*atoms)[n];
	if (lar)
		return lar;
	return lar = l_atom::create(n);
}

void l_interpreter::init()
{
	static bool was_here = false;
	if (was_here)
		return;
	was_here = true;

	nil = l_atom::instance("nil");
	t = l_atom::instance("t");
	quote = l_atom::instance("quote");
	function = l_atom::instance("function");
	nil->bind(nil);
	t->bind(t);

#define defun(x) l_atom::instance(#x)->fbind(l_built_in::create(lb_##x::instance()))
	defun(quote);
	defun(function);
	defun(car);
	defun(cdr);
	defun(cons);
	defun(consp);
	defun(cond);
	defun(lambda);
	defun(macro);
	defun(set);
	defun(eq);
#undef defun
#define defunn(n,x) l_atom::instance(n)->fbind(l_built_in::create(lb_##x::instance()))
	defunn("%fset", fset);
#undef defunn
}


end_package(ui);
end_package(lestes);
