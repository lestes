/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/common.hh>
#include <lestes/std/vector.hh>
#include <lestes/std/istream_wrapper.hh>
#include <lestes/lang/cplus/syn/tokeniser.hh>
#include <fstream>
#include <iostream>

package(lestes);
package(ui);

using namespace ::lestes::lang::cplus::syn;

int main(ptr < ::lestes::std::vector < lstring > > args)
{
	vector<lstring>::iterator it = args->begin();
	vector<lstring>::iterator end = args->end();
	lstring progname = args->front();
	++it;

	lstring as_filename = "";
	lstring ss_filename = "";

	for ( ; it != end ; ++it) {
		if ((*it)[0] != '-') {
			tokeniser::input_set(istream_wrapper::create(
				new ::std::ifstream(it->c_str()), true));
			goto name_was_set;
		}
		if (*it == "--dump-flat-as") {
		}
	}
name_was_set:
	if (++it != end) {
		::std::cerr << progname << ": extraneous argument: ``" << *it << "''\n";
		exit(1);
	}
	return 0;
}

end_package(ui);
end_package(lestes);

using namespace ::lestes::std;

int main(int argc, char ** argv)
{
	ptr < ::lestes::std::vector < lstring > > args
		= ::lestes::std::vector < lstring >::create(argv,argv+argc);
	return ::lestes::ui::main(args);
}
