; 
;  The lestes compiler suite
;  Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
;  Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
;  Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
;  Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
;  Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
;  Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
;  Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas
; 
;  This program is free software; you can redistribute it and/or modify
;  it under the terms of the GNU General Public License as published by
;  the Free Software Foundation; version 2 of the License.
; 
;  This program is distributed in the hope that it will be useful,
;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;  GNU General Public License for more details.
; 
;  See the full text of the GNU General Public License version 2, and
;  the limitations in the file doc/LICENSE.
; 
;  By accepting the license the licensee waives any and all claims
;  against the copyright holder(s) related in whole or in part to the
;  work, its use, and/or the inability to use it.
; 
(%fset 'list #'(lambda l l))
(%fset 'defmacro
       #'(macro (n a . b)
             (list '%fset (list 'quote n)
                   (list 'macro a . b))))
(%fset 'defun
       #'(macro (n a . b)
             (list '%fset (list 'quote n)
                   (list 'lambda a . b))))
(%fset 'first #'car)
(%fset 'rest #'cdr)

(defun atom (x) (not (consp x)))
(defun not (x) (eq x nil))
(%fset 'endp #'atom)
(defmacro if (a b c) (list 'cond (list a b) (list t c)))

(defun identity (x) x)

(defun apply (f . args)
  ((identity f) . args))

(defun list* x
  (cond ((endp x) x)
	((endp (cdr x)) (car x))
	(t (cons (car x) (apply #'list* (cdr x))))
	))

(defun append x
  (cond ((endp x) x)
	((endp (cdr x)) (car x))
	(t (append-2 (car x) (apply #'list* (cdr x))))
	))

(defun append-2 (a b)
  (cond ((endp a) b)
	(t (cons (car a) (append-2 (cdr a) b)))
	))

(defmacro setq (a b)
  (list 'set (list 'quote a) b))
(%fset 'defvar #'setq)
