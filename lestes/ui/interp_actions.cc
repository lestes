/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/common.hh>
#include <lestes/ui/interp.hh>
#include <lestes/ui/interp.g.hh>
#include <lestes/ui/interp_actions.g.hh>
#include <lestes/std/lassert.hh>
#include <iostream>

package(lestes);
package(ui);

void car_evaluator::method(ptr < l_object >)
{
	r_set(l_interpreter::nil_get());
}

void car_evaluator::visit_l_cons(ptr < l_cons > c)
{
	r_set(c->car_get());
}

void cdr_evaluator::method(ptr < l_object >)
{
	r_set(l_interpreter::nil_get());
}

void cdr_evaluator::visit_l_cons(ptr < l_cons > c)
{
	r_set(c->cdr_get());
}

void consp_evaluator::method(ptr < l_object >)
{
	r_set(l_interpreter::nil_get());
}

void consp_evaluator::visit_l_cons(ptr < l_cons >)
{
	r_set(l_interpreter::t_get());
}


void function_evaluator::visit_l_atom(ptr< ::lestes::ui::l_atom > x)
{
	lassert(!x->fstack_get()->empty());
	r_set(x->fstack_get()->top());
}

void function_evaluator::method(ptr< ::lestes::ui::l_object > x)
{
	r_set(basic_evaluator::instance()->go(x));
}

void basic_evaluator::visit_l_atom(ptr< ::lestes::ui::l_atom > x)
{
	lassert(!x->vstack_get()->empty());
	r_set(x->vstack_get()->top());
}

void basic_evaluator::visit_l_string(ptr< ::lestes::ui::l_string > x)
{
	r_set(x);
}

void basic_evaluator::visit_l_cons(ptr< ::lestes::ui::l_cons > x)
{
	lp a = function_evaluator::instance()->go(car(x));
	ptr < l_function > f = a.dncast<l_function>();
	r_set(f->run(cdr(x)));
}

void basic_evaluator::visit_l_built_in(ptr< l_built_in > x)
{
	r_set(x);
}

void basic_evaluator::visit_l_lambda(ptr< l_lambda > x)
{
	r_set(x);
}

void basic_evaluator::visit_l_macro(ptr< l_macro > x)
{
	r_set(x);
}

void basic_evaluator::method(ptr< ::lestes::ui::l_object >)
{
	lassert(false);
}

void printer::visit_l_atom( ptr< l_atom > a )
{
	*osw->stream_get() << a->name_get();
}

void printer::visit_l_string( ptr< l_string > a )
{
	*osw->stream_get() << a->escaped_get();
}

void printer::visit_l_cons( ptr< l_cons > a )
{
	::std::ostream & os = *osw->stream_get();
	os << "(";
	print(car(a));
	lp x = a;
	while (!endp(x = cdr(x))) {
		os << " ";
		print(car(x));
	}
	if (x != l_interpreter::nil_get()) {
		os << " . ";
		print(x);
	}

	os << ")";
}

void printer::visit_l_built_in(ptr< l_built_in >)
{
	*osw->stream_get() << "#<built-in>";
}

void printer::visit_l_lambda(ptr< l_lambda >)
{
	*osw->stream_get() << "#<lambda>";
}

void printer::visit_l_macro(ptr< l_macro >)
{
	*osw->stream_get() << "#<macro>";
}

void printer::method(ptr< ::lestes::ui::l_object >)
{
	lassert(false);
}


end_package(ui);
end_package(lestes);
