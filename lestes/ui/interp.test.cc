/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/common.hh>
#include <lestes/ui/interp.hh>
#include <lestes/ui/interp.g.hh>
#include <lestes/ui/interp_actions.g.hh>

package(lestes);
package(ui);

const char * code =
" t nil ''t '(t) '(t . t)\n"
" '(t t t nil a b c d e f) (set 'a 'b) a"
" #'function #'car #'%fset"
"\"aaa\""
"((lambda l l) a t nil)"
"(%fset 'list (lambda l l))"
"(list a t t a)"
"((lambda (a b) b) a t)"
"((macro (a b) (list 'quote a)) baalzabub)"
"(%fset 'defmacro"
"       (macro (n a . b)"
"	      (list '%fset (list 'quote n)"
"		    (list 'macro a . b))))"
"(%fset 'defun"
"       (macro (n a . b)"
"	      (list '%fset (list 'quote n)"
"		    (list 'lambda a . b))))"
"(defun atom (x) (not (consp x)))"
"(defun not (x) (eq x nil))"
"(%fset 'endp 'atom)"
"(cond)"
"(cond ((eq t nil) 'a)"
"      (t 'b))"
"(defmacro if (a b c) (list 'cond (list a b) (list t c)))"
;

void yyparse();

int run()
{
	l_interpreter::init();
	scan_string(code);
	yyparse();
}

end_package(ui);
end_package(lestes);

int main(int, char **)
{
	return lestes::ui::run();
}
