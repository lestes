; 
;  The lestes compiler suite
;  Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
;  Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
;  Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
;  Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
;  Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
;  Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
;  Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas
; 
;  This program is free software; you can redistribute it and/or modify
;  it under the terms of the GNU General Public License as published by
;  the Free Software Foundation; version 2 of the License.
; 
;  This program is distributed in the hope that it will be useful,
;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;  GNU General Public License for more details.
; 
;  See the full text of the GNU General Public License version 2, and
;  the limitations in the file doc/LICENSE.
; 
;  By accepting the license the licensee waives any and all claims
;  against the copyright holder(s) related in whole or in part to the
;  work, its use, and/or the inability to use it.
; 
; the command-line comes in the variable *command-line*

(defvar *predefined*
  (
   ("_GNU_SOURCE" . "1")
   ("__CHAR_BIT__" . "8")
   ("__DBL_DENORM_MIN__" . "4.9406564584124654e-324")
   ("__DBL_DIG__" . "15")
   ("__DBL_EPSILON__" . "2.2204460492503131e-16")
   ("__DBL_HAS_INFINITY__" . "1")
   ("__DBL_HAS_QUIET_NAN__" . "1")
   ("__DBL_MANT_DIG__" . "53")
   ("__DBL_MAX_10_EXP__" . "308")
   ("__DBL_MAX_EXP__" . "1024")
   ("__DBL_MAX__" . "1.7976931348623157e+308")
   ("__DBL_MIN_10_EXP__" . "(-307)")
   ("__DBL_MIN_EXP__" . "(-1021)")
   ("__DBL_MIN__" . "2.2250738585072014e-308")
   ("__DECIMAL_DIG__" . "21")
   ("__DEPRECATED" . "1")
   ("__ELF__" . "1")
   ("__EXCEPTIONS" . "1")
   ("__FINITE_MATH_ONLY__" . "0")
   ("__FLT_DENORM_MIN__" . "1.40129846e-45F")
   ("__FLT_DIG__" . "6")
   ("__FLT_EPSILON__" . "1.19209290e-7F")
   ("__FLT_EVAL_METHOD__" . "2")
   ("__FLT_HAS_INFINITY__" . "1")
   ("__FLT_HAS_QUIET_NAN__" . "1")
   ("__FLT_MANT_DIG__" . "24")
   ("__FLT_MAX_10_EXP__" . "38")
   ("__FLT_MAX_EXP__" . "128")
   ("__FLT_MAX__" . "3.40282347e+38F")
   ("__FLT_MIN_10_EXP__" . "(-37)")
   ("__FLT_MIN_EXP__" . "(-125)")
   ("__FLT_MIN__" . "1.17549435e-38F")
   ("__FLT_RADIX__" . "2")
   ; we are not g++
   ;("__GNUC_MINOR__" . "4")
   ;("__GNUC_PATCHLEVEL__" . "3")
   ;("__GNUC__" . "3")
   ;("__GNUG__" . "3")
   ;("__GXX_ABI_VERSION" . "1002")
   ;("__GXX_WEAK__" . "1")
   ("__INT_MAX__" . "2147483647")
   ("__LDBL_DENORM_MIN__" . "3.64519953188247460253e-4951L")
   ("__LDBL_DIG__" . "18")
   ("__LDBL_EPSILON__" . "1.08420217248550443401e-19L")
   ("__LDBL_HAS_INFINITY__" . "1")
   ("__LDBL_HAS_QUIET_NAN__" . "1")
   ("__LDBL_MANT_DIG__" . "64")
   ("__LDBL_MAX_10_EXP__" . "4932")
   ("__LDBL_MAX_EXP__" . "16384")
   ("__LDBL_MAX__" . "1.18973149535723176502e+4932L")
   ("__LDBL_MIN_10_EXP__" . "(-4931)")
   ("__LDBL_MIN_EXP__" . "(-16381)")
   ("__LDBL_MIN__" . "3.36210314311209350626e-4932L")
   ;("__LONG_LONG_MAX__" . "9223372036854775807LL") ; we do not support long long (yet)
   ("__LONG_MAX__" . "2147483647L")
   ("__NO_INLINE__" . "1")
   ("__PTRDIFF_TYPE__" . "int")
   ("__REGISTER_PREFIX__" . "")
   ("__SCHAR_MAX__" . "127")
   ("__SHRT_MAX__" . "32767")
   ("__SIZE_TYPE__" . "unsigned int")
   ("__STDC_HOSTED__" . "1")
   ("__USER_LABEL_PREFIX__" . "")
   ("__VERSION__" . "\"3.4.3  (Gentoo Linux 3.4.3, ssp-3.4.3-0, pie-8.7.6.6)\"")
   ("__WCHAR_MAX__" . "2147483647")
   ("__WCHAR_TYPE__" . "long int")
   ("__WINT_TYPE__" . "unsigned int")
   ("__cplusplus" . "1")
   ;("__cplusplus" . "199711L") ; after we are conforming implementation
   ("__gnu_linux__" . "1")
   ("__i386" . "1")
   ("__i386__" . "1")
   ("__linux" . "1")
   ("__linux__" . "1")
   ("__tune_i686__" . "1")
   ("__tune_pentiumpro__" . "1")
   ("__unix" . "1")
   ("__unix__" . "1")
   ("i386" . "1")
   ("linux" . "1")
   ("unix" . "1")
   ))


(defvar *user-defined* nil)

(defun process ()
  )
