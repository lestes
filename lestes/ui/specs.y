%{
/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
%}
%token TOK_LPAREN TOK_RPAREN TOK_ATOM TOK_STRING TOK_DOT TOK_QUOTE TOK_ERROR TOK_HASH_QUOTE
%start se_seq
%%
se_seq:
	  /* empty */
	| se_seq1
	;
se_seq1:
	  se_act
	| error
	| se_seq1 se_act
	;
se_act:
	  se
	  {
		$$ = eval($1);
		print($$);
		*printer::instance()->osw_get()->stream_get() << ::std::endl;
	  }
	;
se:
	  TOK_ATOM { $$ = $1; }
	| TOK_LPAREN list_rest { $$ = $2; }
	| TOK_QUOTE se
	  { $$ = cons(l_interpreter::quote_get(), cons($2, l_interpreter::nil_get())); }
	| TOK_HASH_QUOTE se
	  { $$ = cons(l_interpreter::function_get(), cons($2, l_interpreter::nil_get())); }
	;
list_rest:
	  se TOK_RPAREN { $$ = cons($1, l_interpreter::nil_get()); }
	| se TOK_DOT se TOK_RPAREN { $$ = cons($1, $3); }
	| se list_rest { $$ = cons($1, $2); }
	;

%%
