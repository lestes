/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes___equality_hh___included
#define lestes___equality_hh___included

/*! \file
  \brief Equality templates.

  Defines equality and comparison templates.
  Attempt for unified handling of simple types and objects.
  A call of the template on simple types dispatches to the respective operator, 
  while a call on objects dispatches to a respective method.
  NULL values for objects are supported.
  \author pt
*/

#include <lestes/common.hh>

package(lestes);

/*!
  Tests equality of two values of arbitrary types.
  \param Left  The type of the left operand.
  \param Right  The type of the right operand.
  \param left  The first value to compare.
  \param right  The second value to compare.
  \return true  If both values are is_equal.
*/
template <typename Left, typename Right>
bool is_equal(const Left &left, const Right &right)
{
	return left == right;
}

/*!
  Tests equality of two values of arbitrary ptr types.
  \param Left  The type of the pointee of the left operand.
  \param Right  The type of the pointee of the right operand.
  \param left  The first value to compare.
  \param right  The second value to compare.
  \return true  If both values are null or equals returns true.
*/
template <typename Left, typename Right>
bool is_equal(const ptr<Left> &left, const ptr<Right> &right)
{
	// either both are null, or method call succeeded
	if (!left) return !right;
	return left->equals(right);
}

/*!
  Tests equality of two values of arbitrary ptr types.
  \param Left  The type of the pointee of the left operand.
  \param Right  The type of the pointee of the right operand.
  \param left  The first value to compare.
  \param right  The second value to compare.
  \return true  If both values are null or equals returns true.
*/
template <typename Left, typename Right>
bool is_equal(const srp<Left> &left, const ptr<Right> &right)
{
	// either both are null, or method call succeeded
	if (!left) return !right;
	return left->equals(right);
}

/*!
  Tests equality of two values of arbitrary ptr types.
  \param Left  The type of the pointee of the left operand.
  \param Right  The type of the pointee of the right operand.
  \param left  The first value to compare.
  \param right  The second value to compare.
  \return true  If both values are null or equals returns true.
*/
template <typename Left, typename Right>
bool is_equal(const srp<Left> &left, const srp<Right> &right)
{
	// either both are null, or method call succeeded
	if (!left) return !right;
	return left->equals(right);
}

/*!
  Tests equality of two values of arbitrary ptr types.
  \param Left  The type of the pointee of the left operand.
  \param Right  The type of the pointee of the right operand.
  \param left  The first value to compare.
  \param right  The second value to compare.
  \return true  If both values are null or equals returns true.
*/
template <typename Left, typename Right>
bool is_equal(const ptr<Left> &left, const srp<Right> &right)
{
	// either both are null, or method call succeeded
	if (!left) return !right;
	return left->equals(right);
}

/*!
  Tests inequality of two values of arbitrary types.
  \param Left  The type of the left operand.
  \param Right  The type of the right operand.
  \param first  The first value to compare.
  \param second  The second value to compare.
  \return true  If first < second.
*/
template <typename Left, typename Right>
bool is_less(const Left &left, const Right &right)
{
	return left < right;
}
/*!
  Tests inequality of two values of arbitrary ptr types.
  \param Left  The type of the pointee of the left operand.
  \param Right  The type of the pointee of the right operand.
  \param left  The first value to compare.
  \param right  The second value to compare.
  \return true  If only left operand is null or left->less_than(right).
*/
template <typename Left, typename Right>
bool is_less(const ptr<Left> &left, const ptr<Right> &right)
{
	// either left is null so right decides
	if (!left) return right;
	// or method call decides
	return left->less_than(right);
}

end_package(lestes);

#endif
/* vim: set ft=lestes : */
