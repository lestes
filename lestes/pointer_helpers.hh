/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes___pointer_helpers_hh___included
#define lestes___pointer_helpers_hh___included

/*! \file
  \brief Helper templates for smart pointers.
  
  Definition of helper templates to operate uniformly with
  smart pointer and simple types. Useful when the actual type
  is not known, e.g. comes from a template parameter
  \author pt
*/
#include <lestes/common.hh>

package(lestes);

/*!
  Converts type to ptr or srp, if of pointer type,
  otherwise does no conversion for simple types.
*/
template <typename T>
struct convert {
	//! The same type as the parameter.
	typedef T to_ptr;
	//! The same type as the parameter.
	typedef T to_srp;
};

/*!
  Converts type to ptr or srp, if of pointer type,
  otherwise ddoes no conversion.
  Specialization for ptr<T>.
  \param T the type of the pointee.
*/
template <typename T>
struct convert< ptr<T> > {
	//! The same ptr type.
	typedef ptr<T> to_ptr;
	//! The corresponding srp type for the ptr.
	typedef srp<T> to_srp;
};

/*!
  Converts type to ptr or srp, if of pointer type.
  otherwise does no conversion.
  Specialization for srp<T>.
  \param T the type of the pointee.
*/
template <typename T>
struct convert< srp<T> > {
	//! The corresponding srp type for the ptr.
	typedef ptr<T> to_ptr;
	//! The same srp type.
	typedef srp<T> to_srp;
};

/*!
  Fallback marking method, for simple types does no operation.
  Used in gc_mark methods of templated collectible classes.
  \param T The simple type of the field.
*/
template <typename T>
inline void gc_mark_srp(T &) {
} 

/*!
  Marks the object of srp type.
  Used in gc_mark methods of templated collectible classes.
  \param T The type of the pointee of the field.
  \param x The field to mark.
*/
template <typename T>
inline void gc_mark_srp(srp<T> &x) {
	x.gc_mark();
} 


end_package(lestes);

#endif
/* vim: set ft=lestes : */
