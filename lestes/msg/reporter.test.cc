/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Unit test.
  
  Unit test for class reporter.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/equality.hh>
#include <lestes/std/file_info.hh>
#include <lestes/std/source_location.hh>
#include <lestes/msg/message.hh>
#include <lestes/msg/report_end.hh>
#include <lestes/msg/report_cache.hh>
#include <lestes/msg/report_ostream.hh>
#include <lestes/std/ostream_wrapper.hh>
#include <lestes/msg/reporter.test.hh>
#include <lestes/msg/reporter.test.m.hh>

package(lestes);
package(msg);

using namespace ::std;

/*!
  \brief Tests reporter class.

  Performs testing of reporter class.
*/
void reporter_test(void)
{
	ptr<report_cache> rc = report_cache::create(report_end::create());
	lassert(is_equal(rc->location_get(),ptr<source_location>(NULL)));
	lassert(is_equal(rc->message_get(),ptr<message>(NULL)));

	//ptr<report_ostream> ro = report_ostream::create(rc,ostream_wrapper::create(&::std::cout,false));
	
	ptr<reporter> rep = reporter::instance();
	
	rep->filter_set(rc);
	//rep->filter_set(ro);
	
	ptr<file_info> fi = file_info::create("ab.c",NULL);
	ptr<source_location> loc = source_location::create(fi,1,1);
	
	ptr<message> m0 = message::create(test0->kind_get(),"Test 0.",message::FLG_WARNING);
	
	report << test0 << loc;
	lassert(is_equal(rc->location_get(),loc));
	lassert(is_equal(rc->message_get(),m0));

	ptr<message> m1 = message::create(test1->kind_get(),"Test 1: abcd.",message::FLG_ERROR);
	
	report << test1 << lstring("abcd") << loc;
	lassert(is_equal(rc->location_get(),loc));
	lassert(is_equal(rc->message_get(),m1));

	ptr<message> m2 = message::create(test2->kind_get(),"Test 2: 37 65.",message::FLG_ERROR);
	report << test2 << 37U << 65U << loc;
	lassert(is_equal(rc->location_get(),loc));
	lassert(is_equal(rc->message_get(),m2));
	
	ptr<message> m3 = message::create(test3->kind_get(),"Test 3: all your base are belong to 8472.",message::FLG_WARNING);
	lstring s("are belong to"), t("all your base");
	ulint u = 8472;
	report << test3 << s << u << t << loc;
	lassert(is_equal(rc->location_get(),loc));
	lassert(is_equal(rc->message_get(),m3));
}

end_package(msg);
end_package(lestes);

/*!
  \brief Main function.

  Runs the unit test in different namespace.
*/
int main(void)
{
	::lestes::msg::reporter_test();
	return 0;
}
/* vim: set ft=lestes : */
