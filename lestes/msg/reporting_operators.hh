/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__msg___reporting_operators_hh___included
#define lestes__msg___reporting_operators_hh___included

/*! \file
  \brief Helper operators for reporting.

  Definition of operators enabling easy reporting of messages.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/std/source_location.hh>
#include <lestes/msg/message_stencil.hh>
#include <lestes/msg/message.hh>
#include <lestes/msg/argument_holder.hh>
#include <lestes/msg/reporter.hh>

package(lestes);
package(std);

/*!
  Returns holder for zero arguments, initializes with stencil.
  \param ms0  The stencil to hold.
  \return  The argument holder.
*/
template <typename T>
ptr< ::lestes::msg::argument_holder00<T> > operator<<(const ptr< ::lestes::msg::reporter > &,
		const ptr< ::lestes::msg::message_stencil0<T> > &ms0)
{
	return ::lestes::msg::argument_holder00<T>::create(ms0);
}

/*!
  Reports the message formed from the holder for zero arguments.
  \param holder  The argument holder containing stencil for the message to report.
  \param loc  The location for the report.
*/
template <typename T>
void operator<<(const ptr< ::lestes::msg::argument_holder00<T> > &holder, const ptr<source_location> &loc)
{
	ptr< ::lestes::msg::reporter > r = ::lestes::msg::reporter::instance();
	ptr< ::lestes::msg::message_stencil0<T> > ms0 = holder->stencil_get();
	ptr< ::lestes::msg::message > m = ms0->format();
	r->report(m,loc);
}

/*!
  Returns holder for one argument, initializes with stencil.
  \param ms1  The stencil to hold.
  \return  The argument holder.
*/
template <typename P0>
ptr< ::lestes::msg::argument_holder10<P0> > operator<<(const ptr< ::lestes::msg::reporter > &,
		const ptr< ::lestes::msg::message_stencil1<P0> > &ms1)
{
	return ::lestes::msg::argument_holder10<P0>::create(ms1);
}

/*!
  Extends holder with zeroth argument.
  \param holder  The inferior argument holder to extend.
  \param p0  The zeroth argument to hold.
  \return  The extended argument holder.
*/
template <typename P0>
ptr< ::lestes::msg::argument_holder11<P0> > operator<<(const ptr< ::lestes::msg::argument_holder10<P0> > &holder,
		const P0 &p0)
{
	return ::lestes::msg::argument_holder11<P0>::create(holder,p0);
}

/*!
  Reports the message formed from the holder for one argument.
  \param holder  The argument holder containing stencil and argument for the message to report.
  \param loc  The location for the report.
*/
template <typename P0>
void operator<<(const ptr< ::lestes::msg::argument_holder11<P0> > &holder, const ptr<source_location> &loc)
{
	ptr< ::lestes::msg::reporter > r = ::lestes::msg::reporter::instance();
	ptr< ::lestes::msg::message_stencil1<P0> > ms1 = holder->stencil_get();
	P0 p0 = holder->p0_get();
	ptr< ::lestes::msg::message > m = ms1->format(p0);
	r->report(m,loc);
}

/*!
  Returns holder for two arguments, initializes with stencil.
  \param ms2  The stencil to hold.
  \return  The argument holder.
*/
template <typename P0, typename P1>
ptr< ::lestes::msg::argument_holder20<P0,P1> > operator<<(const ptr< ::lestes::msg::reporter > &,
		const ptr< ::lestes::msg::message_stencil2<P0,P1> > &ms2)
{
	return ::lestes::msg::argument_holder20<P0,P1>::create(ms2);
}

/*!
  Extends holder with zeroth argument.
  \param holder  The inferior argument holder to extend.
  \param p0  The zeroth argument to hold.
  \return  The extended argument holder.
*/
template <typename P0, typename P1>
ptr< ::lestes::msg::argument_holder21<P0,P1> > operator<<(const ptr< ::lestes::msg::argument_holder20<P0,P1> > &holder,
		const P0 &p0)
{
	return ::lestes::msg::argument_holder21<P0,P1>::create(holder,p0);
}

/*!
  Extends holder with first argument.
  \param holder  The inferior argument holder to extend.
  \param p1  The first argument to hold.
  \return  The extended argument holder.
*/
template <typename P0, typename P1>
ptr< ::lestes::msg::argument_holder22<P0,P1> > operator<<(const ptr< ::lestes::msg::argument_holder21<P0,P1> > &holder,
		const P1 &p1)
{
	return ::lestes::msg::argument_holder22<P0,P1>::create(holder,p1);
}

/*!
  Reports the message formed from the holder for two arguments.
  \param holder  The argument holder containing stencil and arguments for the message to report.
  \param loc  The location for the report.
*/
template <typename P0, typename P1>
void operator<<(const ptr< ::lestes::msg::argument_holder22<P0,P1> > &holder, const ptr<source_location> &loc)
{
	ptr< ::lestes::msg::reporter > r = ::lestes::msg::reporter::instance();
	ptr< ::lestes::msg::message_stencil2<P0,P1> > ms2 = holder->stencil_get();
	P0 p0 = holder->p0_get();
	P1 p1 = holder->p1_get();
	ptr< ::lestes::msg::message > m = ms2->format(p0,p1);
	r->report(m,loc);
}

/*!
  Returns holder for three arguments, initializes with stencil.
  \param ms3  The stencil to hold.
  \return  The argument holder.
*/
template <typename P0, typename P1, typename P2>
ptr< ::lestes::msg::argument_holder30<P0,P1,P2> > operator<<(const ptr< ::lestes::msg::reporter > &,
		const ptr< ::lestes::msg::message_stencil3<P0,P1,P2> > &ms3)
{
	return ::lestes::msg::argument_holder30<P0,P1,P2>::create(ms3);
}

/*!
  Extends holder with zeroth argument.
  \param holder  The inferior argument holder to extend.
  \param p0  The zeroth argument to hold.
  \return  The extended argument holder.
*/
template <typename P0, typename P1, typename P2>
ptr< ::lestes::msg::argument_holder31<P0,P1,P2> > operator<<(
		const ptr< ::lestes::msg::argument_holder30<P0,P1,P2> > &holder,
		const P0 &p0)
{
	return ::lestes::msg::argument_holder31<P0,P1,P2>::create(holder,p0);
}

/*!
  Extends holder with first argument.
  \param holder  The inferior argument holder to extend.
  \param p1  The first argument to hold.
  \return  The extended argument holder.
*/
template <typename P0, typename P1, typename P2>
ptr< ::lestes::msg::argument_holder32<P0,P1,P2> > operator<<(
		const ptr< ::lestes::msg::argument_holder31<P0,P1,P2> > &holder, 
		const P1 &p1)
{
	return ::lestes::msg::argument_holder32<P0,P1,P2>::create(holder,p1);
}

/*!
  Extends holder with second argument.
  \param holder  The inferior argument holder to extend.
  \param p2  The second argument to hold.
  \return  The extended argument holder.
*/
template <typename P0, typename P1, typename P2>
ptr< ::lestes::msg::argument_holder33<P0,P1,P2> > operator<<(
		const ptr< ::lestes::msg::argument_holder32<P0,P1,P2> > &holder,
		const P2 &p2)
{
	return ::lestes::msg::argument_holder33<P0,P1,P2>::create(holder,p2);
}

/*!
  Reports the message formed from the holder for three arguments.
  \param holder  The argument holder containing stencil and arguments for the message to report.
  \param loc  The location for the report.
*/
template <typename P0, typename P1, typename P2>
void operator<<(const ptr< ::lestes::msg::argument_holder33<P0,P1,P2> > &holder, const ptr<source_location> &loc)
{
	ptr< ::lestes::msg::reporter > r = ::lestes::msg::reporter::instance();
	ptr< ::lestes::msg::message_stencil3<P0,P1,P2> > ms3 = holder->stencil_get();
	P0 p0 = holder->p0_get();
	P1 p1 = holder->p1_get();
	P2 p2 = holder->p2_get();
	ptr< ::lestes::msg::message> m = ms3->format(p0,p1,p2);
	r->report(m,loc);
}

end_package(std);
end_package(lestes);
#endif
/* vim: set ft=lestes : */
