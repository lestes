/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/msg/logger.hh>
#include <fstream>

using namespace ::lestes;
using namespace ::lestes::msg;

declare_logger( l1 );
declare_logger( l2 );
declare_logger( l21 );
declare_logger( l22 );
declare_logger( l3 );
declare_logger( l31 );
declare_logger( l4 );
declare_logger( l41 );
declare_logger( l42 );
initialize_top_logger( l1, "l1" );
initialize_top_logger( l2, "l2" );
initialize_logger( l21, "l2-1", l2 );
initialize_logger( l22, "l2-2", l2 );
initialize_top_logger( l3, "l3" );
initialize_logger( l31, "l3-1", l3 );
initialize_top_logger( l4, "l4" );
initialize_logger( l41, "l4-1", l4 );
initialize_logger( l42, "l4-2", l4 );

int main()
{
	::std::ofstream f("logger.test.skel.xml");
	logger::dump_skeleton(f);
	f.close();

	llog(logger::root_instance()) << "never seen, but should not crash\n";

	logger::init( "logger.test.xml" );

	llog(logger::root_instance()) << "Vystup root\n";
	llog(l1) << "Vystup prvni\n";
	llog(l2) << "Vystup druhy " << 33 << '.' << "\n";
	llog(l21) << "Vystup druhy_prvni\n";
	llog(l22) << "Vystup druhy_druhy\n";
	llog(l3) << 3 << ::std::endl;
	llog_plain(l31) << "plain treti_prvni\n";
	llog(l4) << "Vystup ctvrty\n";
	llog_xml_open(l41,"tag") << "Vystup ctvrty_prvni\n";
	llog(l42) << "Vystup ctvrty_druhy\n";
	llog_xml_close(l41,"tag") << "after tag\n";
	llog_xml_open(l42,"xxx") << "ctvrty_druhy</xxx>\n";

	logger::finish();

	return 0;
}
