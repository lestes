/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__msg___report_error_flag_hh___included
#define lestes__msg___report_error_flag_hh___included

/*! \file
  \brief Report filter.

  Declaration of report_error_flag class representing report filter.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/msg/report_filter.hh>

package(lestes);
package(std);
// forward declaration to avoid cycle
class source_location;
end_package(std);
package(msg);

// forward declaration to avoid cycle
class message;

/*!
  \brief Report filter.

  Represents abstract filter of reported messages with location.
  Each inherited filter is intended to either perform an optional transformation
  on a report and call process_output(), or filter it out by not calling process_output().
  Individual filters' create methods should reserve first parameter to specifying output,
  followed by other specific parameters, to enable easy chaining of filters.
*/
class report_error_flag : public report_filter {
public:
	//! Processes a report.
	virtual void process(const ptr<message> &a_message,
			const ptr<source_location> &a_location);
	//! Returns error flag.
	bool error_get(void) const;
	//! Factory method.
	static ptr<report_error_flag> create(const ptr<report_filter> &a_output);
protected:
	//! Creates a filter with output.
	report_error_flag(const ptr<report_filter> &a_output);
private:
	//! Has an error already occured?
	bool error;
	//! Hides copy constructor.
	report_error_flag(const report_error_flag &);
	//! Hides assignment operator.
	report_error_flag &operator=(const report_error_flag &);
};

end_package(msg);
end_package(lestes);
#endif
/* vim: set ft=lestes : */
