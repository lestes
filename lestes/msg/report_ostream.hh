/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__msg___report_ostream_hh___included
#define lestes__msg___report_ostream_hh___included

/*! \file
  \brief Report stream output.

  Declaration of report_ostream class representing report output to stream.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/std/ostream_wrapper.hh>
#include <lestes/msg/report_filter.hh>

package(lestes);
package(std);
// forward declaration to avoid cycle
class source_location;
end_package(std);
package(msg);

// forward declaration to avoid cycle
class message;

/*!
  \brief Report output.

  Writes reports to a supplied stream.
*/
class report_ostream : public report_filter {
public:
	//! Writes report to stream.
	virtual void process(const ptr<message> &a_message,
			const ptr<source_location> &a_location);
	//! Returns new instance.
	static ptr<report_ostream> create(const ptr<report_filter> &a_output, const ptr<ostream_wrapper> &a_stream);
protected:
	//! Creates the object.
	report_ostream(const ptr<report_filter> &a_output, const ptr<ostream_wrapper> &a_stream);
	//! Marks the object.
	virtual void gc_mark(void);
private:
	//! Output stream.
	srp<ostream_wrapper> stream;
	//! Hides copy constructor.
	report_ostream(const report_ostream &);
	//! Hides assignment operator.
	report_ostream &operator=(const report_ostream &);
};

end_package(msg);
end_package(lestes);
#endif
/* vim: set ft=lestes : */
