/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Report kind filter.

  Definition of report_kind_filter class representing report kind filter.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/std/source_location.hh>
#include <lestes/msg/message.hh>
#include <lestes/msg/message_stencil.hh>
#include <lestes/msg/report_kind_filter.hh>

package(lestes);
package(msg);

/*! 
  Creates the report kind filter.
  \param a_output  The output of the filter.
  \param a_stencils  The stencil list.
*/
report_kind_filter::report_kind_filter(const ptr<report_filter> &a_output, 
		const ptr<stencils_type> &a_stencils):
	report_filter(checked(a_output)),
	disabled(disabled_type::create())
{
	lassert(a_stencils);
	
	for (stencils_type::iterator it = a_stencils->begin(), end = a_stencils->end();
			it != end; ++it) {
		disabled->insert((*it)->kind_get());
	}
}
	
/*!
  Discards all reports of specified kinds, other reports are passed to output.
  \pre a_message != NULL
  \pre a_location != NULL
  \param a_message  The message to process.
  \param a_location  The location to process.
*/
void report_kind_filter::process(const ptr<message> &a_message,
		const ptr<source_location> &a_location)
{
	lassert(a_message);
	lassert(a_location);
  
	// check if the kind is not disabled
	if (disabled->find(a_message->kind_get()) != disabled->end()) {
		process_output(a_message,a_location);
	}
}

/*! 
  Returns new report kind filter, initializes with output and list of stencils of disabled messages.
  \pre a_output != NULL
  \pre a_stencils != NULL
  \param a_output  The output of the filter.
  \param a_stencils  The stencils whose messages will be disabled.
  \return New report kind filter.
*/
ptr<report_kind_filter> report_kind_filter::create(const ptr<report_filter> &a_output, 
		const ptr<stencils_type> &a_stencils)
{
	return new report_kind_filter(a_output,a_stencils);
}

end_package(msg);
end_package(lestes);
/* vim: set ft=lestes : */
