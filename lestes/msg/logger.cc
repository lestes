/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/msg/logger.hh>
#include <lestes/msg/logger_util.hh>
#include <lestes/std/objectize_macros.hh>
#include <stack>
#include <iostream>
#include <fstream>

#include <libxml/xmlmemory.h>
#include <libxml/parser.h>

package(lestes);
package(msg);

lstring logger2fullname( const ptr<logger> &l );

/* initialization to itself is needed, as the initialization may actually be
 * called after an initializer in other translation unit has called
 * root_instance(), which would have changed the value of the_root_instance.
 * if that is the case, the changed value is not lost, just copied.
 * if it is not the case, null value is copied as from the zero-initialized
 * object [8.5/6]
 */
ptr<logger> logger::the_root_instance = the_root_instance;
/*
 * null_stream, unlike other static fields, has to be initialized even when
 * logger::init() is not called. the loggers must work (have non-null
 * null_ostream at hand) even when logger::init() was never called, as in that
 * case, logging is off
 */
ptr<ostream_wrapper> logger::null_ostream =
		ostream_wrapper::create( new ::std::ofstream(), true );
// does not have to be initialized here, but is
ptr<ostream_wrapper> logger::cerr_wrapper =
		ostream_wrapper::create( &::std::cerr, false );
ptr<logger::files_map_type> logger::files_map = files_map_type::create();

ptr<logger> logger::create( const lstring & a_name, const ptr<logger> & a_parent )
{
	return new logger( a_name, a_parent );
}

ptr<logger> logger::root_instance()
{
	if (!the_root_instance)
		the_root_instance = new logger();
	return the_root_instance;
}

ptr<logger> logger::parent_get() const
{
	return parent;
}
lstring logger::name_get() const
{
	return name;
}
ptr<logger::children_map_type> logger::children_get() const
{
	return children;
}

ptr < logger_formatter > logger::formatter_get() const
{
	return formatter;
}
void logger::formatter_set(ptr < logger_formatter > x)
{
	lassert(x);
	formatter = x;
}
#ifdef ALL_LOGGER_GETTERS_NEEDED
bool logger::logging_get() const
{
	return logging;
}
bool logger::logging_changed_get() const
{
	return logging_changed;
}
#endif

logger::logger()
	: logging(false), logging_changed(false), ostr(NULL),
	parent(this), children( children_map_type::create() ),
	formatter( plain_formatter::instance() )
{}

logger::logger( const lstring & a_name, const ptr<logger> a_parent )
	: logging(false), logging_changed(false), ostr(NULL), name(a_name),
	parent( checked(a_parent) ), children( children_map_type::create() ),
	formatter( plain_formatter::instance() )
{
	bool not_inserted_yet = parent->children->insert(
		*pair< lstring, srp<logger> >::create(name,this) ).second;
	lassert2( not_inserted_yet, "Trying to add a child logger with an already taken name." );
}

#define cast_xml( str )	((const xmlChar *)(str))
#define cast_c( str )	((const char *)(str))

static bool attr2bool( const xmlChar * value )
{
	if (!strcmp(cast_c(value),"on"))
		return true;
	if (!strcmp(cast_c(value),"off"))
		return false;

	::std::cerr << "Invalid on/off value '" << value << "'. Using 'off'." << ::std::endl;

	return false;
}

//! A friend of the logger class, see comments for its 'configure' method.
class logger_configurator {
	// Only allow the method to be called from logger methods.
	friend class logger;
	//! Configures children of given logger according to given xml node chain.
	static void configure( xmlNode *node, bool inherited,
		const ptr<logger> & parent_logger,
		const ptr<ostream_wrapper> & parent_stream );
};

/*!
 * This method is not part of the logger class to avoid dependency on libxml headers.
 * However, as it touches logger's private fields so it has to be in a friend
 * class. Friend method would not help, as that would require parameter types
 * to be known in the header (where the logger class is declared).
 */
void logger_configurator::configure( xmlNode *node, bool inherited,
			const ptr<logger> & parent_logger,
			const ptr<ostream_wrapper> & parent_stream )
{
	lassert( node );
	lassert( parent_stream );
	
	// "full path" to the parent logger; used in error output
	lstring path = logger2fullname(parent_logger);
	if (path != "/")
		path += "/";

	xmlChar *self_prop = NULL;
	xmlChar *children_prop = NULL;
	xmlChar *name_prop = NULL;
	xmlChar *file_prop = NULL;
	xmlChar * formatter_prop = NULL;
	xmlChar * formatter_param_prop = NULL;

	for ( ; node; node = node->next ) {

		// skip text and other irrelevant nodes
		if (node->type != XML_ELEMENT_NODE)
			continue;

		if (strcmp( cast_c(node->name), "logger" )) {
			::std::cerr << "Ignoring unrecognized element '" <<
				path << node->name << "'." << ::std::endl;
			continue;
		}

		// cleanup after previous iteration; freeing NULLs in the first one will not hurt
		xmlFree( self_prop );
		xmlFree( children_prop );
		xmlFree( name_prop );
		xmlFree( file_prop );
		xmlFree( formatter_prop );
		xmlFree( formatter_param_prop );
		name_prop = xmlGetProp( node, cast_xml("name") );
		self_prop = xmlGetProp( node, cast_xml("self") );
		children_prop = xmlGetProp( node, cast_xml("children") );
		file_prop = xmlGetProp( node, cast_xml("file") );
		formatter_prop = xmlGetProp( node, cast_xml("formatter") );
		formatter_param_prop = xmlGetProp( node, cast_xml("parameter") );

		if (!name_prop) {
			::std::cerr << "Name attribute missing in element under '" <<
				path << "'." << ::std::endl;
			continue;
		}
		lstring name_attr(cast_c(name_prop));

		bool self;
		if (!self_prop)
			self = inherited;
		else
			self = attr2bool(self_prop);

		bool children;
		if (!children_prop)
			children = inherited;
		else
			children = attr2bool(children_prop);

		if (parent_logger->children->find(name_attr)
				== parent_logger->children->end()) {
			::std::cerr << "Trying to configure non-existing logger '" <<
				path << name_attr << "'." << ::std::endl;
			continue;
		}

		ptr<ostream_wrapper> stream;
		if (file_prop && file_prop[0]) {
			lstring fn(cast_c(file_prop));
			if (logger::files_map->find(fn) == logger::files_map->end()) {
				stream = (*logger::files_map)[fn] =
					ostream_wrapper::create(
						new ::std::ofstream(fn.c_str()), true );
			} else
				stream = (*logger::files_map)[fn];
		} else if (file_prop) {
			// empty value, use cerr
			stream = logger::cerr_wrapper;
		} else {
			// attribute missing, use stream from parent
			stream = parent_stream;
		}

		const srp<logger> & this_logger = (*(parent_logger->children))[name_attr];
		if (this_logger->logging_changed) {
			::std::cerr << "Trying to configure logger '" <<
				path << name_attr <<
				"' for the second time, ignoring." << ::std::endl;
			continue;
		}
		this_logger->logging = self;
		this_logger->logging_changed = true;
		this_logger->ostr = stream;

		this_logger->formatter = formatter_factory::create_formatter(
				formatter_prop ? cast_c(formatter_prop) : "",
				formatter_param_prop ? cast_c(formatter_param_prop) : "");

		// call self recursively
		if (node->children)
			configure( node->children, children, this_logger, stream );
	}
	
	// check that all the child loggers were configured
	ptr<logger::children_map_type> ch_map = parent_logger->children;
	for (logger::children_map_type::const_iterator it = ch_map->begin();
			it != ch_map->end(); ++it )
		if (!(it->second->logging_changed))
			::std::cerr << "Logger '" << path << it->second->name <<
				"' not configured." << ::std::endl;

	xmlFree( self_prop );
	xmlFree( children_prop );
	xmlFree( name_prop );
	xmlFree( file_prop );
}

/*!
 * Tries to parse filename of given name and apply settings in it to the logger tree.
 * After returning true, should not be called again (violation is detected and not fatal).
 * When not called at all, all loggers are off.
 *
 * \return  false when file could not be read or parsed, when the root element is invalid, or when the root element did not contain the required attributes
 * \return  true otherwise; this includes some non-fatal errors which are reported to the user
 */
bool logger::init( const lstring & filename )
{
	if (root_instance()->logging_changed) {
		::std::cerr << "Trying to initialize loggers again, ignoring." << ::std::endl;
		return false;
	}

	/*
	 * this initializes the library and checks potential ABI mismatches
	 * between the version it was compiled for and the actual shared
	 * library used.
	 */
	LIBXML_TEST_VERSION

	xmlDoc *doc = NULL;
	xmlNode *root_element = NULL;
	xmlChar *self_prop = NULL;
	xmlChar *children_prop = NULL;
	xmlChar *name_prop = NULL;
	xmlChar *file_prop = NULL;

	ptr<ostream_wrapper> stream;

	bool result = false;
	bool self;
	bool children;

        /* parse the file and get the DOM */
        doc = xmlParseFile( filename.c_str() );
	if (!doc) {
		::std::cerr << "Could not parse configuration file." << ::std::endl;
		goto err_out;
	}

	root_element = xmlDocGetRootElement(doc);
	if (!root_element) {
		::std::cerr << "Root element missing." << ::std::endl;
		goto err_out;
	}
	if (strcmp( cast_c(root_element->name), "logger" )) {
		::std::cerr << "Invalid root element '" << root_element->name <<
			"'. Expected 'logger'." << ::std::endl;
		goto err_out;
	}

	self_prop = xmlGetProp( root_element, cast_xml("self") );
	children_prop = xmlGetProp( root_element, cast_xml("children") );

	if (!self_prop || !children_prop) {
		::std::cerr << "Both 'self' and 'children' attributes have"
			" to be set in the root logger element." << ::std::endl;
		goto err_out;
	}

	name_prop = xmlGetProp( root_element, cast_xml("name") );
	if (name_prop)
		::std::cerr << "Name attribute ignored in the root logger element." << ::std::endl;
	file_prop = xmlGetProp( root_element, cast_xml("file") );

	self = attr2bool(self_prop);
	children = attr2bool(children_prop);
	// empty or non-existing 'file' attribute results in cerr being used
	if (file_prop && file_prop[0]) {
		lstring fn(cast_c(file_prop));
		stream = (*files_map)[fn] = ostream_wrapper::create(
				new ::std::ofstream(fn.c_str()), true );
	} else
		stream = cerr_wrapper;

	root_instance()->logging = self;
	root_instance()->logging_changed = true;
	root_instance()->ostr = stream;

	if (root_element->children)
		logger_configurator::configure( root_element->children, children,
						root_instance(), stream );

	result = true;
err_out:
	xmlFree( self_prop );
	xmlFree( children_prop );
	xmlFree( name_prop );
	xmlFree( file_prop );
	xmlFreeDoc( doc );
	xmlCleanupParser();
	return result;
}

/*!
 * Can be called without logger::init() being called previously.
 */
void logger::finish()
{
	for ( files_map_type::iterator it = files_map->begin(); it != files_map->end(); ++it )
		it->second->release();	// this destructs the ofstream; the file is closed
}

void logger::subtree_dump( const ptr<logger> & l, ::std::ostream & os )
{
	children_map_type::const_iterator it = l->children->begin();
	children_map_type::const_iterator end_it = l->children->end();

	for ( ; it != end_it; ++it ) {
		os << "<logger name=\"" << it->second->name << '"';
		if (it->second->children->empty()) {
			os << " />" << ::std::endl;
		} else {
			os << '>' << ::std::endl;
			subtree_dump( it->second, os );
			os << "</logger>" << ::std::endl;
		}
	}
}

::std::ostream & logger::dump_skeleton( ::std::ostream & os )
{
	os <<	"<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n"
		"<logger self=\"off\" children=\"off\""
			" xmlns=\"http://lestes.jikos.cz/schemas/log-conf\">" << ::std::endl;
	
	subtree_dump( root_instance(), os );
	// the root element, unlike the other ones, is never shortened to <logger ... />
	os << "</logger>" << ::std::endl;
	return os;
}

::std::ostream & logger::operator<<( const ptr<logger_formatter> & lf )
{
	return lf->format( this, *((logging ? *ostr : *null_ostream).stream_get()) );
}

void logger::gc_mark()
{
	ostr.gc_mark();
	parent.gc_mark();
	children.gc_mark();
	formatter.gc_mark();
	object::gc_mark();
}

end_package(msg);
end_package(lestes);

