/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Report cache.

  Definition of report_cache class representing last report cache.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/std/source_location.hh>
#include <lestes/msg/message.hh>
#include <lestes/msg/report_cache.hh>

package(lestes);
package(msg);

/*! 
  Creates the report end filter.
  \post message_get() == NULL
  \post location_get() == NULL
  \param a_output  The output of the filter.
*/
report_cache::report_cache(const ptr<report_filter> &a_output):
	report_filter(a_output),
	cached_message(NULL),
	cached_location(NULL)
{
}
	
/*!
  Caches current report
  \pre a_message != NULL
  \pre a_location != NULL
  \param a_message  The message to cache.
  \param a_location  The location to cache.
*/
void report_cache::process(const ptr<message> &a_message,
		const ptr<source_location> &a_location)
{
	lassert(a_message);
	lassert(a_location);

	cached_message = a_message;
	cached_location = a_location;
	
	process_output(a_message,a_location);
}

/*!
  Returns the last message passed process().
  \return  Last message or NULL if process() was not called yet.
*/
ptr<message> report_cache::message_get(void) const
{
	return cached_message;
}

/*!
  Returns the last location passed to process().
  \return  Last location or NULL if process() was not called yet.
*/
ptr<source_location> report_cache::location_get(void) const
{
	return cached_location;
}

/*!
  Returns new object.
  \pre a_output != NULL
  \param a_output  The output of the filter.
  \return New report cache filter.
*/
ptr<report_cache> report_cache::create(const ptr<report_filter> &a_output)
{
	return new report_cache(a_output);
}

end_package(msg);
end_package(lestes);
/* vim: set ft=lestes : */
