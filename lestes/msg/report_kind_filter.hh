/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__msg___report_kind_filter_hh___included
#define lestes__msg___report_kind_filter_hh___included

/*! \file
  \brief Report kind filter.

  Declaration of report_kind_filter class representing report kind filter.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/std/vector.hh>
#include <lestes/std/set.hh>
#include <lestes/msg/report_filter.hh>

package(lestes);
package(std);
// forward declaration to avoid cycle
class source_location;
end_package(std);
package(msg);

// forward declaration to avoid cycle
class message;
class message_stencil;

/*!
  \brief Report kind filter.

  Filters out certain kinds of reports. Used to switch off specific messages.
*/
class report_kind_filter : public report_filter {
public:
	//! Type of message stencils list.
	typedef ::lestes::std::vector< srp<message_stencil> > stencils_type;
	//! Filters out reports by kind.
	virtual void process(const ptr<message> &a_message,
			const ptr<source_location> &a_location);
	//! Returns new instance.
	static ptr<report_kind_filter> create(const ptr<report_filter> &a_output, const ptr<stencils_type> &a_stencils);
protected:
	//! Creates the object.
	report_kind_filter(const ptr<report_filter> &a_output, const ptr<stencils_type> &a_stencils);
private:
	//! Type of disabled messages.
	// TODO pt change for bit vector
	typedef ::lestes::std::set<ulint> disabled_type;
	//! Disabled messages.
	srp<disabled_type> disabled;
	//! Hides copy constructor.
	report_kind_filter(const report_kind_filter &);
	//! Hides assignment operator.
	report_kind_filter &operator=(const report_kind_filter &);
};

end_package(msg);
end_package(lestes);
#endif
/* vim: set ft=lestes : */
