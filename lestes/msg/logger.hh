/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__msg___logger_hh___included
#define lestes__msg___logger_hh___included

/*! \file
 * Logger and the llog macro.
 * \author Rudo
 */

/*!
 * The llog macro calls fullname_formatter on given logger, which prints full
 * name of the logger followed by ": ". You can use the << operator in the result.
 */
#define llog( logger )	((*(logger)) << ::lestes::msg::fullname_formatter::instance())

/*!
 * The llog_plain macro calls plain_formatter on given logger. The plain_formatter does
 * nothing except returning the logger's ostream.
 */
#define llog_plain( logger )	((*(logger)) << ::lestes::msg::plain_formatter::instance())

/*!
 * The llog_xml_open macro outputs opening xml tag with given name to given
 * logger. The tag contains attribute named 'by' filled with full path of the
 * logger. xml_formatter is used to achieve this.
 *
 * Note: To avoid the 'by' attribute, simply use llog_plain.
 */
#define llog_xml_open( logger, tag )					\
	(::lestes::msg::xml_formatter::instance()->tag_name_set(tag),	\
	*(logger) << ::lestes::msg::xml_formatter::instance())
/*!
 * The llog_xml_close prints closing tag with specified name to given logger.
 * It is just a simple wrapper around llog_plain.
 */
#define llog_xml_close( logger, tag )	\
	((*(logger)) << ::lestes::msg::plain_formatter::instance() << "</" << (tag) << '>')

/*!
 * \brief declares logger with specified name, used in namespace scope
 * 
 * The declare_logger macro declares [extern] variable with given name, its type is
 * ptr<logger>; the variable is ready to be passed to llog family of macros.
 * The actual definition of the variable is handled by initialize_top_logger
 * and initialize_logger macros.
 *
 * The macro can only be used in namespace scope. It can be used in a header file.
 *
 * Note: The macro also declares a function named variable_name##_init (i.e.
 * passed name suffixed with _init) which is used to initialize the pointer.
 */
#define declare_logger( variable_name ) \
	extern ::lestes::std::ptr< ::lestes::msg::logger > variable_name; \
	const ::lestes::std::ptr< ::lestes::msg::logger > & variable_name##_init();

/*!
 * \brief declares the logger variable with correct initializer
 *
 * The initialize_logger macro declares the logger variable and initializes it.
 * Name of the logger instance and name of variable with a pointer to parent
 * logger are passed as arguments. The parent variable must be declared and
 * initialized using declare_logger and initialize_logger (or
 * initialize_top_logger) macros.
 *
 * The macro can only be used in namespace scope. It MUST NOT be used in a header file.
 *
 * Note: The macro also defines a function used to initialize the pointer.
 * Such function of the parent logger is called from it. By analogy, this
 * function is used when the logger is used as a parent in different
 * initialize_logger macro invocation.
 */
#define initialize_logger( variable_name, logger_name, parent_variable ) \
	ptr< ::lestes::msg::logger > variable_name = variable_name##_init(); \
	\
	const ptr< ::lestes::msg::logger > & variable_name##_init() \
	{ \
		if (!variable_name) \
			variable_name = ::lestes::msg::logger::create( \
					logger_name, parent_variable##_init() ); \
		return variable_name; \
	}

/*!
 * \brief similar to initialize_logger, just the logger will be a child of the root one
 */
#define initialize_top_logger( variable_name, logger_name ) \
	ptr< ::lestes::msg::logger > variable_name = variable_name##_init(); \
	\
	const ptr< ::lestes::msg::logger > & variable_name##_init() \
	{ \
		if (!variable_name) \
			variable_name = ::lestes::msg::logger::create( \
				logger_name, ::lestes::msg::logger::root_instance() ); \
		return variable_name; \
	}

#include <lestes/common.hh>
#include <lestes/std/map.hh>
#include <lestes/std/objectize_macros.hh>
#include <lestes/std/ostream_wrapper.hh>
#include <lestes/msg/logger_formatters.hh>

#include <iosfwd>

package(lestes);
package(msg);

class logger_formatter;

/*!
 * ...
 * One should only instantiate this class by hand when they know what they are doing.
 */
class logger : public ::lestes::std::object {
	friend class logger_configurator;
public:
	//! Map that maps lstrings to loggers, used to store children of a logger.
	typedef map< lstring, srp<logger> > children_map_type;

	//! Factory method, creates logger with given name. Also adds it to 'children' of given parent.
	static ptr<logger> create( const lstring & a_name, const ptr<logger> & a_parent );
	//! Returns pointer to the root "grandparent" logger.
	static ptr<logger> root_instance();
	//! Configures the logger tree by applying the settings found in given xml file.
	static bool init( const lstring & filename );
	//! Closes all files open during init().
	static void finish();
	//! Traverses the logger tree starting at root_instance(), outputs xml configuration file with corresponding elements.
	static ::std::ostream & dump_skeleton( ::std::ostream & );
	ptr<logger> parent_get() const;
	lstring name_get() const;
	ptr<children_map_type> children_get() const;
	ptr < logger_formatter > formatter_get() const;
	void formatter_set(ptr < logger_formatter > x);
#ifdef ALL_LOGGER_GETTERS_NEEDED
	bool logging_get() const;
	bool logging_changed_get() const;
#endif
	::std::ostream & operator << ( const ptr<logger_formatter> & );
private:
	//! Whether this logger is supposed to actually do something.
	bool logging;
	//! True when logging field has already been changed.
	bool logging_changed;
	//! Pointer to a stream used for the actual logging.
	srp<ostream_wrapper> ostr;

	//! Name of this logger. This does not include names of parents.
	const lstring name;
	//! Pointer to parent logger, the root logger points to self; checked.
	const srp<logger> parent;
	const srp<children_map_type> children;

	//! the default formatter
	srp < logger_formatter > formatter;

	//! Hold pointer to the root logger instance.
	static ptr<logger> the_root_instance;

	typedef map< lstring, srp<ostream_wrapper> > files_map_type;
	static ptr<files_map_type> files_map;

	//! Dumps all children (including "transitive" ones:) as xml to given ostream; used internally by dump_skeleton method.
	static void subtree_dump( const ptr<logger> &, ::std::ostream & );

	static ptr<ostream_wrapper> null_ostream;
	static ptr<ostream_wrapper> cerr_wrapper;
	//! This ctor is only used to construct the root logger.
	logger();
protected:
	//! Constructor run from the 'create' factory method.
	logger( const lstring & a_name, const ptr<logger> a_parent );
	void gc_mark();
};

end_package(msg);
end_package(lestes);

#endif	// lestes__msg___logger_hh___included
