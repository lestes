/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__msg___message_stencil_hh___included
#define lestes__msg___message_stencil_hh___included

/*! \file
  \brief Message stencils.

  Declaration of message_stencil class and message_stencilX templates representing parametrised message stencils.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/std/vector.hh>
#include <lestes/msg/message.hh>
#include <lestes/msg/formatter.hh>

package(lestes);
package(msg);

/*!
  \brief Message stencil base.
  
  Represents base class parametrised stencil for creating formatted messages.
  Each instance of stencil represents one unique message family (generating the same kind of messages).
  Each instance is assigned an unique identification number to help
  expressing sets of message kinds efficiently, for example via bit vectors.
  It is however, not assured, that one object will get the same id each time.
  
  The format text recognizes parameter reference in the form %D where D is a decimal digit.
  The parameter numbering starts with 0 and only valid indexes are accepted.
  The reference to a single parameter can appear zero or more times, in no particular order.
  To express a single %, %% is used.
  Any other character after % is not allowed and neither is end of string.

  Example: "Value %0 is invalid in %3, %0 is out of range %1 - %2 allowed for %3"
  
  Implementation details:
  The parsed format text is stored as a sequence of parameter indexes with a special value
  (equal to the number of parameters) designating use of next part of text, stored in another sequence.
*/
class message_stencil : public ::lestes::std::object {
public:
	//! Returns the number of parameters.
	ulint nparams_get(void) const;
	//! Returns the unique kind identification.
	ulint kind_get(void) const;
	//! Tests equality.
	bool equals(const ptr<message_stencil> &other) const;
protected:
	//! Type of message flags.
	typedef message::flags_type flags_type;
	//! Type of argument list.
	typedef ::lestes::std::vector<lstring> args_type;
	//! Creates the message stencil.
	message_stencil(ulint a_nparams, const lstring &a_format, flags_type a_flags);
	//! Returns a formatted message.
	ptr<message> generate(const ptr<args_type> &args) const;
	//! Marks the object.
	void gc_mark(void);
private:
	//! Type of storage for message texts.
	typedef ::lestes::std::vector<lstring> texts_type;
	//! Type of storage for parameters.
	typedef ::lestes::std::vector<ulint> params_type;
	//! Parses the format string.
	void parse(const lstring &a_format);
	//! Number of parameters.
	ulint nparams;
	//! The unique identification of message kind.
	ulint kind;
	//! Message flags.
	flags_type flags;
	//! The parsed texts.
	srp<texts_type> texts;
	//! The parsed parameters.
	srp<params_type> params;
	//! Hides copy constructor.
	message_stencil(const message_stencil &);
	//! Hides assignment operator.
	message_stencil &operator=(const message_stencil &);
	//! The internal counter to distinguish instances.
	static ulint kind_counter;
};

/*!
  \brief Message stencil.

  Represents message stencil with no parameters.
  \param T Fake parameter to enable templatization.
*/
template <typename T>
class message_stencil0 : public message_stencil {
public:
	//! Formats a message according to the stencil.
	ptr<message> format(void) const;
	//! Returns new stencil. 
	static ptr< message_stencil0<T> > create(const lstring &a_format, flags_type a_flags);
protected:
	//! Creates new stencil.
	message_stencil0(const lstring &a_format, flags_type a_flags);
	//! Marks the object.
	void gc_mark(void);
private:
	//! Fake field to enable templatization.
	//T fake_field;
	//! Fake method to enable templatization.
	void fake_method(T &);
	//! Hides copy constructor.
	message_stencil0(const message_stencil0<T> &);
	//! Hides assignment operator.
	message_stencil0<T> &operator=(const message_stencil0<T> &);
};

/*!
  Creates the stencil.
  \param a_format  The message format.
  \param a_flags  The message flags.
*/
template <typename T>
inline message_stencil0<T>::message_stencil0(const lstring &a_text, flags_type a_flags):
	message_stencil(0,a_text,a_flags)
{
}

/*!
  Formats a messge from the stencil.
  \return The formatted message.
*/
template <typename T>
ptr<message> message_stencil0<T>::format(void) const
{
	ptr<args_type> args = args_type::create();
	return generate(args);
}

/*!
  Returns the zero-parameter stencil, initializes with values to fill into the message.
  \pre  The format string is well-formed.
  \param a_format  The format for the message, with % designating indexed parameter slots.
  \param a_flags  The flags for the message.
  \return  The stencil with given 
*/
template <typename T>
ptr< message_stencil0<T> > message_stencil0<T>::create(const lstring &a_format, flags_type a_flags)
{
	return new message_stencil0<T>(a_format,a_flags);
}

/*!
  Marks the object for garbage collection.
*/
template <typename T>
void message_stencil0<T>::gc_mark(void)
{
	message_stencil::gc_mark();
}

/*!
  \brief Message stencil.

  Represents message stencil with one formatted parameter.
  \param P0  The type of the zeroth parameter.
*/
template <typename P0>
class message_stencil1 : public message_stencil {
public:
	//! The type of formatter for zeroth parameter
	typedef formatter<P0> f0_type;
	//! Formats a message according to the stencil.
	ptr<message> format(const P0 &p0) const;
	//! Returns new stencil. 
	static ptr< message_stencil1<P0> > create(const lstring &a_format, flags_type a_flags, 
			const ptr<f0_type> &a_f0);
protected:
	//! Creates new stencil. 
	message_stencil1(const lstring &a_format, flags_type a_flags,
			const ptr<f0_type> &a_f0);
	//! Marks the object.
	void gc_mark(void);
private:
	//! The formatter for the zeroth parameter.
	srp<f0_type> f0;
	//! Hides copy constructor.
	message_stencil1(const message_stencil1<P0> &);
	//! Hides assignment operator.
	message_stencil1<P0> &operator=(const message_stencil1<P0> &);
};

/*!
  Marks the object for garbage collection.
*/
template <typename P0>
void message_stencil1<P0>::gc_mark(void)
{
	f0.gc_mark();
	message_stencil::gc_mark();
}

/*!
  Creates the stencil.
  \param a_format  The message format.
  \param a_flags  The message flags.
  \param a_f0  The formatter for the zeroth parameter.
*/
template <typename P0>
message_stencil1<P0>::message_stencil1(const lstring &a_text, flags_type a_flags,
		const ptr<f0_type> &a_f0):
	message_stencil(1,a_text,a_flags),
	f0(checked(a_f0))
{
}

/*!
  Formats a messge from the stencil.
  \param p0  The zeroth parameter to format.
  \return The formatted message.
*/
template <typename P0>
ptr<message> message_stencil1<P0>::format(const P0 &p0) const
{
	ptr<args_type> args = args_type::create();
	args->push_back(f0->format(p0));
	return generate(args);
}

/*!
  Returns the one-parameter stencil, initializes with values to fill into the message.
  \pre  The format string is well-formed.
  \pre  a_f0 != NULL
  \param a_format  The format for the message, with % designating indexed parameter slots.
  \param a_flags  The flags for the message.
  \param a_f0  The formatter for the zeroth parameter.
  \return  The stencil with given values.
*/
template <typename P0>
ptr< message_stencil1<P0> > message_stencil1<P0>::create(const lstring &a_format, flags_type a_flags,
		const ptr<f0_type> &a_f0)
{
	return new message_stencil1(a_format,a_flags,a_f0);
}

/*!
  \brief Message stencil.

  Represents message stencil with two formatted parameter.
  \param P0  The type of the zeroth parameter.
  \param P1  The type of the first parameter.
*/
template < typename P0, typename P1>
class message_stencil2 : public message_stencil {
public:
	//! The type of formatter for zeroth parameter.
	typedef formatter<P0> f0_type;
	//! The type of formatter for first parameter.
	typedef formatter<P1> f1_type;
	//! Formats a message according to the stencil.
	ptr<message> format(const P0 &p0, const P1 &p1) const;
	//! Returns new stencil. 
	static ptr< message_stencil2<P0,P1> > create(const lstring &a_format, flags_type a_flags,
			const ptr<f0_type> &a_f0, const ptr<f1_type> &a_f1);
protected:
	//! Creates new stencil. 
	message_stencil2(const lstring &a_format, flags_type a_flags,
			const ptr<f0_type> &a_f0, const ptr<f1_type> &a_f1);
	//! Marks the object.
	void gc_mark(void);
private:
	//! The formatter for the zeroth parameter.
	srp<f0_type> f0;
	//! The formatter for the first parameter.
	srp<f1_type> f1;
	//! Hides copy constructor.
	message_stencil2(const message_stencil2<P0,P1> &);
	//! Hides assignment operator.
	message_stencil2<P0,P1> &operator=(const message_stencil2<P0,P1> &);
};

/*!
  Marks the object for garbage collection.
*/
template <typename P0, typename P1>
void message_stencil2<P0,P1>::gc_mark(void)
{
	f0.gc_mark();
	f1.gc_mark();
	message_stencil::gc_mark();
}

/*!
  Creates the stencil.
  \param a_format  The message format.
  \param a_flags  The message flags.
  \param a_f0  The formatter for the zeroth parameter.
  \param a_f1  The formatter for the first parameter.
*/
template <typename P0, typename P1>
message_stencil2<P0,P1>::message_stencil2(const lstring &a_text, flags_type a_flags,
		const ptr<f0_type> &a_f0, const ptr<f1_type> &a_f1):
	message_stencil(2,a_text,a_flags),
	f0(checked(a_f0)),
	f1(checked(a_f1))
{
}

/*!
  Formats a messge from the stencil.
  \param p0  The zeroth argument to format.
  \param p1  The first argument to format.
  \return The formatted message.
*/
template <typename P0, typename P1>
ptr<message> message_stencil2<P0,P1>::format(const P0 &p0, const P1 &p1) const
{
	ptr<args_type> args = args_type::create();
	args->push_back(f0->format(p0));
	args->push_back(f1->format(p1));
	return generate(args);
}

/*!
  Returns the two-parameter stencil, initializes with values to fill into the message.
  \pre  The format string is well-formed.
  \pre  a_f0 != NULL
  \pre  a_f1 != NULL
  \param a_format  The format for the message, with % designating indexed parameter slots.
  \param a_flags  The flags for the message.
  \param a_f0  The formatter for the zeroth parameter.
  \param a_f1  The formatter for the first parameter.
  \return  The stencil with given values.
*/
template <typename P0, typename P1>
ptr< message_stencil2<P0,P1> > message_stencil2<P0,P1>::create(const lstring &a_format, flags_type a_flags,
		const ptr<f0_type> &a_f0, const ptr<f1_type> &a_f1)
{
	return new message_stencil2(a_format,a_flags,a_f0,a_f1);
}

/*!
  \brief Message stencil.

  Represents message stencil with three formatted parameters.
  \param P0  The type of the zeroth parameter.
  \param P1  The type of the first parameter.
  \param P1  The type of the second parameter.
*/
template < typename P0, typename P1, typename P2>
class message_stencil3 : public message_stencil {
public:
	//! The type of formatter for zeroth parameter.
	typedef formatter<P0> f0_type;
	//! The type of formatter for first parameter.
	typedef formatter<P1> f1_type;
	//! The type of formatter for second parameter.
	typedef formatter<P2> f2_type;
	//! Formats a message according to the stencil.
	ptr<message> format(const P0 &p0, const P1 &p1, const P2 &p2) const;
	//! Returns new stencil. 
	static ptr< message_stencil3<P0,P1,P2> > create(const lstring &a_format,
		flags_type a_flags, const ptr<f0_type> &a_f0,
		const ptr<f1_type> &a_f1, const ptr<f2_type> &a_f2);
protected:
	//! Creates new stencil. 
	message_stencil3(const lstring &a_format, flags_type a_flags, const ptr<f0_type> &a_f0,
		const ptr<f1_type> &a_f1, const ptr<f2_type> &a_f2);
	//! Marks the object.
	void gc_mark(void);
private:
	//! The formatter for the zeroth parameter.
	srp<f0_type> f0;
	//! The formatter for the first parameter.
	srp<f1_type> f1;
	//! The formatter for the second parameter.
	srp<f2_type> f2;
	//! Hides copy constructor.
	message_stencil3(const message_stencil3<P0,P1,P2> &);
	//! Hides assignment operator.
	message_stencil3<P0,P1,P2> &operator=(const message_stencil3<P0,P1,P2> &);
};

/*!
  Marks the object for garbage collection.
*/
template <typename P0, typename P1, typename P2>
void message_stencil3<P0,P1,P2>::gc_mark(void)
{
	f0.gc_mark();
	f1.gc_mark();
	f2.gc_mark();
	message_stencil::gc_mark();
}

/*!
  Creates the stencil.
  \param a_format  The message format.
  \param a_flags  The message flags.
  \param a_f0  The formatter for the zeroth parameter.
  \param a_f1  The formatter for the first parameter.
  \param a_f2  The formatter for the second parameter.
*/
template <typename P0, typename P1, typename P2>
message_stencil3<P0,P1,P2>::message_stencil3(const lstring &a_text, flags_type a_flags,
		const ptr<f0_type> &a_f0, const ptr<f1_type> &a_f1, const ptr<f2_type> &a_f2):
	message_stencil(3,a_text,a_flags),
	f0(checked(a_f0)),
	f1(checked(a_f1)),
	f2(checked(a_f2))
{
}

/*!
  Formats a messge from the stencil.
  \param p0  The zeroth argument to format.
  \param p1  The first argument to format.
  \param p2  The second argument to format.
  \return The formatted message.
*/
template <typename P0, typename P1, typename P2>
ptr<message> message_stencil3<P0,P1,P2>::format(const P0 &p0, const P1 &p1, const P2 &p2) const
{
	ptr<args_type> args = args_type::create();
	args->push_back(f0->format(p0));
	args->push_back(f1->format(p1));
	args->push_back(f2->format(p2));
	return generate(args);
}

/*!
  Returns the two-parameter stencil, initializes with values to fill into the message.
  \pre  The format string is well-formed.
  \pre  a_f0 != NULL
  \pre  a_f1 != NULL
  \pre  a_f2 != NULL
  \param a_format  The format for the message, with % designating indexed parameter slots.
  \param a_flags  The flags for the message.
  \param a_f0  The formatter for the zeroth parameter.
  \param a_f1  The formatter for the first parameter.
  \param a_f2  The formatter for the second parameter.
  \return  The stencil with given values.
*/
template <typename P0, typename P1, typename P2>
ptr< message_stencil3<P0,P1,P2> > message_stencil3<P0,P1,P2>::create(const lstring &a_format, flags_type a_flags,
		const ptr<f0_type> &a_f0, const ptr<f1_type> &a_f1, const ptr<f2_type> &a_f2)
{
	return new message_stencil3(a_format,a_flags,a_f0,a_f1,a_f2);
}

end_package(msg);
end_package(lestes);
#endif
/* vim: set ft=lestes : */
