/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__msg___as_id_formatter_hh___included
#define lestes__msg___as_id_formatter_hh___included

/*! \file
 * \brief Formatter for as_id type
 *
 * \author Rudo
 */

#include <lestes/msg/formatter.hh>

package(lestes);
package(lang);
package(cplus);
package(sem);
class as_id;
end_package(sem);
end_package(cplus);
end_package(lang);
package(msg);

template<>
class formatter< ptr< ::lestes::lang::cplus::sem::as_id > > : public object {
public:
	//! Returns the instance.
	static ptr< formatter< ptr< ::lestes::lang::cplus::sem::as_id > > > instance();
	//! Formats given as_id.
	virtual lstring format(const ptr< ::lestes::lang::cplus::sem::as_id > &);
protected:
	//! Constructs the formatter.
	formatter();
private:
	//! The singleton instance.
	static ptr< formatter< ptr< ::lestes::lang::cplus::sem::as_id > > > the_instance;
	//! Hides copy constructor.
	formatter(const formatter< ptr< ::lestes::lang::cplus::sem::as_id > > &);
	//! Hides assignment operator.
	formatter< ptr< ::lestes::lang::cplus::sem::as_id > > &operator=(const formatter< ptr< ::lestes::lang::cplus::sem::as_id > > &);
};

end_package(msg);
end_package(lestes);

#endif	// lestes__msg___as_id_formatter_hh___included
