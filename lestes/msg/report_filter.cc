/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Report filter.

  Definition of report_filter class representing report filter.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/std/source_location.hh>
#include <lestes/msg/message.hh>
#include <lestes/msg/report_filter.hh>

package(lestes);
package(msg);

/*! 
  Creates a filter, initializes with no output.
  \post output_get() == NULL
*/
report_filter::report_filter(void):
	output(NULL)
{
}

/*! 
  Creates a filter, initializes with output.
  \pre a_output != NULL
  \post output_get() == a_output
  \param a_output  The output of the filter.
*/
report_filter::report_filter(const ptr<report_filter> &a_output):
	output(a_output)
{
}

/*!
  Returns output of this filter.
  \return The output to pass processed reports to.
*/
ptr<report_filter> report_filter::output_get(void) const
{
	return output;
}

/*!
  Processes a report by output.
  \pre a_message != NULL
  \pre a_location != NULL
  \pre output != NULL
  \param a_message  The message to pass to the output.
  \param a_location  The location to pass to the output.
*/
void report_filter::process_output(const ptr<message> &a_message,
		const ptr<source_location> &a_location)
{
	lassert(a_message);
	lassert(a_location);

	lassert(output);
	output->process(a_message,a_location);
}

end_package(msg);
end_package(lestes);
/* vim: set ft=lestes : */
