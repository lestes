/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__msg___report_origin_filter_hh___included
#define lestes__msg___report_origin_filter_hh___included

/*! \file
  \brief Report origin filter.

  Declaration of report_origin_filter class representing filter of report origin.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/msg/report_filter.hh>

package(lestes);
package(std);
// forward declaration to avoid cycle
class source_location;
class file_info;
end_package(std);
package(msg);

// forward declaration to avoid cycle
class message;

/*!
  \brief Report output.

  Writes reports to a supplied stream.
*/
class report_origin_filter : public report_filter {
public:
	//! Filters the origin.
	virtual void process(const ptr<message> &a_message,
			const ptr<source_location> &a_location);
	//! Returns new instance.
	static ptr<report_origin_filter> create(const ptr<report_filter> &a_output);
protected:
	//! Creates the object.
	report_origin_filter(const ptr<report_filter> &a_output);
	//! Marks the object.
	virtual void gc_mark(void);
private:
	//! Origin of the last report location's file.
	srp<source_location> last_origin;
	//! New file information with removed origin.
	srp<file_info> new_file;
	//! Hides copy constructor.
	report_origin_filter(const report_origin_filter &);
	//! Hides assignment operator.
	report_origin_filter &operator=(const report_origin_filter &);
};

end_package(msg);
end_package(lestes);
#endif
/* vim: set ft=lestes : */
