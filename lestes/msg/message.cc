/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Message wrapper.

  Definition of message class representing message.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/equality.hh>
#include <lestes/msg/message.hh>

package(lestes);
package(msg);

/*! 
  Creates a message.
  \param a_kind  The identification of the message kind.
  \param a_tex  The text of the message.
  \param a_flags  The flags for the message.
*/
message::message(ulint a_kind, const lstring &a_text, flags_type a_flags):
	kind(a_kind),
	text(a_text),
	flags(a_flags)
{
}

/*!
  Returns messsage kind identification.
  \return  The identification number of the message kind.
*/
ulint message::kind_get(void) const
{
	return kind;
}

/*!
  Returns the messsage text. 
  \return  The text of the message.
*/
lstring message::text_get(void) const
{
	return text;
}

/*!
  Returns the messsage flags. 
  \return  The flags of the message.
*/
message::flags_type message::flags_get(void) const
{
	return flags;
}

/*!
  Tests equality to other message.
  \param other  The message to compare to.
  \return true  If both messages have the same fields.
*/
bool message::equals(const ptr<message> &other) const
{
	return other &&
		is_equal(kind,other->kind_get()) &&
		is_equal(text,other->text_get()) &&
		is_equal(flags,other->flags_get());
}

/*!
  Returns new message.
  \param a_kind  The unique identification the message kind.
  \param a_text  The text of the message.
  \param a_flags  The flags for the message.
  \return The message with supplied text.
*/
ptr<message> message::create(ulint a_kind, const lstring &a_text, flags_type a_flags)
{
	return new message(a_kind,a_text,a_flags);
}

end_package(msg);
end_package(lestes);
/* vim: set ft=lestes : */
