/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__msg___logger_util_hh___included
#define lestes__msg___logger_util_hh___included

#include <lestes/common.hh>
#include <lestes/msg/logger.hh>
#include <iosfwd>

/*! \file
  \author TMA
 */

#ifdef _MSC_VER
#define NAMESPACE_SCOPE_OPERATOR 1
#endif

package(lestes);
package(msg);

class logger_end_marker {};
extern logger_end_marker eolog;
::std::ostream & operator << (::std::ostream &,const logger_end_marker &);

class binded_logger {
public:
	binded_logger(ptr < logger > l);
	~binded_logger();
	void operator << (const logger_end_marker &) const;
#if !NAMESPACE_SCOPE_OPERATOR
	template <typename T>
	inline const binded_logger & operator << (const T & x) const;
#endif
	binded_logger(const binded_logger & l);
private:
	ptr < logger > log;
#if NAMESPACE_SCOPE_OPERATOR
public:
#endif
	::std::ostream * stream;
};

#if NAMESPACE_SCOPE_OPERATOR
template <typename T>
const binded_logger & operator << (const binded_logger & bl, const T & x)
{
	*bl.stream << x;
	return bl;
}
#else
template <typename T>
const binded_logger & binded_logger::operator << (const T & x) const
{
	*stream << x;
	return *this;
}
#endif

class formatter_factory : public object {
public:
	static ptr < logger_formatter > create_formatter(lstring name, lstring parameter);
private:
	formatter_factory(const formatter_factory &);
};

end_package(msg);
package(std);

template <typename T>
inline ::lestes::msg::binded_logger operator <<(ptr < ::lestes::msg::logger > l, T x)
{
	return ::lestes::msg::binded_logger(l) << x;
}

end_package(std);
end_package(lestes);
#endif /* lestes__msg___logger_util_hh___included */
