/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Unit test.
  
  Unit test for class message.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/equality.hh>
#include <lestes/msg/message.hh>

package(lestes);
package(msg);

using namespace ::std;

/*!
  \brief Tests message class.

  Performs testing of message class.
*/
void message_test(void)
{
	ptr<message> m1 = message::create(37,"message1",message::FLG_ERROR);

	lassert(is_equal(m1,m1));
	lassert(is_equal(m1->kind_get(),37U));
	lassert(is_equal(m1->text_get(),"message1"));
	lassert(is_equal(m1->flags_get(),message::FLG_ERROR));
	
	ptr<message> m2 = message::create(37,"message2",message::FLG_ERROR);

	lassert(is_equal(m2,m2));
	lassert(!is_equal(m1,m2));
	lassert(is_equal(m2->kind_get(),37U));
	lassert(is_equal(m2->text_get(),"message2"));
	lassert(is_equal(m2->flags_get(),message::FLG_ERROR));
	
}

end_package(msg);
end_package(lestes);

/*!
  \brief Main function.

  Runs the unit test in different namespace.
*/
int main(void)
{
	::lestes::msg::message_test();
	return 0;
}
/* vim: set ft=lestes : */
