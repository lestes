/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__msg___report_end_hh___included
#define lestes__msg___report_end_hh___included

/*! \file
  \brief Report end filter.

  Declaration of report_end class representing report end filter.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/msg/report_filter.hh>

package(lestes);
package(std);
// forward declaration to avoid cycle
class source_location;
end_package(std);
package(msg);

// forward declaration to avoid cycle
class message;

/*!
  \brief Report end filter.

  Represents end filter, discarding all reports. Used at the end of filter chain.
*/
class report_end : public report_filter {
public:
	//! Discards a report.
	virtual void process(const ptr<message> &a_message,
			const ptr<source_location> &a_location);
	//! Returns new instance.
	static ptr<report_end> create(void);
protected:
	//! Creates the object.
	report_end(void);
private:
	//! Hides copy constructor.
	report_end(const report_end &);
	//! Hides assignment operator.
	report_end &operator=(const report_end &);
};

end_package(msg);
end_package(lestes);
#endif
/* vim: set ft=lestes : */
