/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__msg___logger_formatters_hh___included
#define lestes__msg___logger_formatters_hh___included

/*! \file
 * Logger formatters.
 * \author Rudo
 * \author TMA
 */

#include <lestes/common.hh>
#include <lestes/std/map.hh>
#include <lestes/std/objectize_macros.hh>
#include <lestes/std/ostream_wrapper.hh>

#include <iosfwd>

package(lestes);
package(msg);

class logger_formatter;
class logger;

class logger_formatter : public ::lestes::std::object {
public:
	virtual ::std::ostream & format( const ptr<logger> &, ::std::ostream & ) = 0;
	virtual void format_end( const ptr<logger> &, ::std::ostream & ) = 0;
};

class fullname_formatter : public logger_formatter {
public:
	static ptr<fullname_formatter> instance();
	virtual ::std::ostream & format( const ptr<logger> &, ::std::ostream & );
	virtual void format_end( const ptr<logger> &, ::std::ostream & );
protected:
	fullname_formatter();
private:
	static ptr<fullname_formatter> the_instance;
};

class plain_formatter : public logger_formatter {
public:
	static ptr<plain_formatter> instance();
	virtual ::std::ostream & format( const ptr<logger> &, ::std::ostream & );
	virtual void format_end( const ptr<logger> &, ::std::ostream & );
protected:
	plain_formatter();
private:
	static ptr<plain_formatter> the_instance;
};

class xml_formatter : public logger_formatter {
public:
	static ptr<xml_formatter> instance();
	virtual ::std::ostream & format( const ptr<logger> &, ::std::ostream & );
	virtual void format_end( const ptr<logger> &, ::std::ostream & );
	lstring tag_name_get() const;
	void tag_name_set( const lstring & );
protected:
	xml_formatter();
private:
	static ptr<xml_formatter> the_instance;
	lstring tag_name;
};

/*!
  \brief Combines sequentially two formatters.
  \author TMA
 */
class conjunct_formatter : public logger_formatter {
public:
	static ptr < conjunct_formatter > create(const ptr < logger_formatter > &, const ptr < logger_formatter > &);
	virtual ::std::ostream & format(const ptr<logger> &, ::std::ostream &);
	virtual void format_end(const ptr<logger> &, ::std::ostream &);
protected:
	conjunct_formatter(const ptr < logger_formatter > &, const ptr < logger_formatter > &);
	virtual void gc_mark();
private:
	srp < logger_formatter > first;
	srp < logger_formatter > second;
};

/*!
  \brief Prints the logger output in color
  \author TMA
 */
class color_formatter : public logger_formatter {
public:
	static ptr < color_formatter > create(lstring color);
	virtual ::std::ostream & format(const ptr<logger> &, ::std::ostream &);
	virtual void format_end(const ptr<logger> &, ::std::ostream &);
protected:
	color_formatter(lstring b);
private:
	lstring begin;
	color_formatter(const color_formatter &);
};

class shortname_formatter : public logger_formatter {
public:
	static ptr < shortname_formatter > instance();
	virtual ::std::ostream & format(const ptr<logger> &, ::std::ostream &);
	virtual void format_end(const ptr<logger> &, ::std::ostream &);
protected:
	shortname_formatter();
private:
	shortname_formatter(const color_formatter &);
	static ptr < shortname_formatter > the_instance;
};

end_package(msg);
end_package(lestes);

#endif	// lestes__msg___logger_formatters_hh___included
