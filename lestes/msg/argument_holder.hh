/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__msg___argument_holder_hh___included
#define lestes__msg___argument_holder_hh___included

/*! \file
  \brief Message argument holders.

  Definition of classes holding stencil and arguments for message.
  Enables incremental building of message by adding individual arguments.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/pointer_helpers.hh>
#include <lestes/msg/message_stencil.hh>

package(lestes);
package(msg);

#if 0
/*!
  \brief Type convertor.

  Convertor from ptr to the equivalent srp type. General case for non-ptr types.
  The result of the conversion is the original type, availiable in ptr_to_srp::result.
  \param T  The type to convert.
*/
template <typename T>
struct ptr_to_srp {
	//! The resulting type of the conversion.
	typedef T result;
};

/*!
  \brief Type convertor.

  Convertor from ptr to the equivalent srp type.  Special case for ptr types.
  The result of the conversion is srp to the pointee type, availiable in ptr_to_srp::result.
  \param T  The pointee type of the ptr to convert.
*/
template <typename T>
struct ptr_to_srp< ptr<T> >
{
	//! The resulting type of the conversion.
	typedef srp<T> result;
};

/*!
  \brief Object marker.

  Does nothing for simple types.
  \param T The simple type.
*/
template <typename T>
struct ptr_marker {
	//! Marks simple type.
	static void gc_mark(T &);
};

/*!
  Fallback method for simple types.
  Does no operation.
*/
template <typename T>
void ptr_marker<T>::gc_mark(T &) 
{
}

/*!
  \brief Object marker.

  Marks the object pointed to by the ptr.
  FIXME: Add a comment why we use srp<> and not ptr<>.
  \param T The pointee type.
*/
template <typename T>
struct ptr_marker< ptr<T> > {
	//! Marks the ptr type.
	static void gc_mark(srp<T> &p);
};

/*!
  \brief Marks the object.
  
  Marks the object pointed to.
  \param p The object to mark.
*/
template <typename T>
void ptr_marker< ptr<T> >::gc_mark(srp<T> &p)
{
	p.gc_mark();
}
#endif

/*!
  \brief Argument holder.

  Holder for zero arguments with zero arguments filled.
  \param T  Fake argument to enable templatization.
*/
template <typename T>
class argument_holder00: public object {
public:
	//! Type of the stencil.
	typedef ptr< message_stencil0<T> > stencil_type;
	//! Returns the stencil.
	stencil_type stencil_get(void) const;
	//! Returns the holder.
	static ptr< argument_holder00<T> > create(const stencil_type &a_stencil);
protected:
	//! Creates the holder.
	argument_holder00(const stencil_type &a_stencil);
	//! Marks the object.
	void gc_mark(void);
private:
	//! Stencil for the message.
	srp< message_stencil0<T> > stencil;
	//! Fake method to enable templatization.
	void fake_method(T &);
	//! Hides copy constructor.
	argument_holder00(const argument_holder00<T> &);
	//! Hides assignment operator.
	argument_holder00<T> &operator=(const argument_holder00<T> &);
};

/*!
  Creates holder for zero arguments.
  \param a_stencil  The stencil for the message.
*/
template <typename T>
argument_holder00<T>::argument_holder00(const stencil_type &a_stencil):
	stencil(checked(a_stencil))
{
}

/*!
  Returns the stencil.
  \return The stored stencil.
*/
template <typename T>
typename argument_holder00<T>::stencil_type argument_holder00<T>::stencil_get(void) const
{
	return stencil;
}

/*!
  Returns holder for no arguments.
  \pre a_stencil != NULL
  \param a_stencil  The stencil for the message.
  \return  The holder.
*/
template <typename T>
ptr< argument_holder00<T> > argument_holder00<T>::create(const stencil_type &a_stencil)
{
	return new argument_holder00<T>(a_stencil);
}

/*!
  Marks the object for garbage collection.
*/
template <typename T>
void argument_holder00<T>::gc_mark(void)
{
	stencil.gc_mark();
	object::gc_mark();
}

/*!
  \brief Argument holder.

  Holder for one argument with no arguments filled.
  \param P0  The type of the zeroth argument.
*/
template <typename P0>
class argument_holder10: public object {
public:
	//! Type of the stencil.
	typedef ptr< message_stencil1<P0> > stencil_type;
	//! Returns the stencil.
	stencil_type stencil_get(void) const;
	//! Returns the holder.
	static ptr< argument_holder10<P0> > create(const stencil_type &a_stencil);
protected:
	//! Creates the holder.
	argument_holder10(int, const stencil_type &a_stencil);
	//! Creates copy of the holder.
	argument_holder10(const ptr< argument_holder10<P0> > &other);
	//! Marks the object.
	void gc_mark(void);
private:
	//! Stencil for the message.
	srp< message_stencil1<P0> > stencil;
	//! Hides copy constructor.
	argument_holder10(const argument_holder10<P0> &);
	//! Hides assignment operator.
	argument_holder10<P0> &operator=(const argument_holder10<P0> &);
};

/*!
  Creates the holder.
  \param a_stencil  The stencil for the message.
*/
template <typename P0>
argument_holder10<P0>::argument_holder10(int, const stencil_type &a_stencil):
	stencil(checked(a_stencil))
{
}

/*!
  Creates the holder by copying other holder. 
  \param a_other  The holder to copy.
*/
template <typename P0>
argument_holder10<P0>::argument_holder10(const ptr< argument_holder10<P0> > &other):
	stencil(((void)checked(other),other->stencil_get()))
{
}

/*!
  Returns the stencil.
  \return The stored stencil.
*/
template <typename P0>
typename argument_holder10<P0>::stencil_type argument_holder10<P0>::stencil_get(void) const
{
	return stencil;
}

/*!
  Returns the holder.
  \pre a_stencil != NULL
  \param a_stencil  The stencil for the message.
  \return  The holder.
*/
template <typename P0>
ptr< argument_holder10<P0> > argument_holder10<P0>::create(const stencil_type &a_stencil)
{
	return new argument_holder10<P0>(0,a_stencil);
}

/*!
  Marks the object for garbage collection.
*/
template <typename P0>
void argument_holder10<P0>::gc_mark(void)
{
	stencil.gc_mark();
	object::gc_mark();
}

/*!
  \brief Argument holder.

  Holder for one argument with one argument filled.
  \param P0  The type of the zeroth argument.
*/
template <typename P0>
class argument_holder11: public argument_holder10<P0> {
public:
	// Type of the zeroth argument.
	typedef typename convert<P0>::to_ptr p0_type;
	//! Returns the zeroth argument.
	p0_type p0_get(void) const;
	//! Returns the holder.
	static ptr< argument_holder11<P0> > create(const ptr< argument_holder10<P0> > &parent, const p0_type &a_p0);
protected:
	//! Creates the holder.
	argument_holder11(const ptr< argument_holder10<P0> > &parent, const P0 &a_p0);
	//! Marks the object.
	void gc_mark(void);
private:
	//! The zeroth argument.
	typename convert<P0>::to_srp p0;
	//! Hides copy constructor.
	argument_holder11(const argument_holder11<P0> &);
	//! Hides assignment operator.
	argument_holder11<P0> &operator=(const argument_holder11<P0> &);
};

/*!
  Creates holder for one argument.
  \param a_parent  The parent of this holder.
  \param a_p0  The zeroth argument for the message.
*/
template <typename P0>
argument_holder11<P0>::argument_holder11(const ptr< argument_holder10<P0> > &parent, const P0 &a_p0):
	argument_holder10<P0>(checked(parent)),
	p0(a_p0)
{
}

/*!
  Returns the zeroth argument.
  \return The zeroth argument.
*/
template <typename P0>
typename argument_holder11<P0>::p0_type argument_holder11<P0>::p0_get(void) const
{
	return p0;
}

/*!
  Returns holder for one argument.
  \pre parent != NULL
  \param parent  The parent of this holder.
  \param a_p0  The zeroth argument for the message.
  \return  The holder.
*/
template <typename P0>
ptr< argument_holder11<P0> > argument_holder11<P0>::create(const ptr< argument_holder10<P0> > &parent, const p0_type &a_p0)
{
	return new argument_holder11<P0>(parent,a_p0);
}

/*!
  Marks the object for garbage collection.
*/
template <typename P0>
void argument_holder11<P0>::gc_mark(void)
{
	gc_mark_srp(p0);
	argument_holder10<P0>::gc_mark();
}

/*!
  \brief Argument holder.

  Holder for two arguments with no arguments filled.
  \param P0  The type of the zeroth argument.
  \param P1  The type of the first argument.
*/
template <typename P0, typename P1>
class argument_holder20: public object {
public:
	//! Type of the stencil.
	typedef ptr< message_stencil2<P0,P1> > stencil_type;
	//! Returns the stencil.
	stencil_type stencil_get(void) const;
	//! Returns the holder.
	static ptr< argument_holder20<P0,P1> > create(const stencil_type &a_stencil);
protected:
	//! Creates the holder.
	argument_holder20(int, const stencil_type &a_stencil);
	//! Creates copy of the holder.
	argument_holder20(const ptr< argument_holder20<P0,P1> > &other);
	//! Marks the object.
	void gc_mark(void);
private:
	//! Stencil for the message.
	srp< message_stencil2<P0,P1> > stencil;
	//! Hides copy constructor.
	argument_holder20(const argument_holder20<P0,P1> &);
	//! Hides assignment operator.
	argument_holder20<P0,P1> &operator=(const argument_holder20<P0,P1> &);
};

/*!
  Creates the holder for two arguments.
  \param a_stencil  The stencil for the message.
*/
template <typename P0, typename P1>
argument_holder20<P0,P1>::argument_holder20(int, const stencil_type &a_stencil):
	stencil(checked(a_stencil))
{
}

/*!
  Creates holder by copying other holder. 
  \param a_other  The holder to copy.
*/
template <typename P0, typename P1>
argument_holder20<P0,P1>::argument_holder20(const ptr< argument_holder20<P0,P1> > &other):
	stencil(((void)checked(other),other->stencil_get()))
{
}

/*!
  Returns the stencil.
  \return The stored stencil.
*/
template <typename P0, typename P1>
typename argument_holder20<P0,P1>::stencil_type argument_holder20<P0,P1>::stencil_get(void) const
{
	return stencil;
}

/*!
  Returns holder for two arguments.
  \pre a_stencil != NULL
  \param a_stencil  The stencil for the message.
  \return  The holder.
*/
template <typename P0, typename P1>
ptr< argument_holder20<P0,P1> > argument_holder20<P0,P1>::create(const stencil_type &a_stencil)
{
	return new argument_holder20<P0,P1>(0,a_stencil);
}

/*!
  Marks the object for garbage collection.
*/
template <typename P0, typename P1>
void argument_holder20<P0,P1>::gc_mark(void)
{
	stencil.gc_mark();
	object::gc_mark();
}

/*!
  \brief Argument holder.

  Holder for two arguments with one argument filled.
  \param P0  The type of the zeroth argument.
  \param P1  The type of the first argument.
*/
template <typename P0, typename P1>
class argument_holder21: public argument_holder20<P0,P1> {
public:
	//! Type of the zeroth argument.
	typedef typename convert<P0>::to_ptr p0_type;
	//! Returns the zeroth argument.
	p0_type p0_get(void) const;
	//! Returns the holder.
	static ptr< argument_holder21<P0,P1> > create(const ptr< argument_holder20<P0,P1> >&parent, const p0_type &a_p0);
protected:
	//! Creates the holder.
	argument_holder21(const ptr< argument_holder20<P0,P1> > &parent, const p0_type &a_p0);
	//! Creates a copy of the holder.
	argument_holder21(const ptr< argument_holder21<P0,P1> > &other);
	//! Marks the object.
	void gc_mark(void);
private:
	//! The zeroth argument.
	typename convert<P0>::to_srp p0;
	//! Hides copy constructor.
	argument_holder21(const argument_holder21<P0,P1> &);
	//! Hides assignment operator.
	argument_holder21<P0,P1> &operator=(const argument_holder21<P0,P1> &);
};

/*!
  Creates the holder for two arguments.
  \param a_parent  The parent of this holder.
  \param a_p0  The zeroth argument for the message.
*/
template <typename P0, typename P1>
argument_holder21<P0,P1>::argument_holder21(const ptr< argument_holder20<P0,P1> > &parent, const p0_type &a_p0):
	argument_holder20<P0,P1>(checked(parent)),
	p0(a_p0)
{
}

/*!
  Creates the holder by copying other holder. 
  \param a_other  The holder to copy.
*/
template <typename P0, typename P1>
argument_holder21<P0,P1>::argument_holder21(const ptr< argument_holder21<P0,P1> > &other):
	argument_holder20<P0,P1>(checked(other)),
	p0(other->p0_get())
{
}

/*!
  Returns the zeroth argument.
  \return The zeroth argument.
*/
template <typename P0, typename P1>
typename argument_holder21<P0,P1>::p0_type argument_holder21<P0,P1>::p0_get(void) const
{
	return p0;
}

/*!
  Returns the holder for two arguments.
  \pre parent != NULL
  \param parent  The parent of this holder.
  \param a_p0  The zeroth argument for the message.
  \return  The holder.
*/
template <typename P0, typename P1>
ptr< argument_holder21<P0,P1> > argument_holder21<P0,P1>::create(const ptr< argument_holder20<P0,P1> > &parent,
		const p0_type &a_p0)
{
	return new argument_holder21<P0,P1>(parent,a_p0);
}

/*!
  Marks the object for garbage collection.
*/
template <typename P0, typename P1>
void argument_holder21<P0,P1>::gc_mark(void)
{
	gc_mark_srp(p0);
	argument_holder20<P0,P1>::gc_mark();
}

/*!
  \brief Argument holder.

  Holder for two arguments with two arguments filled.
  \param P0  The type of the zeroth argument.
  \param P1  The type of the first argument.
*/
template <typename P0, typename P1>
class argument_holder22: public argument_holder21<P0,P1> {
public:
	//! Type of the first argument.
	typedef typename convert<P1>::to_ptr p1_type;
	//! Returns the first argument.
	p1_type p1_get(void) const;
	//! Returns the holder.
	static ptr< argument_holder22<P0,P1> > create(const ptr< argument_holder21<P0,P1> > &parent, const p1_type &a_p1);
protected:
	//! Creates the holder.
	argument_holder22(const ptr< argument_holder21<P0,P1> > &parent, const p1_type &a_p1);
	//! Marks the object.
	void gc_mark(void);
private:
	//! The first argument.
	typename convert<P1>::to_srp p1;
	//! Hides copy constructor.
	argument_holder22(const argument_holder22<P0,P1> &);
	//! Hides assignment operator.
	argument_holder22<P0,P1> &operator=(const argument_holder22<P0,P1> &);
};

/*!
  Creates the holder for two arguments.
  \param parent  The parent of this holder.
  \param a_p1  The first argument for the message.
*/
template <typename P0, typename P1>
argument_holder22<P0,P1>::argument_holder22(const ptr< argument_holder21<P0,P1> > &parent, const p1_type &a_p1):
	argument_holder21<P0,P1>(checked(parent)),
	p1(a_p1)
{
}

/*!
  Returns the first argument.
  \return The first argument.
*/
template <typename P0, typename P1>
typename argument_holder22<P0,P1>::p1_type argument_holder22<P0,P1>::p1_get(void) const
{
	return p1;
}

/*!
  Returns holder for two arguments.
  \pre a_parent != NULL
  \param a_parent  The parent of this holder.
  \param a_p1  The first argument for the message.
  \return  The holder.
*/
template <typename P0, typename P1>
ptr< argument_holder22<P0,P1> > argument_holder22<P0,P1>::create(const ptr< argument_holder21<P0,P1> > &parent, 
		const p1_type &a_p1)
{
	return new argument_holder22<P0,P1>(parent,a_p1);
}

/*!
  Marks the object for garbage collection.
*/
template <typename P0, typename P1>
void argument_holder22<P0,P1>::gc_mark(void)
{
	gc_mark_srp(p1);
	argument_holder21<P0,P1>::gc_mark();
}

/*!
  \brief Argument holder.

  Holder for three arguments with no arguments filled.
  \param P0  The type of the zeroth argument.
  \param P1  The type of the first argument.
  \param P2  The type of the second argument.
*/
template <typename P0, typename P1, typename P2>
class argument_holder30: public object {
public:
	//! Type of the stencil.
	typedef ptr< message_stencil3<P0,P1,P2> > stencil_type;
	//! Returns the stencil.
	stencil_type stencil_get(void) const;
	//! Returns the holder.
	static ptr< argument_holder30<P0,P1,P2> > create(const stencil_type &a_stencil);
protected:
	//! Creates the holder.
	argument_holder30(int, const stencil_type &a_stencil);
	//! Creates copy of the holder.
	argument_holder30(const ptr< argument_holder30<P0,P1,P2> > &other);
	//! Marks the object.
	void gc_mark(void);
private:
	//! Stencil for the message.
	srp< message_stencil3<P0,P1,P2> > stencil;
	//! Hides copy constructor.
	argument_holder30(const argument_holder30<P0,P1,P2> &);
	//! Hides assignment operator.
	argument_holder30<P0,P1,P2> &operator=(const argument_holder30<P0,P1,P2> &);
};

/*!
  Creates the holder for three arguments.
  \param a_stencil  The stencil for the message.
*/
template <typename P0, typename P1, typename P2>
argument_holder30<P0,P1,P2>::argument_holder30(int, const stencil_type &a_stencil):
	stencil(checked(a_stencil))
{
}

/*!
  Creates holder by copying other holder. 
  \param a_other  The holder to copy.
*/
template <typename P0, typename P1, typename P2>
argument_holder30<P0,P1,P2>::argument_holder30(const ptr< argument_holder30<P0,P1,P2> > &other):
	stencil(((void)checked(other),other->stencil_get()))
{
}

/*!
  Returns the stencil.
  \return The stored stencil.
*/
template <typename P0, typename P1, typename P2>
typename argument_holder30<P0,P1,P2>::stencil_type argument_holder30<P0,P1,P2>::stencil_get(void) const
{
	return stencil;
}

/*!
  Returns holder for three arguments.
  \pre a_stencil != NULL
  \param a_stencil  The stencil for the message.
  \return  The holder.
*/
template <typename P0, typename P1, typename P2>
ptr< argument_holder30<P0,P1,P2> > argument_holder30<P0,P1,P2>::create(const stencil_type &a_stencil)
{
	return new argument_holder30<P0,P1,P2>(0,a_stencil);
}

/*!
  Marks the object for garbage collection.
*/
template <typename P0, typename P1, typename P2>
void argument_holder30<P0,P1,P2>::gc_mark(void)
{
	stencil.gc_mark();
	object::gc_mark();
}

/*!
  \brief Argument holder.

  Holder for three arguments with one argument filled.
  \param P0  The type of the zeroth argument.
  \param P1  The type of the first argument.
  \param P2  The type of the second argument.
*/
template <typename P0, typename P1, typename P2>
class argument_holder31: public argument_holder30<P0,P1,P2> {
public:
	//! Type of the zeroth argument.
	typedef typename convert<P0>::to_ptr p0_type;
	//! Returns the zeroth argument.
	p0_type p0_get(void) const;
	//! Returns the holder.
	static ptr< argument_holder31<P0,P1,P2> > create(const ptr< argument_holder30<P0,P1,P2> >&parent, const p0_type &a_p0);
protected:
	//! Creates the holder.
	argument_holder31(const ptr< argument_holder30<P0,P1,P2> > &parent, const p0_type &a_p0);
	//! Creates a copy of the holder.
	argument_holder31(const ptr< argument_holder31<P0,P1,P2> > &other);
	//! Marks the object.
	void gc_mark(void);
private:
	//! The zeroth argument.
	typename convert<P0>::to_srp p0;
	//! Hides copy constructor.
	argument_holder31(const argument_holder31<P0,P1,P2> &);
	//! Hides assignment operator.
	argument_holder31<P0,P1,P2> &operator=(const argument_holder31<P0,P1,P2> &);
};

/*!
  Creates the holder for three arguments.
  \pre a_parent != NULL
  \param a_parent  The parent of this holder.
  \param a_p0  The zeroth argument for the message.
*/
template <typename P0, typename P1, typename P2>
argument_holder31<P0,P1,P2>::argument_holder31(const ptr< argument_holder30<P0,P1,P2> > &parent, const p0_type &a_p0):
	argument_holder30<P0,P1,P2>(checked(parent)),
	p0(a_p0)
{
}

/*!
  Creates the holder by copying other holder.
  \pre a_other != NULL
  \param a_other  The holder to copy.
*/
template <typename P0, typename P1, typename P2>
argument_holder31<P0,P1,P2>::argument_holder31(const ptr< argument_holder31<P0,P1,P2> > &other):
	argument_holder30<P0,P1,P2>(checked(other)),
	p0(other->p0_get())
{
}

/*!
  Returns the zeroth argument.
  \return The zeroth argument.
*/
template <typename P0, typename P1, typename P2>
typename argument_holder31<P0,P1,P2>::p0_type argument_holder31<P0,P1,P2>::p0_get(void) const
{
	return p0;
}

/*!
  Returns the holder for three arguments.
  \pre parent != NULL
  \param parent  The parent of this holder.
  \param a_p0  The zeroth argument for the message.
  \return  The holder.
*/
template <typename P0, typename P1, typename P2>
ptr< argument_holder31<P0,P1,P2> > argument_holder31<P0,P1,P2>::create(const ptr< argument_holder30<P0,P1,P2> > &parent,
		const p0_type &a_p0)
{
	return new argument_holder31<P0,P1,P2>(parent,a_p0);
}

/*!
  Marks the object for garbage collection.
*/
template <typename P0, typename P1, typename P2>
void argument_holder31<P0,P1,P2>::gc_mark(void)
{
	gc_mark_srp(p0);
	argument_holder30<P0,P1,P2>::gc_mark();
}

/*!
  \brief Argument holder.

  Holder for three arguments with two arguments filled.
  \param P0  The type of the zeroth argument.
  \param P1  The type of the first argument.
  \param P2  The type of the second argument.
*/
template <typename P0, typename P1, typename P2>
class argument_holder32: public argument_holder31<P0,P1,P2> {
public:
	//! The type of the first argument.
	typedef typename convert<P1>::to_ptr p1_type;
	//! Returns the first argument.
	p1_type p1_get(void) const;
	//! Returns the holder.
	static ptr< argument_holder32<P0,P1,P2> > create(const ptr< argument_holder31<P0,P1,P2> > &parent, const p1_type &a_p1);
protected:
	//! Creates the holder.
	argument_holder32(const ptr< argument_holder31<P0,P1,P2> > &parent, const p1_type &a_p1);
	//! Creates a copy of the holder.
	argument_holder32(const ptr< argument_holder32<P0,P1,P2> > &other);
	//! Marks the object.
	void gc_mark(void);
private:
	//! The first argument.
	typename convert<P1>::to_srp p1;
	//! Hides copy constructor.
	argument_holder32(const argument_holder32<P0,P1,P2> &);
	//! Hides assignment operator.
	argument_holder32<P0,P1,P2> &operator=(const argument_holder32<P0,P1,P2> &);
};

/*!
  Creates the holder for a one-argument message.
  \param parent  The parent of this holder.
  \param a_p1  The first argument for the message.
*/
template <typename P0, typename P1, typename P2>
argument_holder32<P0,P1,P2>::argument_holder32(const ptr< argument_holder31<P0,P1,P2> > &parent, const p1_type &a_p1):
	argument_holder31<P0,P1,P2>(checked(parent)),
	p1(a_p1)
{
}

/*!
  Creates the holder by copying other holder.
  \pre a_other != NULL
  \param a_other  The holder to copy.
*/
template <typename P0, typename P1, typename P2>
argument_holder32<P0,P1,P2>::argument_holder32(const ptr< argument_holder32<P0,P1,P2> > &other):
	argument_holder31<P0,P1,P2>(checked(other)),
	p1(other->p1_get())
{
}

/*!
  Returns the first argument.
  \return The first argument.
*/
template <typename P0, typename P1, typename P2>
typename argument_holder32<P0,P1,P2>::p1_type argument_holder32<P0,P1,P2>::p1_get(void) const
{
	return p1;
}

/*!
  Returns holder for two arguments.
  \pre a_parent != NULL
  \param a_parent  The parent of this holder.
  \param a_p1  The zeroth argument for the message.
  \return  The holder.
*/
template <typename P0, typename P1, typename P2>
ptr< argument_holder32<P0,P1,P2> > argument_holder32<P0,P1,P2>::create(const ptr< argument_holder31<P0,P1,P2> > &parent,
		const p1_type &a_p1)
{
	return new argument_holder32<P0,P1,P2>(parent,a_p1);
}

/*!
  Marks the object for garbage collection.
*/
template <typename P0, typename P1, typename P2>
void argument_holder32<P0,P1,P2>::gc_mark(void)
{
	gc_mark_srp(p1);
	argument_holder31<P0,P1,P2>::gc_mark();
}

/*!
  \brief Argument holder.

  Holder for three arguments with three arguments filled.
  \param P0  The type of the zeroth argument.
  \param P1  The type of the first argument.
  \param P2  The type of the second argument.
*/
template <typename P0, typename P1, typename P2>
class argument_holder33: public argument_holder32<P0,P1,P2> {
public:
	typedef typename convert<P2>::to_ptr p2_type;
	//! Returns the second argument.
	p2_type p2_get(void) const;
	//! Returns the holder.
	static ptr< argument_holder33<P0,P1,P2> > create(const ptr< argument_holder32<P0,P1,P2> > &parent, const p2_type &a_p2);
protected:
	//! Creates the holder.
	argument_holder33(const ptr< argument_holder32<P0,P1,P2> > &parent, const p2_type &a_p2);
	//! Marks the object.
	void gc_mark(void);
private:
	//! The second argument.
	typename convert<P2>::to_srp p2;
	//! Hides copy constructor.
	argument_holder33(const argument_holder33<P0,P1,P2> &);
	//! Hides assignment operator.
	argument_holder33<P0,P1,P2> &operator=(const argument_holder33<P0,P1,P2> &);
};

/*!
  Creates the holder for three argument message.
  \param parent  The parent of this holder.
  \param a_p2  The second argument for the message.
*/
template <typename P0, typename P1, typename P2>
argument_holder33<P0,P1,P2>::argument_holder33(const ptr< argument_holder32<P0,P1,P2> > &parent, const p2_type &a_p2):
	argument_holder32<P0,P1,P2>(checked(parent)),
	p2(a_p2)
{
}

/*!
  Returns the second argument.
  \return The second argument.
*/
template <typename P0, typename P1, typename P2>
typename argument_holder33<P0,P1,P2>::p2_type argument_holder33<P0,P1,P2>::p2_get(void) const
{
	return p2;
}

/*!
  Returns holder for three arguments.
  \pre a_parent != NULL
  \param a_parent  The parent of this holder.
  \param a_p2  The second argument for the message.
  \return  The holder.
*/
template <typename P0, typename P1, typename P2>
ptr< argument_holder33<P0,P1,P2> > argument_holder33<P0,P1,P2>::create(const ptr< argument_holder32<P0,P1,P2> > &parent,
		const p2_type &a_p2)
{
	return new argument_holder33<P0,P1,P2>(parent,a_p2);
}

/*!
  Marks the object for garbage collection.
*/
template <typename P0, typename P1, typename P2>
void argument_holder33<P0,P1,P2>::gc_mark(void)
{
	gc_mark_srp(p2);
	argument_holder32<P0,P1,P2>::gc_mark();
}

end_package(msg);
end_package(lestes);
#endif
/* vim: set ft=lestes : */
