/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/common.hh>
#include <lestes/msg/logger.hh>
#include <lestes/msg/logger_util.hh>

#include <fstream>

/*! \file
  \author TMA
 */

package(lestes);
package(msg);

declare_logger(my_logger);
declare_logger(his_logger);
declare_logger(her_logger);

initialize_top_logger(my_logger, "my");
initialize_logger(his_logger, "his", my_logger);
initialize_logger(her_logger, "her", my_logger);

void logger_util_test_run()
{
	::std::ofstream f("logger_util.test.skel.xml");
	logger::dump_skeleton(f);
	f.close();

	logger::init("logger_util.test.xml");

	my_logger << "aaaaaaaa\n" << eolog;
	his_logger << "cccccccc\n" << eolog;
	her_logger << "dddddddd\n" << eolog;
	my_logger << "bbbbbbbb\n" << eolog;
	her_logger << "eeeeeeee\n" << eolog;
	his_logger << "ffffffff\n" << eolog;

	llog_plain(his_logger) << "old use\n" << eolog;

	logger::finish();
}

end_package(msg);
end_package(lestes);

int main()
{
	lestes::msg::logger_util_test_run();
}

