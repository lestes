/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/msg/logger.hh>
#include <lestes/msg/logger_util.hh>
#include <lestes/std/objectize_macros.hh>
#include <stack>
#include <iostream>
#include <fstream>

#include <libxml/xmlmemory.h>
#include <libxml/parser.h>

package(lestes);
package(msg);

lstring logger2fullname( const ptr<logger> &l )
{
	lstring full_name;
	for ( ptr<logger> i = l; i != logger::root_instance(); i = i->parent_get() )
		full_name = "/" + i->name_get() + full_name;
	if (full_name.empty())
		full_name = "/";
	return full_name;
}

lstring logger2shortname( const ptr<logger> &l )
{
	lstring full_name;
	full_name = l->name_get();
	if (full_name.empty())
		full_name = "/";
	return full_name;
}

fullname_formatter::fullname_formatter() : logger_formatter()
{}

ptr<fullname_formatter> fullname_formatter::the_instance = the_instance;

ptr<fullname_formatter> fullname_formatter::instance()
{
	if (!the_instance)
		the_instance = new fullname_formatter();
	return the_instance;
}

::std::ostream & fullname_formatter::format( const ptr<logger> &l, ::std::ostream & os )
{
	return os << logger2fullname(l) << ": ";
}

void fullname_formatter::format_end( const ptr<logger> &, ::std::ostream & )
{
}

plain_formatter::plain_formatter() : logger_formatter()
{}

ptr<plain_formatter> plain_formatter::the_instance = the_instance;

ptr<plain_formatter> plain_formatter::instance()
{
	if (!the_instance)
		the_instance = new plain_formatter();
	return the_instance;
}

::std::ostream & plain_formatter::format( const ptr<logger> &, ::std::ostream & os )
{
	return os;
}

void plain_formatter::format_end( const ptr<logger> &, ::std::ostream & )
{
}

xml_formatter::xml_formatter() : logger_formatter(), tag_name("not-set-yet")
{}

ptr<xml_formatter> xml_formatter::the_instance = the_instance;

ptr<xml_formatter> xml_formatter::instance()
{
	if (!the_instance)
		the_instance = new xml_formatter();
	return the_instance;
}

::std::ostream & xml_formatter::format( const ptr<logger> &l, ::std::ostream & os )
{
	return os << '<' << tag_name << " by=\"" << logger2fullname(l) << "\">";
}

void xml_formatter::format_end( const ptr<logger> &, ::std::ostream & )
{
}

void xml_formatter::tag_name_set( const lstring & a_tag_name )
{
	tag_name = a_tag_name;
}

lstring xml_formatter::tag_name_get() const
{
	return tag_name;
}

conjunct_formatter::conjunct_formatter(const ptr < logger_formatter > & f, const ptr < logger_formatter > & s)
	: first(checked(f)), second(checked(s))
{ }

ptr < conjunct_formatter > conjunct_formatter::create(const ptr < logger_formatter > & f, const ptr < logger_formatter > & s)
{
	return new conjunct_formatter(f, s);
}

void conjunct_formatter::gc_mark()
{
	first.gc_mark();
	second.gc_mark();
	logger_formatter::gc_mark();
}

::std::ostream & conjunct_formatter::format(const ptr<logger> & l, ::std::ostream & os)
{
	return second->format(l,first->format(l, os));
}
void conjunct_formatter::format_end(const ptr<logger> & l, ::std::ostream & os)
{
	second->format_end(l,os);
	first->format_end(l,os);
}


color_formatter::color_formatter(lstring b)
	: begin(b)
{ }

::std::ostream & color_formatter::format(const ptr<logger> &, ::std::ostream & os)
{
	return os << begin;
}
void color_formatter::format_end(const ptr<logger> &, ::std::ostream & os)
{
	os << "\033[m";
}

ptr < color_formatter > color_formatter::create(lstring color)
{
	lstring b;
	if (color == "black" || color == "0")
		b = "\033[0;30m";
	else if (color == "red" || color == "1")
		b = "\033[0;31m";
	else if (color == "green" || color == "2")
		b = "\033[0;32m";
	else if (color == "yellow" || color == "3")
		b = "\033[0;33m";
	else if (color == "blue" || color == "4")
		b = "\033[0;34m";
	else if (color == "magenta" || color == "5")
		b = "\033[0;35m";
	else if (color == "cyan" || color == "6")
		b = "\033[0;36m";
	else if (color == "white" || color == "7")
		b = "\033[0;37m";
	else if (color == "bright-black" || color == "bright black" || color == "8")
		b = "\033[1;30m";
	else if (color == "bright-red" || color == "bright red" || color == "9")
		b = "\033[1;31m";
	else if (color == "bright-green" || color == "bright green" || color == "10")
		b = "\033[1;32m";
	else if (color == "bright-yellow" || color == "bright yellow" || color == "11")
		b = "\033[1;33m";
	else if (color == "bright-blue" || color == "bright blue" || color == "12")
		b = "\033[1;34m";
	else if (color == "bright-magenta" || color == "bright magenta" || color == "13")
		b = "\033[1;35m";
	else if (color == "bright-cyan" || color == "bright cyan" || color == "14")
		b = "\033[1;36m";
	else if (color == "bright-white" || color == "bright white" || color == "15")
		b = "\033[1;37m";
	else
		b = "";

	return new color_formatter(b);
}

ptr < shortname_formatter > shortname_formatter::the_instance = the_instance;

shortname_formatter::shortname_formatter()
{ }

::std::ostream & shortname_formatter::format(const ptr<logger> & log, ::std::ostream & os)
{
	return os << logger2shortname(log) << ": ";
}
void shortname_formatter::format_end(const ptr<logger> &, ::std::ostream &)
{
}

ptr < shortname_formatter > shortname_formatter::instance()
{
	if (!the_instance)
		the_instance = new shortname_formatter();
	return the_instance;
}

end_package(msg);
end_package(lestes);

