/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Report end filter.

  Definition of report_end class representing report end filter.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/std/source_location.hh>
#include <lestes/msg/message.hh>
#include <lestes/msg/report_end.hh>

package(lestes);
package(msg);

/*! 
  Creates the report end filter.
  \post output_get() == NULL
*/
report_end::report_end(void):
	report_filter()
{
}
	
/*!
  Discards any report, effectively terminating the filter chain.
  \pre a_message != NULL
  \pre a_location != NULL
  \param a_message  The message to discard.
  \param a_location  The location to discard.
*/
void report_end::process(const ptr<message> &a_message,
		const ptr<source_location> &a_location)
{
	lassert(a_message);
	lassert(a_location);
	
	// do not call process_output
}

/*!
  Returns new object.
  \return New report end filter.
*/
ptr<report_end> report_end::create(void)
{
	return new report_end();
}

end_package(msg);
end_package(lestes);
/* vim: set ft=lestes : */
