/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__msg___message_hh___included
#define lestes__msg___message_hh___included

/*! \file
  \brief Message wrapper.

  Declaration of message class representing text message.
  \author pt
*/
#include <lestes/common.hh>

package(lestes);
package(msg);

/*!
  \brief Message wrapper.

  Represents text message with message kind identification and flags.
  The message is usually returned from message stencil.
*/
class message: public ::lestes::std::object {
public:
	//! Type of message flags.
	typedef enum message_flags {
		//! General message.
		FLG_NONE = 0x0,
		//! Fatal error message.
		FLG_FATAL = 0x1,
		//! Error message.
		FLG_ERROR = 0x2,
		//! Warning message.
		FLG_WARNING = 0x4,
		//! Hint message.
		FLG_HINT = 0x8
	} flags_type;
	//! Returns stencil kind identification.
	ulint kind_get(void) const;
	//! Returns the message text.
	lstring text_get(void) const;
	//! Returns message flags.
	flags_type flags_get(void) const;
	//! Tests equality.
	bool equals(const ptr<message> &other) const;
	//! Returns new message.
	static ptr<message> create(ulint a_id, const lstring &a_text, flags_type a_flags);
private:
	//! Creates a message.
	message(ulint a_id, const lstring &a_text, flags_type a_flags);
protected:
	//! Identification of message kind.
	ulint kind;
	//! Message text.
	lstring text;
	//! Message flags.
	flags_type flags;
	//! Hides copy constructor.
	message(const message &);
	//! Hides assignment operator.
	message &operator=(const message &);
};

end_package(msg);
end_package(lestes);
#endif
/* vim: set ft=lestes : */
