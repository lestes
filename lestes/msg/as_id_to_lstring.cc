/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/msg/as_id_to_lstring.g.hh>
#include <lestes/lang/cplus/sem/as_decl.g.hh>
#include <lestes/lang/cplus/syn/token.hh>

package(lestes);
package(msg);

void as_id_to_lstring::visit_as_template_id( ptr< ::lestes::lang::cplus::sem::as_template_id > )
{
	result_set( "<template_id>" );
}

void as_id_to_lstring::visit_as_identifier( ptr< ::lestes::lang::cplus::sem::as_identifier > identifier )
{
	result_set( identifier->token_get()->value_get().to_host_string() );
}

void as_id_to_lstring::visit_as_destructor_id_token( ptr< ::lestes::lang::cplus::sem::as_destructor_id_token > )
{
	result_set( "<destructor_id>" );
}

void as_id_to_lstring::visit_as_destructor_id_template( ptr< ::lestes::lang::cplus::sem::as_destructor_id_template > )
{
	result_set( "<destructor_id_template>" );
}

void as_id_to_lstring::visit_as_constructor_id( ptr< ::lestes::lang::cplus::sem::as_constructor_id > )
{
	result_set( "<constructor_id>" );
}

void as_id_to_lstring::visit_as_op_function_id( ptr< ::lestes::lang::cplus::sem::as_op_function_id > )
{
	result_set( "<operator_function_id>" );
}

void as_id_to_lstring::visit_as_conv_function_id( ptr< ::lestes::lang::cplus::sem::as_conv_function_id > )
{
	result_set( "<conversion_function_id>" );
}

void as_id_to_lstring::visit_as_empty_id( ptr< ::lestes::lang::cplus::sem::as_empty_id > )
{
	result_set( "<empty>" );
}

void as_id_to_lstring::visit_as_global_namespace_fake_id( ptr< ::lestes::lang::cplus::sem::as_global_namespace_fake_id > )
{
	result_set( "::" );
}

end_package(msg);
end_package(lestes);
