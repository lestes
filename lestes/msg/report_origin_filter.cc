/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Report origin filter
  
  Definition of report_origin_filter class representing filter of report origin.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/equality.hh>
#include <lestes/msg/report_origin_filter.hh>
#include <lestes/std/source_location.hh>
#include <lestes/msg/message.hh>
#include <lestes/msg/formatter.hh>

package(lestes);
package(msg);

/*! 
  Creates the report origin filter.
  \param a_output  The output of the filter.
*/
report_origin_filter::report_origin_filter(const ptr<report_filter> &a_output):
	report_filter(checked(a_output)),
	last_origin(NULL),
	new_file(NULL)
{
}
	
/*!
  Clear the origin in the location if the same was reported before.
  \pre a_message != NULL
  \pre a_location != NULL
  \param a_message  The message to report.
  \param a_location  The location to filter origin from.
*/
void report_origin_filter::process(const ptr<message> &a_message,
		const ptr<source_location> &a_location)
{
	lassert(a_message);
	lassert(a_location);
  
	ptr<source_location> loc = a_location->file_get()->origin_get();

	if (is_equal(last_origin,loc)) {
		if (!new_file) {
			new_file = file_info::create(a_location->file_get()->name_get(),NULL);
		}
		
		// remove origin from the location before processing
		process_output(a_message,a_location->clone_file(new_file));

	} else {
		last_origin = loc;
		// reset the cached file information
		new_file = NULL;
		// continue processing
		process_output(a_message,a_location);
	}
}

/*!
  Marks the object.
*/
void report_origin_filter::gc_mark(void)
{
	last_origin.gc_mark();
	new_file.gc_mark();
	report_filter::gc_mark();
}

/*! 
  Creates the report origin filter, initializes with output.
  \pre a_output != NULL
  \param a_output  The output of the filter.
  \return New report origin filter.
*/
ptr<report_origin_filter> report_origin_filter::create(const ptr<report_filter> &a_output)
{
	return new report_origin_filter(a_output);
}

end_package(msg);
end_package(lestes);
/* vim: set ft=lestes : */
