/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__msg___reporting_support_hh___included
#define lestes__msg___reporting_support_hh___included

/*! \file
  \brief Message reporting support.

  Contains necessary includes for error message integration.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/msg/message.hh>
#include <lestes/msg/message_stencil.hh>
#include <lestes/msg/reporter.hh>
#include <lestes/msg/reporting_operators.hh>

// TODO pt remove ! Shortcut macro for reporter singleton.
//#define report ::lestes::msg::reporter::instance()

package(lestes);
package(msg);

end_package(msg);
end_package(lestes);

#endif
/* vim: set ft=lestes : */
