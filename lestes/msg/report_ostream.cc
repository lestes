/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Report stream output. 

  Definition of report_ostream class representing report output to stream.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/std/source_location.hh>
#include <lestes/msg/message.hh>
#include <lestes/msg/report_ostream.hh>
#include <lestes/msg/formatter.hh>

package(lestes);
package(msg);

/*! 
  Creates the report stream output.
  \param a_output  The output of the filter.
  \param a_stream  The stream to write to.
*/
report_ostream::report_ostream(const ptr<report_filter> &a_output, 
		const ptr<ostream_wrapper> &a_stream):
	report_filter(checked(a_output)),
	stream(checked(a_stream))
{
}
	
/*!
  Writes the report to a stream.
  \pre a_message != NULL
  \pre a_location != NULL
  \param a_message  The message to write.
  \param a_location  The location to write.
*/
void report_ostream::process(const ptr<message> &a_message,
		const ptr<source_location> &a_location)
{
	lassert(a_message);
	lassert(a_location);
  
	ostream_wrapper::stream_type o(stream->stream_get());

	ptr<source_location> parent = a_location->file_get()->origin_get();
	if (parent) {
		ptr<file_info> file;
		bool first = true;
		
		while (parent) {
			file = parent->file_get();
			// TODO make this via symbols, so that it needs not be localized
			// not to mention that the current text is too long
			(*o) << 
				(first ?
					"In file included from " : 
				",\n                 from " ) << 
					file->name_get() << 
				':' << parent->line_get();
			parent = file->origin_get();
			first = false;
		}

		(*o) << ":\n";
	}

	(*o) << a_location->file_get()->name_get() << ':' << 
			  a_location->line_get() << ':' << 
			  a_location->column_get() << 
			  (a_message->flags_get() == message::FLG_WARNING ? ": warning: " : ": ") <<
			  a_message->text_get() << '\n';
	
	// continue processing
	process_output(a_message,a_location);
}

/*!
  Marks the object.
*/
void report_ostream::gc_mark(void)
{
	stream.gc_mark();
	report_filter::gc_mark();
}
/*! 
  Creates the report stream output, initializes with output and stream.
  \pre a_output != NULL
  \pre a_stream != NULL
  \param a_output  The output of the filter.
  \param a_stream  The stream to write to.
  \return New report stream output filter.
*/
ptr<report_ostream> report_ostream::create(const ptr<report_filter> &a_output, 
		const ptr<ostream_wrapper> &a_stream)
{
	return new report_ostream(a_output,a_stream);
}

end_package(msg);
end_package(lestes);
/* vim: set ft=lestes : */
