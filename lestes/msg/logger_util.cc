/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/common.hh>
#include <lestes/msg/logger.hh>
#include <lestes/msg/logger_util.hh>

/*! \file
  \author TMA
 */

#include <iostream>

package(lestes);
package(msg);

logger_end_marker eolog;

::std::ostream & operator << (::std::ostream & os,const logger_end_marker &)
{
	return os;
}

binded_logger::binded_logger(ptr < logger > l)
	: log(l), stream(&(*l << l->formatter_get()))
{ }

binded_logger::binded_logger(const binded_logger & l)
	: log(l.log), stream(l.stream)
{ }

binded_logger::~binded_logger()
{
	//*this << eolog;
}

void binded_logger::operator << (const logger_end_marker &) const
{
	log->formatter_get()->format_end(log, *stream);
}

ptr < logger_formatter > formatter_factory::create_formatter(lstring name, lstring parameter)
{
	if (name == "color" || name == "colorific")
		return color_formatter::create(parameter);
	else if (name == "simple" || name == "plain" || name == "")
		return plain_formatter::instance();
	else if (name == "shortname")
		return ::lestes::msg::shortname_formatter::instance();
	else if (name == "fullname")
		return ::lestes::msg::fullname_formatter::instance();
	else if (name[0] == '(') {
		bool is_parameter_list = parameter[0] == '(';
		lstring::size_type sz = 1, psz = is_parameter_list;
		lstring::size_type nsz, npsz = parameter.size();
		ptr < logger_formatter > lf = NULL;
		for ( ; (nsz = name.find_first_of(" )", sz)) != lstring::npos ; sz = nsz+1) {
			lstring formater = name.substr(sz,nsz - sz);
			lstring param;
			ptr < logger_formatter > nlf;
			if (is_parameter_list) {
				npsz = parameter.find_first_of(" )",psz);
				if (npsz != lstring::npos) {
					param = parameter.substr(psz,npsz-psz);
					psz = npsz + 1;
				} else
					param = "";
			} else
				param = parameter;

		       	nlf = create_formatter(formater, param);

			if (lf)
				lf = conjunct_formatter::create(lf, nlf);
			else
				lf = nlf;
		}
		return lf;
	}
	/*else if (name == "xml")
		return xml_formatter(parameter); */
	::std::cerr << "unknown formatter (" << name << ")" << ::std::endl;
	return plain_formatter::instance();
}


end_package(msg);
end_package(lestes);
