/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Message reporter.

  Definition of reporter class representing message reporter.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/std/source_location.hh>
#include <lestes/std/ostream_wrapper.hh>
#include <lestes/msg/message.hh>
#include <lestes/msg/reporter.hh>
#include <lestes/msg/report_end.hh>
#include <iostream>

package(lestes);
package(msg);

/*!
  Creates the only reporter.
*/
reporter::reporter(void):
	filter(report_end::create())
{
}

/*!
  Reports message at location. Calls process of the active filter.
  \pre a_message != NULL
  \pre a_location != NULL
  \pre filter != NULL
  \param a_message  The message to report.
  \param a_location  The associated location.
*/
void reporter::report(const ptr<message> &a_message,
			const ptr<source_location> &a_location)
{
	lassert(a_message);
	lassert(a_location);
	
	filter->process(a_message,a_location);
}

/*! 
  Sets report filter to use for processing reports.
  \pre a_filter != NULL
  \param a_filter  The filter to set.
*/
void reporter::filter_set(const ptr<report_filter> &a_filter)
{
	lassert(a_filter);
	filter = a_filter;
}

/*! 
  Returns active filter.
  \return  The filter processing the reports.
*/
ptr<report_filter> reporter::filter_get(void) const
{
	return filter;
}

/*!
  Returns the only instance of the object.
  \return  The singleton reporter object.
*/
ptr<reporter> reporter::instance(void)
{
	if (!singleton) {
		singleton = new reporter();
	}
	return singleton;
}

/*!
  The only instance of the class. Uses lazy instantiation.
*/
ptr<reporter> reporter::singleton;

end_package(msg);

ptr< ::lestes::msg::reporter > report(::lestes::msg::reporter::instance());

end_package(lestes);

/* vim: set ft=lestes : */

