/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Message stencils.

  Definition of message_stencil class representing parametrised message stencil.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/msg/message_stencil.hh>

package(lestes);
package(msg);

/*!
  Creates the stencil, initializes with number of expected parameters and values to fill into the message.
  Assigns a unique id number, starting from zero.
  \pre  The format string is well-formed.
  \param a_nparams  Number of parameters for the stencil.
  \param a_format  The format for the message, with % designating indexed parameter slots.
  \param a_flags  The flags for the message.
*/
message_stencil::message_stencil(ulint a_nparams, const lstring &a_format, flags_type a_flags):
	nparams(a_nparams),
	kind(kind_counter++),
	flags(a_flags),
	texts(texts_type::create()),
	params(params_type::create())
{
	// attempt to fill internal structures for faster message creation
	parse(a_format);
}

/*!
  Returns the number of parameters for the message.
  \return  The number of parameters.
*/
ulint message_stencil::nparams_get(void) const
{
	return nparams;
}

/*!
  Returns the assigned unique identification number.
  The number is only for internal use and may vary between compilations.
  \return The kind identification number.
*/
ulint message_stencil::kind_get(void) const
{
	return kind;
}

/*!
  Tests equality to other stencil. Each stencil object represents one message
  kind and is thus unique.
  \param other  The stencil to compare to.
  \return true  If both stencils point to the same object.
*/
bool message_stencil::equals(const ptr<message_stencil> &other) const
{
	return other && other == this;
}

/*!
  Parses the format text.
  \pre The text is well-formed according to the class documentation.
  \param text  The text to parse.
*/
void message_stencil::parse(const lstring &a_format)
{
	bool percent = false;
	// start of not yet stored part
	ulint start = 0;
	lstring out;
	
	for (ulint i = 0, len = a_format.length(); i < len; i++) {
		char c = a_format[i];
		if (percent) {
			// add the part of a_format before the % sequence
			out += a_format.substr(start,i - start - 1);
			if (c == '%') {
				out += '%';
			} else {
				ucn u = character::create_from_host(c);
			
				lassert2(character::is_digit(u),"Invalid `%' sequence.");
				ulint x = character::extract_digit(u);
				lassert2(x < nparams,"Invalid parameter in `%' sequence.");
				
				if (out.length()) {
					// flush the a_format before the parameter
					texts->push_back(out);
					out = "";
					// signal a_format output
					params->push_back(nparams);
				}
				params->push_back(x);
			}
			start = i + 1;
			percent = false;
		} else {
			percent = (c == '%');
		}
	}

	lassert2(!percent,"Unterminated `%' sequence.");

	out += a_format.substr(start);
	if (out.length()) {
		// flush the last a_format
		texts->push_back(out);
		// signal a_format output
		params->push_back(nparams);
	}
}

/*!
  Returns message with arguments filled into the parameter slots.
  \pre args != NULL
  \pre args->size() == nparams_get()
  \param args  The arguments for the message.
*/
ptr<message> message_stencil::generate(const ptr<args_type> &args) const
{
	lassert(args);
	lassert(args->size() == nparams);
	
	lstring result;
	
	texts_type::iterator tit = texts->begin();
	texts_type::iterator tend = texts->end();
	
	for (params_type::iterator it = params->begin(), end = params->end(); it != end; ++it) {
		ulint idx = *it;
		if (idx == nparams) {
			lassert(tit != tend);
			result += *tit;
			++tit;
		} else {
			lassert(idx < nparams);
			// operator[] does no range check
			result += args->operator[](idx);
		}
	}

	// pass all stored info
	return message::create(kind,result,flags);
}

/*!
  Marks the object for garbage collection.
*/
void message_stencil::gc_mark(void)
{
	texts.gc_mark();
	params.gc_mark();
	object::gc_mark();
}

/*!
  Internal message stencil instance identification number counter.
*/
ulint message_stencil::kind_counter = 0;

end_package(msg);
end_package(lestes);
/* vim: set ft=lestes : */

