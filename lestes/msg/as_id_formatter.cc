/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#include <lestes/msg/as_id_formatter.hh>
#include <lestes/msg/as_id_to_lstring.g.hh>

package(lestes);
package(msg);

using ::lestes::lang::cplus::sem::as_id;

ptr< formatter< ptr<as_id> > > formatter< ptr<as_id> >::the_instance = the_instance;

ptr< formatter< ptr<as_id> > > formatter< ptr<as_id> >::instance()
{
	if (!the_instance)
		the_instance = new formatter< ptr<as_id> >();
	return the_instance;
}

lstring formatter< ptr<as_id> >::format( const ptr<as_id> & id )
{
	return as_id_to_lstring::instance()->process( id );
}

formatter< ptr<as_id> >::formatter() : object()
{}

end_package(lestes);
end_package(msg);
