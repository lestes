/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
/*! \file
  \brief Unit test.
  
  Unit test for class message_stencil.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/equality.hh>
#include <lestes/msg/message_stencil.hh>

#include <iostream>

package(lestes);
package(msg);

using namespace ::std;

/*!
  \brief Tests message_stencil class.

  Performs testing of message_stencil class.
*/
void message_stencil_test(void)
{
	ptr< message_stencil0<bool> > ms0 = message_stencil0<bool>::create("%%message_stencil0%%",message::FLG_ERROR);
	lassert(is_equal(ms0,ms0));
	lassert(is_equal(ms0->nparams_get(),0U));
	
	ptr<message> m0 = ms0->format();
	lassert(is_equal(m0->kind_get(),ms0->kind_get()));
	//::std::cerr << "`" << m0->text_get() << "'" << ::std::endl;
	lassert(is_equal(m0->text_get(),"%message_stencil0%"));
	lassert(is_equal(m0->flags_get(),message::FLG_ERROR));
	
	ptr< message_stencil1<ulint> > ms1 = 
		message_stencil1<ulint>::create("%0 message_stencil1 %0",message::FLG_ERROR,formatter<ulint>::instance());
	lassert(!is_equal(ms1,ms0));
	lassert(is_equal(ms1,ms1));
	lassert(is_equal(ms1->nparams_get(),1U));

	ptr<message> m1 = ms1->format(9);
	lassert(is_equal(m1->kind_get(),ms1->kind_get()));
	lassert(is_equal(m1->text_get(),"9 message_stencil1 9"));
	lassert(is_equal(m1->flags_get(),message::FLG_ERROR));
	
	ptr< message_stencil2<ulint,ulint> > ms2 = 
		message_stencil2<ulint,ulint>::create("%0%1 message_stencil2 %1%0",message::FLG_ERROR,
				formatter<ulint>::instance(),formatter<ulint>::instance());
	lassert(!is_equal(ms2,ms0));
	lassert(!is_equal(ms2,ms1));
	lassert(is_equal(ms2,ms2));
	lassert(is_equal(ms2->nparams_get(),2U));
	
	ptr<message> m2 = ms2->format(6,7);
	lassert(is_equal(m2->kind_get(),ms2->kind_get()));
	lassert(is_equal(m2->text_get(),"67 message_stencil2 76"));
	lassert(is_equal(m2->flags_get(),message::FLG_ERROR));
	
	ptr< message_stencil3<ulint,ulint,lstring> > ms3 = 
		message_stencil3<ulint,ulint,lstring>::create("%0%1%2 message_stencil3 %2%1%0",message::FLG_ERROR,
				formatter<ulint>::instance(),formatter<ulint>::instance(),formatter<lstring>::instance());
	lassert(!is_equal(ms3,ms0));
	lassert(!is_equal(ms3,ms1));
	lassert(!is_equal(ms3,ms2));
	lassert(is_equal(ms3,ms3));
	lassert(is_equal(ms3->nparams_get(),3U));
	
	ptr<message> m3 = ms3->format(1,2,"xyz");
	lassert(is_equal(m3->kind_get(),ms3->kind_get()));
	lassert(is_equal(m3->text_get(),"12xyz message_stencil3 xyz21"));
	lassert(is_equal(m3->flags_get(),message::FLG_ERROR));
	
}

end_package(msg);
end_package(lestes);

/*!
  \brief Main function.

  Runs the unit test in different namespace.
*/
int main(void)
{
	::lestes::msg::message_stencil_test();
	return 0;
}
/* vim: set ft=lestes : */

