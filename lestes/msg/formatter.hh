/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__msg___formatter_hh___included
#define lestes__msg___formatter_hh___included

/*! \file
  \brief Formatter template.

  Declaration of formatter template representing object formatter.
  \author pt
*/
#include <lestes/common.hh>
#include <sstream>
#include <ostream>

package(lestes);
package(msg);

/*!
  \brief Object formatter.

  Represents formatter template performing conversion of objects to lstring representation.
  The default formatting is done through ostream and is thus suitable for basic types.
  \param T  The type of object to format.
*/
template <typename T>
class formatter : public object {
public:
	//! Formats object of type T.
	virtual lstring format(const T &x);
	//! Returns the instance.
	static ptr< formatter<T> > instance(void);
protected:
	//! Creates the object.
	formatter(void);
private:
	//! The singleton instance.
	static srp< formatter<T> > singleton;
	//! Hides copy constructor.
	formatter(const formatter<T> &);
	//! Hides assignment operator.
	formatter<T> &operator=(const formatter<T> &);
};

/*!
  The singleton instance of the formatter.
*/
template <typename T>
srp< formatter<T> > formatter<T>::singleton;

/*!
  Creates the formatter.
*/
template <typename T>
formatter<T>::formatter()
{
}

/*!
  Returns the instance of the formatter.
  \return  The singleton instance.
*/
template <typename T>
ptr< formatter<T> > formatter<T>::instance(void)
{
	if (!singleton) {
		singleton = new formatter<T>();
	}
	return singleton;
}

/*!
  Converts argument into readable form.
  \param x  The object to format.
  \return  The text representation of the argument.
*/
template <typename T>
lstring formatter<T>::format(const T &x)
{
	::std::stringbuf sb;
	::std::ostream os(&sb);
	os << x;
	return sb.str();
}

#if 0
/*!
  \brief Formatter for ulint.
  
  Specialization of formatter for ulint.
*/
template <>
class formatter<ulint>
{
public:
	//! Formats ulint.
	static lstring format(ulint x);
private:
	//! Hides copy constructor.
	formatter(const formatter<ulint> &);
	//! Hides assignment operator.
	formatter<ulint> &operator=(const formatter<ulint> &);
};

/*!
  Converts argument into readable form.
  \param x  The object to format.
  \return  The text representation of the argument.
*/
lstring formatter<ulint>::format(ulint x)
{
	::std::stringbuf sb;
	::std::ostream os(&sb);
	os << x;
	return sb.str();
}
#endif

end_package(msg);
end_package(lestes);

#include <lestes/msg/as_id_formatter.hh>

#endif
/* vim: set ft=lestes : */
