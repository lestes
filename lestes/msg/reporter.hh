/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__msg___reporter_hh___included
#define lestes__msg___reporter_hh___included

/*! \file
  \brief Message reporter.

  Declaration of reporter class representing message reporter.
  \author pt
*/
#include <lestes/common.hh>

package(lestes);

package(std);
// forward declaration to avoid cycle
class source_location;
end_package(std);

package(msg);

// forward declaration to avoid cycle
class message;
class report_filter;

/*!
  \brief Message reporter.

  Represents singleton reporter of messages with location.
  The behaviour is altered by setting the filter to process the messages.
*/
class reporter: public ::lestes::std::object {
public:
	//! Reports message at location.
	void report(const ptr<message> &a_message,
			const ptr<source_location> &a_location);
	//! Sets active filter.
	void filter_set(const ptr<report_filter> &a_filter);
	//! Returns active filter.
	ptr<report_filter> filter_get(void) const;
	//! Returns the reporter.
	static ptr<reporter> instance(void);
private:
	//! Creates the reporter.
	reporter(void);
protected:
	//! Filter used for reporting.
	srp<report_filter> filter;
	//! Hides copy constructor.
	reporter(const reporter &);
	//! Hides assignment operator.
	reporter &operator=(const reporter &);
	//! The only instance.
	static ptr<reporter> singleton;
};

end_package(msg);

// TODO
extern ptr< ::lestes::msg::reporter > report;

end_package(lestes);
#endif
/* vim: set ft=lestes : */
