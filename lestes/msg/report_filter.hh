/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__msg___report_filter_hh___included
#define lestes__msg___report_filter_hh___included

/*! \file
  \brief Report filter.

  Declaration of report_filter class representing report filter.
  \author pt
*/
#include <lestes/common.hh>

package(lestes);
package(std);
// forward declaration to avoid cycle
class source_location;
end_package(std);
package(msg);

// forward declaration to avoid cycle
class message;

/*!
  \brief Report filter.

  Represents abstract filter of reported messages with location.
  Each inherited filter is intended to either perform an optional transformation
  on a report and call process_output(), or filter it out by not calling process_output().
  Individual filters' create methods should reserve first parameter to specifying output,
  followed by other specific parameters, to enable easy chaining of filters.
*/
class report_filter : public ::lestes::std::object {
public:
	//! Processes a report.
	virtual void process(const ptr<message> &a_message,
			const ptr<source_location> &a_location) abstract;
	//! Returns output.
	ptr<report_filter> output_get(void) const;
protected:
	//! Creates a filter with no output.
	report_filter(void);
	//! Creates a filter with output.
	report_filter(const ptr<report_filter> &a_output);
	//! Processes a report by output.
	void process_output(const ptr<message> &a_message,
			const ptr<source_location> &a_location);
private:
	//! The output to pass reports to.
	srp<report_filter> output;
	//! Hides copy constructor.
	report_filter(const report_filter &);
	//! Hides assignment operator.
	report_filter &operator=(const report_filter &);
};

end_package(msg);
end_package(lestes);
#endif
/* vim: set ft=lestes : */
