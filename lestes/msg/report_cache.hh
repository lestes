/*
   The lestes compiler suite
   Copyright (C) 2002, 2003, 2004, 2005 Miroslav Tichy
   Copyright (C) 2002, 2003, 2004, 2005 Petr Zika
   Copyright (C) 2002, 2003, 2004, 2005 Vojtech Hala
   Copyright (C) 2002, 2003, 2004, 2005 Jiri Kosina
   Copyright (C) 2002, 2003, 2004, 2005 Pavel Sanda
   Copyright (C) 2002, 2003, 2004, 2005 Jan Zouhar
   Copyright (C) 2002, 2003, 2004, 2005 Rudolf Thomas

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   See the full text of the GNU General Public License version 2, and
   the limitations in the file doc/LICENSE.

   By accepting the license the licensee waives any and all claims
   against the copyright holder(s) related in whole or in part to the
   work, its use, and/or the inability to use it.
 
 */
#ifndef lestes__msg___report_cache_hh___included
#define lestes__msg___report_cache_hh___included

/*! \file
  \brief Report cache.

  Declaration of report_cache class representing last report cache.
  \author pt
*/
#include <lestes/common.hh>
#include <lestes/msg/report_filter.hh>

package(lestes);
package(std);
// forward declaration to avoid cycle
class source_location;
end_package(std);
package(msg);

// forward declaration to avoid cycle
class message;

/*!
  \brief Report cache.

  Represents cache, saving last report for examination. Used for testing.
*/
class report_cache : public report_filter {
public:
	//! Caches a report.
	virtual void process(const ptr<message> &a_message,
			const ptr<source_location> &a_location);
	//! Returns cached message.
	ptr<message> message_get(void) const;
	//! Returns cached location.
	ptr<source_location> location_get(void) const;
	//! Returns new instance.
	static ptr<report_cache> create(const ptr<report_filter> &a_output);
protected:
	//! Creates the object.
	report_cache(const ptr<report_filter> &a_output);
private:
	//! Last cached message.
	srp<message> cached_message;
	//! Last cached location.
	srp<source_location> cached_location;
	//! Hides copy constructor.
	report_cache(const report_cache &);
	//! Hides assignment operator.
	report_cache &operator=(const report_cache &);
};

end_package(msg);
end_package(lestes);
#endif
/* vim: set ft=lestes : */
