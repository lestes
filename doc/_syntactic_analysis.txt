Syntactic analysis
==================

DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT 

The syntactic analysis in lestes project comprises of a disambiguation manager
("manager"), a stack of bison-generated LALR(1) parsers ("parsers") run by
the manager, and a hinter which provides parsers with type information.

As analysis of some parts of the source must be delayed, the analyzer is
designed to be re-entrant. A new manager with its parsers and a hinter can be
spawned at any time. Method bodies defined inside classes are good example of
a constructs that cannot be analysed immediately (see [3.4.1/8]).

Disambiguation manager
----------------------

The main purpose of the manager is to supervise the process of disambiguation.
The disambiguation itself is controlled by the parsers - they call the manager
because they "know" where, and what kind of disambiguation needs to be run.
The manager is responsible for actually starting a new parser whenever a
disambiguation is needed.

The manager also does "packing", a method for delaying analysis of some chunks
from the source. Again, packing itself is controlled from the parsers yet
performed by the manager. For example, when the parser expects function body
to follow, it makes the manager pack it and reads it as one token which
contains the original token sequence inside. The body is extracted verbatim
when its analysis can be conducted -- usually when the tables are filled so
that the hinter can correctly supply type information.

Disambiguation
--------------

Some parts of the C++ language are ambiguous, which means that the same part
of a source (sequence of tokens) can have more than one interpretation. The
norm states which of the possibilities is to be considered. Sometimes, the
norm explicitly says that one of the possibilities takes precedence (e.g.
declaration-statement before expression-statement [6.8/1]). In other cases,
these precedences can be worked out.

Syntactic analysis in lestes builds on the assumption that all disambiguation
rules are formulated using precedences.

The norm mandates that the disambiguation precedes parsing [6.8/3]. That
implies the analyzer has to break out from parsing and process incoming token
sequence sideways to find out the possible interpretations. If there is more
than one of them, the analyzer has to choose the right one according to the
norm.

Whenever parser reaches a state requiring disambiguation, it is already known
which two (or more) ambiguous constructs are allowed to follow
(declaration-statement and expression-statement from the example above). For
each such construct a new "one-purpose" parser is started at the very position
in the source token stream. Each construct has its own specialised parser.

The sole purpose of the newly started parser -- which runs while the main one
is paused -- is to find out whether the following token sequence can match
specific language construct. During this trial pass we do not modify
identifier (declaration) tables and the only outcome of the pass is a logical
value: Whether parsing succeeded (the construct matches), or not.

This way, all of the constructs in question are tried, starting at the same
position in source. As all disambiguation rules are precedence-based we can
run the specific parsers in order dictated by these rules and accept the first
successful try as result of the disambiguation. When the result is computed,
the main parser continues knowing which of the paths to choose. Technical
details of implementing this within a bison-generated parser will be discussed
later in implementation section.

Important thing to notice is that disambiguations can nest. In other words,
while resolving an ambiguity, another ambiguity might arise. The concept is
the same, however. We try parsing the possible subconstructs while the parser
which was itself parsing some construct is not running. After resolving the
sub-ambiguity (accepting the first matching successful try) the parser knows
how to carry on.

Hinter
------

Unless ambiguities are handled completely in semantic analysis (which is not
the case in lestes), a C++ syntactic analyzer cannot do without type
information: In some contexts it needs to know whether an identifier denotes a
type or a non-type. (Yet this is not enough to solve all ambiguities.) This
information is provided by hinter.

The hinter processes all identifiers before they are passed to the parser. It
searches the tables for matching declarations, following the rules described
by the norm as Name lookup [3.4]. Set of found declarations is then bound to
the identifier token and may be used if needed. Note that the search takes
place at all times -- even when the results are not actually required.

Hinter must be tightly knit to both the semantic analysis and the parsers.
The former is required because the lookup performed by the hinter must be run
in correct scope. Qualified names, or elaborated specifiers imply the latter,
as these influence parameters of the lookup process.

===
OLD OLD OLD OLD:

If it finishes without an syntax error, the disambiguation is said to succeed
(or commit). Otherwise, the disambiguation fails (rolls back).

NO TIME:
Whenever a (possibly nested) disambiguation is started, start hooks are run.
When a disambiguation succeeds (commits), commit hooks are run.
When a disambiguation fails (rolls back), rollback hooks are run.

NO TIME:
When a disambiguation fails, all disambiguations that were nested in it (and
committed on their own previously) are rolled back. Rollback hooks are run
only once, though. (The nested disambiguations' undo actions are run, however.
See manager_hooks.txt .)
