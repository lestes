Overview

The message reporting framework is used to output user messages.
Each message is reported together with the associated location
in the source code. There are error and warning messages.


Message description

Each message has a name, optional fixed number of parameters and the message
text. The messages are defined in .lmd files, which stands for Lestes message
definition.

The name of the message shall be a C++ identifier, unique within
the namespace, in which the message was defined (see below).

The parameters can have simple or collectible data type, shall not
be of type source_location and must be able to co�perate with formatter
(see Formatter section).

The message text is a special format string, with %N sequences, where N is a
decimal digit (0-9) (TODO pt only two parameters supported, so only %0 and %1
are valid) designating the index of parameter to substitute into the place.
The percent sign can be written as %%, anything else after % is prohibited.
Before substituted into the message text, the parameters are converted to
string representation with the help of formatters.


Structure of .lmd files

The Lestes message definition files are xml files, resembling subset of .lsd
syntax combined with tags for defining messages. The root tag is
<lmd>. Each message is defined in a separate tag, describing the properties
of the message.


Tags common with .lsd

The supported lsd tags meaning is equivalent to that of .lsd files. 

<dox file="...">...</dox>
The message file brief and detailed doxymentation.

<packages><p>...</p></packages>
The place, where the messages will reside.

<imports><i>...</i></imports>
The headers used mainly for parameter data types.

<implementation-imports> ... </implementation-imports>
The headers used mainly for custom formatters.


Special message definition tags

Each error is defined using <error name="..."> tag, specifying the name of the
defined message.  Similarly, warnings are defined using <warning name="...">
tag. This tag contains <dox>, <text> and <param> tags specifying the message.

<dox>
	<bri>...</bri>
	<det>...</det>
</dox>

The doxymentation for the message with the usual .lsd meaning.

<text>The message text %1 %0.</text>

The format text of the message, with placeholders for the arguments.

<param type="type1" />
<param type="type2" formatter="type2_formatter"/>

The definition of the parameters, with data types and optional custom
formatter.  The data type can be collectible, but that needs to be stated
explicitly as "ptr &lt; type &gt;", or any simple type like ulint or lstring.
The type of formatter is written without the enclosing "ptr", because it is
always collectible.  The order of definitions is significant and corresponds
to the indexes N in the placeholder %N sequences. The type of the custom
formatter has to be visible at the time of definition (in the implementation).

Example .lmd file.

<?xml version="1.0" encoding="UTF-8"?>
<lmd xmlns="http://lestes.jikos.cz/schemas/lmd" xmlns:h="http://www.w3.org/TR/REC-html40">
	<file-name>file_name</file-name>

	<dox file="both">
		<bri>
			Brief dox.
		</bri>
		<det>
			Detailed dox.
		</det>
	</dox>

	<packages>
		<p>lestes</p>
		<p>msg</p>
	</packages>
	
	<imports>
		<i>lestes/param_types.hh</i>
	</imports>

	<implementation-imports>
		<i>lestes/spec_formatters.hh</i>
	</implementation-imports>

	<warning name="name_of_warning">
		<dox>
			<bri>
				Brief message doxymentation.
			</bri>
			<det>
				Detailed message doxymentation.
			</det>
		</dox>
		<text>Argument 0: %0 Warning text. Argument 1: %1 Argument 0: %0 again.</text> 
		<param type="ulint" formatter="spec_ulint_formatter" />
		<param type="ptr &lt; bla &gt;" />
	</warning>
</lmd>


Formatters

Formatter is a special class used for converting message arguments to text
representation. There is a default template
::lestes::msg::formatter<name_of_type>, using the ostream << operator,
which is suitable for most of simple types. For special behaviour,
it is possible to use a formatter inherited from the default template.
If the default template is unacceptable, because the << operator is not
defined, it is possible to specialize this template with the desired
behaviour. 

Reporting errors and warnings

To be able to report the defined messages, it is necessary to #include the
generated .m.hh file into the .cc file that uses the message. The .m.hh file
contains all the definitions necessary for using the framework.

The reporting itself is done using the << operator applied on a special entity
called report.  The first object to send to the report shall be the error or
warning message name.  Then shall follow any arguments for the message, in the
same order as defined in the appropriate .lmd file. The last object sent by
the << operator shall be the location to be used for the message.

Example:
report << message_name << argument0 << argument1 << location;

Beware that the compiler will not issue any warnings, if the << operator chain
is unterminated, i.e. if the tail argument and location are omitted. In this
case, the message will not be issued. Also pay attention to types passed as
arguments, because the template parameter deduction will not recognize certain
conversions (e.g. integer literal to ulint, U suffix must be used)
