#LyX 1.3 created this file. For more info see http://www.lyx.org/
\lyxformat 221
\textclass book
\begin_preamble
% after proofreading use the following to hide
% all the boxes around clickable links:
%\usepackage[breaklinks=true,pdfborder={0 0 0}]{hyperref}
% in the meantime let the hyperlinks be surrounded by color boxes:
\usepackage[breaklinks=true]{hyperref}
\end_preamble
\language english
\inputencoding auto
\fontscheme pslatex
\graphics default
\paperfontsize default
\spacing single 
\papersize a4paper
\paperpackage a4
\use_geometry 1
\use_amsmath 0
\use_natbib 0
\use_numerical_citations 0
\paperorientation portrait
\leftmargin 1in
\topmargin 1in
\rightmargin 1in
\bottommargin 1in
\headheight 0.7in
\headsep 0.3in
\footskip 0.3in
\secnumdepth 5
\tocdepth 3
\paragraph_separation indent
\defskip medskip
\quotes_language english
\quotes_times 2
\papercolumns 1
\papersides 2
\paperpagestyle default

\layout Chapter

Implementation Definitions
\layout Standard

All decisions below are implementation defined.
\layout Section

Access to Volatile Quantities
\layout Enumerate

Reference(accessing expression through &) does not constitute an access
 to volatile variable.
\begin_deeper 
\layout Itemize

Rationale: There are difficulties in determining of volatile access.
 References: - info '(gcc)Volatiles' - ss_access_of.has_se
\end_deeper 
\layout Enumerate

Dereference of pointer to volatile does not constitute an access to volatile
 object.
\layout Enumerate

Conversion of volatile lvalue(vol_get) to an rvalue constitutes an volatile
 access, hence a side effect.
\layout Section

Data Types
\layout Enumerate

The plain char type is signed.
\layout Enumerate

The char types are eight bits (one byte) wide two's complement.
\layout Enumerate

The short types are sixteen bits (two bytes) wide two's complement.
\layout Enumerate

The plain int and long types are thirty two bits (four bytes) wide two's
 complement.
\layout Section

The Preprocessor
\layout Enumerate

The physical source file characters are interpreted as either 7-bit ASCII
 or UTF-8 and mapped to the internal representation of source character
 set.
 
\layout Enumerate

The whitespace characters are not retained, they are collapsed into a single
 one in the course of processing
\layout Enumerate

The backslash newline sequence can paste into the universal-character-name
 escape sequence.
\layout Enumerate

Nonempty source file ending in backslash newline sequence, or not ending
 with newline is rejected with an error.
\layout Enumerate

There are no additional members of execution character set supported.
\layout Enumerate

Only basic source characters are supported in header names.
\layout Enumerate

Value of multi-character literal is the value of the last character of the
 literal.
\layout Enumerate

If the value of character literal is out of range, it is truncated.
\layout Enumerate

The path for 
\family typewriter 
#include
\family default 
 search is specified by -I option of the compiler.
\layout Enumerate

The character are combined into the header names via simple concatenation.
\layout Enumerate

Nesting limit is set to 16 to avoid infinite recursion.
\layout Enumerate

Only 
\family typewriter 
#pragma lestes
\family default 
 is defined to switch off the ss2pi.
\layout Enumerate

Concatenation into universal-character-name escape sequence is not recognized.
\layout Enumerate

The quote or double quote character falling into other token category issues
 an error message about unterminated literal, but the token is still created.
\layout Enumerate

Escape sequences are not allowed in first type of header name.
\layout Enumerate

Invalid escape sequences cause error to be issued.
\layout Enumerate

If the third type of include does not match, error is issued.
\layout Enumerate

Empty macro argument is allowed, behave as if no tokens were there.
\layout Enumerate

Sequences in parameters resembling directives are completely legal.
\layout Enumerate

Stringification of all tokens is escaped to be used
\layout Enumerate

Concatenation by 
\family typewriter 
##
\family default 
 not resulting into token is error.
\layout Enumerate

Directive 
\family typewriter 
#line
\family default 
 with number 0 or greater than 32767 is an error.
\layout Enumerate

Directive 
\family typewriter 
#line
\family default 
 with unrecognized content is an error.
\the_end
