#LyX 1.3 created this file. For more info see http://www.lyx.org/
\lyxformat 221
\textclass book
\begin_preamble
\usepackage[breaklinks=true,pdfborder={0 0 0}]{hyperref}
\end_preamble
\language english
\inputencoding auto
\fontscheme pslatex
\graphics default
\paperfontsize default
\spacing single 
\papersize Default
\paperpackage a4
\use_geometry 1
\use_amsmath 0
\use_natbib 0
\use_numerical_citations 0
\paperorientation portrait
\leftmargin 1in
\topmargin 1in
\rightmargin 1in
\bottommargin 1in
\headheight 0.7in
\headsep 0.3in
\footskip 0.3in
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\defskip medskip
\quotes_language english
\quotes_times 2
\papercolumns 1
\papersides 1
\paperpagestyle default

\layout Chapter

Project History and Design Decisions
\begin_inset LatexCommand \label{cha:Summary}

\end_inset 


\layout Section

Analysis
\layout Standard

In the planning phase of the project, we had to study both literature on
 compiler construction and -- more importantly -- the Standard of the C++
 language.
\layout Standard

The Standard is not only quite large, it is also very complicated to comprehend
 during the first couple of readings.
 We spent more than a year trying to understand what exactly is needed to
 be implemented and how.
 This part of the analysis was very important because all the features of
 the language are so interconnected that an overlooked characteristic or
 construct would probably cause a complete overhaul later, when the deficiency
 is found.
 
\layout Standard

During the initial analysis of the task the team has divided into groups
 that were working on different issues of the design.
 These groups were covering the following parts of the compiler: preprocessor,
 parser, semantics, architecture-dependent actions.
 Others focused on investigating the underlying development framework: intermedi
ate representation, automated generation of its structure, and design patterns
 utilization.
\layout Section

Development Framework
\layout Subsection

Structure Generator
\layout Standard

Basic support for data structures in Lestes project is provided by the Standard
 Template Library.
 However, in the early days of designing we conceived that we need more
 than this.
 First of all, C++ lacks automatic garbage collection, which is necessary
 for such a large project.
 Likewise, we wanted automatic support for dumping the structures at any
 time for the purposes of development and debugging (and educational purposes
 in the future).
 Later it turned out that support for specific design patterns would be
 quite useful.
 We have chosen XML as a suitable tool for description of the structures,
 since it is a standardized meta-language for structured documents and it
 is flexible enough.
 The XSLT language emerged naturally as a tool for automatic processing
 the structure description and generating C++ classes with the desired support.
 The total number of classes in the project exceeds one thousand and it
 would be impossible to maintain it fully by hand.
\layout Subsection

Design Patterns
\layout Standard

Bringing some of the design patterns to practice turned out to be quite
 painful.
 In the end they proved to be inevitable, learning to think differently
 was not easy however.
 As we did not have much experience with these, we were not sure what to
 expect and what to require.
 Very good example of misunderstanding were the visitors and their integration
 within the generator: support for them had to be reworked several times
 until an acceptable one was introduced.
 Nevertheless, the final version is robust enough to provide us with automatical
ly-generated code without which the project would not be possible.
\layout Subsection

Memory Management
\layout Standard

In an early stage of the design, it became clear that manual deallocations
 of dynamically allocated structures would be nearly impossible.
 Thus the concept of automated memory management was introduced.
 After several attempts, we implemented a lightweight garbage collector,
 suitable for our purposes.
 
\layout Subsection

Build System
\layout Standard

The build system had undergone quite a rapid changes during the different
 phases of the project.
 because in the beginning it was not clear what features will be needed
 and what features are useless for us.
 Our project is heavily using automatically generated code for various purposes.
 There are XML files, describing not only data structures (which was the
 original idea), but also algorithms, design patterns (visitors), error
 messages, configuration for debugging output, etc.
 Specific groups of these files have to be handled in slightly different
 ways.
\layout Standard

We immediately hit problems with dependencies.
 As we are mixing manually written source code and automatically generated
 sources, generating dependency graph is not a straightforward task.
\layout Standard

In the current project stage, we ended up with four-phase compilation process.
 During the first phase, the whole source tree is recursively processed,
 and dependencies between our XML files are generated (we had to write our
 own XSLT template, which will process XML files and emit dependencies in
 the GNU Make compatible format).
 During the second recursive phase, XML files are processed by XSLT processor
 and our templates, and C++ source code is generated.
 The third phase is again a dependency generating phase - it is not possible
 to generate dependencies between C++ sources sooner, as the generated sources
 were not up-to-date or did not exist at all.
 The fourth phase compiles all C++ sources into corresponding object files,
 and the last phase links object files together.
 The fourth and fifth phases could not be merged into one, as the linking
 is cross-directory, i.e.
 one resulting file may be product of different object files from different
 subtrees, and dependencies between these files can't be easily described
 when running build system recursively.
\layout Standard

Our project also desires extensive testing and running individual tests
 every time after compiling the source tree was not acceptable.
 However, different test cases have different needs (definition of what
 "success" means is not clear and uniform - for one test successful run
 means that parser accepted the source code but semantic analysis rejected,
 other test's correct behavior is to make parser fail, another one must
 compile flawlessly and give predefined output, etc) so the build system
 has to be prepared for this.
\layout Section

Compiler
\layout Subsection

Lexical Analysis (preprocessor)
\layout Standard

Unlike in other programming languages (Pascal, C#, Java), preprocessing
 of C++, described in the ISO Standard, is an important part of C++ lexical
 analysis and cannot be omitted.
 We considered three major approaches to implementing the preprocessor.
\layout Subsubsection

External Program
\layout Standard

The first approach was to omit implementing the preprocessor entirely in
 favor of using an external preprocessor.
 Before being analysed by the compiler, the source code would be sent to
 a standalone preprocessor.
 The preprocessor would carry out all the macro expansions and inclusions
 itself.
 The resulting preprocessed translation unit would be only lexically analysed
 to obtain the tokens.
\layout Standard

Although this choice is suitable for an experimental compiler, the idea
 was abandoned.
 The reason is because the available freestanding preprocessor, GNU cpp,
 did not implement certain requirements of the C++ Standard (universal character
s), while it was extended by many unwanted features.
 Also tracking the location of errors in the preprocessed source would be
 quite difficult.
\layout Subsubsection

Wrapped Module
\layout Standard

The second approach was to encapsulate an open source third party preprocessor
 into the compilers sources and to write wrapper code to interface with
 the preprocessor.
 Handling of the input files would be managed by the compiler, which would
 pass their content into the preprocessor.
 The compiler would then pick up the tokens and continue their processing.
 The candidate was the Wave C++ preprocessor, a part of Boost library open
 source project, dedicated to programming using advanced C++ techniques.
\layout Standard

This approach was abandoned for several reasons.
 Firstly, the Wave preprocessor was in a preliminary version and the support
 for the preprocessor features was questionable.
 Secondly, it was extensively using the non-standard Boost library, which
 could endanger the compilability and portability of the compiler source
 code.
 Moreover, the error reports coming from the preprocessor could be insufficient
 for the purposes of the compiler and would be incompatible with the designed
 error reporting support.
\layout Subsubsection

Own Implementation
\layout Standard

The third approach was to implement the preprocessor from a scratch.
 The main advantage of this choice is simple interfacing with the rest of
 the compiler, chiefly the parser and the error message reporter.
 Another advantage is implementing and supporting exactly the features required
 by the ISO standard, with the possibility to choose only a subset of features,
 vital for operation of the rest of the compiler.
 We chose to write our own implementation, which is suitable for our purposes.
\layout Subsection

Syntactic Analysis (parser)
\layout Standard

We succeeded at implementing a parser that deals with peculiarities of the
 C++ language quite well.
 After a few attempts failed, bison-generated LALR(1) parser suite was chosen.
 What makes our solution special is that ambiguity resolution is performed
 while the main parser is paused.
 We try the alternatives possible by the grammar one after another, until
 one that actually matches the input is found.
 Implementing this in bison was not trivial at all.
 The most problematic is the property of the finite state automaton it is
 based on, namely the look-ahead token.
 Great care must be taken to run the sub-parsers before the automaton looks
 ahead and makes a decision based on the incoming token value.
 Therefore disambiguation is in almost every case started 
\emph on 
after
\emph default 
 a construct that has been processed rather than before a possibly ambiguous
 one, as the latter might involve reading the look-ahead token.
\layout Subsubsection

GLR
\layout Standard

The idea of trying to parse the input and see what the input can actually
 mean is similar to the GLR parsers.
 They approach the problem by running the parsers in parallel.
 Whenever there are more possibilities to choose from, GLR parser 
\emph on 
forks
\emph default 
 to be able to cover all possibilities.
 The parsers then work synchronously, which means that they all read the
 same input token and they do it at the same time.
 Whenever a syntax error occurs, the parser in question simply passes away.
 When no parser is left, the game is over, there really was an error on
 the input.
\layout Standard

Although writing these parsers is much easier, compared to changing the
 grammar to allow for disambiguation breaks, one has much less control over
 the parsing process.
 As all the parsers are equivalent, it is difficult to find out whether
 the current branch is the one that will be confirmed or it will come to
 an dead end.
 Lack of control was the main reason we stepped away from GLR and chose
 the LALR(1) automaton.
\layout Subsubsection

Super-set of the Language
\layout Standard

Another failed attempts include single LALR(1) parser that would not itself
 solve any of the ambiguities of the language, but rather leave this task
 up to the semantic analysis.
 The grammar would have to cover a super-set of the C++ language.
 Presumably, the super-set grammar would not resemble the one in the standard.
\layout Standard

This path was abandoned because the disambiguation would be far too difficult
 to accomplish in semantic analysis.
 Predecessor of the final parser was a concept that assumed that the correct
 path can be recognized by trying to match the ambiguous input against regular
 expressions.
 For relatively long time this was being worked on until we realized that
 the specially-crafted regular expression can be better specified using
 grammar.
 That is what the syntactic analyzer does now -- rather than matching against
 special regular expressions (that would have to support nesting, for example),
 special -- yet normally generated -- LALR(1) parsers are run.
\layout Subsubsection

Hinter
\layout Standard

With regard to the hinter, it was clear from the beginning that we would
 need the type information to be passed back from the semantics.
 All identifiers are tagged with type information before they enter the
 parser.
 It would be possible to only do the search when really needed, but this
 was deemed not worth the effort.
 The hinter would have to be turned on and off from the parser depending
 on the context, which would clutter the grammar rules.
\layout Subsection

Semantic Analysis
\layout Standard

We modeled our intercode on the WHIRL (see 
\begin_inset LatexCommand \htmlurl[the documentation]{http://www.cs.ualberta.ca/%7Epengzhao/ORC/whirl.pdf}

\end_inset 

) at first.
 We took the layered philosophy of WHIRL, yet we abandoned the actual representa
tion of structures, because they were poorly suited to the objective design
 paradigm.
 The five layers of WHIRL were subsequently subdivided and merged to form
 seven-layer architecture.
 After more detailed expression transformation analysis, it was found necessary
 to further subdivide the layers to form a total of nine layers.
 Shortly afterwards it was however found out that the separation that was
 the basis for differentiating various levels is impossible, because C++
 language mandates complete analysis of subexpressions before the whole
 expression transformation starts (e.g.
 in order to determine the type of a subexpression it is necessary to perform
 overload resolution on that subexpression).
\layout Standard

The next requirement of the project assignment was to use DAG-like form
 of intercode.
 We explored the possibility to use SSA (Static Single Assignment Form)
 as it is held as the most progressive intercode form used today.
 We abandoned this approach because of its complexity.
 The current intercode shares some of the features with SSA (e.g.
 there is only one write to any given pseudoregister).
 The cunning encoding of data control dependencies by means of sequence
 points and origins bring the full power of DAG to our intercode.
\layout Subsection

Code Generation
\layout Subsubsection

Machine Description 
\layout Standard

Early idea, we had, is that some of existing architecture description languages
 is to be used.
 For example LISA, CMDL.
 Unfortunately, creation of an exhaustive description in such a language
 is not realistic.
 On the other hand, descriptions for few processors can be found for each
 of these languages.
\layout Standard

One problem is to find or create a description.
 Next, and perhaps even bigger problem, is to extract the needed information
 from it.
 The language usually uses its proprietary data format and tools that generate
 C++ sources with classes representing target architecture are very rare
 and not usable for our goal.
 Even if one exists, it can be impossible to integrate it into our project.
\layout Standard

Contrary way is to create C++ classes for architecture by hand.
 This has several disadvantages: 
\layout Itemize

It is not retargetable in any way.
\layout Itemize

It is error-prone because of a lot of similar entities (e.g.
 instructions).
\layout Itemize

It is not comprehensible.
\layout Itemize

It can become very complex.
\layout Standard

The XML format, which is simple and extensible, was chosen for representing
 machine description.
 C++ classes are generated from it by XSLT.
 XML format is favourable because it is standard, there are many tools (such
 as editors, validators, parsers, processors), we have the know-how thanks
 to structure generator (LSD) and XSLT templates are simple but mighty tool
 for transformation to the desired form.
\layout Subsubsection

Output Format
\layout Standard

We had to decide on what output format is to be generated.
 In early stages of the design, we intended to generate binary object file.
 By reason of special handling of templates it would be necessary to implement
 own linker.
 It also had to be compatible with .obj files generated by GCC in order to
 be able link with libstdc++.
 For portability, it is needed to support few object file formats or portable
 one (DWARF).
 It seems very complicated and due to lack of time it was abandoned.
\layout Standard

We have chosen simple but flexible solution - the backend generates only
 the assembly file.
 It has few advantages:
\layout Itemize

Description of the asm language can be added to machine description so backend
 is not asm language dependent.
\layout Itemize

It is readable by human.
 This is important for debugging.
\layout Itemize

Information about templates could be passed through asm to object file transpare
ntly.
 
\layout Subsubsection

ABI
\layout Standard

The ABI (application binary interface) specification document imposes many
 stringent requirements that are not easy to implement.
 This includes exceptions, VMT, guard variables, RTTI, and DSO object destructio
n.
 Additionally, the majority of literature is accessible in the form of drafts
 only, and is incomplete.
 These features can be thought of as modules, that can be implemented later.
 Now only limited subset of features is implemented, that permits us to
 link against GCC's standard library.
 
\layout Chapter

Conclusion and Future Work
\layout Section

Conclusion
\layout Standard

Quite a indispensable amount of work on the project was initially spent
 on reading the Standard and preparing a framework, that would be robust
 enough to ease such a complex task, as writing the compiler for a language
 like C++ is.
 This was successfully achieved -- as soon as the framework was developed
 and ready-to-use, the evolution of the compiler itself to it's current
 shape was quite rapid.
 This was caused by the fact that the work was properly scheduled in advance
 when studying the Standard and also because the framework we developed
 provided us with features which were rich enough to make the work comfortable.
\layout Standard

From the very beginning of the project it was obvious (and it directly follows
 from the project assignment), that the result is not going to be fully-featured
 compiler.
 The project was meant to be a good foundation for subsequent development.
 The fact that the created framework is well-suited for compiler writing
 task was proved by writing our working compiler, which is modular and open
 for easy adding of new features.
 In addition to that, the C++ Standard was deeply and thoroughly studied
 and according to the gained knowledge, the internal structures for the
 code representation were designed, implemented and documented.
 
\layout Subsection

Doxygen Documentation
\layout Standard

As this project is meant to be used by other programmers to continue on
 developing it, the proper commenting of the code and structures was not
 forgotten.
 It is supported by our framework, the generated sources have Doxygen-compatible
 comments and the Doxygen documentation is provided together with the project.
 As the project currently has over 1000 classes, it is very important that
 structure, represented by namespace hierarchies, can be easily seen in
 the resulting Doxygen documentation, allowing it to be quite easy to navigate
 through and understand the relations between individual classes.
\layout Subsection

Internal Notes
\layout Standard

Last but not least, the result outcome of the study of the C++ standard
 resulted in notes we have created for our purposes.
 These 
\begin_inset Quotes eld
\end_inset 

caveats
\begin_inset Quotes erd
\end_inset 

 can be used in further development and enhancement of the compiler, to
 provide information about non-trivial features and properties of the C++
 language, that have to be carried on mind and must not be forgotten.
 These notes are provided together with the compiler.
\layout Section

Future Work
\layout Standard

We have implemented only a subset of the C++ language, specified by the
 ISO standard.
 The continuation of our work will involve adding support for these missing
 features into the compiler.
 As shown before, a good hinterland for adding such features has been created.
 The most notable features of the language that have to be added are
\layout Itemize

support for constructors in the terms of cooperation between syntactic and
 semantic analysis
\layout Itemize

better support for object types in SA transformations, including member
 access, indirect function calls, casts.
 Improving support for pointers to non-builtin types and implementing missing
 conversions.
\layout Itemize

support for object types in the backend part of the compiler
\layout Itemize

support for templates
\layout Itemize

adding optimizations, either on the intercode level, and also in the backend
\layout Standard

It is important to note here, that there are already assigned master theses
 on top of this compiler, which will continue with developing this code.
 The assigned theses concern the following parts of the compiler
\layout Itemize

parser
\layout Itemize

garbage collector for internal compiler structures
\layout Itemize

framework - code and structure generator
\layout Itemize

backend
\the_end
