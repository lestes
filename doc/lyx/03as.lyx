#LyX 1.3 created this file. For more info see http://www.lyx.org/
\lyxformat 221
\textclass book
\begin_preamble
\usepackage[breaklinks=true,pdfborder={0 0 0}]{hyperref}
\end_preamble
\language english
\inputencoding auto
\fontscheme pslatex
\graphics default
\paperfontsize default
\spacing single 
\papersize Default
\paperpackage a4
\use_geometry 1
\use_amsmath 0
\use_natbib 0
\use_numerical_citations 0
\paperorientation portrait
\leftmargin 1in
\topmargin 1in
\rightmargin 1in
\bottommargin 1in
\headheight 0.7in
\headsep 0.3in
\footskip 0.3in
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\defskip medskip
\quotes_language english
\quotes_times 2
\papercolumns 1
\papersides 1
\paperpagestyle default

\layout Chapter

Abstract Syntax Structures (AS)
\begin_inset LatexCommand \label{cha:Abstract-syntax-structures}

\end_inset 


\layout Standard

The AS acronym stands for Abstract Syntax.
 These structures represent the tree-like structure of the parsed program.
 They do not follow the actual grammar, however.
\layout Section

Declarations
\layout Standard

The proper declaration representation pose a great challenge to the compiler,
 as they are both crucial to the meaning and difficult to handle.
 While the declarations need to be processed early, it is nearly impossible
 to get 
\begin_inset Quotes eld
\end_inset 

hands on
\begin_inset Quotes erd
\end_inset 

 a complete
\begin_inset Foot
collapsed false

\layout Standard

Complete in the syntactic sense.
\end_inset 

 declaration.
 The AS representation of the declarations is constructed nevertheless,
 because such construction proved as a great aid to developing and debugging
 the parser.
\layout Subsection

Declaration Specifiers
\layout Standard

The declaration specifiers exhibit another instance of straightforward abstract
 grammar to AS structures mapping.
 Their handling during the SA phase is described in 
\begin_inset LatexCommand \ref{sub:Decl-spec-seq}

\end_inset 

 in greater detail.
\layout Subsection

Declarators
\layout Standard

The declarators are normalized in a way that eases their processing.
 Each declarator consists of an optional name accompanied by a possibly
 empty sequence of declarator operators.
 A declarator operator is a star (optionally accompanied by cv-qualifiers),
 an array operator (brackets with an optional size), an ampersand, or function
 parameter list.
\layout Standard


\series bold 
\bar under 
Example
\series default 
\bar default 
: The declarator 
\family typewriter 
*a(int)
\family default 
 is represented as 
\family typewriter 
a,(int),*
\family default 
 whereas 
\family typewriter 
b,*,(int)
\family default 
 corresponds to 
\family typewriter 
(*b)(int)
\family default 
.
 The details of declarator handling in SA is discussed in 
\begin_inset LatexCommand \ref{sec:Declarations}

\end_inset 


\layout Section

Names
\layout Standard

The representation of names in the AS came up as a result of long and painful
 evolution.
 The different needs imposed on the representation of names were working
 against each other.
 Thus precluding many of the more straightforward solutions.
 The current solution, i.e.
 the class 
\family typewriter 
as_name
\family default 
 with it's supporting collection of 
\family typewriter 
as_id
\family default 
 subclasses provided us with all the necessary information we needed at
 later stages.
 The first remarkable feature of this approach is that the qualified and
 unqualified names are represented by the same structure.
 The second remarkable feature is that the names so represented can be of
 any kind, for example the same structure can accommodate a templatized
 operator name as well as an ordinary identifier.
 
\layout Standard

The qualified names have the 
\family typewriter 
was_qualified
\family default 
 field set to true whereas the unqualified have not.
 Irrespective of the status of this flag, the field 
\family typewriter 
qualification 
\family default 
holds a reference to another as_name, which represent the scope, the identifier
 is either qualified into, or contained in.
 
\layout Standard

The last remaining field of class 
\family typewriter 
as_name
\family default 
 that was not discussed yet is the representation of the name itself expressed
 as an instance of 
\family typewriter 
as_id
\family default 
 subclass.
 The diverse subclasses of as_id represent the full spectrum of C++ names
 allowed: plain identifiers, operator function ids, templated names, pseudodestr
uctor names, constructor and destructor names.
 Each particular subclass contains all the necessary information to fully
 determine the name.
\layout Section

Statements
\layout Standard

The statements are represented by a manner that directly follows their syntactic
 representation.
 The structure is discussed in the ISO standard in the Chapter 
\series bold 
[6] 
\series default 
(q.v.).
\layout Standard

The statements undergo little or no transformation during parsing and AS
 stage, almost no normalization takes place and the iteration statements
 for and while retain the representation of the condition as a special object
 different from the representation of expressions which takes them apart
 from the do iteration statement which does not permit the condition to
 be a declaration.
 The expression statement is not removed in cases that exhibit use of empty
 expression.
 The declarators in a simple declaration are not separated from each and
 from declaration specifier sequence, but are kept as a unit that is only
 subsequently separated into distinct objects representing the declaration.
 For detailed discussion of the representation of statements in the AS level
 structures please see the doxygen documentation.
\layout Section

Expressions
\layout Standard

The expressions are represented by instances of as_expression subclasses.
 The precedence of operators expressed by the use of several nonterminal
 symbols in the concrete grammar is not directly observable, nor is the
 presence or absence of parentheses in the expression.
 The structure itself reflects the grouping of operands to operators.
 Thus the expressions a+b*c, a+(b*c), and ((a)+((((b))*(c))) are represented
 by the same structure.
\begin_inset Foot
collapsed false

\layout Standard

Except, of course, for the location information.
\end_inset 

 The expression (a+b)*c is represented differently, so as to show the proper
 grouping expressed by the programmer.
\layout Standard

The expression hierarchy on the AS level is formed along the arity of the
 operators represented.
 The binary operators do each have two operands while the unary have only
 one operand.
 Some of the expressions do not take other expressions as arguments.
 We can mention for example 
\family typewriter 
sizeof
\family default 
 expression.
 Literals and identifier expressions alongside with 
\family typewriter 
this
\family default 
 expression do not take any arguments because they represent sources of
 value, not the transformation of values.
 As with statements, more detailed information can be obtained from the
 doxygen documentation and the LSD files (see 
\begin_inset LatexCommand \ref{sub:lsg-formats}

\end_inset 

).
\layout Section

Varia
\layout Standard

This section describes various structures that do not directly correspond
 to a source program construct but are nevertheless necessary for the implementa
tion.
\layout Subsection

Wrappers
\layout Standard

During the parsing it is necessary to pass sequences of AS structures back
 and forth.
 A set of wrappers for lists was developed that permits passing the lists
 as the semantic values of bison symbols without requiring error-prone modificat
ions that would be otherwise necessary.
 The wrappers are manipulated by a set of macros that makes use of their
 common structure and generic algorithms of STL.
 That is used for underlying lists representation.
 
\layout Subsection

Bearers
\layout Standard

For certain bison symbols, there is sometimes a possibility that they do
 not represent any part of the input text, while in other circumstances
 they do.
 A device for passing the semantic value in the second case needs to be
 adapted in order to convey the information about their incorrespondence
 with any portion of the source text in the source first.
 A trivial way how to achieve this would be to pass non-
\family typewriter 
NULL
\family default 
 and 
\family typewriter 
NULL
\family default 
 pointer respectively, but the use of NULL pointers is precluded by the
 mechanism that is used for storing semantic value of the bison symbols.
 A new class is therefore introduced for each such situation, that contains
 the conveyed value or 
\family typewriter 
NULL 
\family default 
but itself is not referenced by 
\family typewriter 
NULL
\family default 
 pointer.
 
\layout Standard

For example in the base-specifier of a class declaration, the keywords virtual,
 public, private and protected that are used to specify accessibility and
 kind of inheritance can be missing in the source program text.
 They are reduced by a bison rule that permits null derivation.
 The semantic value of the bison symbol representing that rule cannot be
 set to 
\family typewriter 
NULL
\family default 
 however.
 So the classes 
\family typewriter 
as_virtual_op_bearer
\family default 
 and
\family typewriter 
 as_access_spec_opt_bearer
\family default 
 are introduced.
\the_end
