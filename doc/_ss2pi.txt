Converting semantic strutures into backend structures


Interface between semantic analysis and backend

There are ?two main interfaces between semantic part and backend part of compiler.
1. Conversion routines from ss structures to pi structures.
	This is contructucted according to visitor design pattern.
	Each ss structure has corresponding visitor, which is responsible
	for converting current structure.

2. Passing pi strucutres to backend.
	This is designed according to builder design pattern.
	Basic pseudoinstruction (pi) blocks are pseudonstrucion and its
	operands. ?labels etc
	After assembling these parts into the whole pseudoinstrucion, it is
	passed to pi_builder, which is responsible for enqeuing into
	backend structures.
	After complete transformation of particular ss structure,
	builder is informed by rail() function, which can be useful
	for debuging.

The whole transformation task is governed by semantic part. Backend
participates in 
1. conversion of types, which cant be in no way maintained by
sematic part itself, because target architecture info is required.
2. queuing of pseudoinstructions by builder, which is to be considered
as input for backend.
3. manipulating with memory-place-holders - such as reference bindindg,
return value passing, copying of temporaries on the stack, etc.



Sequencing points
Graph of sequencing points is the basic skeleton for both semantic structures
and backend structures. Just after transforming ss structures into pi, sequence
point skeleton will have the same structure (i.e. they are isomorphic).

The whole transformation is lazy in the sense, that sequence point will
be created when some newly created pi structure needs it.

Sequencing points can be created in the three contexts:
	1. round the statement
	2. round the funcall
	3. inside the expression
?end of compound statement


Levels of pseudoinstructions

For linearization of pseudoinstructions backend needs to know their 'level'
(for detail see backend documentation - topological ordering of pseudoinstructions).

Since its easy to determine level of pseudoinstruction during its creation,
semantic part will provide it to backend. 

Construction rules of ss sequencing point level:
1. Starting sequencing points(i.e. round the statement) has level of 0.
2. While constructing nested sequence point, it has level of 1 + maximum of previous/next
	sequence point.


Construction rules of pseudoinstrucion level:
1. The pi sequence points have the same level as their corresponding ss counterparts.
2. Pseudoinstrucion level is determined as 1 + maximum of levels
	of operands and previous/next sequence point

Note: by level of operand we consider level of pseudoinstruction, which
	created operand, i.e. origin of such operand. 

Note2: There can be only one store for each pseudoregister, thus there is no problem
of determining the origin of the pseudoinstruction. In the case of pi_mem, there shall
be special map, where will be information about last store into the pi_mem.
[irc - vecerni log 21.02.05]XX novinka viz schuze 03_09
pi_memy jsou jinak _ promennou reprezentuje pi_mem faktory; kdyz chces pristup k promenne
musi se volat backend, kteremu predas "origin" coz je vlastne urceni casoveho razitka.
"origin" na pi_memu pak vlastne znamena cas posledniho zapisu(v ramci fullexpresny - jinak 
je to sequence point pred fullexpresnou).



Expressions
In general, expressions are converted by expr_pi_visitor.
Each expression result is returned in pi_operand - commonly
in pi_preg, in some cases in pi_mem (i.e. representation of memory placeholders,
which will occur in case of lvalues - var_ref, pointers, ?)

The whole conversion is recursive, i.e. when expression operands itself
are expressions, they are converted before the 'top' expression.
When expression is atomic, i.e. its operands can be directly converted
into pseudoinstructions equivalents, or all operands have been recursively
processed then it is converted into its pseudoinstruction equivalent.

Temporaries, which are created while ?evaluating subexpressions(such as
returning value from funcall) are destroyed at the end of full-expression.
End of full-expression is reached when transformed the whole expression
inside expression statement(ss_expr_stmt).(only?)
Rationale: Ansi norm 12.2/3.

Moreover that, temporaries are destructed in reverse order as created,
which is provided via lifo BLAbla. 
Rationale: Ansi norm 12.2/5



Binary expressions

After transformation of operands, binary operator is converted into
pseudoinstruction equivalent (see below). Since there is uniform
transformation to pseudoinstructions, its handled by template
binary_op_visit_template. 

Equivalents
ss_add -> pi_add
ss_sub -> pi_sub
ss_mul -> pi_mul
//TODO div
ss_mod -> pi_mod
ss_shr -> pi_shr
//TODO shl _> sal for signed
ss_shl -> pi_shl
//TODO shr _> sar for signed
//TODO branches
ss_band -> pi_band
ss_bor -> pi_bor
ss_bxor -> pi_bxor
ss_land -> pi_land
ss_lor -> pi_lor
//TODO arr-acc
//TODO ss_assing

First exception is for logical binary expressions, since there are two differences:
1. There is sequence point betwen left and right side
2. Control flow i determined by left side, i.e. the whole exception has similar structure
	as if statement.

The transformation will be done using temporary varible and if statement.
Rationale: Through th if statement a sequence point is added to the end of evaluation of binary logical operators in order to provide a point
        which can be jumped to during the course of evaluation. This jump is mandated by the norm[FIXME] requiring to
        omit evaluation of the right sub-expression depending on the result of the left sub-expression evaluation.
Note: By this we lose the possible optimalization based in mangling the right side of expression and next evaluation after
	current binary logical expression.


?Second exception is for assignemnt operator, which produces sideeffect.

Funcalls

Funcall in ss itself consists of three parts.
All of them has to be converted into equivalent pi structures.
1. Parameters of funcall
	Each parameter which is ss_expression, is firstly converted by
	standard visitor. Result in pi_operand is then passed to copy constructor
	which copies argument-value to temporary variable on stack(created by function_parameter_allocator),
	which will be consequently used inside the funcall.
2. Returning value
	Returing value has to be especially handled by backend allocator 
	allocate_for_ret_val_passing, because it can be
	returned in many different ways according to target architecture.
3. Type of funcall itself
	There are two orthogonal distinctions of funcalls in ss: 
	a)Calling of function 
		1. specified by name (funcall, mfuncall, vfuncall)
		2. nominated by an expression (ifuncall,pfuncall)
	b)Calling of function
		1.member function
			i. static binding (mfuncall, pfuncall)
			ii.dynamic binding through VMT (vfuncall, pfuncall)
		2.non_member function (namespace function) (funcall, ifuncall)

	There are two orthogonal disctinctions of funcalls in pi:
	c)Calling function
		1. indirect call through pointer (icall,icallv)
		2. direct call (call,callv)
	d)Calling function
		1. with return value (call, icall)
		2. with no return value (void) (callv, icallv)

Transformation can be described as a map:
	a1 b1i 	-> c2 d1/2
	a1 b1ii	-> c1 d1/2
	a1 b2 	-> c2 d1/2
	a2 b1i	-> c1 d1/2
	a2 d1ii	-> c1 d1/2
	a2 b2	-> c1 d1/2


Funcall transformation can be abstractly written as:
FIXME uz zase se zmenil IFace do BENDU
    1.	pi_call construction.
    
    2.	Transformation of return value
	
		function_parameter_allocator* allocator = function_parameter_allocator::instance(callee, caller);
	    pi_mem into = allocator->allocate_for_ret_val_passing(ss_type rv_type)
	    destructor
		
	Note: Temporary for return value shall be generated such that its
	origin is the the affected funcall.

	Rationale for mem(vs operand): secret argument can be other than usual arguments for funcall.
	Backend shall generate load pseudoinstruction itself. ?todle je divny - JAZ: ano to je. nebude to delat sam od sebe, ale 
	sem mu rekne at to udela v bode 4.
	
    3.	Transformation of arguments 
    
    	For each argument:
	i. Recursive argument expression transformation to pi.
	
	ii. Allocation temporary variable, which will be used inside the function through function_parameter_allocator
	
		function_parameter_allocator* allocator = function_parameter_allocator::instance(callee, caller);
	    pi_mem into = allocator->allocate_regular(ss_type arg_type, ulint arg_index)
		
		Note: pi_mem represents allocated memory chunk.						 
	    
	iii. Construction of copy constructors of parameter (also for simple types).
		list pi_pinstr = copy_constructor_call_generator::generate(pi_mem into, pi_operand arg)
	
		pi->builder (pi_instr)
	iv. Construction of destructor for temporary variable (i.e. temporary deletion).    
		All destrutrors below are pushed into list, which will flushed at the end of full-expression.
		Note:	nsp shall be settled at the end of full-expression
			psp will be psp from this funcall
		    

    4.  Determining pseudoinstruction level

    5. Transmission of pseudoinstruction call to backend 
	pi->builder(pi_call)
	
    5.  Construction of destructors of all parameters (ie temporaries deletion).
    6.  Destruct return value.
    
    
at 3:	list pi_pi copy_constructor_call_generator::generate(pi_mem into, pi_operand arg)

	if (arg->type == builtin) return new pi_mov(into, arg)
	    else
		return new pi_call(into,arg,into->type->copy_constructor)
    Note: this is the right place for copy-constructor elimination.

    
TODO
this - pro konstrukce funcallu to same jako rv, krom: nevolam destruktor; + kopie pointeru na zasobnik;
    otatkou je reprezentace - nejspise 4.varref upraveny
    Moznosti:
	    1. nechat jako lvalue(varref). na implicit object argument se bude pohlizet jako by tam byla tecka (norma)  - tj .objekt pred teckou se bude vetsinou tvarit jako lvalue.
	    2. pomoci GET. konverze na rvalue 
	    3. adress_of ktera se od obou lisi, ze na zakladni tvarkoukame jako na tvar se sipkou (tj. teck a se transformuje naz vzeti adresy +sipku) 
	    4. modifikace varrefu - v pripade, ze rvalue budeme mit specialni chlivecek pro konverzi z rvalue na lvalue (ale jen pro potreby tohoto). kopie pointru na zasobniku znamena, ze misto volani copy constructoru se vytvori pi_lda
	

pi_mem ss_funcall_rv2pi_mem(pi_operand rv,ss_type rv_type)
pi_mem into = ss_funcall_arg2pi_call_arg(pi_operand arg,ss_type arg_type, ulint arg_index)
expr_pi_visitors on ss_funcalls.



Statements


Compound statement

When leaving the compound statement all the variables allocated
in the current compound statement [] has to be destroyed in the reverse order. This may occur in 
many cases (end of compound statement, breakable statements, goto, return statements).
To ensure this we introduce for each compound statement the table of destructors, 
which destroy already allocated variables in the current compound statement.
Thus, when jumping from nested compound statements, we have to proceed all the tables from 
compound statements we are jumping from. 

---------
Destructor tables 
There is only one destructor_tables object for the whole translation unit.
The idea is to create destructor table for each tranformed compound statement(starting with
a compound representing namespace scope),
where will be stored information to proper destruction of objects created in current 
compound statement.

*To maintain correspondence with currently transformed compound statement we call 
open/close_copmpound_stmt functions, which also provides allocation of proper table.

*To register variables currently allocated we call add_destructor function.

*Calling back_roll_* functions always lead to construction of
	1. proper destructor calls of variables currently used in compound statement(s)
	2. instructions for deallocation of memory placeholders for those variables
This construction also take care of proper sequence point binding for created 
pseudoinstructions.


Destructor_tables interface
destructor_table open_compound_stmt(ss_compound_stmt)	//returns DT, where to add new destructors

destructor_table close_compound_stmt()			//returns DT, where to continue adding new destructors; closing of current destructor table

void destructor_add(ss_type type, pi_mem_factory variable)

void back_roll_from_current_compound_stmt()	//normal end of compound_stmt;psp taken from current compound-stmt.destructor_sp, nsp taken from current compound_stmt.nsp

void back_roll_to_compound_stmt(ss_compound_stmt where,psp,nsp)	//abnormal end of compound_stmt;psp,nsp taken from current statement(e.g. break, continue,  goto)

void back_roll_from_function(psp,nsp)	//abnormal end of compound_stmt; psp,nsp taken from current return statement

Destructor table implementation
Destructors table is implemented by simple stack, as we need to destruct variables in reverse order.
open_compound_stmt is called for each compound statement and creates new stack for variable in 
such a compound statement. closing simply remove this table from destructor tables.
This implementation is not sufficient to provide information for goto statements tranformation, 
however interface is designed broadly enough (for details see goto statement section LINK).

---------
Transformation of compound statement can be abstracly written as:
1. Creating empty destructor table
2. Transformation of each statement in current compound statement.
3. Desctruction of local variables introduced in current compound statement
?New Sequence point?

Note: Backend can start make its transformation after tranforming the whole function body.
	Rationale: a. Sequence points are not known on time.
		b. goto statements require next phase of ss->pi transformation
		
		
//FIXME statements
BEGIN & END of statement => sequencing point

conditional + iterative statements: Condition is already convetred to bool from overloading LINK phase.

if statement 
1.
        a. c++:
                if ( a>x ) b++; ale c++'
        b.
                ld $5,(a)
                ld $6,(x)
                cmp $1,$5,$6
                bg @1,$1
                ba @2
         @1:ld $2,(b)
                ldi $3,1
                add $4,$3,$2
                st (b),$4
                ba @3
         @2:ld $7,(c)
                ldi $8,1
                add $9,$7,$8
                st (c),$9
                ba @3
         @3:


 2.
        a. if (++a || ++b ) ...
        b. je tu problem: zkracene vyhodnocovani
        c. reseni vytvori se temporalni promenna
                ld $1, (a)
                ldi $2,1
                add $3,$1,$2
                st (a),$3
                cvt:bool $4,$3
                ldi $5,0
                cmp $6,$4,$5
                be @1,$6
                bne @2,$6
         @1:ldi $7,1
                ld $8,(b)
                add $9,$8,$7
                st (a),$9
                ldi $10,0
                cmp $11,$9,$10
                sne $12,$1
                st (t1),$12
                ba @3
         @2:ldi $13,1
                st (t1),$13
                ba @3
         @3:
                ld $14,(t1)
                ...
                dealloc(t1) - dealokace temporalni promenne


goto statement

To transformation goto statement we have to know labels inside the currenttly transformated function.
To provide it, we have to collect all the labels inside the function, which can be done
1a) in as->ss transformation
1b) in ss->pi transformation

Transformation of goto statement cannot be solved in the first phase of ss->pi.
Rationale: 	a. Destructors for locally introduced variables of the scopes, which we are leaving,
	have to be called. It is not clear which have to be called.
		b. In 1b case we even don't know where to jump.

Thus, for transforming goto statement we need second pass of ss->pi algorithm, in which it will
be known proper destructors and jumping positions.

Destructor tree has to be done in the first pass, in order to manage whole transformation in the second pass.

Now the construction

Active object is an object that is in a scope or that would bi in a scope provided that
it is not hidden by another active object declaration.
Example
{int i;
	{int i;	// here both i's are active, despite the first is hidden by the second.
	}
}

	
Destructor tree (DT) is a structure representing active abjects for any given point in a function.
It consists of nodes, each of them having a parent.

The path from node to the farthest ancestor represents a sequence of active object at a given point
in the function. In addition to that it also represents a compound statement boundaries in a manner
that can be used to distinguish to which coumpound statement the active object belong.
It can be seen, that this structure is tree-like.

Having constructed DT, we can index the corresponding node in DT by the statement start.
Now to transform a goto statement consists of:
1. To find the nearest common ancestor(NCA) of the goto statement and goto-statement-target (statement
	corresponding to the reffered label)
2. To generate destructor call for active object on the path from goto statement to NCA.
3. To check, whether all the active object from target to NCA are non_initialized POD types [].
4. To allocate all the variables from target to NCA.
5. To generate pseudinstruction of jump to target.

Note: goto statements are not implemented. To construct DT, implementation of destructor tables
have to be changed from single LIFO to tree-structure, however current interface for destructor
table is enough and dont need any change.

