sa_declarator_context initialization fields:

decl_specs: declaration specifiers - either list of as_declaration_specifier,
or sa_decl_spec (less likely, although we need it already processed, in sa_declarator,
but causes code overhead for the caller)

access_specifier: access specifier (PUBLIC,PROTECTED,PRIVATE), probably
typedefed enum

parent_decl_seq: parent declaration sequence to insert the analysed
declarations into, also links to parent compound statement

#if 0
"context/scope type": enumeration of significant discriminator (namespace, class,
function, block), perhaps not necessary, extracted from parent_decl_seq
#endif

processing of decl_specs and parent_decl_seq in order to get c/s type in
context create() is not possible due to lsd limitations, so the context will
be created with a static method of another name, perhaps prepare()

ptr<sa_declarator_context> sa_declarator_context::prepare(
      ptr< list< srp<as_declaration_specifier> > > a_decl_specs,
      access_specifier::type a_access_specifier,
      ptr< ss_decl_seq > a_parent_decl_seq);
